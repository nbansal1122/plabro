package com.desmond.parallaxviewpager;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ScrollView;

/**
 * Created by desmond on 1/6/15.
 */
public abstract class ParallaxViewPagerBaseActivity extends AppCompatActivity implements ScrollTabHolder {


    public final String TAG = getTag();

    protected static final String IMAGE_TRANSLATION_Y = "image_translation_y";
    protected static final String HEADER_TRANSLATION_Y = "header_translation_y";

    protected View mHeader;
    protected ViewPager mViewPager;
    protected ParallaxFragmentPagerAdapter mAdapter;

    protected int mMinHeaderHeight;
    protected int mHeaderHeight;
    protected int mMinHeaderTranslation;
    protected int mNumFragments;
    public String pmMessage;
    public int PMCode;

    protected abstract void initValues();

    protected abstract void scrollHeader(int scrollY);

    protected abstract void setupAdapter();

    protected int getScrollYOfListView(AbsListView view) {
        View child = view.getChildAt(0);
        if (child == null) {
            Log.d(TAG, "Child is Null");
            return 0;
        }

        int firstVisiblePosition = view.getFirstVisiblePosition();
        int top = child.getTop();

        int headerHeight = 0;
        if (firstVisiblePosition >= 1) {
            headerHeight = mHeaderHeight;
        }
        int scrollY = -top + firstVisiblePosition * child.getHeight() + headerHeight;
        Log.d(TAG, "SCrollY::" + scrollY + ":Top:" + top + "ChildPosition:" + (firstVisiblePosition * child.getHeight()) + ", HeaderHeight:" + mHeaderHeight);

        return scrollY;
    }

    protected String getTag() {
        return "";
    }

    protected ParallaxViewPagerChangeListener getViewPagerChangeListener() {
        return new ParallaxViewPagerChangeListener(mViewPager, mAdapter, mHeader);
    }

    @Override
    public void adjustScroll(int scrollHeight, int headerHeight) {
    }

    @Override
    public void onListViewScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount, int pagePosition) {
        Log.d(TAG, "CurrentItem :" + mViewPager.getCurrentItem() + ", PagePosition" + pagePosition);
//        if (mViewPager.getCurrentItem() == pagePosition) {
//            scrollHeader(getScrollYOfListView(view));
//        }
        scrollHeader(getScrollYOfListView(view));
    }

    @Override
    public void onScrollViewScroll(ScrollView view, int x, int y, int oldX, int oldY, int pagePosition) {
//        if (mViewPager.getCurrentItem() == pagePosition) {
//            scrollHeader(view.getScrollY());
//        }
        scrollHeader(view.getScrollY());
    }

    @Override
    public void onRecyclerViewScroll(RecyclerView view, int dx, int dy, int scrollY, int pagePosition) {
        scrollHeader(scrollY);
    }

}
