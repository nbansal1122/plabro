package com.desmond.parallaxviewpager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by desmond on 1/6/15.
 */
public class ListViewFragment extends ScrollTabHolderFragment implements View.OnClickListener{
    protected static final String ARG_POSITION = "position";

    protected ListView mListView;
    protected int mPosition;
    protected View rootView;



    @Override
    public void onClick(View view) {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getViewId() != 0) {
            rootView = inflater.inflate(getViewId(), null);
            findViewById();
            return rootView;
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    protected void findViewById(){

    }

    public View findView(int id) {
        return rootView.findViewById(id);
    }

    public void setClickListeners(int... viewIds) {
        for (int id : viewIds) {
            rootView.findViewById(id).setOnClickListener(this);
        }
    }

    public void setClickListeners(View... views) {
        for (View view : views) {
            view.setOnClickListener(this);
        }
    }

    protected void setText(int textViewID, String text) {
        TextView textView = (TextView) findView(textViewID);
        textView.setText(text);
    }

    protected void setText(int textViewID, String text, View row) {
        TextView textView = (TextView) row.findViewById(textViewID);
        textView.setText(text);
    }

    protected int getViewId(){return  0;}



    @Override
    public void adjustScroll(int scrollHeight, int headerHeight) {
        if (mListView == null) return;

        if (scrollHeight == 0 && mListView.getFirstVisiblePosition() >= 1) {
            return;
        }

        mListView.setSelectionFromTop(1, scrollHeight);
    }

    protected void setListViewOnScrollListener() {
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (mScrollTabHolder != null) {
                    mScrollTabHolder.onListViewScroll(
                            view, firstVisibleItem, visibleItemCount, totalItemCount, mPosition);
                }
            }
        });
    }
}
