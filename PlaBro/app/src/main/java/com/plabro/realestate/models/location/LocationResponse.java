package com.plabro.realestate.models.location;


import com.plabro.realestate.models.ServerResponse;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class LocationResponse extends ServerResponse {

    LocationListOutputParams output_params = new LocationListOutputParams();


    public LocationListOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(LocationListOutputParams output_params) {
        this.output_params = output_params;
    }

}
