package com.plabro.realestate.models.country;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.AppController;
import com.plabro.realestate.models.ServerResponse;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.JSONUtils;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;

import java.io.IOException;
import java.io.InputStream;

public class CountryCodes extends ServerResponse {

    @SerializedName("output_params")
    @Expose
    private Output_params output_params;

    @SerializedName("RT")
    @Expose
    private String RT;

    /**
     * @return The output_params
     */
    public Output_params getOutput_params() {
        return output_params;
    }

    /**
     * @param output_params The output_params
     */
    public void setOutput_params(Output_params output_params) {
        this.output_params = output_params;
    }

    /**
     * @return The RT
     */
    public String getRT() {
        return RT;
    }

    /**
     * @param RT The RT
     */
    public void setRT(String RT) {
        this.RT = RT;
    }

    public static CountryCodes getCountriesFromAssets() {
        try {
            InputStream is = AppController.getInstance().getAssets().open(Constants.FILE_COUNTRY_JSON);
            String countriesJson = Utility.convertStreamToString(is);
            if (!TextUtils.isEmpty(countriesJson)) {
                return (CountryCodes) JSONUtils.parseJson(countriesJson, CountryCodes.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}