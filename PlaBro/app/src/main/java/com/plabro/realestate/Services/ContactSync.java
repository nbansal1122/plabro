package com.plabro.realestate.Services;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.activeandroid.query.Delete;
import com.crashlytics.android.Crashlytics;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.fragments.ContactsFragment;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.Contact;
import com.plabro.realestate.models.contacts.ContactInfo;
import com.plabro.realestate.models.contacts.ContactUploadResponse;
import com.plabro.realestate.models.contacts.PlabroFriends;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.utils.ContactsQuery;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;


public class ContactSync extends Service implements Handler.Callback, Loader.OnLoadCompleteListener<Cursor> {
    Handler handler;
    private static final String TAG = ContactSync.class.getSimpleName();
    private HashMap<String, String> contactsMap = new HashMap<>();
    private List<PlabroFriends> plabroFriends = new ArrayList<PlabroFriends>();
    private boolean isLast;
    private boolean errorSyncingContacts = false;
    CursorLoader mCursorLoader;

    public static HashMap<String, String> allContacts = new HashMap<>();

    public static void addContacts(HashMap<String, String> contacts) {
        allContacts.putAll(contacts);
    }


    public static boolean isStarted;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onCreate() {
        super.onCreate();

        if (!(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M)) {
            if (AppController.getInstance().checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

            } else {
                stopSelf();
                return;
            }
        }
        handler = new Handler(this);
        isStarted = true;
        Log.d(TAG, "ServiceStarted");
        mCursorLoader = new CursorLoader(this, ContactsQuery.CONTENT_URI,
                ContactsQuery.PROJECTION, ContactsQuery.SELECTION, null,
                ContactsQuery.SORT_ORDER);
        mCursorLoader.registerListener(1, this);
        mCursorLoader.startLoading();
        PBPreferences.saveData(PBPreferences.KEY_AUTO_SYNC, true);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void startContactSyncing(Context ctx) {
        if (!isStarted) {
            Log.d(TAG, "Trying to start contact syncing");
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) {
                ctx.startService(new Intent(ctx, ContactSync.class));
                Log.d(TAG, "SDK Version greater than = to M");
                return;
            } else if (ctx.checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Checking permission");
                ctx.startService(new Intent(ctx, ContactSync.class));
            }
        }
    }

    public static void stopContactSyncing(Context ctx) {
        ctx.stopService(new Intent(ctx, ContactSync.class));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return START_STICKY;
    }

    private void uploadDataOnUiThread(final List<ContactInfo> contacts, final String data, final int i) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                uploadContacts(contacts, data, i);
            }
        });
    }

    private void uploadContacts(final List<ContactInfo> contacts, final String data, final int i) {


        if (!Util.haveNetworkConnection(ContactSync.this)) {
            sendErrorBroadcast();
            return;
        }


        final HashSet<ContactInfo> clonedContacts = new HashSet<ContactInfo>();
        clonedContacts.addAll(contacts);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.PLABRO_UPLOAD_CONTACTS_POST_PARAM_CONTACTS_ARR, data);
        Log.d(TAG, "Contacts Array:" + data);
        boolean isContactSyncedFirstTime = PBPreferences.getData(PBPreferences.KEY_IS_CONTACT_SYNCED_FT, false);
        if (i == 0 && !isContactSyncedFirstTime) {
            params = Utility.getInitialParams(PlaBroApi.RT.UPLOAD_CONTACTS, params);
            PBPreferences.saveData(PBPreferences.KEY_IS_CONTACT_SYNCED_FT, true);
        } else {
            params = Utility.getInitialParams(PlaBroApi.RT.UPLOAD_CONTACTS_INC, params);
        }
        AppVolley.processRequest(Constants.TASK_CODES.CONTACT_UPLOAD, ContactUploadResponse.class, null, String.format(PlaBroApi.getBaseUrl(this)), params, RequestMethod.POST, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                if (response != null) {
                    //                 Utility.dismissProgress(progress);
                    PBPreferences.setContactsState(true);
                    ContactUploadResponse contactUploadResponse = (ContactUploadResponse) response.getResponse();

                    for (ContactInfo c : clonedContacts) {
                        ContactInfo info = new ContactInfo();
                        info.setDisplayName(c.getDisplayName());
                        info.setContactId(c.getContactId());
                        info.setIsSynced(true);
                        info.setContactNumber(c.getContactNumber());
                        Log.d(TAG, "Saving Cloned Contacts:" + c.getContactId());
                        long d = info.save();
                    }
                    getPlabroFriendsFromServer(PlaBroApi.RT.GET_PLABRO_FRIENDS);
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

                sendErrorBroadcast();
            }
        });
    }

    private void sendErrorBroadcast() {
        errorSyncingContacts = true;
        sendBroadcast("Some error in syncing contacts, please try again");
    }

    private void getPlabroFriendsFromServer(final String rt) {
        if (!Util.haveNetworkConnection(ContactSync.this)) {
            sendErrorBroadcast();
            return;
        }
        HashMap<String, String> params = Utility.getInitialParams(rt, null);
        AppVolley.processRequest(Constants.TASK_CODES.GET_PLABRO_FRIENDS, ContactUploadResponse.class, null, String.format(PlaBroApi.getBaseUrl(this)), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                if (response != null) {
                    ContactUploadResponse contactUploadResponse = (ContactUploadResponse) response.getResponse();
                    if (contactUploadResponse.isStatus()) {
                        plabroFriends = contactUploadResponse.getOutput_params().getData();
                        if (plabroFriends != null && plabroFriends.size() > 0) {
                            for (PlabroFriends f : plabroFriends) {
                                f.save();
                            }

                        }
                        sendBroadcast("");
                    } else {
                        sendErrorBroadcast();
                    }
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                errorSyncingContacts = true;
                sendBroadcast("Some error while refreshing contacts");
            }
        });


    }

    private void sendBroadcast(String errorMessage) {
        Intent i = new Intent();
        i.setAction(ContactsFragment.BROADCAST_ACTION_SYNC_CONTACTS);
        i.putExtra("isLast", isLast);
        i.putExtra("errorMessage", errorMessage);
        Log.d(TAG, errorSyncingContacts + "");
        PBPreferences.saveData(PBPreferences.KEY_ERROR_SYNC_CONTACTS, errorSyncingContacts);
        sendBroadcast(i);
        if (isLast && errorSyncingContacts) {
            stopSelf();
        }
    }

    private String modifyContactAndGetLastTenDigits(String mobileNumber) {
        Log.d(TAG, "Number before modifying:" + mobileNumber);
        String mobile = mobileNumber.replace("(", "").replace(")", "").replaceAll("-", "").replaceAll(" ", "").replaceAll("\\.", "").trim();
        return getLastTenDigits(mobile);
    }
    private String modifyContact(String mobileNumber){
        Log.d(TAG, "Number before modifying:" + mobileNumber);
        return mobileNumber.replace("(", "").replace(")", "").replaceAll("-", "").replaceAll(" ", "").replaceAll("\\.", "").trim();
    }

    private String getLastTenDigits(String number) {
        return ActivityUtils.getLastTenDigits(number);
    }

    @Override
    public boolean handleMessage(Message message) {
        return false;
    }

    private HashSet<String> contactIds;

    @Override
    public void onLoadComplete(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG, "OnLoad Finished");

        contactIds = ContactInfo.getContactIds();
        fetchContacts(data);
    }

    private void fetchContacts(final Cursor cursor) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    boolean isContactsDelectedOnce = PBPreferences.getData(PBPreferences.IS_CONTACTS_DELETED, false);
                    if (!isContactsDelectedOnce) {
                        try {
                            new Delete().from(ContactInfo.class).execute();

                        } catch (Exception e) {
                            Crashlytics.logException(e);
                        }
                        PBPreferences.saveData(PBPreferences.IS_CONTACTS_DELETED, true);
                    }
                    if (cursor != null && cursor.getCount() > 0) {
                        contactsMap = PBPreferences.loadMap(PBPreferences.CONTACTS_MAP);
                        JSONArray array = new JSONArray();
                        List<ContactInfo> contacts = new ArrayList<ContactInfo>();
                        int loopCounter = 0;
                        while (!cursor.isClosed() && cursor.moveToNext()) {
                            Contact contact = new Contact();
                            contact.setContactUri(ContactsContract.Contacts.getLookupUri(
                                    cursor.getLong(ContactsQuery.ID),
                                    cursor.getString(ContactsQuery.LOOKUP_KEY)));
                            contact.setDisplayName(cursor.getString(ContactsQuery.DISPLAY_NAME));
                            String contactId =
                                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                            contact.setContactId(contactId);
                            List<String> userPhones = new ArrayList<String>();

                            //
                            //  Get all phone numbers.
                            //
                            ContentResolver cr = getContentResolver();
                            Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                            if (null != phones && !phones.isClosed()) {
                                while (phones.moveToNext()) {
                                    String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                                    int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                                    userPhones.add(number);
                                    switch (type) {
//                                case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
//                                    // do something with the Home number here...
//                                    break;
                                        default:
                                            if (null != cursor) {
                                                String lastTenDigitsNumber = modifyContactAndGetLastTenDigits(number);
                                                String contactNumber = modifyContact(number);
                                                // Log.d(TAG, "Putting in contacts" + contactNumber + "Vs " + contact.getDisplayName());
                                                ContactInfo info = new ContactInfo();
                                                info.setDisplayName(cursor.getString(ContactsQuery.DISPLAY_NAME));
                                                info.setContactId(contactId);
                                                info.setContactNumber(contactNumber);

                                                contactsMap.put(lastTenDigitsNumber, contact.getDisplayName());


                                                Log.i(TAG, "Contact ID :" + contactId);
//                                                contacts.add(info);
//                                                array.put(contactNumber);
                                                if (!contactIds.contains(contactId)) {
                                                    array.put(contactNumber);
                                                    Log.d(TAG, "Saving Contact:" + contactNumber + ":" + info.getDisplayName());
                                                    info.setIsSynced(false);
                                                    info.save();
                                                    contacts.add(info);
                                                } else {
                                                    Log.d(TAG, "Contains Contact:" + contactNumber + ":" + info.getDisplayName());
                                                }

                                                if (array.length() == 100) {
                                                    isLast = false;
                                                    final String data = array.toString();
                                                    array = new JSONArray();
                                                    PBPreferences.saveMap(contactsMap, PBPreferences.CONTACTS_MAP);
                                                    addContacts(contactsMap);
                                                    List<ContactInfo> infos = new ArrayList<ContactInfo>();
                                                    infos.addAll(contacts);
                                                    uploadDataOnUiThread(infos, data, loopCounter);
                                                    //uploadDataOnUiThread(data, loopCounter);
                                                    loopCounter++;
                                                    contacts.clear();

                                                }
                                            }


                                            break;
//                                case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
//                                    // do something with the Work number here...
//                                    break;
                                    }
                                }
                                phones.close();
                            }

                        }
                        PBPreferences.saveMap(contactsMap, PBPreferences.CONTACTS_MAP);
                        addContacts(contactsMap);
                        isLast = true;
                        if (array.length() > 0) {
                            uploadDataOnUiThread(contacts, array.toString(), loopCounter);
                        } else {
                            uploadDataOnUiThread(contacts, array.toString(), loopCounter);
//                            sendBroadcast("");
                        }
                        loopCounter = 0;
                    }
                } catch (Exception e) {
                    isLast = true;
                    sendErrorBroadcast();
                }
            }
        }).start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isStarted = false;
        if (mCursorLoader != null) {
            mCursorLoader.unregisterListener(this);
            mCursorLoader.cancelLoad();
            mCursorLoader.stopLoading();
        }
    }
}
