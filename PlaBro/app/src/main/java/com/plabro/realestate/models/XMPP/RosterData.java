package com.plabro.realestate.models.XMPP;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by nitin on 11/06/15.
 */
@Table(name = "RosterData", id = BaseColumns._ID)
public class RosterData extends Model {
    @Column(unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String user;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Column
    private String userName;
    @Column
    private String status;
    @Column
    private String type;
    @Column
    private String phoneNumber;
    @Column
    private boolean isAvailable;
    @Column
    private boolean isAway;

    public long getLastSeenTimeStamp() {
        return lastSeenTimeStamp;
    }

    public void setLastSeenTimeStamp(long lastSeenTimeStamp) {
        this.lastSeenTimeStamp = lastSeenTimeStamp;
    }

    @Column

    private long lastSeenTimeStamp;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public boolean isAway() {
        return isAway;
    }

    public void setIsAway(boolean isAway) {
        this.isAway = isAway;
    }
}
