package com.plabro.realestate.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.NewsFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.ActivityUtils;

import java.util.HashMap;

/**
 * Created by nitin on 18/08/15.
 */
public class NewsHolder extends BaseFeedHolder {
    private TextView newsSummary, newsDesc, newsInfo, readMore;
    private ImageView newsImage;

    public NewsHolder(View itemView) {
        super(itemView);
        newsSummary = findTV(R.id.tv_news_summ);
        newsDesc = findTV(R.id.tv_news_desc);
        newsInfo = findTV(R.id.tv_news_time_info);
        newsImage = (ImageView) findView(R.id.iv_news);
        readMore = findTV(R.id.tv_read_more_news);
    }


    @Override
    public void onBindViewHolder(final int position, BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        if (feed instanceof NewsFeed) {
            final NewsFeed f = (NewsFeed) feed;
            ActivityUtils.loadImage(ctx, f.getImg(), newsImage, R.drawable.default_bg_news);
            newsImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("source", "News Card Image View");
                    wrData.put("ScreenName", TAG);
                    trackEvent(Analytics.CardActions.CardClicked, position, f.get_id(), f.getType(), wrData);
                    ActivityUtils.openImageGallery(ctx, position, f.getImg());
                }
            });
            newsSummary.setText(f.getSumm());
            newsDesc.setText(f.getText());
            newsInfo.setText(f.getTime());
            if (null != f.getText() && f.getText().length() > 130) {
                readMore.setVisibility(View.VISIBLE);
            } else {
                readMore.setVisibility(View.GONE);
            }
            readMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!f.isExpanded()) {
                        trackEvent(Analytics.CardActions.Expanded, position, f.get_id(), f.getType(), null);
                        newsDesc.setMaxLines(25);
                        readMore.setText("READ LESS");
                    } else {
                        newsDesc.setMaxLines(3);
                        readMore.setText("READ MORE");

                    }
                    f.setIsExpanded(!f.isExpanded());
                }
            });
        }
    }
}
