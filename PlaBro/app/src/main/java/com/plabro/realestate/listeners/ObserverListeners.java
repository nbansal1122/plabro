package com.plabro.realestate.listeners;

/**
 * Created by hemant on 17/08/15.
 */
public class ObserverListeners {

    ShoutObserver.PBObserver pbObserver;
    String eventType = "";

    public ShoutObserver.PBObserver getPbObserver() {
        return pbObserver;
    }

    public void setPbObserver(ShoutObserver.PBObserver pbObserver) {
        this.pbObserver = pbObserver;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
}
