package com.plabro.realestate.adapter;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.plabro.realestate.models.services.Services_list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 25/11/15.
 */
public class ServicesAdapter extends RecyclerView.Adapter {

    List<Services_list> list = new ArrayList<>();
    AppCompatActivity context;

    public ServicesAdapter(AppCompatActivity context, List<Services_list> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    private class ServiceHolder extends RecyclerView.ViewHolder {

        private ImageView serviceImage;
        private TextView serviceText;

        public ServiceHolder(View itemView) {
            super(itemView);
        }
    }
}
