package com.plabro.realestate.models.auctions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nitin on 02/02/16.
 */
public class AuctionInfo {
    @SerializedName("auction_summary")
    @Expose
    private String auction_summary;

    @SerializedName("auction_status")
    @Expose
    private String auction_status;
    @SerializedName("auction_time")
    @Expose
    private String auction_time;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("auction_participants")
    @Expose
    private String auction_participants;

    public String getAuction_summary() {
        return auction_summary;
    }

    public void setAuction_summary(String auction_summary) {
        this.auction_summary = auction_summary;
    }

    public String getAuction_status() {
        return auction_status;
    }

    public void setAuction_status(String auction_status) {
        this.auction_status = auction_status;
    }

    public String getAuction_time() {
        return auction_time;
    }

    public void setAuction_time(String auction_time) {
        this.auction_time = auction_time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuction_participants() {
        return auction_participants;
    }

    public void setAuction_participants(String auction_participants) {
        this.auction_participants = auction_participants;
    }
}
