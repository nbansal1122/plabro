package com.plabro.realestate.Services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.CallLog;
import android.support.annotation.Nullable;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.Calls.CallData;
import com.plabro.realestate.utilities.PBPreferences;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by nitin on 06/11/15.
 */
public class CallSync extends Service implements Handler.Callback, Loader.OnLoadCompleteListener<Cursor> {


    private Handler handler;
    private static final String TAG = CallSync.class.getSimpleName();
    private CursorLoader mCursorLoader;

    public static boolean isStarted;

    @Override
    public void onCreate() {
        super.onCreate();

        if (!(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M)) {
            if (AppController.getInstance().checkSelfPermission(Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED) {

            } else {
                stopSelf();
                return;
            }
        }
        handler = new Handler(this);
        isStarted = true;
        Log.d(TAG, "ServiceStarted");
        mCursorLoader = new CursorLoader(this, CallLog.Calls.CONTENT_URI,
                null, null, null,
                CallLog.Calls.DATE + " DESC");
        mCursorLoader.registerListener(1, this);
        mCursorLoader.startLoading();
    }

    public static void startService(Context ctx) {
        if (!isStarted) {
            if (!(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M)) {
                if (AppController.getInstance().checkSelfPermission(Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED) {
                    ctx.startService(new Intent(ctx, CallSync.class));
                }
            } else {
                ctx.startService(new Intent(ctx, CallSync.class));
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public boolean handleMessage(Message message) {
        return false;
    }

    @Override
    public void onLoadComplete(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG, "OnLoad Complete");
        if (data != null && data.getCount() > 0) {
            long time = PBPreferences.getData(PBPreferences.CALL_READ_TIME, getDefaultTime());
            readCallLogsInBackground(time, data);
        }
    }

    private long getDefaultTime() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DAY_OF_YEAR, -3);
        return c.getTimeInMillis();
    }

    private void readCallLogsInBackground(final long prevTime, final Cursor managedCursor) {
        try {
            if (managedCursor.isClosed()) return;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        boolean isClosed = false;
                        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
                        while (managedCursor.moveToNext()) {
                            Log.d(TAG, "Recording Call");
                            long callTime = Long.parseLong(managedCursor.getString(date));
                            if (callTime < prevTime) {
                                break;
                            }
                            CallData callDetail = getCallLog(managedCursor);
                            if (callDetail == null) {
                                // Cursor is already closed
                                isClosed = true;
                                break;
                            }
                            callDetail.setPlabro_call(false);
                            callDetail.save();
                        }
                        if (isClosed) return;
                        PBPreferences.saveData(PBPreferences.CALL_READ_TIME, System.currentTimeMillis());
                        managedCursor.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            shareCallLogs();
                        }
                    });
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void shareCallLogs() {
        PlabroIntentService.sendCallLogsToServer(CallSync.this);
    }


    public synchronized static CallData getCallLog(final Cursor managedCursor) throws Exception {
        if (managedCursor.isClosed()) return null;
        CallData callDetail = new CallData();
        String phNumber = managedCursor.getString(managedCursor.getColumnIndex(CallLog.Calls.NUMBER));
        String callDuration = managedCursor.getString(managedCursor.getColumnIndex(CallLog.Calls.DURATION));
        String callType = managedCursor.getString(managedCursor.getColumnIndex(CallLog.Calls.TYPE));
        long callTime = Long.parseLong(managedCursor.getString(managedCursor.getColumnIndex(CallLog.Calls.DATE)));
        callDetail.setDuration(callDuration);
        switch (Integer.parseInt(callType)) {
            case CallData.OUTGOING_CALL:
                callDetail.setCall_type("outgoing");
                break;
            case CallData.INCOMING_CALL:
                callDetail.setCall_type("incoming");
                break;
            case CallData.MISSED_CALL:
                callDetail.setCall_type("missed");
                break;
        }
        callDetail.setCallee(phNumber);
        callDetail.setCallTime(callTime);
        return callDetail;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isStarted = false;
        if (mCursorLoader != null) {
            mCursorLoader.unregisterListener(this);
            mCursorLoader.cancelLoad();
            mCursorLoader.stopLoading();
        }
    }
}
