package com.plabro.realestate.listeners;

/**
 * Created by nitin on 02/08/15.
 */
public interface SessionExpiredDialogListener {

    public void onOkClicked(boolean addMore);
    public void onCancelClicked(boolean addMore);
}
