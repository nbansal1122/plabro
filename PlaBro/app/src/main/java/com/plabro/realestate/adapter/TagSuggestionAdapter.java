package com.plabro.realestate.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.models.location_auto.LocationsAuto;

import java.util.ArrayList;
import java.util.List;



public class TagSuggestionAdapter extends ArrayAdapter<LocationsAuto> implements Filterable{
    private LayoutInflater layoutInflater;
    List<LocationsAuto> mLocations;

    private Filter mFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            return ((LocationsAuto)resultValue).getTitle();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null) {
                ArrayList<LocationsAuto> suggestions = new ArrayList<LocationsAuto>();
                for (LocationsAuto customer : mLocations) {
                    // Note: change the "contains" to "startsWith" if you only want starting matches
                    if (customer.getTitle().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(customer);
                    }
                }

                results.values = suggestions;
                results.count = suggestions.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            if (results != null && results.count > 0) {
                // we have filtered results
                addAll((ArrayList<LocationsAuto>) results.values);
            } else {
                // no filter, add entire original list back in
                addAll(mLocations);
            }
            notifyDataSetChanged();
        }
    };

    public TagSuggestionAdapter(Context context, int textViewResourceId, List<LocationsAuto> locations) {
        super(context, textViewResourceId, locations);
        // copy all the customers into a master list
        mLocations = new ArrayList<LocationsAuto>(locations.size());
        mLocations.addAll(locations);
        layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.location_layout, null);
        }

        LocationsAuto locationsAuto = getItem(position);

        TextView name = (TextView) view.findViewById(R.id.locationItem);
        name.setText(locationsAuto.getTitle());


        return view;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }


}