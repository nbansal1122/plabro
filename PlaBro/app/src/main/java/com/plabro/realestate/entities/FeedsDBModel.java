package com.plabro.realestate.entities;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.utils.Serializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xebia on 03-Apr-15.
 */
@Table(name = "FeedsDB")
public class FeedsDBModel extends Model {

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public byte[] getFeedObject() {
        return feedObject;
    }

    public void setFeedObject(byte[] feedObject) {
        this.feedObject = feedObject;
    }

    @Column(name = BaseColumns._ID, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String _id;
    @Column
    private byte[] feedObject;

    public static void insertFeed(Feeds data) {
        FeedsDBModel model = new FeedsDBModel();
        model._id = data.get_id();
        try {
            model.feedObject = Serializer.serialize(data);

        } catch (Exception e) {
            Log.i("FeedsDBModel", "Exception while serializing data");
        }
        model.save();

    }

    public static List<Feeds> getFeeds(List<FeedsDBModel> list) {
        List<Feeds> feeds = new ArrayList<Feeds>();
        for (FeedsDBModel model : list) {
            try {
                Feeds feed = (Feeds) Serializer.deserialize(model.getFeedObject());
                feeds.add(feed);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return feeds;
    }

    public static List<Feeds> getMyShouts(List<FeedsDBModel> list, String authorId) {
        List<Feeds> feeds = new ArrayList<Feeds>();

        Log.i("AuthorID", authorId);
        for (FeedsDBModel model : list) {
            try {
                Feeds feed = (Feeds) Serializer.deserialize(model.getFeedObject());
                Log.i("FeedAuthorID", feed.getAuthorid() + "");
                if (authorId.equals(feed.getAuthorid() + "") )
                    feeds.add(feed);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return feeds;
    }
    public static List<Feeds> getBookmarkedShouts(List<FeedsDBModel> list) {
        List<Feeds> feeds = new ArrayList<Feeds>();

        for (FeedsDBModel model : list) {
            try {
                Feeds feed = (Feeds) Serializer.deserialize(model.getFeedObject());
                if (feed.getBookmarked() )
                    feeds.add(feed);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return feeds;
    }


}
