package com.plabro.realestate.fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.activeandroid.content.ContentProvider;
import com.activeandroid.query.Select;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.HomeActivity;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.activity.Shout;
import com.plabro.realestate.adapter.ChatsCustomAdapter;
import com.plabro.realestate.listeners.HidingScrollListener;
import com.plabro.realestate.listeners.OnChildListener;
import com.plabro.realestate.models.chat.ChatMessage;
import com.plabro.realestate.models.contacts.PlabroFriends;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.xmpp.RosterManager;

import org.jivesoftware.smack.packet.Presence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ChatsFragment extends MasterFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    ChatsCustomAdapter mAdapter;
    private HashMap<String, String> contactsMap;
    RecyclerView mRecyclerView;
    ProgressWheel mProgressWheelLoadMore;
    LinearLayoutManager mLayoutManager;
    RelativeLayout noFeeds;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private Context mContext;
    private Toolbar mToolbar;
    private SearchBoxCustom search;

    private int page = 0;
    private boolean isLoading = false;

    public static final String TAG = "ChatFragment";

    public static ChatsFragment newInstance() {

        ChatsFragment mFeedsFragment = new ChatsFragment();
        return mFeedsFragment;
    }

    public static ChatsFragment getInstance(Context mContext, Toolbar mToolbar, SearchBoxCustom searchbox) {
        Log.i(TAG, "Creating New Instance");
        ChatsFragment mChatsFragment = new ChatsFragment();
        mChatsFragment.mContext = mContext;
        mChatsFragment.mToolbar = mToolbar;
        mChatsFragment.search = searchbox;

        return mChatsFragment;
    }


    @Override
    protected int getViewId() {
        return R.layout.chats_view_frag;
    }

    public void setRecyclerViewLayoutManager() {
        int scrollPosition = 0;

        mLayoutManager = new LinearLayoutManager(mContext);

        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }


    public void getData() {


        List<ChatMessage> chats = new Select(new String[]{"ChatRecord.Id,ChatRecord.phone,ChatRecord.userType,ChatRecord.msg,ChatRecord.time,ChatRecord.status,UserFriends.name,UserFriends.authorid, UserFriends.img"}).from(ChatMessage.class).leftJoin(PlabroFriends.class).on("ChatRecord.phone=UserFriends.phone").groupBy("ChatRecord.phone").orderBy("ChatRecord.time DESC").execute();
        //List<ChatMessage> chats  = new Select().from(ChatMessage.class).execute();

        List<ChatMessage> chatsUpdated = new ArrayList<ChatMessage>();

        for (ChatMessage cm : chats) {
            Log.d(TAG, "Name in records:" + cm.getName());
            String displayName = ActivityUtils.getDisplayNameFromPhone(cm.getPhone());
            if (displayName != null && !TextUtils.isEmpty(displayName)) {
                cm.setName(displayName);
            } else {
                // Get Name if it exists in profile class
                ProfileClass profile = new Select().from(ProfileClass.class).where("phone ='" + cm.getPhone() + "'").executeSingle();
                if (null != profile && null != profile.getName() && !TextUtils.isEmpty(profile.getName())) {
                    displayName = profile.getName();
                } else {
                    if (null == cm.getName() || "".equalsIgnoreCase(cm.getName())) {
                        displayName = cm.getPhone();
                    }
                }

            }
            if (null != displayName && !TextUtils.isEmpty(displayName) && !displayName.equalsIgnoreCase(cm.getName())) {
                cm.setName(displayName);
                //cm.save();
            }
            chatsUpdated.add(cm);


        }

        if (chatsUpdated.size() > 0) {
            noFeeds.setVisibility(View.GONE);
        } else {
            noFeeds.setVisibility(View.VISIBLE);
        }
        mAdapter = new ChatsCustomAdapter(mContext, chatsUpdated, deleteListener);
        mRecyclerView.setAdapter(mAdapter);
        stopSpinners();
        setupChatNotification();

    }

    View.OnClickListener MyOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.rl_bg_shout:
                case R.id.shout_fab:


                    Intent intent = new Intent(mContext, Shout.class);
                    startActivity(intent);


                    break;

            }

        }
    };


    @Override
    public String getTAG() {
        return "ChatsFragment";
    }

    @Override
    protected void findViewById() {
        Log.d(TAG, "OnCreateView");
        setTitle("Chats");
        page = 0;
        isLoading = false;

        rootView.setTag(TAG);
        contactsMap = PBPreferences.loadMap("contactsMap");

        //mFeedsFragmentView.findViewById(R.id.rl_bg_shout).setOnClickListener(MyOnClickListener);

        mRecyclerView = (RecyclerView) findView(R.id.recyclerView);
        setRecyclerViewLayoutManager();

        mProgressWheelLoadMore = (ProgressWheel) findView(R.id.progress_wheel_load_more);
        mProgressWheelLoadMore.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheelLoadMore.setProgress(0.0f);

        noFeeds = (RelativeLayout) findView(R.id.noFeeds);


        mRecyclerView.setOnScrollListener(new HidingScrollListener(20) {
            @Override
            public void onHide(RecyclerView recyclerView, int dx, int dy) {
            }

            @Override
            public void onShow(RecyclerView recyclerView, int dx, int dy) {

            }

            @Override
            public void onScrolledFire(RecyclerView recyclerView, int dx, int dy) {
//                //WizRocket tracking
//                WizRocket.getInstance(mContext).recordEvent(mContext.getResources().getString(R.string.e_scroll));
//                //GA Event Tracker
//                Analytics.trackEvent(mContext, mContext.getResources().getString(R.string.consumption), mContext.getResources().getString(R.string.e_scroll), mContext.getResources().getString(R.string.s_chats_list));


                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading) {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount - Constants.SCROLL_ITEM_COUNT) {
//                        getData(true);
                    }
                }


            }
        });


        //pull to refresh
        mSwipeRefreshLayout = (SwipeRefreshLayout) findView(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        page = 0;
                        mSwipeRefreshLayout.setRefreshing(true);
                        mProgressWheelLoadMore.stopSpinning();
                        getData();

                    }
                }, Constants.GOOGLE_PULL_TO_REFRESH_TIMER);

            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        getActivity().getSupportLoaderManager().initLoader(Constants.LoaderId.CHAT_LIST, null, this);
        getData();

    }

    private void sendPresence() {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflateToolbar(menu);
    }

    private void inflateToolbar(Menu menu) {
        if(mToolbar == null){
            mToolbar = (Toolbar) getActivity().findViewById(R.id.my_awesome_toolbar);
        }
        mToolbar.inflateMenu(R.menu.menu_chat_list);
        Log.v(TAG, "toolbar inflated");

        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.action_search:
                        openSearch();
                        break;

                    default:
                        break;

                }
                return true;
            }

        });
    }

    public void openSearch() {
        search.clearSearchable(); // clear all the searchable history previously present

        search.revealFromMenuItem(R.id.action_search, getActivity());
        search.setMenuListener(new SearchBoxCustom.MenuListener() {

            @Override
            public void onMenuClick() {
                if (search.isShown()) {
                    search.hideCircularly(getActivity());
                }

            }

        });
        search.setSearchListener(new SearchBoxCustom.SearchListener() {


            @Override
            public void onSearchOpened() {
                // Use this to tint the screen

            }

            @Override
            public void onSearchClosed() {
                // Use this to un-tint the screen
                if (mAdapter != null) {
                    mAdapter.filter("");
                }
            }

            @Override
            public void onSearchTermChanged(String searchTerm) {
                // React to the search term changing
                // Called after it has updated results
                if (mAdapter != null) {
                    mAdapter.filter(searchTerm);
                }
            }


            @Override
            public void onSearch(String searchTerm) {

                if (mAdapter != null) {
                    mAdapter.filter(searchTerm);
                }
            }

            @Override
            public void onSearchCleared() {

            }

        });

    }


    public void UpdateChatsFromOutside() {
        //getData();
    }

    OnChildListener deleteListener = new OnChildListener() {
        @Override
        public void onResponse(Boolean response) {

            if (response) {

                //getData();
            }

        }
    };

    @Override
    public void onResume() {
        super.onResume();
        if (null != search && search.isShown()) {
            search.hideCircularly(getActivity());
        }
        RosterManager.sendPresence(Presence.Type.available);
    }

    private void setupChatNotification() {

        final String unreadMSG = new Select().from(ChatMessage.class).where("status='" + ChatMessage.STATUS_RECEIVED + "' and msgType = " + ChatMessage.MESSAGE_INCOMING).execute().size() + "";
        if (null != unreadMSG && !TextUtils.isEmpty(unreadMSG) && !unreadMSG.equalsIgnoreCase("0")) {
            HomeActivity.setNoOfNotification(Constants.SectionIndex.CHATS, true, unreadMSG);
        } else {
            HomeActivity.setNoOfNotification(Constants.SectionIndex.CHATS, false, unreadMSG);
        }

    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), ContentProvider.createUri(ChatMessage.class, null), null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        Log.d(TAG, "OnLoadFinished");
        getData();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    protected void stopSpinners() {

        try {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 2000ms
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }, 2000);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerLocalReceiver();
        RosterManager.sendPresence(Presence.Type.available);
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Profile Action Received");
        if (Constants.ACTION_PROFILE_UPDATE.equals(intent.getAction())) {
            ((PlabroBaseActivity) getActivity()).setTitle("Chats");
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStart() {
        super.onStart();
        Analytics.trackScreen(Analytics.Chat.ConversationView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RosterManager.sendPresence(Presence.Type.unavailable);
    }
}
