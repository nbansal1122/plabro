package com.plabro.realestate;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.HashMap;


public class Rating extends ActionBarActivity implements VolleyListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        initiateRatingScreen();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_rating, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initiateRatingScreen() {
        final RelativeLayout rateLayout = (RelativeLayout) findViewById(R.id.rl_rate_layout);
        ProfileClass pc = AppController.getInstance().getUserProfile();
        Log.d("Rating",pc.getRating_flag()+"");

        if (null != pc) {
            switch (pc.getRating_flag()) {
                case 0:
                    finish();
                    break;
                case 1:
                    setupRatingElements(rateLayout);
                    break;
                default:
                    finish();
                    break;
            }
        }
    }

    private void setupRatingElements(final RelativeLayout rateLayout) {
        FontText later = (FontText) findViewById(R.id.tv_later);
        FontText rateNow = (FontText) findViewById(R.id.tv_rate);

        later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRatingStatus(Constants.RATING_STATE_TYPE.LATER);
                finish();
            }
        });

        rateNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRatingStatus(Constants.RATING_STATE_TYPE.RATED);
                finish();
                redirectToPlayStore();
            }
        });

    }

    private void redirectToPlayStore() {

        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }

    }

    private void sendRatingStatus(String state) {
        long currentTime = System.currentTimeMillis();
        PBPreferences.setRatingTimer(currentTime);
        ParamObject obj = new ParamObject();
        obj.setClassType(null);
        obj.setTaskCode(Constants.TASK_CODES.RATING_STATE);
        HashMap<String, String> params = new HashMap<>();
        params.put(PlaBroApi.PLABRO_RATING_STATE_POST_PARAM_RATING_STATE, state);
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.RATING_STATE, params));
        AppVolley.processRequest(obj, this);
    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        switch (taskCode) {
            case Constants.TASK_CODES.RATING_STATE:
//                FeedResponse resp = (FeedResponse) response.getResponse();
//                if (null != resp.getOutput_params() && null != resp.getOutput_params().getData()) {
//                    List<BaseFeed> feeds = resp.getOutput_params().getData();
//                    if (feeds.size() > 0) {
//                        Feeds feedsObject = (Feeds) feeds.get(0);
//                        finish();
//                        FeedDetails.startFeedDetails(Shout.this, feedsObject.get_id(), "", feedsObject);
//                    }
//                }
                break;
        }
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        switch (taskCode) {
            case Constants.TASK_CODES.RATING_STATE:
                break;
        }
    }
}
