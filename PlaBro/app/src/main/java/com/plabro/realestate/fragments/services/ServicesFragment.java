package com.plabro.realestate.fragments.services;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.BaseServiceActivity;
import com.plabro.realestate.activity.FragmentContainer;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.activity.WebViewActivity;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.fragments.HomeFragment;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.fragments.RecyclerViewFragment;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.services.Question;
import com.plabro.realestate.models.services.S_data;
import com.plabro.realestate.models.services.ServiceResponse;
import com.plabro.realestate.models.services.Services;
import com.plabro.realestate.models.services.Services_list;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.DeepLinkNavigationUtil;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 25/11/15.
 */
public class ServicesFragment extends PlabroBaseFragment implements VolleyListener, CustomListAdapter.CustomListAdapterInterface, AdapterView.OnItemClickListener, HomeFragment.CityRefreshListener {
    private GridView grid;
    private CustomListAdapter<Services_list> adapter;
    private List<Services_list> services;
    private List<Services> servicesList;
    ProgressWheel mProgressWheel;


    @Override
    public String getTAG() {
        return "Services";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerLocalReceiver();
    }

    @Override
    protected void findViewById() {
        grid = (GridView) findView(R.id.gridView);
        initProgressWheel();
        fetchServiceList();
        setTitle(getString(R.string.services));
    }

    private void fetchServiceList() {
        mProgressWheel.spin();
        ParamObject obj = new ParamObject();
        obj.setClassType(ServiceResponse.class);
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.JSON_SHARE, null));
        AppVolley.processRequest(obj, this);
    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        mProgressWheel.stopSpinning();
        try {
            ServiceResponse serviceResponse = (ServiceResponse) response.getResponse();
            List<Services_list> servicesLists = serviceResponse.getOutput_params().getData().getServices().getServices_list();
            setFragmentList(servicesLists);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setFragmentList(List<Services_list> services) {
        if (services != null && services.size() > 0) {
            this.services = services;
            adapter = new CustomListAdapter<>(getActivity(), R.layout.row_grid_service, services, this);
            grid.setAdapter(adapter);
            grid.setOnItemClickListener(this);
        } else {
            Log.d(TAG, "Services null or empty");
        }

    }


    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        if (isRunning) {
            mProgressWheel.stopSpinning();
        }
        showToast(response.getCustomException().getMessage());
        if (null == services || services.size() < 1) {
            showViews(R.id.noFeeds);
        }
    }

    public void initProgressWheel() {
        mProgressWheel = (ProgressWheel) findView(R.id.progress_wheel);
        mProgressWheel.setBarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
    }

    @Override
    protected int getViewId() {
        return R.layout.layout_all_services;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getActivity()).inflate(resourceID, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ActivityUtils.loadImage(getActivity(), services.get(position).getS_data().getImg(), holder.imageView, R.drawable.img_blank_services);
//        holder.imageView.setImageResource(getImageResource(services.get(position).getService_code()));
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Services_list service = services.get(i);
        onServiceClicked(service);
        sendMetaData(service);
//        BaseServiceActivity.startActivity(getActivity(), service);
    }

    private void sendMetaData(Services_list service) {
        if (service.getS_data() != null && service.getS_data().getService_type() != null) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("ServiceType", service.getS_data().getService_type());
            Analytics.trackServiceEvent(Analytics.CardActions.CardClicked, map);
        }
    }

    private int getImageResource(String serviceCode) {
        switch (serviceCode) {
            case Constants.ServiceTypes.SMS_SERVICE:
                return R.drawable.img_sms_services;
            case Constants.ServiceTypes.LOAN_SERVICE:
                return R.drawable.img_services_loan;
            case Constants.ServiceTypes.DIRECT_INVENTORY:
                return R.drawable.img_services_direct;
            case Constants.ServiceTypes.MATCH_MAKING:
                return R.drawable.img_blank_services;
            case Constants.ServiceTypes.BIDDING_SERVICE:
                return R.drawable.img_services_bidding;
            default:
                return R.drawable.img_blank_services;
        }
    }

    private void onServiceClicked(Services_list service) {
        Bundle b = new Bundle();
        b.putSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE, service);
        switch (service.getService_code()) {
            case Constants.ServiceTypes.SMS_SERVICE:
                Analytics.trackScreen(Analytics.SERVICES.SMSService);
                BaseServiceActivity.startActivity(getActivity(), service);
//                FragmentContainer.startActivity(getActivity(), Constants.FRAGMENT_TYPE.SMS_SERVICE, b);
                break;
            case Constants.ServiceTypes.LOAN_SERVICE:
                Analytics.trackScreen(Analytics.SERVICES.LoanService);
                BaseServiceActivity.startActivity(getActivity(), service);
//                FragmentContainer.startActivity(getActivity(), Constants.FRAGMENT_TYPE.LOAN_SERVICE, b);
                break;
            case Constants.ServiceTypes.DIRECT_INVENTORY:
                FragmentContainer.startActivity(getActivity(), Constants.FRAGMENT_TYPE.DIRECT_INVENTORY, new Bundle());
                break;
            case Constants.ServiceTypes.MATCH_MAKING:
                FragmentContainer.startActivity(getActivity(), Constants.FRAGMENT_TYPE.MATCH_MAKING, b);
                break;
            case Constants.ServiceTypes.BIDDING_SERVICE:
                FragmentContainer.startActivity(getActivity(), Constants.FRAGMENT_TYPE.AUCTION, new Bundle());
//                WebViewActivity.startActivity(getActivity(), "MY BIDS", PlaBroApi.getBidsUrl(), PlaBroApi.getBidsExitUrl());
                break;
            default:
                b = DeepLinkNavigationUtil.getMap(service.getS_data().getDeep_link());
                DeepLinkNavigationUtil.navigate(b, getActivity());
                break;
        }
    }

    @Override
    public void onCityRefresh() {

    }


    private class ViewHolder {
        private ImageView imageView;
        private TextView textView;

        ViewHolder(View v) {
            imageView = (ImageView) v.findViewById(R.id.iv_service);
        }
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Profile Action Received" + intent.getAction());
        if (Constants.ACTION_PROFILE_UPDATE.equals(intent.getAction())) {
            ((PlabroBaseActivity) getActivity()).setTitle("Services");

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegsiterLocalReceiver();
    }
}
