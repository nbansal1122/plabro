package com.plabro.realestate.models.propFeeds;

import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.utilities.JSONUtils;

/**
 * Created by nitin on 25/03/16.
 */
@Table(name = "ExternalSource")
public class ExternalSourceFeed extends Feeds {
    @SerializedName("lister_url")
    @Expose
    private String redirectUrl;
    @SerializedName("redirect_button_name")
    @Expose
    private String redirect_button_name;

    public String getRedirect_button_name() {
        return redirect_button_name;
    }

    public void setRedirect_button_name(String redirect_button_name) {
        this.redirect_button_name = redirect_button_name;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public static ExternalSourceFeed parseJson(String json) {
        return (ExternalSourceFeed) JSONUtils.parseJson(json, ExternalSourceFeed.class);
    }
}
