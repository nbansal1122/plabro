package com.plabro.realestate.models.followers;

import com.plabro.realestate.models.profile.ProfileClass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class FollowersOutputParams {

    List<ProfileClass> data = new ArrayList<ProfileClass>();

    public List<ProfileClass> getData() {
        return data;
    }

    public void setData(List<ProfileClass> data) {
        this.data = data;
    }
}
