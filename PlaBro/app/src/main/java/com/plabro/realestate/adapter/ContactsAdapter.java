package com.plabro.realestate.adapter;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.models.Contact;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utils.CircularContactView;
import com.plabro.realestate.utils.ContactImageUtil;
import com.plabro.realestate.utils.ImageCache;
import com.plabro.realestate.utils.async_task_thread_pool.AsyncTaskEx;
import com.plabro.realestate.utils.async_task_thread_pool.AsyncTaskThreadPool;
import com.plabro.realestate.widgets.stickyList.SearchablePinnedHeaderListViewAdapter;
import com.plabro.realestate.widgets.stickyList.StringArrayAlphabetIndexer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by hemant on 6/2/15.
 */

public class ContactsAdapter extends SearchablePinnedHeaderListViewAdapter<Contact> {
    private ArrayList<Contact> mContacts;
    private final int CONTACT_PHOTO_IMAGE_SIZE;
    private final int[] PHOTO_TEXT_BACKGROUND_COLORS;
    private final AsyncTaskThreadPool mAsyncTaskThreadPool = new AsyncTaskThreadPool(1, 2, 10);
    private Activity mActivity;
    private LayoutInflater mInflater;


    @Override
    public CharSequence getSectionTitle(int sectionIndex) {
        return ((StringArrayAlphabetIndexer.AlphaBetSection) getSections()[sectionIndex]).getName();
    }

    public ContactsAdapter(final ArrayList<Contact> contacts, Activity mActivity, LayoutInflater mInflater) {
        this.mActivity = mActivity;
        this.mInflater = mInflater;

        setData(contacts);
        PHOTO_TEXT_BACKGROUND_COLORS = mActivity.getResources().getIntArray(R.array.contacts_text_background_colors);
        CONTACT_PHOTO_IMAGE_SIZE = mActivity.getResources().getDimensionPixelSize(
                R.dimen.list_item__contact_imageview_size);
    }

    public void setData(final ArrayList<Contact> contacts) {
        this.mContacts = contacts;
        final String[] generatedContactNames = generateContactNames(contacts);
        setSectionIndexer(new StringArrayAlphabetIndexer(generatedContactNames, true));
    }

    private String[] generateContactNames(final List<Contact> contacts) {
        final ArrayList<String> contactNames = new ArrayList<String>();
        if (contacts != null)
            for (final Contact contactEntity : contacts)
                contactNames.add(contactEntity.getDisplayName());
        return contactNames.toArray(new String[contactNames.size()]);
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        final View rootView;
        if (convertView == null) {
            holder = new ViewHolder();
            rootView = mInflater.inflate(R.layout.listview_item, parent, false);
            holder.friendProfileCircularContactView = (CircularContactView) rootView
                    .findViewById(R.id.listview_item__friendPhotoImageView);
            holder.friendProfileCircularContactView.getTextView().setTextColor(0xFFffffff);
            holder.friendName = (TextView) rootView
                    .findViewById(R.id.listview_item__friendNameTextView);
            holder.headerView = (TextView) rootView.findViewById(R.id.header_text);
            holder.contact = (RelativeLayout) rootView.findViewById(R.id.contact);


            rootView.setTag(holder);
        } else {
            rootView = convertView;
            holder = (ViewHolder) rootView.getTag();
        }
        final Contact contact = getItem(position);

        holder.contact.setTag(contact);

        final String displayName = contact.getDisplayName();
        holder.friendName.setText(displayName);
        boolean hasPhoto = !TextUtils.isEmpty(contact.getPhotoId());
        if (holder.updateTask != null && !holder.updateTask.isCancelled())
            holder.updateTask.cancel(true);
        final Bitmap cachedBitmap = hasPhoto ? ImageCache.INSTANCE.getBitmapFromMemCache(contact.getPhotoId()) : null;
        if (cachedBitmap != null)
            holder.friendProfileCircularContactView.setImageBitmap(cachedBitmap);
        else {
            final int backgroundColorToUse = PHOTO_TEXT_BACKGROUND_COLORS[position
                    % PHOTO_TEXT_BACKGROUND_COLORS.length];
            if (TextUtils.isEmpty(displayName))
                holder.friendProfileCircularContactView.setImageResource(R.drawable.ic_person_white_120dp,
                        backgroundColorToUse);
            else {
                final String characterToShow = TextUtils.isEmpty(displayName) ? "" : displayName.substring(0, 1).toUpperCase(Locale.getDefault());
                holder.friendProfileCircularContactView.setTextAndBackgroundColor(characterToShow, backgroundColorToUse);
            }
            if (hasPhoto) {
                holder.updateTask = new AsyncTaskEx<Void, Void, Bitmap>() {

                    @Override
                    public Bitmap doInBackground(final Void... params) {
                        if (isCancelled())
                            return null;
                        final Bitmap b = ContactImageUtil.loadContactPhotoThumbnail(mActivity.getApplicationContext(), contact.getPhotoId(), CONTACT_PHOTO_IMAGE_SIZE);
                        if (b != null)
                            return ThumbnailUtils.extractThumbnail(b, CONTACT_PHOTO_IMAGE_SIZE,
                                    CONTACT_PHOTO_IMAGE_SIZE);
                        return null;
                    }

                    @Override
                    public void onPostExecute(final Bitmap result) {
                        super.onPostExecute(result);
                        if (result == null)
                            return;
                        ImageCache.INSTANCE.addBitmapToCache(contact.getPhotoId(), result);
                        holder.friendProfileCircularContactView.setImageBitmap(result);
                    }
                };
                mAsyncTaskThreadPool.executeAsyncTask(holder.updateTask);
            }

        }
        bindSectionHeader(holder.headerView, null, position);

        holder.contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String contactId = ((Contact) v.getTag()).getContactId();
                String displayName1 = ((Contact) v.getTag()).getDisplayName();
                int userId = ((Contact) v.getTag()).getUserId();

                List<String> userPhones = new ArrayList<String>();

                if (contactId != null) {

                    //
                    //  Get all phone numbers.
                    //
                    ContentResolver cr = mActivity.getContentResolver();
                    Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                    while (phones.moveToNext()) {
                        String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                        int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        userPhones.add(number);
                        switch (type) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                // do something with the Home number here...
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                // do something with the Mobile number here...
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                // do something with the Work number here...
                                break;
                        }
                    }
                    phones.close();
                } else {

                    userPhones.add(displayName1);

                }

                HashMap<String, Object> data = new HashMap<>();
                data.put("ScreenName", "ContactsAdapter");
                data.put("Action", "Chat");
                Analytics.trackInteractionEvent(Analytics.CardActions.Chat, data);
                Intent intent = new Intent(mActivity, XMPPChat.class);
                String num = userPhones.get(0);
                String displayName = ActivityUtils.getDisplayNameFromPhone(num);
                if (displayName != null && !TextUtils.isEmpty(displayName)) {
                    intent.putExtra("name", displayName);

                } else {
                    intent.putExtra("name", num);

                }
                intent.putExtra("phone_key", num);
                intent.putExtra("userid", userId + "");

                mActivity.startActivity(intent);
//                    startActivityForResult(intent, Constants.REVIEW_REQUEST_CODE);

//                Toast.makeText(getActivity(), "hiee", Toast.LENGTH_LONG).show();
            }
        });


        return rootView;
    }


    public boolean doFilter(final Contact item, final CharSequence constraint) {
        if (TextUtils.isEmpty(constraint))
            return true;
        final String displayName = item.getDisplayName();
        return !TextUtils.isEmpty(displayName) && displayName.toLowerCase(Locale.getDefault())
                .contains(constraint.toString().toLowerCase(Locale.getDefault()));
    }


    public ArrayList<Contact> getOriginalList() {
        return mContacts;
    }

    // /////////////////////////////////////////////////////////////////////////////////////
    // ViewHolder //
    // /////////////
    public static class ViewHolder {
        public CircularContactView friendProfileCircularContactView;
        TextView friendName, headerView;
        public AsyncTaskEx<Void, Void, Bitmap> updateTask;
        RelativeLayout contact;
    }

}

