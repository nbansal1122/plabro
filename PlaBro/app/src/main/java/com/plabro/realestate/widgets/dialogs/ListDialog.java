package com.plabro.realestate.widgets.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.listeners.LocationSelectionListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hemant on 23-03-2015.
 */
public class ListDialog extends DialogFragment implements CustomListAdapter.CustomListAdapterInterface, AdapterView.OnItemClickListener {

    ListView mListView;
    ItemViewHolder holder;
    private String title;

    private String mSelectedItem;
    Context mContext;
    private ArrayList<String> itemList = new ArrayList<String>();
    private ArrayList<String> tempList = new ArrayList<String>();

    private LocationSelectionListener locationSelectionListener;
    private boolean showSearch = false;
    private ImageView searchButton;
    private EditText mSearch;
    private boolean isSearchVisible = false;
    private CustomListAdapter<String> adapter;
    private boolean mCountIncreased;

    @SuppressLint("ValidFragment")
    public ListDialog(ArrayList<String> itemList, Context context, String mSelectedItem, String title, LocationSelectionListener locationSelectionListener, boolean showSearch) {
        this.mContext = context;
        this.itemList.addAll(itemList);
        this.tempList.addAll(itemList);
        this.mSelectedItem = mSelectedItem;
        this.title = title;
        this.locationSelectionListener = locationSelectionListener;
        this.showSearch = showSearch;

    }

    public ListDialog() {
    }

    DialogInterface.OnCancelListener onCancelListener = null;

    public void setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
        this.onCancelListener = onCancelListener;
    }


    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if (onCancelListener != null)
            onCancelListener.onCancel(getDialog());
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.list_dialog, null);
        builder.setView(convertView);
        mSearch = (EditText) convertView.findViewById(R.id.et_search);
        searchButton = (ImageView) convertView.findViewById(R.id.iv_search);

        if (showSearch) {
            searchButton.setVisibility(View.VISIBLE);
        } else {
            searchButton.setVisibility(View.GONE);
        }

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearch.setText("");
                if (!isSearchVisible) {
                    mSearch.setVisibility(View.VISIBLE);
                    searchButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_cross_big));
                    isSearchVisible = true;
                } else {
                    mSearch.setVisibility(View.GONE);
                    searchButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_search));
                    isSearchVisible = false;
                }
            }
        });

        mSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mCountIncreased = after <= count;

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("Filter : search char", s.toString());
                if (s.toString().length() == 0) {
                    tempList.clear();
                    tempList.addAll(itemList);
                    adapter.notifyDataSetChanged();
                    return;
                }


                tempList.clear();

                for (String item : itemList) {
                    if (item.toLowerCase().contains(s)) {
                        tempList.add(item);
                        Log.d("filter", item);
                    }
                }
                adapter.notifyDataSetInvalidated();
                adapter.notifyDataSetChanged();
            }
        });

        TextView mTitle = (TextView) convertView.findViewById(R.id.tv_title);
        mTitle.setText(title);
        if (TextUtils.isEmpty(title)) {
            mTitle.setVisibility(View.GONE);
        }
        mListView = (ListView) convertView.findViewById(R.id.lv_item_list);
        setUpPhoneList();
        return builder.create();
    }

    private void setUpPhoneList() {
        if (mContext == null) {
            mContext = getActivity();
        }
        setupAdapter();
        // ListView Item Click Listener
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                locationSelectionListener.onResponse(true, itemList.get(position));
                dismiss();

            }

        });

    }

    private void setupAdapter() {

        if (adapter != null) {
            adapter.clear();
        }
        adapter = new CustomListAdapter<String>(mContext, R.layout.item_selector_layout, tempList, this);
        // Assign adapter to ListView
        mListView.setAdapter(adapter);

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceID, null);
            holder = new ItemViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ItemViewHolder) convertView.getTag();
        }
        String item = tempList.get(position);

        holder.item.setText(item);
        holder.item.setTag(item);
        holder.itemCheck.setTag(item);

        if (!TextUtils.isEmpty(mSelectedItem) && mSelectedItem.equalsIgnoreCase(item)) {
            holder.item.setTextColor(mContext.getResources().getColor(R.color.appColor18));
            holder.itemCheck.setChecked(true);
        } else {
            holder.item.setTextColor(mContext.getResources().getColor(R.color.appColor11));
            holder.itemCheck.setChecked(false);

        }

        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String selectedItem = itemList.get(i);
        //holder.
    }

    private class ItemViewHolder {
        TextView item;
        CheckBox itemCheck;

        public ItemViewHolder(View view) {
            this.item = (TextView) view.findViewById(R.id.tv_item);
            this.itemCheck = (CheckBox) view.findViewById(R.id.rb_status);

            this.itemCheck.setOnClickListener(locationSelectClickListener);

            this.item.setOnClickListener(locationSelectClickListener);
        }
    }

    View.OnClickListener locationSelectClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.rb_status:
                    selectLocation((String) view.getTag());
                    break;
                case R.id.tv_item:
                    selectLocation((String) view.getTag());
                    break;
            }
        }
    };

    void selectLocation(String val) {
        locationSelectionListener.onResponse(true, val);
        dismiss();
    }


}
