package com.plabro.realestate.models.propFeeds;

/**
 * Created by hemantkumar on 01/06/15.
 */
public class FilterValues {

    String displayname="";
    String value="";

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    
}
