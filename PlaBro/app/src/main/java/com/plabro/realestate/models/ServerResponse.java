package com.plabro.realestate.models;

/**
 * Created by nitin on 03/08/15.
 */
public class ServerResponse {
    public boolean status;
    public String msg = "";

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
