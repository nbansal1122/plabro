package com.plabro.realestate.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.models.history.History;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemant on 1/3/15.
 */
public class SearchHistorySuggestionAdapter extends BaseAdapter implements Filterable {



    List<History> mData;
    LayoutInflater inflater;
    Context mContext;

    public SearchHistorySuggestionAdapter(Context context, List<History> data) {
        mData = data;
        mContext = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        TextView textView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.auto_suggest_layout, null);
            holder.textView = (TextView) convertView
                    .findViewById(R.id.tv_auto_suggest);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textView.setText(mData.get(position).getText());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new AutoSuggestFilter();
    }

    private class AutoSuggestFilter extends Filter {

        @Override
        public String convertResultToString(Object resultValue) {
            return (resultValue.toString());
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            ArrayList<History> data = new ArrayList<>();

            if(mData!=null) {
                data.addAll(mData);
            }
            results.values = data;
            results.count = data.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null && results.count > 0) {
                notifyDataSetChanged();
            }
        }
    }

}

//public class SearchHistorySuggestionAdapter extends ArrayAdapter<History> implements Filterable{
//    private LayoutInflater layoutInflater;
//    List<History> histories = new ArrayList<History>();
//    List<History> historiesSuggest = new ArrayList<History>();
//
//
//    private Filter mFilter = new Filter() {
//        @Override
//        public String convertResultToString(Object resultValue) {
//            return ((History)resultValue).getH_text();
//        }
//
//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            FilterResults results = new FilterResults();
//
//            if (constraint != null) {
//                ArrayList<History> suggestions = new ArrayList<History>();
//                for (History customer : histories) {
//                    // Note: change the "contains" to "startsWith" if you only want starting matches
//                    if (customer.getH_text().toLowerCase().contains(constraint.toString().toLowerCase())) {
//                        suggestions.add(customer);
//                    }
//                }
//
//                results.values = suggestions;
//                results.count = suggestions.size();
//            }
//
//            return results;
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            clear();
//            if (results != null && results.count > 0) {
//                // we have filtered results
//                addAll((ArrayList<History>) results.values);
//            } else {
//                // no filter, add entire original list back in
//                addAll(histories);
//            }
//            notifyDataSetChanged();
//        }
//    };
//
//    public SearchHistorySuggestionAdapter(Context context, int textViewResourceId, List<History> histories1) {
//        super(context, textViewResourceId, histories1);
//        // copy all the customers into a master list
//        histories = new ArrayList<History>(histories1.size());
//        histories.addAll(histories1);
//        historiesSuggest.addAll(histories1);
//
//        layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        View view = convertView;
//
//        if (view == null) {
//            view = layoutInflater.inflate(R.layout.location_layout, null);
//        }
//
//        History locationsAuto = getItem(position);
//
//        TextView name = (TextView) view.findViewById(R.id.locationItem);
//        name.setText(locationsAuto.getH_text());
//
//
//        return view;
//    }
//
//
//
//
//    // Filter Class
//    public void filter(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        histories.clear();
//        if(historiesSuggest.size()>0) {
//            if (charText.length() == 0) {
//                histories.addAll(historiesSuggest);
//            } else {
//                for (History gsd : historiesSuggest) {
//                    if (gsd.getH_text().toLowerCase(Locale.getDefault()).contains(charText)) {
//                        histories.add(gsd);
//                    }
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }
//
//}