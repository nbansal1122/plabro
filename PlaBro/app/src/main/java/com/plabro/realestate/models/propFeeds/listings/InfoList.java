package com.plabro.realestate.models.propFeeds.listings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InfoList implements Serializable{

    @SerializedName("info")
    @Expose
    private List<Info> info = new ArrayList<Info>();
    @SerializedName("header")
    @Expose
    private String header;

    /**
     * @return The info
     */
    public List<Info> getInfo() {
        return info;
    }

    /**
     * @param info The info
     */
    public void setInfo(List<Info> info) {
        this.info = info;
    }

    /**
     * @return The header
     */
    public String getHeader() {
        return header;
    }

    /**
     * @param header The header
     */
    public void setHeader(String header) {
        this.header = header;
    }

}