package com.plabro.realestate.models.profile;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Endorsement_profile implements Serializable {

    @Expose
    private Integer count;
    @Expose
    private List<Endorser_list> endorser_list = new ArrayList<Endorser_list>();

    public List<String> getKeys() {
        return keys;
    }

    public void setKeys(List<String> keys) {
        this.keys = keys;
    }

    @Expose
    private List<String> keys;
    @Expose
    private String title;

    private boolean isEmpty = false;

    public boolean isEmpty() {
        return isEmpty;
    }

    public void setIsEmpty(boolean isEmpty) {
        this.isEmpty = isEmpty;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    @Expose
    private String img_url;

    public String getWhite_img_url() {
        return white_img_url;
    }

    public void setWhite_img_url(String white_img_url) {
        this.white_img_url = white_img_url;
    }


    @Expose
    private String white_img_url;

    /**
     * @return The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * @return The endorser_list
     */
    public List<Endorser_list> getEndorser_list() {
        return endorser_list;
    }

    /**
     * @param endorser_list The endorser_list
     */
    public void setEndorser_list(List<Endorser_list> endorser_list) {
        this.endorser_list = endorser_list;
    }


    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

}