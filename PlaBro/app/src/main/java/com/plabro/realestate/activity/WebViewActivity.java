package com.plabro.realestate.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.payu.custombrowser.Bank;
import com.payu.custombrowser.PayUWebViewClient;
import com.plabro.realestate.R;
import com.plabro.realestate.fragments.PaymentDialogFragment;
import com.plabro.realestate.fragments.services.BiddingFragment;
import com.plabro.realestate.utilities.Constants;

public class WebViewActivity extends PlabroBaseActivity {

    private WebView webView;
    private String title, inputUrl, exitUrl;
    private BiddingFragment biddingFragment;
    private boolean isAppendedUrl;

    public static void startActivity(Context ctx, String title, String inputUrl, String exitUrl) {
        startActivity(ctx, title, inputUrl, exitUrl, true);
    }
    public static void startActivity(Context ctx, String title, String inputUrl, String exitUrl, boolean isAppendedUrl) {
        Bundle b = new Bundle();
        b.putString(Constants.BUNDLE_KEYS.TITLE, title);
        b.putString(Constants.BUNDLE_KEYS.INPUT_URL, inputUrl);
        b.putString(Constants.BUNDLE_KEYS.EXIT_URL, exitUrl);
        b.putBoolean(Constants.BUNDLE_KEYS.IS_APPENDED, isAppendedUrl);
        Intent i = new Intent(ctx, WebViewActivity.class);
        i.putExtras(b);
        ctx.startActivity(i);
    }

    @Override
    protected String getTag() {
        return "Web View";
    }

    @Override
    protected int getResourceId() {
        return R.layout.fragment_container;
    }

    @Override
    protected void loadBundleData(Bundle b) {
        title = b.getString(Constants.BUNDLE_KEYS.TITLE);
        inputUrl = b.getString(Constants.BUNDLE_KEYS.INPUT_URL);
        exitUrl = b.getString(Constants.BUNDLE_KEYS.EXIT_URL);
        isAppendedUrl = b.getBoolean(Constants.BUNDLE_KEYS.IS_APPENDED);
    }


    @Override
    public void findViewById() {
        inflateToolbar();
        setTitle(title);
        biddingFragment = BiddingFragment.getInstance(title, inputUrl, exitUrl, isAppendedUrl);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, biddingFragment).commitAllowingStateLoss();
    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void onBackPressed() {
        biddingFragment.onBackPressed();
    }
}
