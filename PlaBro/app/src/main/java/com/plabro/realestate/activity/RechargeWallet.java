package com.plabro.realestate.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.payu.india.Extras.PayUChecksum;
import com.payu.india.Extras.PayUSdkDetails;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PostData;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.payuui.PayUBaseActivity;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.payment.PaymentPostData;
import com.plabro.realestate.models.payment.PaymentResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class RechargeWallet extends PlabroBaseActivity implements View.OnClickListener, VolleyListener {

    private ProgressDialog dialog;
    private EditText amountEntered;
    private boolean amountChangeEventTracked = false;
    int merchantIndex = 0;
    int env = PayuConstants.MOBILE_STAGING_ENV;
    // in case of production make sure that merchantIndex is fixed as 0 (0MQaQP) for other key's payu server cant generate hash
    //int env = PayuConstants.PRODUCTION_ENV;

    String merchantTestKeys[] = {"gtKFFx", "gtKFFx"};
    String merchantTestSalts[] = {"eCwWELxi", "eCwWELxi"};

    String merchantProductionKeys[] = {"dhAO9L", "smsplus"};
    String merchantProductionSalts[] = {"AHH7JwBT", "1b1b0",};

    String offerKeys[] = {"test123@6622", "offer_test@ffer_t5172", "offerfranklin@6636"};

    String merchantKey = env == PayuConstants.PRODUCTION_ENV ? merchantProductionKeys[merchantIndex] : merchantTestKeys[merchantIndex];
    // String merchantSalt = env == PayuConstants.PRODUCTION_ENV ? merchantProductionSalts[merchantIndex] : merchantTestSalts[merchantIndex];
    String mandatoryKeys[] = {PayuConstants.KEY, PayuConstants.AMOUNT, PayuConstants.PRODUCT_INFO, PayuConstants.FIRST_NAME, PayuConstants.EMAIL, PayuConstants.TXNID, PayuConstants.SURL, PayuConstants.FURL, PayuConstants.USER_CREDENTIALS, PayuConstants.UDF1, PayuConstants.UDF2, PayuConstants.UDF3, PayuConstants.UDF4, PayuConstants.UDF5, PayuConstants.ENV};
    String mandatoryValues[] = {merchantKey, "10.0", "myproduct", "firstname", "me@itsmeonly.com", "" + System.currentTimeMillis(), "https://payu.herokuapp.com/success", "https://payu.herokuapp.com/failure", merchantKey + ":payutest@payu.in", "udf1", "udf2", "udf3", "udf4", "udf5", "" + env};

    String inputData = "";

    private Toolbar mToolbar;
    private Button addButton;
    private Button mAddMoney;
    private ScrollView mainScrollView;
    private LinearLayout rowContainerLinearLayout;

    private PayUChecksum checksum;
    private PostData postData;
    private String key;
    private String salt;
    private String var1;
    private Intent intent;
    //    private mPaymentParams mPaymentParams;
    private PaymentParams mPaymentParams;
    private PayuConfig payuConfig;
    private String cardBin;
    private List<Integer> amountViewIds = new ArrayList<>();

    @Override
    protected String getTag() {
        return null;
    }

    public static void startRechargeWallet(Activity context, String bal) {
        Intent rechargeWallet = new Intent(context, RechargeWallet.class);
        rechargeWallet.putExtra(Constants.BUNDLE_KEYS.BALANCE, bal);
        context.startActivityForResult(rechargeWallet, PayuConstants.PAYU_REQUEST_CODE);
    }

    public static void startRechargeWallet(Activity context, String bal, float amount) {
        Intent rechargeWallet = new Intent(context, RechargeWallet.class);
        rechargeWallet.putExtra(Constants.BUNDLE_KEYS.BALANCE, bal);
        rechargeWallet.putExtra(Constants.BUNDLE_KEYS.AMOUNT, amount);
        context.startActivityForResult(rechargeWallet, PayuConstants.PAYU_REQUEST_CODE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_wallet);
        findViewById();
    }

    public void inflateToolbar() {
        // lets set up the tool bar;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        // Set an OnMenuItemClickListener to handle menu item clicks
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Handle the menu item
                int id = item.getItemId();
                switch (id) {
                }
                return true;
            }

        });
        // Inflate a menu to be displayed in the mToolbar
        mToolbar.inflateMenu(R.menu.menu_recharge_wallet);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mAddMoney.setEnabled(true);
        if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_add:
                addView();
                break;
            case R.id.btn_add_money:
                //navigateToBaseActivity();

                getPaymentDataToRecharge();
                break;
            case R.id.tv_Rs1000:
            case R.id.tv_Rs200:
            case R.id.tv_Rs500:
//                setAmountViews(v.getId());
                long value = 0L;
                String text = amountEntered.getText().toString().trim();
                if (!TextUtils.isEmpty(text)) {
                    value = Long.parseLong(amountEntered.getText().toString());
                }
                value += Long.parseLong(v.getTag().toString());
                amountEntered.setText(String.valueOf(value));
                mAddMoney.setText(getString(R.string.addRs, String.valueOf(value)));
                break;
            case R.id.tv_apply:
                String promotionalCode = getEditText(R.id.et_offer);
                if (TextUtils.isEmpty(promotionalCode)) {
                    showToast(getString(R.string.empty_promotional_code));
                } else {
                    applyCode(promotionalCode);
                }
                break;
        }
    }

    private void applyCode(String promotionalCode) {
    }

    protected String getEditText(int id) {
        EditText et = (EditText) findViewById(id);
        return et.getText().toString();
    }

    private void setAmountViews(int viewId) {
        for (int id : amountViewIds) {
            TextView tv = (TextView) findViewById(id);
            tv.setTextColor(ContextCompat.getColor(this, R.color.appColor1));
            tv.setBackgroundResource(R.drawable.gray_square_filled);
        }
        TextView tv = (TextView) findViewById(viewId);
        tv.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        tv.setBackgroundResource(R.drawable.payment_tv_selected);
    }

    private void addView() {
        rowContainerLinearLayout.addView(getLayoutInflater().inflate(R.layout.row, null));
        findViewById(R.id.scroll_view_main).post(new Runnable() {
            @Override
            public void run() {
                mainScrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }


    private void navigateToBaseActivityNew(PaymentPostData paymentPostData) {
        intent = new Intent(this, PaymentmodeActivity.class);
        mPaymentParams = new PaymentParams();
        payuConfig = new PayuConfig();


        mPaymentParams.setKey(paymentPostData.getKey());
        key = paymentPostData.getKey();
        mPaymentParams.setAmount(paymentPostData.getAmount());

        mPaymentParams.setProductInfo(paymentPostData.getProductinfo());
        mPaymentParams.setFirstName(paymentPostData.getFirstname());
        mPaymentParams.setEmail(paymentPostData.getEmail());
        mPaymentParams.setTxnId(paymentPostData.getTxnid());
        mPaymentParams.setSurl(paymentPostData.getSurl());
        mPaymentParams.setFurl(paymentPostData.getFurl());
        mPaymentParams.setUdf1("");
        mPaymentParams.setUdf2("");
        mPaymentParams.setUdf3("");
        mPaymentParams.setUdf4("");
        mPaymentParams.setUdf5("");
        // in case store user card

        mPaymentParams.setUserCredentials(paymentPostData.getUser_credentials());
        var1 = paymentPostData.getUser_credentials();

        // for offer key
        mPaymentParams.setOfferKey(paymentPostData.getOffer_key());

        // other params- should be inside bundle, so that we can get them in next page.
        intent.putExtra(PayuConstants.SALT, paymentPostData.getSalt());
        salt = paymentPostData.getSalt();
        Log.d(TAG, "Hash Server:" + mPaymentParams.getHash());
//        mPaymentParams.setHash(hash);

        // stetting up the environment
        String environment = paymentPostData.getEnv() + "";
        Log.d(TAG, "Env:" + environment);
        payuConfig.setEnvironment(environment.contentEquals("" + PayuConstants.PRODUCTION_ENV) ? PayuConstants.PRODUCTION_ENV : PayuConstants.MOBILE_STAGING_ENV);

//        payuConfig.setEnvironment(PayuConstants.MOBILE_STAGING_ENV);

        // is_Domestic
        // cardBin = paymentPostData.getCardBin;


        /*
         * if you have any other payment default param please add them here. something like
         */
        mPaymentParams.setPhone(paymentPostData.getPhone());
        mPaymentParams.setPg(paymentPostData.getPg());

        mAddMoney.setEnabled(false);// lets not allow the user to click the button again and again.

        PayuHashes payuHashes = new PayuHashes();
        payuHashes.setPaymentHash(paymentPostData.getHash());
        payuHashes.setDeleteCardHash(paymentPostData.getDelete_user_card_hash());
        payuHashes.setEditCardHash(paymentPostData.getEdit_user_card_hash());
        payuHashes.setSaveCardHash(paymentPostData.getSave_user_card_hash());
        payuHashes.setStoredCardsHash(paymentPostData.getGet_user_cards_hash());
//        payuHashes.setDeleteCardHash(paymentPostData.get);
        mPaymentParams.setHash(paymentPostData.getHash());
        Log.d(TAG, "Mobile SDK Hash:" + paymentPostData.getPayment_related_details_for_mobile_sdk_hash());
        payuHashes.setPaymentRelatedDetailsForMobileSdkHash(paymentPostData.getPayment_related_details_for_mobile_sdk_hash());

        launchSdkUI(payuHashes);

    }

    /******************************
     * Server hash generation
     ********************************/
    // lets generate hashes from server
    protected String concatParams(String key, String value) {
        return key + "=" + value + "&";
    }

    @Override
    public void findViewById() {
        inflateToolbar();
        amountViewIds.add(R.id.tv_Rs1000);
        amountViewIds.add(R.id.tv_Rs200);
        amountViewIds.add(R.id.tv_Rs500);
        mAddMoney = (Button) findViewById(R.id.btn_add_money);
        amountEntered = (EditText) findViewById(R.id.et_amount);
        amountEntered.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                long amount = 0L;
                if (TextUtils.isEmpty(s) || "0".equals(s)) {
                } else {
                    amount = Long.parseLong(s.toString());
                }
                mAddMoney.setText(getString(R.string.addRs, s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!amountChangeEventTracked) {
                    amountChangeEventTracked = true;
                }

            }
        });
        String bal = getIntent().getExtras().getString(Constants.BUNDLE_KEYS.BALANCE);
        float amount = getIntent().getExtras().getFloat(Constants.BUNDLE_KEYS.AMOUNT, 0.0f);
        if (amount > 0.0f) {
            amountEntered.setText((int)amount+"");
        }
        setText(R.id.tv_balance, String.format(getString(R.string.wallet_balance), TextUtils.isEmpty(bal) ? "0.00" : bal));

        // lets initialize the views
        addButton = (Button) findViewById(R.id.button_add);

        rowContainerLinearLayout = (LinearLayout) findViewById(R.id.linear_layout_row_container);

        mainScrollView = (ScrollView) findViewById(R.id.scroll_view_main);

        // lets set the on click listener to the buttons
        addButton.setOnClickListener(this);
        mAddMoney.setOnClickListener(this);


        // filling up the ui with the values.
        for (int i = 0; i < mandatoryKeys.length; i++) {
            addView();
            LinearLayout currentLayout = (LinearLayout) rowContainerLinearLayout.getChildAt(i);
            ((EditText) currentLayout.getChildAt(0)).setText(mandatoryKeys[i]);
            if (null != mandatoryValues[i])
                ((EditText) currentLayout.getChildAt(1)).setText(mandatoryValues[i]);
        }

        setClickListeners(R.id.tv_Rs1000, R.id.tv_Rs200, R.id.tv_Rs500, R.id.tv_apply);
        // lets tell the people what version of sdk we are using
        setRightDrawable(amountEntered);

    }

    private void setRightDrawable(final EditText et) {
        final Drawable x = ContextCompat.getDrawable(this, R.drawable.ic_notification_cross);//your x image, this one from standard android images looks pretty good actually
        x.setBounds(0, 0, x.getIntrinsicWidth(), x.getIntrinsicHeight());
        et.setCompoundDrawables(null, null, x, null);
        et.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (et.getCompoundDrawables()[2] == null) {
                    return false;
                }
                if (event.getAction() != MotionEvent.ACTION_UP) {
                    return false;
                }
                if (event.getX() > et.getWidth() - et.getPaddingRight() - x.getIntrinsicWidth()) {
                    et.setText("");
                }
                return false;
            }
        });
    }


    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }


    @Override
    protected void onResume() {
        super.onResume();

        LinearLayout rowContainerLayout = (LinearLayout) findViewById(R.id.linear_layout_row_container);

        int childNodeCount = rowContainerLayout.getChildCount();
        // we need a unique txnid every time..
        for (int i = 0; i < childNodeCount; i++) {
            LinearLayout linearLayout = (LinearLayout) rowContainerLayout.getChildAt(i);
            switch (((EditText) linearLayout.getChildAt(0)).getText().toString()) {
                case PayuConstants.TXNID: // lets set up txnid.
                    ((EditText) linearLayout.getChildAt(1)).setText("" + System.currentTimeMillis());
                    break;
            }

        }
    }


    public void launchSdkUI(PayuHashes payuHashes) {
        // let me add the other params which i might use from other activity

        intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
//        intent.putExtra(PayuConstants.PAYMENT_DEFAULT_PARAMS, mPaymentParams);
        intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
        intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);


        /**
         *  just for testing, dont do this in production.
         *  i need to generate hash for {@link com.payu.india.Tasks.GetTransactionInfoTask} since it requires transaction id, i don't generate hash from my server
         *  merchant should generate the hash from his server.
         *
         */
        intent.putExtra(PayuConstants.SALT, salt);
        Log.d(TAG, "Salt :" + salt);

        startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);

    }


    /******************************
     * Client hash generation
     ***********************************/
    // Do not use this, you may use this only for testing.
    // lets generate hashes.
    // This should be done from server side..
    // Do not keep salt anywhere in app.
    private void getPaymentDataToRecharge() {
        String amount = amountEntered.getText().toString();

        if (TextUtils.isEmpty(amount)) {
            showToast("Please enter amount to recharge.");
            return;
        }
        showProgressDialog("Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();


        String f_amount = Float.parseFloat(amount) + "";

        HashMap<String, Object> data = new HashMap<>();
        data.put(Analytics.Params.AMOUNT, f_amount);
        Analytics.trackWalletEvent(Analytics.Actions.AddedMoney, data);

        params.put(PlaBroApi.PLABRO_RECHARGE_WALLET_AUTO_PARAM_AMOUNT, f_amount);
        params.put(PlaBroApi.PLABRO_RECHARGE_WALLET_AUTO_PARAM_ACTION, "recharge");
        params.put(PlaBroApi.PLABRO_RECHARGE_WALLET_AUTO_PARAM_STATE, "initiated");
        params.put(PlaBroApi.PLABRO_RECHARGE_WALLET_AUTO_PARAM_PAYMENT_MODE, "cash");
        params.put(PlaBroApi.PLABRO_RECHARGE_WALLET_AUTO_PARAM_PAYMENT_ENV, "0");
        String hashValues = "get_user_cards,save_user_card,edit_user_card,delete_user_card,payment_related_details_for_mobile_sdk";
        params.put(PlaBroApi.PLABRO_RECHARGE_WALLET_AUTO_PARAM_PAYMENT_GETHASH, hashValues);
        params = Utility.getInitialParams(PlaBroApi.RT.PAYMENT, params);
        AppVolley.processRequest(Constants.TASK_CODES.PAYMENT, PaymentResponse.class, null, PlaBroApi.getPaymentURL(), params, RequestMethod.GET, this);

    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        hideProgressDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.PAYMENT:
                PaymentResponse paymentResponse = (PaymentResponse) response.getResponse();
                navigateToBaseActivityNew(paymentResponse.getOutput_params().getData().getPostData());
                break;
        }
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        hideProgressDialog();
        if (!isRunning) return;
        Log.d(TAG, "OnFailure");
        //hideProgressDialog();
        switch (response.getCustomException().getExceptionType()) {
            case VolleyListener.SESSION_EXPIRED:
                onPlabroSessionExpired();
//                conditionalView.setVisibility(View.VISIBLE);
//                conditionalView.setType(PBConditionalDialogView.ConditionalDialog.SESSION_EXPIRED);

                break;
            case VolleyListener.NO_INETERNET_EXCEPTION:

//                conditionalView.setVisibility(View.VISIBLE);
//                conditionalView.setType(PBConditionalDialogView.ConditionalDialog.INTERNET_CONNECTION);
                break;
            case VolleyListener.STATUS_FALSE_EXCEPTION:
//                conditionalView.setVisibility(View.VISIBLE);
//                conditionalView.setType(PBConditionalDialogView.ConditionalDialog.SERVER_ERROR);
                Toast.makeText(RechargeWallet.this, response.getCustomException().getMessage(), Toast.LENGTH_LONG).show();
            case VolleyListener.JSON_PARSE_EXCEPTION:
//                conditionalView.setVisibility(View.VISIBLE);
//                conditionalView.setType(PBConditionalDialogView.ConditionalDialog.SERVER_ERROR);
            default:
                Toast.makeText(RechargeWallet.this, response.getCustomException().getMessage(), Toast.LENGTH_LONG).show();
//                conditionalView.setVisibility(View.VISIBLE);
//                conditionalView.setType(PBConditionalDialogView.ConditionalDialog.SERVER_ERROR);
                break;
        }


        switch (taskCode) {
            case Constants.TASK_CODES.PAYMENT:
                break;

        }
    }


    private void showProgressDialog(String msg) {
        if (null != dialog && dialog.isShowing()) {
        } else {
            dialog = new ProgressDialog(RechargeWallet.this);
            dialog.setMessage(msg);
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    private void hideProgressDialog() {
        if (null != dialog && dialog.isShowing()) {
            Utility.dismissProgress(dialog);
        }
    }

}







