package com.plabro.realestate.holders.notification;

import com.plabro.realestate.models.notifications.NotificationData;

/**
 * Created by nitin on 14/08/15.
 */
public interface NotificationHolderInterface {
    public void onBindViewHolder(int position, NotificationData notification);
}
