package com.plabro.realestate.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.plabro.realestate.R;
import com.plabro.realestate.holders.BaseFeedHolder;
import com.plabro.realestate.holders.BuilderHolder;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by nitin on 12/08/15.
 */
public class BuilderAdapter extends RecyclerView.Adapter<BaseFeedHolder> {
    private static final String TAG = "BuilderAdapter";
    ArrayList<BaseFeed> feeds;
    Context ctx;
    Picasso picasso;

    @Override
    public BaseFeedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_builder, parent, false);
        return new BuilderHolder(v);
    }

    @Override
    public int getItemCount() {
        return feeds.size();
    }

    @Override
    public void onBindViewHolder(BaseFeedHolder holder, int position) {
        holder.onBindViewHolder(position, feeds.get(position));
    }


    public BuilderAdapter(ArrayList<BaseFeed> feeds, Context ctx) {
        this.feeds = feeds;
        this.ctx = ctx;
        picasso = Picasso.with(ctx);
    }

//    @Override
//    public void onBindViewHolder(ViewHolder holder, int position) {
//        final BuilderFeed f = (BuilderFeed) feeds.get(position);
//        holder.builderDisplayName.setText(f.getDisplayname());
//        holder.desc.setText(f.getText());
//        holder.builderInfo.setText(f.getUsername() + "  " + f.getCallTime());
//        String img = f.getDisplay_img();
//        String logoImg = f.getLogo_img();
//        if (!TextUtils.isEmpty(img)) {
//            picasso.load(img).into(holder.displayImg);
//        }
//        if (!TextUtils.isEmpty(logoImg)) {
//            picasso.load(logoImg).into(holder.builderLogo);
//        }
////        int elipseCount = holder.desc.getLayout().getEllipsisCount(2);
////        Log.d(TAG, "Ellipsecount" + elipseCount);
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Bundle b = new Bundle();
//                b.putString(Constants.BUNDLE_KEYS.CITY_ID, "1");
//                b.putString(Constants.BUNDLE_KEYS.BUILDER_ID, f.getBuilder_id() + "");
//                b.putString(Constants.BUNDLE_KEYS.SHOUT_ID, f.get_id() + "");
//                Intent i = new Intent(ctx, BuilderDetail.class);
//                i.putExtras(b);
//                ctx.startActivity(i);
//            }
//        });
//        holder.call.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String phone = f.getProfile_phone();
//                ActivityUtils.callPhone(ctx, phone);
//            }
//        });
//        holder.chat.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Bundle b = new Bundle();
//                b.putString(Constants.BUNDLE_KEYS.PHONE_KEY, f.getProfile_phone());
//                b.putString(Constants.BUNDLE_KEYS.NAME, f.getUsername());
//                Intent i = new Intent(ctx, XMPPChat.class);
//                i.putExtras(b);
//                ctx.startActivity(i);
//            }
//        });
//        holder.collaborate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                FontText c = (FontText) view;
//                c.setEnabled(false);
//                if ("collaborate".equalsIgnoreCase(c.getText().toString())) {
//                    sendCollaboration(true, f, c);
//                } else {
//                    sendCollaboration(false, f, c);
//                }
//            }
//        });
//    }
//
//    private void sendCollaboration(boolean isCollaboration, BuilderFeed f, final FontText collaborationButton) {
//        HashMap<String, String> params = new HashMap<>();
//        params.put(Constants.BUNDLE_KEYS.BUILDER_ID, f.getBuilder_id() + "");
//        params.put(Constants.BUNDLE_KEYS.SHOUT_ID, f.get_id() + "");
//        String rt = PlaBroApi.RT.BUILDER_COLLABORATE;
//        int taskCode = Constants.TASK_CODES.BUILDER_COLLABORATE;
//        if (!isCollaboration) {
//            rt = PlaBroApi.RT.BUILDER_UN_COLLABORATE;
//            taskCode = Constants.TASK_CODES.BUILDER_UN_COLLABORATE;
//        }
//        params = Utility.getInitialParams(rt, params);
//        final String RT = rt;
//        final int TASK_CODE = taskCode;
//        AppVolley.processRequest(TASK_CODE, ServerResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
//            @Override
//            public void onSuccessResponse(PBResponse response, int taskCode) {
//                collaborationButton.setEnabled(true);
//                switch (taskCode) {
//                    case Constants.TASK_CODES.BUILDER_COLLABORATE:
//                        ServerResponse resp = (ServerResponse) response.getResponse();
//                        if (null != resp) {
//                            setCollaboratedButton(true, resp, collaborationButton);
//                        }
//                        break;
//                    case Constants.TASK_CODES.BUILDER_UN_COLLABORATE:
//                        ServerResponse resp2 = (ServerResponse) response.getResponse();
//                        if (null != resp2) {
//                            setCollaboratedButton(false, resp2, collaborationButton);
//                        }
//                        break;
//
//                }
//            }
//
//            @Override
//            public void onFailureResponse(PBResponse response, int taskCode) {
//                collaborationButton.setEnabled(true);
//            }
//        });
//    }
//
//    private void setCollaboratedButton(boolean isCollaborated, ServerResponse response, final FontText collaboratedButton) {
//        if (response.isStatus()) {
//            if (isCollaborated) {
//                collaboratedButton.setText("UN-COLLABORATE");
//            } else {
//                collaboratedButton.setText("COLLABORATE");
//            }
//        } else {
//            Toast.makeText(ctx, response.getMsg() + "", Toast.LENGTH_LONG).show();
//        }
//    }
//

//
//    static class ViewHolder extends RecyclerView.ViewHolder {
//
//        private FontText chat, call, collaborate, builderDisplayName, builderInfo, desc, readMore, time;
//        private ImageView displayImg, builderLogo;
//
//        public ViewHolder(View v) {
//            super(v);
//            chat = findTV(R.id.tv_chat);
//            call = findTV(R.id.tv_call);
//            collaborate = findTV(R.id.tv_collaborate);
//            builderDisplayName = findTV(R.id.tv_builder_name);
//            builderInfo = findTV(R.id.tv_builder_info);
//            desc = findTV(R.id.tv_builder_desc);
//            readMore = findTV(R.id.tv_readMore);
//            time = findTV(R.id.tv_builderFeedTime);
//            displayImg = (ImageView) findView(R.id.image_builder);
//            builderLogo = (ImageView) findView(R.id.iv_builder_logo);
//        }
//
//        private View findView(int id) {
//            return itemView.findViewById(id);
//        }
//
//        private FontText findTV(int id) {
//            return (FontText) findView(id);
//        }
//    }
}
