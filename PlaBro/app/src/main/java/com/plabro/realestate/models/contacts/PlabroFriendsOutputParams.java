package com.plabro.realestate.models.contacts;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class PlabroFriendsOutputParams {

    List<PlabroFriends> data = new ArrayList<PlabroFriends>();

    public List<PlabroFriends> getData() {
        return data;
    }

    public void setData(List<PlabroFriends> data) {
        this.data = data;
    }
}
