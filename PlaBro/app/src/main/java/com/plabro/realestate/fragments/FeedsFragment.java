package com.plabro.realestate.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.google.ads.conversiontracking.AdWordsConversionReporter;
import com.nhaarman.supertooltips.ToolTip;
import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.HomeActivity;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.adapter.FeedsCustomAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.Tracker.Campaign;
import com.plabro.realestate.models.chat.ChatPopUpInfo;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.LoaderFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.receivers.PBLocalReceiver;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.AppVolleyCacheManager;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;


/**
 * Created by hemant on 27/08/15.
 */
public class FeedsFragment extends CardListFragment implements HomeFragment.CityRefreshListener, ShoutObserver.PBObserver {

    FeedsCustomAdapter adapter;
    boolean addMoreGlobal = false;
    protected TextView mNewPost;
    protected boolean isToolTipVisible = false;


    @Override
    public String getTAG() {
        return "FeedsFragment";
    }

    public static Fragment getInstance(Bundle b) {
        FeedsFragment f = new FeedsFragment();
        f.setArguments(b);
        return f;
    }


    @Override
    protected void findViewById() {

        sendVersionQuery();
        mNewPost = (TextView) findView(R.id.new_feeds);
        mNewPost.setOnClickListener(newPostClickListener);

        super.findViewById();


    }


    @Override
    public FeedsCustomAdapter getAdapter() {
        if (null == adapter) {
            adapter = new FeedsCustomAdapter(baseFeeds, getActivity(), "Feeds");
        }
        return adapter;
    }

    @Override
    public void getData(final boolean addMore) {
        super.getData(addMore);
        Log.d(TAG, "Ge Data");
        addMoreGlobal = addMore;
        if (addMoreGlobal && baseFeeds.size() > 0) {
            if (baseFeeds.get(baseFeeds.size() - 1).getType().equalsIgnoreCase(Constants.FEED_TYPE.NO_MORE_DATA)) {
                return;
            }
        }
        Log.d(TAG, "Ge Data " + addMore);

        sendScreenName(R.string.funnel_property_feed_requested);
        showSpinners(addMore);
        HashMap<String, String> params = new HashMap<>();
        if (addMore) {
            params.put(Constants.START_IDX, "" + page);
        } else {
            params.put(Constants.START_IDX, "0");
        }

        params = Utility.getInitialParams(PlaBroApi.RT.PROPFEEDS, params);

        AppVolleyCacheManager.processRequest(Constants.TASK_CODES.PROPFEEDS, FeedResponse.class, !addMore, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                // Util.showSnackBarMessage(getActivity(),getActivity(),"Feeds fetched successfully");
                stopSpinners();
                sendScreenName(R.string.goal_property_feed_success);

                FeedResponse feedResponse = (FeedResponse) response.getResponse();
                if (null != feedResponse.getOutput_params()) {
                    noFeeds.setVisibility(View.GONE);
                    ArrayList<BaseFeed> tempFeeds = (ArrayList<BaseFeed>) feedResponse.getOutput_params().getData();
                    if (tempFeeds.size() > 0) {

                        LoaderFeed lf = new LoaderFeed();
                        if (tempFeeds.size() < Util.getDefaultIndexForFeed()) {
                            lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                        } else {
                            lf.setType(Constants.FEED_TYPE.PROGRESS_MORE);
                        }

                        if (addMore) {
                            baseFeeds.remove(baseFeeds.size() - 1);//for footer
                            baseFeeds.addAll(tempFeeds);
                            baseFeeds.add(lf);
                            adapter.notifyDataSetChanged();
                        } else {
                            page = 0;
                            mNewPost.setVisibility(View.GONE);
                            int val = PBPreferences.getTrialVersionState();
                            baseFeeds.clear();
                            if(val == 0 || val == 1){
                                for(int i=0; i<tempFeeds.size(); i++){
                                    if(Constants.FEED_TYPE.SHOUT.equalsIgnoreCase(tempFeeds.get(i).getType())){
                                        tempFeeds.get(i).setIsTrialFeed(true);
                                        break;
                                    }
                                }
                            }
                            baseFeeds.addAll(tempFeeds);
                            baseFeeds.add(lf);
                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            }
                        }
                        page = Util.getStartIndexForFeed(page);
                        //page scrolled for next set of data
                        HashMap<String, Object> data = new HashMap<>();
                        data.put("ScreenName", TAG);
                        data.put("Action", "Manual");
                        Analytics.trackEventWithProperties(R.string.consumption, R.string.e_scroll_next, data);


                    } else {
                        if (!addMore) {
                            comingSoon = true;
                        }

                        if (addMore) {
                            LoaderFeed lf = new LoaderFeed();
                            baseFeeds.remove(baseFeeds.size() - 1);//for footer
                            lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                            baseFeeds.add(lf);
                            adapter.notifyDataSetChanged();
                        }
                    }
                }
                showNoFeeds();
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                onApiFailure(response, taskCode);
                if (!addMoreGlobal) {
                    Util.showSnackBarMessage(getActivity(), getActivity(), "Unable to fetch new feeds.");
                    showNoFeeds();
                }

                try {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 2000ms
                            LoaderFeed lf = new LoaderFeed();
                            lf.setType(Constants.FEED_TYPE.NO_MORE);

                            if (addMore) {
                                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                                baseFeeds.add(lf);
                                adapter.notifyDataSetChanged();

                            }
                        }
                    }, 2000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    protected int getViewId() {
        return R.layout.feeds_fragment;
    }

    @Override
    public void onCityRefresh() {
        Log.d(TAG, "City Refresh : FF");
        getData(false);
    }

    @Override
    public void onShoutUpdate() {
        Log.d(TAG, "OnShoutUpdate");
        getData(false);
    }

    @Override
    public void onShoutUpdateGen(String type) {
        Log.d(TAG, "OnShout load more");
        if (type.equalsIgnoreCase(Constants.LOAD_MORE.SHOUT)) {
            if (addMoreGlobal) {
                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                LoaderFeed lf = new LoaderFeed();
                lf.setType(Constants.FEED_TYPE.PROGRESS_MORE);
                baseFeeds.add(lf);
                adapter.notifyDataSetChanged();
            }
            getData(addMoreGlobal);
        }
    }

    @Override
    public void onTrialVersionExpired(PBResponse response, int taskCode) {
        super.onTrialVersionExpired(response, taskCode);

        if (null != adapter && adapter.getItemCount() > 0 && !addMoreGlobal) {
            showRegistrationCard();
        }
    }

    private int getVersionCode() {
        PackageManager manager = getActivity().getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(getActivity().getPackageName(), 0);
            Log.d(TAG, "PackageName = " + info.packageName + "\nVersionCode = "
                    + info.versionCode + "\nVersionName = "
                    + info.versionName + "\nPermissions = " + info.permissions);
            return info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return -1;
    }

    private void sendVersionQuery() {

        final int versionCode = getVersionCode();
        final int savedVersionCode = PBPreferences.getData(PBPreferences.KEY_VERSION_CODE, -2);
        if (versionCode != -1) {
            if (versionCode != savedVersionCode) {
                String installUri = PBPreferences.getData(PBPreferences.CAMPAIGN_URI, null);
                String androidVersion = android.os.Build.VERSION.RELEASE;

                Log.d(TAG, "VersionCode :" + versionCode + ", SavedVersionCode:" + savedVersionCode + ",, " + installUri);
                HashMap<String, String> params = new HashMap<String, String>();
                params.put(PlaBroApi.GCM_REG_GET_PARAM_OSVERSION, androidVersion);
                params.put(PlaBroApi.PLABRO_UPDATEDAPPVERSION_POST_PARAM_APP_VERISON, versionCode + "");
                if (installUri != null) {
                    params.putAll(Campaign.parseUri(installUri));
                }
                params = Utility.getInitialParams(PlaBroApi.RT.UPDATEAPPVERSION, params);

                AdWordsConversionReporter.reportWithConversionId(
                        getActivity().getApplicationContext(),
                        Constants.CONVERSION_ID,
                        "BsyCCNjRo2EQkfXzwgM",
                        "0.00",  // The value of your conversion; can be modified to a transaction-specific value.
                        true);


                AppVolley.processRequest(1, null, null, String.format(PlaBroApi.getBaseUrl(getActivity())), params, RequestMethod.GET, new VolleyListener() {
                    @Override
                    public void onSuccessResponse(PBResponse response, int taskCode) {
                        PBPreferences.saveData(PBPreferences.KEY_VERSION_CODE, versionCode);
                        if (savedVersionCode != -2) {
//                            Utility.deleteLoginObject();
                        }

                    }

                    @Override
                    public void onFailureResponse(PBResponse response, int taskCode) {
                        if (savedVersionCode != -2) {
//                            Utility.deleteLoginObject();
                        }


                    }
                });

            }
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerLocalReceiver();
        ShoutObserver.getInstance().addListener(this);
        // Login login = (Login) new Select().from(Login.class).executeSingle();
        //Log.d(TAG,"Authkey "+login.getAuthkey());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegsiterLocalReceiver();
        ShoutObserver.getInstance().remove(this);
    }


    @Override
    protected void reloadRequest() {
        showSpinners(addMoreGlobal);
        getData(false);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String id = null;
        int pos = 0;
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.FEEDS_REQUEST_CODE) {

            page = 0;
            reloadRequest();

        }
    }


    private void checkingNewFeeds() {

        int val = PBPreferences.getFeedNotificationVal();

        String text = "New Feeds";
        if (val > 0) {
            HomeActivity.setNoOfNotification(0, true, val + "");

            if (val == 1) {
                text = val + " New Feed";

            } else if (val > 1 && val < 10) {
                text = val + " New Feeds";

            } else {
                text = "9+ New Feeds";
            }
            mNewPost.setText(text);
            mNewPost.setAnimation(animZoomIn);
            if (mLayoutManager.findFirstVisibleItemPosition() < 3) {
                mNewPost.setVisibility(View.GONE);
                disableNewFeedsNotification();
            } else {
                mNewPost.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        checkingNewFeeds();
//        if (ChatPopUpInfo.isChatStarted) {
//            ChatPopUpInfo.isChatStarted = false;
//            ((PlabroBaseActivity) getActivity()).showCallDialog(getActivity(), ChatPopUpInfo.feed, "CHAT ENDED WITH", false);
//        }
    }


    public void initiateToolTip() {
        Set<String> indexSet = PBPreferences.getHomeToolTipSet();
        boolean shoutTip = true;
        boolean mapTip = true;
        boolean searchTip = true;
        boolean relatedTip = true;


        for (String s : indexSet) {

            if (s.equalsIgnoreCase("shoutTip")) {
                shoutTip = false;
            }
            if (s.equalsIgnoreCase("mapTip")) {
                mapTip = false;
            }
            if (s.equalsIgnoreCase("searchTip")) {
                searchTip = false;
            }
            if (s.equalsIgnoreCase("relatedTip")) {
                relatedTip = false;
            }

        }
        long toolTipTime = PBPreferences.getHomeToolTipTime();
        long currentTime = System.currentTimeMillis();

        if (shoutTip) {
            showShoutToolTip(currentTime, indexSet);

        } else if (searchTip) {
            if (currentTime - toolTipTime > Constants.HOMETOOLTIPTIMER) {
                showSearchToolTip(currentTime, indexSet);
            }
        } else if (relatedTip) {
            if (currentTime - toolTipTime > Constants.HOMETOOLTIPTIMER) {
                showRelatedToolTip(currentTime, indexSet);
            }
        } else if (mapTip) {
            if (currentTime - toolTipTime > Constants.HOMETOOLTIPTIMER) {
                showMapToolTip(currentTime, indexSet);
            }
        }


    }

    void showShoutToolTip(final long currentTime, final Set<String> indexSet) {
        isToolTipVisible = true;

        toolTipRelativeLayout = (ToolTipRelativeLayout) findView(R.id.activity_main_tooltipRelativeLayout);
        ToolTip toolTipShout = new ToolTip()
                .withText("Shout about your requirements from here\n to help others reach you.")
                .withColor(getResources().getColor(R.color.colorPrimary))
                .withTextColor(getResources().getColor(R.color.white))
                .withShadow()
                .withAnimationType(ToolTip.AnimationType.FROM_TOP);
        ToolTipView myToolTipView = toolTipRelativeLayout.showToolTipForView(toolTipShout, findView(R.id.tv_shout_tt));
        myToolTipView.setOnToolTipViewClickedListener(new ToolTipView.OnToolTipViewClickedListener() {
            @Override
            public void onToolTipViewClicked(ToolTipView toolTipView) {
                indexSet.add("shoutTip");
                PBPreferences.setHomeToolTipSet(indexSet);
                PBPreferences.setHomeToolTipTime(currentTime);
                isToolTipVisible = false;

            }
        });
    }

    void showSearchToolTip(final long currentTime, final Set<String> indexSet) {
        isToolTipVisible = true;

        toolTipRelativeLayout = (ToolTipRelativeLayout) findView(R.id.activity_main_tooltipRelativeLayout);

        ToolTip toolTipSearch = new ToolTip()
                .withText("Search what others have to share with you.")
                .withColor(getResources().getColor(R.color.colorPrimary))
                .withTextColor(getResources().getColor(R.color.white))
                .withShadow()
                .withAnimationType(ToolTip.AnimationType.FROM_TOP);
        ToolTipView myToolTipViewSearch = toolTipRelativeLayout.showToolTipForView(toolTipSearch, findView(R.id.tv_search_tt));
        myToolTipViewSearch.setOnToolTipViewClickedListener(new ToolTipView.OnToolTipViewClickedListener() {
            @Override
            public void onToolTipViewClicked(ToolTipView toolTipView) {
                PBPreferences.setHomeToolTipTime(currentTime);
                indexSet.add("searchTip");
                PBPreferences.setHomeToolTipSet(indexSet);
                isToolTipVisible = false;

            }
        });

    }


    void showRelatedToolTip(final long currentTime, final Set<String> indexSet) {
        isToolTipVisible = true;

        toolTipRelativeLayout = (ToolTipRelativeLayout) findView(R.id.activity_main_tooltipRelativeLayout);

        ToolTip toolTipRelated = new ToolTip()
                .withText("Click on the card to view the matching shouts.")
                .withColor(getResources().getColor(R.color.colorPrimary))
                .withTextColor(getResources().getColor(R.color.white))
                .withShadow()
                .withAnimationType(ToolTip.AnimationType.FROM_TOP);
        ToolTipView myToolTipViewRelated = toolTipRelativeLayout.showToolTipForView(toolTipRelated, findView(R.id.tv_related_tt));
        myToolTipViewRelated.setOnToolTipViewClickedListener(new ToolTipView.OnToolTipViewClickedListener() {
            @Override
            public void onToolTipViewClicked(ToolTipView toolTipView) {
                PBPreferences.setHomeToolTipTime(currentTime);
                indexSet.add("relatedTip");
                PBPreferences.setHomeToolTipSet(indexSet);
                isToolTipVisible = false;

            }
        });

    }


    void showMapToolTip(final long currentTime, final Set<String> indexSet) {
        /*isToolTipVisible = true;
        toolTipRelativeLayout = (ToolTipRelativeLayout) findView(R.id.activity_main_tooltipRelativeLayout);

        ToolTip toolTipMaps = new ToolTip()
                .withText("Feeds in map view. Feature Coming soon.")
                .withColor(mContext.getResources().getColor(R.color.colorPrimary))
                .withTextColor(mContext.getResources().getColor(R.color.white))
                .withShadow()
                .withAnimationType(ToolTip.AnimationType.FROM_TOP);
        ToolTipView myToolTipViewMap = toolTipRelativeLayout.showToolTipForView(toolTipMaps, findView(R.id.tv_maps_tt));
        myToolTipViewMap.setOnToolTipViewClickedListener(new ToolTipView.OnToolTipViewClickedListener() {
            @Override
            public void onToolTipViewClicked(ToolTipView toolTipView) {
                PBPreferences.setHomeToolTipTime(mContext, currentTime);
                indexSet.add("mapTip");
                PBPreferences.setHomeToolTipSet(mContext, indexSet);
                isToolTipVisible =false;
            }
        });*/

    }

    View.OnClickListener newPostClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mRecyclerView.smoothScrollToPosition(0);
            mRecyclerView.setOnTouchListener(null);
            page = 0;
            reloadRequest();
            newPostListener();
            disableNewFeedsNotification();

        }
    };


    private void disableNewFeedsNotification() {

        mNewPost.setAnimation(animZoomOut);
        mNewPost.setVisibility(View.GONE);
        PBPreferences.setFeedNotificationVal(0);
        HomeActivity.setNoOfNotification(0, false, "");

    }

    @Override
    public void onHideRecycleView(RecyclerView recyclerView, int dx, int dy) {
        super.onHideRecycleView(recyclerView, dx, dy);
        if (!isToolTipVisible) {
            // initiateToolTip();
        }
    }

    @Override
    public void onShowRecycleView(RecyclerView recyclerView, int dx, int dy) {
        super.onShowRecycleView(recyclerView, dx, dy);
    }

    @Override
    public void onScrollFire(RecyclerView recyclerView, int dx, int dy) {
        super.onScrollFire(recyclerView, dx, dy);
    }


    @Override
    public void newPostListener() {
        super.newPostListener();
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Received INtent Action :" + intent.getAction());
        if (PBLocalReceiver.PBActionListener.ACTION_REGISTRATION_UPDATE.equals(intent.getAction())) {
            if (adapter != null && adapter.getItemCount() > 0) {
                adapter.notifyItemChanged(0);
            }
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        Analytics.trackScreen(Analytics.PropFeed.PropFeed);
    }
}
