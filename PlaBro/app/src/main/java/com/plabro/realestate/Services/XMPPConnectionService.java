package com.plabro.realestate.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.text.TextUtils;

import com.activeandroid.query.Select;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.listeners.MessageListenerImpl;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.chat.ChatMessage;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.PBConnectionManager;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.Connection;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Random;


public class XMPPConnectionService extends Service implements PBConnectionManager.XMPPConnectionListener {
    private static final String TAG = XMPPConnectionService.class.getSimpleName();

    private static final String ACTION_ON_CONNECTIVITY_CHANGE = "onConnectivityChange";

    private int retryAttempt = 0;
    private static final int MAX_RETRY_ATTEMPT = 3;
    private PBConnectionManager connManager;
    private Chat currentChat;
    private Connection conn;
    private MessageListenerImpl messageListener;
    private boolean isSendingChatMsg = false;
    private boolean stopSelf;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        connManager = PBConnectionManager.getInstance(AppController.getInstance(), this);
    }

    public static void startXMPPService(Context ctx) {
        ctx.startService(new Intent(ctx, XMPPConnectionService.class));
    }

    public static void startServiceOnConnectivityChange(Context ctx) {
        Intent intent = new Intent(ctx, XMPPConnectionService.class);
        intent.putExtra("stopSelf", true);
        ctx.startService(intent);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        retryAttempt = 0;
//        hitUpdateQRCode();
        if (connManager.getConnection() != null && connManager.getConnection().isConnected() && connManager.getConnection().isAuthenticated()) {
        } else {
            connManager.connectInBackground();
        }
        return START_STICKY;
    }

    private void sendChatMessages(Connection conn) {
        final ChatManager chatManager = conn.getChatManager();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "SendingChatMessages");


                List<ChatMessage> chatMessages = new Select().from(ChatMessage.class).where("msgType = " + ChatMessage.MESSAGE_OUTGOING + " and status = '" + ChatMessage.STATUS_PENDING + "'").execute();

                if (chatMessages != null && chatMessages.size() > 0) {
                    Log.d(TAG, "Size:" + chatMessages.size());
                    for (ChatMessage msg : chatMessages) {
                        Log.d(TAG, "Trying to Create Chat:" + ActivityUtils.getTo(msg.getPhone()));
                        currentChat = chatManager.createChat(ActivityUtils.getTo(msg.getPhone()), messageListener);
                        Log.d(TAG, "Current Chat :" + currentChat);
                        messageListener.sendChatMessage(msg, currentChat);
                    }
                }
            }
        }).start();

    }


    @Override
    public void onConnectionCreated(Connection c) {
        Log.d(TAG, "onConnectionCreated");
        if (c != null) {
            conn = c;
            retryAttempt = 0;
            messageListener = null;
            messageListener = new MessageListenerImpl(XMPPConnectionService.this, null, null);
            sendChatMessages(conn);


        }
    }

    private String randomNumberString;

    private void hitUpdateQRCode() {
        randomNumberString = getRandomNumber() + "";
        ParamObject p = new ParamObject();
        HashMap<String, String> params = new HashMap<>();
        params.put("qrcode", randomNumberString);
        p.setParams(Utility.getInitialParams(PlaBroApi.RT.GET_AUTH_QR_CODE, params));
        AppVolley.processRequest(p, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "Response QRCode:" + response);
                String json = (String) response.getResponse();
                if (!TextUtils.isEmpty(json)) {
                    try {
                        JSONObject obj = new JSONObject(json);
                        String serverKey = obj.getJSONObject("output_params").getJSONObject("data").getString("sk");
                        getUserNamePassword(serverKey);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "Response QRCode:" + response.getCustomException().getMessage());
            }
        });
    }

    private void getUserNamePassword(String serverKey) {
        ParamObject p = new ParamObject();
        HashMap<String, String> params = new HashMap<>();
        params.put("get_un_p", "1");
        params.put("sk", serverKey);
        p.setParams(Utility.getInitialParams(PlaBroApi.RT.GET_AUTH_QR_CODE, params));
        AppVolley.processRequest(p, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "Response QRCode:" + response);
                String json = (String) response.getResponse();
                if (!TextUtils.isEmpty(json)) {
                    try {
                        JSONObject obj = new JSONObject(json);
//                        String serverKey = obj.getJSONObject("output_params").getJSONObject("data").getString("sk");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "Response QRCode:" + response.getCustomException().getMessage());
            }
        });
    }

    private int getRandomNumber() {
        return new Random().nextInt(1000);
    }

    @Override
    public void onConnectionClosed(Connection conne) {
        conn = null;
        Log.d(TAG, "onConnectionFailed");
        if (connManager.getConnection() != null && connManager.getConnection().isConnected() && connManager.getConnection().isAuthenticated()) {
        } else {
            retryAttempt++;
            if (retryAttempt < MAX_RETRY_ATTEMPT)
                connManager.connectInBackground();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "OnDestroy");
        if (connManager != null && connManager.getConnection() != null) {
            connManager.removeConnectionListener(this);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (connManager.getConnection().isConnected())
                        connManager.getConnection().disconnect();
                }
            }).start();

        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d(TAG, "Task Removed");
        super.onTaskRemoved(rootIntent);
        Log.d(TAG, "Task Removed");
    }
}
