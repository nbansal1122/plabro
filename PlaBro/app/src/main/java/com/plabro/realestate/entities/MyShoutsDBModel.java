package com.plabro.realestate.entities;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.utils.Serializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 18-Apr-15.
 */
@Table(name = "MyShouts")
public class MyShoutsDBModel extends Model {

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public byte[] getFeedObject() {
        return feedObject;
    }

    public void setFeedObject(byte[] feedObject) {
        this.feedObject = feedObject;
    }

    @Column(name = BaseColumns._ID, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String _id;
    @Column
    private byte[] feedObject;

    public static void insertFeed(BaseFeed data) {
        MyShoutsDBModel model = new MyShoutsDBModel();
        if ("shout".equals(data.getType())) {
            Feeds d = (Feeds) data;
            model._id = d.get_id();
            try {
                model.feedObject = Serializer.serialize(d);

            } catch (Exception e) {
                Log.i("FeedsDBModel", "Exception while serializing data");
            }
            model.save();
        }

    }


    public static List<Feeds> getMyShouts(List<MyShoutsDBModel> list) {
        List<Feeds> feeds = new ArrayList<Feeds>();

        if (list != null) {
            for (MyShoutsDBModel model : list) {
                try {
                    Feeds feed = (Feeds) Serializer.deserialize(model.getFeedObject());
                    Log.d("MyShoutsDBModel", "Adding Feed");
                    feeds.add(feed);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        Log.d("MyShoutsDBModel", "List is null");
        return feeds;
    }
}
