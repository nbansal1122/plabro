package com.plabro.realestate.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.gson.GsonBuilder;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.LocationAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.location.GoogleLocationResponse;
import com.plabro.realestate.models.location.GoogleResult;
import com.plabro.realestate.models.location.Places;
import com.plabro.realestate.uiHelpers.GPSTracker;
import com.plabro.realestate.uiHelpers.MaterialDialog;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.JSONUtils;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class AddLocation extends PlabroBaseActivity implements LocationListener /*implements SearchView.OnQueryTextListener */ {

    SearchView mSearchView;
    TextView searchText;
    View searchDiv;
    ListView locationListView;
    LocationAdapter adapter;
    Filter filter;
    private TextView mNoLocations;
    ProgressWheel mProgressWheel;
    private String url = "https://maps.googleapis.com/maps/api/place/radarsearch/json";
    GoogleMap googleMap;
    EditText placeText;
    double latitude = 0;
    double longitude = 0;
    private int PROXIMITY_RADIUS = 5000;


//    private String url = "https://maps.googleapis.com/maps/api/place/radarsearch/json?location=48.859294,2.347589&radius=5000&types=food|cafe&keyword=vegetarian&key=AddYourOwnKeyHere";

    @Override
    protected String getTag() {
        return "AddLocation";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);


        //setting action bar
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        // Set an OnMenuItemClickListener to handle menu item clicks
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Handle the menu item
                int id = item.getItemId();

                switch (id) {


                }
                return true;
            }

        });

        // Inflate a menu to be displayed in the toolbar
        toolbar.inflateMenu(R.menu.menu_shout);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });


        // mSearchView = (SearchView) findViewById(R.id.search);
        locationListView = (ListView) findViewById(R.id.locations);
        mNoLocations = (TextView) findViewById(R.id.noLocations);
        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mProgressWheel.spin();

        //locationListView.setTextFilterEnabled(true);


       /* searchText = (TextView) findViewById(R.id.searchText);
        searchDiv = findViewById(R.id.searchDiv);
        locationListView = (ListView) findViewById(R.id.locations);

        mSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                searchText.setVisibility(View.GONE);
                searchDiv.setVisibility(View.GONE);

            }
        });

        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                searchText.setVisibility(View.VISIBLE);
                searchDiv.setVisibility(View.VISIBLE);
                return false;
            }
        });*/

        // getAndShowLocation();
        getAndShowGoogleLocation();
        // getGoogleResults();
        // setupSearchView();


    }


    private String convertStreamToString(InputStream in) {
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        StringBuilder jsonstr = new StringBuilder();
        String line;
        try {
            while ((line = br.readLine()) != null) {
                String t = line + "\n";
                jsonstr.append(t);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonstr.toString();
    }


//    void getAndShowLocation() {
//
//
//        if (!Util.haveNetworkConnection(AddLocation.this)) {
//
//            final MaterialDialog mMaterialDialog = new MaterialDialog(AddLocation.this);
//
//            mMaterialDialog.setTitle("No Connectivity. Try again ? ")
//                    .setMessage("Hey! Seems there some lack in network connection")
//                    .setPositiveButton("OK", new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            // getAndShowLocation();
//                            mMaterialDialog.dismiss();
//                        }
//                    })
//
//                    .setNegativeButton("CANCEL", new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            mMaterialDialog.dismiss();
//                        }
//                    });
//
//            mMaterialDialog.show();
//            return;
//        }
//
//
//        HashMap<String, String> params = new HashMap<String, String>();
//
//        params.put(PlaBroApi.PLABRO_LOCATION_POST_PARAM_LOCATION, "{\"loc\": [28.42, 77.04]}");
//        params.put(PlaBroApi.PLABRO_LOCATION_POST_PARAM_COUNT, "10  ");
//
//        params = Utility.getInitialParams(PlaBroApi.RT.LOCATION_NEAR_BY, params);
//
//        AppVolley.processRequest(Constants.TASK_CODES.LOCATION_NEARBY, LocationResponse.class, null, PlaBroApi.getBaseUrl(AddLocation.this), params, RequestMethod.GET, new VolleyListener() {
//            @Override
//            public void onSuccessResponse(PBResponse response, int taskCode) {
//                mProgressWheel.stopSpinning();
//                if (response != null) {
//
//                    LocationResponse locationResponse = (LocationResponse) response.getResponse();
//
//                    if (locationResponse.isStatus()) {
//
//
//                        List<Locations> values = locationResponse.getOutput_params().getData();
//
//                        if (values.size() > 0) {
//
//                            final ArrayList<String> list = new ArrayList<String>();
//                            for (int i = 0; i < values.size(); ++i) {
//
//                                list.add(values.get(i).getLocation());
//                            }
//
//
//                            adapter = new LocationAdapter(getApplicationContext(), AddLocation.this, list);
//                            //filter = mAdapter.getFilter();
//
//                            locationListView.setAdapter(adapter);
//                            locationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                                @Override
//                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//
//                                    Intent intent = new Intent();
//                                    intent.putExtra("location", list.get(position));
//                                    setResult(RESULT_OK, intent);
//                                    finish();
//
//
//                                }
//                            });
//                            Log.i("PlaBro", "Location fetched sussessfully " + locationResponse.getOutput_params().getData().get(0).getLoc());
//                            mNoLocations.setVisibility(View.GONE);
//                        } else {
//                            mNoLocations.setVisibility(View.VISIBLE);
//                        }
//
//                    } else {
//
//
//                        Log.i("PlaBro", "cant fetch location list due to :" + locationResponse.getMsg());
//                        Toast.makeText(AddLocation.this, "Some error occurred. Please try again.", Toast.LENGTH_LONG).show();
//
//                        if (locationResponse.getMsg().equalsIgnoreCase("Session Expired") || locationResponse.getMsg().equalsIgnoreCase("Missing AuthKey in the request")) {
//                            showSessionExpiredDialog();
//
//                        }
//
//                    }
//
//                }
//            }
//
//            @Override
//            public void onFailureResponse(PBResponse response, int taskCode) {
//                mProgressWheel.stopSpinning();
//                switch (response.getCustomException().getExceptionType()) {
//                    case VolleyListener.SESSION_EXPIRED:
//                        showSessionExpiredDialog();
//                        break;
//
//                }
//            }
//
//        });
//
//
//    }


    public void showSessionExpiredDialog() {
        if (!PBPreferences.getSessEXpDialogState()) {

            PBPreferences.setSessEXpDialogState(true);

            final MaterialDialog mMaterialDialog = new MaterialDialog(AddLocation.this);

            mMaterialDialog.setTitle("Try again")
                    .setMessage("Oops ! Your Session Expired.")
                    .setPositiveButton("CONNECT", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utility.userLogin(AddLocation.this);

                            //  getAndShowLocation();
                            mMaterialDialog.dismiss();
                        }
                    })

                    .setNegativeButton("CANCEL", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            mMaterialDialog.dismiss();
                        }
                    });

            mMaterialDialog.show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_location, menu);
        return true;
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        // latitude = location.getLatitude();
        // longitude = location.getLongitude();
        //LatLng latLng = new LatLng(latitude, longitude);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void findViewById() {

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }


    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }
    }


    void getAndShowGoogleLocation() {

        //fetch fresh location
        fetchLocation();

        if (latitude == 0) {
            Toast.makeText(AddLocation.this, "Cant fetch location.", Toast.LENGTH_LONG).show();
            return;
        }

        //String type = placeText.getText().toString();
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        // googlePlacesUrl.append("&types=" + type);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + Constants.LOCATION_SERVER_NEARBY_API_KEY);

        url = googlePlacesUrl.toString();


        if (!Util.haveNetworkConnection(AddLocation.this)) {

            final MaterialDialog mMaterialDialog = new MaterialDialog(AddLocation.this);

            mMaterialDialog.setTitle("No Connectivity. Try again ? ")
                    .setMessage("Hey! Seems there some lack in network connection")
                    .setPositiveButton("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getAndShowGoogleLocation();
                            mMaterialDialog.dismiss();
                        }
                    })

                    .setNegativeButton("CANCEL", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMaterialDialog.dismiss();
                        }
                    });

            mMaterialDialog.show();
            return;
        }


        HashMap<String, String> params = new HashMap<String, String>();


        AppVolley.processRequest(0, null, null, url, params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse pbResponse, int taskCode) {
                String response = (String) pbResponse.getResponse();
                if (response != null) {
                    mProgressWheel.stopSpinning();

                    GsonBuilder gson = JSONUtils.getGSONBuilder();

                    GoogleLocationResponse locationResponse = gson.create().fromJson(response, GoogleLocationResponse.class);

                    if (locationResponse.getStatus().equalsIgnoreCase("OK")) {

                        Places places = new Places();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            List<GoogleResult> nearBy = locationResponse.getResults();


                            if (nearBy.size() > 0) {

                                final ArrayList<String> list = new ArrayList<String>();
                                for (int i = 0; i < nearBy.size(); ++i) {

                                    list.add(nearBy.get(i).getName());
                                }


                                adapter = new LocationAdapter(getApplicationContext(), AddLocation.this, list);
                                //filter = mAdapter.getFilter();

                                locationListView.setAdapter(adapter);
                                locationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                                        Intent intent = new Intent();
                                        intent.putExtra("location", list.get(position));
                                        setResult(RESULT_OK, intent);
                                        finish();


                                    }
                                });
                                mNoLocations.setVisibility(View.GONE);
                            } else {
                                mNoLocations.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {

                    }

                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Log.i("PlaBro", "onFailure : Oops...something went wrong");
                mProgressWheel.stopSpinning();

            }
        });


    }

    void fetchLocation() {
        // check if GPS enabled
        GPSTracker gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

//            String stringLatitude = String.valueOf(gpsTracker.getLatitude());
//
//            String stringLongitude = String.valueOf(gpsTracker.getLongitude());
//
//            String country = gpsTracker.getCountryName(this);
//
//            String city = gpsTracker.getLocality(this);
//
//            String postalCode = gpsTracker.getPostalCode(this);
//
//            String addressLine = gpsTracker.getAddressLine(this);

        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert(AddLocation.this);
            mProgressWheel.stopSpinning();
        }

    }

}
