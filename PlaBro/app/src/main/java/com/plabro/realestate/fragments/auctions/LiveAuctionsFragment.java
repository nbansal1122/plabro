package com.plabro.realestate.fragments.auctions;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.FeedsCustomAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.fragments.CardListFragment;
import com.plabro.realestate.fragments.HomeFragment;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.LoaderFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.receivers.PBLocalReceiver;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nitin on 22/01/16.
 */
public class LiveAuctionsFragment extends CardListFragment implements HomeFragment.CityRefreshListener {

    public static final String FEED_TYPE = "feed_type";
    private static final String CITY_ID = "city_id";
    FeedsCustomAdapter adapter;
    boolean addMoreGlobal = false;
    private String feedType = "all";
    private String city_id;
    private Toolbar toolbar;
    private SearchBoxCustom searchBoxCustom;
    private String searchText;
    private boolean isSearched = false;
    private AuctionFragment.TabToggleListener tabToggleListener;


    @Override
    public String getTAG() {
        return "FeedsFragment";
    }


    @Override
    public FeedsCustomAdapter getAdapter() {
        if (null == adapter) {
            adapter = new FeedsCustomAdapter(baseFeeds, getActivity(), "Feeds");
        }
        return adapter;
    }

    @Override
    protected void loadBundleData(Bundle b) {
        feedType = b.getString(FEED_TYPE);
    }

    @Override
    public void getData(final boolean addMore) {
        super.getData(addMore);
        Log.d(TAG, "Ge Data");
        addMoreGlobal = addMore;
        if (addMoreGlobal && baseFeeds.size() > 0) {
            if (baseFeeds.get(baseFeeds.size() - 1).getType().equalsIgnoreCase(Constants.FEED_TYPE.NO_MORE_DATA)) {
                return;
            }
        }
        Log.d(TAG, "Ge Data " + addMore);

        showSpinners(addMore);
        HashMap<String, String> params = new HashMap<>();
        if (addMore) {
            params.put("startidx", "" + page);
        } else {
            params.put("startidx", "0");
        }
        isSearched = false;
        int taskCode = Constants.TASK_CODES.GET_LIVE_AUCTIONS;
//        params.put(CITY_ID, TextUtils.isEmpty(city_id) ? "1" : city_id);
        if (searchBoxCustom != null && searchBoxCustom.isShown()) {
            searchText = searchBoxCustom.getSearchText();
        }else{
            searchText = null;
        }
        if (TextUtils.isEmpty(searchText)) {

            params = Utility.getInitialParams(PlaBroApi.RT.GET_LIVE_AUCTIONS, params);
        } else {
            params.put("q_str", searchText);
            taskCode = Constants.TASK_CODES.SEARCH_AUCTION;
            params = Utility.getInitialParams(PlaBroApi.RT.SEARCH_AUCTIONS, params);
        }


        AppVolley.processRequest(taskCode, FeedResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                // Util.showSnackBarMessage(getActivity(),getActivity(),"Feeds fetched successfully");
                stopSpinners();

                FeedResponse feedResponse = (FeedResponse) response.getResponse();
                if (null != feedResponse.getOutput_params()) {
                    noFeeds.setVisibility(View.GONE);
                    ArrayList<BaseFeed> tempFeeds = (ArrayList<BaseFeed>) feedResponse.getOutput_params().getData();
                    if (tempFeeds.size() > 0) {

                        LoaderFeed lf = new LoaderFeed();
                        if (tempFeeds.size() < Util.getDefaultStartIdx()) {
                            lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                            lf.setText("Auctions");
                        } else {
                            lf.setType(Constants.FEED_TYPE.PROGRESS_MORE);
                        }

                        if (addMore && baseFeeds.size() > 0) {
                            baseFeeds.remove(baseFeeds.size() - 1);//for footer
                            baseFeeds.addAll(tempFeeds);
                            baseFeeds.add(lf);
                            adapter.notifyDataSetChanged();
                        } else {
                            page = 0;
                            baseFeeds.clear();
                            baseFeeds.addAll(tempFeeds);
                            baseFeeds.add(lf);
                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            }
                        }
                        page = Util.getStartIndex(page);
                        //page scrolled for next set of data
                        HashMap<String, Object> data = new HashMap<>();
                        data.put("ScreenName", TAG);
                        data.put("Action", "Manual");
                        Analytics.trackEventWithProperties(R.string.consumption, R.string.e_scroll_next, data);


                    } else {
                        if (!addMore) {
                            comingSoon = true;
                            if (taskCode == Constants.TASK_CODES.SEARCH_AUCTION) {
                                baseFeeds.clear();
                                page = 0;
                                isSearched = true;
                                if (adapter != null) {
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        } else if (baseFeeds.size() > 0) {
                            LoaderFeed lf = new LoaderFeed();
                            baseFeeds.remove(baseFeeds.size() - 1);//for footer
                            lf.setText("listings");
                            lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                            baseFeeds.add(lf);
                            adapter.notifyDataSetChanged();
                        } else {
                        }
                    }
                }
                showNoFeeds();
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                if (null == getActivity()) return;
                onApiFailure(response, taskCode);
                stopSpinners();
                try {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 2000ms
                            LoaderFeed lf = new LoaderFeed();
                            lf.setType(Constants.FEED_TYPE.NO_MORE);

                            if (addMore) {
                                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                                baseFeeds.add(lf);
                                adapter.notifyDataSetChanged();

                            }
                        }
                    }, 2000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    protected void showNoFeeds() {
        if (baseFeeds.size() < 1) {
            noFeeds.setVisibility(View.VISIBLE);
            setnoFeedsLayout();
        } else {
            noFeeds.setVisibility(View.GONE);
        }
    }


    @Override
    protected int getNoFeedDrawable() {
        return R.drawable.img_empty_live_auctions;
    }

    @Override
    protected String getNoFeedsText() {
        if (isSearched) {
            return getString(R.string.no_searched_auctions);
        }
        return getString(R.string.no_active_auctions);
    }

    @Override
    protected int getViewId() {
        return R.layout.feeds_fragment;
    }


    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerLocalReceiver();
    }

    @Override
    public IntentFilter getFilter() {
        filter = super.getFilter();
        filter.addAction(PBLocalReceiver.PBActionListener.ACTION_REFRESH_AUCTIONS);
        return filter;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    protected void reloadRequest() {
        showSpinners(addMoreGlobal);
        getData(false);
    }


    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Received INtent Action :" + intent.getAction());
        if (PBLocalReceiver.PBActionListener.ACTION_REFRESH_AUCTIONS.equals(intent.getAction())) {
            getData(false);
        }
    }

    @Override
    protected void findViewById() {
        if ("all".equalsIgnoreCase(feedType)) {
            Analytics.trackScreen(Analytics.Requirements.AllRequirements);
        } else {
            Analytics.trackScreen(Analytics.Requirements.MyBids);
        }
        super.findViewById();
        shout.setVisibility(View.GONE);
        scroll.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    public static Fragment getInstance(Bundle b, Toolbar toolbar, SearchBoxCustom searchBoxCustom, AuctionFragment.TabToggleListener listener) {
        LiveAuctionsFragment f = new LiveAuctionsFragment();
        f.setArguments(b);
        f.toolbar = toolbar;
        f.searchBoxCustom = searchBoxCustom;
        f.tabToggleListener = listener;
        return f;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (toolbar == null) {
            toolbar = (Toolbar) getActivity().findViewById(R.id.my_awesome_toolbar);
        }
        if (searchBoxCustom == null) {
            searchBoxCustom = (SearchBoxCustom) getActivity().findViewById(R.id.searchbox);
        }
//        toolbar.getMenu().clear();
        inflateToolbar();
//        super.onCreateOptionsMenu(menu, inflater);
    }

    public void openSearch(final int id) {
        //toolbar.setTitle("");
        searchBoxCustom.enableVoiceRecognition(this);
        searchBoxCustom.revealFromMenuItem(R.id.action_search, getActivity());
        searchBoxCustom.setMenuListener(new SearchBoxCustom.MenuListener() {

            @Override
            public void onMenuClick() {
                if (searchBoxCustom.isShown()) {
                    searchBoxCustom.hideCircularly(getActivity());
                }

            }

        });
        searchBoxCustom.setSearchListener(new SearchBoxCustom.SearchListener() {


            @Override
            public void onSearchOpened() {
                if (tabToggleListener != null) {
                    tabToggleListener.hideTabs();
                }
                Log.d(TAG, "On Search Opened");
            }

            @Override
            public void onSearchClosed() {
                // Use this to un-tint the screen
                Log.d(TAG, "On Search Closed");
                if (tabToggleListener != null) {
                    tabToggleListener.showTabs();
                }
                getData(false);
            }

            @Override
            public void onSearchTermChanged(String term) {
                // React to the search term changing
                // Called after it has updated results
                Log.d(TAG, "On Search Term Changed");
            }


            @Override
            public void onSearch(String searchTerm) {
                searchText = searchTerm.trim();
                getData(false);
                searchText = null;
            }

            @Override
            public void onSearchCleared() {
                Log.d(TAG, "On Search Cleared");
            }

        });

    }

    private void inflateToolbar() {
        Log.v(TAG, "toolbar inflated");
        toolbar.inflateMenu(R.menu.menu_auction);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id) {

                    case R.id.action_search:
                        openSearch(id);
                        break;


                }
                return true;
            }

        });
    }


    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    @Override
    public void onCityRefresh() {
        if (baseFeeds != null) {
            baseFeeds.clear();
            adapter.notifyDataSetChanged();
        }
        getData(false);
    }
}
