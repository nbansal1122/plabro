package com.plabro.realestate.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by nbansal2211 on 24/07/17.
 */

public class CustomSpinnerAdapter<T> extends CustomListAdapter {

    public CustomSpinnerAdapter(Context context, int resource, List<T> objects, CustomListAdapterInterface ref) {
        super(context, resource, objects, ref);

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return ref.getView(position, convertView, parent, rowlayoutID);
    }
}
