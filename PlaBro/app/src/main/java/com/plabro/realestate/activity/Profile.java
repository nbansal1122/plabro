package com.plabro.realestate.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Intents;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.plabro.realestate.ImageGallery;
import com.plabro.realestate.R;
import com.plabro.realestate.ShoutByOthers;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.follow_unfollow.FollowUnFollowResponse;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.profile.ProfileResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.uiHelpers.MaterialDialog;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;


public class Profile extends PlabroBaseActivity {


    private Toolbar toolbar;
    private ScrollView sv_profile;
    private RoundedImageView user_image;
    private TextView tv_last_active, tv_profile_user_followers, tv_profile_user_following, tv_follow,
            tv_following, tv_profile_status, tv_user_phone, tv_user_mail, tv_user_social, tv_user_addr,
            tv_business, tv_shouts, tv_no_profile;
    private ImageView mPhoneImage, mChatImage;
    private RelativeLayout mPhoneLayout;
    private ProgressWheel mProgressWheel;
    private String mAuthorId, mPhone;
    private Activity mActivity;
    private String mUserName;
    private RelativeLayout mEmailLayout;
    private ProfileClass profileClass;

    @Override
    protected String getTag() {
        return "Profile";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Intent i = new Intent(this, UserProfileNew.class);
        i.putExtras(getIntent().getExtras());
        startActivity(i);
        finish();


//        Analytics.trackScreen(Profile.this, getResources().getString(R.string.s_profile));
//
//
//        findViewById();
//
//        inflateToolbar();
//
//        getUSerProfile(); //todo get local profile if available and add check not to show error if profile available

    }

    @Override
    public void inflateToolbar() {
        //setting action bar
        toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        // Set an OnMenuItemClickListener to handle menu item clicks
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Handle the menu item
                int id = item.getItemId();

                switch (id) {

                    case R.id.action_options:

                        if (null != profileClass) {
                            // saveContact();
                            editOrInsertContact();
                        } else {
                            Util.showSnackBarMessage(Profile.this, Profile.this, "Could not fetch profile.Please check Network.");
                        }

                        return true;
                }
                return true;
            }

        });

        // Inflate a menu to be displayed in the toolbar
        toolbar.inflateMenu(R.menu.menu_profile);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });

    }

    private void saveContact() {

        // Creates a new Intent to insert a contact
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        // Sets the MIME type to match the Contacts Provider
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

        // Inserts an email address
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, profileClass.getEmail());
/*
 * In this example, sets the email type to be a work email.
 * You can set other email types as necessary.
 */
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK);
// Inserts a phone number
        intent.putExtra(Intents.Insert.PHONE, profileClass.getPhone());
/*
 * In this example, sets the phone type to be a work phone.
 * You can set other phone types as necessary.
 */
        intent.putExtra(Intents.Insert.PHONE_TYPE, Phone.TYPE_WORK);
          /* Sends the Intent
     */
        startActivity(intent);
    }

    private void editOrInsertContact() {

        Intent i = new Intent(Intent.ACTION_INSERT_OR_EDIT);
        i.setType(ContactsContract.Contacts.CONTENT_ITEM_TYPE);
        i.putExtra(Intents.Insert.NAME, profileClass.getName());
        i.putExtra(Intents.Insert.PHONETIC_NAME, profileClass.getName());
        i.putExtra(Intents.Insert.PHONE, profileClass.getPhone());
        i.putExtra(Intents.Insert.EMAIL, profileClass.getEmail());
        i.putExtra(Intents.Insert.COMPANY, profileClass.getBusiness_name());

        startActivity(i);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }


    @Override
    public void findViewById() {

        mActivity = Profile.this;
        sv_profile = (ScrollView) findViewById(R.id.sv_profile);
        user_image = (RoundedImageView) findViewById(R.id.user_image);
        mPhoneImage = (ImageView) findViewById(R.id.iv_phone_1);
        mChatImage = (ImageView) findViewById(R.id.iv_chat);
        tv_last_active = (TextView) findViewById(R.id.tv_last_active);
        tv_profile_user_followers = (TextView) findViewById(R.id.tv_profile_user_followers);
        tv_profile_user_following = (TextView) findViewById(R.id.tv_profile_user_following);
        tv_follow = (TextView) findViewById(R.id.tv_follow);
        tv_following = (TextView) findViewById(R.id.tv_following);
        tv_profile_status = (TextView) findViewById(R.id.tv_profile_status);
        tv_user_phone = (TextView) findViewById(R.id.tv_user_phone);
        tv_user_mail = (TextView) findViewById(R.id.tv_user_mail);
        tv_user_social = (TextView) findViewById(R.id.tv_user_social);
        tv_user_addr = (TextView) findViewById(R.id.tv_user_addr);
        tv_no_profile = (TextView) findViewById(R.id.tv_no_profile);
        tv_business = (TextView) findViewById(R.id.tv_user_business);
        tv_shouts = (TextView) findViewById(R.id.shouts);
        mPhoneLayout = (RelativeLayout) findViewById(R.id.rl_phone);
        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mEmailLayout = (RelativeLayout) findViewById(R.id.rl_email);


        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mProgressWheel.spin();

        Bundle bundle = getIntent().getExtras();

        mAuthorId = bundle.getString("authorid");
        mPhone = bundle.getString("phone");


        findViewById(R.id.rl_social).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = (String) view.getTag();
                if (null != url && !TextUtils.isEmpty(url)) {
                    PBSharingManager.openWebUrl(url, Profile.this);
                }
            }
        });

        tv_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Un follow the user
                unfollow_user();
                HashMap<String, Object> wrData = new HashMap<>();
                wrData.put("ScreenName", TAG);
                if (null != mAuthorId) {
                    wrData.put("UserId", mAuthorId);
                }
                wrData.put("Action", "Un Follow");


            }
        });

        tv_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<String, Object> wrData = new HashMap<>();
                wrData.put("ScreenName", TAG);
                if (null != mAuthorId) {
                    wrData.put("UserId", mAuthorId);
                }
                wrData.put("Action", "Follow");

                //follow the user
                follow_user();
            }
        });

        tv_shouts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //WizRocket tracking
                HashMap<String, Object> wrData = new HashMap<>();
                wrData.put("ScreenName", TAG);
                if (null != mAuthorId) {
                    wrData.put("UserId", mAuthorId);
                }
                wrData.put("Action", "View Others Shouts");


                Intent intent = new Intent(Profile.this, ShoutByOthers.class);
                intent.putExtra("contactId", mAuthorId);
                intent.putExtra("contactName", mUserName);

                startActivity(intent);


            }
        });

        mPhoneImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phoneNumber = (String) v.getTag();
                Utility.userStats(Profile.this, "call");


                HashMap<String, Object> wrData = new HashMap<>();
                wrData.put("ScreenName", TAG);
                if (null != mAuthorId) {
                    wrData.put("UserId", mAuthorId);
                }
                wrData.put("Action", "Call");
                wrData.put("callFrom", PBPreferences.getPhone());
                wrData.put("callTo", phoneNumber);


                if (phoneNumber != null && phoneNumber != "") {

                    ActivityUtils.callPhone(Profile.this, phoneNumber);

                } else {
                    Toast.makeText(Profile.this,
                            "Invalid Phone", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mChatImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ProfileClass profileClass = (ProfileClass) view.getTag();

                Utility.userStats(Profile.this, "im");

                HashMap<String, Object> dataIm = new HashMap<String, Object>();
                dataIm.put("imTo", profileClass.getPhone());
                dataIm.put("imFrom", PBPreferences.getPhone());


                HashMap<String, Object> wrData = new HashMap<>();
                wrData.put("ScreenName", TAG);
                if (null != mAuthorId) {
                    wrData.put("UserId", mAuthorId);
                }
                wrData.put("Action", "Chat");

                Intent intent = new Intent(Profile.this, XMPPChat.class);
                String num = profileClass.getPhone();
                String displayName = ActivityUtils.getDisplayNameFromPhone(num);
                if (displayName != null && !TextUtils.isEmpty(displayName)) {
                    intent.putExtra("name", displayName);

                } else {
                    intent.putExtra("name", profileClass.getName());

                }
                intent.putExtra("img", profileClass.getImg());
                intent.putExtra("last_seen", profileClass.getTime());

                intent.putExtra("phone_key", num);
                intent.putExtra("userid", profileClass.getAuthorid() + "");

                startActivity(intent);
                //startActivityForResult(intent, Constants.REVIEW_REQUEST_CODE);
            }
        });

        mEmailLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = (String) view.getTag();
                if (null != email && !TextUtils.isEmpty(email)) {
                    // send email
                    PBSharingManager.sendMail(Profile.this, email, "", "", "");
                    HashMap<String, Object> wrData = new HashMap<>();
                    wrData.put("ScreenName", TAG);
                    if (null != mAuthorId) {
                        wrData.put("UserId", mAuthorId);
                    }
                    wrData.put("Action", "Send Mail");

                }
            }
        });
        findViewById(R.id.rl_addr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String addr = (String) view.getTag();
                if (null != addr && !TextUtils.isEmpty(addr)) {
                    // open map
                    Uri geo = Uri.parse("geo:0,0?q=" + addr);
                    PBSharingManager.showMap(geo, Profile.this);
                    HashMap<String, Object> wrData = new HashMap<>();
                    wrData.put("ScreenName", TAG);
                    if (null != mAuthorId) {
                        wrData.put("UserId", mAuthorId);
                    }
                    wrData.put("Action", "View Address");

                }
            }
        });

        user_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<String> imageUrlList = new ArrayList<String>();
                if (profileClass == null || TextUtils.isEmpty(profileClass.getImg())) {
                    return;
                }

                HashMap<String, Object> wrData = new HashMap<>();
                wrData.put("ScreenName", TAG);
                if (null != mAuthorId) {
                    wrData.put("UserId", mAuthorId);
                }
                wrData.put("Action", "View Image");


                imageUrlList.add(profileClass.getImg());
                Intent intent = new Intent(Profile.this, ImageGallery.class);
                intent.putStringArrayListExtra("menus", imageUrlList);
                intent.putExtra("position", 0);
                startActivity(intent);


            }
        });

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }


    void getUSerProfile() {

        HashMap<String, String> params = new HashMap<String, String>();
        if (!TextUtils.isEmpty(mAuthorId) || null != mAuthorId) {
            params.put(PlaBroApi.PLABRO_GET_PROFILE_AUTO_PARAM_CONTACT_ID, mAuthorId);
        }
        if (!TextUtils.isEmpty(mPhone) || null != mPhone) {
            params.put(PlaBroApi.PLABRO_GET_PROFILE_AUTO_PARAM_PHONE, mPhone);
        }
        params = Utility.getInitialParams(PlaBroApi.RT.GETPROFILE, params);

        AppVolley.processRequest(Constants.TASK_CODES.USER_PROFILE, ProfileResponse.class, null, String.format(PlaBroApi.getBaseUrl(Profile.this)), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                if (response != null) {
                    mProgressWheel.stopSpinning();
                    ProfileResponse profileResponse = (ProfileResponse) response.getResponse();

                    if (profileResponse.isStatus()) {


                        profileClass = profileResponse.getOutput_params().getData();

                        profileClass.save();

                        sv_profile.setVisibility(View.VISIBLE);


                        mPhoneImage.setTag(profileClass.getPhone());
                        mChatImage.setTag(profileClass);
                        tv_profile_user_followers.setText(profileClass.getFollowed() + " Followers");
                        tv_profile_user_following.setText(profileClass.getFollowing() + " Following");
                        tv_profile_status.setText(profileClass.getStatus_msg());
                        mUserName = "";

                        String displayName = ActivityUtils.getDisplayNameFromPhone(profileClass.getPhone());
                        if (displayName != null && !TextUtils.isEmpty(displayName)) {
                            mUserName = displayName;
                        } else {

                            mUserName = profileClass.getName();
                            if (TextUtils.isEmpty(mUserName)) {
                                mUserName = profileClass.getPhone();
                            }

                        }
                        toolbar.setTitle(mUserName);

                        tv_last_active.setText("Last seen " + profileClass.getTime());

                        String imgUrl = profileClass.getImg();

//                        user_image.setDefaultImageResId(R.drawable.profile_default_one);
//                        user_image.setImage(imgUrl, AppController.getInstance().getImageLoader());
                        if (imgUrl != null) {
                            Picasso.with(Profile.this).load(imgUrl).placeholder(R.drawable.profile_default_one).into(user_image);
                        }

                        tv_no_profile.setVisibility(View.GONE);

                        if (profileClass.getIsFollowedByMe()) {
                            tv_following.setVisibility(View.VISIBLE);
                            tv_follow.setVisibility(View.GONE);

                        } else {
                            tv_following.setVisibility(View.GONE);
                            tv_follow.setVisibility(View.VISIBLE);

                        }

                        if (!profileClass.getPhone().equalsIgnoreCase("")) {
                            tv_user_phone.setText(profileClass.getPhone());
                        }

                        if (!profileClass.getEmail().equalsIgnoreCase("")) {
                            tv_user_mail.setText(profileClass.getEmail());
                            mEmailLayout.setTag(profileClass.getEmail());

                        }


                        if (!profileClass.getWebsite().equalsIgnoreCase("")) {
                            tv_user_social.setText(profileClass.getWebsite());
                            findViewById(R.id.rl_social).setTag(profileClass.getWebsite());
                        }

                        if (!profileClass.getAddress().equalsIgnoreCase("")) {
                            tv_user_addr.setText(profileClass.getAddress());
                            findViewById(R.id.rl_addr).setTag(profileClass.getAddress());
                        }

                        if (!profileClass.getBusiness_name().equalsIgnoreCase("")) {
                            tv_business.setText(profileClass.getBusiness_name());
                        }


                    } else {
                        sv_profile.setVisibility(View.GONE);
                        tv_no_profile.setVisibility(View.VISIBLE);

                        if (profileResponse.getMsg().contains("Session Expired") || profileResponse.getMsg().equalsIgnoreCase("Missing AuthKey in the request")) {
                            final MaterialDialog mMaterialDialog = new MaterialDialog(Profile.this);

                            mMaterialDialog.setTitle("Try again")
                                    .setMessage("Oops ! Your Session Expired.")
                                    .setPositiveButton("OK", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Utility.userLogin(Profile.this);
                                            getUSerProfile();
                                            mMaterialDialog.dismiss();
                                        }
                                    })

                                    .setNegativeButton("CANCEL", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mMaterialDialog.dismiss();
                                        }
                                    });

                            mMaterialDialog.show();
                        }
                    }
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

                sv_profile.setVisibility(View.GONE);
                mProgressWheel.stopSpinning();
                tv_no_profile.setVisibility(View.VISIBLE);


            }
        });


    }

    void follow_user() {
        final ProgressDialog progress = ProgressDialog.show(this, "Please wait",
                "Processing your request.", true);


        HashMap<String, String> params = new HashMap<String, String>();

        params.put(PlaBroApi.PLABRO_FOLLOW_UNFOLLOW_POST_PARAM_CONTACT_ID, mAuthorId);

        params = Utility.getInitialParams(PlaBroApi.RT.FOLLOW, params);


        AppVolley.processRequest(Constants.TASK_CODES.FOLLOW, FollowUnFollowResponse.class, null, String.format(PlaBroApi.getBaseUrl(Profile.this)), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);

                if (response != null) {


                    FollowUnFollowResponse followUnFollowResponse = (FollowUnFollowResponse) response.getResponse();

                    if (followUnFollowResponse.isStatus()) {


                        tv_following.setVisibility(View.VISIBLE);
                        tv_follow.setVisibility(View.GONE);


                    } else {
                        sv_profile.setVisibility(View.GONE);
                        tv_no_profile.setVisibility(View.VISIBLE);

                        if (followUnFollowResponse.getMsg().contains("Session Expired") || followUnFollowResponse.getMsg().equalsIgnoreCase("Missing AuthKey in the request")) {


                            final MaterialDialog mMaterialDialog = new MaterialDialog(Profile.this);

                            mMaterialDialog.setTitle("Try again")
                                    .setMessage("Oops ! Your Session Expired.")
                                    .setPositiveButton("OK", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Utility.userLogin(Profile.this);
                                            follow_user();
                                            mMaterialDialog.dismiss();
                                        }
                                    })

                                    .setNegativeButton("CANCEL", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mMaterialDialog.dismiss();
                                        }
                                    });

                            mMaterialDialog.show();
                        }
                    }
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

                Utility.dismissProgress(progress);

                mProgressWheel.stopSpinning();


            }
        });


    }


    void unfollow_user() {
        final ProgressDialog progress = ProgressDialog.show(this, "Please wait",
                "Processing your request.", true);


        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.PLABRO_FOLLOW_UNFOLLOW_POST_PARAM_CONTACT_ID, mAuthorId);
        params = Utility.getInitialParams(PlaBroApi.RT.UN_FOLLOW, params);

        AppVolley.processRequest(Constants.TASK_CODES.UNFOLLOW, FollowUnFollowResponse.class, null, String.format(PlaBroApi.getBaseUrl(Profile.this)), params, RequestMethod.POST, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);

                if (response != null) {

                    FollowUnFollowResponse followUnFollowResponse = (FollowUnFollowResponse) response.getResponse();

                    if (followUnFollowResponse.isStatus()) {


                        tv_following.setVisibility(View.GONE);
                        tv_follow.setVisibility(View.VISIBLE);


                    } else {
                        sv_profile.setVisibility(View.GONE);
                        tv_no_profile.setVisibility(View.VISIBLE);

                        if (followUnFollowResponse.getMsg().contains("Session Expired") || followUnFollowResponse.getMsg().equalsIgnoreCase("Missing AuthKey in the request")) {
                            showSessionExpiredDialog(false);
                        }
                    }
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

                Utility.dismissProgress(progress);

                mProgressWheel.stopSpinning();


            }
        });

    }


}
