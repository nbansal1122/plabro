package com.plabro.realestate.fragments.services;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.HashMap;

/**
 * Created by nitin on 11/12/15.
 */
public class ServiceInfoFragment extends PlabroBaseFragment {

    private ImageView serviceInfoImage;
    private TextView serviceSummaryText, serviceDetailText;
    private ServiceInfo serviceInfo;

    public static ServiceInfoFragment getInstance(ServiceInfo serviceInfo) {
        ServiceInfoFragment f = new ServiceInfoFragment();
        f.serviceInfo = serviceInfo;
        return f;
    }

    @Override
    public String getTAG() {
        return "ServiceInfoScreen";
    }

    @Override
    protected void findViewById() {
        serviceInfoImage = (ImageView) findView(R.id.iv_service_info);
        serviceSummaryText = (TextView) findView(R.id.tv_service_summary);
        serviceDetailText = (TextView) findView(R.id.tv_service_desc);
        ActivityUtils.loadImage(getActivity(), serviceInfo.imageUrl, serviceInfoImage, serviceInfo.defImageIcon);
        serviceSummaryText.setText(serviceInfo.summary);
        serviceDetailText.setText(serviceInfo.description);
        setText(R.id.tv_service_info, serviceInfo.serviceType);
        setTitle(serviceInfo.serviceType);
        setClickListeners(R.id.btn_req_callback);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btn_req_callback:
                reqCallback();
                break;
        }
    }

    private void reqCallback() {
        ParamObject obj = new ParamObject();
        HashMap<String, String> params = new HashMap<>();
        params.put("service_type", serviceInfo.serviceType + "");
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.REQUEST_CALLBACK, params));
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                showToast(response.getCustomException().getMessage());
            }
        });
    }

    @Override
    protected int getViewId() {
        return R.layout.service_info_screen;
    }

    public static class ServiceInfo {
        public String summary, description, imageUrl, serviceType;
        public int defImageIcon;

    }
}
