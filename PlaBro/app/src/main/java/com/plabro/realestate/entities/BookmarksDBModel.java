package com.plabro.realestate.entities;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.utils.Serializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xebia on 18-Apr-15.
 */
@Table(name = "Bookmarks")
public class BookmarksDBModel extends Model {


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public byte[] getFeedObject() {
        return feedObject;
    }

    public void setFeedObject(byte[] feedObject) {
        this.feedObject = feedObject;
    }

    @Column(name = BaseColumns._ID, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String _id;
    @Column
    private byte[] feedObject;

    public static void insertFeed(BaseFeed data) {
        if("shout".equals(data.getType())) {
            BookmarksDBModel model = new BookmarksDBModel();
            model._id = ((Feeds)data).get_id();
            try {
                model.feedObject = Serializer.serialize(data);

            } catch (Exception e) {
                Log.i("FeedsDBModel", "Exception while serializing data");
            }
            long i = model.save();
            Log.d("BookmarkSaving", "Is bookmark saved:"+i);
        }
    }

    public static boolean isFieldExist(String tableName, String fieldName)
    {
        boolean isExist = true;
        SQLiteDatabase db = ActiveAndroid.getDatabase();
        Cursor res = db.rawQuery("PRAGMA table_info("+tableName+")",null);
        int value = res.getColumnIndex(fieldName);

        if(value == -1)
        {
            isExist = false;
        }
        return isExist;
    }


    public static List<BaseFeed> getBookmarkedShouts(List<BookmarksDBModel> list) {
        List<BaseFeed> feeds = new ArrayList<BaseFeed>();

        if(list != null) {
            for (BookmarksDBModel model : list) {
                try {
                    Feeds feed = (Feeds) Serializer.deserialize(model.getFeedObject());
                    feeds.add(feed);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
        return feeds;
    }
}
