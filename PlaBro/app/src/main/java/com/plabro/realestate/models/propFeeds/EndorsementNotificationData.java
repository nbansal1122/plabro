package com.plabro.realestate.models.propFeeds;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 06/10/15.
 */
@Table(name = "EndorsementNotification", id = BaseColumns._ID)
public class EndorsementNotificationData extends Model {
    @Expose
    private String img_url;
    @Expose
    private long contact_id;

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }

    @Column
    private String jsonString;

    @Expose
    private List<String> keys = new ArrayList<String>();
    @Expose
    private String title;

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    @Expose
    private String profile_name;

    /**
     * @return The keys
     */
    public List<String> getKeys() {
        return keys;
    }

    /**
     * @param keys The keys
     */
    public void setKeys(List<String> keys) {
        this.keys = keys;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public long getContact_id() {
        return contact_id;
    }

    public void setContact_id(long contact_id) {
        this.contact_id = contact_id;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }
}
