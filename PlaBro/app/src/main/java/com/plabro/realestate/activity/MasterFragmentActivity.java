package com.plabro.realestate.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.plabro.realestate.R;
import com.plabro.realestate.listeners.MasterLifecycleInterface;
import com.plabro.realestate.widgets.others.AppMsg;

public abstract class MasterFragmentActivity extends PlabroBaseActivity implements MasterLifecycleInterface {

    static Animation animFadeIn, animFadeOut, animTopIn, animTopOut;

    protected void onCreate(Bundle savedInstanceState, int layoutResourceId) {
        super.onCreate(savedInstanceState);
        setContentView(layoutResourceId);
    }

    protected void loadDefaultData() {

        if (animFadeIn == null || animFadeOut == null || animTopIn == null || animTopOut == null) {
            animFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
            animFadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
            animTopIn = AnimationUtils.loadAnimation(this, R.anim.top_in);
            animTopOut = AnimationUtils.loadAnimation(this, R.anim.bottom_out);
        }

        findViewById();
        setViewSizes();
        callScreenDataRequest();

    }


    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {

        if (intent != null) {

            super.startActivityForResult(intent, requestCode);
        }
    }

    @Override
    public void finish() {
        super.finish();
    }

    public void finishWithReverseAnimation1() {
        super.finish();
    }

    public void showCroutonMsg(Activity activity, String text, AppMsg.Style style) {

        AppMsg appMsg = AppMsg.makeText(activity, text, style);

        appMsg.setAnimation(animTopIn, animTopOut);

        appMsg.show();
    }

    public void showCroutonMsg(Activity activity, String text) {
        showCroutonMsg(activity, text, AppMsg.STYLE_TC_ERROR);
    }

    public void showCroutonMsg(Activity activity, String text, int colorResId) {
        showCroutonMsg(activity, text, new AppMsg.Style(AppMsg.LENGTH_SHORT, colorResId));
    }

}
