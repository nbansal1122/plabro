package com.plabro.realestate.utilities;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import com.activeandroid.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;

public class PlabroLocationManagerTwo implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static PlabroLocationManagerTwo instance;
    private static Context ctx;
    private static final String TAG = "PlabroLoc";
    private static GoogleApiClient mGoogleApiClient;
    GoogleMap map;
    Location mLastLocation;
    LocationRequest mLocationRequest = new LocationRequest();
    boolean requestingLocationUpdate = true;

    private PlabroLocationManagerTwo(Context ctx) {
        this.ctx = ctx.getApplicationContext();
    }

    public static PlabroLocationManagerTwo getInstance(Context ctx) {
        if (instance == null) {
            instance = new PlabroLocationManagerTwo(ctx);
            instance.initClient();
            mGoogleApiClient.connect();//be connected with GoogleApiClient
        }
        return instance;
    }


    private synchronized void buildGoogleApiClient(Context ctx) {
        mGoogleApiClient = new GoogleApiClient.Builder(ctx)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

    }


    private synchronized void saveLastLocationToPrefs(Location location) {
        if (location != null) {
            Log.d(TAG, "Lat:" + location.getLatitude() + ", Long:" + location.getLongitude());
            PBPreferences.setLastKnownLat(String.valueOf(location.getLatitude()));
            PBPreferences.setLastKnownLong(String.valueOf(location.getLongitude()));
        }
    }


    public void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "onLocationChanged");
//        mLastLocation = location;
        updateLocationData(location);

    }

    public void startLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        } else {
            mGoogleApiClient.connect();
        }
    }


    protected void initClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(ctx)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        /*mLocationRequest.setInterval(Constants.WIFITIMER);
        mLocationRequest.setFastestInterval(Constants.WIFITIMER);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);*/
    }


    @Override
    public void onConnected(Bundle bundle) {
//        startLocationUpdates();
        // updateLocationData();
        Log.d(TAG, "OnConnected");
        getLastKnownLocation();
        /*if (requestingLocationUpdate) {

            startLocationUpdate();//trying to get the update location
        }*/
    }

    private void startLocationUpdate() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }


    public Location getLastKnownLocation() {
        Location lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (lastKnownLocation != null) {
            Log.d(TAG, lastKnownLocation.getLatitude() + ":" + lastKnownLocation.getLongitude());
        } else {
            Log.d(TAG, "Last Known Loca is Null");
        }
        return lastKnownLocation;
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "connection has been suspended");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.d(TAG, "not connected with GoogleApiClient");
    }


    private void stopLocationUpdate() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);//stopping the update location
    }

    void updateLocationData(Location currentLocation) {
        //mLastLocation = getLocation();
        if (mLastLocation != null) {
            float distanceFromPrevLocation = mLastLocation.distanceTo(currentLocation);
            if (distanceFromPrevLocation > Constants.WIFIMINDISTANCE) {
                long currentTimeStamp = System.currentTimeMillis();
                saveLastLocationToPrefs(currentLocation);
                PBPreferences.setLastKnownDistance(Math.round(distanceFromPrevLocation));
                PBPreferences.setLocationUpdateTimestamp(currentTimeStamp);
            }
        }
        mLastLocation = currentLocation;
    }

}
