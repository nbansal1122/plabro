package com.plabro.realestate.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.crashlytics.android.Crashlytics;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.FeedsCustomAdapter;
import com.plabro.realestate.adapter.FiltersAdapter;
import com.plabro.realestate.adapter.SuggestedSearchAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.HidingScrollListener;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.listeners.onSearchFilterSelectionListener;
import com.plabro.realestate.models.history.AutoSuggestion;
import com.plabro.realestate.models.history.History;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.FilterClass;
import com.plabro.realestate.models.propFeeds.LoaderFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.tourguide.Overlay;
import com.plabro.realestate.uiHelpers.tourguide.Pointer;
import com.plabro.realestate.uiHelpers.tourguide.ToolTip;
import com.plabro.realestate.uiHelpers.tourguide.TourGuide;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.ShowcaseView;
import com.plabro.realestate.widgets.dialogs.PBConditionalDialogView;
import com.software.shell.fab.ActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class SearchFeeds extends PlabroBaseActivity implements ShoutObserver.PBObserver {

    private FeedsCustomAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private ShowcaseView sv;
    private static final int REQUEST_CODE = 0000;
    public static final String TAG = "SearchActivity";
    private int hashKey;
    private ProgressWheel mProgressWheel;
    private LinearLayoutManager mLayoutManager;
    private RelativeLayout noFeeds;
    private RelativeLayout shout, scroll;
    private ActionButton mShoutButton;
    protected ActionButton mScrollButton;
    private View mOverlay;
    private boolean initalLaunch = true;
    private static Context mContext;
    private static Activity mActivity;
    private int page = 0;
    private boolean isLoading = false;
    private String searchText = "";
    private String selfilterJson;
    private RelativeLayout mHeaderLayout;
    private String currentSearchText = "";
    private boolean currentAddMore = false;
    private boolean currentIsVoiceSearch = false;
    private ArrayList<BaseFeed> baseFeeds = new ArrayList<BaseFeed>();
    private RelativeLayout mOptionHeader;
    private PBConditionalDialogView mConditionalView;
    private boolean isBuilderSearch = false;
    private ListView mSuggestionList, mFilterList;
    List<AutoSuggestion> searchResults = new ArrayList<>();
    private SuggestedSearchAdapter mSuggestionAdapter;
    private FiltersAdapter mFiltersAdapter;
    private RelativeLayout filtersLayout;
    AutoCompleteTextView mSearchTextView;
    ImageButton btnClearText, btnVoice, btnSearchNow, btnSearchFilter;
    ImageView mBack;
    List<History> histories = new ArrayList<>();
    List<String> historyArray = new ArrayList<>();
    private HashMap<String, String> selectedFilters = new HashMap<>();
    private boolean filtersOpened = false;
    private Button mApplyFilter, mClearFilter;
    List<FilterClass> filter_arr = new ArrayList<FilterClass>();
    List<FilterClass> filter_arr_backup = new ArrayList<FilterClass>();
    private Animation animSlideDown, animSlideUp;
    private FontText filterAppliedText;
    private int filterActiveIcon, filterDisableIcon;
    private RelativeLayout mFilterCountLayout;
    private FontText mFilterCount;
    private boolean mFilterApplyToolTipFlag = false;
    private Button filterToolTipFilterapply;
    private TourGuide mTourGuideHandlerFilterapply;
    private TourGuide mTourGuideHandlerToolTipFilterapply;

    @Override
    protected String getTag() {
        return "SearchFeeds";
    }


    @Override
    protected int getResourceId() {

        return R.layout.activity_search_feeds;
    }

    @Override
    public void inflateToolbar() {

        //setting action bar
        toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);


        // Set an OnMenuItemClickListener to handle menu item clicks
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Handle the menu item
                int id = item.getItemId();

                switch (id) {
                    case R.id.action_filter:
                        Intent i = new Intent(SearchFeeds.this, SearchFilter.class);
                        startActivityForResult(i, Constants.FILTER_REQ_CODE);
                        break;

                    case R.id.action_clear_history:
                        clearHistory();
                        break;

                }
                return true;
            }

        });

        // Inflate a menu to be displayed in the toolbar
        toolbar.inflateMenu(R.menu.menu_search_feeds);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PBPreferences.setPrevSearchQuerry(mSearchTextView.getText().toString());
                finish();


            }
        });


    }

    private void clearHistory() {
        new Delete().from(History.class).execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mActivity.getWindow().setSoftInputMode(
//                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
//        );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_feeds, menu);
        return true;
    }


    public void setRecyclerViewLayoutManager() {
        int scrollPosition = 0;

        mLayoutManager = new LinearLayoutManager(this);

        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }


    @Override
    public void findViewById() {
        isBuilderSearch = getIntent().getBooleanExtra(Constants.BUNDLE_KEYS.IS_BUILDER_SEARCH, false);
        // registerLocalReceiver();
        ShoutObserver.getInstance().addListener(this);
        SearchFeeds.mContext = this;
        SearchFeeds.mActivity = this;
        inflateToolbar();

        page = 0;
        isLoading = false;
        filterActiveIcon = R.drawable.ic_filter_active;
        filterDisableIcon = R.drawable.ic_filter;

        mHeaderLayout = (RelativeLayout) findViewById(R.id.ll_header_layout);
        mOptionHeader = (RelativeLayout) findViewById(R.id.options_header);

        animSlideDown = AnimationUtils.loadAnimation(this, R.anim.top_in_search);
        animSlideUp = AnimationUtils.loadAnimation(this, R.anim.top_out_search);

        mConditionalView = (PBConditionalDialogView) findViewById(R.id.pbConditionalView);
        filterAppliedText = (FontText) findViewById(R.id.ft_applied_filters);
        mFilterCount = (FontText) findViewById(R.id.ft_fl_counter);
        mFilterCountLayout = (RelativeLayout) findViewById(R.id.rl_fl_counter);

        mRecyclerView = (RecyclerView) findViewById(R.id.search_recyclerview);
        setRecyclerViewLayoutManager();

        mProgressWheel = (ProgressWheel) findViewById(R.id.search_progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);

        mApplyFilter = (Button) findViewById(R.id.btn_apply_filters);
        mClearFilter = (Button) findViewById(R.id.btn_clear_filters);
        mApplyFilter.setOnClickListener(MyOnClickListener);
        mClearFilter.setOnClickListener(MyOnClickListener);

        mOverlay = (View) findViewById(R.id.v_overlay_filter);
        mOverlay.setOnClickListener(MyOnClickListener);


        mSearchTextView = (AutoCompleteTextView) findViewById(R.id.et_search);
        //setThreshold(3);

        btnClearText = (ImageButton) findViewById(R.id.btn_search_text_cancel);
        btnVoice = (ImageButton) findViewById(R.id.btn_search_text_voice);
        btnSearchFilter = (ImageButton) findViewById(R.id.btn_search_text_filter);
        btnSearchFilter.setOnClickListener(MyOnClickListener);

        filtersLayout = (RelativeLayout) findViewById(R.id.rl_filters);
        mSuggestionList = (ListView) findViewById(R.id.lv_suggestions);
        mFilterList = (ListView) findViewById(R.id.lv_fl);
        mFilterList.setOnItemClickListener(null);
        mBack = (ImageView) findViewById(R.id.iv_back);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        noFeeds = (RelativeLayout) findViewById(R.id.noFeeds);

        shout = (RelativeLayout) findViewById(R.id.rl_compose_shout);
        scroll = (RelativeLayout) findViewById(R.id.rl_scroll);

        shout.findViewById(R.id.rl_post_iv).setOnClickListener(MyOnClickListener);
        mShoutButton = (ActionButton) shout.findViewById(R.id.rl_post_iv);
        Utility.setupShoutActionButton(mShoutButton, mContext);
        mShoutButton.setOnClickListener(MyOnClickListener);


        mScrollButton = (ActionButton) scroll.findViewById(R.id.rl_scroll_btn);
        Utility.setupShoutActionButton(mShoutButton, SearchFeeds.this);
        Utility.setupScrollActionButton(mScrollButton, SearchFeeds.this);

        mScrollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayoutManager.scrollToPositionWithOffset(0, 0);
                mScrollButton.playHideAnimation();
                mScrollButton.setVisibility(View.GONE);

            }
        });


        mRecyclerView.setOnScrollListener(hSL);

        if (baseFeeds.size() < 1) {
            noFeeds.setVisibility(View.VISIBLE);
        }


        String prevSearchQuerry = PBPreferences.getPrevSearchQuerry();
        if (TextUtils.isEmpty(prevSearchQuerry)) {
            mSearchTextView.requestFocus();
        } else {
            mSearchTextView.requestFocus();

//            mSearchTextView.clearFocus();
//            Util.hideKeyboard(mSearchTextView, this);
//            FilterClass flObj = new FilterClass();
//            mSearchTextView.setFocusable(false);
//            mSearchTextView.setFocusableInTouchMode(false);
//            mSearchTextView.setSearchText(prevSearchQuerry);
//            mSearchTextView.setFocusable(true);
//            mSearchTextView.setFocusableInTouchMode(true);
//
//            getData(prevSearchQuerry, currentAddMore, currentIsVoiceSearch, flObj, false);
        }
        // showVoiceShowcaseView();

        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();
        Log.d(TAG, data + "");
        if (null != data) {
            mSearchTextView.setText(data.getQuery());
        }

//        if (initalLaunch) {
//            Log.d(TAG, "keyboard open");
//            initalLaunch = false;
//            mActivity.getWindow().setSoftInputMode(
//                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
//            );
//
//        } else {
//            Log.d(TAG, "keyboard closed");
//
//            mActivity.getWindow().setSoftInputMode(
//                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
//            );
//
//        }


        //  mSearchTextView.fetchInitialHistory();

        isBuilderSearch = getIntent().getBooleanExtra(Constants.BUNDLE_KEYS.IS_BUILDER_SEARCH, false);
        if (isBuilderSearch) {
            getSupportActionBar().setTitle("Search Builders");
            mSearchTextView.setHint("Search Builders");

        } else {
            mSearchTextView.setHint("Search in Plabro");
            getSupportActionBar().setTitle("Search in Plabro");
        }

        btnClearText.setOnClickListener(MyOnClickListener);

        btnVoice.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (onSearchActionListener != null)
                    onSearchActionListener.onSearchViaVoice();

            }
        });

        mSearchTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if (b || filter_arr.size() < 1) {
                    getSearchHistory("");
                    if (TextUtils.isEmpty(mSearchTextView.getText().toString())) {
                        btnClearText.setVisibility(View.INVISIBLE);
                        //btnSearchNow.setVisibility(View.INVISIBLE);
                        btnVoice.setVisibility(View.VISIBLE);

                    } else {

                        if (onSearchActionListener != null)
                            onSearchActionListener.onSearchTypo();
                        btnClearText.setVisibility(View.VISIBLE);
                        //btnSearchNow.setVisibility(View.VISIBLE);
                        btnVoice.setVisibility(View.GONE);
                    }
                    btnSearchFilter.setVisibility(View.INVISIBLE);
                    filtersLayout.setVisibility(View.GONE);
                    //filtersLayout.startAnimation(animSlideUp);
                    mSuggestionList.setVisibility(View.VISIBLE);
                    mOverlay.setVisibility(View.VISIBLE);
                    mFilterCountLayout.setVisibility(View.GONE);
                    filterAppliedText.setVisibility(View.GONE);
                    mSearchTextView.setPadding(16, 10, 50, 16);


                } else if (filter_arr.size() > 0) {
                    mSuggestionList.setVisibility(View.GONE);
                    btnClearText.setVisibility(View.INVISIBLE);
                    btnVoice.setVisibility(View.INVISIBLE);
                    btnSearchFilter.setVisibility(View.VISIBLE);
                    // filtersLayout.setVisibility(View.VISIBLE);
                    mSearchTextView.setPadding(16, 10, 50, 16);
                    if (selectedFilters.size() > 0) {
                        mFilterCountLayout.setVisibility(View.VISIBLE);
                        filterAppliedText.setVisibility(View.VISIBLE);
                        mSearchTextView.setPadding(16, 10, 50, 42);

                    }

                }


            }
        });
        mSearchTextView.addTextChangedListener(watcher);


        mSearchTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                filter_arr.clear();
                mOverlay.setVisibility(View.GONE);

                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (onSearchActionListener != null) {
                        //add to history
                        String searchText = v.getText().toString();
                        insertHistory(searchText);
                        onSearchActionListener.onSearchCalled(v.getText().toString());
                    }
                    clearData();
                    mSearchTextView.dismissDropDown();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    return true;
                }

                return false;
            }
        });

        searchText = getIntent().getStringExtra("searchText");
        initiateSearch(searchText);
        setAdapter();
    }

    TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            clearData();
            if (mSuggestionAdapter != null) {
                mSuggestionAdapter.notifyDataSetChanged();
            }
            if (s.toString().length() > 1) {
                getSearchHistory(s.toString());
            }


        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {

            if (TextUtils.isEmpty(s.toString())) {
                btnClearText.setVisibility(View.INVISIBLE);
                //btnSearchNow.setVisibility(View.INVISIBLE);
                btnVoice.setVisibility(View.VISIBLE);
                if (selectedFilters != null) {
                    selectedFilters.clear();
                }
            } else {

                if (onSearchActionListener != null)
                    onSearchActionListener.onSearchTypo();
                btnClearText.setVisibility(View.VISIBLE);
                //btnSearchNow.setVisibility(View.VISIBLE);
                btnVoice.setVisibility(View.GONE);
            }


            //  String text = searchText.getText().toString().toLowerCase(Locale.getDefault());
            // adapter.filter(text);
            // searchText.setSelection(searchText.getText().toString().length());


        }
    };

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {


    }

    @Override
    public void reloadRequest() {
        if (currentSearchText != null && !TextUtils.isEmpty(currentSearchText.trim())) {
            // Request data from the network.
            getData(currentSearchText, currentAddMore, currentIsVoiceSearch);
        }
    }

    OnSearchActionListener onSearchActionListener = new OnSearchActionListener() {

        @Override
        public void onSearchClearText() {
            clearSearch();
            noFeeds.setVisibility(View.VISIBLE);
        }

        @Override
        public void onSearchCalled(String searchString) {
            page = 0;
            searchText = searchString;
            if (searchString != null && !TextUtils.isEmpty(searchString.trim())) {
                getData(searchString, false, false);
            }
        }

        @Override
        public void onSearchTypo() {

        }

        @Override
        public void onSearchViaVoice() {
            startVoiceRecognitionActivity();
        }
    };


    private void startVoiceRecognitionActivity() {

        if (sv != null)
            sv.hide();
        sendScreenName(R.string.funnel_VOICE_BUTTON_CLICKED);
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak to search on PlaBro");
        try {
            startActivityForResult(intent, REQUEST_CODE);
        } catch (android.content.ActivityNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }
    }

    private void enableSearchField(boolean isEnable) {
        mSearchTextView.setEnabled(isEnable);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            mSearchTextView.setText(matches.get(0));
            searchText = matches.get(0);
            page = 0;
            if (searchText != null && !TextUtils.isEmpty(searchText.trim())) {
                getData(searchText, false, true);
            }
            return;
        }
//        if (requestCode == Constants.FILTER_REQ_CODE && resultCode == RESULT_OK) {
//            filterJson = data.getStringExtra(Constants.BUNDLE_KEY_FILTER_JSON);
//            Log.d(TAG, filterJson + "");
//            searchText = mSearchTextView.getText().toString();
//            if (filterJson == null || filterJson.equalsIgnoreCase("")) {
//                toolbar.getMenu().getItem(0).setIcon(getResources().getDrawable(clearedFilterDrawableId));
//            } else {
//                toolbar.getMenu().getItem(0).setIcon(getResources().getDrawable(appliedFilterDrawableId));
//
//            }
//            if (searchText != null && !TextUtils.isEmpty(searchText.trim())) {
//                getData(searchText, false, true, new FilterClass(), false);
//            }
//
//            return;
//        }


    }

    public void getData(final String searchText, final boolean addMore, final boolean isVoiceSearch) {

        noFeeds.setVisibility(View.GONE);

        currentSearchText = searchText;
        currentAddMore = addMore;
        currentIsVoiceSearch = isVoiceSearch;

        if (null != searchText && TextUtils.isEmpty(searchText.trim())) {
            Log.d(TAG, "Search String is empty or null!");
            Util.showSnackBarMessage(SearchFeeds.this, SearchFeeds.this, "Please enter something to search");
            //Toast.makeText(SearchFeeds.this, , Toast.LENGTH_LONG).show();
            // noFeeds.setVisibility(View.VISIBLE);
            return;
        }

        String filteredText = "";
        JSONObject self = new JSONObject();
        Set<String> keys = selectedFilters.keySet();
        for (String key : keys) {
            try {
                JSONArray selval = new JSONArray();
                selval.put(selectedFilters.get(key));
                if (TextUtils.isEmpty(filteredText)) {
                    filteredText = selectedFilters.get(key);
                } else {
                    filteredText = filteredText + " + " + selectedFilters.get(key);
                }
                self.put(key, selval);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        selfilterJson = self.toString();
        Log.v(TAG, selfilterJson);

        String length = "" + searchText.length();
        HashMap<String, Object> data = new HashMap<>();
        data.put("ScreenName", getTag());
        data.put("SearchLength", length);


        this.searchText = searchText;
        isLoading = true;
        if (addMore) {
            mConditionalView.setVisibility(View.GONE);

        } else {
            mConditionalView.setVisibility(View.GONE);
            mProgressWheel.spin();
        }


        if (isVoiceSearch) {
            sendScreenName(R.string.funnel_VOICE_SEARCH_QUERY);
            HashMap<String, Object> wrData = new HashMap<>();
            wrData.put("ScreenName", getTag());
            wrData.put("SearchType", "Voice");

        } else {
            sendScreenName(R.string.funnel_SEARCH_QUERY);
            HashMap<String, Object> wrData = new HashMap<>();
            wrData.put("ScreenName", getTag());
            wrData.put("SearchType", "Text");

        }

        HashMap<String, String> params = new HashMap<String, String>();


        if (selectedFilters.size() > 0 && selfilterJson != null && !TextUtils.isEmpty(selfilterJson)) {
            params.put(PlaBroApi.PLABRO_SEARCH_POST_PARAM_SEL_FILTER_JSON, selfilterJson);
            filterAppliedText.setVisibility(View.VISIBLE);
            filterAppliedText.setText(filteredText);
            btnSearchFilter.setImageResource(filterActiveIcon);
            mFilterCountLayout.setVisibility(View.VISIBLE);
            mFilterCount.setText(selectedFilters.size() + "");
            mSearchTextView.setPadding(16, 10, 50, 42);


        } else {
            mFilterCountLayout.setVisibility(View.GONE);
            filterAppliedText.setVisibility(View.GONE);
            filterAppliedText.setText("");
            btnSearchFilter.setImageResource(filterDisableIcon);
            mSearchTextView.setPadding(16, 10, 50, 16);

        }


        if (!searchText.trim().equalsIgnoreCase("")) {
            params.put(PlaBroApi.PLABRO_SEARCH_POST_PARAM_Q_STR, searchText);
        }


        if (!addMore) {
            page = 0;
        }
        params.put("startidx", (page) + "");


        if (isBuilderSearch) {
            params = Utility.getInitialParams(PlaBroApi.RT.BUILDER_SHOUTS_SEARCH, params);
        } else {
            params = Utility.getInitialParams(PlaBroApi.RT.SEARCH, params);
        }

        //AppController.getInstance().cancelPendingRequests("SearchFeeds");

        AppVolley.processRequest(0, FeedResponse.class, null, String.format(PlaBroApi.getBaseUrl(SearchFeeds.this)), params, RequestMethod.GET, new VolleyListener() {


            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                FeedResponse feedResponse = (FeedResponse) response.getResponse();
                ArrayList<BaseFeed> tempFeeds = (ArrayList<BaseFeed>) feedResponse.getOutput_params().getData();

                if (!addMore) {
                    if (null == filter_arr || filter_arr.size() < 1) {
                        filter_arr = feedResponse.getOutput_params().getFilter_arr();
                        if (filter_arr.size() > 0) {
                            filter_arr_backup = feedResponse.getOutput_params().getFilter_arr();
                            mFiltersAdapter = new FiltersAdapter(SearchFeeds.this, filter_arr, filterSelectionListener, selectedFilters);
                            mFiltersAdapter.notifyDataSetChanged();
                            initiateFilterTooltip();
                        }
                    }
                }

                //  mSearchTextView.disableTextChangeListener();
                mSearchTextView.setFocusable(false);
                mSearchTextView.setFocusableInTouchMode(false);


                mSearchTextView.setFocusable(true);
                mSearchTextView.setFocusableInTouchMode(true);

                int th = 0;//toolbar height if u want it
                //int tmargin = 100;
                int ts = mHeaderLayout.getHeight();
                mRecyclerView.setPadding(0, th + ts, 0, 0);


                LoaderFeed lf = new LoaderFeed();
                if (tempFeeds.size() < Util.getDefaultStartIdx()) {
                    lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                } else {
                    lf.setType(Constants.FEED_TYPE.PROGRESS_MORE);
                }

                if (null != tempFeeds && tempFeeds.size() > 0) {


                    if (isVoiceSearch) {
                        sendScreenName(R.string.goal_VOICE_SEARCH_RESULT);
                    } else {
                        sendScreenName(R.string.goal_SEARCH_RESULT);
                    }
                    if (addMore) {
                        baseFeeds.remove(baseFeeds.size() - 1);//for footer
                        baseFeeds.addAll(tempFeeds);
                        baseFeeds.add(lf);
                        mAdapter.notifyDataSetChanged();
                        // mAdapter = new FeedsCustomAdapter(baseFeeds, "search", SearchFeeds.this, hashKey);
                        //mRecyclerView.setAdapter(mAdapter);
                    } else {
                        page = 0;
                        baseFeeds.clear();
                        baseFeeds.addAll(tempFeeds);
                        baseFeeds.add(lf);
                        if (mAdapter != null) {
                            mAdapter.notifyDataSetChanged();
                            // mAdapter.addItems(baseFeeds);
                        }

                    }
                    page = Util.getStartIndex(page);
                    noFeeds.setVisibility(View.GONE);

                } else {

                    if (!addMore) {
                        baseFeeds.clear();
                        mAdapter = new FeedsCustomAdapter(baseFeeds, "search", SearchFeeds.this, hashKey);
                        mRecyclerView.setAdapter(mAdapter);
                    }

                    if (addMore) {
                        LoaderFeed lfn = new LoaderFeed();
                        baseFeeds.remove(baseFeeds.size() - 1);//for footer
                        lfn.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                        baseFeeds.add(lfn);
                        mAdapter.notifyDataSetChanged();
                    }


                    //Toast.makeText(SearchFeeds.this, "Nothing found for this search", Toast.LENGTH_LONG).show();
                }
//                if (mAdapter != null) {
//                    page = Utility.getPageIndex(mAdapter.getItemCount());
//                }

                HashMap<String, Object> data = new HashMap<>();
                data.put("ScreenName", getTag());
                data.put("Action", "Manual");

                mProgressWheel.stopSpinning();
                isLoading = false;
                // Log.i("PlaBro", "Feeds : " + feeds2.size());


                setAndCheckNoDataLayout();

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                setAndCheckNoDataLayout();
                isLoading = false;
                mProgressWheel.stopSpinning();
                onApiFailure(response, taskCode);
                try {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 2000ms
                            LoaderFeed lf = new LoaderFeed();
                            lf.setType(Constants.FEED_TYPE.NO_MORE);

                            if (addMore) {
                                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                                baseFeeds.add(lf);
                                mAdapter.notifyDataSetChanged();

                            }
                        }
                    }, 2000);

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
    }


    @Override
    public void onInternetException(PBResponse response, int taskCode) {
        super.onInternetException(response, taskCode);
        if (baseFeeds.size() < 1) {
            showConditionalMessage(PBConditionalDialogView.ConditionalDialog.INTERNET_CONNECTION, true, false);
            mConditionalView.setVisibility(View.VISIBLE);
            setAndCheckNoDataLayout();

        }
    }

    @Override
    public void onSessionExpiry(PBResponse response, int taskCode) {
        super.onSessionExpiry(response, taskCode);
        showConditionalMessage(PBConditionalDialogView.ConditionalDialog.SESSION_EXPIRED, true, false);
        mConditionalView.setVisibility(View.VISIBLE);
        setAndCheckNoDataLayout();


    }

    private void setAndCheckNoDataLayout() {
        if (null != mAdapter) {
            if (mAdapter.getItemCount() == 0) {
                noFeeds.setVisibility(View.VISIBLE);
                filtersLayout.setVisibility(View.GONE);
                mOverlay.setVisibility(View.GONE);
                mSuggestionList.setVisibility(View.GONE);
            } else {
                noFeeds.setVisibility(View.GONE);
            }
        } else {
            noFeeds.setVisibility(View.GONE);
        }
    }


    private void clearSearch() {
        if (mRecyclerView.getAdapter() != null) {
            mRecyclerView.setAdapter(null);
        }
        baseFeeds.clear();
        mAdapter.notifyDataSetChanged();
        setAdapter();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // unRegsiterLocalReceiver();
        ShoutObserver.getInstance().remove(this);
    }


    @Override
    public void onBackPressed() {
        if (filtersOpened || searchResults.size() > 0) {
            hidefilters();
            searchResults.clear();
            mSuggestionAdapter.notifyDataSetChanged();
            mSearchTextView.clearFocus();
        } else {
            //  PBPreferences.setPrevSearchQuerry(mSearchTextView.getText().toString());
            super.onBackPressed();
        }
    }

    private void hidefilters() {
        filtersLayout.setVisibility(View.GONE);
        //filtersLayout.startAnimation(animSlideUp);
        mOverlay.setVisibility(View.GONE);
        filtersOpened = false;
        setAndCheckNoDataLayout();
    }

    private void showfilters() {
        setupFilters();
        if (filter_arr.size() > 0) {
            filtersOpened = true;
            filtersLayout.setVisibility(View.VISIBLE);
            //  filtersLayout.startAnimation(animSlideDown);

            noFeeds.setVisibility(View.GONE);

            mOverlay.setVisibility(View.VISIBLE);

        } else {
            Toast.makeText(SearchFeeds.this, "No filters available", Toast.LENGTH_SHORT).show();
        }
    }


    boolean checkIfLayoutOnTop() {
        try {
            if (mLayoutManager.findViewByPosition(mLayoutManager.findFirstVisibleItemPosition()).getTop() == 0 && mLayoutManager.findFirstVisibleItemPosition() == 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    HidingScrollListener hSL = new HidingScrollListener(300) {
        @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
        @Override
        public void onHide(RecyclerView recyclerView, int dx, int dy) {
            mHeaderLayout.animate().translationY(-mHeaderLayout.getHeight()).setInterpolator(new AccelerateInterpolator(2));
            // toggle scroll icon here
            hSL.setHidingScrollListener(100);
            mScrollButton.playHideAnimation();
            mScrollButton.setVisibility(View.GONE);
            mScrollButton.setAnimation(animFadeOut);
        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
        @Override
        public void onShow(RecyclerView recyclerView, int dx, int dy) {
            mHeaderLayout.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
            // toggle scroll icon here

            hSL.setHidingScrollListener(800);
            mScrollButton.playShowAnimation();
            mScrollButton.setVisibility(View.VISIBLE);


        }

        @Override
        public void onScrolledFire(RecyclerView recyclerView, int dx, int dy) {

            int visibleItemCount = mLayoutManager.getChildCount();
            int totalItemCount = mLayoutManager.getItemCount();
            int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();

            if (pastVisibleItems == 0) {
                // toggle scroll icon here
            }

            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                //reached bottom show loading
                // mProgressWheelLoadMore.setVisibility(View.VISIBLE);

            } else {
                //mProgressWheelLoadMore.setVisibility(View.GONE);

            }

            if (!isLoading) {
                if (totalItemCount > Constants.SCROLL_ITEM_COUNT) {

                    if (totalItemCount - Constants.SCROLL_ITEM_COUNT > 0) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount - Constants.SCROLL_ITEM_COUNT) {
                            getData(searchText, true, false);
                        }
                    }
                }
            }

            if (checkIfLayoutOnTop()) {
                mHeaderLayout.setAnimation(animTopInSlow);
                mHeaderLayout.setVisibility(View.VISIBLE);
            }

            if (mLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                //Its at top ..
                mScrollButton.playHideAnimation();
                mScrollButton.setVisibility(View.GONE);

            }
        }
    };


    View.OnClickListener MyOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.rl_post_iv:
                    openShoutActivity();
                    break;
                case R.id.btn_apply_filters:
                    if (mFilterApplyToolTipFlag) {
                        dismissFilterApplyTooltip();
                    } else {
                        hidefilters();
//                        Analytics.trackScreen(Analytics.Search.FiltersApplied);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("filters", selfilterJson);
                        Analytics.trackInteractionEvent(Analytics.Actions.FILTERS, map);
                        getData(searchText, false, false);
                    }
                    break;
                case R.id.btn_clear_filters:
                    hidefilters();
                    selectedFilters.clear();
                    filter_arr.clear();
                    filter_arr.addAll(filter_arr_backup);
                    getData(searchText, false, false);
                    break;
                case R.id.v_overlay_filter:
                    hidefilters();
                    searchResults.clear();
                    mSuggestionAdapter.notifyDataSetChanged();
                    mSearchTextView.clearFocus();
                    break;
                case R.id.btn_search_text_filter:
                    if (!filtersOpened) {
                        showfilters();
                        if (filter_arr.size() > 0) {
                            initiateFilterSelectTooltip();
                        }
                    } else {
                        hidefilters();
                    }

                    break;

                case R.id.btn_search_text_cancel:
                    mSearchTextView.setText("");

                    if (onSearchActionListener != null) {
                        onSearchActionListener.onSearchClearText();
                    }

                    mScrollButton.setVisibility(View.GONE);
                    mOverlay.setVisibility(View.GONE);
                    break;


            }

        }
    };

    private void openShoutActivity() {
        String searchQueryText = mSearchTextView.getText().toString();

        if (!searchQueryText.equalsIgnoreCase("") && searchQueryText != null) {
            HashMap<String, Object> wrData = new HashMap<>();
            wrData.put("ScreenName", getTag());

            JSONArray filterJsonArray = null;

            String postString = searchQueryText + " ";

            //todo implement it after filer fixed
           /* for (int fct = 0; fct < filtersAppliedList.size(); fct++) {
                if (fct == filtersAppliedList.size() - 1) {

                    postString = postString + filtersAppliedList.get(fct);

                } else {

                    postString = postString + filtersAppliedList.get(fct) + " // ";
                }
            }

            if (filtersAppliedList.size() > 0) {
                postString = postString + "\n" + searchQueryText + " ";
            } else {
                postString = searchQueryText + " ";
            }*/


            Intent intent = new Intent(mContext, Shout.class);
            intent.putExtra("searchText", postString);
            intent.putExtra("searchFlag", true);

            startActivity(intent);
            ((Activity) mContext).overridePendingTransition(R.anim.bottom_in_fast, R.anim.bottom_out_fast);


        } else {

            Intent intent = new Intent(mContext, Shout.class);
            intent.putExtra("searchText", "");
            intent.putExtra("searchFlag", true);
            startActivity(intent);
            ((Activity) mContext).overridePendingTransition(R.anim.bottom_in_fast, R.anim.bottom_out_fast);


        }
    }

    void getAutoSuggestResults(String searchText) {
        //todo add response from elastic search server
    }

    private void getSearchHistory(String searchText) {
        clearData();

        try {
            if (TextUtils.isEmpty(searchText)) {

                histories = new Select().from(History.class).orderBy("_id DESC LIMIT 5").execute();

            } else {
                histories = new Select().from(History.class).where("text like '%" + searchText + "%'").orderBy("text").execute();
            }
            for (int x = 0; x < histories.size(); x++) {
                String val = histories.get(x).getText();
                AutoSuggestion autoSuggestion = new AutoSuggestion();
                autoSuggestion.set_id(x);
                autoSuggestion.setText(val);
                autoSuggestion.setIcon(getResources().getDrawable(
                        R.drawable.ic_history));
                searchResults.add(autoSuggestion);
                historyArray.add(val);
            }

            //add history clear option
            String val = "Clear history";
            AutoSuggestion autoSuggestion = new AutoSuggestion();
            autoSuggestion.set_id(-1);
            autoSuggestion.setText(val);
            autoSuggestion.setIcon(getResources().getDrawable(
                    R.drawable.ic_history));
            searchResults.add(autoSuggestion);
            historyArray.add(val);


            mSuggestionAdapter = null;
            mSuggestionAdapter = new SuggestedSearchAdapter(SearchFeeds.this, searchResults);

            mSuggestionList.setAdapter(mSuggestionAdapter);
            mSuggestionList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    mOverlay.setVisibility(View.GONE);
                    filter_arr.clear();
                    mFilterCountLayout.setVisibility(View.GONE);
                    filterAppliedText.setVisibility(View.GONE);
                    mSearchTextView.setPadding(16, 10, 50, 16);
                    btnSearchFilter.setImageResource(filterDisableIcon);
                    selectedFilters.clear();

                    AutoSuggestion data = searchResults.get(position);
                    String searchText = data.getText();

                    if (searchText.equalsIgnoreCase("Clear history")) {
                        clearHistory();
                        clearData();

                    } else {
//                        Analytics.trackScreen(Analytics.Search.HistoryQuerySelected);
                        initiateSearch(searchText);

                    }
                }
            });

            if (histories.size() > 0) {
                noFeeds.setVisibility(View.GONE);
            } else {
                if (baseFeeds.size() < 1) {
                    noFeeds.setVisibility(View.VISIBLE);

                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initiateSearch(String searchText) {
        this.searchText = searchText;
        if (!TextUtils.isEmpty(searchText)) {
            mSearchTextView.setText(this.searchText);
            mSearchTextView.setSelection(this.searchText.length());
        }
        if (onSearchActionListener != null) {
            //add to history

            insertHistory(this.searchText);
            onSearchActionListener.onSearchCalled(this.searchText);
        }
        clearData();
        mSearchTextView.dismissDropDown();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mSearchTextView.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);


    }

    private void clearData() {
        searchResults.clear();
        mSuggestionAdapter = new SuggestedSearchAdapter(SearchFeeds.this, searchResults);
        mSuggestionList.setAdapter(mSuggestionAdapter);
        mSuggestionAdapter.notifyDataSetChanged();
    }

    private void insertHistory(String searchTerm) {

        if (!historyArray.contains(searchTerm) && !TextUtils.isEmpty(searchTerm)) {
            History history = new History();
            history.setText(searchTerm);
            history.save();
        }
    }

    @Override
    public void onShoutUpdate() {

    }

    @Override
    public void onShoutUpdateGen(String type) {
        Log.d(TAG, "OnShout load more: " + type + currentAddMore);
        if (type.equalsIgnoreCase(Constants.LOAD_MORE.SHOUT)) {
            if (currentAddMore) {
                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                LoaderFeed lf = new LoaderFeed();
                lf.setType(Constants.FEED_TYPE.PROGRESS_MORE);
                baseFeeds.add(lf);
                mAdapter.notifyDataSetChanged();
            }


            getData(searchText, currentAddMore, false);

        }
    }

    public interface OnSearchActionListener {
        public void onSearchCalled(String searchString);

        public void onSearchClearText();

        public void onSearchViaVoice();

        public void onSearchTypo();

    }

    private void setAdapter() {
        mAdapter = new FeedsCustomAdapter(baseFeeds, "search", SearchFeeds.this, hashKey);
        mRecyclerView.setAdapter(mAdapter);

    }

    void setupFilters() {
        mFilterList.setAdapter(mFiltersAdapter);
        mFiltersAdapter.notifyDataSetChanged();

//        filterNameArray.clear();
//        for (FilterClass fc : filter_arr) {
//
//            FeedsFilter ff = new FeedsFilter();
//            ff.setKey(fc.getKey());
//            ff.setTitle(fc.getTitle());
//            ArrayList<String> ffVal = new ArrayList<String>();
//            if (fc.getValues().size() > 1) {
//                ffVal.add(fc.getValues().get(0).getDisplayname());
//                ffVal.add(fc.getValues().get(1).getDisplayname());
//            }
//            ff.setIsFirstSelected(fc.isFirstSelected());
//            ff.setIsSecondSelected(fc.isSecondSelected());
//            ff.setValues(ffVal);
//            filterNameArray.add(ff);
//
//        }
//
//
//
//        mFiltersAdapter.notifyDataSetChanged();


    }

    onSearchFilterSelectionListener filterSelectionListener = new onSearchFilterSelectionListener() {

        @Override
        public void onSelected(Boolean response, FilterClass ff) {

            if (response && ff.getValues().size() > 1) {
                if (ff.isFirstSelected()) {
                    selectedFilters.put(ff.getKey(), ff.getValues().get(0).getValue()); //first option selected
                } else if (ff.isSecondSelected()) {
                    selectedFilters.put(ff.getKey(), ff.getValues().get(1).getValue());//second option selected

                } else {
                    if (selectedFilters.containsKey(ff.getKey())) {
                        selectedFilters.remove((ff.getKey()));//no option selected
                    }
                }
            }

            List<FilterClass> mDataTmp = new ArrayList<>();
            for (FilterClass fct : filter_arr) {
                if (fct.getKey().equalsIgnoreCase(ff.getKey())) {
                    mDataTmp.add(ff);

                } else {
                    mDataTmp.add(fct);

                }


//                Set<String> keys = selectedFilters.keySet();
//                for (String key : keys) {
//                    if (key.equalsIgnoreCase(fct.getKey())) {
//                        if (selectedFilters.get(key).equalsIgnoreCase(fct.getValues().get(0).getDisplayname())) {
//                            fct.setIsFirstSelected(true);
//
//                        }
//
//                        if (selectedFilters.get(key).equalsIgnoreCase(fct.getValues().get(1).getDisplayname())) {
//                            fct.setIsSecondSelected(true);
//                        }
//                    }
//                }
            }
            filter_arr.clear();
            filter_arr.addAll(mDataTmp);
            mDataTmp.clear();
        }
    };


    private void initiateFilterTooltip() {

        Set<String> indexSet = PBPreferences.getPBToolTipSet();
        boolean searchTooltipShown = false;

        for (String s : indexSet) {

            if (s.equalsIgnoreCase(Constants.ToolTip.FILTERICON)) {
                searchTooltipShown = true;
            }
        }

        if (!searchTooltipShown) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 300ms
                    setupFilterTooltip();

                }
            }, 300);
        }

    }

    private void setupFilterTooltip() {


        final Button filterPlaceHolder = (Button) findViewById(R.id.filterPlaceHolder);
        final Button filterToolTip = (Button) findViewById(R.id.filterToolTip);


        int oColor = getResources().getColor(R.color.pbCoachMark);
        Overlay overlay = Utility.getOverLay(SearchFeeds.this, oColor);
        Pointer pointer = Utility.getPointer(SearchFeeds.this);
        String title = getResources().getString(R.string.filter_tooltip_msg_one);
        String msg = "";
        filterPlaceHolder.setVisibility(View.VISIBLE);
        filterToolTip.setVisibility(View.VISIBLE);

        ToolTip toolTip = Utility.getTooltip(SearchFeeds.this, title, msg, Gravity.RIGHT, Gravity.BOTTOM);

        final TourGuide mTourGuideHandler = TourGuide.init(this).with(TourGuide.Technique.Click)
                .setPointer(pointer)
                .setOverlay(overlay)
                .playOn(filterPlaceHolder);

        final TourGuide mTourGuideHandlerToolTip = TourGuide.init(this).with(TourGuide.Technique.Click)
                .setToolTip(toolTip)
                .playOn(filterToolTip);

        overlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTourGuideHandler.cleanUp();
                mTourGuideHandlerToolTip.cleanUp();
                filterPlaceHolder.setVisibility(View.GONE);
                filterToolTip.setVisibility(View.GONE);

                Set<String> indexSet = PBPreferences.getPBToolTipSet();
                indexSet.add(Constants.ToolTip.FILTERICON);
                PBPreferences.setPBToolTipSet(indexSet);
            }
        });


        filterPlaceHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTourGuideHandler.cleanUp();
                mTourGuideHandlerToolTip.cleanUp();
                filterPlaceHolder.setVisibility(View.GONE);
                filterToolTip.setVisibility(View.GONE);

                Set<String> indexSet = PBPreferences.getPBToolTipSet();
                indexSet.add(Constants.ToolTip.FILTERICON);
                PBPreferences.setPBToolTipSet(indexSet);
            }
        });


    }


    void initiateFilterSelectTooltip() {
        if (filter_arr.size() > 0) {
            Set<String> indexSet = PBPreferences.getPBToolTipSet();
            boolean searchTooltipShown = false;

            for (String s : indexSet) {

                if (s.equalsIgnoreCase(Constants.ToolTip.SELECTINGFILTER)) {
                    searchTooltipShown = true;
                }
            }

            if (!searchTooltipShown) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 300ms
                        try {
                            setupFilterSelectTooltip();
                        }catch (OutOfMemoryError e){
                            Crashlytics.logException(e);
                        }catch (Exception e){
                            Crashlytics.logException(e);
                        }

                    }
                }, 300);
            }
        }
    }

    private void setupFilterSelectTooltip() throws OutOfMemoryError, Exception{

        final Button filterSelectPlaceHolder = (Button) findViewById(R.id.filterSelectPlaceHolder);
        final Button filterToolTip = (Button) findViewById(R.id.filterSelectToolTip);


        int oColor = getResources().getColor(R.color.pbCoachMark);
        Overlay overlay = Utility.getOverLay(SearchFeeds.this, oColor);
        Pointer pointer = Utility.getPointer(SearchFeeds.this);
        String title = getResources().getString(R.string.filter_select_tooltip_msg_one);
        String msg = "";
        filterSelectPlaceHolder.setVisibility(View.VISIBLE);
        filterToolTip.setVisibility(View.VISIBLE);

        ToolTip toolTip = Utility.getTooltip(SearchFeeds.this, title, msg, Gravity.RIGHT, Gravity.BOTTOM);

        final TourGuide mTourGuideHandler = TourGuide.init(this).with(TourGuide.Technique.Click)
                .setPointer(pointer)
                .setOverlay(overlay)
                .playOn(filterSelectPlaceHolder);

        final TourGuide mTourGuideHandlerToolTip = TourGuide.init(this).with(TourGuide.Technique.Click)
                .setToolTip(toolTip)
                .playOn(filterToolTip);

        overlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTourGuideHandler.cleanUp();
                mTourGuideHandlerToolTip.cleanUp();
                filterSelectPlaceHolder.setVisibility(View.GONE);
                filterToolTip.setVisibility(View.GONE);

                Set<String> indexSet = PBPreferences.getPBToolTipSet();
                indexSet.add(Constants.ToolTip.SELECTINGFILTER);
                PBPreferences.setPBToolTipSet(indexSet);
                initiateFilterApplyTooltip();
            }
        });

        filterSelectPlaceHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTourGuideHandler.cleanUp();
                mTourGuideHandlerToolTip.cleanUp();
                filterSelectPlaceHolder.setVisibility(View.GONE);
                filterToolTip.setVisibility(View.GONE);

                Set<String> indexSet = PBPreferences.getPBToolTipSet();
                indexSet.add(Constants.ToolTip.SELECTINGFILTER);
                PBPreferences.setPBToolTipSet(indexSet);
                initiateFilterApplyTooltip();
            }
        });

    }


    void initiateFilterApplyTooltip() {
        if (filter_arr.size() > 0) {
            Set<String> indexSet = PBPreferences.getPBToolTipSet();
            boolean searchTooltipShown = false;

            for (String s : indexSet) {

                if (s.equalsIgnoreCase(Constants.ToolTip.APPLYINGFILTER)) {
                    searchTooltipShown = true;
                }
            }

            if (!searchTooltipShown) {
                setupFilterApplyTooltip();

            }
        }
    }

    private void setupFilterApplyTooltip() {
        mFilterApplyToolTipFlag = true;
        filterToolTipFilterapply = (Button) findViewById(R.id.filterApplyToolTip);

        int oColor = getResources().getColor(R.color.pbCoachMark);
        Overlay overlay = Utility.getOverLay(SearchFeeds.this, oColor);
        Pointer pointer = Utility.getPointer(SearchFeeds.this);
        String title = getResources().getString(R.string.filter_apply_tooltip_msg_one);
        String msg = "";
        mApplyFilter.setVisibility(View.VISIBLE);
        filterToolTipFilterapply.setVisibility(View.VISIBLE);

        ToolTip toolTip = Utility.getTooltip(SearchFeeds.this, title, msg, Gravity.RIGHT, Gravity.BOTTOM);

        mTourGuideHandlerFilterapply = TourGuide.init(this).with(TourGuide.Technique.Click)
                .setPointer(pointer)
                .setOverlay(overlay)
                .playOn(mApplyFilter);

        mTourGuideHandlerToolTipFilterapply = TourGuide.init(this).with(TourGuide.Technique.Click)
                .setToolTip(toolTip)
                .playOn(filterToolTipFilterapply);

        overlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissFilterApplyTooltip();
            }
        });


    }

    private void dismissFilterApplyTooltip() {
        mFilterApplyToolTipFlag = false;
        if (null != mTourGuideHandlerFilterapply && null != mTourGuideHandlerToolTipFilterapply && null != filterToolTipFilterapply) {
            mTourGuideHandlerFilterapply.cleanUp();
            mTourGuideHandlerToolTipFilterapply.cleanUp();
            // mApplyFilter.setVisibility(View.GONE);
            filterToolTipFilterapply.setVisibility(View.GONE);

            Set<String> indexSet = PBPreferences.getPBToolTipSet();
            indexSet.add(Constants.ToolTip.APPLYINGFILTER);
            PBPreferences.setPBToolTipSet(indexSet);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.trackScreen(Analytics.PropFeed.Search);
    }
}