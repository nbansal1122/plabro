package com.plabro.realestate.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.plabro.realestate.R;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.ProgressWheel;

public class PBWeb extends PlabroBaseActivity {

    private Toolbar mToolbar;
    private ProgressWheel mProgressWheel;
    private WebView webview;

    @Override
    protected String getTag() {
        return "PBWeb";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pb_web);
        setProgressBarVisibility(true);
//        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
//        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
//        mProgressWheel.setProgress(0.0f);
//        mProgressWheel.spin();
        inflateToolbar();

        findViewById();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void findViewById() {
        webview = (WebView) findViewById(R.id.pb_web);

        // Let's display the progress in the activity title bar, like the
        // browser app does.

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setAllowContentAccess(true);
        webview.getSettings().setAllowFileAccess(true);
        webview.getSettings().setAllowFileAccessFromFileURLs(true);
        webview.getSettings().setAllowUniversalAccessFromFileURLs(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webview.getSettings().setSupportZoom(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
        final Activity activity = this;
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                activity.setProgress(progress * 1000);
            }
        });

        Bundle b = getIntent().getExtras();
        String url = b.getString("url", Constants.PLABRO_URL);
        String title = b.getString("title", Constants.PLABRO_TITLE);
        getSupportActionBar().setTitle(title);
        webview.loadUrl(url);

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void inflateToolbar() {
        //setting action bar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);


        // Inflate a menu to be displayed in the toolbar
        mToolbar.inflateMenu(R.menu.menu_image_cropper);

        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image_cropper, menu);
        return true;
    }
}