package com.plabro.realestate.others;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.utilities.PBPreferences;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Hemant on 13/05/15.
 */
public class CleverTap {

    private static Context mContext;
    private static CleverTap mCleverTap;

    public static CleverTapAPI getmCleverTapAPI() {
        return mCleverTapAPI;
    }

    private static CleverTapAPI mCleverTapAPI;

    private Context ctx;


    public static void initCleverTap(Context ctx) {
        if (mCleverTap == null) {
            mCleverTap = new CleverTap(ctx);
        }
//        if (null != AppController.getInstance().getUserProfile()) {
//            recordProfile(AppController.getInstance().getUserProfile());
//        }
    }

    public static CleverTap getInstance(Context ctx) {
        if (mCleverTap == null) {
            mCleverTap = new CleverTap(ctx.getApplicationContext());
        }
        mCleverTapAPI = mCleverTap.getCleverTapAPI();
        //  mCleverTapAPI.setDebugLevel(1);

        return mCleverTap;
    }

    private CleverTap(Context ctx) {
        try {
            mCleverTapAPI = CleverTapAPI.getInstance(ctx);
            mCleverTapAPI.enablePersonalization();
        } catch (CleverTapMetaDataNotFoundException e) {
            e.printStackTrace();
        } catch (CleverTapPermissionsNotSatisfied CleverTapPermissionsNotSatisfied) {
            CleverTapPermissionsNotSatisfied.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public CleverTapAPI getCleverTapAPI() {

        if (mCleverTapAPI == null) {
            try {
                mCleverTapAPI = CleverTapAPI.getInstance(mContext);
            } catch (CleverTapMetaDataNotFoundException e) {
                e.printStackTrace();
            } catch (CleverTapPermissionsNotSatisfied e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return mCleverTapAPI;
    }

    public static void recordEvent(String eventName) {
        mCleverTapAPI.event.push(eventName);
    }

    public static void recordEventWithProperties(String eventName, HashMap<String, Object> props) {
        mCleverTapAPI.event.push(eventName, props);

    }

    public static void recordProfile(ProfileClass profileClass) {
        if (null == profileClass) return;
        HashMap<String, Object> profileUpdate = new HashMap<String, Object>();


        profileUpdate.put("Name", profileClass.getName()); // String
        profileUpdate.put("Identity", profileClass.getAuthorid()); // String or number
        profileUpdate.put("Email", profileClass.getEmail()); // Email address of the user
        String phone = profileClass.getPhone();
        if (null != phone && !TextUtils.isEmpty(phone)) {
            String countryCode = PBPreferences.getCountryCode();
            if (phone.contains(countryCode)) {
                phone = phone.replace(countryCode, ""); //todo remove
            }
            profileUpdate.put("Phone", phone); // Phone(without the country code
        }
        profileUpdate.put("MSG-email", true); // Enable email notifications
        profileUpdate.put("MSG-push", true); // Enable push notifications
        profileUpdate.put("MSG-sms", false); // Disable SMS notifications


        mCleverTapAPI.profile.push(profileUpdate);

        Iterator it = profileUpdate.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            System.out.println("CleverTapProfile" + pair.getKey() + " = " + pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }

    }

    public void recordNotificationClicked(Intent intent) {
        // mCleverTapAPI.event.pushNotificationEvent(intent.getExtras());
    }


}
