package com.plabro.realestate.models.community.info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.models.ServerResponse;

/**
 * Created by nbansal2211 on 15/06/16.
 */
public class CommunityInfoResponse extends ServerResponse {


    @SerializedName("output_params")
    @Expose
    private Output_params output_params;

    @SerializedName("loginStatus")
    @Expose
    private Boolean loginStatus;
    @SerializedName("RT")
    @Expose
    private String rT;

    /**
     * @return The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return The output_params
     */
    public Output_params getOutput_params() {
        return output_params;
    }

    /**
     * @param output_params The output_params
     */
    public void setOutput_params(Output_params output_params) {
        this.output_params = output_params;
    }

    /**
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return The loginStatus
     */
    public Boolean getLoginStatus() {
        return loginStatus;
    }

    /**
     * @param loginStatus The loginStatus
     */
    public void setLoginStatus(Boolean loginStatus) {
        this.loginStatus = loginStatus;
    }

    /**
     * @return The rT
     */
    public String getRT() {
        return rT;
    }

    /**
     * @param rT The RT
     */
    public void setRT(String rT) {
        this.rT = rT;
    }

}
