package com.plabro.realestate.utilities;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.activity.AuctionDetails;
import com.plabro.realestate.activity.DeepLinking;
import com.plabro.realestate.activity.FragmentContainer;
import com.plabro.realestate.activity.Redirect;
import com.plabro.realestate.activity.RelatedTagFeeds;
import com.plabro.realestate.activity.SearchFeeds;
import com.plabro.realestate.activity.Shout;
import com.plabro.realestate.activity.UserProfileNew;
import com.plabro.realestate.activity.WebViewActivity;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.fragments.BlogNewsFragment;
import com.plabro.realestate.fragments.auctions.AuctionFragment;
import com.plabro.realestate.models.propFeeds.AdvertisementFeed;

import java.util.HashMap;

/**
 * Created by nitin on 11/02/16.
 */
public class DeepLinkNavigationUtil {

    public static void sendChatInteraction(String phoneNumber, String type, String TAG){
        HashMap<String, Object> wrData = new HashMap<String, Object>();
        wrData.put("imTo", phoneNumber);
        wrData.put("imFrom", PBPreferences.getPhone());
        wrData.put("source", type);
        wrData.put("ScreenName", TAG);
        wrData.put("Action", "IM");
        PlabroIntentService.startForNotifyingInteraction(wrData);
    }

    public static void navigate(Bundle b, Context ctx) {
        if (b == null) return;
        String act = b.getString("act");
        if (TextUtils.isEmpty(act)) return;
        String title = "";
        switch (act) {
            case DeepLinking.ACT_TYPE.CHAT:
                if (b.containsKey("phone") && b.containsKey("name")) {
                    b.putString("phone_key", b.getString("phone"));
                    b.putString("name", b.getString("name"));
                    sendChatInteraction(b.getString("phone"), "DeepLink", b.getString("name"));
                    startNextActivity(ctx, XMPPChat.class, b);
                } else {
                    FragmentContainer.startActivity(ctx, Constants.FRAGMENT_TYPE.CHAT, b);
                }

                break;
            case DeepLinking.ACT_TYPE.SEARCH:
                if (b.containsKey("searchString")) {
                    b.putString("searchText", b.getString("searchString"));
                    if (b.containsKey("filter")) {
                        b.putString("filter", b.getString("filter"));
                    }
                }
                startNextActivity(ctx, SearchFeeds.class, b);
                break;
            case DeepLinking.ACT_TYPE.COMPOSE:
                if (b.containsKey("shoutText")) {
                    // The below line is to help Shout.java to fill the text
                    // Same Code is already implemented for filling the text
                    // for search cases
                    b.putString("searchText", b.getString("shoutText"));
                    b.putBoolean("searchFlag", true);
                }
                startNextActivity(ctx, Shout.class, b);
                break;
            case DeepLinking.ACT_TYPE.HASH:
                if (b.containsKey("tagString")) {
                    b.putString("refKey", "");
                    b.putString("hashTitle", b.getString("tagString"));
                    b.putString("message_id", b.getString("messageId"));
                }
                startNextActivity(ctx, RelatedTagFeeds.class, b);
                break;
            case DeepLinking.ACT_TYPE.SHOUT:
                if (b.containsKey("messageId")) {
                    b.putString("message_id", b.getString("messageId"));
                }
                startNextActivity(ctx, Redirect.class, b);
                break;
            case DeepLinking.ACT_TYPE.AUCTION:
                FragmentContainer.startActivity(ctx, Constants.FRAGMENT_TYPE.AUCTION, b);
                break;
            case DeepLinking.ACT_TYPE.NOTIF:
                FragmentContainer.startActivity(ctx, Constants.FRAGMENT_TYPE.NOTIFICATION, b);
                break;
            case DeepLinking.ACT_TYPE.WALLET:
                FragmentContainer.startActivity(ctx, Constants.FRAGMENT_TYPE.WALLET, b);
                break;
            case DeepLinking.ACT_TYPE.DIRECT_LISTING:
                FragmentContainer.startActivity(ctx, Constants.FRAGMENT_TYPE.DIRECT_LISTING, b);
                break;
            case DeepLinking.ACT_TYPE.NEWS:
                b.putString("type", BlogNewsFragment.NEWS);
                FragmentContainer.startActivity(ctx, Constants.FRAGMENT_TYPE.NEWS, b);
                break;
            case DeepLinking.ACT_TYPE.BLOGS:
                b.putString("type", BlogNewsFragment.BLOGS);
                FragmentContainer.startActivity(ctx, Constants.FRAGMENT_TYPE.BLOGS, b);
                break;
            case DeepLinking.ACT_TYPE.SERVICES:
                FragmentContainer.startActivity(ctx, Constants.FRAGMENT_TYPE.SERVICES, b);
                break;
            case DeepLinking.ACT_TYPE.AUCTION_DETAIL:
                // Start Auction or Bid Detail Activity

                String auctionId = b.getString(Constants.BUNDLE_KEYS.AUCTION_ID);
                String bidId = b.getString(Constants.BUNDLE_KEYS.BID_ID);
                title = b.getString(Constants.BUNDLE_KEYS.AD_TITLE);
                if (TextUtils.isEmpty(title)) {
                    title = b.getString(Constants.BUNDLE_KEYS.TITLE);
                }
                if (TextUtils.isEmpty(title)) {
                    title = "Requirement on Plabro";
                }
                b.putString(Constants.BUNDLE_KEYS.TITLE, title);
                b.putString(Constants.BUNDLE_KEYS.AD_TITLE, title);
                boolean flag = false;
                if (!TextUtils.isEmpty(auctionId)) {
                    flag = true;

                } else if (!TextUtils.isEmpty(bidId)) {
                    b.putBoolean(Constants.BUNDLE_KEYS.IS_MY_BID, true);
                    flag = true;
                } else {

                }
                if(flag){
                    Intent i = new Intent(ctx, AuctionDetails.class);
                    i.putExtras(b);
                    ctx.startActivity(i);
                }else{
                    FragmentContainer.startActivity(ctx, Constants.FRAGMENT_TYPE.AUCTION, b);
                }
                break;
            case DeepLinking.ACT_TYPE.PLABRO_WEB:
                if (b.containsKey("input")) {
                    String url = b.getString("input");
                    title = b.getString("title");
                    if (TextUtils.isEmpty(title)) {
                        title = "Plabro";
                    }
                    WebViewActivity.startActivity(ctx, title, url, "", false);
                }
                break;
            case DeepLinking.ACT_TYPE.P_SCAN:
                FragmentContainer.startActivity(ctx, Constants.FRAGMENT_TYPE.PLABRO_SCAN, b);
                break;
            case DeepLinking.ACT_TYPE.PROFILE:
                if (b.containsKey("authorid")) {
                    UserProfileNew.startActivity(ctx, b.getString("authorid"), "", "");
                }
                break;

        }
    }

    public static void startNextActivity(Context ctx, Class activityClass, Bundle bundle) {
        Intent i = new Intent(ctx, activityClass);
        i.putExtras(bundle);
        ctx.startActivity(i);
    }

    public static Bundle getMap(String uriString) {
        Bundle map = new Bundle();
        try {
            String[] splittedString = uriString.split("\\?");
            String suffixString = splittedString[1];
            splittedString = suffixString.split("&");
            for (String s : splittedString) {
                String[] array = s.split("=");
                map.putString(array[0], array[1]);
            }
        } catch (Exception e) {

        }
        return map;
    }
}
