package com.plabro.realestate.models.profile;


import com.plabro.realestate.models.ServerResponse;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class ProfileResponse extends ServerResponse {

    ProfileOutputParams output_params = new ProfileOutputParams();


    public ProfileOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(ProfileOutputParams output_params) {
        this.output_params = output_params;
    }

}
