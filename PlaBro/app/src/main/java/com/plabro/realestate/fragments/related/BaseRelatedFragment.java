package com.plabro.realestate.fragments.related;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.RelatedCustomAdapter;
import com.plabro.realestate.fragments.RecyclerViewFragment;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.LoaderFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;

/**
 * Created by nitin on 22/09/15.
 */
public abstract class BaseRelatedFragment extends RecyclerViewFragment implements ShoutObserver.PBObserver {
    protected RelatedCustomAdapter mAdapter;
    //    private RecyclerView mRecyclerView;
    protected LinearLayoutManager mLayoutMgr;
    protected int hashKey = 0;
    protected boolean isRunning = false;
    protected Context mContext;
    protected int page = 0;
    protected boolean isLoading = false;
    protected ArrayList<BaseFeed> feeds2 = new ArrayList<BaseFeed>();
    protected String message_id = "";
    protected int headerHeight;


    @Override
    protected int getViewId() {
        return R.layout.frag_related;
    }

    public void setRecyclerViewLayoutManager() {
        int scrollPosition = 0;
        mLayoutMgr = new LinearLayoutManager(getActivity());
//        if (mRecyclerView.getLayoutManager() != null) {
//            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
//                    .findFirstCompletelyVisibleItemPosition();
//        }
        mRecyclerView.setLayoutManager(mLayoutMgr);
//        mRecyclerView.scrollToPosition(scrollPosition);
    }


    @Override
    public void onStatusFalse(PBResponse response, int taskCode) {
        super.onStatusFalse(response, taskCode);
    }


    protected void showSpinners() {

    }

    private void stopSpinners() {
//                mProgressWheel.stopSpinning();
//                isLoading = false;
//                mProgressWheelLoadMore.stopSpinning();
    }


    @Override
    protected void findViewById() {
        Analytics.trackScreen(Analytics.PropFeed.RelatedBrokers);
        Log.d(TAG, "Find View By ID");
        LoaderFeed f = new LoaderFeed();
        f.setType(Constants.FEED_TYPE.PROGRESS_MORE);
        feeds2.add(f);
        registerLocalReceiver();
        ShoutObserver.getInstance().addListener(this);

        page = 0;
        isLoading = false;

//

        mRecyclerView = (RecyclerView) findView(R.id.recyclerView);

        setRecyclerViewLayoutManager();
//        mRecyclerView.setItemAnimator(new FadeInUpAnimator());
        setListAdapter(false);
        setRecyclerViewOnScrollListener();
        // Request data from the network.
        getData(false);


    }


    @Override
    protected void setViewSizes() {

    }

    @Override
    protected void callScreenDataRequest() {

    }

    @Override
    protected void reloadRequest() {

        callScreenDataRequest();
    }


    private void setListAdapter(boolean scrollToLast) {
        Log.d(TAG, "setting list adapter");
        mAdapter = new RelatedCustomAdapter(feeds2, TAG, getActivity(), hashKey);
        mAdapter.setHeaderHeight(headerHeight);
        mRecyclerView.setAdapter(mAdapter);
    }


    void showNoDataDialog(final Boolean addMore) {
        //  Log.i(TAG, "No Data Dialog" + addMore);
        stopSpinners();
        if (!addMore && feeds2.size() < 1) {
//                        mConditionalView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void OnReceive(Context context, Intent intent) {
//        Log.d(TAG, "onReceive");
//        mAdapter = new RelatedCustomAdapter(feeds2, "normal", getActivity(), hashKey);
//        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    protected void setScrollOnLayoutManager(int scrollY) {
        Log.d(TAG, "y scrolling" + scrollY);
        mLayoutMgr.scrollToPositionWithOffset(0, -(scrollY));
    }

    @Override
    public void onShoutUpdate() {

    }

    @Override
    public void onShoutUpdateGen(String type) {
        feeds2.get(feeds2.size() - 1).setType(Constants.FEED_TYPE.PROGRESS_MORE);
        mAdapter.notifyItemChanged(mAdapter.getItemCount() - 1);
        if (mAdapter.getItemCount() > 10) {
            getData(true);
        } else {
            getData(false);
        }
    }

    @Override
    protected void onRecycleScroll(RecyclerView view, int dx, int dy, int scrollY, int pagePosition) {
        Log.d(TAG, "OnRecycle Scroll");
        int totalItemCount = mLayoutMgr.getItemCount();
        if (!isLoading && dy > 0) {

            int visibleItemCount = mLayoutMgr.getChildCount();

            int pastVisibleItems = mLayoutMgr.findFirstVisibleItemPosition();

            Log.d(TAG, "Visible Items :" + visibleItemCount + ", PastVisibleItems:" + pastVisibleItems + ", Total ItemCount:" + totalItemCount);

            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                //reached bottom show loading
                //mProgressWheelLoadMore.setVisibility(View.VISIBLE);

            } else {
                // mProgressWheelLoadMore.setVisibility(View.GONE);

            }

            if ((visibleItemCount + pastVisibleItems) >= totalItemCount - Constants.SCROLL_ITEM_COUNT) {
                Log.d(TAG, "GetData Scroll");
                getData(true);
            }
        }
    }
}
