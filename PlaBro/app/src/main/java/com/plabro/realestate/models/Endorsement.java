package com.plabro.realestate.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Table;

/**
 * Created by nitin on 29/07/15.
 */
@Table(name = "Endorsement")
public class Endorsement extends Model {
    private String endorsementKey;
    private String authorId;

    public String getEndorsementKey() {
        return endorsementKey;
    }

    public void setEndorsementKey(String endorsementKey) {
        this.endorsementKey = endorsementKey;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }
}
