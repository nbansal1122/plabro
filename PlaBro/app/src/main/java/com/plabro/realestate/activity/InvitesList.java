package com.plabro.realestate.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.HomeActivity;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.contacts.InviteResponse;
import com.plabro.realestate.models.contacts.NonPlabroContact;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 07/09/15.
 */
public class InvitesList extends PlabroBaseActivity implements VolleyListener, CustomListAdapter.CustomListAdapterInterface, AdapterView.OnItemClickListener {

    private int headerHeight;
    private ListView listView;
    private CustomListAdapter<NonPlabroContact> adapter;
    private List<NonPlabroContact> contactList = new ArrayList<>();
    private int selectedCount = 0;
    private int prevSelectedPosition = -1;
    private ProgressDialog dialog;
    private String urlWithoutAuthKey;

    @Override
    protected String getTag() {
        return "Invitation List";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_new_invites);
        Analytics.trackScreen(Analytics.InvitesScreens.INVITATION_LIST);
        inflateToolbar();
        setTitle(getString(R.string.invite_your_friends));
        ActivityUtils.setStatusBarColor(this, getResources().getColor(R.color.color_purple_bg));
        int inviteCounter = PBPreferences.getData(PBPreferences.INVITE_SCREEN_COUNTER, 0);
        PBPreferences.saveData(PBPreferences.INVITE_SCREEN_COUNTER, inviteCounter + 1);
        listView = (ListView) findViewById(R.id.listView);

        listView.setOnItemClickListener(this);
//        listView.addHeaderView(getHeaderView());
        setListViewScroll();
        setFooter(false);
        setAdapter();
        setClickListeners(R.id.btn_send_invite, R.id.btn_skip);
        getNonPlabroFriends();
    }

    private View getHeaderView() {
        View headerView = LayoutInflater.from(this).inflate(R.layout.header_new_invites, null);
        Log.d(TAG, "Header Height :" + headerView.getHeight());
        headerHeight = headerView.getHeight();
        return headerView;
    }

    private void setAdapter() {
        List<NonPlabroContact> nonPlabroContactList = new Select().from(NonPlabroContact.class).execute();
        contactList.clear();
        for (NonPlabroContact c : nonPlabroContactList) {
            if (!c.isInvited()) {
                contactList.add(c);
            }
        }
        adapter = new CustomListAdapter<>(this, R.layout.row_new_invites, contactList, this);
        listView.setAdapter(adapter);
        selectedCount = 0;
        setFooter(selectedCount > 0);
    }

    private void getNonPlabroFriends() {
        showDialog();
        ParamObject obj = new ParamObject();
        obj.setTaskCode(Constants.TASK_CODES.GET_NON_PLABRO_FRIENDS);
        obj.setClassType(InviteResponse.class);
        HashMap<String, String> params = Utility.getInitialParams(PlaBroApi.RT.GET_NON_PLABRO_FRIENDS, null);
        obj.setParams(params);
        AppVolley.processRequest(obj, this);
    }


    private void loadContactsFromMap(List<String> nonPlabroContacts) {
        HashMap<String, String> map = PBPreferences.loadMap(PBPreferences.CONTACTS_MAP);

        for (String s : nonPlabroContacts) {
            NonPlabroContact c = new NonPlabroContact();
            c.setPhoneNumber(s);
            String name = ActivityUtils.getDisplayNameFromPhone(s);
            if (!TextUtils.isEmpty(name)) {
                c.setContactName(name);
            }
            c.save();
        }

        Log.d(TAG, "Size of contact list:" + contactList.size());
        setAdapter();

    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        dismissDialog();
        onApiFailure(response, taskCode);
        switch (taskCode) {
            case Constants.TASK_CODES.GET_NON_PLABRO_FRIENDS:
                Log.d(TAG, "Response On Failure:");
//                closeActivity();
                break;
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this).inflate(resourceID, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        NonPlabroContact c = contactList.get(position);
        holder.name.setText(c.getContactName());
        holder.mobile.setText(c.getPhoneNumber());
        if (c.isSelected()) {
            holder.imgCheckBox.setImageResource(R.drawable.ic_invitation_selected);
        } else {
            holder.imgCheckBox.setImageResource(R.drawable.ic_invitation_unselected);
        }
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        NonPlabroContact c = contactList.get(i);
//        if (prevSelectedPosition != -1 && prevSelectedPosition != (i - 1)) {
//            contactList.get(prevSelectedPosition).setIsSelected(false);
//        }
//        prevSelectedPosition = i - 1;

        c.setIsSelected(!c.isSelected());
        adapter.notifyDataSetChanged();

        if (c.isSelected()) {
            selectedCount++;
        } else {
            selectedCount--;
        }
        setFooter(selectedCount > 0);

    }

    @Override
    public void findViewById() {

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    private class ViewHolder {
        private TextView name, mobile;
        private ImageView imgCheckBox;

        public ViewHolder(View view) {
            name = (TextView) view.findViewById(R.id.tv_inviteeName);
            mobile = (TextView) view.findViewById(R.id.tv_inviteePhone);
            imgCheckBox = (ImageView) view.findViewById(R.id.iv_invitation_checkbox);
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btn_send_invite:
//                JSONArray invitedArray = new JSONArray();
//                for (NonPlabroContact c : contactList) {
//                    if (c.isSelected()) {
//                        invitedArray.put(c.getCallee());
//                    }
//                }
//                sendInvite(invitedArray);
                List<NonPlabroContact> contacts = new ArrayList<>();
                for (NonPlabroContact c : contactList) {
                    if (c.isSelected()) {
                        contacts.add(c);
                    }
                }
                sendInvitedUsers(contacts);
                break;
            case R.id.btn_skip:
                Intent i = new Intent(InvitesList.this, HomeActivity.class);
                if (getIntent().getExtras() != null)
                    i.putExtras(getIntent().getExtras());
                startActivity(i);
                finish();
                break;
        }
    }


    private void sendInvite(JSONArray invitedArray, final List<NonPlabroContact> list) {
        ParamObject obj = new ParamObject();
        HashMap<String, String> params = new HashMap<>();
        params.put("contacts_arr", invitedArray.toString());
        params = Utility.getInitialParams(PlaBroApi.RT.INVITED_USERS, params);
        obj.setParams(params);
        obj.setRequestMethod(RequestMethod.POST);
        obj.setTaskCode(Constants.TASK_CODES.INVITE_USERS);
        showDialog();
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "OnSucess");
                dismissDialog();
                showToast("Invitation sent successfully via Plabro");
                Analytics.trackScreen(Analytics.InvitesScreens.INVITE_SENT_SUCCESSFULLY);
                for (NonPlabroContact c : list) {
                    c.setIsInvited(true);
                    c.save();
                }
                setAdapter();
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "onFailure");
                dismissDialog();
                onApiFailure(response, taskCode);
            }
        });
    }

    private void sendInvitedUsers(final List<NonPlabroContact> list) {

        JSONArray array = new JSONArray();
        for (NonPlabroContact c : list) {
            array.put(c.getPhoneNumber());
        }

        sendInvite(array, list);

    }


    private void setFooter(boolean isSelected) {
        Button skip = (Button) findViewById(R.id.btn_skip);
        Button sendInvite = (Button) findViewById(R.id.btn_send_invite);
        if (isSelected) {
            sendInvite.setVisibility(View.VISIBLE);
            skip.setBackgroundColor(getResources().getColor(R.color.color_skip_inv_bg));
            skip.setTextColor(getResources().getColor(R.color.pbBlack));
        } else {
            sendInvite.setVisibility(View.GONE);
            skip.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            skip.setTextColor(getResources().getColor(R.color.white));
        }
    }

    protected int getScrollYOfListView(AbsListView view) {
        View child = view.getChildAt(0);
        if (child == null) {
            android.util.Log.d(TAG, "Child is Null");
            return 0;
        }

        int firstVisiblePosition = view.getFirstVisiblePosition();
        int top = child.getTop();

        int headerHeight = 0;
        if (firstVisiblePosition >= 1) {
            headerHeight = this.headerHeight;
        }
        Log.d(TAG, "HH:" + this.headerHeight);
        int scrollY = -top + firstVisiblePosition * child.getHeight() + headerHeight;
        Log.d(TAG, "SCrollY::" + scrollY + ":Top:" + top + "ChildPosition:" + (firstVisiblePosition * child.getHeight()) + ", HeaderHeight:" + headerHeight);

        return scrollY;
    }

    private void setListViewScroll() {
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                Log.d(TAG, "ListView Scroll");
                getScrollYOfListView(absListView);
            }
        });
    }


    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        // mProgressWheel.stopSpinning();
        dismissDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.GET_NON_PLABRO_FRIENDS:
                InviteResponse res = (InviteResponse) response.getResponse();
                if (null != res.getOutput_params() && null != res.getOutput_params().getData()) {
                    try {
                        new Delete().from(NonPlabroContact.class).execute();
                    } catch (Exception e) {

                    }
                    List<String> nonPLabroContacts = res.getOutput_params().getData();
                    loadContactsFromMap(nonPLabroContacts);
                }
//                closeActivity();
                break;

        }

    }

    private void closeActivity() {
        if (null != contactList && contactList.size() < 1) {
            onClick(findViewById(R.id.btn_skip));
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
