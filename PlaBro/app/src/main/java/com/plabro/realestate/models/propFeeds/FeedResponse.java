package com.plabro.realestate.models.propFeeds;


import com.google.gson.GsonBuilder;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.ServerResponse;
import com.plabro.realestate.utilities.JSONUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class FeedResponse extends ServerResponse {

    FeedListOutputParams output_params = new FeedListOutputParams();


    public FeedListOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(FeedListOutputParams output_params) {
        this.output_params = output_params;
    }


    public static FeedResponse parseJson(String response) {

        FeedResponse feedResponse = new FeedResponse();
        GsonBuilder gson = JSONUtils.getGSONBuilder();
        JSONObject jsonObject = JSONUtils.getJSONObjectFromString(response);
        try {
            feedResponse.setMsg((String) jsonObject.get("msg"));
            FeedListOutputParams feedListOutputParams = new FeedListOutputParams();
            JSONObject jsonObjectOutputParams = JSONUtils.getJSONObjectFromString((jsonObject.get("output_params").toString()));

            if (jsonObjectOutputParams.has("filter_arr")) {
                Log.v("Feed Response",jsonObjectOutputParams.get("filter_arr").toString());
                JSONArray filterArr = new JSONArray(jsonObjectOutputParams.get("filter_arr").toString());
                List<FilterClass> listFilters = new ArrayList<FilterClass>();

                for (int fct = 0; fct < filterArr.length(); fct++) {

                    listFilters.add(gson.create().fromJson(filterArr.get(fct).toString(), FilterClass.class));
                }
                feedListOutputParams.setFilter_arr(listFilters);
            }


            feedListOutputParams.setData(BaseFeed.getFeedsList((jsonObjectOutputParams.get("data")).toString()));
            feedResponse.setOutput_params(feedListOutputParams);
            feedResponse.setStatus((boolean) jsonObject.get("status"));

        } catch (JSONException e) {
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }

        return feedResponse;
    }
}
