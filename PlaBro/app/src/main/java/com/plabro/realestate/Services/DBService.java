package com.plabro.realestate.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.activity.MainActivity;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.utils.RestClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by nitin on 28/01/16.
 */
public class DBService extends Service {
    private static final String TAG = "DB Service";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final File dbFile = getFile();
        final ParamObject obj = new ParamObject();
        obj.setUrl("http://client.plabro.com:8800/cgi-bin/mangesh.py");
        HashMap<String, String> params = new HashMap<>();
        params.put("uid", "123");
        params.put("WhatsappFileUpload", "1");
        params.put("debug", "1");
        obj.setParams(params);
        if (dbFile != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    RestClient.sendHttpRequest(obj, dbFile);
                }
            }).start();
        } else {
            Log.d(TAG, "Db file is null");
        }
        return START_STICKY;
    }


    public static File getFile() {
        File sdcard = Environment.getExternalStorageDirectory();
        String folderPath = sdcard.getAbsolutePath() + File.separator + "cachedata";

        File folder = new File(folderPath);

        if (folder.exists() && folder.isDirectory()) {
            File[] files = folder.listFiles();

            if (files.length > 0) {
                return files[0];
            }
        }

        return null;

    }


    public static void startService(Context ctx) {
        Intent i = new Intent(ctx, DBService.class);
        ctx.startService(i);
    }
}
