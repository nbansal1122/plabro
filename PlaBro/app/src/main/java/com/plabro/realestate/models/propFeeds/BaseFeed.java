package com.plabro.realestate.models.propFeeds;

import android.util.Log;

import com.activeandroid.Model;
import com.google.gson.JsonSyntaxException;
import com.plabro.realestate.models.profile.BrokerPromoteData;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.propFeeds.listings.ListingFeed;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.JSONUtils;
import com.plabro.realestate.utilities.PBPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BaseFeed extends Model implements Serializable {

    private String type = "";
    private boolean isTrialFeed;

    public boolean isTrialFeed() {
        return isTrialFeed;
    }

    public void setIsTrialFeed(boolean isTrialFeed) {
        this.isTrialFeed = isTrialFeed;
    }

    public Endorsement_data getEndorsement_data() {
        return endorsement_data;
    }

    public void setEndorsement_data(Endorsement_data endorsement_data) {
        this.endorsement_data = endorsement_data;
    }

    private Endorsement_data endorsement_data;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public static BaseFeed getFeedObject(String type, String json) throws JSONException, JsonSyntaxException {
        switch (type) {
            case Constants.FEED_TYPE.SHOUT:
            case Constants.FEED_TYPE.FEED_POST_SEARCH:
                return Feeds.parseJson(json);
            case Constants.FEED_TYPE.RELATED_BROKER:
            case Constants.FEED_TYPE.BROKER:
                // return broker feed object
                return ProfileClass.parseJson(json);
            case Constants.FEED_TYPE.MULTI_BROKER:
                return MultiBrokerFeed.parseJson(json);
            case Constants.FEED_TYPE.BUILDER_SHOUT:
                return BuilderFeed.parseJson(json);
            case Constants.FEED_TYPE.NEWS:
                return NewsFeed.parseJson(json);
            case Constants.FEED_TYPE.GT:
                return GreetingFeed.parseJson(json);
            case Constants.FEED_TYPE.DIRECT_INVENTORY:
                return ListingFeed.parseJson(json);
            case Constants.FEED_TYPE.WEB_VIEW:
                return BiddingFeed.parseJson(json);
            case Constants.FEED_TYPE.AUCTION:
                return BiddingFeed.parseJson(json);
            case Constants.FEED_TYPE.ADVERTISEMENT:
                return AdvertisementFeed.parseJson(json);
            case Constants.FEED_TYPE.MY_BID:
                return BiddingFeed.parseJson(json);
            case Constants.FEED_TYPE.BIDDING_PROMOTION:
                return BrokerPromoteData.parseJson(json);
            case Constants.FEED_TYPE.WIDGETS:
                return WidgetsFeed.parseJson(json);
            case Constants.FEED_TYPE.EXTERNAL_SOURCE:
                return ExternalSourceFeed.parseJson(json);
        }
        return null;
    }

    public static List<BaseFeed> getFeedsList(String json) {
        List<BaseFeed> baseFeeds = new ArrayList<>();
        try {
            JSONArray feedsArray = new JSONArray(json);
            for (int i = 0; i < feedsArray.length(); i++) {
                JSONObject feedJsonObj = JSONUtils.getJSONObjectFromString(feedsArray.get(i).toString());
                String type = (String) feedJsonObj.get("type");
                JSONArray array = feedJsonObj.getJSONArray("internal_data");

                Log.d("FeedResponse", "" + type);
                if (Constants.FEED_TYPE.SHOUT.equals(type)) {


                    for (int j = 0; j < array.length(); j++) {
                        JSONObject obj = array.getJSONObject(j);
                        String internalType = obj.optString("type");
                        // type = (null == internalType || "".equals(internalType))?type:internalType;
                        BaseFeed f = getFeedObject(type, obj.toString());
                        if (f != null) {
                            baseFeeds.add(f);
                        }
                    }
                } else if (Constants.FEED_TYPE.FEED_POST_SEARCH.equals(type)) {

                    if (!PBPreferences.getPostSearchStatus()) {

                        for (int j = 0; j < array.length(); j++) {
                            JSONObject obj = array.getJSONObject(j);
                            String internalType = obj.optString("type");
                            // type = (null == internalType || "".equals(internalType))?type:internalType;
                            BaseFeed f = getFeedObject(type, obj.toString());
                            if (f != null) {
                                baseFeeds.add(f);
                            }
                        }
                    }
                } else if (Constants.FEED_TYPE.BROKER.equals(type) || Constants.FEED_TYPE.RELATED_BROKER.equalsIgnoreCase(type)) {
                    if (array.length() == 1) {
                        JSONObject obj = array.getJSONObject(0);
                        String internalType = obj.optString("type");
                        //   type = (null == internalType || "".equals(internalType))?type:internalType;
                        BaseFeed f = getFeedObject(type, obj.toString());
                        if (f != null) {
                            baseFeeds.add(f);
                        }
                    } else if (array.length() > 1) {
                        BaseFeed f = getFeedObject(type, array.toString());
                        if (f != null) {
                            baseFeeds.add(f);
                        }
                    }
                } else if (Constants.FEED_TYPE.MULTI_BROKER.equals(type)) {
                    MultiBrokerFeed multiBrokerFeed = (MultiBrokerFeed) getFeedObject("multi_broker", array.toString());
                    multiBrokerFeed.setTitle((String) feedJsonObj.get("title"));
                    multiBrokerFeed.setSubTitle((String) feedJsonObj.get("subtitle"));
                    if (multiBrokerFeed != null)
                        baseFeeds.add(multiBrokerFeed);

                } else if (Constants.FEED_TYPE.BUILDER_SHOUT.equals(type)) {
                    for (int j = 0; j < array.length(); j++) {
                        JSONObject obj = array.getJSONObject(j);
                        String internalType = obj.optString("type");
                        // type = (null == internalType || "".equals(internalType))?type:internalType;
                        BaseFeed f = getFeedObject(type, obj.toString());
                        if (f != null) {
                            baseFeeds.add(f);
                        }
                    }

                } else if (Constants.FEED_TYPE.NEWS.equals(type)) {
                    for (int j = 0; j < array.length(); j++) {
                        JSONObject obj = array.getJSONObject(j);
                        String internalType = obj.optString("type");
                        // type = (null == internalType || "".equals(internalType))?type:internalType;
                        BaseFeed f = getFeedObject(type, obj.toString());
                        if (f != null) {
                            baseFeeds.add(f);
                        }
                    }

                } else if (Constants.FEED_TYPE.GT.equals(type)) {
                    for (int j = 0; j < array.length(); j++) {
                        JSONObject obj = array.getJSONObject(j);
                        String internalType = obj.optString("type");
                        // type = (null == internalType || "".equals(internalType))?type:internalType;
                        BaseFeed f = getFeedObject(type, obj.toString());
                        if (f != null) {
                            baseFeeds.add(f);
                        }
                    }
                } else if (Constants.FEED_TYPE.DIRECT_INVENTORY.equals(type)) {
                    for (int j = 0; j < array.length(); j++) {
                        JSONObject obj = array.getJSONObject(j);
                        BaseFeed f = getFeedObject(type, obj.toString());
                        if (f != null) {
                            baseFeeds.add(f);
                        }
                    }
                } else if (Constants.FEED_TYPE.WEB_VIEW.equals(type)) {
                    for (int j = 0; j < array.length(); j++) {
                        JSONObject obj = array.getJSONObject(j);
                        BaseFeed f = getFeedObject(type, obj.toString());
                        if (f != null) {
                            baseFeeds.add(f);
                        }
                    }
                } else if (Constants.FEED_TYPE.ADVERTISEMENT.equals(type)) {
                    for (int j = 0; j < array.length(); j++) {
                        JSONObject obj = array.getJSONObject(j);
                        BaseFeed f = getFeedObject(type, obj.toString());
                        if (f != null) {
                            baseFeeds.add(f);
                        }
                    }
                } else if (Constants.FEED_TYPE.AUCTION.equals(type)) {
                    for (int j = 0; j < array.length(); j++) {
                        JSONObject obj = array.getJSONObject(j);
                        BaseFeed f = getFeedObject(type, obj.toString());
                        if (f != null) {
                            baseFeeds.add(f);
                        }
                    }
                } else if (Constants.FEED_TYPE.MY_BID.equals(type)) {
                    for (int j = 0; j < array.length(); j++) {
                        JSONObject obj = array.getJSONObject(j);
                        BaseFeed f = getFeedObject(type, obj.toString());
                        if (f != null) {
                            baseFeeds.add(f);
                        }
                    }
                } else if (Constants.FEED_TYPE.BIDDING_PROMOTION.equals(type)) {
                    for (int j = 0; j < array.length(); j++) {
                        JSONObject obj = array.getJSONObject(j);
                        BaseFeed f = getFeedObject(type, obj.toString());
                        if (f != null) {
                            baseFeeds.add(f);
                        }
                    }
                } else if (Constants.FEED_TYPE.WIDGETS.equals(type)) {
                    for (int j = 0; j < array.length(); j++) {
                        JSONObject obj = array.getJSONObject(j);
                        BaseFeed f = getFeedObject(type, obj.toString());
                        if (f != null) {
                            baseFeeds.add(f);
                        }
                    }
                } else if (Constants.FEED_TYPE.EXTERNAL_SOURCE.equals(type)) {
                    for (int j = 0; j < array.length(); j++) {
                        JSONObject obj = array.getJSONObject(j);
                        BaseFeed f = getFeedObject(type, obj.toString());
                        if (f != null) {
                            baseFeeds.add(f);
                        }
                    }
                }
            }

        } catch (JSONException e) {

            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (BaseFeed f : baseFeeds) {
            if (f != null)
                Log.d("Base Feed Type", f.getType() + "");
        }

        return baseFeeds;
    }


}

