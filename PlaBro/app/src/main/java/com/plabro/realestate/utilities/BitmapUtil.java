package com.plabro.realestate.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;

/**
 * Created by nbansal2211 on 17/05/16.
 */
public class BitmapUtil {

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int getWindowWidth(Activity ctx) {
        DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        return width;
    }

    public static int getWindowHeight(Activity ctx) {
        DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
        int width = metrics.heightPixels;
        return width;
    }

    public static int convertDpToPixels(Activity ctx, int dimenId) {
        return ctx.getResources().getDimensionPixelOffset(dimenId);
    }

    public static Bitmap getBackgroundBitmap(Activity ctx, int heightDimenId, int drawableId) {
        int reqWidth = getWindowWidth(ctx);
        int reqHeight = convertDpToPixels(ctx, heightDimenId);
        return decodeSampledBitmapFromResource(ctx.getResources(), drawableId, reqWidth, reqHeight);
    }
}
