package com.plabro.realestate.models.profile;

import android.os.Bundle;
import android.text.SpannableString;

import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.propFeeds.BiddingFeed;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.JSONUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by nitin on 11/02/16.
 */
@Table(name = "BrokerPromote")
public class BrokerPromoteData extends Feeds {
    @SerializedName("desc")
    @Expose
    private String description;
    @SerializedName("sub_desc")
    @Expose
    private String sub_desc;
    @SerializedName("chat_text")
    @Expose
    private String chat_text;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("img")
    @Expose
    private String img;

    @SerializedName("phone")
    @Expose
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public String getChat_text() {
        return chat_text;
    }

    public void setChat_text(String chat_text) {
        this.chat_text = chat_text;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSub_desc() {
        return sub_desc;
    }

    public void setSub_desc(String sub_desc) {
        this.sub_desc = sub_desc;
    }

    private Bundle deepLinkBundle = new Bundle();

    public Bundle getDeepLinkBundle() {
        return deepLinkBundle;
    }

    public void setDeepLinkBundle(Bundle deepLinkBundle) {
        this.deepLinkBundle = deepLinkBundle;
    }

    public static BrokerPromoteData parseJson(String jsonString) {
        BrokerPromoteData feed = (BrokerPromoteData) JSONUtils.parseJson(jsonString, BrokerPromoteData.class);
        Log.d("BrokerPromote Feed", "json string :" + jsonString);
        try {
            JSONObject object = new JSONObject(jsonString);
            JSONArray paramsArray = object.getJSONArray("deeplink_params");
            if (null != paramsArray && paramsArray.length() > 0) {
                for (int i = 0; i < paramsArray.length(); i++) {
                    JSONObject param = paramsArray.getJSONObject(i);
                    Iterator<String> it = param.keys();
                    while (it.hasNext()) {
                        String key = it.next();
                        String value = param.getString(key);
                        feed.getDeepLinkBundle().putString(key, value);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return feed;
    }


}
