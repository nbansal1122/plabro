package com.plabro.realestate.models.auctions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Data {

    @SerializedName("page_action_title")
    @Expose
    private String page_action_title;
    @SerializedName("page_action_url")
    @Expose
    private String page_action_url;
    @SerializedName("page_title")
    @Expose
    private String page_title;
    @SerializedName("req_id")
    @Expose
    private String req_id;
    @SerializedName("page_action_form")
    @Expose
    private List<Page_action_form> page_action_form = new ArrayList<Page_action_form>();
    @SerializedName("_id")
    @Expose
    private Integer _id;
    @SerializedName("sections")
    @Expose
    private List<Section> sections = new ArrayList<Section>();
    @SerializedName("auction_header")
    @Expose
    private AuctionHeader auction_header;
    @SerializedName("auction_info")
    @Expose
    private AuctionInfo auction_info;

    public AuctionHeader getAuction_header() {
        return auction_header;
    }

    public void setAuction_header(AuctionHeader auction_header) {
        this.auction_header = auction_header;
    }

    public AuctionInfo getAuction_info() {
        return auction_info;
    }

    public void setAuction_info(AuctionInfo auction_info) {
        this.auction_info = auction_info;
    }

    /**
     * @return The page_action_title
     */
    public String getPage_action_title() {
        return page_action_title;
    }

    /**
     * @param page_action_title The page_action_title
     */
    public void setPage_action_title(String page_action_title) {
        this.page_action_title = page_action_title;
    }

    /**
     * @return The page_action_url
     */
    public String getPage_action_url() {
        return page_action_url;
    }

    /**
     * @param page_action_url The page_action_url
     */
    public void setPage_action_url(String page_action_url) {
        this.page_action_url = page_action_url;
    }

    /**
     * @return The page_title
     */
    public String getPage_title() {
        return page_title;
    }

    /**
     * @param page_title The page_title
     */
    public void setPage_title(String page_title) {
        this.page_title = page_title;
    }

    /**
     * @return The req_id
     */
    public String getReq_id() {
        return req_id;
    }

    /**
     * @param req_id The req_id
     */
    public void setReq_id(String req_id) {
        this.req_id = req_id;
    }

    /**
     * @return The page_action_form
     */
    public List<Page_action_form> getPage_action_form() {
        return page_action_form;
    }

    /**
     * @param page_action_form The page_action_form
     */
    public void setPage_action_form(List<Page_action_form> page_action_form) {
        this.page_action_form = page_action_form;
    }

    /**
     * @return The _id
     */
    public Integer get_id() {
        return _id;
    }

    /**
     * @param _id The _id
     */
    public void set_id(Integer _id) {
        this._id = _id;
    }

    /**
     * @return The sections
     */
    public List<Section> getSections() {
        return sections;
    }

    /**
     * @param sections The sections
     */
    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

}