package com.plabro.realestate.models.propFeeds.listings;

import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.utilities.JSONUtils;

import java.io.Serializable;

@Table(name = "ListingFeed")
public class ListingFeed extends Feeds implements Serializable {

    @SerializedName("owner_name")
    @Expose
    private String owner_name;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("detailed_view")
    @Expose
    private ListingDetail detailed_view;
    @SerializedName("original")
    @Expose
    private String original;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("bookmark_enabled")
    @Expose
    private boolean bookmark_enabled;
    @SerializedName("payment_status")
    @Expose
    private boolean payment_status;
    @SerializedName("customer_care_phone")
    @Expose
    private String customer_care_phone;
    @SerializedName("img")
    @Expose
    private String img;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getCustomer_care_phone() {
        return customer_care_phone;
    }

    public void setCustomer_care_phone(String customer_care_phone) {
        this.customer_care_phone = customer_care_phone;
    }

    public boolean isPayment_status() {
        return payment_status;
    }

    public void setPayment_status(boolean payment_status) {
        this.payment_status = payment_status;
    }

    public boolean isBookmark_enabled() {
        return bookmark_enabled;
    }

    public void setBookmark_enabled(boolean bookmark_enabled) {
        this.bookmark_enabled = bookmark_enabled;
    }

    /**
     * @return The owner_name
     */
    public String getOwner_name() {
        return owner_name;
    }

    /**
     * @param owner_name The owner_name
     */
    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public ListingDetail getDetailed_view() {
        return detailed_view;
    }

    public void setDetailed_view(ListingDetail detailed_view) {
        this.detailed_view = detailed_view;
    }

    /**
     * @return The original
     */
    public String getOriginal() {
        return original;
    }

    /**
     * @param original The original
     */
    public void setOriginal(String original) {
        this.original = original;
    }

    /**
     * @return The desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc The desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static ListingFeed parseJson(String json) {
        ListingFeed feed = (ListingFeed) JSONUtils.parseJson(json, ListingFeed.class);
        Log.d("Listing Feed", "Feed JSon:" + json);
        Log.d("Listing Feed", "Feed Type:" + feed.getType());
        return feed;
    }
}
