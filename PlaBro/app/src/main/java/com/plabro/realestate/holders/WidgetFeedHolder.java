package com.plabro.realestate.holders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.WidgetsFeed;
import com.plabro.realestate.models.services.Widget;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.receivers.PBLocalReceiver;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.DeepLinkNavigationUtil;
import com.plabro.realestate.utilities.PBPreferences;

import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 25/03/16.
 */
public class WidgetFeedHolder extends BaseFeedHolder implements PBLocalReceiver.PBActionListener {
    private LinearLayout widgetContainer;
    private int badgeCount = 0, chatCount = 0;

    private BaseFeed feed;
    private int position;

    public WidgetFeedHolder(View itemView) {
        super(itemView);
        widgetContainer = (LinearLayout) itemView.findViewById(R.id.ll_launchers);
    }

    @Override
    public void onBindViewHolder(final int position, BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        this.feed = feed;
        this.position = position;
        if (feed instanceof WidgetsFeed) {
            PBLocalReceiver receiver = new PBLocalReceiver(this);
            initViews();
        }
    }

    private void initViews() {
        final WidgetsFeed f = (WidgetsFeed) feed;
        List<Widget> widgets = f.getWidgets();
        widgetContainer.removeAllViewsInLayout();
        for (Widget w : widgets) {
            View row = getWidgetRow(w, position);
            widgetContainer.addView(row);
        }
    }

    private View getWidgetRow(final Widget w, final int position) {
        View row = LayoutInflater.from(ctx).inflate(R.layout.row_widget, null);
        TextView tv = (TextView) row.findViewById(R.id.tv_widget);
        TextView badgeCountTV = (TextView) row.findViewById(R.id.tv_badgeCount);

        badgeCount = PBPreferences.getData(PBPreferences.NOTIF_BADGE_COUNT, 0);
        chatCount = PBPreferences.getData(PBPreferences.CHAT_BADGE_COUNT, 0);
        badgeCountTV.setVisibility(View.GONE);
        if (w.getText().toUpperCase().startsWith("CHAT")) {
            if (chatCount > 0) {
                badgeCountTV.setVisibility(View.VISIBLE);
                badgeCountTV.setText(chatCount + "");
            }
        }
        if (w.getBadgeCount() > 0) {
            badgeCountTV.setVisibility(View.VISIBLE);
            badgeCountTV.setText(w.getBadgeCount() + "");
        }
        if (w.getText().toUpperCase().startsWith("NOTIF")) {
            if (badgeCount > 0) {
                badgeCountTV.setVisibility(View.VISIBLE);
                badgeCountTV.setText(badgeCount + "");
            }
        }

        ImageView iv = (ImageView) row.findViewById(R.id.iv_widget);
        ActivityUtils.loadImage(ctx, w.getImage(), iv, 0);
        tv.setText(w.getText());
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Launcher Clicked");
                trackEvent(Analytics.CardActions.LauncherClicked, position, w.getText(), "widgets", null);
                w.setBadgeCount(0);
                Bundle b = DeepLinkNavigationUtil.getMap(w.getDeepLink());

                if (b != null)
                    DeepLinkNavigationUtil.navigate(b, ctx);
                if (w.getText().toUpperCase().startsWith("NOTIF")) {
                    PBPreferences.saveData(PBPreferences.NOTIF_BADGE_COUNT, 0);
                }
                if (w.getText().toUpperCase().startsWith("CHAT")) {
                    PBPreferences.saveData(PBPreferences.CHAT_BADGE_COUNT, 0);
                }
                view.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sendBroadcast();
                    }
                }, 3000);

            }
        });
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
        row.setLayoutParams(params);
        return row;
    }

    private void sendBroadcast() {

        Intent i = new Intent(Constants.ACTION_FEED_REFRESH);
        ctx.sendBroadcast(i);
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        Log.d("WidgetFeed", "on Received:" + intent.getAction());
        if (PBLocalReceiver.PBActionListener.ACTION_BADGE_UPDATE.equalsIgnoreCase(intent.getAction())) {
            initViews();
        }
    }
}
