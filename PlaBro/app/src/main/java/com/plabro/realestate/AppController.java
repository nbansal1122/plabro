package com.plabro.realestate;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap.CompressFormat;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Base64;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.activeandroid.query.Select;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.appsflyer.AppsFlyerLib;
import com.clevertap.android.sdk.ActivityLifecycleCallback;
import com.crashlytics.android.Crashlytics;
import com.digits.sdk.android.Digits;
import com.facebook.FacebookSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.entities.BookmarksDBModel;
import com.plabro.realestate.entities.FeedsDBModel;
import com.plabro.realestate.entities.MyDraftsDBModel;
import com.plabro.realestate.entities.MyShoutsDBModel;
import com.plabro.realestate.models.Calls.CallInfo;
import com.plabro.realestate.models.Calls.CallData;
import com.plabro.realestate.models.Endorsement;
import com.plabro.realestate.models.Tracker.WifiDBModel;
import com.plabro.realestate.models.Tracker.WifiData;
import com.plabro.realestate.models.XMPP.RosterData;
import com.plabro.realestate.models.chat.ChatMessage;
import com.plabro.realestate.models.city.CityClass;
import com.plabro.realestate.models.contacts.ContactInfo;
import com.plabro.realestate.models.contacts.NonPlabroContact;
import com.plabro.realestate.models.contacts.PlabroFriends;
import com.plabro.realestate.models.login.Login;
import com.plabro.realestate.models.notifications.Notification;
import com.plabro.realestate.models.notifications.NotificationAction;
import com.plabro.realestate.models.payment.TransactionData;
import com.plabro.realestate.models.profile.BrokerPromoteData;
import com.plabro.realestate.models.profile.Invitee;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.propFeeds.AdvertisementFeed;
import com.plabro.realestate.models.propFeeds.BiddingFeed;
import com.plabro.realestate.models.propFeeds.BuilderFeed;
import com.plabro.realestate.models.propFeeds.EndorsementNotificationData;
import com.plabro.realestate.models.propFeeds.ExternalSourceFeed;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.models.propFeeds.GreetingFeed;
import com.plabro.realestate.models.propFeeds.LoaderFeed;
import com.plabro.realestate.models.propFeeds.MultiBrokerFeed;
import com.plabro.realestate.models.propFeeds.NewsFeed;
import com.plabro.realestate.models.propFeeds.ShareFeed;
import com.plabro.realestate.models.propFeeds.WidgetsFeed;
import com.plabro.realestate.models.propFeeds.listings.ListingFeed;
import com.plabro.realestate.models.propFeeds.tabs.TabInfo;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.KeyBoardSuggestions;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.DeviceResourceManager;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.utils.DigitVerification;
import com.plabro.realestate.volley.AppVolleyCacheManager;
import com.plabro.realestate.volley.ImageCacheManager;
import com.plabro.realestate.volley.ImageCacheManager.CacheType;
import com.plabro.realestate.volley.LruBitmapCache;

import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.List;

import io.fabric.sdk.android.Fabric;


/**
 * Created by hemantkumar on 07/01/15.
 */
public class AppController extends MultiDexApplication {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
//    private static final String TWITTER_KEY = "wK4OikHSxJrsdnqamgGSt5Pz9";
//    private static final String TWITTER_SECRET = "9LhVj26Jle5fyzgAQheMJUNssQDDCP3BvR1jVQN1xKbKIz9Y0B";


    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "C37xmIYifaAgnoaRQ8zCVKdID";
    private static final String TWITTER_SECRET = "T967BnhknKtlEyNEgdLyZVCoj3Jp0kxmj1ieJLWgrKfhpfeAEp";

    public static boolean isDebugMode;
    private Handler handler;
    Boolean mRegStatus;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private PackageInfo packageInfo;

    public static final String TAG = AppController.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static AppController mInstance;
    private static Context mContext;
    private static boolean activityVisible;
    public ProfileClass userProfile;
    private static GoogleAnalytics mGA;
    private static Tracker mTracker;
    private static DeviceResourceManager mDeviceResourceManager;
    private LruBitmapCache mImageCache;
    private Crashlytics crashlytics;
    public boolean isDatabaseInitialized;
    public static HashSet<String> rtSet = new HashSet<>();

    public String getCurrentChatPhoneNumber() {
        return currentChatPhoneNumber;
    }

    public void setCurrentChatPhoneNumber(String currentChatPhoneNumber) {
        this.currentChatPhoneNumber = currentChatPhoneNumber;
    }

    private String currentChatPhoneNumber;


    @Override
    public void onCreate() {
        ActivityLifecycleCallback.register(this);
        super.onCreate();
        mContext = this;
        mInstance = this;
        isDebugMode();
        initRtSet();
        //appsflyer init

       initAppsFlyer();
        //facebook init
        FacebookSdk.sdkInitialize(this);

        if (!BuildConfig.DEBUG) {
            boolean debugMode = PBPreferences.getIsDebugEnable();
            if (!debugMode) {

                initializeFabric();

            } else {
//                AppsFlyerLib.getInstance().setDeviceTrackingDisabled(false);
            }
        } else {
//            AppsFlyerLib.setDeviceTrackingDisabled(true);
        }
        boolean ftUser = PBPreferences.getFirstTimeState();
        if (!ftUser) {
//            AppsFlyerLib.setDeviceTrackingDisabled(true);
        }

        if (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE) && hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            initDB();
            isDatabaseInitialized = true;
        } else {
            isDatabaseInitialized = false;
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        init();
        Log.d(TAG, "EG ID:" + PBPreferences.getGCMRegistrarId());
        DigitVerification.getInstance(this).initDigitVerification();
        PBPreferences.initContactsMap();
//        printHash();
        //  AppLaunchDetectorService.startAppLaunchDetectorService(this); //todo turn this on to start whatsapp and other launcher detector
    }

    private void initAppsFlyer() {
        AppsFlyerLib.getInstance().setAppId(getResources().getString(R.string.app_flyer_dev_key));
//        AppsFlyerLib.setAppsFlyerKey(getResources().getString(R.string.app_flyer_dev_key));
        AppsFlyerLib.getInstance().setGCMProjectNumber(getResources().getString(R.string.gcm_sender_id));
        AppsFlyerLib.getInstance().startTracking(this, getResources().getString(R.string.app_flyer_dev_key));
        AppsFlyerLib.getInstance().setDeviceTrackingDisabled(false);
    }

    private void testDecryption() {
        String decryptedData = AppVolleyCacheManager.getDecryptedData("");
        Log.d(TAG, "Decrypted Data" + decryptedData);
    }

//    static{
//        rtSet.add(PlaBroApi.RT.WIFI_DATA);
//        rtSet.add(PlaBroApi.RT.SAVE_CALL_LOGS);
//        rtSet.add(PlaBroApi.RT.USER_CONTACTS_FEED);
//        rtSet.add(PlaBroApi.RT.GET_NON_PLABRO_FRIENDS);
//    }
    private void initRtSet() {
        rtSet.add(PlaBroApi.RT.WIFI_DATA);
        rtSet.add(PlaBroApi.RT.SAVE_CALL_LOGS);
        rtSet.add(PlaBroApi.RT.USER_CONTACTS_FEED);
        rtSet.add(PlaBroApi.RT.GET_NON_PLABRO_FRIENDS);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void initializeFabric() {
        crashlytics = new Crashlytics();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, crashlytics, new Crashlytics(), new TwitterCore(authConfig), new Digits());
        Log.isEnabled = false;
    }


    public void initDB() {
        try {
            initDB(Utility.getFolderPath());
            // Log.d(TAG, "DB Created At:" + ActiveAndroid.getDatabase().getPath());
        } catch (Exception e) {
            Log.i(TAG, "Exception initDB");
            initDB("");
            e.printStackTrace();
        }
    }

    private void printHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.plabro.realestate", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void initDB(String folderPath) {
        ActiveAndroid.dispose();
        Configuration.Builder builder = new Configuration.Builder(AppController.this);
        builder.setDatabaseName(folderPath + "plabro_aa.db");
        builder.setDatabaseVersion(77);
        builder.addModelClass(WidgetsFeed.class);
        builder.addModelClass(ExternalSourceFeed.class);
        builder.addModelClass(TransactionData.class);
        builder.addModelClass(BrokerPromoteData.class);
        builder.addModelClass(AdvertisementFeed.class);
        builder.addModelClass(ListingFeed.class);
        builder.addModelClass(BiddingFeed.class);
        builder.addModelClass(NotificationAction.class);
        builder.addModelClass(CallData.class);
        builder.addModelClass(Login.class);
        builder.addModelClass(ShareFeed.class);
        builder.addModelClass(EndorsementNotificationData.class);
        builder.addModelClass(NonPlabroContact.class);
        builder.addModelClass(TabInfo.class);
        builder.addModelClass(MultiBrokerFeed.class);
        builder.addModelClass(NewsFeed.class);
        builder.addModelClass(FeedsDBModel.class);
        builder.addModelClass(BookmarksDBModel.class);
        builder.addModelClass(Endorsement.class);
        builder.addModelClass(MyShoutsDBModel.class);
        builder.addModelClass(ChatMessage.class);
        builder.addModelClass(ProfileClass.class);
        builder.addModelClass(Feeds.class);
        builder.addModelClass(WifiData.class);
        builder.addModelClass(WifiDBModel.class);
        builder.addModelClass(PlabroFriends.class);
        builder.addModelClass(ContactInfo.class);
        builder.addModelClass(RosterData.class);
        builder.addModelClass(Invitee.class);
        builder.addModelClass(CallInfo.class);
        builder.addModelClass(CityClass.class);
        builder.addModelClass(BuilderFeed.class);
        builder.addModelClass(com.plabro.realestate.models.history.History.class);
        builder.addModelClass(Notification.class);
        builder.addModelClass(LoaderFeed.class);
        builder.addModelClass(MyDraftsDBModel.class);
        builder.addModelClass(GreetingFeed.class);
        ActiveAndroid.initialize(builder.create());
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    private void init() {
        if (checkPlayServices()) {
            initializeGA();
        }
        getRequestQueue();
//        createImageCache();
        Analytics.initialize(this);
        KeyBoardSuggestions.initialize(this);

        // initGcmServices();

    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
//                GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity)this.getApplicationContext(),
//                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("PLABRO", "This device is not supported.");
            }
            return false;
        }
        return true;
    }


    private void initializeGA() {
        try {
            mGA = GoogleAnalytics.getInstance(this);
            mGA.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
            Log.d(TAG, mGA.getAppOptOut() + ":AppOptOut");
            mGA.setDryRun(false);
            mTracker = mGA.newTracker(getResources().getString(R.string.ga_test_tracking_id));
            mTracker.setAppName("PlaBro");
            mTracker.enableAdvertisingIdCollection(true);
//            mTracker.enableAutoActivityTracking(true);

            String phone = PBPreferences.getPhone();
            if (null != phone) {
                mTracker.set("&uid", phone);
            }
            mGA.setLocalDispatchPeriod(15);
            mGA.dispatchLocalHits();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Tracker getGaTracker() {
        return mTracker;
    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public String getCityName() {
        ProfileClass profile = getUserProfile();
        if (null != profile && !TextUtils.isEmpty(profile.getCity_name())) {
            Log.d(TAG, "CityName :" + profile.getCity_name());
            return profile.getCity_name() + " Feeds";
        } else {
            return "Plabro";
        }
    }

    public String getCityNameOnly() {
        ProfileClass profile = getUserProfile();
        if (null != profile && !TextUtils.isEmpty(profile.getCity_name())) {
            Log.d(TAG, "CityName :" + profile.getCity_name());
            return profile.getCity_name();
        } else {
            return null;
        }
    }

    public String getCityId() {
        ProfileClass profile = getUserProfile();
        if (null != profile && !TextUtils.isEmpty(profile.getCity_id())) {
            return profile.getCity_id();
        } else {
            return "1";
        }
    }

    public ImageLoader getImageLoader() {
        //getRequestQueue();
        getRequestQueueWithCache();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue, new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    private void createImageCache() {
        int DISK_IMAGECACHE_SIZE = 1024 * 1024 * 10;
        CompressFormat DISK_IMAGECACHE_COMPRESS_FORMAT = CompressFormat.PNG;
        int DISK_IMAGECACHE_QUALITY = 100;

        ImageCacheManager.getInstance().init(this, this.getPackageCodePath(), DISK_IMAGECACHE_SIZE, DISK_IMAGECACHE_COMPRESS_FORMAT,
                DISK_IMAGECACHE_QUALITY, CacheType.MEMORY);
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


    public RequestQueue getRequestQueueWithCache() {
        if (mRequestQueue == null) {

            Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap
            Network network = new BasicNetwork(new HurlStack());

            mRequestQueue = new RequestQueue(cache, network);
            mRequestQueue.start();
        }

        return mRequestQueue;
    }


    public AppController() {
        super();
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    public ProfileClass getUserProfile() {
        if (null == userProfile) {
            try {
                String whereCondition = "phone = '" + PBPreferences.getPhone() + "'";
                List<ProfileClass> profiles = new Select().from(ProfileClass.class).where(whereCondition).execute();
                if (null != profiles && profiles.size() > 0) {
                    userProfile = profiles.get(profiles.size() - 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return userProfile;
            }
        }
        return userProfile;
    }

    public void setUserProfile(ProfileClass userProfile) {
        this.userProfile = userProfile;
        sendProfileUpdateBroadcast();
    }

    public void sendProfileUpdateBroadcast() {
        Intent i = new Intent();
        i.setAction(Constants.ACTION_PROFILE_UPDATE);
        sendBroadcast(i);
    }

    public void showNewIMsPopup() {
        Boolean newIm = PBPreferences.getNewIMOnApp();
        if (newIm) {
            // Home.setUpChatNotification("new",true);
        } else {
            // Home.setUpChatNotification("new",false);
        }
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.d(TAG, "OnTerminate");
    }

    public static DeviceResourceManager getDeviceResourceManager() {
        if (mDeviceResourceManager == null)
            mDeviceResourceManager = new DeviceResourceManager(mContext);

        return mDeviceResourceManager;
    }

    public void deleteImageCache() {
        if (mImageCache != null) {
            mImageCache.evictAll();
        }
    }


    public PackageInfo getPackageInfo() {
        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return packageInfo;
    }

    public static boolean isDebugMode() {
        isDebugMode = (0 != (AppController.getInstance().getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));
        return isDebugMode;
    }

    public Crashlytics getCrashlytics() {
        return crashlytics;
    }

    public void setCrashlytics(Crashlytics crashlytics) {
        this.crashlytics = crashlytics;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) {
            return true;
        }


        if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            //permission is already available, proceed normally
            return true;

        }
        return false;
    }

    public void clearUserAndLogout() {
        //clear preferences of user
        PBPreferences.clearAll();
        //clear the tables available in db
        //new Delete().from(MyClass.class).executeSingle();

    }

    /**
     * Current Activity instance will go through its lifecycle to onDestroy() and a new instance then created after it.
     */
    @SuppressLint("NewApi")
    public static final void recreateActivityCompat(final Activity a) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            a.recreate();
        } else {
            final Intent intent = a.getIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            a.finish();
            a.overridePendingTransition(0, 0);
            a.startActivity(intent);
            a.overridePendingTransition(0, 0);
        }
    }
}
