package com.plabro.realestate.fragments.services;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.plabro.realestate.R;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.models.services.Question;
import com.plabro.realestate.others.Analytics;

import org.json.JSONArray;

import java.util.HashMap;

/**
 * Created by nitin on 24/11/15.
 */
public abstract class BaseServiceFragment extends PlabroBaseFragment {

    private LinearLayout layout;
    protected Question q;
    private String serviceResponse;
    protected String selectedValue;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void findViewById() {
        layout = (LinearLayout) findView(R.id.ll_content);
        setText(R.id.tv_question, q.getTitle());
    }

    protected void addLayout(View v) {
        layout.addView(v);
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_base_service;
    }

    public static BaseServiceFragment getServiceFragment(Question q) {
        BaseServiceFragment f = null;
        switch (q.getAns_ui_type()) {
            case "radio_button":
                f = new Radio();
                break;
            case "text":
                f = new EditText();
                break;
            case "dropdonw":
            case "dropdown":
                f = new DropDown();
                break;

        }
        if (null != f) {
            f.q = q;
        }
        return f;
    }

    public boolean onNextClicked() {
        if (TextUtils.isEmpty(getSelectedValue())) {
            showErrorMessage();
            return false;
        }

        return true;
    }

    protected void showErrorMessage() {
        showToast(getErrorMessage());
    }

    protected String getErrorMessage() {
        return "";
    }

    protected String getSelectedValue() {
        return null;
    }

    public JSONArray getServiceResponse() {
        JSONArray array = new JSONArray();
        array.put(getSelectedValue());
        return array;
    }
}
