package com.plabro.realestate.listeners;

import com.plabro.realestate.CustomLogger.Log;

import java.util.Map;
import java.util.WeakHashMap;

/**
 * Created by nitin on 24/08/15.
 */
public class GenericObserver {
    private static String TAG = "GenericObserver";
    private static GenericObserver observer = new GenericObserver();
    private static WeakHashMap<GenericObserverInterface, String> map = new WeakHashMap<>();
//    private static List<GenericObserverInterface> list = new ArrayList<>();


    public static GenericObserver getInstance() {
        return observer;
    }

    private GenericObserver() {
    }

    public static interface GenericObserverInterface {
        String EVENT_AUTHKEY_UPDATE = "AuthKeyUpdate";

        public void onEventUpdate(String event);

    }

    public void addListener(GenericObserverInterface listener, String eventType) {
        map.put(listener, eventType);
    }

    public void remove(GenericObserverInterface listener) {
        if (map.containsKey(listener))
            map.remove(listener);
    }

    public static void dispatchEvent(String event) {
        Log.v(TAG, "inside shout dispatch");
        for (Map.Entry<GenericObserverInterface, String> entry : map.entrySet()) {
            if (entry.getValue().equalsIgnoreCase(event)) {
                if (null != entry.getKey()) {
                    entry.getKey().onEventUpdate(event);
                }
            }
        }

    }
}
