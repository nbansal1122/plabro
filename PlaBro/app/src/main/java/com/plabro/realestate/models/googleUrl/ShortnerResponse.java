package com.plabro.realestate.models.googleUrl;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class ShortnerResponse {

    String kind;
    String id;
    String longUrl;

    public String getKind() {
        return kind;
    }

    public String getId() {
        return id;
    }

    public String getLongUrl() {
        return longUrl;
    }
}
