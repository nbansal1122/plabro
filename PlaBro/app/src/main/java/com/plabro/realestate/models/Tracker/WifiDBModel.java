package com.plabro.realestate.models.Tracker;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.utils.Serializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmd on 5/14/2015.
 */
@Table(name = "WifiDBModel", id = BaseColumns._ID)
public class WifiDBModel extends Model {
    @Column
    private byte[] wifiTracker;

    public WifiDBModel()
    {

    }

    public static void insertTracker(WifiTracker data) {
        WifiDBModel model = new WifiDBModel();
        try {
            model.wifiTracker = Serializer.serialize(data);

        } catch (Exception e) {
            Log.i("FeedsDBModel", "Exception while serializing data");
        }
        model.save();
    }

    public static List<WifiTracker> getTrackers(List<WifiDBModel> list) {
        List<WifiTracker> trackerList = new ArrayList<WifiTracker>();
        for (WifiDBModel model : list) {
            try {
                WifiTracker tracker = (WifiTracker) Serializer.deserialize(model.wifiTracker);
                trackerList.add(tracker);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return trackerList;
    }


}
