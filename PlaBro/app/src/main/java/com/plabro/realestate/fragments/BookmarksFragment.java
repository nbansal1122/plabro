package com.plabro.realestate.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.adapter.FeedsCustomAdapter;
import com.plabro.realestate.entities.BookmarksDBModel;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.OnUpdateListener;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by hemant on 29/08/15.
 */
public class BookmarksFragment extends CardListFragment implements HomeFragment.CityRefreshListener, ShoutObserver.PBObserver {

    FeedsCustomAdapter adapter;
    boolean addMoreGlobal = false;
    private static Toolbar mToolbar;
    ArrayList<BaseFeed> tempFeeds;
    private static SearchBoxCustom search;


    @Override
    public String getTAG() {
        return "BookmarksFragment";
    }

    public static Fragment getInstance(Toolbar mToolbar, SearchBoxCustom searchbox) {
        BookmarksFragment f = new BookmarksFragment();
        f.mToolbar = mToolbar;
        f.search = searchbox;

        return f;
    }

    @Override
    protected void findViewById() {
        registerLocalReceiver();
        super.findViewById();
        Analytics.trackScreen(Analytics.OtherScreens.Bookmarks);
    }


    @Override
    public FeedsCustomAdapter getAdapter() {
        if (null == adapter) {
            adapter = new FeedsCustomAdapter(baseFeeds, "bookmarked", getActivity(), bookmarkDeleteListener);
        }
        return adapter;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflateToolbar(menu);

    }

    private void inflateToolbar(Menu menu) {
        mToolbar.inflateMenu(R.menu.menu_contacts);
        Log.v(TAG, "toolbar inflated");

        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.action_search:
                        openSearch();
                        break;

                    default:
                        break;

                }
                return true;
            }

        });
    }


    public void openSearch() {
        search.clearSearchable(); // clear all the searchable history previously present

        search.revealFromMenuItem(R.id.action_search, getActivity());
        search.setMenuListener(new SearchBoxCustom.MenuListener() {

            @Override
            public void onMenuClick() {
                if (search.isShown()) {
                    search.hideCircularly(getActivity());
                }

            }

        });
        search.setSearchListener(new SearchBoxCustom.SearchListener() {


            @Override
            public void onSearchOpened() {
                // Use this to tint the screen

            }

            @Override
            public void onSearchClosed() {
                // Use this to un-tint the screen
                if (adapter != null) {
                    adapter.filter("");
                }
            }

            @Override
            public void onSearchTermChanged(String searchTerm) {
                // React to the search term changing
                // Called after it has updated results
                if (adapter != null) {
                    adapter.filter(searchTerm);
                }
            }


            @Override
            public void onSearch(String searchTerm) {

                if (adapter != null) {
                    adapter.filter(searchTerm);
                }
            }

            @Override
            public void onSearchCleared() {
                if (adapter != null)
                    adapter.filter("");
            }

        });

    }


    @Override
    public void getData(final boolean addMore) {
        addMoreGlobal = addMore;
        showSpinners(addMore);
        HashMap<String, String> params = new HashMap<>();
        if (addMore) {
            params.put(Constants.START_IDX, "" + page);
        } else {
            params.put(Constants.START_IDX, "0");
        }

        params = Utility.getInitialParams(PlaBroApi.RT.BOOKMARKSLIST, params);

        AppVolley.processRequest(Constants.TASK_CODES.BOOKMARKSLIST, FeedResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                stopSpinners();
                FeedResponse feedResponse = (FeedResponse) response.getResponse();
                if (null != feedResponse.getOutput_params()) {
                    noFeeds.setVisibility(View.GONE);
                    tempFeeds = (ArrayList<BaseFeed>) feedResponse.getOutput_params().getData();
                    Log.d(TAG, "Temp Feeds Size:" + tempFeeds.size());
                    if (tempFeeds.size() > 0) {

                        for (BaseFeed feed : tempFeeds) {
                            BookmarksDBModel.insertFeed(feed);
                        }
//                            if (!addMore) {
//                                page = 0;
//                                baseFeeds.clear();
//                            }


                        adapter.updateItems(tempFeeds, addMore);
                        if (addMore) {
                            baseFeeds.addAll(tempFeeds);
//                            adapter.notifyDataSetChanged();
                        } else {
                            page = 0;
                            baseFeeds.clear();
                            baseFeeds.addAll(tempFeeds);
//                            if (adapter != null) {
//                                adapter.notifyDataSetChanged();
//                            }
                        }
                        page = Util.getStartIndex(page);

                        //page scrolled for next set of data
                        HashMap<String, Object> data = new HashMap<>();
                        data.put("ScreenName", TAG);
                        data.put("Action", "Manual");
                        showNoFeeds();

                        tempFeeds.clear();
                        adapter.filter(adapter.getSearchText());

                    }
                }

                if (null != baseFeeds && baseFeeds.size() > 0) {
                    noFeeds.setVisibility(View.GONE);
                } else {
                    noFeeds.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                onApiFailure(response, taskCode);
                stopSpinners();
                if (null != baseFeeds && baseFeeds.size() > 0) {
                    noFeeds.setVisibility(View.GONE);
                } else {
                    noFeeds.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private void setAdapter() {

    }

    @Override
    protected int getViewId() {
        return R.layout.feeds_fragment;
    }

    @Override
    public void onCityRefresh() {
        getData(false);
    }

    @Override
    public void onShoutUpdate() {
        Log.d(TAG, "OnShoutUpdate");
        getData(false);
    }

    @Override
    public void onShoutUpdateGen(String type) {

    }

    @Override
    public void onTrialVersionExpired(PBResponse response, int taskCode) {
        super.onTrialVersionExpired(response, taskCode);

        if (null != adapter && adapter.getItemCount() > 0) {
            showRegistrationCard();
        }
    }

    @Override
    public void onStatusFalse(PBResponse response, int taskCode) {
        super.onStatusFalse(response, taskCode);
        if (null != adapter && adapter.getItemCount() > 0) {
            noFeeds.setVisibility(View.VISIBLE);
        } else {
            noFeeds.setVisibility(View.GONE);
        }

    }


    @Override
    protected void reloadRequest() {
        showSpinners(addMoreGlobal);
        page = 0;
        getData(false);
    }

    public void UpdateBookmarksFromOutside() {
        page = 0;
        getData(false);
    }

    OnUpdateListener bookmarkDeleteListener = new OnUpdateListener() {

        @Override
        public void onResponse(Boolean response, String val) {

        }

        @Override
        public void onResponse(Boolean response, int val) {

            if (response) {
                adapter.removeItem(val);
                adapter.notifyDataSetChanged();
                if (adapter.getItemCount() < 1) {
                    noFeeds.setVisibility(View.VISIBLE);
                }
            }

        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerLocalReceiver();
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Profile Action Received");
        if (Constants.ACTION_PROFILE_UPDATE.equals(intent.getAction())) {
            ((PlabroBaseActivity) getActivity()).setTitle("Bookmarks");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(null!=search&&search.isShown())
        {
            search.hideCircularly(getActivity());
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        Analytics.trackScreen(Analytics.OtherScreens.Bookmarks);
    }
}
