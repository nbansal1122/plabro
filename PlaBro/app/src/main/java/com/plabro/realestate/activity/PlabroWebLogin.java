package com.plabro.realestate.activity;

import android.Manifest;
import android.app.ProgressDialog;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.ServerResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

/**
 * Created by nitin on 15/03/16.
 */
public class PlabroWebLogin extends PlabroBaseFragment implements ZBarScannerView.ResultHandler {
    private ZBarScannerView mScannerView;
    boolean isResumed;

    @Override
    public String getTAG() {
        return "PlabroWebLogin";
    }

    @Override
    public void findViewById() {
        setTitle("Plabro Web");
        Analytics.trackScreen(Analytics.OtherScreens.WebLogin);
        mScannerView = (ZBarScannerView) findView(R.id.scannerView);
    }

    @Override
    protected int getViewId() {
        return R.layout.activity_scanner;
    }

    @Override
    public void onResume() {
        super.onResume();
        checkPermission();// Start camera on resume
    }

    private void checkPermission(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            new TedPermission(getActivity())
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            isResumed = true;
                            mScannerView.setResultHandler(PlabroWebLogin.this); // Register ourselves as a handler for scan results.
                            mScannerView.startCamera();
                        }

                        @Override
                        public void onPermissionDenied(ArrayList<String> arrayList) {
                            showToast("Allow permission to capture image");
                        }
                    })
                    .setDeniedMessage("Permission required by Plabro Scan QR Code")
                    .setPermissions(Manifest.permission.CAMERA)
                    .setRationaleMessage("Permission required by Plabro Scan QR Code")
                    .check();
        } else {
            isResumed = true;
            mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
            mScannerView.startCamera();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isResumed)
            mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
//        mScannerView.setupScanner();
//        mScannerView.startCamera();

        ParamObject p = new ParamObject();
        HashMap<String, String> params = new HashMap<>();
        params.put("qrcode", rawResult.getContents());
        p.setClassType(ServerResponse.class);
        p.setParams(Utility.getInitialParams(PlaBroApi.RT.UPDATE_QR_CODE, params));
        showDialog();
        AppVolley.processRequest(p, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "Response:" + response.getResponse());
                dismissDialog();
                hideViews(R.id.scannerView, R.id.tv_info_text);
                showViews(R.id.tv_text_after_scan);
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                dismissDialog();
                if (getActivity() != null) {
                    mScannerView.setupScanner();
                    mScannerView.startCamera();
                }
                Log.d(TAG, "Exception:" + response.getCustomException().getMessage());
            }
        });

        Log.d(TAG, rawResult.getContents()); // Prints scan results
        Log.d(TAG, rawResult.getBarcodeFormat().getName()); // Prints the scan format (qrcode, pdf417 etc.)
    }

    private ProgressDialog dialog;

    private void showDialog() {
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading");
        dialog.show();
    }

    private void dismissDialog() {
        if (null != getActivity())
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
    }
}
