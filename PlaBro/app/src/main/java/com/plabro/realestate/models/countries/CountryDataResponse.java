package com.plabro.realestate.models.countries;

import android.content.Context;

import com.plabro.realestate.AppController;
import com.plabro.realestate.models.city.CityClass;
import com.plabro.realestate.models.city.CityResponse;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.JSONUtils;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nbansal2211 on 24/07/17.
 */

public class CountryDataResponse {
    private List<Country> data = new ArrayList<>();

    public List<Country> getData() {
        return data;
    }

    public void setData(List<Country> data) {
        this.data = data;
    }

    public static List<Country> parseJson(String json) {
        CountryDataResponse response = (CountryDataResponse) JSONUtils.parseJson(json, CountryDataResponse.class);
        return response.getData();
    }

    public static List<Country> getCountryListFromAsset(Context context) {
        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open(Constants.FILE_COUNTRY_CODE_JSON);
            String json = Utility.convertStreamToString(inputStream);
            return parseJson(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static List<CityClass> getCitiesList(String fileName) {
        try {
            InputStream inputStream = AppController.getInstance().getAssets().open(fileName);
            String json = Utility.convertStreamToString(inputStream);
            if (Constants.FILE_SHOUT_CITY_JSON.equalsIgnoreCase(fileName)) {
                return CityResponse.parseJson(json, false);
            }
            return CityResponse.parseJson(json);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }
        return new ArrayList<>();
    }
}
