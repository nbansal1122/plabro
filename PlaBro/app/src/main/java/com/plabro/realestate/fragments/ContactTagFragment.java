package com.plabro.realestate.fragments;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.Contact;
import com.plabro.realestate.models.contacts.ContactInfo;
import com.plabro.realestate.utils.ContactsQuery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by jmd on 6/4/2015.
 */
public class ContactTagFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = ContactTagFragment.class.getSimpleName();
    HashSet<ContactInfo> contactSet = new HashSet<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "OnCreate");

        getLoaderManager().initLoader(0, null, this);

    }

    static final String[] CONTACTS_SUMMARY_PROJECTION = new String[]{
            ContactsContract.CommonDataKinds.Phone._ID,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "onCreateLoader");
        int loopCounter = 0;
        return new CursorLoader(getActivity(), ContactsQuery.CONTENT_URI,
                ContactsQuery.PROJECTION, ContactsQuery.SELECTION, null,
                ContactsQuery.SORT_ORDER);
    }

    HashSet<String> contactIds = new HashSet<>();

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        contactIds = ContactInfo.getContactIds();
        fetchContacts(cursor
        );
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void fetchContacts(final Cursor cursor) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (cursor != null && cursor.getCount() > 0) {

                    while (cursor.moveToNext()) {
                        Contact contact = new Contact();
                        contact.setContactUri(ContactsContract.Contacts.getLookupUri(
                                cursor.getLong(ContactsQuery.ID),
                                cursor.getString(ContactsQuery.LOOKUP_KEY)));
                        contact.setDisplayName(cursor.getString(ContactsQuery.DISPLAY_NAME));
                        String contactId =
                                cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        contact.setContactId(contactId);
                        List<String> userPhones = new ArrayList<String>();

                        //
                        //  Get all phone numbers.
                        //
                        ContentResolver cr = getActivity().getContentResolver();
                        Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                        while (phones.moveToNext()) {
                            String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                            int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                            userPhones.add(number);
                            switch (type) {
                                case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                    // do something with the Home number here...
                                    break;
                                case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                    Log.d(TAG, "Putting in contacts" + modifyContact(number) + "Vs " + contact.getDisplayName());
                                    String contactNumber = modifyContact(number);

                                    ContactInfo info = new ContactInfo();
                                    info.setDisplayName(cursor.getString(ContactsQuery.DISPLAY_NAME));
                                    info.setContactId(contactId);
                                    info.setContactNumber(contactNumber);
                                    if (!contactIds.contains(contactId)) {
                                        Log.d(TAG, "Saving Contact:" + contactNumber + ":" + info.getDisplayName());
                                        info.setIsSynced(false);
                                        info.save();
                                    } else {
                                        Log.d(TAG, "Contains Contact:" + contactNumber + ":" + info.getDisplayName());
                                    }


                                    break;
                                case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                    // do something with the Work number here...
                                    break;
                            }
                        }

                    }
                }
            }
        }).start();
    }


    private String modifyContact(String mobileNumber) {
        String mobile = mobileNumber.replace("(", "").replace(")", "").replace("-", "").replace(" ", "");
        return mobile;
    }
}
