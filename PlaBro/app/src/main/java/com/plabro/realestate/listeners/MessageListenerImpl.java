package com.plabro.realestate.listeners;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.chat.ChatMessage;
import com.plabro.realestate.models.notifications.Notification;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.PBConnectionManager;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlabroPushNotificationManager;
import com.plabro.realestate.utils.Hash;
import com.plabro.realestate.xmpp.ReadReceipts.ReadReceipt;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.ChatState;
import org.jivesoftware.smackx.ChatStateListener;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class MessageListenerImpl implements ChatStateListener {
    private static final String TAG = "MessageListenerImpl";
    Context ctx;
    Connection conn;
    private PBConnectionManager manager;
    private ChatStateChangeListener listener;
    private OnUpdateListener messageUpdateListener;

    @Override
    public void stateChanged(Chat chat, ChatState chatState) {
        Log.d(TAG, "ChatStateChanged");
        if (listener != null) {
            Log.d(TAG, "ChatStateChanged Listener is not null" + chat.getParticipant());
//            listener.onChatStateChanged(chat, chatState);
            Log.d(TAG, "Recvd Chat State Thread id:" + chat.getThreadID());
        }
    }

    public static interface ChatStateChangeListener {
        public void onChatStateChanged(Chat chat, ChatState chatState);
    }


    public MessageListenerImpl(Context ctx, ChatStateChangeListener listener, OnUpdateListener messageUpdateListener) {
        this.ctx = ctx;
        this.manager = PBConnectionManager.getInstance(AppController.getInstance(), null);
        this.conn = manager.getConnection();
        if (conn != null && conn.isConnected() && conn.isAuthenticated()) {
            Log.d(TAG, "Message Listemer Connection");
            DeliveryReceiptManager dm = DeliveryReceiptManager.getInstanceFor(conn);
//            dm.enableAutoReceipts();
            dm.registerReceiptReceivedListener(recieptListener);
        }
        this.listener = listener;
        this.messageUpdateListener = messageUpdateListener;

    }


    @Override
    public void processMessage(Chat chat, Message message) {
        Log.d(TAG, "Process Message:" + message.toXML());
        Log.d(TAG, "Process Message Type:" + message.getType());

        if (TextUtils.isEmpty(message.getBody())) {
            {
                Collection<PacketExtension> extensions = message.getExtensions();
                Iterator<PacketExtension> it = extensions.iterator();
                if (it.hasNext()) {
                    PacketExtension ext = it.next();
                    Log.d(TAG, "Extension Element Name:" + ext.getElementName());
                    Log.d(TAG, "Extension Namespace:" + ext.getElementName());
                    Log.d(TAG, "Extension XML:" + ext.toXML());
                    switch (ext.getElementName()) {
                        case ReadReceipt.ELEMENT:
                            PBConnectionManager.handleReadReceiptMessage(message, ext);
                            return;
                        case DeliveryReceipt.ELEMENT:
                            PBConnectionManager.handleDeliveryReceiptMessage(message, ext);
                            return;

                    }
                    ChatState chatState = ChatState.valueOf(ext.getElementName());
                    Log.d(TAG, "Chat State is:" + chatState);
//                    message.getXmlns().equalsIgnoreCase((new ChatStateExtension.Provider()).)
                    if (listener != null) {
                        if (!Message.Type.error.equals(message.getType())) {
                            listener.onChatStateChanged(chat, chatState);
                        } else {
                            listener.onChatStateChanged(chat, ChatState.gone);
                        }
                    }
                }
            }
        }

    }


    public void sendChatMessage(ChatMessage msg, Chat currentChat) {
        Message m = new Message();

        android.util.Log.d(TAG, "PacketId " + m.getPacketID());
        if (null != conn && conn.isConnected()) {
            try {
                m.setType(Message.Type.chat);
                m.setFrom(conn.getUser());
                String text = msg.getMsg();
                m.setBody(text);
                m.setTo(ActivityUtils.getTo(msg.getPhone()));
                m.addExtension(new DeliveryReceipt(m.getPacketID()));
                android.util.Log.i(TAG, "***Sending text [" + text + "] to [" + msg.getPhone() + "]");
                currentChat.sendMessage(m);
                msg.setStatus(ChatMessage.STATUS_SENT);
                msg.setMsgType(ChatMessage.MESSAGE_OUTGOING);
                Log.d(TAG, "Packet ID & Message Type before Sending:::" + msg.getPacketIdAndMessageType());
                Log.d(TAG, "Packet ID & Message Type Should be After Sending;;;" + m.getPacketID() + ChatMessage.MESSAGE_OUTGOING);
                new Update(ChatMessage.class).set("packetIdAndMessageType = '" + m.getPacketID() + ChatMessage.MESSAGE_OUTGOING + "'").where("packetIdAndMessageType = '" + msg.getPacketIdAndMessageType() + "'").execute();
                msg.setPacketId(m.getPacketID());
                msg.setPacketIdAndMessageType(m.getPacketID() + ChatMessage.MESSAGE_OUTGOING);
                msg.setMsgType(ChatMessage.MESSAGE_OUTGOING);
                msg.save();
                Log.d(TAG, "Packet ID & Message Type After Sending:::" + msg.getPacketIdAndMessageType());

            } catch (Exception e) {
                android.util.Log.d(TAG, "Exception Sending Message");
            }

        } else {
            android.util.Log.d(TAG, "Connection Error");

        }


        //msg.setPacketIdAndMessageType(m.getPacketID() + ChatMessage.MESSAGE_OUTGOING);
        //msg.save();
    }


//    @Override
//    public void onConnectionCreated(Connection conn) {
//        this.conn = conn;
//        DeliveryReceiptManager dm = DeliveryReceiptManager.getInstanceFor(conn);
//        dm.enableAutoReceipts();
//        dm.registerReceiptReceivedListener(recieptListener);
//    }

    private static DeliveryReceiptManager.ReceiptReceivedListener recieptListener = new DeliveryReceiptManager.ReceiptReceivedListener() {
        @Override
        public void onReceiptReceived(String s, String s1, String s2) {
            Log.d(TAG, "Message Received:" + s + "... " + s1 + "... " + s2);
            String packetIdAndMessageType = s2 + ChatMessage.MESSAGE_OUTGOING;
            List<ChatMessage> chatMessages = new Select().from(ChatMessage.class).where("packetIdAndMessageType = '" + packetIdAndMessageType + "'").execute();
            if (chatMessages != null && chatMessages.size() > 0) {
                for (ChatMessage m : chatMessages) {
                    m.setStatus(ChatMessage.STATUS_DELIVERED);
                    m.save();
                }
            }

        }
    };

    public static void sendLocalPush(String msg, String phone, Context ctx) {
        boolean sendLocalPushOrNot = true;
        String currentChatPhone = AppController.getInstance().getCurrentChatPhoneNumber();
        currentChatPhone = ActivityUtils.getLastTenDigits(currentChatPhone);
        String last10Phone = ActivityUtils.getLastTenDigits(phone);
        if (null != currentChatPhone) {
            if (currentChatPhone.equals(last10Phone)) {
                sendLocalPushOrNot = false;
            }
        }
        if (sendLocalPushOrNot) {

            String title = "";
            String displayName = "";
            ProfileClass userProfile = new Select().from(ProfileClass.class).where("phone=?", phone).executeSingle();

            if (userProfile != null) {

                displayName = ActivityUtils.getDisplayNameFromPhone(phone);
                if (displayName == null || TextUtils.isEmpty(displayName)) {

                    displayName = userProfile.getName();

                }
            } else {
                displayName = phone;
            }
            title = "New message from " + displayName;

            int previousNotifyId = -1;

            String query = "select * from ChatRecord where status = 'received' and msgType = 1";
            Cursor c = ActiveAndroid.getDatabase().rawQuery(query, new String[]{});
            if (c != null && c.getCount() > 0) {
                HashSet<String> set = new HashSet<>();
                while (c.moveToNext()) {
                    set.add(c.getString(c.getColumnIndex("phone")));
                }
                int size = set.size();
                msg = String.format("%s new messages from %s Brokers", c.getCount(), size);
            } else {

            }
            PlabroPushNotificationManager.sendLocalPlabroPush(ctx, title, msg, phone, displayName, false, 1);
            if (true) {
                return;
            }
            List<Notification> notificationList = new Select().from(Notification.class).where("status='unread' AND act='im' ").execute();

            int notifyId = -1;

            if (notificationList.size() == 0) {
                //send push notif
                notifyId = PlabroPushNotificationManager.sendLocalPlabroPush(ctx, title, msg, phone, displayName, false, previousNotifyId);
            } else {
                previousNotifyId = Integer.parseInt(notificationList.get(0).getNotifyId());
                String newMsg = "";
                int msgCounter = 0;
                String firstPhone = "";
                List<String> phoneList = new ArrayList<>();
                for (Notification notification : notificationList) {
                    if (!phoneList.contains(notification.getPhone())) {
                        phoneList.add(notification.getPhone());
                    }
                    Log.v(TAG, firstPhone);
                    Log.v(TAG, notification.getMsg());

                    msgCounter++;
                }

                if (msgCounter > 0) {
                    if (phoneList.size() > 0) {
                        newMsg = msgCounter + " new IM from " + phoneList.size() + " brokers";

                    } else {
                        newMsg = msgCounter + " new IM from " + displayName;

                    }
                    notifyId = PlabroPushNotificationManager.sendLocalPlabroPush(ctx, title, newMsg, phone, displayName, false, previousNotifyId);
                }

            }

            //save notif
            Notification notification = new Notification();
            notification.setTitle(title);
            notification.setAct("im");
            notification.setMsg(msg);
            notification.setPhone(phone);
            notification.setNotifyId(notifyId + "");
            notification.setStatus(Notification.STATUS_UNREAD);
            notification.save();
        }
    }


    public static void processIncomingMessage(Message message, Connection conn, Context ctx) {
        Log.d(TAG, "Message Listener :" + message.toXML());
        if (message.getBody() != null && null != message.getType() && !message.getType().equals(Message.Type.error)) {
            // sendDeliveryReport();
            DeliveryReceipt dr = (DeliveryReceipt) message.getExtension(DeliveryReceipt.ELEMENT, DeliveryReceipt.NAMESPACE);
            if (dr != null) {

                System.out.println("sending deliveryreport:::::::::::::::::::::");

                Packet received = new Message();
                received.addExtension(new DeliveryReceipt(message.getPacketID()));
                received.setTo(message.getFrom());
                conn.sendPacket(received);
            }
            DeliveryReceiptRequest drr = (DeliveryReceiptRequest) message.getExtension(DeliveryReceiptRequest.ELEMENT, new DeliveryReceiptRequest().getNamespace());
            if (drr != null) {
                Log.d(TAG, "DRR Not NUll" + drr.toXML());

            }

            Log.d(TAG, message.getPacketID() + "");

            String fromName = StringUtils.parseBareAddress(message.getFrom());
            Log.i(TAG, "********************************************************Got text [" + message.getBody() + "] from [" + fromName + "]");
            PBPreferences.setNewIMOnApp(true);
            ChatMessage cm = new ChatMessage();
            String recepientPhone = fromName.substring(0, 13);
            if (fromName.contains("@")) {
                String[] splitArray = fromName.split("@");
                recepientPhone = splitArray[0];
            }
            sendLocalPush(message.getBody(), recepientPhone, ctx);
            cm.setPhone(recepientPhone);
            cm.setMsg(message.getBody());
            cm.setMsgType(ChatMessage.MESSAGE_INCOMING);
            cm.setStatus(ChatMessage.STATUS_RECEIVED);
            cm.setTime(System.currentTimeMillis());
            cm.setPacketId(message.getPacketID());
            cm.setPacketIdAndMessageType(message.getPacketID() + ChatMessage.MESSAGE_INCOMING);
            cm.save();


        }
    }

//    @Override
//    public void onConnectionClosed(Connection conn) {
//        manager.connectInBackground();
//
//    }
}