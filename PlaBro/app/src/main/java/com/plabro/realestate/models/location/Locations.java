package com.plabro.realestate.models.location;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class Locations {

    String _id;
    String loc;
    String location;
    String name_of_project;
    String avg_price_sale;
    String micro_market;
    String property_type;


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        location = location;
    }

    public String getName_of_project() {
        return name_of_project;
    }

    public void setName_of_project(String name_of_project) {
        this.name_of_project = name_of_project;
    }

    public String getAvg_price_sale() {
        return avg_price_sale;
    }

    public void setAvg_price_sale(String avg_price_sale) {
        this.avg_price_sale = avg_price_sale;
    }

    public String getMicro_market() {
        return micro_market;
    }

    public void setMicro_market(String micro_market) {
        this.micro_market = micro_market;
    }

    public String getProperty_type() {
        return property_type;
    }

    public void setProperty_type(String property_type) {
        this.property_type = property_type;
    }

}
