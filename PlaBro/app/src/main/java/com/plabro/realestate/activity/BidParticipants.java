package com.plabro.realestate.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.auctions.auction_meta.AuctiopnMetaInfo;
import com.plabro.realestate.models.auctions.auction_meta.Data;
import com.plabro.realestate.models.bids.BidParticipantResponse;
import com.plabro.realestate.models.bids.Datum;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class BidParticipants extends PlabroBaseActivity implements CustomListAdapter.CustomListAdapterInterface {

    private String bidId;

    private List<String> bidInfoList = new ArrayList<>();
    private ListView listView;
    private CustomListAdapter<String> adapter;

    private void setAdapter() {
        listView = (ListView) findViewById(R.id.listView);
        adapter = new CustomListAdapter<>(this, R.layout.tv_auction_detail_item, bidInfoList, this);
        listView.setAdapter(adapter);
    }

    @Override
    protected String getTag() {
        return "Bid Participants";
    }


    @Override
    protected int getResourceId() {
        return R.layout.activity_bid_participants;
    }

    @Override
    protected void loadBundleData(Bundle b) {
        bidId = b.getString(Constants.BUNDLE_KEYS.BID_ID);
    }


    private void getAuctionMetaInfo() {
        ParamObject obj = new ParamObject();
        HashMap<String, String> params = new HashMap<>();
        params.put("auction_id", bidId);
        obj.setClassType(AuctiopnMetaInfo.class);
        params = Utility.getInitialParams(PlaBroApi.RT.GET_AUCTION_META_INFO, params);
        obj.setParams(params);
        showDialog();
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                dismissDialog();
                Log.d(TAG, "Respose :" + response.getResponse());
                bidInfoList.clear();
                AuctiopnMetaInfo metaInfo = (AuctiopnMetaInfo) response.getResponse();
                if (metaInfo.getOutput_params() != null && metaInfo.getOutput_params().getData() != null) {
                    bidInfoList.addAll(metaInfo.getOutput_params().getData().getBids_info());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                dismissDialog();
                onApiFailure(response, taskCode);
            }
        });
    }


    @Override
    public void findViewById() {
        inflateToolbar();
        setTitle("Bid Info");
        setAdapter();
        getAuctionMetaInfo();
        Analytics.trackScreen(Analytics.Requirements.BidDetails);
    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        convertView = LayoutInflater.from(this).inflate(resourceID, null);
        TextView bidInfoTv = (TextView) convertView.findViewById(R.id.tv_auction_detail_item);
        int padding16 = getResources().getDimensionPixelOffset(R.dimen.margin_payment_page);
        bidInfoTv.setPadding(padding16, padding16, padding16, padding16);
        bidInfoTv.setText(bidInfoList.get(position));
        return convertView;
    }
}
