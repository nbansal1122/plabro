package com.plabro.realestate.uiHelpers.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;

import com.plabro.realestate.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 06/07/15.
 */
public class ImageEffects {

    private static final float BITMAP_SCALE = 0.4f;
    private static final float BLUR_RADIUS = 7.5f;

    public static List<Bitmap> createBlurredImage(int rad, Bitmap image, Context ctx) {
        List<Bitmap> bitmaps = new ArrayList<Bitmap>();
        int width = Math.round(ctx.getResources().getDimensionPixelOffset(R.dimen.dimen_user_image) * BITMAP_SCALE);
        int height = Math.round(ctx.getResources().getDimensionPixelOffset(R.dimen.dimen_user_image) * BITMAP_SCALE);
        RenderScript rs = RenderScript.create(ctx);
        try {
            Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height, false);
            Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);


            ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
            Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
            Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
            theIntrinsic.setRadius(BLUR_RADIUS);
            theIntrinsic.setInput(tmpIn);
            theIntrinsic.forEach(tmpOut);
            tmpOut.copyTo(outputBitmap);
            bitmaps.add(outputBitmap);
        } catch (Exception e) {
            bitmaps.add(image);

        }

        bitmaps.add(image);

        //Make the image greyscale
       /* final ScriptIntrinsicColorMatrix scriptColor =
                ScriptIntrinsicColorMatrix.create(rs, Element.U8_4(rs));
        scriptColor.setGreyscale();
        scriptColor.forEach(input, output);
        output.copyTo(grayBitmap);*/

        rs.destroy();

        return bitmaps;
    }

}
