package com.plabro.realestate.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.CycleInterpolator;
import android.view.animation.Interpolator;
import android.widget.RelativeLayout;

import com.activeandroid.query.Select;
import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.adapter.FeedsCustomAdapter;
import com.plabro.realestate.entities.FeedsDBModel;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.HidingScrollListener;
import com.plabro.realestate.listeners.MaterialDialogListener;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.WaveDrawable;
import com.plabro.realestate.widgets.dialogs.PBConditionalDialogView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by hemant on 28/07/15.
 */


/**
 * Created by hemant on 28/07/15.
 */
public class RelatedBrokers extends MasterFragment implements CustomListAdapter.CustomListAdapterInterface {
    private FeedsCustomAdapter mAdapter;
    private RecyclerView mRecyclerView;
    ProgressWheel mProgressWheel, mProgressWheelLoadMore;
    LinearLayoutManager mLayoutManager;
    private RelativeLayout noFeeds;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private int hashKey = 0;
    // private ImageButton scrollUpButton;
    private boolean isRunning = false;
    private static Context mContext;
    private static Activity mActivity;
    private int page = 0;
    private boolean isLoading = false;
    private Handler handler;
    public static final String TAG = "RelatedBrokers";
    private ArrayList<BaseFeed> feeds2 = new ArrayList<BaseFeed>();
    PBConditionalDialogView mConditionalView;
    private boolean isPageAlreadyRefreshed;
    Animation zoomin, zoomout;
    private ToolTipRelativeLayout toolTipRelativeLayout;
    Animation animZoomIn, animZoomOut;
    private SmoothProgressBar mSmoothProgressBar;
    private DialogFragment feedPopUpDialog;
    private FragmentManager mFragmentManager;
    private static String message_id = "";


    public static RelatedBrokers newInstance(Context mContext, Activity mActivity) {
        //  Log.i(TAG, "Creating New Instance");
        RelatedBrokers mRelatedBrokers = new RelatedBrokers();
        RelatedBrokers.mContext = mContext;
        RelatedBrokers.mActivity = mActivity;

        return mRelatedBrokers;
    }

    public static RelatedBrokers getInstance(Context mContext, String msgId) {
        //  Log.i(TAG, "Creating New Instance");
        RelatedBrokers mRelatedBrokers = new RelatedBrokers();
        RelatedBrokers.mContext = mContext;
        RelatedBrokers.message_id = msgId;
        return mRelatedBrokers;
    }

    @Override
    protected int getViewId() {
        return R.layout.related_brokers;
    }

    public void setRecyclerViewLayoutManager() {
        int scrollPosition = 0;
        mLayoutManager = new LinearLayoutManager(mActivity);
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }


    public void getData(final boolean addMore) {
        //sendScreenName(R.string.funnel_property_feed_requested);
        isLoading = true;

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("message_id", message_id);

        if (addMore) {
            mProgressWheelLoadMore.spin();
            params.put("startidx", (page) + "");
        } else {
            params.put("startidx", "0");
        }

        params = Utility.getInitialParams(PlaBroApi.RT.RELATED_BROKERS, params);
        AppVolley.processRequest(2, FeedResponse.class, null, String.format(PlaBroApi.getBaseUrl(mContext)), params, RequestMethod.GET, new VolleyListener() {
                    @Override
                    public void onSuccessResponse(PBResponse response, int taskCode) {
                        mSmoothProgressBar.setVisibility(View.GONE);

                        isPageAlreadyRefreshed = true;
                        stopSpinners();
                        sendScreenName(R.string.goal_property_feed_success);
                        // Log.i(TAG, "Response: " + response);

                        if (response != null) {

                            FeedResponse feedResponse = (FeedResponse) response.getResponse();

                            //Log.d(TAG, feedResponse.getOutput_params() +"");
                            //Log.d(TAG, feedResponse.getOutput_params().getData() +"");
                            if (feedResponse != null && feedResponse.isStatus()) {

                                ArrayList<BaseFeed> tempFeeds = (ArrayList<BaseFeed>) feedResponse.getOutput_params().getData();
                                sendScreenName(R.string.goal_property_feed_success);

                                if (addMore) {
                                    feeds2.addAll(tempFeeds);
                                    mAdapter.notifyDataSetChanged();
                                } else {
                                    feeds2.clear();
                                    feeds2.addAll(tempFeeds);
                                    if (mAdapter != null)
                                        mAdapter.notifyDataSetChanged();
                                    else {
                                        // setListAdapter(addMore);
                                    }
                                    //setListAdapter(addMore);
                                }
                                tempFeeds.clear();
                                if (feeds2.size() > 0) {
                                    page = Utility.getPageIndex(feeds2.size());
                                    HashMap<String, Object> data = new HashMap<>();
                                    data.put("ScreenName", TAG);
                                    data.put("Action", "Manual");
                                    Analytics.trackEventWithProperties(R.string.consumption, R.string.e_scroll_next, data);

                                    noFeeds.setVisibility(View.GONE);
                                } else {
                                    page = 0;
                                    noFeeds.setVisibility(View.VISIBLE);
                                }

                            } else {
                                if (feedResponse == null || feedResponse.getMsg().contains("Session Expired") || feedResponse.getMsg().equalsIgnoreCase("Missing AuthKey in the request")) {
                                    Utility.userLogin(getActivity());

                                    if (!addMore) {
                                        if (!PBPreferences.getSessEXpDialogState()) {

                                            PBPreferences.setSessEXpDialogState(true);

                                            //  ActivityUtils.showMaterialDialog(getActivity(), MaterialDialogListener.DIALOG_SESSION_EXPIRE, "Try again", "Oops ! Your Session Expired.", "CONNECT", "CANCEL", RelatedBrokers.this);

                                        }
                                    }

                                } else {
                                    showNoDataDialog(addMore);
                                }
                            }
                        } else {

                            showNoDataDialog(addMore);

                        }
                    }

                    @Override
                    public void onFailureResponse(PBResponse response, int taskCode) {
                        mSmoothProgressBar.setVisibility(View.GONE);
                        stopSpinners();
                        switch (response.getCustomException().getExceptionType()) {
                            case SESSION_EXPIRED:
                                Utility.userLogin(getActivity());

                                if (!addMore) {
                                    if (!PBPreferences.getSessEXpDialogState()) {

                                        PBPreferences.setSessEXpDialogState(true);

                                        //  ActivityUtils.showMaterialDialog(getActivity(), MaterialDialogListener.DIALOG_SESSION_EXPIRE, "Try again", "Oops ! Your Session Expired.", "CONNECT", "CANCEL", RelatedBrokers.this);

                                    }
                                }
                                return;
                        }
                        showNoDataDialog(addMore);

                    }
                }

        );


    }

    @Override
    public  void onStatusFalse(PBResponse response, int taskCode) {
        super.onStatusFalse(response, taskCode);
        if (null != mAdapter && mAdapter.getItemCount() > 0) {
            noFeeds.setVisibility(View.VISIBLE);
        }
        else {
            noFeeds.setVisibility(View.GONE);
        }

    }

    private void stopSpinners() {
        mProgressWheel.stopSpinning();
        isLoading = false;
        mProgressWheelLoadMore.stopSpinning();
    }

    View.OnClickListener MyOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.action_button:

                    break;

            }

        }
    };

    private WaveDrawable waveDrawable;
    private RelativeLayout layout;
    private boolean isShoutClicked;

    private void initWaveDrawable() {
        isShoutClicked = PBPreferences.getData(PBPreferences.IS_SHOUT_CLICKED, false);

        if (isShoutClicked) {
            return;
        }
        waveDrawable = new WaveDrawable(Color.parseColor("#33b5e5"), layout.getLayoutParams().width / 2, 1000);
        if (Build.VERSION.SDK_INT >= 16) {
            layout.setBackground(waveDrawable);
        } else {
            layout.setBackgroundDrawable(waveDrawable);
        }
        Interpolator interpolator;
        interpolator = new CycleInterpolator(3);
        waveDrawable.startAnimation();
    }


    private void stopAnimation() {

        PBPreferences.saveData(PBPreferences.IS_SHOUT_CLICKED, true);
        waveDrawable.stopAnimation();
        layout.setBackgroundResource(R.drawable.blue_circle);
    }


    @Override
    public String getTAG() {
        return "RelatedBrokers";
    }

    @Override
    protected void findViewById() {
        registerLocalReceiver();

        page = 0;
        isLoading = false;
        handler = new Handler();

        mConditionalView = (PBConditionalDialogView) findView(R.id.pbConditionalView);

        //attaching on scroll floating button
        mSmoothProgressBar = (SmoothProgressBar) findView(R.id.SmoothProgressBar);
        layout = (RelativeLayout) findView(R.id.rl_bg_shout);
        layout.setOnClickListener(MyOnClickListener);


        mRecyclerView = (RecyclerView) findView(R.id.recyclerView);

        setRecyclerViewLayoutManager();

        mProgressWheel = (ProgressWheel) findView(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mProgressWheel.spin();

        mProgressWheelLoadMore = (ProgressWheel) findView(R.id.progress_wheel_load_more);
        mProgressWheelLoadMore.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheelLoadMore.setProgress(0.0f);

        noFeeds = (RelativeLayout) findView(R.id.noFeeds);
        setListAdapter(false);

        // Request data from the network.
        getData(false);

        //pull to refresh
        mSwipeRefreshLayout = (SwipeRefreshLayout) findView(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isPageAlreadyRefreshed = false;
                        page = 0;
                        mSwipeRefreshLayout.setRefreshing(false);
                        mProgressWheelLoadMore.stopSpinning();
                        reloadRequest();
                        mSmoothProgressBar.setVisibility(View.VISIBLE);

                    }
                }, Constants.GOOGLE_PULL_TO_REFRESH_TIMER);

            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mRecyclerView.setOnScrollListener(new HidingScrollListener(20) {
            @Override
            public void onHide(RecyclerView recyclerView, int dx, int dy) {
            }

            @Override
            public void onShow(RecyclerView recyclerView, int dx, int dy) {
            }

            @Override
            public void onScrolledFire(RecyclerView recyclerView, int dx, int dy) {


                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();

                if (pastVisibleItems == 0) {

                }

                if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                    //reached bottom show loading
                    mProgressWheelLoadMore.setVisibility(View.VISIBLE);

                } else {
                    mProgressWheelLoadMore.setVisibility(View.GONE);

                }

                if (!isLoading) {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount - Constants.SCROLL_ITEM_COUNT) {
                        if (isPageAlreadyRefreshed)
                            getData(true);

                    }
                }

            }
        });


        PBPreferences.setShoutState(false);

        //mRecyclerView.setOnTouchListener(new ShowHideOnScroll(mNewPost, R.anim.floating_action_button_hide,R.anim.floating_action_button_show));


        // initiateAnimation();


    }


    @Override
    protected void setViewSizes() {

    }

    @Override
    protected void callScreenDataRequest() {

        if (!Util.haveNetworkConnection(mContext)) {

            if (feeds2.size() < 1) {
                showConditionalMessage(PBConditionalDialogView.ConditionalDialog.INTERNET_CONNECTION, true, false);
                mConditionalView.setVisibility(View.VISIBLE);
            } else {
//                Toast.makeText(mContext, "Seems there is no Internet Connection", Toast.LENGTH_LONG).show();

            }

            return;
        }

        mProgressWheel.spin();
        // Request data from the network.
        getData(false);

    }

    @Override
    protected void reloadRequest() {

        callScreenDataRequest();
    }


    private ArrayList<Feeds> getFeedsFromDB() {
        List<FeedsDBModel> dbFeeds = new Select().from(FeedsDBModel.class).execute();
        return (ArrayList<Feeds>) FeedsDBModel.getFeeds(dbFeeds);
    }

    private void setListAdapter(boolean scrollToLast) {

        mAdapter = new FeedsCustomAdapter(feeds2, "normal", getActivity(), hashKey);
        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String id = null;
        int pos = 0;
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.FEEDS_REQUEST_CODE) {

            page = 0;
            reloadRequest();


        }
    }


    void showNoDataDialog(final Boolean addMore) {
        //  Log.i(TAG, "No Data Dialog" + addMore);
        stopSpinners();
        if (!addMore && feeds2.size() < 1) {
            mConditionalView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onOkClicked(int dialogTypeCode) {
        switch (dialogTypeCode) {
            case MaterialDialogListener.DIALOG_HOME_STATE:
                getData(false);
                break;

            case MaterialDialogListener.DIALOG_SESSION_EXPIRE:
                getData(false);
                break;
        }
    }

    @Override
    public void onCancelClicked(int dialogTypeCode) {
        switch (dialogTypeCode) {
            case MaterialDialogListener.DIALOG_HOME_STATE:
                break;

            case MaterialDialogListener.DIALOG_SESSION_EXPIRE:

                break;
        }
    }

    @Override
    public void onDismissListener(int dialogTypeCode) {
        switch (dialogTypeCode) {
            case MaterialDialogListener.DIALOG_HOME_STATE:
                PBPreferences.setHomeDialogState(false);
                break;

            case MaterialDialogListener.DIALOG_SESSION_EXPIRE:
                PBPreferences.setSessEXpDialogState(false);
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void OnReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive");
        mAdapter = new FeedsCustomAdapter(feeds2, "normal", getActivity(), hashKey);
        mRecyclerView.setAdapter(mAdapter);
        /*if(null!=feedPopUpDialog)
        {
            feedPopUpDialog.dismiss();
            feedPopUpDialog.show(mFragmentManager, "UserRegister");
        }*/
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        return null;
    }


}
