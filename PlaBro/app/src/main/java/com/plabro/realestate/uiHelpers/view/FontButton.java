package com.plabro.realestate.uiHelpers.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Button;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;

/**
 * Created by nitin on 05/08/15.
 */
public class FontButton extends Button {
    private static final String TAG = "FontButton";

    public FontButton(Context context) {
        this(context, null);
    }

    public FontButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FontButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        if (isInEditMode())
            return;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.FontText);

        if (ta != null) {
            String fontAsset = ta.getString(R.styleable.FontText_font);
            Log.d(TAG, fontAsset + "");

            if (TextUtils.isEmpty(fontAsset)) {
                fontAsset = "Roboto-Regular";
            }

            Typeface tf = FontManager.getInstance().getFont("Fonts/" + fontAsset + ".ttf");
            int style = Typeface.NORMAL;
            float size = getTextSize();

            if (getTypeface() != null)
                style = getTypeface().getStyle();

            if (tf != null)
                setTypeface(tf, style);
            else
                Log.d("FontText", String.format("Could not create a font from asset: %s", fontAsset));
        }
    }
}
