package com.plabro.realestate.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.Services.XMPPConnectionService;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.Util;

public class ConnectivityChange extends BroadcastReceiver {
    private static final String TAG = ConnectivityChange.class.getSimpleName();


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("ConnectivityChange", "Connectivity Change");
        boolean isLoggedIn = PBPreferences.getLoggedInState();
        Log.d(TAG, "Logged in :"+isLoggedIn);
        if(isLoggedIn && Util.haveNetworkConnection(context)){
            Log.i(TAG, "Starting Tracking");
            PlabroIntentService.startConnectivityChangeAction(context);
            XMPPConnectionService.startServiceOnConnectivityChange(context);
        }else if(isLoggedIn){
            Intent i = new Intent(context, XMPPConnectionService.class);
            context.stopService(i);
        }
    }
}
