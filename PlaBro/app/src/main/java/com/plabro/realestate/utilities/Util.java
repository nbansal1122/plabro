package com.plabro.realestate.utilities;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.enums.SnackbarType;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.models.location_auto.LocationsAuto;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hemantkumar on 15/01/15.
 */
public class Util {

    public final static String DO_LOGIN_RECEIVER = "com.plabro.realestate.login";

    final public static boolean IS_DEVELOPMENT = false;
    final private static String LOG_TAG = "PlaBro";
    public static final int REQUEST_CODE_CAMERA = 12;
    public static final int REQUEST_CODE_GALLARY = 13;
    public static SimpleDateFormat dateFormat = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");
    static SimpleDateFormat dateFormatTime = new SimpleDateFormat("HH:mm a");
    static SimpleDateFormat dateFormatDate = new SimpleDateFormat("yyyy-MM-dd");


    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        if (context == null)
            return false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        for (NetworkInfo ni : cm.getAllNetworkInfo()) {
            if (ni.getType() == ConnectivityManager.TYPE_WIFI && ni.isConnected())
                haveConnectedWifi = true;
            if (ni.getType() == ConnectivityManager.TYPE_MOBILE && ni.isConnected())
                haveConnectedMobile = true;
        }

        return haveConnectedWifi || haveConnectedMobile;
    }

    public static boolean hasFroyo() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
    }

    public static boolean hasGingerbread() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
    }

    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    public static final boolean hasHoneycombMR2() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2;
    }


    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }

    public static void v(String msg) {
        if (IS_DEVELOPMENT) {
            Log.v(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
        }
    }

    public static void i(String msg) {
        if (IS_DEVELOPMENT) {
            Log.i(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
        }
    }

    public static void w(String msg) {
        if (IS_DEVELOPMENT) {
            Log.w(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
        }
    }

    public static void d(String msg) {
        if (IS_DEVELOPMENT) {
            Log.d(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
        }
    }

    public static void e(String msg) {
        Log.e(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
    }

    public static void e(String msg, Exception e) {
        Log.e(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg, e);
    }

    public static void hideKeyboard(View v, Activity mActivity) {
        if (null == v) return;
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);

    }

    public static void hideKeyboard(Activity mActivity) {
        // InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        //imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        mActivity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }

    public static void showKeyboard(View v, Activity mActivity) {

        mActivity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
        );

        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
    }


    public static String convertToUTF(String data) {
        if (data.equalsIgnoreCase("")) {
            return data;
        }
        String result = "";

        byte[] bute = null;
        bute = data.getBytes();
        try {
            result = new String(bute, "UTF-8");

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return result;
    }

    public static String decodeFromUTF(String data) {
        if (TextUtils.isEmpty(data)) {
            return "";
        }
        String dataDecoded = "";
        try {
            dataDecoded = new String(data.getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return dataDecoded;
    }


    public static List<LocationsAuto> getNewHashTagsIndices(String text, List<LocationsAuto> hashtags, List<String> phonemap, List<String> webmap) {
        List<LocationsAuto> hashtagsNew = new ArrayList<LocationsAuto>();
        HashMap<String, Integer> oldLoc = new HashMap<String, Integer>();
        String textlower = text.toLowerCase();
        List<LocationsAuto> phonemapList = new ArrayList<LocationsAuto>();
        List<LocationsAuto> webmapList = new ArrayList<LocationsAuto>();


        if (phonemap.size() > 0 && phonemap != null) {
            for (String phone : phonemap) {
                LocationsAuto locationsAuto = new LocationsAuto();
                locationsAuto.set_id("");
                locationsAuto.setTitle(phone);
                phonemapList.add(locationsAuto);
            }
        }

        if (webmap.size() > 0 && webmap != null) {
            for (String web : webmap) {
                LocationsAuto locationsAuto = new LocationsAuto();
                locationsAuto.set_id("");
                locationsAuto.setTitle(web);
                phonemapList.add(locationsAuto);
            }
        }
        hashtags.addAll(phonemapList);
        hashtags.addAll(webmapList);

        for (int i = 0; i < hashtags.size(); i++) {
            int lastIndex = 0;
            while (lastIndex != -1) {
                LocationsAuto la = hashtags.get(i);
                String hashtagtitle = la.getTitle().toLowerCase();
                lastIndex = textlower.indexOf(hashtagtitle, lastIndex);

                if (lastIndex != -1) {
                    LocationsAuto newla = new LocationsAuto();
                    int[] indices = {0, 0};
                    indices[0] = lastIndex; //-1 for the hash #
                    //                  indices[0] = lastIndex - 1; //-1 for the hash #
                    indices[1] = indices[0] + hashtagtitle.length() - 1;
                    oldLoc.put(hashtagtitle, indices[1]);
                    //la.setIndices(indices);
                    newla.set_id(la.get_id());
                    newla.setTitle(la.getTitle());
                    newla.setIndices(indices);
                    hashtagsNew.add(newla);
                    lastIndex += hashtagtitle.length();
                }
            }
        }
        return hashtagsNew;
    }


    public static List<LocationsAuto> getIndexFromText(String text, List<LocationsAuto> hashtags, List<String> itemMap) {

        List<LocationsAuto> hashtagsNew = new ArrayList<LocationsAuto>();
        HashMap<String, Integer> oldLoc = new HashMap<String, Integer>();
        String textlower = text.toLowerCase();
        List<LocationsAuto> phonemapList = new ArrayList<LocationsAuto>();

        if (itemMap.size() > 0 && itemMap != null) {
            for (String phone : itemMap) {
                LocationsAuto locationsAuto = new LocationsAuto();
                locationsAuto.set_id("");
                locationsAuto.setTitle(phone);
                phonemapList.add(locationsAuto);
            }
        }


        hashtags.addAll(phonemapList);

        for (int i = 0; i < hashtags.size(); i++) {
            int lastIndex = 0;
            while (lastIndex != -1) {
                LocationsAuto la = hashtags.get(i);
                String hashtagtitle = la.getTitle().toLowerCase();
                lastIndex = textlower.indexOf(hashtagtitle, lastIndex);

                if (lastIndex != -1) {
                    LocationsAuto newla = new LocationsAuto();
                    int[] indices = {0, 0};
                    indices[0] = lastIndex; //-1 for the hash #
                    //                  indices[0] = lastIndex - 1; //-1 for the hash #
                    indices[1] = indices[0] + hashtagtitle.length();
                    oldLoc.put(hashtagtitle, indices[1]);
                    //la.setIndices(indices);
                    newla.set_id(la.get_id());
                    newla.setTitle(la.getTitle());
                    newla.setIndices(indices);
                    hashtagsNew.add(newla);
                    lastIndex += hashtagtitle.length();
                }
            }
        }
        return hashtagsNew;
    }


    public static Date getDateFromString(String dt_str) {
        Date date = null;
        try {
            date = dateFormat.parse(dt_str);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }


    public static String getURLEncodedString(String urlAsString) {
        String encodedURL = null;
        try {
            encodedURL = URLEncoder.encode(urlAsString, "UTF-8");
            return encodedURL;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return urlAsString;

    }

    public void addTagsToMapFromText(List<LocationsAuto> hashtags, String text) {

        List<LocationsAuto> hashtagsTemp = new ArrayList<LocationsAuto>();

        //add multiple entries for multiple hashtags existence in the code
        for (int i = 0; i < hashtags.size(); i++) {

            hashtagsTemp.add(hashtags.get(i));

            int tStart = text.indexOf(hashtags.get(i).getTitle().toLowerCase());
            int tEnd = tStart + hashtags.get(i).getTitle().toLowerCase().length();

            text = text.substring(0, tStart) + text.substring(tEnd, text.length());

            int newIndex = text.indexOf(hashtags.get(i).getTitle().toLowerCase());

            if (newIndex > 0) {
                hashtagsTemp.add(hashtags.get(i));

            }

        }

    }


    public static boolean checkIfStringIsPhone(String text) {
        if (TextUtils.isEmpty(text)) {
            return false;
        }
        String PHONE_REGEX = "(\\+?9?1?\\-?\\s*\\(?[0-9]{2,4}\\s*\\)?\\s*\\-?\\s*[0-9-]{6,})|([0-9]{5}\\s*\\-\\s*[0-9]{5})";

        Pattern phonePattern = Pattern.compile(PHONE_REGEX);


        boolean status = false;
        if (phonePattern.matcher(text).find()) {
            return true;
        }
        return status;

    }


    public static boolean checkIfStringIsEmail(String text) {
        if (TextUtils.isEmpty(text)) {
            return false;
        }
        String PHONE_REGEX = "[.a-zA-Z0-9_-]+@[a-z0-9]+\\.[a-z]{2,}[.]?([a-z]{2,})*";

        Pattern emailPattern = Pattern.compile(PHONE_REGEX);


        boolean status = false;
        if (emailPattern.matcher(text).find()) {
            return true;
        }
        return status;

    }

    public static boolean checkIfStringIsWebUrl(String text) {
        if (TextUtils.isEmpty(text)) {
            return false;
        }
        boolean status = false;
        if (text.contains("http") || text.contains("www")) {
            return true;
        }

//        Pattern emailPattern = Pattern.compile(ActivityUtils.WEB_REGEX);
//
//
//        if (emailPattern.matcher(text).find()) {
//            return true;
//        }
        return status;

    }


    public static String getPhoneFromString(String text) {
        text = text.replace("+", "");
        Pattern phonePattern1 = Pattern.compile("\\d{10}");
        Pattern phonePattern2 = Pattern.compile("\\d{12}");

        Matcher m1 = phonePattern1.matcher(text);
        Matcher m2 = phonePattern2.matcher(text);


        String status = "";
        if (m1.find()) {
            status = m1.group(0);
        } else if (m2.find()) {
            status = m2.group(0);
        }

        return status;

    }

    public static double parseDoubleOrNull(String str) {
        return str != null ? Double.parseDouble(str) : 0;
    }

    public static boolean isAlphaNumericWithSpeacialChar(String str) {
        if (str.matches("[a-zA-Z!@#$%&*+=\\-_ ]+")) {
            return true;
        } else {
            return false;
        }
    }

    public static String getImageUrlSizeParameter(String url, ImageView imageView) {

//        DeviceResourceManager mDeviceResourceManager = AppController.getInstance().getDeviceResourceManager();

//        if (image_view == null || mDeviceResourceManager == null) {
//            return "";
//        }

//        int width = image_view.getMeasuredWidth() * 2 / 3;
//        int height = image_view.getMeasuredHeight() * 2 / 3;

        String queryString = "";

//        if (mScreenWidth == 0)
//            mScreenWidth = mDeviceResourceManager.getScreenWidth();

        if (Connectivity.getNetworkType() == Connectivity.NetworkType.NW_2G) {
            queryString = url + "?q=70";
        } else {
            queryString = url + "?q=80";
        }

//        if (width == 0 && height == 0) {
//
//            if (Connectivity.getNetworkType() == Connectivity.NetworkType.NW_2G) {
//                queryString = "?w=" + mScreenWidth / 2 + "&q=50";
//            } else {
//                queryString = "?w=" + mScreenWidth / 2 + "&q=70";
//            }
//        } else {
//            if (Connectivity.getNetworkType() == Connectivity.NetworkType.NW_2G) {
//                queryString = "?" + ((width >= height && width != 0) ? ("w=" + width) : ("h=" + height)) + "&q=50";
//            } else {
//                queryString = "?" + ((width >= height && width != 0) ? ("w=" + width) : ("h=" + height)) + "&q=70";
//            }
//        }
        return queryString;
    }

    public static void showSnackBarMessage(Activity activity, Context context, String text) {
        try {
            Snackbar.with(context)
                    .text(text)
                    .textColor(Color.WHITE)
                    .textTypeface(PBFonts.ROBOTTO_MEDIUM)
                    .type(SnackbarType.MULTI_LINE)
                    .color(context.getResources().getColor(R.color.appColor11))
                    .duration(Snackbar.SnackbarDuration.LENGTH_LONG)
                    .actionLabel("Dismiss")
                    .actionLabelTypeface(PBFonts.ROBOTTO_BOLD)
                    .actionColor(context.getResources().getColor(R.color.white))
                    .dismissOnActionClicked(true)
                    .show(activity);
        } catch (Exception e) {
            if (null != context)
                Toast.makeText(context, text + "", Toast.LENGTH_SHORT).show();
        }
    }

    public static String getDateWithoutTime(Date date) {
        String formats1 = dateFormatDate.format(date);
        return formats1;
    }

    public static String toDisplayCase(String s) {
        if (s != null && !TextUtils.isEmpty(s)) {
            final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
            // to be capitalized

            StringBuilder sb = new StringBuilder();
            boolean capNext = true;

            for (char c : s.toCharArray()) {
                c = (capNext)
                        ? Character.toUpperCase(c)
                        : Character.toLowerCase(c);
                sb.append(c);
                capNext = (ACTIONABLE_DELIMITERS.indexOf((int) c) >= 0); // explicit cast not needed
            }
            return sb.toString();
        } else {
            return s;
        }
    }

    public static String getStringFromInputStream(InputStream is) {
        StringBuilder response = new StringBuilder();
        try {
            BufferedReader buReader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 50000);

            String line;

            while ((line = buReader.readLine()) != null) {
                response.append(line);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return response.toString();

    }

    /*
     * take date string with format like 01-08-2015 return string like Today, 8th Jan 2015, Thursday
     */

    public static String getDisplayFormatDate(String dt_str) {

        if (dt_str == null)
            return " ";
        if ("".equals(dt_str.trim()))
            return "";

        final Calendar c = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        today.setTime(new Date());

        try {
            c.setTime(dateFormat.parse(dt_str));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String resutlDt = c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US) + ", " + c.get(Calendar.DAY_OF_MONTH) + getDayNumberSuffix(c.get(Calendar.DAY_OF_MONTH)) + " " + new SimpleDateFormat("MMM").format(c.getTime()) + " " + c.get(Calendar.YEAR);

        return resutlDt;
    }

    private static String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public static void pringIntentData(Intent i) {
        Bundle b = i.getExtras();
        if (b != null) {
            for (String key : b.keySet()) {
                Log.d("Intent Data:", key + "," + b.get(key));
            }
        } else {
            Log.d("Intent Data:", "Bundle is null");
        }
    }

    public static int getStartIndexForFeed(int currentPageCount) {
        currentPageCount += PBPreferences.getData(PBPreferences.START_IDX, 20);
        return currentPageCount;
    }

    public static int getStartIndex(int startIdx) {
        startIdx += 10;
        return startIdx;
    }

    public static int getDefaultStartIdx() {
        return 10;
    }

    public static int getDefaultIndexForFeed() {
        return PBPreferences.getData(PBPreferences.START_IDX, 20);
    }


    public static Bitmap getBitmapFromUri(Context ctx, Uri imageUri) {
        try {
            return MediaStore.Images.Media.getBitmap(ctx.getContentResolver(), imageUri);
        } catch (IOException e) {
            e.printStackTrace();

        }
        return null;
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public static byte[] getBytesFromBitmap(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 80, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static Uri getImageFromCamera(Activity activity) {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        Uri imageUri = Util.getOutputMediaFileUri();
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        activity.startActivityForResult(cameraIntent, REQUEST_CODE_CAMERA);
        return imageUri;
    }

    public static void getImageFromGallery(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        activity.startActivityForResult(intent, REQUEST_CODE_GALLARY);
    }

    public static Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "Plabro");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".png");

        return mediaFile;
    }

    public static String generatePIN() {
        //generate a 4 digit integer 1000 <10000
        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(900000);
        //Store integer in a string
        return "" + n;
    }

}

