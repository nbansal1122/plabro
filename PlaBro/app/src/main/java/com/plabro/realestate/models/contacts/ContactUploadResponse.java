package com.plabro.realestate.models.contacts;


import com.plabro.realestate.models.ServerResponse;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class ContactUploadResponse extends ServerResponse {

    PlabroFriendsOutputParams output_params = new PlabroFriendsOutputParams();


    public PlabroFriendsOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(PlabroFriendsOutputParams output_params) {
        this.output_params = output_params;
    }

}
