package com.plabro.realestate.models.login;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class LoginOutputParams {

    Login data = new Login();

    private int trial_version ;

    public Login getData() {
        return data;
    }

    public void setData(Login data) {
        this.data = data;
    }

    public int getTrial_version() {
        return trial_version;
    }
}
