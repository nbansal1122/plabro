package com.plabro.realestate.Services;

import android.Manifest;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.Contact;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.contacts.ContactInfo;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.utils.ContactsQuery;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by nitin on 08/10/15.
 */
public class BrokerContacts extends Service implements Handler.Callback, Loader.OnLoadCompleteListener<Cursor> {
    Handler handler;
    CursorLoader mCursorLoader;


    public static boolean isStarted;

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler(this);
        isStarted = true;
        Log.d(TAG, "ServiceStarted");
        String contactJSON = PBPreferences.getData(PBPreferences.KEY_BROKER_CONTACT_JSON, null);
        if (TextUtils.isEmpty(contactJSON)) {
            mCursorLoader = new CursorLoader(this, ContactsQuery.CONTENT_URI,
                    ContactsQuery.PROJECTION, ContactsQuery.SELECTION, null,
                    ContactsQuery.SORT_ORDER);
            mCursorLoader.registerListener(1, this);
            mCursorLoader.startLoading();
        } else {
            uploadJSON(contactJSON);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static void startContactSyncing(Context ctx) {
        boolean isBrokerContactSynced = PBPreferences.getData(PBPreferences.IS_BROKER_CONTACT_SYNCED, false);
        isBrokerContactSynced = false;
        if (!isBrokerContactSynced) {

            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) {
                ctx.startService(new Intent(ctx, BrokerContacts.class));
                Log.d(TAG, "SDK Version greater than = to M");
                return;
            } else if (ctx.checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Checking permission");
                ctx.startService(new Intent(ctx, BrokerContacts.class));
            }
        }
    }

    public static void stopContactSyncing(Context ctx) {
        ctx.stopService(new Intent(ctx, BrokerContacts.class));
    }

    private void uploadDataOnUiThread(final JSONArray array) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Length of array :" + array);
                if (array.length() > 0) {
                    uploadJSON(array + "");
                } else {
                    stopSelf();
                }
            }
        });
    }

    private void uploadJSON(final String array) {
        if (!TextUtils.isEmpty(array)) {
            ParamObject obj = new ParamObject();
            obj.setRequestMethod(RequestMethod.POST);
            HashMap<String, String> params = new HashMap<>();
            params.put("contacts_feed", array);
            obj.setParams(Utility.getInitialParams(PlaBroApi.RT.USER_CONTACTS_FEED, params));
            AppVolley.processRequest(obj, new VolleyListener() {
                @Override
                public void onSuccessResponse(PBResponse response, int taskCode) {
                    Log.d(TAG, "Success Response:");
                    PBPreferences.saveData(PBPreferences.IS_BROKER_CONTACT_SYNCED, true);
                    PBPreferences.saveData(PBPreferences.KEY_BROKER_CONTACT_JSON, "");
                    stopSelf();
                }

                @Override
                public void onFailureResponse(PBResponse response, int taskCode) {
                    Log.d(TAG, "Failure Response:" + response.getCustomException().getMessage());
                    PBPreferences.saveData(PBPreferences.KEY_BROKER_CONTACT_JSON, array);
                    stopSelf();
                }
            });
        } else {
            Log.d(TAG, "Array JSON is Empty");
            stopSelf();
        }
    }


    private String modifyContact(String mobileNumber) {
        if (mobileNumber == null) return "";
        String mobile = mobileNumber.replace("(", "").replace(")", "").replaceAll("-", "").replaceAll(" ", "").replaceAll("\\.", "").trim();
        return getLastTenDigits(mobile);
    }

    private String getLastTenDigits(String number) {
        String countryCode = PBPreferences.getCountryCode();
        if (number.contains(countryCode)) {
            return countryCode + number;
        } else {
            return number;
        }
    }

    @Override
    public boolean handleMessage(Message message) {
        return false;
    }


    @Override
    public void onLoadComplete(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG, "OnLoad Finished");

        fetchContacts(data);
    }

    private void fetchContacts(final Cursor cursor) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    if (cursor != null && cursor.getCount() > 0) {
                        JSONArray array = new JSONArray();
                        while (!cursor.isClosed() && cursor.moveToNext()) {
                            Contact contact = new Contact();
                            contact.setContactUri(ContactsContract.Contacts.getLookupUri(
                                    cursor.getLong(ContactsQuery.ID),
                                    cursor.getString(ContactsQuery.LOOKUP_KEY)));
                            String dn = cursor.getString(ContactsQuery.DISPLAY_NAME);
                            String contactId =
                                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                            contact.setContactId(contactId);
                            List<String> userPhones = new ArrayList<String>();

                            //
                            //  Get all phone numbers.
                            //
                            ContentResolver cr = getContentResolver();
                            Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                            if (null != phones && !phones.isClosed()) {
                                while (phones.moveToNext()) {
                                    String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                                    int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                                    userPhones.add(number);
                                    switch (type) {
                                        default:
                                            if (null != cursor) {
                                                String contactNumber = modifyContact(number);

                                                // Log.d(TAG, "Putting in contacts" + contactNumber + "Vs " + contact.getDisplayName());
                                                JSONObject obj = createJSONObject(dn, contactNumber);
                                                if (obj != null) {
                                                    array.put(obj);
                                                }

                                            }


                                            break;
                                    }
                                }
                                phones.close();
                            }

                        }
                        uploadDataOnUiThread(array);
                    }
                } catch (Exception e) {

                    Toast.makeText(BrokerContacts.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    stopSelf();
                    Crashlytics.logException(e);
                }
            }

        }).start();
    }

    private JSONObject createJSONObject(String name, String phone) {
        JSONObject o = new JSONObject();

        try {
            o.put("n", name);
            o.put("p", phone);
            return o;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        isStarted = false;
        if (mCursorLoader != null) {
            mCursorLoader.unregisterListener(this);
            mCursorLoader.cancelLoad();
            mCursorLoader.stopLoading();
        }
    }

    private static final String TAG = "BrokerContacts";

}
