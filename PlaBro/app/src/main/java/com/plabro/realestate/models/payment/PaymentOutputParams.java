package com.plabro.realestate.models.payment;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class PaymentOutputParams {

    PaymentFinalData data = new PaymentFinalData();
    String dataE = "";

    public String getDataE() {
        return dataE;
    }

    public void setDataE(String dataE) {
        this.dataE = dataE;
    }

    public PaymentFinalData getData() {
        return data;
    }

    public void setData(PaymentFinalData data) {
        this.data = data;
    }
}
