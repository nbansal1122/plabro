package com.plabro.realestate.models.contacts;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.plabro.realestate.CustomLogger.Log;

import java.util.HashSet;
import java.util.List;

/**
 * Created by jmd on 6/9/2015.
 */
@Table(name = "ContactInfo", id = BaseColumns._ID)
public class ContactInfo extends Model {

    private static final String TAG = ContactInfo.class.getSimpleName();

    @Column(unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String contactId;
    @Column
    private String contactNumber;
    @Column
    private String displayName;
    @Column
    private boolean isSynced;

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }


    public boolean isSynced() {
        return isSynced;
    }

    public void setIsSynced(boolean isSynced) {
        this.isSynced = isSynced;
    }


    public static HashSet<ContactInfo> getUnSyncedContacts() {
        List<ContactInfo> allContacts = new Select().from(ContactInfo.class).execute();

        if (allContacts != null && allContacts.size() > 0) {
            HashSet<ContactInfo> set = new HashSet<>();
            Log.d(TAG, "AllContacts Size" + allContacts.size());
            for (ContactInfo c : allContacts) {
                if (!c.isSynced) {
                    set.add(c);

                }
            }
            return set;
        }
        return null;
    }

    public static HashSet<ContactInfo> getSyncedContacts() {
        List<ContactInfo> allContacts = new Select().from(ContactInfo.class).execute();

        if (allContacts != null && allContacts.size() > 0) {
            HashSet<ContactInfo> set = new HashSet<>();
            Log.d(TAG, "Alread Synced:" + allContacts.size());
            for (ContactInfo c : allContacts) {
                if (c.isSynced) {
                    Log.d(TAG, "Alread Synced:" + c.getDisplayName());
                    set.add(c);
                }
            }
            return set;
        }
        return null;
    }

    public static HashSet<String> getContactIds(){
        HashSet<String> contactIds = new HashSet<>();

        List<ContactInfo> allContacts = new Select().from(ContactInfo.class).execute();

        if (allContacts != null && allContacts.size() > 0) {
            Log.d(TAG, "Alread Synced:" + allContacts.size());
            for (ContactInfo c : allContacts) {
                if (c.isSynced) {
                    contactIds.add(c.getContactId());
                }
            }

        }
        return contactIds;
    }
}
