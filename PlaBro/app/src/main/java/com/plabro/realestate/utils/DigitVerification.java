package com.plabro.realestate.utils;

import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsAuthConfig;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsSession;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;

import io.fabric.sdk.android.Fabric;

/**
 * Created by nitin on 08/02/16.
 */
public class DigitVerification {
    public static final String TAG = "DigitVerification";
    private AppController instance;
    private static AuthCallback authCallback;
    private static DigitVerification digitVerification = new DigitVerification();
    private static final String TWITTER_KEY = "C37xmIYifaAgnoaRQ8zCVKdID";
    private static final String TWITTER_SECRET = "T967BnhknKtlEyNEgdLyZVCoj3Jp0kxmj1ieJLWgrKfhpfeAEp";

    private DigitVerification() {

    }

    public static DigitVerification getInstance(AppController instance) {
        digitVerification.instance = instance;
        return digitVerification;
    }

    public void authenticate(String phoneNumber, final DigitCallback digitCallback) {
        authCallback = new AuthCallback() {
            @Override
            public void success(DigitsSession session, String phoneNumber) {
                // Do something with the session
                Log.d(TAG, "on Success" + phoneNumber);
                if (null != digitCallback) {
                    digitCallback.success(session, phoneNumber);
                }
            }

            @Override
            public void failure(DigitsException exception) {
                // Do something on failure
                digitCallback.failure(exception);
            }
        };
        DigitsAuthConfig.Builder builder = new DigitsAuthConfig.Builder();
        DigitsAuthConfig config = builder.withAuthCallBack(authCallback).withPhoneNumber(phoneNumber).withThemeResId(R.style.CustomDigitsTheme).build();
        Digits.authenticate(config);
    }

    public void initDigitVerification() {
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(digitVerification.instance, new TwitterCore(authConfig), new Digits());
    }

    public static AuthCallback getAuthCallback() {
        return authCallback;
    }

    public static interface DigitCallback {
        public void success(DigitsSession session, String phoneNumber);

        public void failure(DigitsException exception);
    }
}
