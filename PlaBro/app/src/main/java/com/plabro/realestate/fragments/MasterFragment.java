package com.plabro.realestate.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.plabro.realestate.AppController;
import com.plabro.realestate.R;
import com.plabro.realestate.listeners.MaterialDialogListener;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.MaterialDialog;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.PBConditionalDialogView;
import com.plabro.realestate.widgets.others.AppMsg;

public abstract class MasterFragment extends PlabroBaseFragment implements OnClickListener, MaterialDialogListener {


    protected AppController mAppController;

    PBConditionalDialogView mPBConditionalDialogView = null;

    static Animation animLeftIn, animLeftOut, animFadeIn, animFadeOut, animTopIn, animTopOut;
    ;

    //protected  static final TAG = getTag();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (animLeftIn == null || animLeftOut == null || animFadeIn == null || animFadeOut == null || animTopIn == null || animTopOut == null) {
            animLeftIn = AnimationUtils.loadAnimation(getActivity(), R.anim.left_in);
            animLeftOut = AnimationUtils.loadAnimation(getActivity(), R.anim.left_out);
            animFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            animFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
            animTopIn = AnimationUtils.loadAnimation(getActivity(), R.anim.top_in);
            animTopOut = AnimationUtils.loadAnimation(getActivity(), R.anim.bottom_out);
        }

        mAppController = AppController.getInstance();

    }

    protected void findViewById() {
    }

    protected void setViewSizes() {
    }

    protected void callScreenDataRequest() {
    }

    protected void reloadRequest() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        findViewById();

        setViewSizes();

        if (getView() != null && getView().findViewById(R.id.pbConditionalView) != null) {
            mPBConditionalDialogView = (PBConditionalDialogView) getView().findViewById(R.id.pbConditionalView);

            mPBConditionalDialogView.setOnPBConditionListener(new PBConditionalDialogView.OnPBConditionListener() {

                @Override
                public void onRefresh() {

                    showConditionalMessage(null, false, false);

                    reloadRequest();

                }
            });
        }

        callScreenDataRequest();

    }

    protected void showConditionalMessage(PBConditionalDialogView.ConditionalDialog conditionalDialog, boolean isShow, boolean isShiftLayout) {

        try {

            if (mPBConditionalDialogView == null)
                return;

            if (isShow) {
                mPBConditionalDialogView.setType(conditionalDialog);
                mPBConditionalDialogView.setVisibility(View.VISIBLE);

                getView().findViewById(R.id.pbConditionalView).setVisibility(View.VISIBLE);
                if (isShiftLayout) {
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    params.setMargins(0, 0, 0, (int) getResources().getDimension(R.dimen.conditional_dialog_margin_bottom));
                    mPBConditionalDialogView.setLayoutParams(params);
                }
            } else {
                mPBConditionalDialogView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void startActivity(Intent intent) {
        getActivity().startActivity(intent);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void showInternetConnectionToast() {
        showToast(getString(R.string.no_network_connection));
    }


    public void showCroutonMsg(Activity activity, String text, AppMsg.Style style) {

        AppMsg appMsg = AppMsg.makeText(activity, text, style);

        appMsg.setAnimation(animTopIn, animTopOut);

        appMsg.show();
    }

    public void showCroutonMsg(Activity activity, String text) {
        showCroutonMsg(activity, text, AppMsg.STYLE_TC_ERROR);
    }

    public void showCroutonMsg(Activity activity, String text, int colorResId) {
        showCroutonMsg(activity, text, new AppMsg.Style(AppMsg.LENGTH_SHORT, colorResId));
    }

    @Override
    public void onOkClicked(int dialogTypeCode) {

    }

    @Override
    public void onCancelClicked(int dialogTypeCode) {

    }

    @Override
    public void onDismissListener(int dialogTypeCode) {

    }

    public void sendScreenName(String screen) {
        if (null != getActivity()) {
            //Analytics.trackScreen(screen);
        }
    }

    public void sendScreenName(int screenStringID) {
        if (null != getActivity()) {
            // Analytics.trackScreen(getString(screenStringID));
        }
    }

    protected void showSessionExpired(final boolean addMore) {
        if (!addMore && null != getActivity()) {
            if (!PBPreferences.getSessEXpDialogState()) {

                PBPreferences.setSessEXpDialogState(true);

                final MaterialDialog mMaterialDialog = new MaterialDialog(getActivity());

                mMaterialDialog.setTitle("Try again")
                        .setMessage("Oops ! Your Session Expired.")
                        .setPositiveButton("CONNECT", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PBPreferences.setSessEXpDialogState(false);

                                mMaterialDialog.dismiss();
                                getData(addMore);
                            }
                        })

                        .setNegativeButton("CANCEL", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PBPreferences.setSessEXpDialogState(false);
                                mMaterialDialog.dismiss();
                            }
                        });

                mMaterialDialog.show();
            }
        }
    }

    public void getData(boolean addMore) {
    }

    @Override
    public void onInternetException(PBResponse response, int taskCode) {
        super.onInternetException(response, taskCode);

    }
}
