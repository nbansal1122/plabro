package com.plabro.realestate.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.utilities.PBPreferences;

public class CampaignTracker extends BroadcastReceiver {

    private static final String TAG = "CampaignTracker";

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Log.d(TAG, intent.getAction());
        Log.d(TAG, intent.getData() + "");
        Bundle b = intent.getExtras();
        if (b != null) {
            for (String s : b.keySet()) {
                Log.d(TAG, "Key :" + s + ", Value:" + b.get(s));
            }
        }


        String var3 = intent.getStringExtra("referrer");
        if ("com.android.vending.INSTALL_REFERRER".equals(intent.getAction()) && var3 != null) {
            PlabroIntentService.startCampaignTracking(context, var3);
            Log.d(TAG, "Saving Campaign URI to Preferences");
            PBPreferences.saveData(PBPreferences.CAMPAIGN_URI, var3);
        }
        new CampaignTrackingReceiver().onReceive(context, intent);

    }
}
