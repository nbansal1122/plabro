package com.plabro.realestate;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.plabro.realestate.receivers.MarshMallowReceiver;


/**
 * Created by hemantkumar on 02/11/15.
 */
public class MarshMallowActivity extends AppCompatActivity {

    private int PMCode;
    private String pmMessage;


    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission, int PMCode, String pmMessage) {
        this.PMCode = PMCode;
        this.pmMessage = pmMessage;
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) {
            //Build version below Marshmallow
            onMMPermisssoinResult(PMCode, true, pmMessage);
            return true;
        }


        if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            //permission is already available, proceed normally
            onMMPermisssoinResult(PMCode, true, pmMessage);
            return true;

        } else {
            //Permission not granted, lets ask user for the permissions
            if (shouldShowRequestPermissionRationale(permission)) {
                Toast.makeText(this, pmMessage, Toast.LENGTH_SHORT).show();
            }

            requestPermissions(new String[]{permission}, PMCode);

        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PMCode
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onMMPermisssoinResult(requestCode, true, pmMessage);
        } else {
            onMMPermisssoinResult(requestCode, false, pmMessage);
        }
    }

    public void onMMPermisssoinResult(int requestCode, boolean status, String message) {

        Intent mmBroadCastIntent = new Intent();
        mmBroadCastIntent.putExtra("requestCode", requestCode);
        mmBroadCastIntent.putExtra("status", status);
        mmBroadCastIntent.putExtra("message", message);
        mmBroadCastIntent.setAction(MarshMallowReceiver.MMInterFace.mmAction);
        sendBroadcast(mmBroadCastIntent);


    }

}
