package com.plabro.realestate.models.profile.builder;

import com.google.gson.annotations.Expose;
import com.plabro.realestate.models.ServerResponse;

public class BuilderProfileResponse extends ServerResponse {

    @Expose
    private Output_params output_params;
    @Expose
    private String RT;

    public Output_params getOutput_params() {
        return output_params;
    }

    /**
     * @param output_params The output_params
     */
    public void setOutput_params(Output_params output_params) {
        this.output_params = output_params;
    }


    /**
     * @return The RT
     */
    public String getRT() {
        return RT;
    }

    /**
     * @param RT The RT
     */
    public void setRT(String RT) {
        this.RT = RT;
    }

}