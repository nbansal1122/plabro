package com.plabro.realestate.uiHelpers.images;


/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.plabro.realestate.AppController;
import com.plabro.realestate.ImageGallery;
import com.plabro.realestate.R;
import com.plabro.realestate.utilities.Util;

/**
 * This fragment will populate the children of the ViewPager from {@link ImageDetailFragment}.
 */
public class ImageDetailFragment extends Fragment {
    private static final String IMAGE_DATA_EXTRA = "extra_image_data";
    private String mImageUrl;
    private ImageTouchImageView mImageView;

    // private ImageFetcher mImageFetcher;

    /**
     * Factory method to generate a new instance of the fragment given an image number.
     *
     * @param imageUrl The image url to load
     * @return A new instance of ImageDetailFragment with imageNum extras
     */
    public static ImageDetailFragment newInstance(String imageUrl) {
        final ImageDetailFragment f = new ImageDetailFragment();

        final Bundle args = new Bundle();
        args.putString(IMAGE_DATA_EXTRA, imageUrl);
        f.setArguments(args);

        return f;
    }

    /**
     * Empty constructor as per the Fragment documentation
     */
    public ImageDetailFragment() {
    }

    /**
     * Populate image using a url from extras, use the convenience factory method {@link ImageDetailFragment#newInstance(String)} to create this
     * fragment.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageUrl = getArguments() != null ? getArguments().getString(IMAGE_DATA_EXTRA) : null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate and locate the main ImageView
        final View v = inflater.inflate(R.layout.zoomable, container, false);
        mImageView = (ImageTouchImageView) v.findViewById(R.id.imageView);
        return v;
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Use the parent activity to load the image asynchronously into the
        // ImageView (so a single
        // cache can be used over all pages in the ViewPagerre
        if (ImageGallery.class.isInstance(getActivity())) {

            //    if (Util.isImageAvailable(mImageUrl.replaceAll("_400x300", ""))) {


            String imageUrl = Util.getImageUrlSizeParameter(mImageUrl, null);
                try {
                    Util.v("Full Screen ImageUrl : " + imageUrl);
                    AppController.getInstance().getImageLoader().get(imageUrl, new ImageListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Util.d("" + error);
                            AppController.getInstance().deleteImageCache();
                        }

                        @Override
                        public void onResponse(ImageContainer response, boolean isImmediate) {

                            if (response.getBitmap() == null) {
                                // AppController.getInstance().deleteAllCache();

                            } else {
                                mImageView.setImageBitmap(response.getBitmap());
                            }
                            Util.d("" + response + "  " + isImmediate);
                        }
                    }, 1000, 1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        //}

        // Pass clicks on the ImageView to the parent activity to handle
        if (OnClickListener.class.isInstance(getActivity()) && Util.hasHoneycomb()) {
            mImageView.setOnClickListener((OnClickListener) getActivity());
        }
    }

}
