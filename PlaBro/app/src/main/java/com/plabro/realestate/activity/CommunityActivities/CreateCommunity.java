package com.plabro.realestate.activity.CommunityActivities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.community.CreateCommunityResponse;
import com.plabro.realestate.utilities.ImageCropper;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.HashMap;

public class CreateCommunity extends PlabroBaseActivity implements VolleyListener {
    private ImageView logoImage;
    private Bitmap croppedBitmap;

    @Override
    protected String getTag() {
        return "Create Community";
    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_create_community;
    }

    @Override
    public void findViewById() {
        inflateToolbar();
        logoImage = (ImageView) findViewById(R.id.iv_logo_community);
        setTitle("Create Community");
        setClickListeners(R.id.btn_create_community, R.id.iv_logo_community);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btn_create_community:
                createCommunity();
                break;
            case R.id.iv_logo_community:
                ImageCropper.startActivity(this, false, true);
                break;
        }
    }

    private void createCommunity() {
        String name = getEditTextString(R.id.et_communityName);
        String communityDesc = getEditTextString(R.id.et_communityDesc);
        if (TextUtils.isEmpty(name)) {
            showToast(R.string.empty_community_name);
            return;
        }
        if (TextUtils.isEmpty(communityDesc)) {
            showToast(R.string.empty_community_desc);
            return;
        }
        if (croppedBitmap == null) {
            showToast(R.string.select_logo);
        }

        ParamObject obj = new ParamObject();
        obj.setClassType(CreateCommunityResponse.class);
        obj.setRequestMethod(RequestMethod.POST);
        HashMap<String, String> map = new HashMap<>();
        map.put("groupname", name);
        map.put("action", "create");
        map.put("groupdesc", communityDesc);
        map.put("img", Util.encodeToBase64(croppedBitmap, Bitmap.CompressFormat.PNG, 100));
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.UPDATE_COMMUNITY, map));
showDialog();
        AppVolley.processRequest(obj, this);

    }

    @Override
    public void reloadRequest() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        logoImage.setImageBitmap(ImageCropper.croppedBitmap);
        croppedBitmap = ImageCropper.croppedBitmap;
    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        dismissDialog();
        CreateCommunityResponse res = (CreateCommunityResponse) response.getResponse();
        if (res != null && res.getOutput_params() != null) {
            String communityId = res.getOutput_params().getGroup_id();
            if (!TextUtils.isEmpty(communityId)) {
                InviteCommunityUsers.startActivity(this, communityId);
                finish();
            }
        }
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        dismissDialog();
        onApiFailure(response, taskCode);
    }
}
