package com.plabro.realestate.fragments;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomPagerAdapter;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.models.propFeeds.listings.ListingFeed;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 03/01/16.
 */
public class ViewPagerFragment extends PlabroBaseFragment implements CustomPagerAdapter.PagerAdapterInterface<String> {
    private ViewPager pager;
    private CustomPagerAdapter<String> adapter;
    private ArrayList<String> urls = new ArrayList<>();
    private ListingFeed model;
    private List<ImageView> indicators = new ArrayList<>();
    private ImageView currentIndicator;

    public static ViewPagerFragment getInstance(ListingFeed model) {
        ViewPagerFragment f = new ViewPagerFragment();
        f.urls = (ArrayList<String>) model.getDetailed_view().getImage_urls();
        f.model = model;
        return f;
    }

    private ImageView getIndicatorImage() {
        ImageView view = (ImageView) LayoutInflater.from(getActivity()).inflate(R.layout.indicator_image, null);
        int heoght = getResources().getDimensionPixelOffset(R.dimen.indicator);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(heoght, heoght);
        params.setMargins(0, 0, getResources().getDimensionPixelOffset(R.dimen.indicator_margin), 0);
        view.setLayoutParams(params);
        indicators.add(view);
        return view;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.tv_owner_name:
                Feeds feeds = (Feeds) model;
                HashMap<String, Object> wrData = new HashMap<String, Object>();
                wrData.put("ScreenName", TAG);
                wrData.put("callFrom", PBPreferences.getPhone());
                wrData.put("source", "Listing Detail");
                wrData.put("_id", model.get_id() + "");
                ActivityUtils.initiateCallDialog(getActivity(), feeds, feeds.getPhonemap(), feeds.getProfile_phone(), wrData, feeds.getIsFreeCall(), feeds.getProfile_img(), feeds.getProfile_name());

                break;
        }
    }

    @Override
    public void findViewById() {

        setText(R.id.tv_listing_name, model.getDetailed_view().getTitle());
        setText(R.id.tv_listing_time, model.getDetailed_view().getTime());

        if (model.isPayment_status()) {
            showViews(R.id.tv_owner_name);
            setText(R.id.tv_owner_name, model.getOwner_name());
            setClickListeners(R.id.tv_owner_name);
        } else {
            hideViews(R.id.tv_owner_name);
        }

        pager = (ViewPager) findView(R.id.view_pager);
        adapter = new CustomPagerAdapter<>(getChildFragmentManager(), urls, this);
        LinearLayout layout = (LinearLayout) findView(R.id.ll_indicator);
        if (model.getDetailed_view().getImage_urls().size() > 0) {
            for (String s : model.getDetailed_view().getImage_urls()) {
                layout.addView(getIndicatorImage());
            }
            currentIndicator = indicators.get(0);
        }
        currentIndicator.setBackgroundResource(R.drawable.img_slider_blue);
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentIndicator.setBackgroundResource(R.drawable.img_slider_white);
                indicators.get(position).setBackgroundResource(R.drawable.img_slider_blue);
                currentIndicator = indicators.get(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public String getTAG() {
        return "ViewPagerFragment";
    }


    @Override
    protected int getViewId() {
        return R.layout.fragment_view_pager;
    }

    @Override
    public Fragment getFragmentItem(int position, String listItem) {
        return ImageFragment.getInstance(model, listItem, position);
    }

    @Override
    public CharSequence getPageTitle(int position, String listItem) {
        return listItem;
    }

    @SuppressLint("ValidFragment")
    public static class ImageFragment extends PlabroBaseFragment {

        private String url;
        private Bitmap bMap;
        private ImageView image;
        private int currentIndex;
        private ListingFeed model;

        public static ImageFragment getInstance(ListingFeed model, String url, int index) {
            ImageFragment f = new ImageFragment();
            f.url = url;
            f.currentIndex = index;
            f.model = model;
            return f;
        }


        @Override
        public String getTAG() {
            return "Image View";
        }

        @Override
        protected void findViewById() {
            image = (ImageView) findView(R.id.iv_listing_image);
//
//            DisplayMetrics metrics = new DisplayMetrics();
////get display information
//            getParentFragment().getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
//            int width = metrics.widthPixels;
//            int height = getResources().getDimensionPixelOffset(R.dimen.height_iv_selfie);


            ActivityUtils.loadImage(getActivity(), url, image, 0);

        }

        @Override
        protected int getViewId() {
            return R.layout.layout_image_header;
        }
    }


}
