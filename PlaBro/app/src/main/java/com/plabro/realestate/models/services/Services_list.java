package com.plabro.realestate.models.services;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Services_list implements Serializable{

    @SerializedName("s_data")
    @Expose
    private S_data s_data;
    @SerializedName("type")
    @Expose
    private String type;

    public String getService_code() {
        return service_code;
    }

    public void setService_code(String service_code) {
        this.service_code = service_code;
    }

    @SerializedName("service_code")
    @Expose
    private String service_code;



    /**
     * @return The s_data
     */
    public S_data getS_data() {
        return s_data;
    }

    /**
     * @param s_data The s_data
     */
    public void setS_data(S_data s_data) {
        this.s_data = s_data;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

}