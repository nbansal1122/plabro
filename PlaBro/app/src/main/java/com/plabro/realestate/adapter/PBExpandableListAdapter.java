package com.plabro.realestate.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.models.profile.Endorsement_profile;
import com.plabro.realestate.models.profile.Endorser_list;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.utilities.ActivityUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PBExpandableListAdapter extends BaseExpandableListAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    ArrayList<Endorsement_profile> list;
    private String emptyName = "";


    public PBExpandableListAdapter(Activity c, List<Endorsement_profile> list, String name) {
        this.inflater = LayoutInflater.from(c);
        this.activity = c;
        this.list = (ArrayList<Endorsement_profile>) list;
        this.emptyName = name;
    }


    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        ChildViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_child_endorsement, null);
            holder = new ChildViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ChildViewHolder) convertView.getTag();
        }

        Endorser_list child = this.list.get(groupPosition).getEndorser_list().get(childPosition);
        holder.endorseeName.setText(child.getName() + "");

        if (null != child.getImg() && !TextUtils.isEmpty(child.getImg()))
            Picasso.with(this.activity).load(child.getImg()).placeholder(R.drawable.profile_default_one).into(holder.imageView);

        return convertView;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder holder;
        Endorsement_profile group = this.list.get(groupPosition);
        if(this.list.size()==1 && group.isEmpty()){
            convertView = getEmptyViewForEndorsements();
        }else {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.row_group_endorsement, null);
                holder = new GroupViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (GroupViewHolder) convertView.getTag();
            }
            holder.endorseCount.setText(group.getCount() + "");
            holder.endorseType.setText(group.getTitle());
            ActivityUtils.loadImage(this.activity, group.getImg_url(), holder.icon, R.drawable.ic_plot);
        }



//        holder.icon.setImageResource(group.get);

//        if (null != child.getImg() && !TextUtils.isEmpty(child.getImg()))
//            Picasso.with(this.activity).load(child.getImg()).placeholder(R.drawable.profile_default_one).into(holder.imageView);

        return convertView;
    }

    private View getEmptyViewForEndorsements() {
        View v= inflater.inflate(R.layout.view_empty_endorsements, null);
        TextView view = (TextView) v.findViewById(R.id.tv_help_endorsement);
        String text = String.format((this.activity.getString(R.string.help_endorsement)), this.emptyName);
        view.setText(text);
        return v;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        //return (list.get(groupPosition).getEndorser_list()).size();
        return 0; //todo: prannoy rating changes
    }

    @Override
    public Object getGroup(int groupPosition) {
        return list.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return list.size();
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    private class ChildViewHolder {
        TextView endorseeName;
        RoundedImageView imageView;

        public ChildViewHolder(View view) {
            this.endorseeName = (TextView) view.findViewById(R.id.tv_endorserName);
            this.imageView = (RoundedImageView) view.findViewById(R.id.iv_endorserImage);
        }
    }

    private class GroupViewHolder {
        TextView endorseType, endorseCount;
        RoundedImageView icon;

        public GroupViewHolder(View view) {
            this.endorseType = (TextView) view.findViewById(R.id.tv_row_grp_endorsement);
            this.endorseCount = (TextView) view.findViewById(R.id.tv_row_grp_endorsement_count);
            this.icon = (RoundedImageView) view.findViewById(R.id.iv_icon_row_grp_endorsement);
        }
    }

}