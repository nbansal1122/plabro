package com.plabro.realestate.models.shouts;

/**
 * Created by hemantkumar on 06/10/15.
 */
public class ShoutOutputParams {

    String message_id = "";

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }
}
