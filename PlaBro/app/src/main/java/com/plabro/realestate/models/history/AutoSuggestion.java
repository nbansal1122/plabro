package com.plabro.realestate.models.history;

import android.graphics.drawable.Drawable;

/**
 * Created by hemant on 21/4/15.
 */

public class AutoSuggestion  {

    int _id ;
    String text;
    Drawable icon;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }
}
