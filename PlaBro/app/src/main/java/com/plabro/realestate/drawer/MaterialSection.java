package com.plabro.realestate.drawer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.listeners.MaterialSectionListener;


public class MaterialSection implements OnClickListener {

    public static final boolean TARGET_FRAGMENT = true;
    public static final boolean TARGET_ACTIVITY = false;


    private int position;
    private View view;
    private TextView text, pendingStatus;
    private ImageView icon;
    private RelativeLayout sectionParentLayout;
    private MaterialSectionListener listener;
    private boolean targetType, isHomeFragment, showPendingStatus;

    private int iconColorUnpressed;
    private int iconColorSelected;
    private int textColorUnpressed;
    private int textColorSelected;
//    private int bgColorUnpressed;
//    private int bgColorSelected;

    private String title, pendingCount;

    private Fragment targetFragment;
    private Intent targetIntent;
    private boolean isNew;

    public MaterialSection(Context ctx, int position, boolean targetType, boolean isHomeFragment, boolean showPendingStatus) {

        this(ctx, position, targetType, isHomeFragment, showPendingStatus, false);

    }

    public MaterialSection(Context ctx, int position, boolean targetType, boolean isHomeFragment, boolean showPendingStatus, boolean isNewSection) {


        view = LayoutInflater.from(ctx).inflate(R.layout.layout_material_section_icon, null);

        text = (TextView) view.findViewById(R.id.section_text);
        icon = (ImageView) view.findViewById(R.id.section_icon);
        pendingStatus = (TextView) view.findViewById(R.id.tv_section_pending_details);
        sectionParentLayout = (RelativeLayout) view.findViewById(R.id.rl_section_parent);

        this.isNew = isNewSection;
        if (isNew) {
            view.findViewById(R.id.iv_new_section).setVisibility(View.VISIBLE);
        } else {
            view.findViewById(R.id.iv_new_section).setVisibility(View.GONE);
        }

        textColorUnpressed = ctx.getResources().getColor(R.color.drawer_unselected_text_color);
        textColorSelected = ctx.getResources().getColor(R.color.drawer_selected_text_color);
        iconColorUnpressed = ctx.getResources().getColor(R.color.drawer_unselected_icon_color);
        iconColorSelected = ctx.getResources().getColor(R.color.drawer_selected_icon_color);
//        bgColorUnpressed = ctx.getResources().getColor(R.color.drawer_unpressed_bg_color);
//        bgColorSelected = ctx.getResources().getColor(R.color.drawer_selected_bg_color);

        this.position = position;
        this.targetType = targetType;
        this.isHomeFragment = isHomeFragment;
        this.showPendingStatus = showPendingStatus;

        if (showPendingStatus)
            pendingStatus.setVisibility(View.VISIBLE);

        view.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        select();

        if (listener != null)
            listener.onClick(this);

    }

    public void select() {

//        sectionParentLayout.setBackgroundColor(bgColorSelected);
        text.setTextColor(textColorSelected);
        icon.setColorFilter(textColorSelected);

    }

    public void unSelect() {

//        sectionParentLayout.setBackgroundColor(bgColorUnpressed);
        text.setTextColor(textColorUnpressed);
        icon.setColorFilter(null);

    }

    public int getPosition() {
        return position;
    }

    public void setOnClickListener(MaterialSectionListener listener) {
        this.listener = listener;
    }

    public View getView() {
        return view;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        this.text.setText(title);
        this.text.setTextColor(textColorUnpressed);
    }

    public void setIcon(Drawable icon) {
        this.icon.setImageDrawable(icon);
        this.icon.setColorFilter(null);
    }

    public void setIcon(Bitmap icon) {
        this.icon.setImageBitmap(icon);
        this.icon.setColorFilter(null);
    }

    public void setNoOfPendings(boolean showPendingStatus, String pendingCount) {
        this.showPendingStatus = showPendingStatus;
        this.pendingCount = pendingCount;

        if (showPendingStatus)
            pendingStatus.setVisibility(View.VISIBLE);
        else
            pendingStatus.setVisibility(View.GONE);

        this.pendingStatus.setText(pendingCount);
    }

    public void setTarget(Fragment target) {
        this.targetFragment = target;
    }

    public boolean getTarget() {
        return targetType;
    }

    public boolean isHomeFragment() {
        return isHomeFragment;
    }


    public void setTarget(Intent intent) {
        this.targetIntent = intent;
    }

    public Fragment getTargetFragment() {
        return targetFragment;
    }

    public Intent getTargetIntent() {
        return targetIntent;
    }

    public int getSectionColor() {
        return iconColorSelected;
    }

}
