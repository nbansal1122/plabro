package com.plabro.realestate;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.plabro.realestate.activity.Registration;
import com.plabro.realestate.uiHelpers.MaterialDialog;


public class GuidedTour extends ActionBarActivity {

    Button bt_skip, bt_nxt;
    LinearLayout v_1, v_2, v_3, v_4;
    ImageView iv_gt_img;

    int counter = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guided_tour);

        //intialization
        bt_skip = (Button) findViewById(R.id.gt_skip_to_app);
        bt_nxt = (Button) findViewById(R.id.gt_next);

        v_1 = (LinearLayout) findViewById(R.id.indicator_1);
        v_2 = (LinearLayout) findViewById(R.id.indicator_2);
        v_3 = (LinearLayout) findViewById(R.id.indicator_3);
        v_4 = (LinearLayout) findViewById(R.id.indicator_4);
        iv_gt_img = (ImageView) findViewById(R.id.gt_img);


        setIndicatorStyle(0);

        bt_nxt.setOnClickListener(customOnClickLsitener);
        bt_skip.setOnClickListener(customOnClickLsitener);

    }





    //custom on click listener to handle all clicks
    private View.OnClickListener customOnClickLsitener = new View.OnClickListener() {

        @Override
        public void onClick(View c_view) {

            Intent intent = new Intent(GuidedTour.this, Registration.class);
            // switch for different onclicks
            switch (c_view.getId()) {

                case R.id.gt_skip_to_app:

                    //Navigate to home

                    startActivity(intent);

                    break;

                case R.id.gt_next:


                    if (counter == 1) {
                        setIndicatorStyle(1);
                        counter++;
                    } else if (counter == 2) {
                        setIndicatorStyle(2);
                        counter++;
                    } else if (counter == 3) {
                        setIndicatorStyle(3);
                        counter++;
                    }


                    break;


                default:


            }

        }
    };

    //change view style to show the indicator
    void setIndicatorStyle(int ctr) {

        switch (ctr) {
            case 0:
                v_1.setVisibility(View.VISIBLE);
                v_2.setVisibility(View.GONE);
                v_3.setVisibility(View.GONE);
                v_4.setVisibility(View.GONE);
                break;
            case 1:
                v_1.setVisibility(View.GONE);
                v_2.setVisibility(View.VISIBLE);
                v_3.setVisibility(View.GONE);
                v_4.setVisibility(View.GONE);
                break;
            case 2:
                v_1.setVisibility(View.GONE);
                v_2.setVisibility(View.GONE);
                v_3.setVisibility(View.VISIBLE);
                v_4.setVisibility(View.GONE);
                break;
            case 3:
                v_1.setVisibility(View.GONE);
                v_2.setVisibility(View.GONE);
                v_3.setVisibility(View.GONE);
                v_4.setVisibility(View.VISIBLE);
                bt_nxt.setVisibility(View.GONE);


                break;

        }

    }


    @Override
    public void onBackPressed() {
        //confirm from user ofr exit
        showExitAlert();

    }


    void showExitAlert() {

        final MaterialDialog mMaterialDialog;
        mMaterialDialog= new MaterialDialog(this);

        mMaterialDialog.setTitle(R.string.exit_confirm_title)
                .setMessage(R.string.exit_confirm_msg)
                .setPositiveButton(R.string.exit, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                        // exit application
                        moveTaskToBack(true);

                    }
                })
                .setNegativeButton(R.string.cancel, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();


                    }
                });

        mMaterialDialog.show();


    }


}