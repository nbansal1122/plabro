package com.plabro.realestate.models.bids;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.models.ServerResponse;

public class BidParticipantResponse extends ServerResponse {

    @SerializedName("RT")
    @Expose
    private String RT;
    @SerializedName("loginStatus")
    @Expose
    private Boolean loginStatus;
    @SerializedName("output_params")
    @Expose
    private Output_params output_params;


    /**
     * @return The RT
     */
    public String getRT() {
        return RT;
    }

    /**
     * @param RT The RT
     */
    public void setRT(String RT) {
        this.RT = RT;
    }

    /**
     * @return The loginStatus
     */
    public Boolean getLoginStatus() {
        return loginStatus;
    }

    /**
     * @param loginStatus The loginStatus
     */
    public void setLoginStatus(Boolean loginStatus) {
        this.loginStatus = loginStatus;
    }

    /**
     * @return The output_params
     */
    public Output_params getOutput_params() {
        return output_params;
    }

    /**
     * @param output_params The output_params
     */
    public void setOutput_params(Output_params output_params) {
        this.output_params = output_params;
    }

}