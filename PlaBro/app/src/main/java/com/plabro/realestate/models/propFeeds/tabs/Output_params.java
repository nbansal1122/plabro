package com.plabro.realestate.models.propFeeds.tabs;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class Output_params {

    @Expose
    private List<TabInfo> data = new ArrayList<TabInfo>();

    /**
     * @return The data
     */
    public List<TabInfo> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<TabInfo> data) {
        this.data = data;
    }

}