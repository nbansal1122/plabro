package com.plabro.realestate.models.propFeeds;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by nitin on 08/10/15.
 */
@Table(name = "ShareFeed", id = BaseColumns._ID)
public class ShareFeed extends Model {
    public String getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(String feed_id) {
        this.feed_id = feed_id;
    }

    @Column
    private String feed_id;
}
