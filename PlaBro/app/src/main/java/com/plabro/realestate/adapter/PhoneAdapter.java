
package com.plabro.realestate.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.listeners.PhoneConnectListener;
import com.plabro.realestate.models.propFeeds.FilterClass;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.Exotel;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;

import java.util.HashMap;
import java.util.List;

public class PhoneAdapter extends ArrayAdapter<FilterClass> {
    private final String TAG = "PhoneAdapter";
    Context mContext;
    List<String> mList;
    int freeCall;
    HashMap<String, Object> wrData = new HashMap<>();
    String name = "";
    String image = "";
    PhoneConnectListener phoneConnectListener;
    private String freeCallPhoneNumber;

    public PhoneAdapter(Context context, List arr_phone, int freeCall, HashMap<String, Object> wrData, String profile_img, String profile_name, PhoneConnectListener phoneConnectListener) {
        super(context, R.layout.filter_layout, arr_phone);
        this.mList = arr_phone;
        this.freeCall = freeCall;
        this.wrData = wrData;
        this.name = profile_name;
        this.image = profile_img;
        this.mContext = context;
        this.phoneConnectListener = phoneConnectListener;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.phone_layout, parent, false);

        FontText phone = (FontText) rowView.findViewById(R.id.tv_user_phone);
        FontText otherPhone = (FontText) rowView.findViewById(R.id.tv_other_phone);
        FontText tvFreeCall = (FontText) rowView.findViewById(R.id.tv_free_call);
        FontText tvFreeCallInfo = (FontText) rowView.findViewById(R.id.tv_free_call_info);
        ImageView mFreeCallImg = (ImageView) rowView.findViewById(R.id.iv_freeCall);


        RelativeLayout mCall = (RelativeLayout) rowView.findViewById(R.id.rl_item);
        RelativeLayout mCallUser = (RelativeLayout) rowView.findViewById(R.id.rl_item_user);
        RelativeLayout mFreeCall = (RelativeLayout) rowView.findViewById(R.id.rl_item_free);
        View mDiv = (View) rowView.findViewById(R.id.v_div);


        FontText otherOptions = (FontText) rowView.findViewById(R.id.tv_other);
        phone.setText(mList.get(position));
        otherPhone.setText(mList.get(position));
        Log.d(TAG, mList.get(position) + "");

        if (position == 0) {
            mCallUser.setVisibility(View.GONE);
            mCall.setVisibility(View.VISIBLE);
        } else {
            mCall.setVisibility(View.GONE);
            mCallUser.setVisibility(View.VISIBLE);

        }

        if (mList.size() > 1 && position == 0) {
            otherOptions.setVisibility(View.VISIBLE);
            mDiv.setVisibility(View.GONE);

        } else {
            otherOptions.setVisibility(View.GONE);
            mDiv.setVisibility(View.VISIBLE);

        }


        mCall.setTag(mList.get(position));
        mCallUser.setTag(mList.get(position));
        mFreeCall.setTag(mList.get(position));
        if (position == 0) {
            if (freeCall == 2) {
                int colorPrimary = mContext.getResources().getColor(R.color.colorPrimary);
                tvFreeCall.setTextColor(colorPrimary);
                tvFreeCallInfo.setTextColor(colorPrimary);
                mFreeCallImg.setImageDrawable(Utility.getDrawableWithDiffColor(mContext.getResources().getDrawable(R.drawable.ic_free_call), colorPrimary));
                mFreeCall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showCallDialogToConnect();
//                        String phone = (String) view.getTag();
//                        Exotel exotel = new Exotel(mContext, callListener);
//                        try {
//                            // If in case phone does not connect show a message
//                            freeCallPhoneNumber = phone;
//
//                            exotel.connectPhoneNumbers(PBPreferences.getPhone(), phone);
//                            wrData.put("callType", "free");
//                            wrData.put("callTo", phone);
//                            Analytics.trackInteractionEvent(Analytics.CardActions.Called, wrData);
//                            wrData.remove("callTo");
//                            wrData.remove("callTo");
//                            Util.showSnackBarMessage((AppCompatActivity) mContext, mContext, "Connecting your call");
//                            phoneConnectListener.onResponse(true);
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            showCallDialogToConnect();
//
//                        }


                    }
                });
                mFreeCall.setVisibility(View.VISIBLE);

            } else if (freeCall == 1) {
                int gray = mContext.getResources().getColor(R.color.pbGray1);
                tvFreeCall.setTextColor(gray);
                tvFreeCallInfo.setTextColor(gray);
                mFreeCallImg.setImageDrawable(Utility.getDrawableWithDiffColor(mContext.getResources().getDrawable(R.drawable.ic_free_call), gray));
                mFreeCall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Util.showSnackBarMessage((AppCompatActivity) mContext, mContext, "Invite 2 professionals on Plabro to avail FREE calls");
                    }
                });
                mFreeCall.setVisibility(View.VISIBLE);

            } else if (freeCall == 0) {
                mFreeCall.setVisibility(View.GONE);
            }

        } else {
            mFreeCall.setVisibility(View.GONE);

        }

        mCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = (String) view.getTag();
                wrData.put("callTo", phone);
                ActivityUtils.callPhone(mContext, phone, wrData);
                wrData.remove("callTo");
                phoneConnectListener.onResponse(false);

            }
        });

        mCallUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = (String) view.getTag();
                wrData.put("callTo", phone);
                ActivityUtils.callPhone(mContext, phone, wrData);
                wrData.remove("callTo");
                phoneConnectListener.onResponse(false);

            }
        });


        return rowView;
    }

    private Exotel.ExotelListener callListener = new Exotel.ExotelListener() {
        @Override
        public void onPostExecute(String response) {
            if (TextUtils.isEmpty(response)) {
                showCallDialogToConnect();
            } else {
                showCallConnectingDialog();
            }
        }
    };

    private void showCallConnectingDialog(){
        try {
            if (null != mContext && !TextUtils.isEmpty(freeCallPhoneNumber)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Plabro");
                builder.setMessage("Please wait while we are connecting your call");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        wrData.put("callTo", freeCallPhoneNumber);
                        ActivityUtils.callPhone(mContext, freeCallPhoneNumber, wrData);
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
            }
        } catch (Exception e) {

        }
    }
    private void showCallDialogToConnect() {
        try {
            if (null != mContext && !TextUtils.isEmpty(freeCallPhoneNumber)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("Unable to connect your free call\n\nPlease try calling "+name+" directly");
                builder.setPositiveButton("CALL NOW", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        wrData.put("callTo", freeCallPhoneNumber);
                        ActivityUtils.callPhone(mContext, freeCallPhoneNumber, wrData);
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
            }
        } catch (Exception e) {

        }
    }


}