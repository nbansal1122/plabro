package com.plabro.realestate.models.tnc;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class TNCOutputParams {

    TNC data = new TNC();

    public TNC getData() {
        return data;
    }

    public void setData(TNC data) {
        this.data = data;
    }
}
