/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.plabro.realestate.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.FeedDetails;
import com.plabro.realestate.activity.Shout;
import com.plabro.realestate.entities.MyShoutsDBModel;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.OnChildListener;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.BookmarkTag;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.models.shouts.DeleteShoutResponse;
import com.plabro.realestate.models.shouts.RepostShoutResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.MaterialDialog;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ShoutsCustomAdapter extends RecyclerView.Adapter<ShoutsCustomAdapter.ViewHolder> implements VolleyListener {

    private static Context mContext;
    private static Activity mActivity;
    private static OnChildListener updateShouts;
    private ProgressDialog dialog;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    private String searchText = "";
    private int feedsCount = 0;
    private static final String TAG = "ShoutsCustomAdapter";
    private static int hashKey = 0;

    private ArrayList<Feeds> feeds = new ArrayList<>();
    private ArrayList<Feeds> arrayList = new ArrayList<>();


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView iv_options;
        private final TextView tv_post_time, post_content, tv_post_id, user_phone;
        private final TextView anchor;
        private final CardView card_view;
        private final LinearLayout mMore;
        private final TextView postContentTitle;

        public ViewHolder(View v) {
            super(v);

            this.postContentTitle = (TextView) itemView.findViewById(R.id.post_content_title);
            this.tv_post_time = (TextView) itemView.findViewById(R.id.post_time);
            this.post_content = (TextView) itemView.findViewById(R.id.post_content);
            this.iv_options = (ImageView) itemView.findViewById(R.id.card_option);
            this.tv_post_id = (TextView) itemView.findViewById(R.id.post_id);
            this.user_phone = (TextView) itemView.findViewById(R.id.user_phone);
            this.anchor = (TextView) itemView.findViewById(R.id.popupMenyAnchor);
            this.card_view = (CardView) itemView.findViewById(R.id.card_view);
            this.mMore = (LinearLayout) itemView.findViewById(R.id.more);


        }


        public ImageView getIv_options() {
            return iv_options;
        }


        public TextView getTv_post_time() {
            return tv_post_time;
        }

        public TextView getPost_content() {
            return post_content;
        }

        public TextView getTv_post_id() {
            return tv_post_id;
        }

        public TextView getUser_phone() {
            return user_phone;
        }

        public TextView getAnchor() {
            return anchor;
        }


        public CardView getCard_view() {
            return card_view;
        }

        public LinearLayout getmMore() {
            return mMore;
        }
    }


    public ShoutsCustomAdapter(Context mContext, ArrayList<Feeds> feeds, Activity activity, OnChildListener updateShouts) {
        this.mContext = mContext;
        this.feeds = feeds;
        mActivity = activity;
        feedsCount = feeds.size();
        this.updateShouts = updateShouts;
        arrayList.addAll(feeds);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.shouts_cards_layout, viewGroup, false);


        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");
        final Feeds feed = feeds.get(position);

        viewHolder.getTv_post_time().setText(feed.getTime());
        viewHolder.getTv_post_id().setText(feed.get_id() + "");
        viewHolder.getUser_phone().setText(feed.getProfile_phone());

        BookmarkTag bt = new BookmarkTag();
        bt.setId(feed.get_id());


        viewHolder.getIv_options().setTag(feed);
        viewHolder.getPost_content().setTag(feed);
        viewHolder.getmMore().setTag(feed);

        String text = feed.getText();
        int start = 0;
        int end = 250;

        if (feed.getSnippet().size() > 0) {

            start = Integer.parseInt(feed.getSnippet().get(0));
            end = Integer.parseInt(feed.getSnippet().get(1));
        }

        //since now we show details page link
        /*        if (feed.getText().length() <= end) {
                    viewHolder.getmMore().setVisibility(View.GONE);

                } else {

                    text = text.substring(start, end);
                    //  viewHolder.getPost_content().setMaxLines(4);
                    viewHolder.getmMore().setVisibility(View.VISIBLE);
               }
        */

        //adding span for hashtags in post
        ActivityUtils.setHashTags(feed, text, start, end, mContext, viewHolder.getPost_content(), "Shouts", position, false, hashKey, mActivity);


        viewHolder.getCard_view().setTag(feed);

        viewHolder.mMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Feeds feed = (Feeds) v.getTag();
                getShoutDetails(feed.get_id());
//                FragmentManager mFragmentManager = mActivity.getFragmentManager();
//                DialogFragment feedPopUpDialog = new FeedPopUpDialog(feed, mContext, mActivity, hashKey);
//                feedPopUpDialog.show(mFragmentManager, "feed");
            }
        });


        viewHolder.post_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id = ((Feeds) v.getTag()).get_id();

                Log.d(TAG, "Element " + id + " clicked.");
                //Toast.makeText(mContext, id, Toast.LENGTH_LONG).show();
                Log.d(TAG, "Element " + " clicked.");

                FeedDetails.startFeedDetails(mContext, id, hashKey + "", ((Feeds) v.getTag()));


            }
        });


        viewHolder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id = ((Feeds) v.getTag()).get_id();

                Log.d(TAG, "Element " + id + " clicked.");
                //Toast.makeText(mContext, id, Toast.LENGTH_LONG).show();
                Log.d(TAG, "Element " + " clicked.");

                FeedDetails.startFeedDetails(mContext, id, hashKey + "", ((Feeds) v.getTag()));

            }
        });

        viewHolder.card_view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                String text = ((Feeds) v.getTag()).getText();
                ActivityUtils.copyFeed(text, mContext);
                return true;
            }
        });

        viewHolder.post_content.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                String text = ((Feeds) v.getTag()).getText();
                ActivityUtils.copyFeed(text, mContext);
                return true;
            }
        });


        viewHolder.iv_options.setOnClickListener(new View.OnClickListener() {
                                                     @Override
                                                     public void onClick(View v) {

                                                         final Feeds feedObject = (Feeds) v.getTag();

                                                         PopupMenu popup = new PopupMenu(mContext, viewHolder.iv_options);
                                                         //Inflating the Popup using xml file
                                                         popup.getMenuInflater().inflate(R.menu.shout_options_menu, popup.getMenu());


                                                         //registering popup with OnMenuItemClickListener
                                                         popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                                                                              public boolean onMenuItemClick(MenuItem item) {

                                                                                                  String post_id = feedObject.get_id().toString();
                                                                                                  String shoutText = feedObject.getText();


                                                                                                  switch (item.getTitle().toString()) {
                                                                                                      case "Delete Shout":
                                                                                                          HashMap<String, Object> wrdata = new HashMap<>();
                                                                                                          wrdata.put("ScreenName", TAG);
                                                                                                          wrdata.put("Action", "Delete");
                                                                                                          wrdata.put("Position", "" + position);
                                                                                                          Analytics.trackInteractionEvent(Analytics.Actions.DeleteShout, wrdata);


                                                                                                          deleteShout(post_id);

                                                                                                          break;

                                                                                                      case "Edit Shout":
//
                                                                                                          //WizRocket tracking
                                                                                                          HashMap<String, Object> wrdata1 = new HashMap<>();
                                                                                                          wrdata1.put("ScreenName", TAG);
                                                                                                          wrdata1.put("Action", "Edit Shout");
                                                                                                          wrdata1.put("Position", "" + position);
                                                                                                          Analytics.trackInteractionEvent(Analytics.CardActions.Edit, wrdata1);

                                                                                                          Intent intent = new Intent(mContext, Shout.class);
                                                                                                          intent.putExtra("postId", post_id);
                                                                                                          intent.putExtra("feedObject", feedObject);
                                                                                                          intent.putExtra("shoutType", "edit");
                                                                                                          mContext.startActivity(intent);
                                                                                                          ((Activity) mContext).overridePendingTransition(R.anim.bottom_in_fast, R.anim.bottom_out_fast);


                                                                                                          break;

                                                                                                      case "Repost Shout":
//
                                                                                                          //WizRocket tracking
                                                                                                          HashMap<String, Object> wrdata2 = new HashMap<>();
                                                                                                          wrdata2.put("ScreenName", TAG);
                                                                                                          wrdata2.put("Action", "Repost");
                                                                                                          wrdata2.put("Position", "" + position);
                                                                                                          Analytics.trackInteractionEvent(Analytics.Actions.Repost, wrdata2);

                                                                                                          repostShout(post_id);

                                                                                                          break;


                                                                                                  }


                                                                                                  return true;
                                                                                              }
                                                                                          }

                                                         );


                                                         popup.show();//showing popup menu


                                                     }
                                                 }

        );


    }

    @Override
    public int getItemCount() {
        return feeds.size();
    }

    public void addItems(ArrayList<Feeds> feeds) {
        for (Feeds feeds1 : feeds) {
            addItem(feedsCount, feeds1);
        }

    }

    public void addItem(int position, Feeds feed) {
        feeds.add(position, feed);
        notifyItemInserted(position);
        feedsCount++;
    }

    public void removeItem(int position) {
        feeds.remove(position);
        notifyItemRemoved(position);
    }

    public void removeItemFromList(String shout_id) {

        int pos = 0;
        for (Feeds feed : feeds) {
            if (feed.get_id().equals(shout_id))
                break;
            pos++;
        }

        if (pos < feeds.size()) {
            removeItem(pos);
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            });
        }
    }

    static void deleteShout(final String shout_id) {

        //Crittercism.beginTransaction("Delete");


        final ProgressDialog progress;
        progress = ProgressDialog.show(mActivity, "Please wait",
                "Deleting Shouts...", true);
        progress.setCancelable(true);
        progress.setCanceledOnTouchOutside(true);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.PLABRO_DELETE_SHOUT_PARAM_SHOUT_ID, shout_id);
        params = Utility.getInitialParams(PlaBroApi.RT.DELETE_SHOUTS, params);
        Log.i("PlaBro", "Message id" + shout_id);
        AppVolley.processRequest(1, DeleteShoutResponse.class, null, PlaBroApi.getBaseUrl(mContext), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);
                new Delete().from(MyShoutsDBModel.class).where("_id = ?", shout_id).execute();
                DeleteShoutResponse deleteShoutResponse = (DeleteShoutResponse) response.getResponse();
                updateShouts.onResponse(true);

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);

                switch (response.getCustomException().getExceptionType()) {
                    case SESSION_EXPIRED:
                        showToast(response.getCustomException().getMessage());
                        Utility.userLogin(mContext);
                        break;
                    case STATUS_FALSE_EXCEPTION:
                        showToast(response.getCustomException().getMessage());
                        break;
                    case TRIAL_EXPIRED_EXCEPTION:
                        showToast(response.getCustomException().getMessage());
                        break;
                }

            }

        });

    }

    public static void showToast(String toastMessage) {
        if (null == toastMessage || TextUtils.isEmpty(toastMessage)) {
            toastMessage = "Cannot Delete Shout, Please check your network or try again";
        }
        Toast toast = Toast.makeText(mContext, toastMessage, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);

        LinearLayout layout = (LinearLayout) toast.getView();
        if (layout.getChildCount() > 0) {
            TextView tv = (TextView) layout.getChildAt(0);
            tv.setGravity(Gravity.CENTER);
        }

        toast.show();
    }

    static void repostShout(final String shout_id) {

        //  Crittercism.beginTransaction("Repost");
        final ProgressDialog progress;
        progress = ProgressDialog.show(mContext, "Please wait",
                "Reposting Shouts...", true);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.PLABRO_DELETE_SHOUT_PARAM_SHOUT_ID, shout_id);
        params = Utility.getInitialParams(PlaBroApi.RT.REPOST_SHOUTS, params);
        Log.i("PlaBro", "Message id" + shout_id);
        AppVolley.processRequest(Constants.TASK_CODES.REPOST_SHOUT, RepostShoutResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);

                RepostShoutResponse repostShoutResponse = (RepostShoutResponse) response.getResponse();


                if (repostShoutResponse.isStatus()) {

                    // Crittercism.endTransaction("Repost");


                    Toast.makeText(mContext, repostShoutResponse.getMsg(), Toast.LENGTH_LONG).show();

                    updateShouts.onResponse(true);


                } else {

                    //Crittercism.failTransaction("Repost");

                    Log.i("PlaBro", "error posting shout feed due to : " + repostShoutResponse.getMsg());


                    Toast.makeText(mContext, repostShoutResponse.getMsg(), Toast.LENGTH_LONG).show();


                    if (repostShoutResponse.getMsg().equalsIgnoreCase("Session Expired") || repostShoutResponse.getMsg().equalsIgnoreCase("Missing AuthKey in the request")) {
                        if (!PBPreferences.getSessEXpDialogState()) {

                            PBPreferences.setSessEXpDialogState(true);

                            final MaterialDialog mMaterialDialog = new MaterialDialog(mContext);

                            mMaterialDialog.setTitle("Try again")
                                    .setMessage("Oops ! Your Session Expired.")
                                    .setPositiveButton("CONNECT", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Utility.userLogin(mContext);
                                            repostShout(shout_id);
                                            mMaterialDialog.dismiss();
                                        }
                                    })

                                    .setNegativeButton("CANCEL", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mMaterialDialog.dismiss();
                                        }
                                    });

                            mMaterialDialog.show();
                        }
                    }
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);

                Toast.makeText(mContext, response.getCustomException().getMessage(), Toast.LENGTH_LONG).show();

                Log.i("PlaBro", response.getCustomException().getMessage());
            }


        });

    }

    // Filter Class
    public void filter(String charText) {
        searchText = charText.toLowerCase(Locale.getDefault());
        feeds.clear();
        if (arrayList.size() > 0) {
            if (charText.length() == 0) {
                feeds.addAll(arrayList);
            } else {
                for (Feeds gsd : arrayList) {
                    if (gsd.getText().toLowerCase(Locale.getDefault()).contains(searchText)) {
                        feeds.add(gsd);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public int getCurrentItemSize() {
        return arrayList.size();
    }

    public void updateItems(ArrayList<Feeds> feeds, boolean addMore) {
        if (!addMore)
            arrayList.clear();
        arrayList.addAll(feeds);
    }


    private void getShoutDetails(String message_id) {
        ParamObject obj = new ParamObject();
        obj.setClassType(FeedResponse.class);
        obj.setTaskCode(Constants.TASK_CODES.SHOUT_DETAILS);
        HashMap<String, String> params = new HashMap<>();
        params.put(PlaBroApi.PLABRO_FEED_DETAILS_POST_PARAM_MESSAGE_ID, message_id);
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.SHOUT_DETAILS, params));
        showDialog();
        AppVolley.processRequest(obj, this);
    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        hideDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.SHOUT_DETAILS:
                FeedResponse resp = (FeedResponse) response.getResponse();
                if (null != resp.getOutput_params() && null != resp.getOutput_params().getData()) {
                    List<BaseFeed> feeds = resp.getOutput_params().getData();
                    if (feeds.size() > 0) {
                        Feeds feedsObject = (Feeds) feeds.get(0);
                        FeedDetails.startFeedDetails(mContext, feedsObject.get_id(), "", feedsObject);
                    }
                }
                break;
        }
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        hideDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.SHOUT_DETAILS:
                showFailureMessage(response);
                break;
        }
    }

    private void showDialog() {
        if (dialog == null) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Loading...");
            dialog.show();
        } else if (!dialog.isShowing()) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Loading...");
            dialog.show();
        }
    }

    private void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private void showFailureMessage(PBResponse response) {
        showToast(response.getCustomException().getMessage());
    }
}
