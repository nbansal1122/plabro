package com.plabro.realestate.models.matchMaking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MatchMakingData {

    @SerializedName("shout_id_src")
    @Expose
    private Integer shout_id_src;
    @SerializedName("userid_src")
    @Expose
    private Integer userid_src;
    @SerializedName("listing_working")
    @Expose
    private List<PlabroWorking> listing_working = new ArrayList<PlabroWorking>();
    @SerializedName("listing_src")
    @Expose
    private String listing_src;
    @SerializedName("_id")
    @Expose
    private Integer _id;

    /**
     * @return The shout_id_src
     */
    public Integer getShout_id_src() {
        return shout_id_src;
    }

    /**
     * @param shout_id_src The shout_id_src
     */
    public void setShout_id_src(Integer shout_id_src) {
        this.shout_id_src = shout_id_src;
    }

    /**
     * @return The userid_src
     */
    public Integer getUserid_src() {
        return userid_src;
    }

    /**
     * @param userid_src The userid_src
     */
    public void setUserid_src(Integer userid_src) {
        this.userid_src = userid_src;
    }

    /**
     * @return The listing_working
     */
    public List<PlabroWorking> getListing_working() {
        return listing_working;
    }

    /**
     * @param plabroWorking The plabroWorking
     */
    public void setListing_working(List<PlabroWorking> plabroWorking) {
        this.listing_working = plabroWorking;
    }

    /**
     * @return The listing_src
     */
    public String getListing_src() {
        return listing_src;
    }

    /**
     * @param listing_src The listing_src
     */
    public void setListing_src(String listing_src) {
        this.listing_src = listing_src;
    }

    /**
     * @return The _id
     */
    public Integer get_id() {
        return _id;
    }

    /**
     * @param _id The _id
     */
    public void set_id(Integer _id) {
        this._id = _id;
    }

}