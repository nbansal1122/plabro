package com.plabro.realestate.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.models.propFeeds.AdvertisementFeed;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.DeepLinkNavigationUtil;

/**
 * Created by nitin on 22/01/16.
 */
public class AdvertisementFeedHolder extends BaseFeedHolder {
    private TextView actionTitleTv, titleTv, subtitleTv;
    private ImageView img;

    @Override
    public String getTAG() {
        return "AdvertisementCard";
    }

    public AdvertisementFeedHolder(View itemView) {
        super(itemView);
        actionTitleTv = findTV(R.id.tv_action_title);
        titleTv = findTV(R.id.tv_auction_title);
        subtitleTv = findTV(R.id.tv_auction_subtitle);
    }

    @Override
    public void onBindViewHolder(final int position, BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        if (feed instanceof AdvertisementFeed) {
            final AdvertisementFeed f = (AdvertisementFeed) feed;
            actionTitleTv.setText(f.getAction_title());
            titleTv.setText(f.getTitle());
            subtitleTv.setText(f.getSubtitle());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    trackEvent(Analytics.CardActions.CardClicked, position, "AdvertisementFeedId", f.getType(), null);
                    DeepLinkNavigationUtil.navigate(f.getDeepLinkBundle(), ctx);
                }
            });
        }
    }


}
