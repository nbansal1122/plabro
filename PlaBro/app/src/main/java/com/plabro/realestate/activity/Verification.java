package com.plabro.realestate.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsSession;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.Tracker.Campaign;
import com.plabro.realestate.models.registerUser.RegisterResponse;
import com.plabro.realestate.models.registerUser.SMSResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.CleverTap;
import com.plabro.realestate.others.Exotel;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.uiHelpers.MaterialDialog;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.utils.DigitVerification;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.software.shell.fab.ActionButton;

import java.util.HashMap;


public class Verification extends PlabroBaseActivity {

    //verification code edit text
    private static EditText et_vc_1, et_vc_2, et_vc_3, et_vc_4;
    private FontText tv_error_msg, tv_req_sms, request_c_cl, tv_title;
    static ProgressDialog progressVer;
    static Context ct;
    int retryCounter = 1;
    private CountDownTimer countDownTimer;
    private boolean callRequested = false;
    private boolean timerHasStarted = false;

    private final long startTime = 30 * 1000;
    private final long interval = 1 * 1000;
    private FontText mMsgOne, MMsgCounter, mMsgTwo;
    private LinearLayout mMsgLayout, requestLayout;
    private FontText mSubmit;
    private boolean isCodeValid = false;

    @Override
    protected String getTag() {
        return "Verification";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        ct = this;

        countDownTimer = new MyCountDownTimer(startTime, interval);

        //initialise and add default functionality
        setDefauts();
        getSMSReadPermission(PlaBroApi.PERMISSIONS.READ_SMS);
        sendSMSCode();
        sendDigitVerification();
        final ScrollView scrollview = ((ScrollView) findViewById(R.id.scroll));
        scrollview.post(new Runnable() {
            @Override
            public void run() {
                scrollview.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });

    }

    private void getSMSReadPermission(int permission) {
        switch (permission) {
            case PlaBroApi.PERMISSIONS.READ_SMS:
                hasPermission(Manifest.permission.READ_SMS, PlaBroApi.PERMISSIONS.READ_SMS, PlaBroApi.PERMISSIONS_MESSAGE.READ_SMS);

                break;
            case PlaBroApi.PERMISSIONS.RECEIVE_SMS:
                hasPermission(Manifest.permission.RECEIVE_SMS, PlaBroApi.PERMISSIONS.RECEIVE_SMS, PlaBroApi.PERMISSIONS_MESSAGE.RECEIVE_SMS);

                break;
        }
    }

    @Override
    public void onMMPermisssoinResult(int requestCode, boolean status, String message) {
        super.onMMPermisssoinResult(requestCode, status, message);
        switch (requestCode) {
            case PlaBroApi.PERMISSIONS.READ_SMS:
                if (!status) {
                    showSnackBar("Permission denied for READ_SMS");
                    return;
                } else {
                    registerSMSReceiver();
                }
                getSMSReadPermission(PlaBroApi.PERMISSIONS.RECEIVE_SMS);
                break;
            case PlaBroApi.PERMISSIONS.RECEIVE_SMS:
                if (!status) {
                    showSnackBar("Permission denied for RECEIVE_SMS");

                }

                break;

        }

    }


    private void setDefauts() {

        et_vc_1 = (EditText) findViewById(R.id.vc_1);
        et_vc_2 = (EditText) findViewById(R.id.vc_2);
        et_vc_3 = (EditText) findViewById(R.id.vc_3);
        et_vc_4 = (EditText) findViewById(R.id.vc_4);

        tv_error_msg = (FontText) findViewById(R.id.error_msg);
        tv_req_sms = (FontText) findViewById(R.id.request_sms);
        request_c_cl = (FontText) findViewById(R.id.request_c_cl);
        mMsgOne = (FontText) findViewById(R.id.tv_msg_one);
        MMsgCounter = (FontText) findViewById(R.id.tv_msg_counter);
        mMsgTwo = (FontText) findViewById(R.id.tv_msg_two);
        tv_title = (FontText) findViewById(R.id.tv_title);
        tv_title.setText(getResources().getString(R.string.sms_msg) + " " + PBPreferences.getPhone());
        //mMsgThree = (FontText) findViewById(R.id.tv_msg_three);

        mMsgLayout = (LinearLayout) findViewById(R.id.ll_msg_layout);
        requestLayout = (LinearLayout) findViewById(R.id.ll_request);

        tv_req_sms.setText(tv_req_sms.getText());
        tv_req_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!timerHasStarted) {
                    if (callRequested) {
                        SMSConfirmation();
                    } else {
                        promptMsgSent();
                    }
                } else {
                    Toast.makeText(Verification.this, "Please wait your code is arriving soon.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        request_c_cl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!timerHasStarted) {
                    startCounter();
                    callRequested = true;
                    initiateCall();
                } else {
                    Toast.makeText(Verification.this, "Please wait your code is arriving soon.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        // adding listeners

        et_vc_1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) { // TODO Auto-generated method stub
                if (et_vc_1.getText().toString().length() == 1) {
                    //shift focus and clear next value
                    et_vc_1.clearFocus();
                    et_vc_2.requestFocus();
                    et_vc_2.setText("");
                    et_vc_2.setCursorVisible(true);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) { // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        et_vc_2.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) { // TODO Auto-generated method stub
                if (et_vc_2.getText().toString().length() == 1) {
                    //shift focus
                    et_vc_2.clearFocus();
                    et_vc_3.requestFocus();
                    et_vc_3.setText("");
                    et_vc_3.setCursorVisible(true);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) { // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        et_vc_3.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) { // TODO Auto-generated method stub

                if (et_vc_3.getText().toString().length() == 1) {
                    //shift focus
                    et_vc_3.clearFocus();
                    et_vc_4.requestFocus();
                    et_vc_4.setText("");
                    et_vc_4.setCursorVisible(true);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) { // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        et_vc_4.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) { // TODO Auto-generated method stub
                if (et_vc_4.getText().toString().length() == 1) {
                    //shift focus
                    //et_vc_4.clearFocus();
                    //sms validation
                    validate_passcode();
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) { // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
        mSubmit = (FontText) findViewById(R.id.tv_submit);
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //navigate to next screen
                registerUser();
            }
        });


    }

    private Exotel.ExotelListener callListener = new Exotel.ExotelListener() {
        @Override
        public void onPostExecute(String response) {
            if (response == null) {
                //   callCustomerCare();
            }
        }
    };

    private void initiateCall() {
        if (!Util.haveNetworkConnection(Verification.this)) {
            showToast("No network available,Please try again.");
            return;
        }

//        Exotel exotel = new Exotel(Verification.this, callListener);
        try {
            showToast(getString(R.string.msg_exotel_call));
            CleverTap.recordEvent(Constants.RegistrationParams.CALL_BACK);
        } catch (Exception e) {
            e.printStackTrace();
            showToast("No network available,Please try again.");
        }
    }


    private void SMSConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(R.string.sms_conf_one).setTitle("Warning!")
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                promptMsgSent();
                                callRequested = false;
                            }
                        }
                ).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void validate_passcode() {
        String passcode = et_vc_1.getText().toString() + et_vc_2.getText().toString() + et_vc_3.getText().toString() + et_vc_4.getText().toString();

        if (passcode.equalsIgnoreCase(Constants.VERIFICATIONCODE)) {

            //Crittercism.endTransaction("SMS");

            //everything goes fine, lets move it to home baby

            //change text color to white
            et_vc_1.setTextColor(Color.parseColor("#ffffff"));
            et_vc_2.setTextColor(Color.parseColor("#ffffff"));
            et_vc_3.setTextColor(Color.parseColor("#ffffff"));
            et_vc_4.setTextColor(Color.parseColor("#ffffff"));

            //change background box color to green
            et_vc_1.setBackgroundResource(R.drawable.white_ring);
            et_vc_2.setBackgroundResource(R.drawable.white_ring);
            et_vc_3.setBackgroundResource(R.drawable.white_ring);
            et_vc_4.setBackgroundResource(R.drawable.white_ring);

            tv_error_msg.setVisibility(View.GONE);

            checkAndEnableNext(true);

            registerUser();


        } else {
            //Crittercism.failTransaction("SMS");

            //oops code is wrong lets show the user
            //change text color to red
            et_vc_1.setTextColor(Color.parseColor("#FF0000"));
            et_vc_2.setTextColor(Color.parseColor("#FF0000"));
            et_vc_3.setTextColor(Color.parseColor("#FF0000"));
            et_vc_4.setTextColor(Color.parseColor("#FF0000"));

            //change background box color to red
            et_vc_1.setBackgroundResource(R.drawable.red_ring);
            et_vc_2.setBackgroundResource(R.drawable.red_ring);
            et_vc_3.setBackgroundResource(R.drawable.red_ring);
            et_vc_4.setBackgroundResource(R.drawable.red_ring);

            tv_error_msg.setVisibility(View.VISIBLE);

            checkAndEnableNext(false);


        }


    }


    void showSMSSentAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);


        builder.setMessage(R.string.sms_sent_msg1).setTitle(R.string.sms_sent_title)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void showSMSErrorAlert() {

        final MaterialDialog mMaterialDialog;
        mMaterialDialog = new MaterialDialog(this);

        mMaterialDialog.setTitle(R.string.sms_error_title)
                .setMessage(R.string.sms_error_msg)
                .setPositiveButton(R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();

                    }
                });

        mMaterialDialog.show();


    }

    @Override
    public void onBackPressed() {
        //confirm from user ofr exit
        super.onBackPressed();


    }

    void showExitAlert() {

        final MaterialDialog mMaterialDialog;
        mMaterialDialog = new MaterialDialog(this);

        mMaterialDialog.setTitle(R.string.exit_confirm_title)
                .setMessage(R.string.exit_confirm_msg)
                .setPositiveButton(R.string.exit, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                        // exit application
                        moveTaskToBack(true);

                    }
                })
                .setNegativeButton(R.string.cancel, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();


                    }
                });

        mMaterialDialog.show();


    }


    void promptMsgSent() {

        final MaterialDialog mMaterialDialog;
        mMaterialDialog = new MaterialDialog(this);

        String msg = "A passcode is sent as SMS to the following number." + "\n" + PBPreferences.getPhone() + "\n\n" + "If the number is incorrect press back and  re-enter your phone number.";

        mMaterialDialog.setTitle(R.string.sms_sent_title)
                .setMessage(msg)
                .setPositiveButton(R.string.done, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                        sendSMSCode();

                    }
                })
                .setNegativeButton(R.string.reenter, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                        onBackPressed();
                    }
                });

        mMaterialDialog.show();


    }

    private void sendDigitVerification() {
        DigitVerification.getInstance(AppController.getInstance()).authenticate(PBPreferences.getPhone(), new DigitVerification.DigitCallback() {
            @Override
            public void success(DigitsSession session, String phoneNumber) {
                Log.d(TAG, "on Success");
            }

            @Override
            public void failure(DigitsException exception) {
                exception.printStackTrace();
            }
        });
    }


    void sendSMSCode() {


        startCounter();

        if (!Util.haveNetworkConnection(Verification.this)) {
            Toast.makeText(Verification.this, "Please check your network.", Toast.LENGTH_LONG).show();
        }

        final String verificationCode = Utility.getRandomFourDigits();
        //  Toast.makeText(Verification.this, verificationCode, Toast.LENGTH_LONG).show();

        Constants.VERIFICATIONCODE = verificationCode;

        Log.d(TAG, "Code" + verificationCode);

        progressVer = ProgressDialog.show(this, "Please wait",
                "Let's check if you are human.", true);


        HashMap<String, String> params = new HashMap<String, String>();

        params.put(PlaBroApi.IMEI, PBPreferences.getImei());
        params.put(PlaBroApi.PHONE, PBPreferences.getPhone());
        params.put(PlaBroApi.SMSCODE, Constants.VERIFICATIONCODE);
        params.put(PlaBroApi.ISD_CODE, PBPreferences.getCountryCode());
        params.put(PlaBroApi.RETRY, retryCounter + "");
        String uri = PBPreferences.getData(PBPreferences.CAMPAIGN_URI, null);
        Log.d(TAG, "Campaign URI:" + uri);
        if (uri != null) {
            Log.d(TAG, "Tracking URL" + uri);
            //sendScreenName(R.string.f_app_campaign_found);
            params.putAll(Campaign.parseUri(uri));
        }

        params = Utility.getInitialParamsWithoutAuthkey(PlaBroApi.RT.CONFIRMATIONSMS, params);


        if (retryCounter > 1) {
            HashMap<String, Object> metaData = new HashMap<>();
            metaData.put(Constants.RegistrationParams.SMS_RETRIED, true);
            metaData.put(PlaBroApi.RETRY, retryCounter + "");
        }

        retryCounter++;

        //sendScreenName(R.string.funnel_SMS_TRIGGERED);


        AppVolley.processRequest(Constants.TASK_CODES.CONFIRMATION_SMS, SMSResponse.class, null, PlaBroApi.getBaseUrl(Verification.this), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progressVer);

                Log.i("PlaBro", "SMS SEND SUCCESSFULLY ");
                //sendScreenName(R.string.funnel_SMS_SENT);

                SMSResponse smsResponse = (SMSResponse) response.getResponse();

                if (smsResponse.isSmsbp()) {
                    registerUser();
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progressVer);
                onApiFailure(response, taskCode);
                //showRegistrationError("");
                Log.i("PlaBro", "onFailure : Oops...something went wrong");
            }
        });


    }

    @Override
    public void onStatusFalse(PBResponse response, int taskCode) {
        super.onStatusFalse(response, taskCode);
        showRegistrationError(response.getCustomException().getMessage());
    }

    private static boolean isOtpFilledAutomatically;

    public static void receiveSMS(String msg, Boolean status) {

        if (status) {
            String code = msg.substring(msg.indexOf(": ") + 2, msg.length());
            String codeDigits = code;
            et_vc_1.setText(code.substring(0, 1));
            et_vc_2.setText(code.substring(1, 2));
            et_vc_3.setText(code.substring(2, 3));
            et_vc_4.setText(code.substring(3, 4));
            isOtpFilledAutomatically = true;
        } else {
            Toast.makeText(ct, "Unable to fetch code. Please enter code manually.", Toast.LENGTH_LONG).show();
        }

        Log.v("new sms", msg);
        // progressVer.dismiss();


    }


    void registerUser() {

        if (!isCodeValid) {
            showToast("Invalid code entered");
            return;
        }
        final ProgressDialog progress;
        progress = ProgressDialog.show(Verification.this, "Please wait",
                "Registration in progress...", true);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.IMEI, PBPreferences.getImei());
        params.put(PlaBroApi.PHONE, PBPreferences.getPhone());
        params.put(PlaBroApi.ISD_CODE, PBPreferences.getCountryCode());
        params = Utility.getInitialParamsWithoutAuthkey(PlaBroApi.RT.REGISTER_USER, params);

        HashMap<String, Object> metaData = new HashMap<>();
        if (isOtpFilledAutomatically) {
            metaData.put(Constants.RegistrationParams.OTP_FILLED, "Auto");
        } else {
            metaData.put(Constants.RegistrationParams.OTP_FILLED, "Manual");
        }
        Analytics.sendRegistrationData(metaData);
        AppVolley.processRequest(Constants.TASK_CODES.REGISTER_USER, RegisterResponse.class, null, PlaBroApi.getBaseUrl(Verification.this), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);

                RegisterResponse registerResponse = (RegisterResponse) response.getResponse();
                //sendScreenName(R.string.funnel_SMS_APPROVED);
                //Navigate to preferences
                Intent intent = new Intent(Verification.this, Preferences.class);
                startActivity(intent);
                finish();
                //        Log.i("PlaBro", "Account register sussessfully" + registerResponse.isStatus());


            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);

                showRegistrationError(response.getCustomException().getMessage());
                Log.i("PlaBro", "onFailure : Oops...something went wrong");

            }
        });
    }

    void checkAndEnableNext(boolean enable) {
        isCodeValid = enable;
    }

    void showRegistrationError(String msg) {

//        Toast.makeText(Verification.this, "Please contact admin@plabro.com to get access to the app", Toast.LENGTH_LONG).show();

        String errorMsg = "Please contact admin@plabro.com to get access to the app";
        if (null != msg || !msg.equalsIgnoreCase("")) {
            errorMsg = msg;

        }

        final MaterialDialog mMaterialDialog;
        mMaterialDialog = new MaterialDialog(this);

        mMaterialDialog.setTitle("Registration Error!")
                .setMessage(errorMsg)
                .setPositiveButton("Contact Now", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();

                        // send email
                        PBSharingManager.sendMail(Verification.this, Constants.INVITE_MAIL, "Registration Request", "", "Request Plabro");

                    }
                })
                .setNegativeButton("Later", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();


                    }
                });

        try {
            mMaterialDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void findViewById() {

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void inflateToolbar() {

    }

    public class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            countDownTimer.cancel();
            timerHasStarted = false;
            mMsgLayout.setVisibility(View.INVISIBLE);
            requestLayout.setVisibility(View.VISIBLE);
            enablebuttonsView();

            if (retryCounter > 2) {
                tv_req_sms.setText(getResources().getString(R.string.sms_req));
            } else {
                tv_req_sms.setText(getResources().getString(R.string.sms_req));

            }
        }

        @Override
        public void onTick(long millisUntilFinished) {
            mMsgLayout.setVisibility(View.VISIBLE);
            // mMsgThree.setVisibility(View.VISIBLE);
            MMsgCounter.setText(millisUntilFinished / 1000 + " sec");
            // tv_req_sms.setText("We are sending you a SMS code. Please wait for " + +" seconds.");
            // tv_req_sms.setBackgroundColor(getResources().getColor(R.color.appColor39));

        }
    }


    public void setupShoutActionButton(ActionButton mShout, Context mContext) {

        mShout.setType(ActionButton.Type.BIG);

        // To set button color for normal state:
        mShout.setButtonColor(mContext.getResources().getColor(R.color.colorPrimary));
        mShout.setImageSize(24.0f);
        mShout.setSize(60.0f);
        mShout.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_call));
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
        } else {
            // To set button color for pressed state:
            mShout.setButtonColorPressed(mContext.getResources().getColor(R.color.pbGray1));
//        // To set button color ripple:
//        mShout.setButtonColorRipple(mContext.getResources().getColor(R.color.colorPrimaryDark));
//
//        mShout.setRippleEffectEnabled(true);


        }
    }

    void startCounter() {
        //timer started
        countDownTimer.start();
        timerHasStarted = true;
        disablebuttonsView();
    }

    void disablebuttonsView() {
        request_c_cl.setBackgroundDrawable(getResources().getDrawable(R.drawable.gray_square_bottom_hollow_curved));
        tv_req_sms.setBackgroundDrawable(getResources().getDrawable(R.drawable.gray_square_bottom_hollow_curved));
        request_c_cl.setTextColor(getResources().getColor(R.color.pbGray1));
        tv_req_sms.setTextColor(getResources().getColor(R.color.pbGray1));
    }

    void enablebuttonsView() {
        request_c_cl.setBackgroundDrawable(getResources().getDrawable(R.drawable.gen_button_hollow_selector));
        tv_req_sms.setBackgroundDrawable(getResources().getDrawable(R.drawable.gen_button_hollow_selector));
        request_c_cl.setTextColor(getResources().getColor(R.color.pbWhite));
        tv_req_sms.setTextColor(getResources().getColor(R.color.pbWhite));
    }


    private void registerSMSReceiver() {
        if (rec == null) {
            rec = new SMSReceiver();
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction("sms_received");
        registerReceiver(rec, filter);
    }

    private void unregisterReceiver() {
        if (rec != null) {
            unregisterReceiver(rec);
        }
    }

    private SMSReceiver rec;

    private class SMSReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if ("sms_received".equalsIgnoreCase(intent.getAction())) {
                String sms = intent.getStringExtra("message");
                boolean status = intent.getBooleanExtra("status", false);
                receiveSMS(sms, status);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver();
    }
}
