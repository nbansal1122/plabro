package com.plabro.realestate.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.models.auctions.Page_action_form;
import com.plabro.realestate.models.auctions.Section;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class BidForm extends PlabroBaseActivity implements CustomListAdapter.CustomListAdapterInterface {

    private List<Page_action_form> fields = new ArrayList<>();
    private ListView listView;
    private CustomListAdapter<Page_action_form> adapter;

    private void setAdapter() {
        listView = (ListView) findViewById(R.id.listView);
        adapter = new CustomListAdapter<>(this, R.layout.row_bid_form, fields, this);
        listView.setAdapter(adapter);
    }

    @Override
    protected String getTag() {
        return "Bid Participants";
    }


    @Override
    protected int getResourceId() {
        return R.layout.activity_bid_participants;
    }

    @Override
    protected void loadBundleData(Bundle b) {
        fields = (List<Page_action_form>) b.getSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE);
    }


    @Override
    public void findViewById() {
        inflateToolbar();
        setTitle("Your Bid Form");
        setAdapter();
        Analytics.trackScreen(Analytics.Requirements.BidForm);
    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(this).inflate(resourceID, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Page_action_form f = fields.get(position);
        holder.bidName.setText(f.getTitle());
        holder.bidValue.setText(f.getValue());
        return convertView;
    }

    private class ViewHolder {
        private TextView bidName, bidValue;

        ViewHolder(View v) {
            bidName = (TextView) v.findViewById(R.id.tv_bid_info);
            bidValue = (TextView) v.findViewById(R.id.tv_bid_value);
        }
    }
}
