package com.plabro.realestate.models.propFeeds;

import android.provider.BaseColumns;

import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.models.services.Widget;
import com.plabro.realestate.utilities.JSONUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 25/03/16.
 */
@Table(name = "Widgets", id = BaseColumns._ID)
public class WidgetsFeed extends BaseFeed {
    @SerializedName("widgets")
    @Expose
    private List<Widget> widgets = new ArrayList<>();

    public List<Widget> getWidgets() {
        return widgets;
    }

    public void setWidgets(List<Widget> widgets) {
        this.widgets = widgets;
    }

    public static WidgetsFeed parseJson(String json){
        return (WidgetsFeed) JSONUtils.parseJson(json, WidgetsFeed.class);
    }
}
