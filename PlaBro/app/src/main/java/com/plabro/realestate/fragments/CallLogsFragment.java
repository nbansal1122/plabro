package com.plabro.realestate.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.adapter.CallLogAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.Calls.CallLogResponse;
import com.plabro.realestate.models.Calls.CallData;
import com.plabro.realestate.models.propFeeds.LoaderFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;

public class CallLogsFragment extends CardListFragment implements ShoutObserver.PBObserver {

    CallLogAdapter mAdapter;
    boolean addMoreGlobal = false;
    private static Toolbar mToolbar;
    private static SearchBoxCustom search;

    @Override
    public String getTAG() {
        return "CallLogsFragment";
    }

    public static Fragment getInstance(Bundle b) {
        CallLogsFragment f = new CallLogsFragment();
        f.setArguments(b);
        return f;
    }

    public static CallLogsFragment getInstance(Toolbar bar, SearchBoxCustom searchbox) {
        mToolbar = bar;
        search = searchbox;
        return new CallLogsFragment();
    }


    @Override
    protected void findViewById() {
        super.findViewById();
        Analytics.trackScreen(Analytics.OtherScreens.CallLogs);
//        LoaderFeed lf = new LoaderFeed();
//        lf.setType(Constants.NOTIFICATION_TYPE.PROGRESS_MORE);
//        baseFeeds.add(lf);
        mShoutButton.setVisibility(View.GONE);
    }


    @Override
    public RecyclerView.Adapter getAdapter() {
        if (null == mAdapter) {
            mAdapter = new CallLogAdapter(baseFeeds, getActivity(), "CallLogs");
        }
        return mAdapter;
    }

    @Override
    protected void setnoFeedsLayout() {
    }

    @Override
    protected int getViewId() {
        return R.layout.call_logs_fragment;
    }


    @Override
    protected void reloadRequest() {
        showSpinners(addMoreGlobal);
        getData(false);
    }


    @Override
    public void onHideRecycleView(RecyclerView recyclerView, int dx, int dy) {
        super.onHideRecycleView(recyclerView, dx, dy);

    }

    @Override
    public void onShowRecycleView(RecyclerView recyclerView, int dx, int dy) {
        super.onShowRecycleView(recyclerView, dx, dy);
    }

    @Override
    public void onScrollFire(RecyclerView recyclerView, int dx, int dy) {
        super.onScrollFire(recyclerView, dx, dy);
    }


    @Override
    public void getData(final boolean addMore) {
        isLoading = true;
        addMoreGlobal = addMore;
        if (addMoreGlobal && baseFeeds.size() > 0) {
            if (baseFeeds.get(baseFeeds.size() - 1).getType().equalsIgnoreCase(Constants.NOTIFICATION_TYPE.NO_MORE_DATA)) {
                return;
            }
        }

        showSpinners(addMore);
        HashMap<String, String> params = new HashMap<>();
        if (addMore) {
            params.put(Constants.START_IDX, "" + page);
        } else {
            params.put(Constants.START_IDX, "0");
        }

        params = Utility.getInitialParams(PlaBroApi.RT.GET_CALL_LOG, params);

        AppVolley.processRequest(Constants.TASK_CODES.GET_CALL_LOG, CallLogResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                stopSpinners();


                CallLogResponse notificationResponse = (CallLogResponse) response.getResponse();
                if (null != notificationResponse.getOutput_params()) {
                    noFeeds.setVisibility(View.GONE);
                    ArrayList<CallData> tempFeeds = (ArrayList<CallData>) notificationResponse.getOutput_params().getData();

                    if (tempFeeds.size() > 0) {

                        for (CallData u : tempFeeds) {
                            Log.d(TAG, "call Data:" + u.getCallTime() + ", " + u.getDesc() + "," + u.getName());
                        }
                        LoaderFeed lf = new LoaderFeed();
                        if (tempFeeds.size() < Util.getDefaultStartIdx()) {
                            lf.setType(Constants.NOTIFICATION_TYPE.NO_MORE_DATA);
                            lf.setText(Constants.EmtyText.CALLS);
                        } else {
                            lf.setType(Constants.NOTIFICATION_TYPE.PROGRESS_MORE);
                        }

                        if (addMore) {
                            baseFeeds.remove(baseFeeds.size() - 1);//for footer
                        } else {
                            page = 0;
                            baseFeeds.clear();
                        }
                        baseFeeds.addAll(tempFeeds);
                        baseFeeds.add(lf);
                        mAdapter.updateItems(tempFeeds, addMore);
//                        if (!TextUtils.isEmpty(mAdapter.getSearchText())) {
//                            mAdapter.filter(mAdapter.getSearchText());
//                            afterFilter();
//                        }
                        mAdapter.notifyDataSetChanged();
                        page = Util.getStartIndex(page);
                        //page scrolled for next set of data

                    } else {
                        if (addMore && baseFeeds.size() > 0) {
                            LoaderFeed lf = new LoaderFeed();

                            baseFeeds.remove(baseFeeds.size() - 1);//for footer
                            lf.setType(Constants.NOTIFICATION_TYPE.NO_MORE_DATA);
                            lf.setText(Constants.EmtyText.CALLS);
                            baseFeeds.add(lf);
                            mAdapter.notifyDataSetChanged();
                        } else if (!addMore) {
                            baseFeeds.clear();
                            mAdapter.notifyDataSetChanged();
                            noFeeds.setVisibility(View.VISIBLE);
                            return;
                        }
                    }
                }
                showNoFeeds();
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                stopSpinners();
                onApiFailure(response, taskCode);

                try {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 2000ms
                            LoaderFeed lf = new LoaderFeed();
                            lf.setType(Constants.NOTIFICATION_TYPE.NO_MORE);
                            if (addMore) {
                                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                                baseFeeds.add(lf);
                                mAdapter.notifyDataSetChanged();

                            } else {
                                showNoFeeds();
                            }

                        }
                    }, 2000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    protected void showNoFeeds() {
//        super.showNoFeeds();

        if (baseFeeds.size() > 0) {
            noFeeds.setVisibility(View.GONE);
        } else {
            noFeeds.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Profile Action Received");
        if (Constants.ACTION_PROFILE_UPDATE.equals(intent.getAction())) {
            ((PlabroBaseActivity) getActivity()).setTitle("Notifications");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflateToolbar(menu);
    }

    private void inflateToolbar(Menu menu) {
//        mToolbar.inflateMenu(R.menu.menu_chat_list);
        Log.v(TAG, "toolbar inflated");

    }

    public void openSearch() {
        search.clearSearchable(); // clear all the searchable history previously present

        search.revealFromMenuItem(R.id.action_search, getActivity());
        search.setMenuListener(new SearchBoxCustom.MenuListener() {

            @Override
            public void onMenuClick() {
                if (search.isShown()) {
                    search.hideCircularly(getActivity());
                }

            }

        });
        search.setSearchListener(new SearchBoxCustom.SearchListener() {


            @Override
            public void onSearchOpened() {
                // Use this to tint the screen
                if (mAdapter != null) {
                    mAdapter.filter("");
                    afterFilter();
                }

            }

            @Override
            public void onSearchClosed() {
                // Use this to un-tint the screen
                if (mAdapter != null) {
                    mAdapter.filter("");
                    afterFilter();
                }
            }

            @Override
            public void onSearchTermChanged(String searchTerm) {
                // React to the search term changing
                // Called after it has updated results
                if (mAdapter != null) {
                    mAdapter.filter(searchTerm);
                    afterFilter();
                }
            }


            @Override
            public void onSearch(String searchTerm) {

                if (mAdapter != null) {
                    mAdapter.filter(searchTerm);
                    afterFilter();
                }
            }

            @Override
            public void onSearchCleared() {
                if (mAdapter != null) {
                    mAdapter.filter("");
                    afterFilter();
                }
            }

        });

    }

    private void afterFilter() {
        if (mAdapter.getItemCount() > 1) {
            noFeeds.setVisibility(View.GONE);
        } else {
            noFeeds.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onShoutUpdate() {

    }

    @Override
    public void onShoutUpdateGen(String type) {
        Log.d(TAG, "OnShout load more");
        if (type.equalsIgnoreCase(Constants.LOAD_MORE.SHOUT)) {
            if (addMoreGlobal) {
                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                LoaderFeed lf = new LoaderFeed();
                lf.setType(Constants.NOTIFICATION_TYPE.PROGRESS_MORE);
                baseFeeds.add(lf);
                mAdapter.notifyDataSetChanged();
            }
            getData(addMoreGlobal);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShoutObserver.getInstance().addListener(this);
        registerLocalReceiver();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ShoutObserver.getInstance().remove(this);
        unRegsiterLocalReceiver();
    }


}
