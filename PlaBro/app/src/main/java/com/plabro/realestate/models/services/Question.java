package com.plabro.realestate.models.services;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Question implements Serializable {

    @SerializedName("ans_ui_type")
    @Expose
    private String ans_ui_type;
    @SerializedName("q_id")
    @Expose
    private String q_id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("choices")
    @Expose
    private List<String> choices = new ArrayList<String>();
    @SerializedName("hint")
    @Expose
    private String hint;
    @SerializedName("default")
    @Expose
    private String defaultValue;

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getInput_type() {
        return input_type;
    }

    public void setInput_type(String input_type) {
        this.input_type = input_type;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    @SerializedName("input_type")
    @Expose
    private String input_type;

    /**
     * @return The ans_ui_type
     */
    public String getAns_ui_type() {
        return ans_ui_type;
    }

    /**
     * @param ans_ui_type The ans_ui_type
     */
    public void setAns_ui_type(String ans_ui_type) {
        this.ans_ui_type = ans_ui_type;
    }

    /**
     * @return The q_id
     */
    public String getQ_id() {
        return q_id;
    }

    /**
     * @param q_id The q_id
     */
    public void setQ_id(String q_id) {
        this.q_id = q_id;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The choices
     */
    public List<String> getChoices() {
        return choices;
    }

    /**
     * @param choices The choices
     */
    public void setChoices(List<String> choices) {
        this.choices = choices;
    }

}