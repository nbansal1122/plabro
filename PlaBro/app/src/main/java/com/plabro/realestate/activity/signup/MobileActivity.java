package com.plabro.realestate.activity.signup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.plabro.realestate.AppController;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.adapter.CustomSpinnerAdapter;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.countries.Country;
import com.plabro.realestate.models.countries.CountryDataResponse;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utils.PostAsyncManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by nbansal2211 on 24/07/17.
 */

public class MobileActivity extends PlabroBaseActivity implements CustomListAdapter.CustomListAdapterInterface, AdapterView.OnItemSelectedListener {

    private Spinner spinner;
    private CustomSpinnerAdapter<Country> adapter;
    private List<Country> countryList;
    private Country selectedCountry;
    private String mobileNumber;
    private String otpCode = "1234";


    private static final String FROM_TWILIO = "+16193136754";
    private static final String TWILIO_URL = "https://api.twilio.com/2010-04-01/Accounts/AC9419883529a9e1561724a1696dc71521/Messages.json";


    @Override
    protected String getTag() {
        return "MobileActivity";
    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_mobile_number;
    }


    @Override
    public void findViewById() {
        setClickListeners(R.id.btn_continue);
        countryList = CountryDataResponse.getCountryListFromAsset(this);
        spinner = (Spinner) findViewById(R.id.spinner_country);
        adapter = new CustomSpinnerAdapter<>(this, R.layout.row_spinner_country, countryList, this);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        setIndiaAsDefault();
    }

    private void setIndiaAsDefault() {
        int i = 0;
        for (Country c : countryList) {
            if (c.getDialCode().equalsIgnoreCase("+91")) {
                selectedCountry = c;
                spinner.setSelection(i);
                break;
            }
            i++;
        }
    }

    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btn_continue:
                if (isValid()) {
                    sendOtp(selectedCountry.getDialCode(), mobileNumber);
                }
                break;
        }
    }

    private ProgressDialog dialog;

    private void showProgressDialog() {
        if (dialog == null) {
            dialog = new ProgressDialog(this);
        }
        dialog.setMessage("Loading...");
        dialog.show();
    }

    private void hideProgressDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private void sendOtp(String ccode, String mobileNumber) {
        if (!Util.haveNetworkConnection(AppController.getInstance())) {
            showToast(R.string.error_internet);
            return;
        }
        showProgressDialog();
        otpCode = Util.generatePIN();
        ParamObject obj = getParamObject(ccode, mobileNumber, otpCode);
        PostAsyncManager manager = new PostAsyncManager(new PostAsyncManager.AsyncListener() {
            @Override
            public void onPostExecute(boolean result) {
                hideProgressDialog();
                if (result) {
                    startOTPActivity();
                } else {
                    showToast(R.string.error_sending_otp);
                }
            }
        }, obj);
        manager.execute();
    }

    public static ParamObject getParamObject(String ccode, String mobileNumber, String otpCode) {
        HashMap<String, String> map = new HashMap<>();
        map.put("To", ccode + mobileNumber);
        map.put("From", FROM_TWILIO);
        String body = AppController.getInstance().getString(R.string.format_otp_code, otpCode);
        map.put("Body", body);
        ParamObject obj = new ParamObject();
        obj.setUrl(TWILIO_URL);
        obj.addHeader("Authorization", "Basic QUM5NDE5ODgzNTI5YTllMTU2MTcyNGExNjk2ZGM3MTUyMTpmNGIwMmIyYTZjZGU0NmM4OWUxZDFjZjYxYWY1MmQ3OA==");
        obj.setTaskCode(Constants.TASK_CODES.SEND_OTP);
        obj.setParams(map);
        return obj;
    }

    private void startOTPActivity() {
        Bundle b = new Bundle();
        b.putString(Constants.BUNDLE_KEYS.MOBILE, mobileNumber);
        b.putString(Constants.BUNDLE_KEYS.COUNTRY_CODE, selectedCountry.getDialCode());
        b.putString(Constants.BUNDLE_KEYS.OTP, otpCode);
        Intent i = new Intent(this, OTPVerifyActivity.class);
        i.putExtras(b);
        startActivityForResult(i, Constants.REQ_CODE_VERIFY_OTP);
    }

    private boolean isValid() {
        String text = getEditTextString(R.id.et_mobile);
        mobileNumber = text;
        if (text.isEmpty()) {
            showToast(R.string.error_empty_mobile);
            return false;
        } else if (text.length() < 7) {
            showToast(R.string.error_invalid_mobile);
            return false;
        }
        return true;
    }


    @Override
    public void reloadRequest() {

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this).inflate(resourceID, null);
        }
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        Country c = countryList.get(position);
        tv.setText(c.getDisplayString());
        return convertView;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedCountry = countryList.get(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK, data);
            finish();
        }
    }
}
