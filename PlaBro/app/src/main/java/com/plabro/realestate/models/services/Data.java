package com.plabro.realestate.models.services;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Data {

    @SerializedName("services")
    @Expose
    private Services services;
    @SerializedName("urls")
    @Expose
    private Urls urls;

    public PageCount getCall_log() {
        return call_log;
    }

    public void setCall_log(PageCount call_log) {
        this.call_log = call_log;
    }

    @SerializedName("call_log")
    @Expose
    private PageCount call_log;
    @SerializedName("widgets")
    @Expose
    private Widgets widgets;

    public Widgets getWidgets() {
        return widgets;
    }

    public void setWidgets(Widgets widgets) {
        this.widgets = widgets;
    }

    /**
     * @return The services
     */
    public Services getServices() {
        return services;
    }

    /**
     * @param services The services
     */
    public void setServices(Services services) {
        this.services = services;
    }

    /**
     * @return The urls
     */
    public Urls getUrls() {
        return urls;
    }

    /**
     * @param urls The urls
     */
    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    public static class PageCount {
        public double getNum_calls() {
            return num_calls;
        }

        public void setNum_calls(double num_calls) {
            this.num_calls = num_calls;
        }

        @SerializedName("num_calls")
        @Expose
        private double num_calls;


    }

    public static class Widgets{
        @Expose
        private List<Widget> PropFeeds = new ArrayList<>();
        @Expose
        private List<Widget> Search = new ArrayList<>();

        public List<Widget> getPropFeeds() {
            return PropFeeds;
        }

        public void setPropFeeds(List<Widget> propFeeds) {
            PropFeeds = propFeeds;
        }

        public List<Widget> getSearch() {
            return Search;
        }

        public void setSearch(List<Widget> search) {
            Search = search;
        }
    }

}