package com.plabro.realestate.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.MainActivity;

public class ChatHeadService extends Service {

    private static final String TAG = ChatHeadService.class.getSimpleName();
    private static WindowManager windowManager;
    private static ImageView chatHead;
    private Context mContext = this;

    @Override
    public IBinder onBind(Intent intent) {
        // Not used
        return null;
    }


    public static void startChatHeadService(Context context) {
        Log.d(TAG, "starting ChatHeadService");
        Intent intent = new Intent(context, ChatHeadService.class);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        chatHead = new ImageView(this);
        chatHead.setImageResource(R.drawable.ic_launcher);

        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.CENTER | Gravity.LEFT;
        params.x = 0;
        params.y = 100;

        windowManager.addView(chatHead, params);

        chatHead.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                boolean shouldClick = false;
                switch (motionEvent.getAction()&MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        shouldClick = true;
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = motionEvent.getRawX();
                        initialTouchY = motionEvent.getRawY();
                        break;
                    case MotionEvent.ACTION_UP:
                        if (shouldClick) {
                            view.performClick();
                        }
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        shouldClick = false;
                        params.x = initialX + (int) (motionEvent.getRawX() - initialTouchX);
                        params.y = initialY + (int) (motionEvent.getRawY() - initialTouchY);
                        windowManager.updateViewLayout(chatHead, params);
                        break;
                }
                return false;
            }
        });

        chatHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //GA Event Tracker
               // Analytics.trackEvent(mContext, mContext.getResources().getString(R.string.consumption), mContext.getResources().getString(R.string.e_app_launch), mContext.getResources().getString(R.string.s_pop_head_launcher));

                hideAppLauncher();
                Intent dialogIntent = new Intent(mContext, MainActivity.class);
                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(dialogIntent);

            }
        });


//        chatHead.setOnTouchListener(new View.OnTouchListener() {
//            private int initialX;
//            private int initialY;
//            private float initialTouchX;
//            private float initialTouchY;
//
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
////                boolean shouldClick = false;
////                switch (motionEvent.getAction()) {//&MotionEvent.ACTION_MASK
////                    case MotionEvent.ACTION_DOWN:
////                        shouldClick = true;
////                        initialX = params.x;
////                        initialY = params.y;
////                        initialTouchX = motionEvent.getRawX();
////                        initialTouchY = motionEvent.getRawY();
////                        break;
////                    case MotionEvent.ACTION_UP:
////                        if (shouldClick) {
////                            view.performClick();
////                        }
////                        break;
////                    case MotionEvent.ACTION_POINTER_DOWN:
////                        break;
////                    case MotionEvent.ACTION_POINTER_UP:
////                        break;
////                    case MotionEvent.ACTION_MOVE:
////                        shouldClick = false;
////                        params.x = initialX + (int) (motionEvent.getRawX() - initialTouchX);
////                        params.y = initialY + (int) (motionEvent.getRawY() - initialTouchY);
////                        windowManager.updateViewLayout(chatHead, params);
////                        break;
////                }
////                return false;
//
//
//                switch (motionEvent.getAction()) {
//                    case MotionEvent.ACTION_DOWN:
//                        initialX = params.x;
//                        initialY = params.y;
//                        initialTouchX = motionEvent.getRawX();
//                        initialTouchY = motionEvent.getRawY();
//                        return true;
//                    case MotionEvent.ACTION_UP:
//                        //view.performClick();
//                        return true;
//                    case MotionEvent.ACTION_MOVE:
//                        params.x = initialX + (int) (motionEvent.getRawX() - initialTouchX);
//                        params.y = initialY + (int) (motionEvent.getRawY() - initialTouchY);
//                        windowManager.updateViewLayout(chatHead, params);
//                        return true;
//                }
//                return false;
//            }
//        });
//        chatHead.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                hideAppLauncher();
//                Intent dialogIntent = new Intent(mContext, MainActivity.class);
//                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(dialogIntent);
//
//            }
//        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       // if (chatHead != null) windowManager.removeView(chatHead);//todo illegal argument exception check
    }

    public static void hidePlabroIcon() {
        if (chatHead != null) {
            try {
                windowManager.removeView(chatHead);

            } catch (Exception e) {

            }
        }

    }

    void hideAppLauncher() {
        if (chatHead != null) {
            try {
                windowManager.removeView(chatHead);

            } catch (Exception e) {

            }
        }
    }


}