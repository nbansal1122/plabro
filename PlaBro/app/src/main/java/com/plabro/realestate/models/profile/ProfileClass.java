package com.plabro.realestate.models.profile;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.utilities.JSONUtils;
import com.plabro.realestate.utilities.PBPreferences;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by hemantkumar on 08/01/15.
 */
@Table(name = "UserProfile")
public class ProfileClass extends BaseFeed implements Serializable {


    private static final String TAG = ProfileClass.class.getSimpleName();

    @Column(unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    String _id;

    public void setInvites(int invites) {
        this.invites = invites;
    }

    @Column
    String name = "";
    @Column
    String img = "";
    @Column
    String area_radius = "";
    @Column
    String authorid = "";
    @Column
    String phone = "";
    @Column
    String location = "";
    @Column
    String time = "";
    @Column
    String followed = "";
    @Column
    String following = "";
    @Column
    String profile_data = "";
    @Column
    Boolean isFollowedByMe = false;
    @Column
    String website = "";
    @Column
    String email = "";
    @Column
    String address = "";
    @Column
    String business_name = "";
    @Column
    String status_msg = "";
    @Column
    Boolean isSharingEnabled = true;
    @Column
    boolean dev = false;
    @Column
    int invites = 0;
    @Column
    String city_name = "";
    @Column
    String city_id = "";
    @Column
    int rating_flag = 0;
    @Column
    int shout_count;
    @Column
    int isFreeCall = 0;
    @SerializedName("username")
    @Expose
    private String username;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @SerializedName("title")
    @Expose
    private String title;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getIsFreeCall() {
        return isFreeCall;
    }

    public void setIsFreeCall(int isFreeCall) {
        this.isFreeCall = isFreeCall;
    }

    public int getShout_count() {
        return shout_count;
    }

    public void setShout_count(int shout_count) {
        this.shout_count = shout_count;
    }

    @Expose
    private List<Endorsement_profile> endorsement_profile = new ArrayList<Endorsement_profile>();

    public List<Endorsement_profile> getEndorsement_profile() {
        return endorsement_profile;
    }

    public void setEndorsement_profile(List<Endorsement_profile> endorsement_profile) {
        this.endorsement_profile = endorsement_profile;
    }

    public int getRating_flag() {
        return rating_flag;
    }

    public void setRating_flag(int rating_flag) {
        this.rating_flag = rating_flag;
    }

    public List<String> getInvcode() {
        return invcode;
    }

    public void setInvcode(List<String> invcode) {
        this.invcode = invcode;
    }

    public void setIsSharingEnabled(Boolean isSharingEnabled) {
        this.isSharingEnabled = isSharingEnabled;
    }

    List<String> invcode = new ArrayList<>();

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getArea_radius() {
        return area_radius;
    }

    public void setArea_radius(String area_radius) {
        this.area_radius = area_radius;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFollowed() {
        return followed;
    }

    public void setFollowed(String followed) {
        this.followed = followed;
    }

    public String getFollowing() {
        return following;
    }

    public void setFollowing(String following) {
        this.following = following;
    }

    public String getProfile_data() {
        return profile_data;
    }

    public void setProfile_data(String profile_data) {
        this.profile_data = profile_data;
    }

    public Boolean getIsFollowedByMe() {
        return isFollowedByMe;
    }

    public void setIsFollowedByMe(Boolean isFollowedByMe) {
        this.isFollowedByMe = isFollowedByMe;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAuthorid() {
        return authorid;
    }

    public void setAuthorid(String authorid) {
        this.authorid = authorid;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getStatus_msg() {
        return status_msg;
    }

    public void setStatus_msg(String status_msg) {
        this.status_msg = status_msg;
    }

    public Boolean getIsSharingEnabled() {
        return isSharingEnabled;
    }

    public Boolean isFollowedByMe() {
        return isFollowedByMe;
    }

    public boolean isDev() {
        return dev;
    }

    public void setDev(boolean dev) {
        this.dev = dev;
    }

    public int getInvites() {
        return invites;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public int getInvitesCount() {
        int invitesLeft = 0;
        List<Invitee> invitees = new Select().from(Invitee.class).execute();
        if (invitees != null) {
            for (Invitee i : invitees) {
                if (i.isConsumed()) {
                    Log.d(TAG, "Consumed IV Code:" + i.getInviteeCode());
                } else {
                    Log.d(TAG, "UnConsumed IV Code:" + i.getInviteeCode());
                    invitesLeft++;
                }
            }
        }
        return invitesLeft;
    }

    public Boolean isSharingEnabled() {
        return isSharingEnabled;
    }

    public void saveData() {
        try {
            String phone = PBPreferences.getPhone();
            if (null != phone && phone.equals(this.phone)) {
                HashSet<String> invCodeSet = new HashSet<>();
                List<Invitee> savedInvitees = new Select().from(Invitee.class).execute();

                if (invcode != null && invcode.size() > 0) {
                    invCodeSet.addAll(invcode);

                    // Remove All those invitees from database which are not present in incoming invCode List
                    if (savedInvitees != null && savedInvitees.size() > 0) {
                        for (Invitee i : savedInvitees) {
                            if (!invCodeSet.contains(i.getInviteeCode())) {
                                new Delete().from(Invitee.class).where("inviteeCode = '" + i.getInviteeCode() + "'").execute();
                            }
                        }
                    }
                    for (String i : invcode) {
                        // Log.d(TAG, "Invite Code:"+i);
                        Invitee invitee = new Invitee();
                        invitee.setInviteeCode(i);
                        invitee.save();
                    }
                } else {
                    new Delete().from(Invitee.class).execute();
                }
            }
            long i = save();
            Log.d(TAG, "Is Profile Saved:" + i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static BaseFeed parseJson(String json) {
        ProfileClass f = JSONUtils.getGSONBuilder().create().fromJson(json, ProfileClass.class);
        f.setType("broker");
        return f;
    }
}
