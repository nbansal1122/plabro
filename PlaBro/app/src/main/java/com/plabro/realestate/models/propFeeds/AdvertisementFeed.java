package com.plabro.realestate.models.propFeeds;

import android.os.Bundle;
import android.provider.BaseColumns;

import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.utilities.JSONUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by nitin on 22/01/16.
 */
@Table(name = "Advertisement", id = BaseColumns._ID)
public class AdvertisementFeed extends BaseFeed {

    @Expose
    private String action_title;
    @Expose
    private String subtitle;
    @Expose
    private String title;
    @Expose
    private String img;

    private Bundle deepLinkBundle = new Bundle();

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getAction_title() {
        return action_title;
    }

    public void setAction_title(String action_title) {
        this.action_title = action_title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Bundle getDeepLinkBundle() {
        return deepLinkBundle;
    }

    public void setDeepLinkBundle(Bundle deepLinkBundle) {
        this.deepLinkBundle = deepLinkBundle;
    }

    public static AdvertisementFeed parseJson(String jsonString) {
        AdvertisementFeed feed = (AdvertisementFeed) JSONUtils.parseJson(jsonString, AdvertisementFeed.class);
        Log.d("Advertisement Feed", "json string :" + jsonString);
        try {
            JSONObject object = new JSONObject(jsonString);
            JSONArray paramsArray = object.getJSONArray("deeplink_params");
            if (null != paramsArray && paramsArray.length() > 0) {
                for (int i = 0; i < paramsArray.length(); i++) {
                    JSONObject param = paramsArray.getJSONObject(i);
                    Iterator<String> it = param.keys();
                    while (it.hasNext()) {
                        String key = it.next();
                        String value = param.getString(key);
                        feed.getDeepLinkBundle().putString(key, value);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return feed;
    }
}
