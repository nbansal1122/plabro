package com.plabro.realestate.models.location_feeds;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class LocationFeedsOutputParams {

    List<LocationData> data = new ArrayList<LocationData>();

    public List<LocationData> getData() {
        return data;
    }

    public void setData(List<LocationData> data) {
        this.data = data;
    }
}
