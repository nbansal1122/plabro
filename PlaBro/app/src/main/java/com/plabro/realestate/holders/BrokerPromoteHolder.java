package com.plabro.realestate.holders;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.Profile;
import com.plabro.realestate.activity.UserProfileNew;
import com.plabro.realestate.models.profile.BrokerPromoteData;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.DeepLinkNavigationUtil;
import com.plabro.realestate.utilities.ShoutActionUtil;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.HashMap;

/**
 * Created by nitin on 11/02/16.
 */
public class BrokerPromoteHolder extends BaseFeedHolder {
    /*for user layout*/
    private final ImageView mBrokerImage;
    private final TextView mBrokerTitle;
    private final TextView mBrokerName;
    private TextView chatTv, reqTv;


    public BrokerPromoteHolder(View itemView) {
        super(itemView);
         /*for single broker card*/
        this.mBrokerImage = (ImageView) itemView.findViewById(R.id.tv_broker_image);
        this.mBrokerTitle = (TextView) itemView.findViewById(R.id.tv_title);
        this.mBrokerName = (TextView) itemView.findViewById(R.id.tv_broker_name);
        this.chatTv = (TextView) itemView.findViewById(R.id.tv_chat);
        this.reqTv = (TextView) itemView.findViewById(R.id.tv_requirements);
    }


    @Override
    public void onBindViewHolder(final int position, final BaseFeed feed) {
        Log.d("BrokerPromoteCard:", "ON Bind View Holder");
        super.onBindViewHolder(position, feed);
        if (feed instanceof BrokerPromoteData) {
            final BrokerPromoteData userProfile = (BrokerPromoteData) feed;
            String brokerImg = userProfile.getImg();
            if (null != brokerImg && !TextUtils.isEmpty(brokerImg)) {
                Picasso.with(ctx).load(brokerImg).placeholder(R.drawable.profile_default_one).into(mBrokerImage);
            } else {
                mBrokerImage.setImageResource(R.drawable.profile_default_one);
            }

            mBrokerTitle.setText(TextUtils.isEmpty(userProfile.getName()) ? "New Broker" : userProfile.getName());
            mBrokerName.setText(userProfile.getDescription());
            mBrokerName.setSingleLine(false);
            mBrokerImage.setTag(userProfile);
            mBrokerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("ScreenName", getTAG());
                    wrData.put("Action", "Single Broker Image Clicked");
                    trackEvent(Analytics.CardActions.ViewProfile, position, userProfile.get_id(), userProfile.getType(), wrData);

//                    Intent intent1 = new Intent(ctx, UserProfileNew.class);
                    UserProfileNew.startActivity(ctx, userProfile.getAuthorid() + "", "", "");
//                    intent1.putExtra("authorid", userProfile.getAuthorid() + "");
//                    ctx.startActivity(intent1);
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    trackEvent(Analytics.CardActions.CardClicked, position, userProfile.get_id(), userProfile.getType(), null);
                    DeepLinkNavigationUtil.navigate(userProfile.getDeepLinkBundle(), ctx);
                }
            });
            reqTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    trackEvent(Analytics.CardActions.CardClicked, position, userProfile.get_id(), userProfile.getType(), null);
                    DeepLinkNavigationUtil.navigate(userProfile.getDeepLinkBundle(), ctx);
                }
            });

            chatTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    trackEvent(Analytics.CardActions.Congratulate, position, userProfile.get_id(), userProfile.getType(), null);
                    userProfile.setSumm(userProfile.getChat_text());
                    userProfile.setProfile_phone(userProfile.getPhone());
                    Intent chatIntent = ShoutActionUtil.getChatActionIntent(ctx, userProfile, position, userProfile.getPhone(), userProfile.getType(), false);
                    if (chatIntent == null) return;
                    Bundle b = chatIntent.getExtras();
                    if (b != null && b.containsKey(Constants.BUNDLE_KEY_FEED)) {
                        b.remove(Constants.BUNDLE_KEY_FEED);
                    }
                    chatIntent.putExtra(Constants.BUNDLE_KEYS.IS_CHAT_PREFIX_REQUIRED, false);
                    ctx.startActivity(chatIntent);
                }
            });
            setFeedText(userProfile, position);
        } else {
            return;
        }
    }

    private void setFeedText(BrokerPromoteData feed, int position) {
        String text = feed.getDescription();
        int start = 0;
        int end = 250;
        if (feed.getSnippet().size() > 0) {
            start = Integer.parseInt(feed.getSnippet().get(0));
            end = Integer.parseInt(feed.getSnippet().get(1)) + 1;
        }

        int occurance = text.length() - text.replace("\n", "").length();


        if (start >= 0 && end < text.length()) {
            try {
                text = text.substring(start, end);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //adding span for hashtags in post
        SpannableString hashTagText = ActivityUtils.setHashTags(feed, text, start, end, ctx, mBrokerName, getType(), position, false, getRefKey(), mActivity);//type and position send to track analytics

    }
}
