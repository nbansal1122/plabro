package com.plabro.realestate.models.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("RT")
    @Expose
    private String RT;
    @SerializedName("output_params")
    @Expose
    private Output_params output_params;

    /**
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return The RT
     */
    public String getRT() {
        return RT;
    }

    /**
     * @param RT The RT
     */
    public void setRT(String RT) {
        this.RT = RT;
    }

    /**
     * @return The output_params
     */
    public Output_params getOutput_params() {
        return output_params;
    }

    /**
     * @param output_params The output_params
     */
    public void setOutput_params(Output_params output_params) {
        this.output_params = output_params;
    }

    public static ServiceResponse parseJson(String json){
        Gson builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return builder.fromJson(json, ServiceResponse.class);
    }
}