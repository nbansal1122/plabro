package com.plabro.realestate.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.entities.MyDraftsDBModel;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.LocationSelectionListener;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.city.CityClass;
import com.plabro.realestate.models.city.CityResponse;
import com.plabro.realestate.models.login.Login;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.models.shouts.ShoutResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.ListDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Shout extends PlabroBaseActivity implements VolleyListener {

    private EditText etShout;
    MenuItem menu_btn_enable;
    MenuItem menu_btn_disabled;
    private static String postId = "";
    private boolean prefillFromSearchFlag = false;
    private int countListener = 0;
    //private TextView mLocation;
    private HashMap<String, String> globalCityList = new HashMap<String, String>();
    final ArrayList<String> myList = new ArrayList<String>();
    private Boolean isLocationSelected = false;
    private String locationSelected = "";
    private List<CityClass> cityClassList;
    private Toolbar mToolbar;
    private String shoutType = "";
    private LinearLayout mShoutEditLayout;
    private FontText mAvailReq, mSaleRent;
    //    private Spinner spinner;
//    private GenSpinnerAdapter spinnerAdapter;
    private Feeds mFeeds;
    private RoundedImageView user_image;
    private FontText mUserName, mBusinessName, mCurrentCity;
    private ProgressDialog dialog;
    private boolean isCityListFetched;
    private boolean draftSaved = false;
    private MyDraftsDBModel draft;
    private String groupId;

    public static void startActivityToPostInGroup(String groupId, Activity ctx) {
        Intent i = new Intent(ctx, Shout.class);
        Bundle b = new Bundle();
        b.putString(Constants.BUNDLE_KEYS.GROUP_ID, groupId);
        i.putExtras(b);
        ctx.startActivity(i);
    }

    @Override
    protected void loadBundleData(Bundle b) {
        groupId = b.getString(Constants.BUNDLE_KEYS.GROUP_ID);
    }

    @Override
    protected String getTag() {
        return "Shout";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shout);
        Analytics.trackScreen(Analytics.OtherScreens.ComposeShout);

        int val = PBPreferences.getTrialVersionState();
        if (val == 0) {
            Login login = new Select().from(Login.class).executeSingle();
            try {
                if (null != login) {
                    val = login.getTrial_version();
                }
            } catch (Exception e) {

            }
        }
        final Toast toast = Toast.makeText(Shout.this, "", Toast.LENGTH_LONG);
        Log.d(TAG, "Trial Version:" + val);
        inner:
        switch (val) {
            case 0:
                Utility.userLogin(Shout.this);

                //Navigate to previous screen
                toast.setText("Please register to get full access.");
                toast.show();
                finish();

                break inner;
            case 1:
                //Navigate to previous screen
                toast.setText("We are verifying your profile. Please wait till then.");
                toast.show();
                onBackPressed();
                break inner;


        }


        //Crittercism.beginTransaction("Shout");
        //setting action bar
        mToolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(mToolbar);

        // Set an OnMenuItemClickListener to handle menu item clicks
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Handle the menu item
                int id = item.getItemId();

                switch (id) {

                    case R.id.action_send_enabled:
                        postShout();
                        return true;
                }
                return true;
            }

        });

        // Inflate a menu to be displayed in the mToolbar
        mToolbar.inflateMenu(R.menu.menu_shout);

        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //  getSupportActionBar().setDisplayShowTitleEnabled(false);

        etShout = (EditText) findViewById(R.id.shoutText);
        etShout.addTextChangedListener(editTextWatcher);

        mShoutEditLayout = (LinearLayout) findViewById(R.id.ll_edit);

        mAvailReq = (FontText) findViewById(R.id.tv_req_avail);
        mSaleRent = (FontText) findViewById(R.id.tv_sale_rent);
        mUserName = (FontText) findViewById(R.id.profile_name);
        mBusinessName = (FontText) findViewById(R.id.tv_buis_name);
        mCurrentCity = (FontText) findViewById(R.id.ft_current_city);

        mCurrentCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ListDialog listDialog = new ListDialog(myList, Shout.this, locationSelected, "Post your shout in", locationSelectionListener);
//                listDialog.show(getSupportFragmentManager(), "listDialog");
            }
        });


        user_image = (RoundedImageView) findViewById(R.id.profile_image);

        mAvailReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final ArrayList<String> mAvailReq = new ArrayList<String>();
                mAvailReq.add("Requirement");
                mAvailReq.add("Available");

                ListDialog listDialog = new ListDialog(mAvailReq, Shout.this, "", "", mAvailReqListener, false);
                listDialog.show(getSupportFragmentManager(), "listDialog");

            }
        });

        mSaleRent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayList<String> mAvailReq = new ArrayList<String>();
                mAvailReq.add("Sale");
                mAvailReq.add("Rent");
                ListDialog listDialog = new ListDialog(mAvailReq, Shout.this, "", "", mSaleRentListener, false);
                listDialog.show(getSupportFragmentManager(), "listDialog");
            }
        });

        cityClassList = CityClass.getCitiesList(Constants.FILE_SHOUT_CITY_JSON);
        fetchCityList();
        setupProfileDetails();
        setupCityDropDown();
        final ScrollView scrollview = ((ScrollView) findViewById(R.id.scrollView));
        scrollview.post(new Runnable() {
            @Override
            public void run() {
                scrollview.fullScroll(ScrollView.FOCUS_UP);
            }
        });
    }


    private void setupProfileDetails() {


        ProfileClass pc = AppController.getInstance().getUserProfile();
        if (pc != null) {
            String imgUrl = pc.getImg();
            String name = pc.getName();
            String businessName = pc.getBusiness_name();
            if (imgUrl != null) {
                Picasso.with(Shout.this).load(imgUrl).placeholder(R.drawable.profile_default_one).into(user_image);
            }
            mUserName.setText(name);
            mBusinessName.setText(businessName);
        }
    }


    private void prefillCheckFromShouts() {


        String repostText = getIntent().getStringExtra("repostText");
        postId = getIntent().getStringExtra("postId");
        boolean repostFlag = getIntent().getBooleanExtra("repostFlag", false);

        if (repostFlag && !TextUtils.isEmpty(repostText)) {
            etShout.setText(repostText);
            etShout.setSelection(repostText.length());
            setMenuVisibility(true);
        }

    }

    private void prefillCheckFromEdit() {

        setLayout();


    }


    private void prefillCheckFromSearch() {

        String searchText = getIntent().getStringExtra("searchText");
        prefillFromSearchFlag = getIntent().getBooleanExtra("searchFlag", false);

        if (prefillFromSearchFlag && !TextUtils.isEmpty(searchText)) {
            etShout.setText(searchText);
            etShout.setSelection(searchText.length());
            setMenuVisibility(true);
        }

    }

    private void prefillFromHistory() {

        String composedShout = PBPreferences.getComposedShoutText();

        if (!prefillFromSearchFlag) {
            if (!TextUtils.isEmpty(composedShout)) {
                etShout.setText(composedShout);
                etShout.setSelection(composedShout.length());
                setMenuVisibility(true);
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_shout, menu);
        menu_btn_enable = menu.getItem(0);
        menu_btn_disabled = menu.getItem(1);
        prefillCheckFromShouts();
        prefillCheckFromSearch();
        prefillFromHistory();
        prefillCheckFromEdit();

        return true;
    }

    private void setCityFromFeedObject() {
        String city_name = (String) mFeeds.getCity_name();
        if (TextUtils.isEmpty(city_name)) return;
        locationSelected = city_name;
        //mCurrentCity.setText(locationSelected);
        mCurrentCity.setText(Util.toDisplayCase(locationSelected));

    }

    public String getCityIdFromName(String locationSelected) {
        if (TextUtils.isEmpty(locationSelected)) return "";
        String city_id = "";
        for (Map.Entry<String, String> entry : globalCityList.entrySet()) {
            if (entry.getKey().toLowerCase().equalsIgnoreCase(locationSelected.toLowerCase())) {
                city_id = entry.getValue();
                break;
            }
        }
        return city_id;
    }


    private void setLayout() {

        shoutType = getIntent().getStringExtra("shoutType");
        if (TextUtils.isEmpty(shoutType)) {
            shoutType = "post";
        }
        postId = getIntent().getStringExtra("postId");
        Log.d(TAG, "Post ID ::" + postId);

        if (shoutType.equalsIgnoreCase("edit")) {
            mShoutEditLayout.setVisibility(View.VISIBLE);
            setMenuVisibility(true);
            mFeeds = (Feeds) getIntent().getSerializableExtra("feedObject");
            String text = (String) mFeeds.getText();
            if (!TextUtils.isEmpty(text)) {
                etShout.setText(text);
                etShout.setSelection(text.length());
            }
            if (null != mFeeds.getAvail_req() && mFeeds.getAvail_req().length > 0) {
                mAvailReq.setText((String) mFeeds.getAvail_req()[0]);
            }
            if (null != mFeeds.getSale_rent() && mFeeds.getSale_rent().length > 0) {
                mSaleRent.setText((String) mFeeds.getSale_rent()[0]);
            }
            setCityFromFeedObject();

//            int itemposition = spinnerAdapter.getItemPosition(locationSelected);
//            spinner.setSelection(itemposition);

        } else if (shoutType.equalsIgnoreCase("draft")) {
            mShoutEditLayout.setVisibility(View.GONE);
            setMenuVisibility(true);
            draft = (MyDraftsDBModel) getIntent().getSerializableExtra("draftObject");
            String text = (String) draft.getText();
            if (!TextUtils.isEmpty(text)) {
                etShout.setText(text);
                etShout.setSelection(text.length());
            }
            if (null != draft.getAvail_req() && !TextUtils.isEmpty(draft.getAvail_req())) {
                mAvailReq.setText((String) draft.getAvail_req());
            }
            if (null != draft.getSale_rent() && !TextUtils.isEmpty(draft.getSale_rent())) {
                mSaleRent.setText((String) draft.getSale_rent());
            }

            String city_name = (String) draft.getCity_name();
            if (!TextUtils.isEmpty(city_name)) {
                locationSelected = city_name;
                //mCurrentCity.setText(locationSelected);
                mCurrentCity.setText(Util.toDisplayCase(locationSelected));
            }


        } else {

            mShoutEditLayout.setVisibility(View.GONE);

            try {
                locationSelected = AppController.getInstance().getUserProfile().getCity_name();
                mCurrentCity.setText(Util.toDisplayCase(locationSelected));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }


    //post new shout
    void postShout() {

        if (shoutType.equalsIgnoreCase("edit")) {
            Analytics.trackProductionEvent(Analytics.CardActions.Edit, new HashMap<String, Object>());
            postEditShout();
        } else {
            Analytics.trackProductionEvent(Analytics.CardActions.Compose, new HashMap<String, Object>());
            postNewShout();
        }
    }

    void postEditShout() {

        final ProgressDialog progress;
        progress = ProgressDialog.show(Shout.this, "Please wait",
                "Updating your shout...", true);
        progress.setCancelable(true);
        HashMap<String, String> params = new HashMap<String, String>();
        String text = etShout.getText().toString();
        String city_name = locationSelected;
        String city_id = getCityIdFromName(city_name);
        String avail_req = mAvailReq.getText().toString();
        String sale_rent = mSaleRent.getText().toString();


        params.put(PlaBroApi.PLABRO_SHOUT_EDIT_PARAM_MSG, text);

        if (!TextUtils.isEmpty(avail_req)) {
            params.put(PlaBroApi.PLABRO_SHOUT_EDIT_PARAM_AVAIL_REQ, avail_req);
        }

        if (!TextUtils.isEmpty(sale_rent)) {
            params.put(PlaBroApi.PLABRO_SHOUT_EDIT_PARAM_SALE_RENT, sale_rent);
        }

        if (!TextUtils.isEmpty(city_name)) {
            params.put(PlaBroApi.PLABRO_SHOUT_EDIT_PARAM_CITY_NAME, city_name);
        }
        if (!TextUtils.isEmpty(city_id)) {
            params.put(PlaBroApi.PLABRO_SHOUT_EDIT_PARAM_CITY_ID, city_id);
        }

        if (postId != null) {
            params.put(PlaBroApi.PLABRO_SHOUT_EDIT_PARAM_SHOUT_ID, postId);
        }

        params = Utility.getInitialParams(PlaBroApi.RT.EDIT_SHOUT, params);

        String countListenerVal = countListener + "";
        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        wrData.put("Count", countListener);

        final HashMap<String, String> tempParams = params;


        AppVolley.processRequest(Constants.TASK_CODES.EDIT_SHOUT, ShoutResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);
                ShoutResponse shoutResponse = (ShoutResponse) response.getResponse();
                sendScreenName(R.string.goal_SHOUT_POSTED_AFTER_EDIT);
                final Toast toast = Toast.makeText(Shout.this, shoutResponse.getMsg(), Toast.LENGTH_LONG);
                //Navigate to previous screen
                if (TextUtils.isEmpty(shoutResponse.getMsg())) {
                    toast.setText("You just shouted successfully");
                }
                toast.show();
                PBPreferences.setComposedShoutText("");
                etShout.setText("");

                ShoutObserver.dispatchEvent();

                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();


            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);

                onApiFailure(response, taskCode);
                showNoDataDialog(response.getCustomException().getMessage());
                //  int notifyid = PlabroPushNotificationManager.sendLocalPlabroPush(Shout.this, "Posting Shout...", "Sit back and relax while we post your shout",true);
                //  BackgroundProcessHandler.initiateBackgroundEditShout(Shout.this, tempParams, notifyid);

            }
        });


    }

    void postNewShout() {

        final ProgressDialog progress;
        progress = ProgressDialog.show(Shout.this, "Please wait",
                "Processing your request...", true);

        HashMap<String, String> params = new HashMap<String, String>();
        final String shout = etShout.getText().toString();

        JSONObject obj = new JSONObject();
        sendScreenName(R.string.funnel_SHOUT_COMPOSED);

        try {
            obj.put("text", shout);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        String city_name = locationSelected;
//        String city_id = globalCityList.get(locationSelected);
        String city_id = getCityIdFromName(city_name);
        final String message = obj.toString();
        params.put(PlaBroApi.PLABRO_SHOUT_POST_PARAM_MESSAGE, message);
        params.put(PlaBroApi.PLABRO_SHOUT_POST_PARAM_POST_TYPE, "New");
        if(!TextUtils.isEmpty(groupId)){
            JSONArray array = new JSONArray();
            array.put(groupId);
            params.put(Constants.BUNDLE_KEYS.GROUP_IDS, array.toString());
        }
        if (!TextUtils.isEmpty(city_name)) {
            params.put(PlaBroApi.PLABRO_SHOUT_POST_PARAM_CITY_NAME, city_name);
        }
        if (!TextUtils.isEmpty(city_id)) {
            params.put(PlaBroApi.PLABRO_SHOUT_POST_PARAM_CITY_ID, city_id);
        }

        if (postId != null) {
            params.put(PlaBroApi.PLABRO_SHOUT_POST_PARAM_POST_ID, postId);
        }

        params = Utility.getInitialParams(PlaBroApi.RT.POSTSHOUTS, params);

        String countListenerVal = countListener + "";
        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        wrData.put("Count", countListener);

        final HashMap<String, String> tempParams = params;


        AppVolley.processRequest(Constants.TASK_CODES.POST_SHOUTS, ShoutResponse.class, null, PlaBroApi.getBaseUrl(Shout.this), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {

                if (null != progress && progress.isShowing()) {
                    progress.dismiss();
                }
                if (shoutType.equalsIgnoreCase("postSearch")) {
                    PBPreferences.setPostSearchStatus(true);
                }


                ShoutResponse shoutResponse = (ShoutResponse) response.getResponse();
                sendScreenName(R.string.goal_SHOUT_POSTED);

                final Toast toast = Toast.makeText(Shout.this, shoutResponse.getMsg(), Toast.LENGTH_LONG);
                //Navigate to previous screen
                if (TextUtils.isEmpty(shoutResponse.getMsg())) {
                    toast.setText("You just shouted successfully");
                }
                toast.show();
                PBPreferences.setComposedShoutText("");
                etShout.setText("");
                String message_id = shoutResponse.getOutput_params().getMessage_id();
                getShoutDetails(message_id);
                if (shoutType.equalsIgnoreCase("draft")) {
                    new Delete().from(MyDraftsDBModel.class).where("draftId = ?", draft.getDraftId()).execute();
                }
                ShoutObserver.dispatchEvent();
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

                saveDraft(true);

                Utility.dismissProgress(progress);

                onApiFailure(response, taskCode);
                showNoDataDialog(response.getCustomException().getMessage());
                // int notifyid = PlabroPushNotificationManager.sendLocalPlabroPush(Shout.this, "Posting Shout...", "Sit back and relax while we post your shout",true);
                // BackgroundProcessHandler.initiateBackgroundPostShout(Shout.this,tempParams,notifyid);

            }
        });


    }


    TextWatcher editTextWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            draftSaved = false;
            countListener++;
            String postText = etShout.getText().toString();
            if (postText.length() > 0) {
                setMenuVisibility(true);
            } else {
                setMenuVisibility(false);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {


        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    @Override
    public void findViewById() {

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void inflateToolbar() {

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        showSoftKeyboard(etShout);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!shoutType.equalsIgnoreCase("edit")) {
            showSoftKeyboard(etShout);
            String composedShout = PBPreferences.getComposedShoutText();
            etShout.setText(composedShout);
            if (composedShout.length() > 0) {
                etShout.setSelection(composedShout.length());
            }
        } else {
            hideSoftKeyboard();
        }
    }


    void showNoDataDialog(String msg) {
        if (TextUtils.isEmpty(msg)) {
            msg = "Unable to process your request. Please try again.";
        }

        if (isRunning) {
            new AlertDialog.Builder(Shout.this)
                    .setTitle("Server Error!")
                    .setMessage(msg)
                    .setNegativeButton(android.R.string.cancel, null) // dismisses by default
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // do the acknowledged action, beware, this is run on UI thread
//                            postShout();

                        }
                    })
                    .create()
                    .show();
        }

    }


    void setMenuVisibility(boolean status) {
        if (menu_btn_enable != null && menu_btn_disabled != null) {
            if (status) {
                menu_btn_enable.setVisible(true);
                menu_btn_disabled.setVisible(false);
            } else {
                menu_btn_enable.setVisible(false);
                menu_btn_disabled.setVisible(true);

            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        initiateSavingDraft();
    }

    private void initiateSavingDraft() {
        if (shoutType.equalsIgnoreCase("post")) {
            PBPreferences.setComposedShoutText(etShout.getText().toString());
        }

        if (!draftSaved) {
            saveDraft(false);
        }
    }

    private void saveDraft(boolean isShoutFailed) {
        String shoutString = etShout.getText().toString();
        String cityString = mCurrentCity.getText().toString();
        String sale_rent = mSaleRent.getText().toString();
        String avail_req = mAvailReq.getText().toString();

        if (!TextUtils.isEmpty(shoutString)) {
            MyDraftsDBModel myDraftsDBModel = new MyDraftsDBModel();
            if (!TextUtils.isEmpty(avail_req)) {
                myDraftsDBModel.setAvail_req(avail_req);

            } else {
                myDraftsDBModel.setAvail_req("");
            }

            if (!TextUtils.isEmpty(sale_rent)) {
                myDraftsDBModel.setSale_rent(sale_rent);
            } else {
                myDraftsDBModel.setSale_rent("");

            }
            if (!TextUtils.isEmpty(cityString)) {
                myDraftsDBModel.setCity_name(cityString);
                String city_id = globalCityList.get(cityString);
                myDraftsDBModel.setCity_id(city_id);


            } else {
                myDraftsDBModel.setCity_name("");
            }
            myDraftsDBModel.setText(shoutString);
            myDraftsDBModel.setTime(System.currentTimeMillis() + "");
            if (!shoutType.equalsIgnoreCase("draft")) {
                List<MyDraftsDBModel> dbDrafts = new Select().from(MyDraftsDBModel.class).execute();
                myDraftsDBModel.setDraftId(dbDrafts.size());
                myDraftsDBModel.save();
            } else if (shoutType.equalsIgnoreCase("draft")) {

                MyDraftsDBModel myDraftsDBModelOld = new Select().from(MyDraftsDBModel.class)
                        .where("draftId" + " = ?", draft.getDraftId()).executeSingle();
                myDraftsDBModelOld.setAvail_req(myDraftsDBModel.getAvail_req());
                myDraftsDBModelOld.setSale_rent(myDraftsDBModel.getSale_rent());
                myDraftsDBModelOld.setText(myDraftsDBModel.getText());
                myDraftsDBModelOld.setTime(myDraftsDBModel.getTime());
                myDraftsDBModelOld.setCity_name(myDraftsDBModel.getCity_name());
                myDraftsDBModel.setCity_id(myDraftsDBModel.getCity_id());

                if (isShoutFailed) {
                    myDraftsDBModelOld.setSending(true);
                }
                myDraftsDBModelOld.save();
            }

        }
        draftSaved = true;
        ShoutObserver.dispatchEvent();
        if (isShoutFailed) {
            //todo redirect user to drafts:hemant
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        initiateSavingDraft();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.top_in_fast, R.anim.top_out_fast);
    }


    private void setupCityDropDown() {
        Log.d(TAG, "City Size :" + cityClassList.size());
        myList.clear();

        for (CityClass cityClass : cityClassList) {
            String cityName = cityClass.getCity_name();
            myList.add(cityName);
            globalCityList.put(cityName, cityClass.get_id());

            if (cityClass.getCity_type().equalsIgnoreCase("default")) {
                //mLocation.setText(cityName);
                if (!TextUtils.isEmpty(cityName)) {
                    locationSelected = cityName;
                }
            }
        }

        Log.d(TAG, "City Name :" + locationSelected);
        if (null != shoutType && shoutType.equalsIgnoreCase("edit")) {
            setCityFromFeedObject();
        }
        Log.d(TAG, "After City Name :" + locationSelected);
        isLocationSelected = true;
        if (!TextUtils.isEmpty(locationSelected))
            mCurrentCity.setText(Util.toDisplayCase(locationSelected));


       /* View spinnerContainer = LayoutInflater.from(this).inflate(R.layout.toolbar_spinner,
                mToolbar, false);
        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mToolbar.addView(spinnerContainer, lp);

        spinnerAdapter = new GenSpinnerAdapter();
        spinnerAdapter.addItems(myList);

        spinner = (Spinner) spinnerContainer.findViewById(R.id.toolbar_spinner);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                locationSelected = myList.get(i);
                Log.v(TAG, locationSelected);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
*/

    }

    LocationSelectionListener mAvailReqListener = new LocationSelectionListener() {
        @Override
        public void onResponse(Boolean status, String response) {
            //Toast.makeText(Preferences.this, response, Toast.LENGTH_LONG).show();
            if (status) {
                String availReqString = mAvailReq.getText().toString();
                mAvailReq.setText(response);
            }
        }
    };

    LocationSelectionListener mSaleRentListener = new LocationSelectionListener() {
        @Override
        public void onResponse(Boolean status, String response) {
            //Toast.makeText(Preferences.this, response, Toast.LENGTH_LONG).show();
            if (status) {
                String availReqString = mSaleRent.getText().toString();
                if (!response.equalsIgnoreCase(availReqString)) {
                    if (response.equalsIgnoreCase("Sale")) {
                    } else {
                    }
                }
                mSaleRent.setText(response);
            }
        }
    };

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        // TODO Auto-generated method stub
//
//        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
//        if (scanResult != null) {
//           String  contantsString = "";
//            // handle scan result
//            contantsString =  scanResult.getContents()==null?"0":scanResult.getContents();
//            if (contantsString.equalsIgnoreCase("0")) {
//                Toast.makeText(this, "Problem to get the  contant Number", Toast.LENGTH_LONG).show();
//
//            }else {
//                Toast.makeText(this, contantsString, Toast.LENGTH_LONG).show();
//
//            }
//
//        }
//        else{
//            Toast.makeText(this, "Problem to secan the barcode.", Toast.LENGTH_LONG).show();
//        }
//    }

    LocationSelectionListener locationSelectionListener = new LocationSelectionListener() {
        @Override
        public void onResponse(Boolean status, String response) {
            //Toast.makeText(Preferences.this, response, Toast.LENGTH_LONG).show();
            if (status) {
                isLocationSelected = true;
                locationSelected = response;
                mCurrentCity.setText(Util.toDisplayCase(response));
                // mCurrentCity.setText(response);
            } else {
                isLocationSelected = false;
                locationSelected = "";
            }
        }
    };


    private void getShoutDetails(String message_id) {
        ParamObject obj = new ParamObject();
        obj.setClassType(FeedResponse.class);
        obj.setTaskCode(Constants.TASK_CODES.SHOUT_DETAILS);
        HashMap<String, String> params = new HashMap<>();
        params.put(PlaBroApi.PLABRO_FEED_DETAILS_POST_PARAM_MESSAGE_ID, message_id);
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.SHOUT_DETAILS, params));
        showDialog();
        AppVolley.processRequest(obj, this);
    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        hideDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.SHOUT_DETAILS:
                FeedResponse resp = (FeedResponse) response.getResponse();
                if (null != resp.getOutput_params() && null != resp.getOutput_params().getData()) {
                    List<BaseFeed> feeds = resp.getOutput_params().getData();
                    if (feeds.size() > 0) {
                        Feeds feedsObject = (Feeds) feeds.get(0);
                        finish();
                        FeedDetails.startFeedDetails(Shout.this, feedsObject.get_id(), "", feedsObject);
                    }
                }
                break;
            case Constants.TASK_CODES.GET_CITY_DROPDOWN:
                String cityJson = (String) response.getResponse();
                if (!TextUtils.isEmpty(cityJson)) {
                    try {
                        cityClassList = CityResponse.parseJson(cityJson, false);
                        setupCityDropDown();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    fetchCityListFromAssets();

                }

                break;
        }
    }

    private void fetchCityListFromAssets() {
        try {
            cityClassList = CityClass.getCitiesList(Constants.FILE_SHOUT_CITY_JSON);
            setupCityDropDown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        hideDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.SHOUT_DETAILS:
                showFailureMessage(response);
                break;
            case Constants.TASK_CODES.GET_CITY_DROPDOWN:
                fetchCityListFromAssets();
                break;
        }
    }


    private void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private void showFailureMessage(PBResponse response) {
        showToast(response.getCustomException().getMessage());
    }

    private void fetchCityList() {
        ParamObject obj = new ParamObject();
        obj.setTaskCode(Constants.TASK_CODES.GET_CITY_DROPDOWN);
        HashMap<String, String> map = new HashMap<>();
        map.put("cityDropDownFlag", "post_shout");
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.GET_CITY_DROPDOWN, map));
        AppVolley.processRequest(obj, this);
    }


    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }

}
