package com.plabro.realestate.others;

import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.models.profile.ProfileClass;

import java.util.HashMap;


public class Analytics {


    public interface MainActivity {
        public String MainActivity = "MainActivity";
    }

    public interface LeftNav {
        public String LeftNavigationBarViewed = "LeftNavigationBarViewed";
    }

    public interface SERVICES {
        String SMSService = "SMS Service";
        //        Screens of SMS Service
        String LoanService = "Loan Service";
//        Screens of Loan Service
    }


    public interface InvitesScreens {
        String INVITATION_LIST = "Invitation List";
        String INVITE_SENT_SUCCESSFULLY = "Invites Sent Successfully";

    }

    public interface Registration {


        String REGISTRATION = "Registration";
        String Verification = "Verification";
        String TNC = "TNC";
        String PREFERENCES = "Preferences";

    }


    public interface PropFeed {


        // New screens
        String PropFeed = "Prop Feed";
        String BuilderFeed = "Builders Feed";
        String Search = "Search";
        String HashTagView = "HashTag View";
        String Compose = "Compose";
        String RelatedFeeds = "Related Feed";
        String RelatedBrokers = "Related Brokers";
        String CallEndorsement = "Call Endorsement";
        String ShoutDetails = "ShoutDetails";
    }


    public interface Chat {

        // new Screens
        String ConversationView = "Conversation View";
        String ChatFragment = "ChatFragment";
    }

    public interface UserProfile {


        // New Events
        String UserProfile = "User Profile";
        String Recommendations = "Recommendations";
        String EndorseList = "EndorseList";
        String Follower = "Follower";
        String Following = "Following";
        String FollowLocation = "Follow Location";
        String SHoutByOthers = "ShoutByOthers";
        String ProfilePicture = "Profile Picture";


    }


    public interface Contacts {
        public String ContactsScreen = "ContactsScreen";
        public String Refreshed = "Refreshed";
        public String Searched = "Searched";
        public String Click = "Click";

    }

    public interface Wallet {


        // New Screens
        String WalletView = "Wallet View";
        String AddMoney = "Add Money";
    }


    public interface OtherScreens {

        // New Screens
        String Bookmarks = "Bookmarks";
        String MyShouts = "MyShouts";
        String Notifications = "Notifications";
        String CallLogs = "Call Logs";
        String Contacts = "Contacts";
        String Invites = "Invites";
        String WebLogin = "WebLogin";
        String NewsFeed = "News Feed";
        String BlogsFeed = "Blogs Feed";
        String Settings = "Settings";


        String Drafts = "Drafts";
        String ComposeShout = "Shout";
    }

    public interface Notification {
    }

    public interface DirectListing {
        // New Screens
        String AllListings = "All Listings";
        String ListingDetails = "Listing Details";
        String ImageView = "Image View";
        String PurchaseForm = "Purchase Form";
        String PurchasedListings = "Purchased Listings";
    }

    public interface Requirements {
        String AllRequirements = "All Requirements";
        String MyBids = "My Bids";
        String RequirementDetails = "Requirement Details";
        String BidDetails = "Bid Details";
        String BidForm = "Bid Form";
        String SubmitRequirements = "Submit Requirement";
    }

    public static final int GA_DISPATCH_PERIOD = 60;


    public static Tracker getGaTracker() {
        if (tracker == null) {
            // int trackingId = AppController.isDebugMode() ? R.string.ga_test_tracking_id : R.string.ga_tracking_id;
            int trackingId = R.string.ga_tracking_id;
            tracker = GoogleAnalytics.getInstance(AppController.getInstance()).newTracker(AppController.getInstance().getResources().getString(trackingId));
        }
        return tracker;
    }


    private static Context context;
    private static Tracker tracker;

    public static void initialize(Context ctx) {
        context = ctx;
        tracker = getGaTracker();
        CleverTap.initCleverTap(ctx);


    }

    public static void trackScreen(String screenName) {
        Tracker tracker = getGaTracker();
        tracker.setPage("Page");
//        tracker.enableAutoActivityTracking(true);
        tracker.setScreenName(screenName);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
//        Toast.makeText(AppController.getInstance(), screenName, Toast.LENGTH_SHORT).show();
    }

    public static void trackEventWithProperties(int category, int actionEventName, HashMap<String, Object> data) {
//        String actionEvent = context.getResources().getString(actionEventName);
//        String cat = context.getResources().getString(category);
//        trackEventWithProperties(cat, actionEvent, data);
    }

    public static void trackEventWithProperties(String cat, String actionEvent, HashMap<String, Object> data) {
        Log.d("Analytics:", AppController.isDebugMode + ":isDebugMode");
        ProfileClass profile = AppController.getInstance().getUserProfile();
        boolean isDev = false;
        if (null != profile) {
            isDev = profile.isDev();
        }
        isDev = true;
        if (isDev) {


            if (data != null) {
                CleverTap.recordEventWithProperties(actionEvent, data);
                String label = "";

                for (String key : data.keySet()) {

                    label = label + key + ":" + data.get(key) + ",";

                }

                getGaTracker().send(new HitBuilders.EventBuilder()
                        .setCategory(cat)
                        .setAction(actionEvent)
                        .setLabel(label)
                        .build());
            } else {
                CleverTap.recordEvent(actionEvent);
                tracker.send(new HitBuilders.EventBuilder()
                        .setCategory(cat)
                        .setAction(actionEvent)
                        .setLabel("")
                        .build());
            }
        }
    }

    public static void sendRegistrationData(HashMap<String, Object> metaData) {
        trackEventWithProperties((R.string.consumption), R.string.e_registration, metaData);
    }


    /// Actions

    public interface Categories {
        String Social = "Social";

        String Profile = "Profile";
        String Interaction = "Interaction";
        String Consumption = "Consumption";
        String Production = "Production";

        String Service = "Service";
        String Requirement = "Requirement";
        String Wallet = "Wallet";
        String Information = "Information";
    }

    public interface Actions {
        String UnfollowUser = "Unfollow User";
        String FollowUser = "Follow User";

        String Endorsed = "Endorsed";
        String AddedMoney = "Added Money";
        String Purchased = "Purchased";
        String Failed = "Failed";
        String Refreshed = "Refreshed";
        String Invited = "Invited";
        String ServiceClicked = "Service Clicked";
        String ServicePurchased = "ServiceRequested";
        String Submitted = "Submitted";
        String DeleteShout = "Delete Shout";
        String Repost = "Repost";
        String MODIFIED = "Tried To Modify";
        String ImageClicked = "Image";
        String FollowLocation = "Follow Location";
        String UnfollowLocation = "Unfollow Location";
        String Skipped = "Skipped";
        String FILTERS = "Applied Filters";

//
//
//        Edited(for my shout)
//
//
//
//
//
//
//
//
//
//
//
//        Scanned
    }

    public interface CardActions {
        String Called = "Called";
        String Initiated = "Initiated";
        String Chat = "Chat";
        String Shared = "Shared";
        String Copied = "Copied";
        String Bookmarked = "Bookmarked";
        String Expanded = "Expanded";

        String OpenedWeb = "Opened Web";

//        View

        String RelatedFeed = "Related Feed";
        String Collaborated = "Collaborated";
        String Post = "Post";
        String Skip = "Skip";
        String Edit = "Edit";

        String CardClicked = "Card Clicked";
        String UnBookMarked = "Un Bookmarked";
        String ViewProfile = "ViewProfile";
        String Congratulate = "Conratulate";
        String OptOut = "OptOut";
        String BidParticipantsClicked = "Bid Participants";
        String LauncherClicked = "LauncherClicked";
        String Forward = "Forward";
        String Compose = "ComposeShout";
        String HashClicked = "Hash Clicked";
    }

    public interface Params {
        String USER_ID = "userid";
        String SHOUT_ID = "shoutid";
        String CARD_POSITION = "ClickedIndex";
        String FEED_TYPE = "feedType";
        String AMOUNT = "Amount";
        String PayuResponse = "PayuResponse";
        String PHONE = "Phone";
        String QuestionText = "QuestionText";
        String NotificationId = "NotificationId";
        String Location = "location";
        String HASH_TAG = "Hash Tag";
    }

    public static void trackSocialEvent(String eventName, HashMap<String, Object> map) {
        trackEventWithProperties(Categories.Social, eventName, map);
    }

    public static void trackInteractionEvent(String eventName, HashMap<String, Object> map) {
        trackEventWithProperties(Categories.Interaction, eventName, map);
    }

    public static void trackWalletEvent(String eventName, HashMap<String, Object> map) {
        trackEventWithProperties(Categories.Wallet, eventName, map);
    }

    public static void trackServiceEvent(String eventName, HashMap<String, Object> map) {
        trackEventWithProperties(Categories.Service, eventName, map);
    }

    public static void trackProfileEvent(String eventName, HashMap<String, Object> map) {
        trackEventWithProperties(Categories.Profile, eventName, map);
    }

    public static void trackProductionEvent(String eventName, HashMap<String, Object> map) {
        trackEventWithProperties(Categories.Production, eventName, map);
    }

}
