package com.plabro.realestate.uiHelpers.images.zoom;

import android.graphics.Bitmap;

/**
 * Base interface used in the {@link ImageViewTouchBase} view
 * @author alessandro
 *
 */
public interface IBitmapDrawable {

	Bitmap getBitmap();
}
