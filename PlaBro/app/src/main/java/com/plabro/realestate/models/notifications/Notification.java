package com.plabro.realestate.models.notifications;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by hemantkumar on 04/09/15.
 */
@Table(name = "Notification")
public class Notification extends Model {

    public static String STATUS_UNREAD = "unread";
    public static String STATUS_READ = "read";

    @Column(unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    String _id;

    @Column
    String title = "";
    @Column
    String act = "";
    @Column
    String msg = "";
    @Column
    String url = "";
    @Column
    String status = "";
    @Column
    String notifyId = "";
    @Column
    String phone = "";
    @Column
    String type = "";


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAct() {
        return act;
    }

    public void setAct(String act) {
        this.act = act;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotifyId() {
        return notifyId;
    }

    public void setNotifyId(String notifyId) {
        this.notifyId = notifyId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
