package com.plabro.realestate.models.securitykeysharing;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class KeySharingOutputParams {

    KeySharingClass data = new KeySharingClass();

    public KeySharingClass getData() {
        return data;
    }

    public void setData(KeySharingClass data) {
        this.data = data;
    }
}
