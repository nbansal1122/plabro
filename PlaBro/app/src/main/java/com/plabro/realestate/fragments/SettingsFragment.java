package com.plabro.realestate.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import com.activeandroid.query.Select;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.activity.RestClientActivity;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.login.Login;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.profile.ProfileResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.SettingsToggle;
import com.plabro.realestate.uiHelpers.Switch;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.ContributePlabroDialog;
import com.plabro.realestate.widgets.dialogs.DebugPopUpDialog;
import com.plabro.realestate.widgets.dialogs.UserRegistrationDialog;

import java.util.HashMap;

public class SettingsFragment extends MasterFragment {

    private static Activity mActivity;
    private ProgressWheel mProgressWheel;
    private Button mContribute, mCallCustomerCare;
    private String mAuthorId = "";
    Switch mPushSwitch, mChatSwitch, mDebugSwitch;
    public static final String TAG = "Settings";
    private static Context mContext;
    private static Toolbar mToolbar;
    private RelativeLayout mDebugMode;
    private Button restClient;
    private static int SECTION = 6;
    private Boolean debugEnabled = false;

    public static SettingsFragment getInstance(Context mContext, Toolbar mToolbar) {
        Log.i(TAG, "Creating New Instance");
        SettingsFragment mSettingsFragment = new SettingsFragment();
        SettingsFragment.mContext = mContext;
        SettingsFragment.mToolbar = mToolbar;

        return mSettingsFragment;
    }

    private boolean isSettingDebugFlag = false;
    private boolean isShortURL;
    private ProfileClass profileClass;

    @Override
    public String getTAG() {
        return "Settings";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        findViewById();
        setHasOptionsMenu(true);

        return rootView;

    }


    @Override
    protected void findViewById() {
        final Boolean pushEnabled = PBPreferences.getIsPushNotificationEnable();
        final Boolean chatEnabled = PBPreferences.getIsPushNotificationEnable();
        debugEnabled = PBPreferences.getIsDebugEnable();


        mProgressWheel = (ProgressWheel) findView(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mProgressWheel.spin();
        mDebugMode = (RelativeLayout) findView(R.id.rl_debug_mode);
        mContribute = (Button) findView((R.id.contribute));
        mCallCustomerCare = (Button) findView((R.id.customer_care));
        restClient = (Button) findView(R.id.btn_restClient);


        mContribute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager mFragmentManager = getChildFragmentManager();
                DialogFragment feedPopUpDialog = new ContributePlabroDialog(getActivity());
                feedPopUpDialog.show(mFragmentManager, "Contribute");
            }
        });

        mCallCustomerCare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.callPhone(getActivity(), Constants.PLABRO_CUSTOMER_CARE);

            }
        });


        mPushSwitch = (Switch) findView(R.id.switch_push);
        mChatSwitch = (Switch) findView(R.id.switch_chat);
        mDebugSwitch = (Switch) findView(R.id.switch_release_mode);


        mPushSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // Utility.userStats(AppController.home, "notificationon");
                    if (! PBPreferences.getIsPushNotificationEnable()) {
                    }
                    PBPreferences.setIsPushNotificationEnable(true);
                } else {
                    // Utility.userStats(AppController.home, "notificationoff");
                    if ( PBPreferences.getIsPushNotificationEnable()) {
                    }
                    PBPreferences.setIsPushNotificationEnable(false);

                }
            }
        });


        mChatSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //  Utility.userStats(AppController.home, "chaton");
                    if (!PBPreferences.getIsChatNotificationEnable()) {
                    }
                    PBPreferences.setIsChatNotificationEnable(true);

                } else {
                    // Utility.userStats(AppController.home, "chatoff");
                    if (PBPreferences.getIsChatNotificationEnable()) {
                    }
                    PBPreferences.setIsChatNotificationEnable(false);

                }
            }
        });


        mPushSwitch.setChecked(pushEnabled);
        mChatSwitch.setChecked(chatEnabled);
        isSettingDebugFlag = true;
        mDebugSwitch.setOnCheckedChangeListener(null);
        mDebugSwitch.setChecked(debugEnabled);
        setClickListeners(restClient);
        mDebugSwitch.setOnCheckedChangeListener(debugSwitchListener);
        isSettingDebugFlag = false;
        setHasOptionsMenu(true);
        setupProfile();


        SettingsToggle mAutoDetectSwitch = (SettingsToggle) findView(R.id.switch_auto_detect);

        //TODO : API Check
        mAutoDetectSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                } else {
                }
            }
        });
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_settings;
    }

    @Override
    protected void setViewSizes() {

    }

    @Override
    protected void callScreenDataRequest() {

    }

    @Override
    protected void reloadRequest() {

    }


    void getUserProfile() {
        ProfileClass userProfile = AppController.getInstance().getUserProfile();
        boolean fetchingProfileFirstTime = true;
        if (userProfile != null) {
            setUserData(userProfile);
            fetchingProfileFirstTime = false;
        }

        // At this place we are just trying to refresh the profile in background
        // If internet connection is not available it will show dialog only if profile is not cached
        if (!Util.haveNetworkConnection(getActivity()) && fetchingProfileFirstTime) {
            mProgressWheel.stopSpinning();
            return;
        }

        HashMap<String, String> params = new HashMap<String, String>();
        params = Utility.getInitialParams(PlaBroApi.RT.GETPROFILE, params);
        AppVolley.processRequest(1, ProfileResponse.class, null, String.format(PlaBroApi.getBaseUrl(mContext)), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                mProgressWheel.stopSpinning();
                ProfileResponse profileResponse = (ProfileResponse) response.getResponse();
                ProfileClass profileClass = profileResponse.getOutput_params().getData();
                profileClass.saveData();
                AppController.getInstance().setUserProfile(profileClass);
                setUserData(profileClass);
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                if (null == getActivity()) return;
                mProgressWheel.stopSpinning();

                onApiFailure(response, taskCode);
            }
        });


    }

    @Override
    public void onStatusFalse(PBResponse response, int taskCode) {
        super.onStatusFalse(response, taskCode);
        //todo if  profileResponse.getMsg().equalsIgnoreCase("Missing AuthKey in the request") call userlogin again
        Utility.userLogin(getActivity());

    }

    @Override
    public void onTrialVersionExpired(PBResponse response, int taskCode) {
        super.onTrialVersionExpired(response, taskCode);
        showRegistrationCard();
    }

    @Override
    public void onSessionExpiry(PBResponse response, int taskCode) {
        super.onSessionExpiry(response, taskCode);
        Utility.userLogin(getActivity());
    }

    protected void showRegistrationCard() {
        int val = PBPreferences.getTrialVersionState();
        if (val == 0) {

            Login login = new Select().from(Login.class).executeSingle();
            if (null != login) {
                val = login.getTrial_version();
            }
        }
        switch (val) {
            case 0:
                Utility.userLogin(getActivity());

                if (!UserRegistrationDialog.isVisible) {
                    UserRegistrationDialog feedPopUpDialog = new UserRegistrationDialog(getActivity(), val);
                    feedPopUpDialog.show(getChildFragmentManager(), "UserRegister");
                }
                break;
            case 1:
                if (!UserRegistrationDialog.isVisible) {
                    UserRegistrationDialog feedPopUpDialog1 = new UserRegistrationDialog(getActivity(), val);
                    feedPopUpDialog1.show(getChildFragmentManager(), "UserRegister");
                }
                break;
        }
    }

    void setupProfile() {

        mProgressWheel.stopSpinning();

        ProfileClass profileClass = AppController.getInstance().getUserProfile();
        if (profileClass != null) {
            setUserData(profileClass);
        } else {
            getUserProfile();
        }
    }

    void setUserData(ProfileClass profileClass) {
        if (profileClass != null) { //check if fragment is attached while updating UI
            mAuthorId = profileClass.getAuthorid();
            PBPreferences.setNotAuthorId(mAuthorId);
            String imgUrl = profileClass.getImg();
            PBPreferences.setUserImg(imgUrl);
            toggleSharing(profileClass.getIsSharingEnabled());
            if (profileClass.isDev()) {
                showViews(mDebugMode, restClient);
            } else {
                hideViews(mDebugMode, restClient);
            }

        }
    }

    private void toggleSharing(boolean status) {
        mContribute.setEnabled(status);
        if (status) {
            mContribute.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        } else {
            mContribute.setBackgroundColor(getResources().getColor(R.color.appColor11));
        }
    }

    CompoundButton.OnCheckedChangeListener debugSwitchListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            if (isChecked && !isSettingDebugFlag) {
                if (!PBPreferences.getIsDebugEnable()) {
                }
                PBPreferences.setIsDebugEnable(true);
                FragmentManager mFragmentManager = getFragmentManager();
                DebugPopUpDialog debugPopUP = new DebugPopUpDialog(mContext);
                debugPopUP.show(mFragmentManager, "debug");

            } else {
                if (PBPreferences.getIsDebugEnable()) {
                }

                PBPreferences.setIsDebugEnable(false);

            }
//            startActivity(new Intent(getActivity(), RestClientActivity.class));
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerLocalReceiver();
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Profile Action Received");
        if (Constants.ACTION_PROFILE_UPDATE.equals(intent.getAction())) {
            ((PlabroBaseActivity) getActivity()).setTitle("Settings");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegsiterLocalReceiver();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_restClient:
                startNextActivity(RestClientActivity.class);
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Analytics.trackScreen(Analytics.OtherScreens.Settings);
    }
}

