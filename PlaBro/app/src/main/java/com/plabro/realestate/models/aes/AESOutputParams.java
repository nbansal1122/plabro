package com.plabro.realestate.models.aes;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class AESOutputParams {

    AESClass data = new AESClass();

    public AESClass getData() {
        return data;
    }

    public void setData(AESClass data) {
        this.data = data;
    }
}
