package com.plabro.realestate.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.payu.india.Payu.PayuConstants;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.activity.RechargeWallet;
import com.plabro.realestate.adapter.TransactionsCustomAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.payment.PaymentResponse;
import com.plabro.realestate.models.payment.Transaction;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class PBWalletFragment extends MasterFragment implements View.OnClickListener {

    RelativeLayout noFeeds;
    TransactionsCustomAdapter mAdapter;
    RecyclerView mRecyclerView;
    private Toolbar mToolbar;
    public static final String TAG = "PBWalletFragment";
    private Context mContext;
    private Button mAddMoney;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private int page = 0;
    private boolean isLoading = false;
    private ArrayList<Feeds> feeds2 = new ArrayList<Feeds>();
    LinearLayoutManager mLayoutManager;
    FontText mWalletBalance;
    private LinearLayout mTransactionLayout;
    ProgressWheel mWalletProgress, mProgressWheel;
    private String bal = "0.0";
    private List<String> timeList = new ArrayList<>();
    private List<Transaction> transactionList = new ArrayList<>();

    public static PBWalletFragment newInstance(Context mContext, Activity mActivity) {
        PBWalletFragment mFeedsFragment = new PBWalletFragment();
        mFeedsFragment.mContext = mContext;
        return mFeedsFragment;
    }

    public static PBWalletFragment getInstance(Context mContext, Toolbar mToolbar, SearchBoxCustom searchbox) {
        Log.i(TAG, "Creating New Instance");
        PBWalletFragment mShoutsFragment = new PBWalletFragment();
        mShoutsFragment.mContext = mContext;
        mShoutsFragment.mToolbar = mToolbar;
        return mShoutsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getViewId() {
        return R.layout.wallet_frag;
    }


    @Override
    public String getTAG() {
        return "PBWalletFragment";
    }

    @Override
    protected void findViewById() {
        setTitle("Plabro Wallet");
        Analytics.trackScreen(Analytics.Wallet.WalletView);
        mAddMoney = (Button) findView(R.id.btn_add_money);
        mAddMoney.setOnClickListener(this);

        page = 0;
        isLoading = false;

        rootView.setTag(TAG);

        mWalletBalance = (FontText) findView(R.id.tv_balance);
        mWalletProgress = (ProgressWheel) findView(R.id.pw_balance);
        mWalletProgress.setBarColor(getResources().getColor(R.color.colorPrimary));
        mWalletProgress.setProgress(0.0f);

        mProgressWheel = (ProgressWheel) findView(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mWalletBalance.setText(String.format(getActivity().getResources().getString(R.string.wallet_balance), bal));

        mTransactionLayout = (LinearLayout) findView(R.id.ll_transactions);


        noFeeds = (RelativeLayout) findView(R.id.noTransaction);
        noFeeds.setVisibility(View.GONE);

//        } else {
//            ((ViewGroup) rootView.getParent()).removeView(rootView);
//        }


        //pull to refresh
        mSwipeRefreshLayout = (SwipeRefreshLayout) findView(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        page = 0;
                        getWalletBalance();
                    }
                }, Constants.GOOGLE_PULL_TO_REFRESH_TIMER);

            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTransViews();
            }
        });
        getData(false);
        getWalletBalance();

    }

    private void resetTransViews() {
        int childCount = mTransactionLayout.getChildCount();

        //show selected transaction details
        for (int cc = 0; cc < childCount; cc++) {
            mTransactionLayout.getChildAt(cc).findViewById(R.id.rl_trans).setVisibility(View.VISIBLE);
            mTransactionLayout.getChildAt(cc).findViewById(R.id.trans_selected).setVisibility(View.GONE);
        }
    }


    private void getWalletBalance() {

        //mProgressWheel.spin();
        mWalletProgress.spin();
        mWalletBalance.setVisibility(View.GONE);

        HashMap<String, String> params = new HashMap<String, String>();
        if (!Util.haveNetworkConnection(getActivity())) {
            showNoDataDialog(false);
            return;
        }

        params.put(PlaBroApi.PLABRO_RECHARGE_WALLET_AUTO_PARAM_ACTION, PlaBroApi.WALLET_ACTIONS.STATUS);
        params.put(PlaBroApi.PLABRO_RECHARGE_WALLET_AUTO_PARAM_STATE, "start");

        params = Utility.getInitialParams(PlaBroApi.RT.PAYMENT, params);
        AppVolley.processRequest(Constants.TASK_CODES.PAYMENT
                , PaymentResponse.class, null, String.format(PlaBroApi.getPaymentURL()), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                if (null == getActivity()) return;
                mWalletProgress.stopSpinning();
                mProgressWheel.stopSpinning();
                mSwipeRefreshLayout.setRefreshing(false);

                mWalletBalance.setVisibility(View.VISIBLE);
                PaymentResponse paymentResponse = (PaymentResponse) response.getResponse();
                if (paymentResponse != null && null != paymentResponse.getOutput_params() && null != paymentResponse.getOutput_params().getData()) {
                    bal = paymentResponse.getOutput_params().getData().getBalance() + "";
                    mWalletBalance.setText(String.format(getActivity().getResources().getString(R.string.wallet_balance), bal));
                    mWalletBalance.setVisibility(View.VISIBLE);
                    setTransactions(paymentResponse);
                    mAddMoney.setEnabled(true);
                } else {
                    showToast("Oops some unknown error occurred");
                }

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                if (null == getActivity()) return;
                mWalletProgress.stopSpinning();
                mSwipeRefreshLayout.setRefreshing(false);
                mWalletBalance.setVisibility(View.VISIBLE);
                mWalletBalance.setText(String.format(getActivity().getResources().getString(R.string.wallet_balance), bal));
                onApiFailure(response, taskCode);
                showNoFeeds();
                mProgressWheel.stopSpinning();
                mAddMoney.setEnabled(false);
            }

        });
    }

    private void setTransactions(PaymentResponse response) {
        mTransactionLayout.removeAllViews();
        timeList.clear();
        transactionList = response.getOutput_params().getData().getTransactions();
        if (transactionList != null && transactionList.size() > 0) {

            for (Transaction tr : transactionList) {
                if (tr != null)
                    mTransactionLayout.addView(getTransactionView(tr));
            }
        } else {
            showNoFeeds();
        }
    }

    private View getTransactionView(Transaction transaction) {
        final View row = LayoutInflater.from(getActivity()).inflate(R.layout.transactions_layout, null);
        row.findViewById(R.id.rl_trans).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resetTransViews();

                row.findViewById(R.id.rl_trans).setVisibility(View.GONE);
                row.findViewById(R.id.trans_selected).setVisibility(View.VISIBLE);

            }
        });

        row.findViewById(R.id.trans_selected).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resetTransViews();

                row.findViewById(R.id.rl_trans).setVisibility(View.VISIBLE);
                row.findViewById(R.id.trans_selected).setVisibility(View.GONE);

            }
        });

        //selected

        TextView sel_desc = (TextView) row.findViewById(R.id.sel_tv_desc);
        TextView sel_amt = (TextView) row.findViewById(R.id.sel_tv_amount);
        TextView sel_type = (TextView) row.findViewById(R.id.sel_tv_type);
        TextView sel_date = (TextView) row.findViewById(R.id.sel_tv_date);
        TextView sel_trans_id = (TextView) row.findViewById(R.id.sel_tv_trans_id);

        sel_desc.setText(transaction.getInfo());
        sel_amt.setText(String.format(getActivity().getResources().getString(R.string.wallet_balance), transaction.getAmount() + ""));
        sel_type.setText(getTransactionType(transaction.getAction()));
        sel_date.setText(Util.getDisplayFormatDate(transaction.getTime()));
        sel_trans_id.setText(String.format(getActivity().getResources().getString(R.string.trans_id), transaction.getTrans_id()));

        //non selected
        TextView reasonTV = (TextView) row.findViewById(R.id.tv_desc_summ);
        TextView typeTV = (TextView) row.findViewById(R.id.tv_trans_type);
        TextView amountTV = (TextView) row.findViewById(R.id.tv_desc_amount);
        TextView date = (TextView) row.findViewById(R.id.tv_date);
        LinearLayout dateLayout = (LinearLayout) row.findViewById(R.id.ll_date);
        String tranDate = Util.getDateWithoutTime(Util.getDateFromString(transaction.getTime()));
        if (timeList.contains(tranDate)) {
            dateLayout.setVisibility(View.GONE);
        } else {
            date.setText(Util.getDisplayFormatDate(transaction.getTime()));
            dateLayout.setVisibility(View.VISIBLE);
            if (timeList.size() > 0) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(0, 100, 0, 0);
                dateLayout.setLayoutParams(params);
            }

        }
        timeList.add(tranDate);
        Log.d(TAG, tranDate);


        reasonTV.setText(transaction.getInfo());


        if (transaction.getAction().equalsIgnoreCase("credit")) {
            typeTV.setText("CR");
            int green = getActivity().getResources().getColor(R.color.pbGreenTwo);
            amountTV.setTextColor(green);
            typeTV.setTextColor(green);
            sel_amt.setTextColor(green);
            sel_type.setTextColor(green);

        } else if (transaction.getType().equalsIgnoreCase("debit")) {
            typeTV.setText("DB");
            int red = getActivity().getResources().getColor(R.color.pbRedTwo);
            amountTV.setTextColor(red);
            typeTV.setTextColor(red);
            sel_amt.setTextColor(red);
            sel_type.setTextColor(red);
        }

        amountTV.setText(String.format(getActivity().getResources().getString(R.string.wallet_balance), transaction.getAmount() + ""));
        return row;
    }

    private String getTransactionType(String action) {
        switch (action) {
            case "debit":
                return "DB";
            default:
                return "CR";
        }
    }


    private void showNoFeeds() {
        if (transactionList.size() > 0) {
            noFeeds.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            noFeeds.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStatusFalse(PBResponse response, int taskCode) {
        super.onStatusFalse(response, taskCode);
        showNoFeeds();
    }

    @Override
    public void onTrialVersionExpired(PBResponse response, int taskCode) {
        super.onTrialVersionExpired(response, taskCode);
        showNoFeeds();

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflateToolbar(menu);

    }

    private void inflateToolbar(Menu menu) {
        mToolbar.inflateMenu(R.menu.menu_wallet);
        Log.v(TAG, "toolbar inflated");

        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                return true;
            }

        });
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Profile Action Received");
        if (Constants.ACTION_PROFILE_UPDATE.equals(intent.getAction())) {
            ((PlabroBaseActivity) getActivity()).setTitle("Plabro Wallet");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_money:
                Intent rechargeWallet = new Intent(getActivity(), RechargeWallet.class);
                rechargeWallet.putExtra(Constants.BUNDLE_KEYS.BALANCE, bal);
                startActivityForResult(rechargeWallet, PayuConstants.PAYU_REQUEST_CODE);

                break;
        }
    }

    void showNoDataDialog(final Boolean addMore) {

        stopSpinners();

    }


    protected void stopSpinners() {
        isLoading = false;
        try {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 2000ms
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }, 2000);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) return;
        if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
            getWalletBalance();
        }
    }
}
