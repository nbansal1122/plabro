package com.plabro.realestate.widgets.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.plabro.realestate.Featured;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.Registration;
import com.plabro.realestate.activity.Verification;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.uiHelpers.LinkMovementMethodOverride;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;

/**
 * Created by Hemant on 19-01-2015.
 */
public class ConfirmationDialog extends android.support.v4.app.DialogFragment {

    Button mNeg, mPos;
    String neg, pos, title, text;
    FontText mTitle, mText;
    Activity mActivity;

    @SuppressLint("ValidFragment")
    public ConfirmationDialog(Activity activity, String neg, String pos, String title, String text) {
        this.neg = neg;
        this.pos = pos;
        this.text = text;
        this.title = title;
        this.mActivity = activity;
    }

    public ConfirmationDialog() {

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.confirmation_dialog, null);
        builder.setView(convertView);
        setCancelable(true);
        mNeg = (Button) convertView.findViewById(R.id.neg);
        mPos = (Button) convertView.findViewById(R.id.pos);
        mTitle = (FontText) convertView.findViewById(R.id.title);
        mText = (FontText) convertView.findViewById(R.id.tv_text);
        setUpFields();

        return builder.create();
    }


    private void setUpFields() {

        mNeg.setText(neg);
        mPos.setText(pos);
        mTitle.setText(title);
        setFieldText();

//        if (text.contains("Terms")) {
//            setFieldText();
//        } else {
//            mText.setText(text);
//        }

        mNeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mPos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                Intent intent = new Intent(mActivity, Verification.class);
                startActivity(intent);
            }
        });

    }

    private void setFieldText() {

        String tncString = getResources().getString(R.string.tnc_agree_msg);
        tncString = text + " " + tncString;
        final SpannableString hashTagText = new SpannableString(tncString);

        final int indexStart = tncString.indexOf("Terms of Use");
        if (indexStart > 0) {
            final int indexEnd = indexStart + 11;
            mText.setOnTouchListener((new LinkMovementMethodOverride()));


            hashTagText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), indexStart, indexEnd + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            final ClickableSpan hashClick =
                    new ClickableSpan() {
                        @Override
                        public void onClick(View v) {
                            Featured.openWeb(mActivity, Constants.PLABRO_APP_TNC, "Terms and Conditions");
                        }

                    };


            hashTagText.setSpan(hashClick, indexStart, indexEnd + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        }
        mText.setText(hashTagText, TextView.BufferType.SPANNABLE);

    }
}
