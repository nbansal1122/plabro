package com.plabro.realestate.holders.notification;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.activity.FeedDetails;
import com.plabro.realestate.activity.Redirect;
import com.plabro.realestate.activity.UserProfileNew;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.holders.BaseNotificationHolder;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.follow_unfollow.FollowUnFollowResponse;
import com.plabro.realestate.models.notifications.NotificationAction;
import com.plabro.realestate.models.notifications.NotificationData;
import com.plabro.realestate.models.notifications.Params;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.uiHelpers.view.FontButton;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.DeepLinkNavigationUtil;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.RelativeTimeTextView;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by hemantkumar on 17/08/15.
 */
public class NotificationsHolder extends BaseNotificationHolder implements View.OnClickListener, VolleyListener {
    private NotificationData feedObject;
    private FontText tv_profile_name, descTV;
    private RelativeTimeTextView timeTV;
    private ImageView profileFooterIcon, callIcon, commentIcon, followIcon, deleteIcon;
    private LinearLayout actionLayout;
    private FontButton referNow;
    private int position;
    private static HashMap<String, String> contactsMap;
    private Params params;
    RoundedImageView profileImage;
    private ProgressDialog dialog;
    private boolean isRead;
    private static HashSet<Integer> readNotifSet = new HashSet<>();


    public NotificationsHolder(View itemView) {
        super(itemView);
        tv_profile_name = findTV(R.id.tv_name);
        descTV = findTV(R.id.tv_desc);
        timeTV = (RelativeTimeTextView) findView(R.id.tv_time);
        profileImage = (RoundedImageView) findImage(R.id.profile_image);
        profileFooterIcon = findImage(R.id.iv_profileFooterIcon);
        callIcon = findImage(R.id.iv_call);
        commentIcon = findImage(R.id.iv_chat);
        followIcon = findImage(R.id.iv_follow);
        deleteIcon = findImage(R.id.iv_cross);
        actionLayout = (LinearLayout) findView(R.id.ll_action_buttons);
        referNow = (FontButton) findView(R.id.btn_refer);
        setClickListeners(callIcon, commentIcon, followIcon, referNow, profileImage, deleteIcon);
        if (contactsMap == null) {
            contactsMap = PBPreferences.loadMap("contactsMap");
        }

    }

    private ImageView findImage(int id) {
        return (ImageView) itemView.findViewById(id);
    }

    private View findView(int id) {
        return itemView.findViewById(id);
    }

    private FontText findTV(int id) {
        return (FontText) findView(id);
    }


    @Override
    public void onBindViewHolder(final int position, final NotificationData notification) {
        super.onBindViewHolder(position, notification);
        if (notification instanceof NotificationData) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Analytics.Params.NotificationId, notification.get_id());
                    map.put(Analytics.Params.FEED_TYPE, notification.getType());
                    Analytics.trackEventWithProperties(Analytics.Categories.Interaction, Analytics.CardActions.CardClicked, map);
                    onCardClick();
                }
            });
            NotificationsHolder holder = this;
            final NotificationData feedEnabled = (NotificationData) notification;
            feedObject = (NotificationData) notification;
            this.position = position;
            this.params = feedObject.getParams();
            setActionLayout(feedObject.getType());
            isRead = feedObject.getRead() == null ? false : feedObject.getRead();
            if (readNotifSet.contains(feedObject.get_id()) || isRead) {
                itemView.setBackgroundColor(ContextCompat.getColor(ctx, R.color.color_payment_bg));
            } else {
                itemView.setBackgroundColor(ContextCompat.getColor(ctx, R.color.white));
            }

        } else {
            return;
        }
    }

    private void onCardClick() {
        String type = feedObject.getType();
        switch (type) {
            case Constants.NotifType.FOLLOW_USER:
                openUserProfile();
                break;
            case Constants.NotifType.POST_SHOUT_FOLLOWER:
                openFeedDetails();
                break;
            case Constants.NotifType.FRIEND_JOINED:
                openUserProfile();
                break;
            case Constants.NotifType.SET_BOOKMARK:
                openUserProfile();
//                openFeedDetails();
                break;
            case Constants.NotifType.IN_APP:
                break;
            case Constants.NotifType.USER_SIGNATURE:
                openFeedDetails();
                break;
            case Constants.NotifType.ENDORSEMENT:
                openUserProfile();
                break;
            case Constants.NotifType.DEEP_LINK:
                openDeepLink();
                break;
        }
        if (!isRead)
            sendNotificationAction(Constants.NotifActionTypes.READ);
        readNotification();

    }

    private void openDeepLink() {
        String deepLinkUri = params.getDeepLink();
        Bundle bundle = DeepLinkNavigationUtil.getMap(deepLinkUri);
        DeepLinkNavigationUtil.navigate(bundle, ctx);
    }

    private void openUserProfile() {
        Intent i = new Intent(ctx, UserProfileNew.class);
        Bundle b = new Bundle();
        b.putString("authorid", params.getUserid() + "");
        b.putString("phone", params.getPhone());
        i.putExtras(b);
        ctx.startActivity(i);
    }

    private void openFeedDetails() {
        getShoutDetails((feedObject.getParams().getMessage_id() == null ? feedObject.getParams().getShout() : feedObject.getParams().getMessage_id()) + "");
    }

    private void setClickListeners(View... views) {
        for (View v : views) {
            v.setOnClickListener(this);
        }
    }

    private void onCallIconClicked() {
        onCallClicked(params.getPhone());
    }

    private void onCallClicked(String phoneNumber) {
        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        wrData.put("position", position);
        Log.d(TAG, "Phone Number is :" + phoneNumber);
        if (!TextUtils.isEmpty(phoneNumber)) {

            ActivityUtils.callPhone(ctx, phoneNumber, wrData);
        } else {
            Toast.makeText(ctx,
                    "Invalid Phone", Toast.LENGTH_SHORT).show();
        }
    }

    private void onCommentIconClicked() {
        XMPPChat.startChat(ctx, params.getPhone(), params.getName(), params.getUserid() + "");
    }

    private void onFollowIconClicked() {
        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        boolean isFollowedByMe = params.isReverse_follow();
        if (TextUtils.isEmpty(params.getUserid() + "")) {
            wrData.put("UserId", params.getUserid());
        }
        if (isFollowedByMe) {
            wrData.put("Action", "Follow");
        } else {
            wrData.put("Action", "Un Follow");
        }
        follow_user(isFollowedByMe);
        setFollowIcon(!isFollowedByMe);
    }

    private void follow_user(boolean isFollowedByMe) {

        HashMap<String, String> params = new HashMap<String, String>();

        params.put(PlaBroApi.PLABRO_FOLLOW_UNFOLLOW_POST_PARAM_CONTACT_ID, this.params.getUserid() + "");

        int taskCode = 1;
        if (!isFollowedByMe) {
            taskCode = Constants.TASK_CODES.FOLLOW;
            params = Utility.getInitialParams(PlaBroApi.RT.FOLLOW, params);
        } else {
            taskCode = Constants.TASK_CODES.UNFOLLOW;
            params = Utility.getInitialParams(PlaBroApi.RT.UN_FOLLOW, params);
        }


        AppVolley.processRequest(taskCode, FollowUnFollowResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, this);
    }

    private void setActionLayout(String type) {
        setProfileImage();
        setTime();
        hideViews(referNow);
        showViews(callIcon, commentIcon, descTV, followIcon, profileFooterIcon);
        setFollowIcon(params.isReverse_follow());
        switch (type) {
            case Constants.NotifType.FOLLOW_USER:
                setProfileFooterIcon(R.drawable.notif_follow);
                hideViews(descTV);
                setTitle(params.getTitle());
                break;
            case Constants.NotifType.ENDORSEMENT:
                setProfileFooterIcon(R.drawable.notif_follow);
                setTitle(params.getTitle());
                hideViews(descTV);
                break;
            case Constants.NotifType.POST_SHOUT_FOLLOWER:
                setProfileFooterIcon(R.drawable.notif_shout);
                setTitle(params.getTitle());
                setDesc(params.getShort());
                break;
            case Constants.NotifType.FRIEND_JOINED:
                setProfileFooterIcon(R.drawable.notif_follow);
                String name = null;
                String title = "";
                if (null != contactsMap && contactsMap.containsKey(params.getName())) {
                    name = contactsMap.get(params.getName());
                    title = name + " " + params.getTitle();
                } else {
                    title = params.getName();
                }
                if (name == null) {
                    name = params.getName();
                }
                setTitle(title);
                String desc = "Your contact " + name + " joined PlaBro";
                setDesc(desc);
                break;
            case Constants.NotifType.SET_BOOKMARK:
                setProfileFooterIcon(R.drawable.notif_bookmark);
                setTitle(params.getTitle());
                setDesc(params.getShort());
                break;
            case Constants.NotifType.IN_APP:
                hideViews(profileFooterIcon);
                showViews(referNow);
                setTitle(params.getText());
                setDesc(params.getShort_desc());
                break;
            case Constants.NotifType.USER_SIGNATURE:
                setProfileFooterIcon(R.drawable.notif_shout);
                setTitle(params.getTitle());
                setDesc(params.getShort());
                break;
            case Constants.NotifType.DEEP_LINK:
                hideViews(profileFooterIcon);
                setTitle(params.getTitle());
                setDesc(params.getMessage());
                break;
        }
        hideViews(callIcon, referNow, commentIcon, followIcon);
        if (type == Constants.NotifType.SET_BOOKMARK) {
            showViews(commentIcon);
        }
    }

    private void setProfileImage(int defIcon) {
        Log.d(TAG, "Loading Image :" + params.getImg() + ", For Position:" + position + ", title :" + params.getTitle());
        if (TextUtils.isEmpty(params.getImg())) {
            profileImage.setImageResource(defIcon);
        } else {
            ActivityUtils.loadImage(ctx, params.getImg(), profileImage, defIcon);
        }
    }

    private void setProfileImage() {
        setProfileImage(R.drawable.profile_default_one);
    }

    private void setTitle(String text) {
        if (!TextUtils.isEmpty(text))
            tv_profile_name.setText(Html.fromHtml(text));
    }

    private void setDesc(String text) {
        if (!TextUtils.isEmpty(text))
            descTV.setText(Html.fromHtml(text));
    }

    private void setTime() {
        long time = (long) (feedObject.getTime() * 1000);
        timeTV.setReferenceTime(time);
    }

    private void hideViews(View... views) {
        for (View v : views) {
            v.setVisibility(View.GONE);
        }
    }

    private void showViews(View... views) {
        for (View v : views) {
            v.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_call:
                onCallIconClicked();
                break;
            case R.id.iv_chat:
                onCommentIconClicked();
                break;
            case R.id.iv_follow:
                onFollowIconClicked();
                break;
            case R.id.iv_cross:
                deleteNotification();
                break;
        }
    }

    private void deleteNotification() {
        sendNotificationAction(Constants.NotifActionTypes.DELETE);
        Intent i = new Intent();
        i.setAction(Constants.ACTION_DELETE_NOTIFICATION);
        i.putExtra(Constants.BUNDLE_KEYS.POSITION, this.position);
        ctx.sendBroadcast(i);
    }

    private void readNotification() {
        readNotifSet.add(feedObject.get_id());
        itemView.setBackgroundColor(ContextCompat.getColor(ctx, R.color.color_payment_bg));
    }

    private void getShoutDetails(String message_id) {
        Redirect.redirectToShoutDetail(ctx, message_id, "");
//        ParamObject obj = new ParamObject();
//        obj.setClassType(FeedResponse.class);
//        obj.setTaskCode(Constants.TASK_CODES.SHOUT_DETAILS);
//        HashMap<String, String> params = new HashMap<>();
//        params.put("message_id", message_id);
//        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.SHOUT_DETAILS, params));
//        showDialog();
//        AppVolley.processRequest(obj, this);
    }


    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        hideDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.SHOUT_DETAILS:
                FeedResponse resp = (FeedResponse) response.getResponse();
                if (null != resp.getOutput_params() && null != resp.getOutput_params().getData()) {
                    List<BaseFeed> feeds = resp.getOutput_params().getData();
                    if (feeds.size() > 0) {
                        Feeds feedsObject = (Feeds) feeds.get(0);
                        FeedDetails.startFeedDetails(ctx, feedsObject.get_id(), "", feedsObject);
                    }
                }
                break;
            case Constants.TASK_CODES.FOLLOW:
                showToast("Followed Successfully");
                params.setReverse_follow(!params.isReverse_follow());
                break;
            case Constants.TASK_CODES.UNFOLLOW:
                params.setReverse_follow(!params.isReverse_follow());
                break;
        }
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        hideDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.SHOUT_DETAILS:
                showFailureMessage(response);
                break;
            case Constants.TASK_CODES.FOLLOW:
                setFollowIcon(false);
                showFailureMessage(response);
                break;
            case Constants.TASK_CODES.UNFOLLOW:
                setFollowIcon(true);
                showFailureMessage(response);
                break;
        }
    }

    private void setFollowIcon(boolean isFollowedByMe) {
        if (isFollowedByMe) {
            followIcon.setImageResource(R.drawable.notif_following);
        } else {
            followIcon.setImageResource(R.drawable.notif_follow_btn);
        }
    }

    private void showToast(String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
    }

    private void showFailureMessage(PBResponse response) {
        showToast(response.getCustomException().getMessage());
    }


    private void showDialog() {
        if (dialog == null) {
            dialog = new ProgressDialog(ctx);
            dialog.setMessage("Loading...");
            dialog.show();
        } else if (!dialog.isShowing()) {
            dialog = new ProgressDialog(ctx);
            dialog.setMessage("Loading...");
            dialog.show();
        }
    }

    private void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private void setProfileFooterIcon(int iconId) {
        profileFooterIcon.setImageResource(iconId);
    }

    private void sendNotificationAction(String actionType) {
        NotificationAction action = new NotificationAction();
        action.setActionType(actionType);
        action.addNotificationId(feedObject.get_id() + "");
        action.saveData();
        PlabroIntentService.senfNotificationActions(ctx);
    }
}
