package com.plabro.realestate.listeners;

import android.support.v7.widget.RecyclerView;

public abstract class HidingScrollListener extends RecyclerView.OnScrollListener {
  private static  int HIDE_THRESHOLD = 1000;
  private int scrolledDistance = 0;
  private boolean controlsVisible = true;


  public HidingScrollListener(int threshold) {
    this.HIDE_THRESHOLD = threshold;
  }

  public void setHidingScrollListener(int threshold) {
    this.HIDE_THRESHOLD = threshold;
  }

  @Override
  public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
    super.onScrolled(recyclerView, dx, dy);
   
    if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {
      onHide(recyclerView,dx,dy);
      controlsVisible = false;
      scrolledDistance = 0;
    } else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible) {
      onShow(recyclerView,dx,dy);
      controlsVisible = true;
      scrolledDistance = 0;
    }
    else
    {
      onScrolledFire(recyclerView,dx,dy);

    }
    
    if((controlsVisible && dy>0) || (!controlsVisible && dy<0)) {
      scrolledDistance += dy;
    }
  }
  
  public abstract void onHide(RecyclerView recyclerView, int dx, int dy);
  public abstract void onShow(RecyclerView recyclerView, int dx, int dy);
  public abstract void onScrolledFire(RecyclerView recyclerView, int dx, int dy);

}