package com.plabro.realestate.models.payment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentData {

    private boolean status;
    private String msg;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    private String transactionId;
    @Expose
    @SerializedName("postData")
    private PaymentFinalData data = new PaymentFinalData();

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public PaymentFinalData getData() {
        return data;
    }

    public void setData(PaymentFinalData data) {
        this.data = data;
    }
}