package com.plabro.realestate.fragments.payu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.payu.india.Interfaces.DeleteCardApiListener;
import com.payu.india.Interfaces.GetStoredCardApiListener;
import com.payu.india.Model.MerchantWebService;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PayuResponse;
import com.payu.india.Model.PostData;
import com.payu.india.Model.StoredCard;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.india.PostParams.MerchantWebServicePostParams;
import com.payu.india.PostParams.PaymentPostParams;
import com.payu.india.Tasks.DeleteCardTask;
import com.payu.india.Tasks.GetStoredCardTask;
import com.payu.payuui.PaymentsActivity;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.fragments.PaymentDialogFragment;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.utilities.Constants;

import java.util.ArrayList;

/**
 * Created by nitin on 18/11/15.
 */
public class StoredCardFragment extends PlabroBaseFragment implements GetStoredCardApiListener, DeleteCardApiListener {
    private LinearLayout storedCards;

    private static final int SELECTED_DRAWABLE = R.drawable.ic_invitation_selected;
    private static final int UNSELECTED_DRAWABLE = R.drawable.ic_invitation_unselected;
    private Bundle bundle;
    private ArrayList<StoredCard> storedCardList;
    private StoredCard selectedStoreCard;
    private View selectedRow;

    private PayuHashes payuHashes;
    private PaymentParams mPaymentParams;
    private Toolbar toolbar;
    private TextView amountTextView;
    private TextView transactionIdTextView;

    private PayuConfig payuConfig;

    @Override
    public String getTAG() {
        return "Stored Card";
    }

    @Override
    protected void findViewById() {
        storedCards = (LinearLayout) findView(R.id.ll_storedCards);
        bundle = getArguments();


        if (bundle != null && bundle.getParcelableArrayList(PayuConstants.STORED_CARD) != null) {
            storedCardList = new ArrayList<StoredCard>();
            storedCardList = bundle.getParcelableArrayList(PayuConstants.STORED_CARD);
        } else {
            // we gotta fetch data from server
            Toast.makeText(getActivity(), "Could not get user card list from the previous activity", Toast.LENGTH_LONG).show();
        }

        payuHashes = bundle.getParcelable(PayuConstants.PAYU_HASHES);
        mPaymentParams = bundle.getParcelable(PayuConstants.PAYMENT_PARAMS);
        payuConfig = bundle.getParcelable(PayuConstants.PAYU_CONFIG);
        payuConfig = null != payuConfig ? payuConfig : new PayuConfig();
        addCardsToLayout();
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_stored_cards;
    }

    private void makePayment(StoredCard storedCard, String cvv) {
        PostData postData = new PostData();
        // lets try to get the post params
        postData = null;
        storedCard.setCvv(cvv); // make sure that you set the cvv also
        mPaymentParams.setHash(payuHashes.getPaymentHash()); // make sure that you set payment hash
        mPaymentParams.setCardToken(storedCard.getCardToken());
        mPaymentParams.setCvv(cvv);
        mPaymentParams.setNameOnCard(storedCard.getNameOnCard());
        mPaymentParams.setCardName(storedCard.getCardName());
        mPaymentParams.setExpiryMonth(storedCard.getExpiryMonth());
        mPaymentParams.setExpiryYear(storedCard.getExpiryYear());

        postData = new PaymentPostParams(mPaymentParams, PayuConstants.CC).getPaymentPostParams();

        if (postData.getCode() == PayuErrors.NO_ERROR) {
            payuConfig.setData(postData.getResult());
            Intent intent = new Intent(getActivity(), PaymentsActivity.class);
            intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
            String successUrl = mPaymentParams.getSurl();
            String failUrl = mPaymentParams.getFurl();
            intent.putExtra(Constants.BUNDLE_KEYS.S_URL, successUrl);
            intent.putExtra(Constants.BUNDLE_KEYS.F_URL, failUrl);
            startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
        } else {
            Toast.makeText(getActivity(), postData.getResult(), Toast.LENGTH_SHORT).show();
        }

    }

    public void doPayment() {
        if (selectedStoreCard == null) {
            showToast("Please select a card to proceed");
        } else {
            final EditText cvvET = (EditText) selectedRow.findViewById(R.id.et_cardCVV);
            String cvv = cvvET.getText().toString();
            makePayment(selectedStoreCard, cvv);
        }
    }

    private void deleteCard(StoredCard storedCard) {
        MerchantWebService merchantWebService = new MerchantWebService();
        merchantWebService.setKey(mPaymentParams.getKey());
        merchantWebService.setCommand(PayuConstants.DELETE_USER_CARD);
        merchantWebService.setVar1(mPaymentParams.getUserCredentials());
        Log.d(TAG, "DeleteCardHash :" + payuHashes.getDeleteCardHash());
        Log.d(TAG, "Card Token :" + storedCard.getCardToken());
        merchantWebService.setVar2(storedCard.getCardToken());
        merchantWebService.setHash(payuHashes.getDeleteCardHash());

        PostData postData = null;
        postData = new MerchantWebServicePostParams(merchantWebService).getMerchantWebServicePostParams();

        if (postData.getCode() == PayuErrors.NO_ERROR) {
            // ok we got the post params, let make an api call to payu to fetch
            // the payment related details
            payuConfig.setData(postData.getResult());
            payuConfig.setEnvironment(payuConfig.getEnvironment());

            DeleteCardTask deleteCardTask = new DeleteCardTask(this);
            deleteCardTask.execute(payuConfig);
        } else {
            Toast.makeText(getActivity(), postData.getResult(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDeleteCardApiResponse(PayuResponse payuResponse) {
        if (payuResponse.isResponseAvailable()) {
            Toast.makeText(getActivity(), payuResponse.getResponseStatus().getResult(), Toast.LENGTH_LONG).show();
        }
        if (payuResponse.getResponseStatus().getCode() == PayuErrors.NO_ERROR) {
            // there is no error, lets fetch te cards list.

            MerchantWebService merchantWebService = new MerchantWebService();
            merchantWebService.setKey(mPaymentParams.getKey());
            merchantWebService.setCommand(PayuConstants.GET_USER_CARDS);
            merchantWebService.setVar1(mPaymentParams.getUserCredentials());
            merchantWebService.setHash(payuHashes.getStoredCardsHash());

            PostData postData = new MerchantWebServicePostParams(merchantWebService).getMerchantWebServicePostParams();

            if (postData.getCode() == PayuErrors.NO_ERROR) {
                // ok we got the post params, let make an api call to payu to fetch the payment related details

                payuConfig.setData(postData.getResult());
                payuConfig.setEnvironment(payuConfig.getEnvironment());

                GetStoredCardTask getStoredCardTask = new GetStoredCardTask(this);
                getStoredCardTask.execute(payuConfig);
            } else {
                Toast.makeText(getActivity(), postData.getResult(), Toast.LENGTH_LONG).show();
            }

        }
    }


    @Override
    public void onGetStoredCardApiResponse(PayuResponse payuResponse) {
        Toast.makeText(getActivity(), payuResponse.getResponseStatus().getResult(), Toast.LENGTH_LONG).show();
        storedCardList = payuResponse.getStoredCards() == null ? new ArrayList<StoredCard>() : payuResponse.getStoredCards();
        addCardsToLayout();
    }


    private void addCardsToLayout() {
        storedCards.removeAllViewsInLayout();
        for (StoredCard c : storedCardList) {
            View row = getStoreCardView(c);
            storedCards.addView(row);
        }
    }

    private View getStoreCardView(final StoredCard storedCard) {
        final View row = LayoutInflater.from(getActivity()).inflate(R.layout.layout_saved_card, null);
        setText(row, R.id.tv_cardNumber, storedCard.getMaskedCardNumber());
        setText(row, R.id.tv_cardName, storedCard.getNameOnCard());
        setText(row, R.id.tv_cardType, storedCard.getCardType());
        final EditText cvvET = (EditText) row.findViewById(R.id.et_cardCVV);
        cvvET.setText(storedCard.getCvv());
        row.findViewById(R.id.tv_cardDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDeleteCardDialog(storedCard);
            }
        });
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedRow != null) {
                    selectRow(selectedRow, false);
                }
                selectRow(row, true);
                selectedStoreCard = storedCard;
                selectedRow = view;
            }
        });
        return row;
    }

    private void showDeleteCardDialog(final StoredCard storedCard) {
        PaymentDialogFragment f = PaymentDialogFragment.getInstance(getString(R.string.are_you_sure), getString(R.string.msg_delete_card), getString(R.string.card_delete), getString(R.string.card_cancel), true, new PaymentDialogFragment.DialogListener() {
            @Override
            public void onOKClicked() {
                deleteCard(storedCard);
            }

            @Override
            public void onCancelClicked() {

            }
        });
        f.show(getChildFragmentManager(), "delete");
    }

    private void selectRow(View row, boolean isSelected) {
        ImageView iv = (ImageView) row.findViewById(R.id.iv_mode_select);
        if (isSelected) {
            iv.setImageResource(SELECTED_DRAWABLE);
        } else {
            iv.setImageResource(UNSELECTED_DRAWABLE);
        }

    }

    private void setText(View v, int textViewID, String text) {
        ((TextView) v.findViewById(textViewID)).setText(text);
    }
}
