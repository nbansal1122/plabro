package com.plabro.realestate.listeners;

import com.plabro.realestate.models.chat.ChatMessage;

/**
 * Created by Hemant on 23-01-2015.
 */
public interface OnChatMessageListener {

    public void  onSelected(Boolean response, String type, ChatMessage msg);


}
