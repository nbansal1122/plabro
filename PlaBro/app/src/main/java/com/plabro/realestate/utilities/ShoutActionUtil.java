package com.plabro.realestate.utilities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.plabro.realestate.R;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.others.Analytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 11/02/16.
 */
public class ShoutActionUtil {
    public static void chatAction(Context ctx, Feeds feedObject, int position, String phoneNumber, String TAG, boolean isSerializableRequired) {
        Intent i = getChatActionIntent(ctx, feedObject, position, phoneNumber, TAG, isSerializableRequired);
        if (i != null)
            ctx.startActivity(i);
    }

    public static Intent getChatActionIntent(Context ctx, Feeds feedObject, int position, String phoneNumber, String TAG, boolean isSerializableRequired) {
        if (null != feedObject && !TextUtils.isEmpty(feedObject.getProfile_phone())) {

        } else {
            return null;
        }

        String authorid = feedObject.getAuthorid() + "";
        List<String> userPhones = new ArrayList<String>();
        userPhones.add(phoneNumber);
        long profileId = feedObject.getAuthorid();

        Utility.userStats(ctx, "im");

        HashMap<String, Object> wrData = new HashMap<String, Object>();
        wrData.put("imTo", phoneNumber);
        wrData.put("imFrom", PBPreferences.getPhone());
        wrData.put(Analytics.Params.FEED_TYPE, feedObject.getType());
        wrData.put("ScreenName", feedObject.getType());
        wrData.put("Action", "IM");
        wrData.put("ClickedIndex", "" + position);

        Analytics.trackInteractionEvent(Analytics.CardActions.Chat, wrData);


        Intent intent = new Intent(ctx, XMPPChat.class);
        String num = userPhones.get(0);
        String displayName = ActivityUtils.getDisplayNameFromPhone(num);
        if (displayName != null && !TextUtils.isEmpty(displayName)) {
            intent.putExtra("name", displayName);

        } else {
            intent.putExtra("name", feedObject.getProfile_name());

        }
        intent.putExtra("img", feedObject.getProfile_img());
        intent.putExtra("last_seen", feedObject.getTime());

        intent.putExtra("phone_key", num);
        intent.putExtra("userid", profileId + "");
        intent.putExtra("feed_text", feedObject.getSumm()); //summary of hashtags

        Bundle b = new Bundle();
        if (isSerializableRequired) {
            b.putSerializable(Constants.BUNDLE_KEY_FEED, feedObject);
        }
        intent.putExtras(b);
        return intent;
    }

    public static void callAction(Context ctx, Feeds feeds, String phone, int position) {
        boolean status = Util.checkIfStringIsPhone(phone);

        Utility.userStats(ctx, "call");


        HashMap<String, Object> wrData = new HashMap<String, Object>();

        wrData.put("ScreenName", "BiddingFeed");
        wrData.put("callFrom", PBPreferences.getPhone());
        wrData.put(Analytics.Params.FEED_TYPE, feeds.getType());
        wrData.put("ClickedIndex", "" + position);

        ActivityUtils.initiateCallDialog(ctx, feeds, feeds.getPhonemap(), feeds.getProfile_phone(), wrData, feeds.getIsFreeCall(), feeds.getProfile_img(), feeds.getProfile_name());
    }
}
