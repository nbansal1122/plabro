
package com.plabro.realestate.adapter;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.plabro.realestate.R;
import com.plabro.realestate.holders.BaseNotificationHolder;
import com.plabro.realestate.holders.ProgressNotificationHolder;
import com.plabro.realestate.holders.notification.NotificationsHolder;
import com.plabro.realestate.models.notifications.NotificationData;
import com.plabro.realestate.models.notifications.Params;
import com.plabro.realestate.utilities.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class NotificationsCustomAdapter extends RecyclerView.Adapter<BaseNotificationHolder> {

    public static final int SHOUT_NOTIFICATION = 1;

    public static final int PROGRESS_MORE = -2;
    public static final int NO_MORE = -3;
    public static final int NO_MORE_DATA = -4;


    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    private String searchText = "";


    private Context mContext;
    private Activity mActivity;
    private int feedsCount = 0;
    private static final String TAG = "FeedsCustomAdapter";
    private List<NotificationData> notifications = new ArrayList<NotificationData>();
    private List<NotificationData> arrayList = new ArrayList<NotificationData>();
    private List<NotificationData> selectedFeedsObject = new ArrayList<NotificationData>();
    private String type = "";
    private String refKey;

    public class ViewHolder extends BaseNotificationHolder {


        public ViewHolder(View v) {
            super(v);
        }

    }


    public NotificationsCustomAdapter(List<NotificationData> notifications, Activity mActivity, String type) {
        this.notifications = notifications;
        this.type = type;
        this.mContext = mActivity;
        this.mActivity = mActivity;
        feedsCount = notifications.size();
        this.type = type;
        arrayList.addAll(notifications);
    }


    @Override
    public BaseNotificationHolder onCreateViewHolder(ViewGroup viewGroup, final int viewType) {
        View v = null;
        NotificationsHolder nh = null;
        switch (viewType) {


            case PROGRESS_MORE:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.loader_layout, viewGroup, false);
                ProgressNotificationHolder ph = new ProgressNotificationHolder(v);
                ph.setType(Constants.NOTIFICATION_TYPE.PROGRESS_MORE);
                return ph;
            case NO_MORE:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.loader_layout, viewGroup, false);
                ProgressNotificationHolder phn = new ProgressNotificationHolder(v);
                phn.setType(Constants.NOTIFICATION_TYPE.NO_MORE);
                return phn;
            case NO_MORE_DATA:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.loader_layout, viewGroup, false);
                ProgressNotificationHolder phnd = new ProgressNotificationHolder(v);
                phnd.setType(Constants.NOTIFICATION_TYPE.NO_MORE_DATA);
                return phnd;

            default:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.new_post_notification_layout, viewGroup, false);
                nh = new NotificationsHolder(v);
                return nh;

        }

    }


    @Override
    public int getItemViewType(int position) {

        int viewType = SHOUT_NOTIFICATION;

        switch (notifications.get(position).getType()) {
            default:
                viewType = SHOUT_NOTIFICATION;
                break;

            case Constants.NOTIFICATION_TYPE.PROGRESS_MORE:
                viewType = PROGRESS_MORE;
                break;
            case Constants.NOTIFICATION_TYPE.NO_MORE:
                viewType = NO_MORE;
                break;
            case Constants.NOTIFICATION_TYPE.NO_MORE_DATA:
                viewType = NO_MORE_DATA;
                break;

        }

        return viewType;
    }

    @Override
    public void onBindViewHolder(final BaseNotificationHolder holder, final int position) {

        int viewType = getItemViewType(position);
        NotificationsCustomAdapter.ViewHolder viewHolder = null;

        if (holder instanceof NotificationsCustomAdapter.ViewHolder) {
            viewHolder = (ViewHolder) holder;
        }
        FragmentManager mFragmentManager = getCurrentActivity().getFragmentManager();

        switch (viewType) {
            case SHOUT_NOTIFICATION:
                holder.setmFragmentManager(mFragmentManager);
                holder.onBindViewHolder(position, notifications.get(position));
                break;
            case PROGRESS_MORE:
                holder.onBindViewHolder(position, notifications.get(position));
                break;
            case NO_MORE:
                holder.onBindViewHolder(position, notifications.get(position));
            case NO_MORE_DATA:
                holder.onBindViewHolder(position, notifications.get(position));
                break;
            default:
                holder.setmFragmentManager(mFragmentManager);
                holder.onBindViewHolder(position, notifications.get(position));
                break;

        }
    }


    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public void addItems(ArrayList<NotificationData> notifications) {
        for (NotificationData notification : notifications) {
            addItem(feedsCount, notification);
        }

    }

    public void addItem(int position, NotificationData notification) {
        if (position <= notifications.size()) {
            notifications.add(position, notification);
            notifyItemInserted(position);
            feedsCount++;
        }
    }

    public void removeItem(int position) {
        notifications.remove(position);
        notifyItemRemoved(position);
    }


    private Activity getCurrentActivity() {
        return mActivity;
    }

    private Context getCurrentContext() {
        return mContext;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        searchText = charText;
//        arrayList.clear();
//        arrayList.addAll(notifications);
        notifications.clear();

        if (arrayList.size() > 0) {
            if (charText.length() == 0) {
                notifications.addAll(arrayList);
            } else {
                for (NotificationData gsd : arrayList) {
                    Params p = gsd.getParams();
                    String title = p.getTitle() == null ? "" : p.getTitle().toLowerCase(Locale.getDefault());
                    String text = p.getText() == null ? "" : p.getText().toLowerCase(Locale.getDefault());
                    String shortDesc = p.getShort_desc() == null ? "" : p.getShort_desc().toLowerCase(Locale.getDefault());
                    String shortText = p.getShort() == null ? "" : p.getShort().toLowerCase(Locale.getDefault());
                    if (title.contains(charText) || text.contains(charText) || shortDesc.contains(charText) || shortText.contains(charText)) {
                        notifications.add(gsd);
                    }
                }
//                notifications.add(arrayList.get(arrayList.size() - 1));
            }
        }
        notifyDataSetChanged();
    }


    public int getCurrentItemSize() {
        return arrayList.size();
    }

    public void updateItems(ArrayList<NotificationData> feeds, boolean addMore) {
        if (!addMore)
            arrayList.clear();
        arrayList.addAll(feeds);
    }

    public void OnReceive(Context context, Intent intent) {

    }


}