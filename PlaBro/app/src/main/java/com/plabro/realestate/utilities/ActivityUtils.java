package com.plabro.realestate.utilities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.payu.india.Payu.PayuConstants;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.ImageGallery;
import com.plabro.realestate.R;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.activity.RelatedTagFeeds;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.listeners.MaterialDialogListener;
import com.plabro.realestate.listeners.MyClipboardManager;
import com.plabro.realestate.models.Calls.CallPopUpInfo;
import com.plabro.realestate.models.Calls.CallData;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.uiHelpers.LinkMovementMethodOverride;
import com.plabro.realestate.uiHelpers.MaterialDialog;
import com.plabro.realestate.widgets.dialogs.PhoneDialog;
import com.squareup.picasso.Picasso;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jmd on 5/1/2015.
 */
public class ActivityUtils {

    private static final String TAG = ActivityUtils.class.getSimpleName();

    // public static String PHONE_REGEX = "((\\+91)?\\-?\\s*\\(?0?12[094]\\s*\\)?\\s*\\-?\\s*[0-9]{6,13})|((\\+91\\s*\\-)?\\s*[0-9]{10,13})|([0-9]{5}\\s*\\-\\s*[0-9]{5})|\\+91\\s*[0-9]{3}\\s*[0-9]{3}\\s*[0-9]{4}|[0-9]{2}\\s*[0-9]{3}\\s*[0-9]{2}\\s*[0-9]{3}\\b|[0-9]{2}\\s*[0-9]{2}\\s*[0-9]{3}\\s*[0-9]{3}\\b";
    public static String PHONE_REGEX = "((\\+91)?\\-?\\s*\\(?0?12[094]\\s*\\)?\\s*\\-?\\s*[0-9]{6,13})|((\\+91\\s*\\-)?\\s*[0-9]{10,13})|([0-9]{5}\\s*\\-\\s*[0-9]{5})|\\+91\\s*[0-9]{3}\\s*[0-9]{3}\\s*[0-9]{4}|[0-9]{2}\\s*[0-9]{3}\\s*[0-9]{2}\\s*[0-9]{3}\\b|[0-9]{2}\\s*[0-9]{2}\\s*[0-9]{3}\\s*[0-9]{3}\\b";
    public static String EMAIL_REGEX = "[\\.a-zA-Z0-9_\\-]+@[a-z0-9]+\\.[a-z]{2,}[.]?([a-z]{2,})*";
    public static String WEB_REGEX = "(?i)(https?:\\/\\/www\\.[\\-a-z0-9]+\\.(gl|com|net|in|co\\.in|org)(\\/[^\\s'\\]]*)*|https?:\\/\\/[\\-a-z0-9]+\\.(gl|com|net|in|co\\.in|org)(\\/[^\\s'\\]]*)*|www\\.[\\-a-z0-9]+\\.(gl|com|net|in|co\\.in|org)(\\/[^\\s'\\]]*)*)";


    public static void showMaterialDialog(Context ctx, final int dialogTypeCode, String title, String message, String okText, String cancelText, final MaterialDialogListener dialogListener) {
        final MaterialDialog mMaterialDialog = new MaterialDialog(ctx);

        mMaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogListener.onDismissListener(dialogTypeCode);
            }
        });

        mMaterialDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton(okText, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mMaterialDialog.dismiss();
                        if (dialogListener != null) {
                            dialogListener.onOkClicked(dialogTypeCode);
                        }

                    }
                })

                .setNegativeButton(cancelText, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mMaterialDialog.dismiss();
                        if (dialogListener != null) {
                            dialogListener.onCancelClicked(dialogTypeCode);
                        }
                    }
                });

        mMaterialDialog.show();
    }

    public static SpannableString getSppnnableStrings(String wholeText,
                                                      int colorId, String... spannedText) {
        SpannableString spanString = new SpannableString(wholeText);
        for (String span : spannedText) {
            int index = wholeText.indexOf(span);
            if (index != -1) {
                ForegroundColorSpan fcSpan = new ForegroundColorSpan(colorId);
                spanString.setSpan(new ForegroundColorSpan(colorId), index,
                        index + span.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        return spanString;
    }

    public static SpannableString getSppnnableString(String wholeText,
                                                     String spannedText, int colorId) {
        SpannableString spanString = new SpannableString(wholeText);
        try {
            int index = wholeText.indexOf(spannedText);
            if (index == -1) {
                return spanString;
            }
            spanString.setSpan(new ForegroundColorSpan(colorId), index, index
                    + spannedText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {

        }
        return spanString;
    }

    public static void copyFeed(String text, Context mContext) {
        String url = Constants.PLABRO_APP_URL;

        if (text != null) {

            if (!text.contains(url)) {
                text += "\n" + url;
            }
            MyClipboardManager clipboardManager = new MyClipboardManager();
            clipboardManager.copyToClipboard(mContext, text);
            Toast.makeText(mContext, "Feed Copied", Toast.LENGTH_SHORT).show();
        }
    }

    public static void copyFeed(Feeds feedCopy, Context ctx) {
        String feedCopiedStr = feedCopy.getProfile_phone() + " : \n" + feedCopy.getText() + "\nBy: " + feedCopy.getProfile_name();

        ActivityUtils.copyFeed(feedCopiedStr, ctx);
    }

    public static void onChatClicked(final Feeds feedObject, Context ctx, String type, int position) {

        if (null != feedObject && !TextUtils.isEmpty(feedObject.getProfile_phone())) {

        } else {
            return;
        }
        String phoneNumber = feedObject.getProfile_phone().toString();
        String authorid = feedObject.getAuthorid() + "";
        List<String> userPhones = new ArrayList<String>();
        userPhones.add(phoneNumber);
        long profileId = feedObject.getAuthorid();

        Utility.userStats(ctx, "im");

        HashMap<String, Object> wrData = new HashMap<String, Object>();
        wrData.put("imTo", phoneNumber);
        wrData.put("imFrom", PBPreferences.getPhone());
        wrData.put("source", type);
        wrData.put("ScreenName", TAG);
        wrData.put("Action", "IM");
        wrData.put("ClickedIndex", "" + position);

        Analytics.trackEventWithProperties(R.string.consumption, R.string.e_shout_action, wrData);


        Intent intent = new Intent(ctx, XMPPChat.class);
        String num = userPhones.get(0);
        String displayName = ActivityUtils.getDisplayNameFromPhone(num);
        if (displayName != null && !TextUtils.isEmpty(displayName)) {
            intent.putExtra("name", displayName);

        } else {
            intent.putExtra("name", feedObject.getProfile_name());

        }
        intent.putExtra("img", feedObject.getProfile_img());
        intent.putExtra("last_seen", feedObject.getTime());

        intent.putExtra("phone_key", num);
        intent.putExtra("userid", profileId + "");
        intent.putExtra("feed_text", feedObject.getSumm()); //summary of hashtags

        PlabroIntentService.startForNotifyingInteraction(wrData);
        Bundle b = new Bundle();
        b.putSerializable(Constants.BUNDLE_KEY_FEED, feedObject);
        intent.putExtras(b);
        ctx.startActivity(intent);
    }

    public static void copyText(String text, Context mContext) {

        if (text != null) {

            MyClipboardManager clipboardManager = new MyClipboardManager();
            clipboardManager.copyToClipboard(mContext, text);
            Toast.makeText(mContext, "Text Copied", Toast.LENGTH_SHORT).show();
        }
    }


    public static SpannableString setHashTags(final Feeds feed, String text, int startIndex, int endIndex, final Context mContext, TextView postContent, final String type, final int position, boolean isInThread, final int refKey, final Activity mActivity) {

        List<String> hlList = feed.getHl(); //highlighting
        int end = text.length() - 0;
        int start = 0;

        boolean highlightingApplied = false;
        SpannableString hashTagText = new SpannableString(text);
        try {
            postContent.setOnTouchListener((new LinkMovementMethodOverride()));
            //postContent.s(LinkMovementMethod.getInstance());
            boolean tagFound = false;
            List<String[]> hashItemsList = null;
            if (isInThread) {
                hashItemsList = feed.getHashingListIndices();
            } else {
                hashItemsList = feed.getHashingListIndicesSnippet();

            }
            Log.v(TAG, hashItemsList + "");


            for (int hashCtr = 0; hashCtr < hashItemsList.size(); hashCtr++) {
                int indexStart = Integer.parseInt(hashItemsList.get(hashCtr)[0]);
                int indexEnd = Integer.parseInt(hashItemsList.get(hashCtr)[1]) - 1;


                if (indexStart >= start && indexEnd >= start && indexStart <= end && indexEnd < end) {

                    indexStart = indexStart - start;
                    indexEnd = indexEnd - start;

                    if ((indexStart - 1) >= 0) {

                        if (text.substring((indexStart - 1), indexStart).equalsIgnoreCase("+")) {
                            indexStart = indexStart - 1;
                        }
                    }

                    hashTagText.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.appColor3)), indexStart, indexEnd + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                    ClickableSpan hashClick =
                            new ClickableSpan() {
                                @Override
                                public void onClick(View v) {
                                    TextView tv = (TextView) v;
                                    Spanned s = (Spanned) tv.getText();
                                    int start = s.getSpanStart(this);
                                    int end = s.getSpanEnd(this);
                                    Log.v("PLABRO", s.toString());

                                    String hashStr = s.subSequence(start, end).toString();
                                    if (Util.checkIfStringIsPhone(hashStr)) {

                                        Utility.userStats(mContext, "call");

                                        HashMap<String, Object> data = new HashMap<String, Object>();
                                        data.put("callFrom", PBPreferences.getPhone());
                                        data.put("callTo", hashStr);
                                        data.put("type", type);
                                        data.put("position", position + "");


                                        if (hashStr != null && hashStr != "") {
                                            CallPopUpInfo.feeds = feed;
                                            ActivityUtils.initiateCallDialog(mContext, feed, data);
                                        } else {
                                            Toast.makeText(mContext,
                                                    "No phone number availablle.", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (Util.checkIfStringIsWebUrl(hashStr)) {
                                        //WizRocket tracking
                                        HashMap<String, Object> wrData = new HashMap<String, Object>();

                                        hashStr = hashStr.trim();
                                        if (!hashStr.startsWith("http://") && !hashStr.startsWith("https://")) {
                                            hashStr = "http://" + hashStr;
                                        }
                                        Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse(hashStr));
                                        mContext.startActivity(viewIntent);

                                    } else if (Util.checkIfStringIsEmail(hashStr)) {
                                        //WizRocket tracking
                                        HashMap<String, Object> wrData = new HashMap<String, Object>();

                                        hashStr = hashStr.trim();
                                        PBSharingManager.sendMail(mActivity, hashStr, "", "", "");

                                    } else if (hashStr.toLowerCase().contains("#")) {
                                        //WizRocket tracking
                                        HashMap<String, Object> wrData = new HashMap<String, Object>();
                                        wrData.put("ScreenName", type);
                                        wrData.put("Action", "HashTag");
                                        wrData.put(Analytics.Params.HASH_TAG, hashStr);
                                        Analytics.trackInteractionEvent(Analytics.CardActions.HashClicked, wrData);

                                        Intent intent = new Intent(mContext, RelatedTagFeeds.class);
                                        intent.setFlags(intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.putExtra("hashTitle", hashStr);
                                        intent.putExtra("refKey", refKey);
                                        intent.putExtra("message_id", feed.get_id());
                                        intent.putExtra("type", feed.getType());

                                        mContext.startActivity(intent);
                                    }
                                }
                            };

//                        if(!feed.isRel()) {
//                        }
                    hashTagText.setSpan(hashClick, indexStart, indexEnd + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                    hashTagText = setUpSearchHighlighters(mContext, hashTagText, postContent, hlList, text, isInThread);
                    tagFound = true;
                }
                //}
            }

            //show searched text highlighting if hash does not exists
            if (hashItemsList.size() <= 0 || tagFound == false) {
                if (!highlightingApplied) {
                    hashTagText = setUpSearchHighlighters(mContext, hashTagText, postContent, hlList, text, isInThread);
                    highlightingApplied = true;

                } else {
                    if (!isInThread) {
                        // viewHolder.getPost_content().setText(TextUtils.join("\n", strings));

                        postContent.setText(text);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            postContent.setText(text);

        }
        return hashTagText;
    }


    public static SpannableString setUpSearchHighlighters(Context mContext, SpannableString hashTagText, TextView postContent, List<String> hlList, String text, boolean isInThread) {
        for (String hashItem : hlList) {
            String patternString = hashItem;
            final Pattern pattern = Pattern.compile("\\b" + patternString.toLowerCase() + "\\b");
            Matcher matcher = pattern.matcher(text.toLowerCase());
            while (matcher.find()) {
                int hlStart = matcher.start();
                int hlEnd = matcher.end();
                hashTagText.setSpan(new BackgroundColorSpan(mContext.getResources().getColor(R.color.appColor29)), hlStart, hlEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                // hashTagText.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.white)), hlStart, hlEnd, 0);
                hashTagText.setSpan(new StyleSpan(Typeface.NORMAL), hlStart, hlEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            }
        }

        if (!isInThread) {
            postContent.setText(hashTagText, TextView.BufferType.SPANNABLE);

        }
        return hashTagText;

    }

    private static Pattern phonePattern = Pattern.compile("\\d{10}");

    public static String getReceipient(String mRecipient) {
        if (TextUtils.isEmpty(mRecipient)) {
            return "";
        }
        String recipient = mRecipient;

        if (TextUtils.isEmpty(mRecipient)) {
            return "";
        }

        recipient = recipient.trim();
        if (recipient.substring(0, 1).equalsIgnoreCase("0")) {
            recipient = recipient.substring(1);
        }

        String countryCode = PBPreferences.getData(PBPreferences.COUNTRY_CODE, Constants.DEF_COUNTRY_CODE);
        if (recipient.contains(countryCode) == false && phonePattern.matcher(recipient).find()) {
            recipient = countryCode + recipient;
        }
        return recipient;
    }

    public static String getTo(String recipient) {
        String reciepient = getReceipient(recipient);
        String to = reciepient + "@plabro.com/Smack";
        // String to = reciepient + "@AppServerLB-1801796256.ap-southeast-1.elb.amazonaws.com/Smack";
        to = to.replaceAll("\\s+", "");
        Log.d(ActivityUtils.class.getSimpleName(), "To : " + to);
        return to;
    }

    public static String modifyContact(String mobileNumber) {
        Log.d(TAG, "Number before modifying:" + mobileNumber);
        String mobile = mobileNumber.replace("(", "").replace(")", "").replaceAll("-", "").replaceAll(" ", "").replaceAll("\\.", "").trim();
        return getLastTenDigits(mobile);
    }

    public static String getLastTenDigits(String number) {
//        if (true) {
//            return number;
//        }
        String countryCode = PBPreferences.getCountryCode();

        if (number != null && number.length() > 10) {
            return number.substring(number.length() - 10, number.length());
        } else {
            return number;
        }


//        if (number.contains(countryCode)) {
//            int lastIndex = number.lastIndexOf(countryCode);
//            number = number.substring(lastIndex);
//            Log.d(TAG, "Number After:" + number);
//            return number;
//        } else {
//            return number;
//        }
    }


//    public static String getDisplayNameFromPhone(String phone) {
//        HashMap<String, String> contactsMap = new HashMap<String, String>();
//        contactsMap = PBPreferences.getContactsMap();
//        return getDisplayNameFromPhone(contactsMap, phone);
//    }

    public static String getDisplayNameFromPhone(String phone) {
        phone = getLastTenDigits(phone);
        HashMap<String, String> contactsMap = PBPreferences.getContactsMap();
        if (contactsMap != null && contactsMap.size() > 0) {
            Log.d(TAG, "Contacts Map is NOt Null:" + phone);
            String displayName = contactsMap.get(phone);
            if (displayName != null && !TextUtils.isEmpty(displayName)) {
                Log.d(TAG, "DN :" + displayName);
                return displayName;

            }
        }

        return "";
    }

    public static void callPhone(final Context context, final String phone, HashMap<String, Object> data) {

        Log.d(TAG, "Calling to number" + phone);
        if (phone == null || phone.equalsIgnoreCase("") || phone.equalsIgnoreCase("null")) {
            Toast.makeText(context,
                    "Invalid Phone", Toast.LENGTH_SHORT).show();
            return;
        }
        if (null == data) {
            data = new HashMap<>();
        }
        data.put("Action", "Call");
        data.put("callFrom", PBPreferences.getPhone());
        data.put("callTo", phone.replace("+", ""));
        Analytics.trackInteractionEvent(Analytics.CardActions.Called, data);
        Intent phoneIntent = new Intent(Intent.ACTION_CALL);
        phoneIntent.setData(Uri.parse("tel:" + phone));

        try {
            PlabroIntentService.startForNotifyingInteraction(data);
            context.startActivity(phoneIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context,
                    "Call failed, please try again later.", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(context,
                    "Call failed, please try again later.", Toast.LENGTH_SHORT).show();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean askPermissionToCall(Activity act, String permission) {
        if (AppController.getInstance().checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            if (act.shouldShowRequestPermissionRationale(permission)) {
                Toast.makeText(act, "Plabro Requires permission to call", Toast.LENGTH_SHORT).show();
            }

            act.requestPermissions(new String[]{permission}, PlaBroApi.PERMISSIONS.CALL_PHONE);
        }
        return false;
    }

    public static void callPhone(final Context context, final String phone) {
        callPhone(context, phone, null);
    }

    public static long getAdditionalTime(int fieldType, int additionalTime) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        c.add(fieldType, additionalTime);
        return c.getTimeInMillis();
    }

    public static void printMatches(String text, TextView postContent, final Activity mActivity) {


        SpannableString hashTagText = new SpannableString(text);
        postContent.setOnTouchListener((new LinkMovementMethodOverride()));

        int indexStart = 0;
        int indexEnd = 0;

        Pattern patternPhone = Pattern.compile(PHONE_REGEX);
        Matcher matcherPhone = patternPhone.matcher(text);
        // Check all occurrences for phone
        while (matcherPhone.find()) {
            indexStart = matcherPhone.start();
            indexEnd = matcherPhone.end() - 1;
            hashTagText = setupHighlighting(indexStart, indexEnd, hashTagText, mActivity, mActivity, postContent);

        }

        Pattern patternEmail = Pattern.compile(EMAIL_REGEX);
        Matcher matcherEmail = patternEmail.matcher(text);
        // Check all occurrences for phone
        while (matcherEmail.find()) {
            indexStart = matcherEmail.start();
            indexEnd = matcherEmail.end() - 1;
            hashTagText = setupHighlighting(indexStart, indexEnd, hashTagText, mActivity, mActivity, postContent);
        }

        Pattern patternWeb = Pattern.compile(WEB_REGEX);
        Matcher matcherWeb = patternWeb.matcher(text);
        // Check all occurrences for phone
        while (matcherWeb.find()) {
            indexStart = matcherWeb.start();
            indexEnd = matcherWeb.end() - 1;
            hashTagText = setupHighlighting(indexStart, indexEnd, hashTagText, mActivity, mActivity, postContent);
        }

        postContent.setText(hashTagText, TextView.BufferType.SPANNABLE);


    }


    private static SpannableString setupHighlighting(int indexStart, int indexEnd, SpannableString hashTagText, final Context mContext, final Activity mActivity, TextView postContent) {
        String name = TAG;
        try {
            name = ((PlabroBaseActivity) mActivity).getName();
        } catch (Exception e) {

        }
        final String type = name;
        hashTagText.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.colorPrimary)), indexStart, indexEnd + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        ClickableSpan hashClick =
                new ClickableSpan() {
                    @Override
                    public void onClick(View v) {
                        TextView tv = (TextView) v;
                        Spanned s = (Spanned) tv.getText();
                        int start = s.getSpanStart(this);
                        int end = s.getSpanEnd(this);
                        Log.v("PLABRO", s.toString());

                        String hashStr = s.subSequence(start, end).toString();
                        if (Util.checkIfStringIsPhone(hashStr)) {

                            Utility.userStats(mContext, "call");

                            HashMap<String, Object> data = new HashMap<String, Object>();
                            data.put("callFrom", PBPreferences.getPhone());
                            data.put("callTo", hashStr);


                            Analytics.trackEventWithProperties(R.string.consumption, R.string.e_call, data);


                            if (hashStr != null && hashStr != "") {
                                ActivityUtils.callPhone(mContext, hashStr);
                            } else {
                                Toast.makeText(mContext,
                                        "No phone number availablle.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (Util.checkIfStringIsEmail(hashStr)) {
                            HashMap<String, Object> wrData = new HashMap<String, Object>();
                            wrData.put("ScreenName", type);
                            wrData.put("Action", "Email");
                            Analytics.trackEventWithProperties(R.string.social, R.string.e_shout_action, wrData);

                            PBSharingManager.sendMail(mActivity, hashStr, "", "", "");

                        } else if (Util.checkIfStringIsWebUrl(hashStr)) {
                            HashMap<String, Object> wrData = new HashMap<String, Object>();
                            wrData.put("ScreenName", type);
                            wrData.put("Action", "Web");
                            Analytics.trackEventWithProperties(R.string.social, R.string.e_shout_action, wrData);

                            PBSharingManager.openWebUrl(hashStr, mActivity);

                        }

                    }
                };

        hashTagText.setSpan(hashClick, indexStart, indexEnd + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return hashTagText;
    }

    public static Bitmap getBitmapFromImageView(ImageView mImageView) {
        Bitmap bitmap;
        if (mImageView.getDrawable() instanceof BitmapDrawable) {
            bitmap = ((BitmapDrawable) mImageView.getDrawable()).getBitmap();
        } else {
            Drawable d = mImageView.getDrawable();
            bitmap = Bitmap.createBitmap(d.getIntrinsicWidth(), d.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            d.draw(canvas);
        }
        return bitmap;
    }

    public static void sendLocalBroadcast(Context ctx, Intent intent) {
        ctx.sendBroadcast(intent);
        //LocalBroadcastManager.getInstance(ctx).sendBroadcast(intent);
    }

    public static void sendLocalBroadcast(Context ctx, String action) {
        Intent i = new Intent();
        i.setAction(action);
        ctx.sendBroadcast(i);
        //LocalBroadcastManager.getInstance(ctx).sendBroadcast(intent);
    }

    public static void dismissProgressBar(ProgressDialog dialog) {
        if (null != dialog && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public static void loadImage(Context ctx, String imgUrl, ImageView imgView) {
        loadImage(ctx, imgUrl, imgView, R.drawable.profile_default_one);
    }

    public static void loadImage(Context ctx, String imgUrl, ImageView imgView, int defIcon) {
        try {
            if (imgUrl != null && !TextUtils.isEmpty(imgUrl)) {
                if (defIcon == 0) {
                    Picasso.with(ctx).load(imgUrl).into(imgView);
                } else {
                    Picasso.with(ctx).load(imgUrl).placeholder(defIcon).into(imgView);
                }
            } else {
                if (defIcon != 0) {
                    imgView.setImageResource(defIcon);
                }
            }
        } catch (OutOfMemoryError e) {
        } catch (Exception e) {

        }
    }

    public static boolean WritePhoneContact(String displayName, String number) {
        Context contetx = AppController.getInstance(); //Application's context or Activity's context
        String strDisplayName = displayName; // Name of the Person to add
        String strNumber = number; //number of the person to add with the Contact

        ArrayList<ContentProviderOperation> cntProOper = new ArrayList<ContentProviderOperation>();
        int contactIndex = cntProOper.size();//ContactSize

        //Newly Inserted contact
        // A raw contact will be inserted ContactsContract.RawContacts table in contacts database.
        cntProOper.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)//Step1
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());

        //Display name will be inserted in ContactsContract.Data table
        cntProOper.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)//Step2
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, contactIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, strDisplayName) // Name of the contact
                .build());
        //Mobile number will be inserted in ContactsContract.Data table
        cntProOper.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)//Step 3
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, contactIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, strNumber) // Number to be added
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE).build()); //Type like HOME, MOBILE etc
        try {
            // We will do batch operation to insert all above data
            //Contains the output of the app of a ContentProviderOperation.
            //It is sure to have exactly one of uri or count set
            ContentProviderResult[] contentProresult = null;
            contentProresult = contetx.getContentResolver().applyBatch(ContactsContract.AUTHORITY, cntProOper);
            //apply above data insertion into contacts list
            return true;
        } catch (RemoteException exp) {
            //logs;
            exp.printStackTrace();
        } catch (OperationApplicationException exp) {
            //logs
            exp.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void openImageGallery(Context ctx, int position, ArrayList<String> imageUrlList) {
        Intent intent = new Intent(ctx, ImageGallery.class);
        intent.putStringArrayListExtra("menus", imageUrlList);
        intent.putExtra("position", 0);
        ctx.startActivity(intent);
    }

    public static void openImageGallery(Context ctx, int position, String... images) {
        ArrayList<String> imageUrlList = new ArrayList<>();
        if (null != images && images.length > 0) {
            for (String img : images) {
                if (!TextUtils.isEmpty(img))
                    imageUrlList.add(img);
            }
        } else {
            return;
        }
        if (imageUrlList.size() > 0)
            openImageGallery(ctx, position, imageUrlList);
    }

    public static Bitmap uriToBitmap(Uri selectedFileUri, Context context) {
        Bitmap image = null;
        try {
            ParcelFileDescriptor parcelFileDescriptor =
                    context.getContentResolver().openFileDescriptor(selectedFileUri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            image = BitmapFactory.decodeFileDescriptor(fileDescriptor);

            parcelFileDescriptor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;

    }

    public static void showViews(View... views) {
        for (View v : views) {
            v.setVisibility(View.VISIBLE);
        }
    }

    public static void hideViews(View... views) {
        for (View v : views) {
            v.setVisibility(View.GONE);
        }
    }

    public static void showViews(Activity act, int... ids) {
        for (int id : ids) {
            act.findViewById(id).setVisibility(View.VISIBLE);
        }
    }

    public static void hideViews(Activity act, int... ids) {
        for (int id : ids) {
            act.findViewById(id).setVisibility(View.GONE);
        }
    }

    public static void onCallClicked(Feeds f, Context ctx, String TAG, String type, int position) {
        String phone = f.getProfile_phone();
        boolean status = Util.checkIfStringIsPhone(phone);

        Utility.userStats(ctx, "call");


        HashMap<String, Object> wrData = getCallStats(f, TAG, type, position);
        CallPopUpInfo.feeds = f;
        callPhone(ctx, phone, wrData);

    }

    public static HashMap<String, Object> getCallStats(Feeds f, String TAG, String type, int position) {
        HashMap<String, Object> wrData = new HashMap<String, Object>();


        wrData.put("ScreenName", TAG);

        wrData.put("callFrom", PBPreferences.getPhone());
        wrData.put("callTo", f.getProfile_phone());
        wrData.put("source", type);
        wrData.put("ClickedIndex", "" + position);
        return wrData;
    }

    public static void setStatusBarColor(Activity ctx, int color) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = ctx.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(color);
        }
    }

    public static String getModifiedImageUrl(String img) {
        if (TextUtils.isEmpty(img) || img.contains("?time=")) return img;
        img += "?time=" + System.currentTimeMillis();
        return img;
    }

    public static void initiateCallDialog(Context context, BaseFeed feed, List<String> phonemap, String profile_phone, HashMap<String, Object> wrData, int freeCall, String profile_img, String profile_name) {


        List arr_phone = new ArrayList();
        if (null != profile_phone && !TextUtils.isEmpty(profile_phone)) {
            arr_phone.add(profile_phone);
        }
        arr_phone.addAll(phonemap);


        if (arr_phone.size() > 0) {
            PhoneDialog phoneDialog = new PhoneDialog(context, feed, arr_phone, freeCall, wrData, profile_img, profile_name);
            if (context instanceof FragmentActivity)
                phoneDialog.show(((FragmentActivity) context).getSupportFragmentManager(), "phoneDialog");
            phoneDialog.setCancelable(true);
        } else {
            Util.showSnackBarMessage((Activity) context, context, "No contact number available");
        }

    }

    public static void initiateCallDialog(Context context, BaseFeed feed, HashMap<String, Object> wrData) {
        if (feed instanceof Feeds) {
            Feeds f = (Feeds) feed;
            initiateCallDialog(context, f, f.getPhonemap(), f.getProfile_phone(), wrData, f.getIsFreeCall(), f.getProfile_img(), f.getProfile_name());
        } else if (feed instanceof ProfileClass) {
            ProfileClass f = (ProfileClass) feed;
            initiateCallDialog(context, f, new ArrayList<String>(), f.getPhone(), wrData, f.getIsFreeCall(), f.getImg(), f.getName());
        }
    }

    public static Feeds createFeedObjectFromProfile(ProfileClass profile) {
        Feeds f = new Feeds();
        f.setProfile_img(profile.getImg());
        f.setEndorsement_data(profile.getEndorsement_data());
        f.setProfile_name(profile.getName());
        f.setProfile_phone(profile.getPhone());
        try {
            f.setAuthorid(Long.parseLong(profile.getAuthorid()));
        } catch (NumberFormatException e) {

        }
        f.setIsFromProfile(true);
        return f;
    }

    public static boolean isCallConnected(Context ctx, Feeds feeds, String source_page) {
        CallData callDetail = CallData.getLastCallDetail(ctx);
        if (null != callDetail && null != feeds.getIncomingNumber() && feeds.getIncomingNumber().equalsIgnoreCase(callDetail.getCallee())) {
            if ("0".equalsIgnoreCase(callDetail.getDuration())) {
                return false;
            }
            callDetail.setPlabro_call(true);
            callDetail.setSource_page(source_page);
            callDetail.setCaller(PBPreferences.getPhone());
            saveCallUtilToDB(ctx, callDetail, feeds);
            return true;
        }
        return false;
    }

    public static void saveCallUtilToDB(Context ctx, CallData call, Feeds feeds) {
        call.setIsFromProfile(feeds.isFromProfile());
        if (feeds.isFromProfile()) {
            call.setAuthorid(feeds.getAuthorid());
        } else {
            call.setShoutid(Long.parseLong(feeds.get_id()));
        }
        long i = call.save();
        Log.d(TAG, "is Call Log saved" + i);
        PlabroIntentService.sendCallLogsToServer(ctx);

    }

    public static void saveFreeCallLogsToUtil(Context ctx, BaseFeed feed, String sourcePage) {
        CallData util = new CallData();
        util.setPlabro_call(true);
        util.setCaller(PBPreferences.getPhone());
        String callee = "";
        long authorid, shoutid;
        if (feed instanceof ProfileClass) {
            ProfileClass f = (ProfileClass) feed;
            util.setAuthorid(Long.parseLong(f.getAuthorid()));
            util.setCallee(f.getPhone());
            util.setName(f.getName());
            util.setIsFromProfile(true);
        } else if (feed instanceof Feeds) {
            Feeds f = (Feeds) feed;
            util.setShoutid(Long.parseLong(f.get_id()));
            util.setCallee(f.getProfile_phone());
            util.setName(f.getProfile_name());
        }
        util.setSource_page(sourcePage);
        util.setDuration("0");
        util.setCall_type("outgoing");
        util.setIsFreeCall(true);
        util.setCallTime(System.currentTimeMillis());
        long i = util.save();
        Log.d(TAG, "is Call Log saved" + i);

        HashMap<String, Object> metaData = new HashMap<>();
        metaData.put("from", util.getCaller());
        metaData.put("to", util.getCallee());
        metaData.put("type", "free call");
        metaData.put("screen", sourcePage);
        metaData.put("action", "freeCall");
        PlabroIntentService.startForNotifyingInteraction(metaData);
        Analytics.trackEventWithProperties(R.string.consumption, R.string.e_shout_action, metaData);
        PlabroIntentService.sendCallLogsToServer(ctx);

    }

    public static Drawable getIssuerDrawable(Context ctx, String issuer) {

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
            switch (issuer) {
                case PayuConstants.VISA:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.visa);
                case PayuConstants.LASER:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.laser);
                case PayuConstants.DISCOVER:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.discover);
                case PayuConstants.MAES:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.maestro);
                case PayuConstants.MAST:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.master);
                case PayuConstants.AMEX:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.amex);
                case PayuConstants.DINR:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.diner);
                case PayuConstants.JCB:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.jcb);
                case PayuConstants.SMAE:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.maestro);
                case PayuConstants.RUPAY:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.rupay);
            }
            return null;
        } else {

            switch (issuer) {
                case PayuConstants.VISA:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.visa, null);
                case PayuConstants.LASER:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.laser, null);
                case PayuConstants.DISCOVER:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.discover, null);
                case PayuConstants.MAES:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.maestro, null);
                case PayuConstants.MAST:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.master, null);
                case PayuConstants.AMEX:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.amex, null);
                case PayuConstants.DINR:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.diner, null);
                case PayuConstants.JCB:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.jcb, null);
                case PayuConstants.SMAE:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.maestro, null);
                case PayuConstants.RUPAY:
                    return ctx.getResources().getDrawable(com.payu.payuui.R.drawable.rupay, null);
            }
            return null;
        }
    }


}
