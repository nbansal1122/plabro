package com.plabro.realestate.models.propFeeds;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.utilities.JSONUtils;

import java.io.Serializable;

/**
 * Created by nitin on 23/12/15.
 */
@Table(name = "GreetingFeed")
public class GreetingFeed extends BaseFeed implements Serializable{

    @SerializedName("subtitle")
    @Expose
    private String subtitle;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("city_id")
    @Expose
    private double city_id;
    @SerializedName("_score")
    @Expose
    private String _score;
    @SerializedName("action_text")
    @Expose
    private String action_text;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("_id")
    @Expose
    @Column
    private String _id;

    public String getService_code() {
        return service_code;
    }

    public void setService_code(String service_code) {
        this.service_code = service_code;
    }

    @SerializedName("service_code")
    @Expose
    private String service_code;

    /**
     * @return The subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * @param subtitle The subtitle
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * @return The img
     */
    public String getImg() {
        return img;
    }

    /**
     * @param img The img
     */
    public void setImg(String img) {
        this.img = img;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The city_id
     */
    public Double getCity_id() {
        return city_id;
    }

    /**
     * @param city_id The city_id
     */
    public void setCity_id(Double city_id) {
        this.city_id = city_id;
    }

    /**
     * @return The _score
     */
    public String get_score() {
        return _score;
    }

    /**
     * @param _score The _score
     */
    public void set_score(String _score) {
        this._score = _score;
    }

    /**
     * @return The action_text
     */
    public String getAction_text() {
        return action_text;
    }

    /**
     * @param action_text The action_text
     */
    public void setAction_text(String action_text) {
        this.action_text = action_text;
    }

    /**
     * @return The time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time The time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return The _id
     */
    public String get_id() {
        return _id;
    }

    /**
     * @param _id The _id
     */
    public void set_id(String _id) {
        this._id = _id;
    }


    public static GreetingFeed parseJson(String json) {
        Log.d("GreetingFeed", "Greeting JSON :: " + json);
        return JSONUtils.getGSONBuilder().create().fromJson(json, GreetingFeed.class);
    }
}