package com.plabro.realestate.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.plabro.realestate.AppController;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.auctions.Page_action_form;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.images.handlers.AsyncTask;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utils.RestClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nitin on 25/01/16.
 */
public class SubmitAuctions extends PlabroBaseActivity {

    private LinearLayout contentLayout;
    private ArrayList<Page_action_form> formFields;
    private ArrayList<FieldHolder> holders = new ArrayList<>();
    private String actionUrl, title;

    @Override
    protected String getTag() {
        return "Submit Auction Screen";
    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_auction_details;
    }

    @Override
    protected void loadBundleData(Bundle b) {
        super.loadBundleData(b);
        formFields = (ArrayList<Page_action_form>) b.getSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE);
        title = b.getString(Constants.BUNDLE_KEYS.TITLE);
        actionUrl = b.getString(Constants.BUNDLE_KEYS.INPUT_URL);
    }

    @Override
    public void findViewById() {
        Analytics.trackScreen(Analytics.Requirements.SubmitRequirements);
        contentLayout = (LinearLayout) findViewById(R.id.ll_auction_content);
        inflateToolbar();
        setTitle(title);
        contentLayout.removeAllViewsInLayout();
        for (Page_action_form field : formFields) {
            addRow(field);
        }
        setText(R.id.btn_bid_now, "SUBMIT NOW");
        setClickListeners(R.id.btn_bid_now);
    }

    private void addRow(Page_action_form field) {
        View row = LayoutInflater.from(this).inflate(R.layout.layout_et_auction_data, null);
        EditText et = (EditText) row.findViewById(R.id.et_auction_field);
//        TextView typeTv = (TextView) row.findViewById(R.id.tv_auction_field_type);
//        typeTv.setText(field.getTitle());
        et.setHint(field.getTitle() + " : " + field.getHint());
        if (field.getType().equalsIgnoreCase("number")) {
            et.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
        FieldHolder holder = new FieldHolder();
        holder.field = field;
        holder.row = row;
        holders.add(holder);
        contentLayout.addView(row);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btn_bid_now:
                submitBid();
                break;
        }
    }

    private void submitBid() {
        boolean isValid = true;
        Analytics.trackEventWithProperties(Analytics.Categories.Requirement, Analytics.Actions.Submitted, new HashMap<String, Object>());
        HashMap<String, String> params = new HashMap<>();
        try {
            JSONArray array = new JSONArray();
            for (FieldHolder holder : holders) {
                Page_action_form field = holder.field;
                switch (field.getType()) {
                    case "text":
                        JSONObject obj = new JSONObject();
                        EditText et = (EditText) holder.row.findViewById(R.id.et_auction_field);
                        String text = et.getText().toString().trim();
                        if (field.is_compulsory()) {
                            if (TextUtils.isEmpty(text)) {
                                showToast(field.getTitle() + "  can not be empty");
                                isValid = false;
                                return;
                            }
                        }
                        if (!TextUtils.isEmpty(text))
                            params.put(field.get_id(), text);
                        obj.put(field.get_id(), text);
                        array.put(obj);
                        break;
                }
            }

            if (isValid) {
                sendPostData(array, params);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void sendPostData(JSONArray array, HashMap<String, String> params) {
        final ParamObject obj = new ParamObject();
        obj.setRequestMethod(RequestMethod.POST);
        if (!TextUtils.isEmpty(PBPreferences.getAuthKey())) {
            params.put(PlaBroApi.AUTH_KEY, PBPreferences.getAuthKey());
        }
        ProfileClass profile = AppController.getInstance().getUserProfile();
        if (null != profile) {
            params.put("userid", profile.getAuthorid() + "");
        }
        params.put(PlaBroApi.APP_SOURCE, "android");
        obj.setParams(params);
        obj.setUrl(actionUrl);
        showDialog();
        new AsyncTask<Object, Object, Object>() {
            @Override
            protected Object doInBackground(Object... params) {
                return RestClient.sendHttpRequest(obj, null);
            }

            @Override
            protected void onPostExecute(Object o) {
                dismissDialog();
                if (o != null) {
                    String response = (String) o;
                    showToast("Your bid submitted successfully");
                    setResult(RESULT_OK);
                    Intent i = new Intent(SubmitAuctions.this, ThankYouAuction.class);
                    startActivity(i);
                    finish();
                } else {
                    showToast("Oops some server error occurred");
                }

                super.onPostExecute(o);
            }
        }.execute();
    }

    @Override
    public void reloadRequest() {

    }

    private class FieldHolder {
        Page_action_form field;
        View row;
    }
}
