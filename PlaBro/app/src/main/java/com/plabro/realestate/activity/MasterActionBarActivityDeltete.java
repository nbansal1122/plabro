package com.plabro.realestate.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.plabro.realestate.R;
import com.plabro.realestate.listeners.MasterLifecycleInterface;
import com.plabro.realestate.widgets.dialogs.DOLoaderDialog;
import com.plabro.realestate.widgets.dialogs.PBConditionalDialogView;


public abstract class MasterActionBarActivityDeltete extends PlabroBaseActivity implements MasterLifecycleInterface {


    static Animation animFadeIn, animFadeOut, animTopIn, animTopOut, animZoomIn, animZoomOut, animTopInSlow, animTopOutSlow;
    DOLoaderDialog progressScreenDialog = null;



    PBConditionalDialogView mTcConditionalDialogView = null;

    public enum AnimationDef {
        ALL, FADE_IN, FADE_OUT, TOP_IN, TOP_OUT, ZOOM_IN, ZOOM_OUT, ZOOM_IN_LOW, ZOOM_OUT_LOW, TOP_IN_LOW, TOP_OUT_LOW;
    }

    protected void onCreate(Bundle savedInstanceState, int layoutResourceId) {
        super.onCreate(savedInstanceState);
        setContentView(layoutResourceId);
         loadDefaultData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(0 != getResourceId()){
            setContentView(getResourceId());
            loadDefaultData();
        }
    }

    protected int getResourceId(){
        return 0;
    }

    protected void loadDefaultData() {

        if (animFadeIn == null || animFadeOut == null || animTopIn == null || animTopOut == null || animZoomIn == null || animZoomOut == null || animTopInSlow == null || animTopOutSlow == null) {
            getAnimation(AnimationDef.ALL);
        }

        findViewById();
        inflateToolbar();
        setViewSizes();

        if (findViewById(R.id.pbConditionalView) != null) {
            mTcConditionalDialogView = (PBConditionalDialogView) findViewById(R.id.pbConditionalView);

            mTcConditionalDialogView.setOnPBConditionListener(new PBConditionalDialogView.OnPBConditionListener() {

                @Override
                public void onRefresh() {

                    showConditionalMessage(null, false, false);

                    reloadRequest();

                }
            });
        }

    }


    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {

        if (intent != null) {

            super.startActivityForResult(intent, requestCode);
        }
    }



    @Override
    public void finish() {
        super.finish();
    }

    public void finishWithReverseAnimation1() {
        super.finish();
    }


    public void showLoadingDialog(boolean isShow, boolean onBackPressedCancel) {

        if (progressScreenDialog == null && isShow) {
            try {
                progressScreenDialog = DOLoaderDialog.newInstance(onBackPressedCancel);
                progressScreenDialog.setCancelable(onBackPressedCancel);
                // progressScreenDialog.show(getSupportFragmentManager(), null);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(progressScreenDialog, null);
                ft.commitAllowingStateLoss();
                //getSupportFragmentManager().executePendingTransactions();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (progressScreenDialog != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        progressScreenDialog.dismiss();
                        progressScreenDialog = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 500);

        }
    }



//    wr.data.pushGcmRegistrationId(gcmRegId, true);

    protected void showConditionalMessage(PBConditionalDialogView.ConditionalDialog conditionalDialog, boolean isShow, boolean isShiftLayout) {

        try {

            if (mTcConditionalDialogView == null)
                return;

            if (isShow) {
                mTcConditionalDialogView.setType(conditionalDialog);
                mTcConditionalDialogView.setVisibility(View.VISIBLE);

                findViewById(R.id.pbConditionalView).setVisibility(View.VISIBLE);
                if (isShiftLayout) {
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    params.setMargins(0, 0, 0, (int) getResources().getDimension(R.dimen.conditional_dialog_margin_bottom));
                    mTcConditionalDialogView.setLayoutParams(params);
                }
            } else {
                mTcConditionalDialogView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Animation getAnimation(AnimationDef animationDef) {

        switch (animationDef) {
            case FADE_IN:
                if (animFadeIn == null)
                    animFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);

                return animFadeIn;

            case FADE_OUT:
                if (animFadeOut == null)
                    animFadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);

                return animFadeOut;

            case TOP_IN:
                if (animTopIn == null)
                    animTopIn = AnimationUtils.loadAnimation(this, R.anim.top_in);
                return animTopIn;

            case TOP_OUT:
                if (animTopOut == null)
                    animTopOut = AnimationUtils.loadAnimation(this, R.anim.bottom_out);
                return animTopOut;

            case ZOOM_IN:
                if (animZoomIn == null)
                    animZoomIn = AnimationUtils.loadAnimation(this, R.anim.zoom_in);

                return animZoomIn;

            case ZOOM_OUT:
                if (animZoomOut == null)
                    animZoomOut = AnimationUtils.loadAnimation(this, R.anim.zoom_out);

                return animZoomOut;
            case ZOOM_IN_LOW:
                if (animZoomIn == null)
                    animZoomIn = AnimationUtils.loadAnimation(this, R.anim.zoom_in_low);

                return animZoomIn;

            case ZOOM_OUT_LOW:
                if (animZoomOut == null)
                    animZoomOut = AnimationUtils.loadAnimation(this, R.anim.zoom_out_low);

                return animZoomOut;

            case TOP_IN_LOW:
                if (animTopInSlow == null)
                    animTopInSlow = AnimationUtils.loadAnimation(this, R.anim.top_in_slow);

                return animTopInSlow;

            case TOP_OUT_LOW:
                if (animTopOutSlow == null)
                    animTopOutSlow = AnimationUtils.loadAnimation(this, R.anim.bottom_out_slow);

                return animTopOutSlow;

            case ALL:
                animFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
                animFadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
                animTopIn = AnimationUtils.loadAnimation(this, R.anim.top_in);
                animTopOut = AnimationUtils.loadAnimation(this, R.anim.bottom_out);
                animZoomIn = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
                animZoomOut = AnimationUtils.loadAnimation(this, R.anim.zoom_out);
                animTopInSlow = AnimationUtils.loadAnimation(this, R.anim.top_in_slow);
                animTopOutSlow = AnimationUtils.loadAnimation(this, R.anim.bottom_out_slow);

                return null;
        }
        return null;
    }

    public void getData(boolean addMore) {

    }
}
