package com.plabro.realestate.widgets.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.plabro.realestate.R;
import com.plabro.realestate.listeners.OnChatMessageListener;
import com.plabro.realestate.models.chat.ChatMessage;

import java.util.ArrayList;

/**
 * Created by Hemant on 19-01-2015.
 */
public class MessageOptionDialog extends DialogFragment {

    private Button mClose;
    private Context mContext;
    private Activity mActivity;
    private ListView mOptions;
    private ArrayList<String> optionsList = new ArrayList<String>();
    private String msgType;
    private OnChatMessageListener messageListener;
    private ChatMessage msg;

    @SuppressLint("ValidFragment")
    public MessageOptionDialog(Activity mActivity, OnChatMessageListener messageListener, ChatMessage msg,String msgType) {
        this.mContext = mActivity.getApplicationContext();
        this.msgType = msgType;
        this.mActivity = mActivity;
        this.messageListener = messageListener;
        this.msg = msg;
    }

    public MessageOptionDialog() {

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.message_option_dialog, null);
        builder.setView(convertView);
        setCancelable(true);

        mClose = (Button) convertView.findViewById(R.id.close);
        mOptions = (ListView) convertView.findViewById(R.id.message_options);
        setUpOptions();
        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return builder.create();
    }


    private void setUpOptions() {

        if (msgType.equalsIgnoreCase("sender")) {
            optionsList.add("Copy");
            //optionsList.add("Delete");
            optionsList.add("Resend");
        } else {
            optionsList.add("Copy");
            //optionsList.add("Delete");
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.message_options_layout, android.R.id.text1, optionsList);

        // Assign adapter to ListView
        mOptions.setAdapter(adapter);

        // ListView Item Click Listener
        mOptions.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                dismiss();
                // ListView Clicked item value
                String itemValue = (String) mOptions.getItemAtPosition(position);
                messageListener.onSelected(true, itemValue, msg);

            }

        });

    }

}
