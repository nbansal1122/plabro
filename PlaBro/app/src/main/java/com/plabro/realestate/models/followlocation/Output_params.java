package com.plabro.realestate.models.followlocation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Output_params {

    @SerializedName("data")
    @Expose
    private List<FollwLocationData> data = new ArrayList<FollwLocationData>();

    /**
     * @return The data
     */
    public List<FollwLocationData> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<FollwLocationData> data) {
        this.data = data;
    }

}
