package com.plabro.realestate.fragments;

import android.support.v7.widget.RecyclerView;

import com.plabro.realestate.CustomLogger.Log;

/**
 * Created by desmond on 1/6/15.
 */
public abstract class RecyclerViewFragment extends ScrollTabHolderFragment {

    protected RecyclerView mRecyclerView;
    protected int mScrollY;

    protected abstract void setScrollOnLayoutManager(int scrollY);

    protected void setRecyclerViewOnScrollListener() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                mScrollY += dy;
                Log.d(TAG, "dy:" + dy + ", scrollY" + mScrollY);

                if (mScrollTabHolder != null) {
                    Log.d(TAG, "Tab Holder Not Null");
                    mScrollTabHolder.onRecyclerViewScroll(recyclerView, dx, dy, mScrollY, mPosition);
                } else {
                    Log.d(TAG, "Tab Holder Not Null");
                }
//                setScrollOnLayoutManager(mScrollY);
                onRecycleScroll(recyclerView, dx, dy, mScrollY, mPosition);

            }
        });
    }

    @Override
    public void adjustScroll(int scrollHeight, int headerHeight) {
        Log.d(TAG, "Adjust Scroll");
        if (mRecyclerView == null) return;

        Log.d(TAG, "HH:" + headerHeight + ", scrollHeight" + scrollHeight);
        mScrollY = headerHeight - scrollHeight;
        setScrollOnLayoutManager(mScrollY);
    }

    protected void onRecycleScroll(RecyclerView view, int dx, int dy, int scrollY, int pagePosition) {

    }
}
