package com.plabro.realestate.uiHelpers;

/**
 * Created by hemant on 18/12/14.
 */

import android.content.Intent;
import android.graphics.drawable.Drawable;

public class CropOption {
    public CharSequence title;
    public Drawable icon;
    public Intent appIntent;
}