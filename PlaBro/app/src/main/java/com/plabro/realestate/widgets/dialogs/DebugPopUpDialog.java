package com.plabro.realestate.widgets.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.plabro.realestate.R;
import com.plabro.realestate.utilities.PBPreferences;

/**
 * Created by Hemant on 19-01-2015.
 */
public class DebugPopUpDialog extends DialogFragment {

    private EditText mDebugUrl;
    private Button mCancel, mOK;
    private Context mContext;
    private Activity mActivity;

    @SuppressLint("ValidFragment")
    public DebugPopUpDialog(Context context) {
        this.mContext = context;

    }

    public DebugPopUpDialog() {

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.dev_pop_up_dialog, null);
        builder.setView(convertView);
        setCancelable(true);

        mDebugUrl = (EditText) convertView.findViewById(R.id.debug_url);
        String prevURl = PBPreferences.getBaseUrl();
        mDebugUrl.setText(prevURl);

        mCancel = (Button) convertView.findViewById(R.id.btn_cancel);
        mOK = (Button) convertView.findViewById(R.id.btn_ok);

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String debugURl = mDebugUrl.getText().toString();
                if(TextUtils.isEmpty(debugURl))
                {
                    Toast.makeText(mContext,"please enter base url",Toast.LENGTH_SHORT).show();
                }
                else {
                    PBPreferences.setBaseUrl( debugURl);
                    dismiss();
                }
            }
        });


        return builder.create();
    }


}
