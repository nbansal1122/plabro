package com.plabro.realestate.models.location;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class GoogleLocationResponse {

    private List<GoogleResult> results = new ArrayList<GoogleResult>();

    private List<String> html_attributions = new ArrayList<String>();

    private String status;

    private String error_message;

    public List<GoogleResult> getResults() {
        return results;
    }

    public void setResults(List<GoogleResult> results) {
        this.results = results;
    }

    public List<String> getHtml_attributions ()
    {
        return html_attributions;
    }

    public void setHtml_attributions (List<String> html_attributions)
    {
        this.html_attributions = html_attributions;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getError_message ()
    {
        return error_message;
    }

    public void setError_message (String error_message)
    {
        this.error_message = error_message;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [results = "+results+", html_attributions = "+html_attributions+", status = "+status+", error_message = "+error_message+"]";
    }
}
