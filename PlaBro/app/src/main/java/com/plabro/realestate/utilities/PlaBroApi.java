package com.plabro.realestate.utilities;

import android.content.Context;
import android.text.TextUtils;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.propFeeds.BaseFeed;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class PlaBroApi {
    public static final String APP_URL = "http://www.plabro.com/app";
    public static final String AUTH_KEY = "authkey";
    public static final String RT_KEY = "RT";
    public static final String IMEI = "imei";
    public static final String PHONE = "phone";
    public static final String SMSCODE = "smscode";
    public static final String APP_SOURCE = "appsource";
    public static final String APP_VERSION = "api_version";
    public static final String DEVICE_ID = "device_id";


    //sms
    public static final String RETRY = "retry";


    //google url expander
    public static final String URL_EXPANDER = "https://www.googleapis.com/urlshortener/v1/url";
    public static final String URL_EXPANDER_PARAM_SHORTURL = "shortUrl";

    //google url shortner
    public static final String URL_SHORTENER = "https://www.googleapis.com/urlshortener/v1/url";
    public static final String URL_SHORTENER_PARAM_LONGURL = "longUrl";


    //set preferences
    public static final String PLABRO_SET_PREF_POST_PARAM_NAME = "name";
    public static final String PLABRO_SET_PREF_POST_PARAM_LOCATION = "location";
    public static final String PLABRO_SET_PREF_POST_PARAM_AREA_RADIUS = "area_radius";
    public static final String PLABRO_SET_PREF_POST_PARAM_CITY = "city";
    public static final String PLABRO_SET_PREF_POST_PARAM_IMG = "img";
    public static final String PLABRO_SET_PREF_POST_PARAM_EMAIL = "email";
    public static final String PLABRO_SET_PREF_POST_PARAM_WEBSITE = "website";
    public static final String PLABRO_SET_PREF_POST_PARAM_ADDRESS = "address";
    public static final String PLABRO_SET_PREF_POST_PARAM_STATUS_MSG = "status_msg";
    public static final String PLABRO_SET_PREF_POST_PARAM_BUSINESS_NAME = "business_name";
    public static final String PLABRO_SET_PREF_POST_PARAM_CITY_ID = "city_id";
    public static final String PLABRO_SET_PREF_POST_PARAM_CITY_NAME = "city_name";
    public static final String PLABRO_SET_PREF_POST_PARAM_COORDINATES = "coordinates";


    //fetch feed details
    public static final String PLABRO_FEED_DETAILS_POST_PARAM_MESSAGE_ID = "message_id";

    //rating state
    public static final String PLABRO_RATING_STATE_POST_PARAM_RATING_STATE = "status_flag";


    //gcm
    public static final String GCM_REG_GET_PARAM_DEVICE_REG_ID = "devregid";
    public static final String GCM_REG_GET_PARAM_OSTTYPE = "ostype";
    public static final String GCM_REG_GET_PARAM_OSVERSION = "osversion";
    public static final String GCM_REG_GET_PARAM_PHONE_MODEL = "phonemodel";
    public static final String GCM_REG_GET_PARAM_PHONE_VERSION = "version";
    public static final String GCM_REG_GET_PARAM_PHONE_APP_VERSION = "appversion";


    //businesCardUPload
    public static final String BC_UPLOAD_PARAM_IMEI = "imei";
    public static final String BC_UPLOAD_PARAM_IMG = "img";
    public static final String BC_UPLOAD_PARAM_PHONE = "phone";


    //property feedsImg
    public static final String PLABRO_FEEDS_POST_PARAM_BOOKMARK_MESSAGE_ID = "message_id";


    //feedsImg for search hash
    public static final String PLABRO_SEARCH_HASH_FEEDS_POST_PARAM_HASHTAG = "hashtag";


    //location
    public static final String PLABRO_LOCATION_POST_PARAM_LOCATION = "location";
    public static final String PLABRO_LOCATION_POST_PARAM_COUNT = "count";

    //post shout
    public static final String PLABRO_SHOUT_POST_PARAM_MESSAGE = "message";
    public static final String PLABRO_SHOUT_POST_PARAM_POST_TYPE = "post_type";
    public static final String PLABRO_SHOUT_POST_PARAM_HASHTAGS = "hashtags";
    public static final String PLABRO_SHOUT_POST_PARAM_POST_ID = "postId";
    public static final String PLABRO_SHOUT_POST_PARAM_CITY_NAME = "city_name";
    public static final String PLABRO_SHOUT_POST_PARAM_CITY_ID = "city_id";

    //edit shout
    public static final String PLABRO_SHOUT_EDIT_PARAM_MSG = "msg";
    public static final String PLABRO_SHOUT_EDIT_PARAM_SHOUT_ID = "shout_id";
    public static final String PLABRO_SHOUT_EDIT_PARAM_AVAIL_REQ = "avail_req";
    public static final String PLABRO_SHOUT_EDIT_PARAM_SALE_RENT = "sale_rent";
    public static final String PLABRO_SHOUT_EDIT_PARAM_CITY_ID = "city_id";
    public static final String PLABRO_SHOUT_EDIT_PARAM_CITY_NAME = "city_name";

    //user suggestion
    public static final String PLABRO_USER_SUGGESTION_ACTION_TYPE = "type";
    public static final String PLABRO_USER_SUGGESTION_ACTION_ACTION = "action";
    public static final String PLABRO_USER_SUGGESTION_ACTION_ID = "id";


    //delete shout
    public static final String PLABRO_DELETE_SHOUT_PARAM_SHOUT_ID = "shout_id";


    //reply post
    public static final String PLABRO_WIFI_DATA_POST_PARAM_WIFIDATA = "wifidata";


    //reply post
    public static final String PLABRO_REPLY_POST_PARAM_MESSAGE = "message";
    public static final String PLABRO_REPLY_POST_PARAM_MESSAGE_ID = "message_id";
    public static final String PLABRO_REPLY_POST_PARAM_HASHTAGS = "hashtags";

    //locaiton auto complete
    public static final String PLABRO_LOC_AUTO_PARAM_KEY_STR = "key_str";


    //get profile
    public static final String PLABRO_GET_PROFILE_AUTO_PARAM_CONTACT_ID = "contact_id";
    public static final String PLABRO_GET_PROFILE_AUTO_PARAM_PHONE = "phone";
    public static final String PLABRO_GET_PROFILE_AUTO_PARAM_WEB_HASH = "hash_id";

    //recharge wallet
    public static final String PLABRO_RECHARGE_WALLET_AUTO_PARAM_ACTION = "action";
    public static final String PLABRO_RECHARGE_WALLET_AUTO_PARAM_STATE = "state";
    public static final String PLABRO_RECHARGE_WALLET_AUTO_PARAM_PAYMENT_MODE = "payment_mode";
    public static final String PLABRO_RECHARGE_WALLET_AUTO_PARAM_AMOUNT = "amount";
    public static final String PLABRO_RECHARGE_WALLET_AUTO_PARAM_PAYMENT_GETHASH = "get_service_hash";
    public static final String PLABRO_RECHARGE_WALLET_AUTO_PARAM_PAYMENT_ENV = "env";
    public static final String PLABRO_RECHARGE_WALLET_AUTO_PARAM_TXNID = "txnid";
    public static final String PLABRO_RECHARGE_WALLET_AUTO_PARAM_RESPONSE = "response";
    public static final String ISD_CODE = "isd_code";
    public static final String PLABRO_USER_SUGGESTION_ACTION_MSG_ID = "msg_id";

    public static interface WALLET_ACTIONS {
        String STATUS = "status";
        String RECHARGE = "recharge";
        String CREDIT = "credit";

    }

    //upload contacts
    public static final String PLABRO_UPLOAD_CONTACTS_POST_PARAM_CONTACTS_ARR = "contacts_arr";

    //get plabro friends

    //search
    public static final String PLABRO_SEARCH_POST_PARAM_Q_STR = "q_str";
    public static final String PLABRO_SEARCH_POST_PARAM_F_STR = "f_str";
    public static final String PLABRO_SEARCH_POST_PARAM_FILTER_JSON = "filter";
    public static final String PLABRO_SEARCH_POST_PARAM_SEL_FILTER_JSON = "search_filters";


    //exotel
    public static final String EXOTEL_CONNECT_CALL_POST_PARAM_FROM = "From";
    public static final String EXOTEL_CONNECT_CALL_POST_PARAM_CALLERID = "CallerId";
    public static final String EXOTEL_CONNECT_CALL_POST_PARAM_CALLTYPE = "CallType";
    public static final String EXOTEL_CONNECT_CALL_POST_PARAM_URL = "Url";


    // follow unfolow
    public static final String PLABRO_FOLLOW_UNFOLLOW_POST_PARAM_CONTACT_ID = "contact_id";

    //shout by others
    public static final String PLABRO_SHOUT_POST_PARAM_CONTACT_ID = "contact_id";

    //stats
    public static final String PLABRO_STATS_POST_PARAM_USER_ID = "user_id";
    public static final String PLABRO_STATS_POST_PARAM_MESSAGE_ID = "message_id";
    public static final String PLABRO_STATS_POST_PARAM_TYPE = "type";
    public static final String PLABRO_STATS_POST_PARAM_PREV_ACTIVITY = "prev_activity";
    public static final String PLABRO_STATS_POST_PARAM_NEXT_ACTIVITY = "next_activity";
    public static final String PLABRO_STATS_POST_PARAM_PHONE = "phone";

    public static final String PLABRO_UPDATEDAPPVERSION_POST_PARAM_APP_VERISON = "appversion";


    //grid records map
    public static final String PLABRO_MAP_POST_PARAM_LOC_LAT = "loc_lat";
    public static final String PLABRO_MAP_POST_PARAM_LOC_LONG = "loc_long";

    //key exchange
    public static final String PLABRO_KEY_EXCHANGE_POST_PARAM_IMEI = "imei";
    public static final String PLABRO_KEY_EXCHANGE_POST_PARAM_CLIENT_KEY = "clientkey";

    //aes key
    public static final String PLABRO_AES_KEY_POST_PARAM_IMEI = "imei";


    public static interface PARAMS {
        String AUTH_KEY = "authkey";
        String CONTACT_ID = "contact_id";
        String ENDORSEMENT_KEY = "endorsement_key";
    }

    public static interface PERMISSIONS {
        int READ_CALL_LOG = 0;
        int GET_TASKS = 1;
        int INTERNET = 2;
        int ACCESS_NETWORK_STATE = 3;
        int WRITE_EXTERNAL_STORAGE = 4;
        int READ_EXTERNAL_STORAGE = 5;
        int MANAGE_DOCUMENTS = 6;
        int ACCESS_COARSE_LOCATION = 7;
        int READ_LOGS = 8;
        int ACCESS_FINE_LOCATION = 9;
        int ACCESS_WIFI_STATE = 10;
        int READ_PHONE_STATE = 11;
        int CALL_PHONE = 12;
        int RECEIVE_SMS = 13;
        int READ_SMS = 14;
        int READ_CONTACTS = 15;
        int WRITE_CONTACTS = 16;
        int GET_ACCOUNTS = 17;
        int WAKE_LOCK = 18;
        int SYSTEM_ALERT_WINDOW = 19;


    }

    public static interface PERMISSIONS_MESSAGE {
        String READ_CALL_LOG = "";
        String GET_TASKS = "";
        String INTERNET = "Internet permission is required to get you latest feeds from brokers";
        String ACCESS_NETWORK_STATE = "";
        String WRITE_EXTERNAL_STORAGE = "Writing to external storage is required to create a local database for Plabro";
        String READ_EXTERNAL_STORAGE = "Reading from external storage is required to read from local database of Plabro";
        String MANAGE_DOCUMENTS = "";
        String ACCESS_COARSE_LOCATION = "";
        String READ_LOGS = "";
        String ACCESS_FINE_LOCATION = "";
        String ACCESS_WIFI_STATE = "";
        String READ_PHONE_STATE = "Phone permission is required for the calling functionality from Plabro";
        String CALL_PHONE = "";
        String RECEIVE_SMS = "Permission required to verify your registration automatically";
        String READ_SMS = "";
        String READ_CONTACTS = "Plabro would like to read your contacts to let you connect with your friends.";
        String WRITE_CONTACTS = "";
        String GET_ACCOUNTS = "";
        String WAKE_LOCK = "";
        String SYSTEM_ALERT_WINDOW = "";


    }

    public static interface RT {
        String JSON_SHARE = "JsonShare";
        String BOOKMARKSLIST = "BookmarksList";
        String LOCATION_AUTO = "LocAutoComplete";
        String KEYBOARDSUGGESTIONS = "KeyboardSuggestions";
        String UPDATE_ENDORSEMENT = "UpdateEndorsement";
        String RELATED_BROKERS = "RelatedBrokers";
        String BUILDER_FEED = "BuilderFeed";
        String BUILDER_COLLABORATE = "BuilderCollaborate";
        String BUILDER_UN_COLLABORATE = "BuilderUnCollaborate";
        String AUTHLOGIN = "AuthLogin";
        String CONFIRMATIONSMS = "ConfirmationSMS";
        String REGISTER_USER = "RegisterUser";
        String SETPREF = "SetPref";
        String SHOUTS = "Shouts";
        String TRANSACTIONS = "transactions";
        String DELETE_SHOUTS = "DeleteShout";
        String REPOST_SHOUTS = "RepostShout";
        String POSTSHOUTS = "PostShouts";
        String GETSHOUTS = "GetShouts";
        String SETBOOKMARK = "SetBookmark";
        String FEEDBOOKMARK = "FeedBookmark";
        String DELETEBOOKMARK = "DeleteBookmark";
        String GETPROFILE = "GetProfile";
        String PROPFEEDS = "PropFeeds";
        String SEARCH_HASH = "SearchHash";
        String SEARCH = "Search";
        String BUILDER_SHOUTS_SEARCH = "BuilderShoutsSearch";
        String UPLOAD_CONTACTS = "UploadContacts";
        String UPLOAD_CONTACTS_INC = "UploadContactsInc";
        String GET_PLABRO_FRIENDS = "GetPlabroFriends";
        String GET_NON_PLABRO_FRIENDS = "GetNonPlabroFriends";
        String FOLLOW = "Follow";
        String FOLLOWING_STATUS = "FollowingStatus";
        String UN_FOLLOW = "Unfollow";
        String REPLYPOST = "ReplyPost";
        String LOCATION_NEAR_BY = "LocNearby";
        String TNC = "TNC";
        String UPDATE_REGISTER_KEY = "UpdateRegisterKey";
        String GRID_RECORDS = "GridRecords";
        String FOLLOWER_LIST = "FollowerList";
        String FOLLOWING_LIST = "FollowingList";
        String STATS = "Stats";
        String UPDATEAPPVERSION = "UpdateAppVersion";
        String WIFI_DATA = "WifiData";
        String KEYEXCHANGE = "KeyExchange";
        String AESKEY = "AESKey";
        String ENCRYPTMESSAGE = "EncryptMessage";
        String DECRYPTMESSAGE = "DecryptMessage";
        String BUSINESSCARDUPLOAD = "BusinessCardUpload";
        String UPDATE_INVCODE = "UpdateINVCode";
        String GET_CITY_DROPDOWN = "GetCityDropDown";
        String GET_BUILDER_PROFILE = "GetBuilderProfile";
        String GET_TABS_INFO = "GetTabsInfo";
        String EDIT_SHOUT = "EditShout";
        String INVITED_USERS = "InvitedUsers";
        String GET_NOTIFICATIONS = "GetNotifications";
        String SHOUT_DETAILS = "ShoutDetails";
        String NOTIFY_INTERACTION = "notifyInteraction";
        String ENDORSEMENT_NOTIFICATION = "EndorsementNotification";
        String RATING_STATE = "RateCard";
        String SET_SHARE = "SetShare";
        String USER_CONTACTS_FEED = "UserContactsFeed";
        String SAVE_CALL_LOGS = "SaveCallLog";
        String GET_CALL_LOG = "GetCallLog";
        String APP_LAUNCH = "AppLaunch";
        String PAYMENT = "payment";
        String REQUEST_CALLBACK = "RequestCallback";
        String HANDLE_SERVICE = "HandleService";
        String NOTIFICATION_ACTIONS = "NotificationsAction";
        String GET_PERSONAL_PAGE = "GetPersonalPage";
        String USER_SUGGESTION_ACTION = "UserSuggestionAction";
        String GET_NOTIFICATIONS_BADGE = "GetNotificationsBadge";
        String GET_COUNTRY_CODES = "GetCountryCodes";
        String DIRECT_LISTING_FEEDS = "DirectListingFeed";
        String BLOG_FEED = "BlogFeed";
        String DIRECT_LISTING_DETAIL = "DirectListingDetail";
        String GET_LIVE_AUCTIONS = "GetLiveAuctions";
        String GET_AUCTION_DETAILS = "GetAuctionDetail";
        String MY_BIDS = "MyBids";
        String GET_BIDDERS = "GetBidders";
        String GET_BID_DETAILS = "GetBidDetail";
        String GET_AUCTION_META_INFO = "GetAuctionMetaInfo";
        String SEARCH_AUCTIONS = "SearchAuction";
        String UPDATE_QR_CODE = "UpdateQRCode";
        String NEWS_FEED = "NewsFeed";
        String GET_FEED_DATA = "GetFeedData";
        String GET_AUTH_QR_CODE = "GetAuthQRCode";
        String USER_COMMUNITIES = "UserCommunities";
        String UPDATE_COMMUNITY = "UpdateCommunity";
        String SUGG_COMMUNITY_USERS = "SuggCommunityUsers";
        String COMMUNITY_FEED = "CommunityFeeds";
        String COMMUNITY_INFO = "CommunityInfo";
    }


    public static final String DEBUG_URL = "http://dbg.plabro.com/cgi-bin/serverscript.py";
    public static final String EDIT_SHOUT_URL = "http://dbg.plabro.com:8800/cgi-bin/prannoy.py";
    public static final String CLIENT_SHORT_URL = "http://client.plabro.com/cgi-bin/serverscript.py";
    public static final String PRANNOY_URL = "http://client.plabro.com:8801/cgi-bin/pmittal.py";
    public static final String GAURAV_URL = "http://dbg.plabro.com:8803/cgi-bin/gaurav.py";

    public static final String PLABRO_GET_ALL_USERS = "GetAllUsers";
    public static String BASE_URL = "http://api.plabro.com/cgi-bin/serverscript.py";
    public static String NEWAPI_URL = "http://newapiusacc.plabro.com/cgi-bin/serverscript.py";
    public static String PAYMENT_URL_STAGING = "http://client.plabro.com/cgi-bin/serverscript.py";
    public static String PAYMENT_URL_PRODUCTION = "https://payment.plabro.com/cgi-bin/serverscript.py";
    public static String PAYMENT_URL_GAURAV = "https://client.plabro.com/cgi-bin/gaurav.py";
    public static String BIDS_URL = "http://www.plabro.co.in/userbiddings";
    public static String BIDS_EXIT_URL = "http://www.plabro.co.in/biddingexit";

    public static String getBaseUrl(Context mContext) {
        return PlaBroApi.getBaseUrl();
    }

    public static String getPaymentURL() {
//        return PAYMENT_URL_GAURAV;
        return PBPreferences.getData(PBPreferences.PAYMENT_URL, PAYMENT_URL_PRODUCTION);
    }

    public static String getBidsUrl() {
        return PBPreferences.getData(PBPreferences.MY_BIDS_URL, BIDS_URL);
    }

    public static String getBidsExitUrl() {
        return PBPreferences.getData(PBPreferences.MY_BIDS_EXIT_URL, BIDS_EXIT_URL);
    }

    public static String getBaseUrl() {
        boolean isDebugEnabled = PBPreferences.getIsDebugEnable();
        if (isDebugEnabled) {
            String devUrl = PBPreferences.getBaseUrl();
            if (!TextUtils.isEmpty(devUrl)) {
                return devUrl;//for full url change
                //BASE_URL = "http://" + devUrl + "/cgi-bin/serverscript.py"; // for base url change
            }
        }
        return BASE_URL;
    }
}
