package com.plabro.realestate.holders;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.activity.ListingDetail;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.BiddingFeed;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.models.propFeeds.listings.ListingFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 11/01/16.
 */
public class DirectInventoryHolder extends BaseFeedHolder {
    private TextView descTv, availReqTv, saleRentTv, textTv, timeTv, locationTv, titleTv, callButton, chatButton, buyTv, shareTv, copyTv;
    private ImageView bookmarkIcon, inventoryImage;
    AlertDialog dialog;

    public DirectInventoryHolder(View itemView) {
        super(itemView);
        descTv = findTV(R.id.tv_directCustomer);
        availReqTv = findTV(R.id.tv_req_avail);
        saleRentTv = findTV(R.id.tv_sale_rent);
        textTv = findTV(R.id.tv_detail_text);
        timeTv = findTV(R.id.tv_time);
        locationTv = findTV(R.id.tv_top_hash);
        titleTv = findTV(R.id.tv_title);
        callButton = findTV(R.id.tv_call);
        buyTv = findTV(R.id.tv_buy);
        chatButton = findTV(R.id.tv_chat);
        copyTv = findTV(R.id.tv_copy);
        shareTv = findTV(R.id.tv_share);
        bookmarkIcon = (ImageView) findView(R.id.iv_bookmark);
        inventoryImage = (ImageView) findView(R.id.profile_image);
    }

    private void setText(String text, TextView tv) {
        tv.setText(text);
    }

    @Override
    public void onBindViewHolder(final int position, BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        if (feed instanceof ListingFeed) {
            final ListingFeed f = (ListingFeed) feed;
            setText(f.getDesc(), descTv);
            setText(f.getText(), textTv);
            setText(f.getTime(), timeTv);
            setText(f.getTop_ht(), locationTv);
            setText(f.getTitle(), titleTv);
            if (!f.isPayment_status()) {
                setText("Buy", buyTv);
            } else {
                setText("View", buyTv);
            }
            buyTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onCardClick(f);
                }
            });

            ActivityUtils.loadImage(ctx, f.getImg(), inventoryImage);

            chatButton.setVisibility(View.VISIBLE);
            chatButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Feeds feedObject = f;

                    if (null != feedObject && !TextUtils.isEmpty(f.getCustomer_care_phone())) {

                    } else {
                        return;
                    }
                    feedObject.setProfile_phone(f.getCustomer_care_phone());

                    String phoneNumber = feedObject.getProfile_phone().toString();
                    String authorid = feedObject.getAuthorid() + "";
                    List<String> userPhones = new ArrayList<String>();
                    userPhones.add(phoneNumber);
                    long profileId = feedObject.getAuthorid();

                    Utility.userStats(ctx, "im");

                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("imTo", phoneNumber);
                    wrData.put("imFrom", PBPreferences.getPhone());
                    wrData.put("source", type);
                    wrData.put("ScreenName", TAG);
                    wrData.put("Action", "IM");
                    trackEvent(Analytics.CardActions.Chat, position, feedObject.get_id(), feedObject.getType(), wrData);


                    Intent intent = new Intent(ctx, XMPPChat.class);
                    String num = userPhones.get(0);
                    String displayName = ActivityUtils.getDisplayNameFromPhone(num);
                    if (displayName != null && !TextUtils.isEmpty(displayName)) {
                        intent.putExtra("name", displayName);

                    } else {
                        intent.putExtra("name", feedObject.getProfile_name());

                    }
                    intent.putExtra("img", feedObject.getProfile_img());
                    intent.putExtra("last_seen", feedObject.getTime());

                    intent.putExtra("phone_key", num);
                    intent.putExtra("userid", profileId + "");
                    intent.putExtra("feed_text", feedObject.getSumm()); //summary of hashtags

                    PlabroIntentService.startForNotifyingInteraction(wrData);
                    Bundle b = new Bundle();
                    b.putSerializable(Constants.BUNDLE_KEY_FEED, feedObject);
                    intent.putExtras(b);
                    ctx.startActivity(intent);
                }
            });
            if (f.isBookmark_enabled()) {
                bookmarkIcon.setVisibility(View.VISIBLE);
            } else {
                bookmarkIcon.setVisibility(View.GONE);
            }
            if (null != f.getAvail_req() && f.getAvail_req().length > 0) {
                availReqTv.setVisibility(View.VISIBLE);
                setText(f.getAvail_req()[0], availReqTv);
            } else {
                availReqTv.setVisibility(View.GONE);
            }

            if (null != f.getSale_rent() && f.getSale_rent().length > 0) {
                saleRentTv.setVisibility(View.VISIBLE);
                setText(f.getSale_rent()[0], saleRentTv);
            } else {
                saleRentTv.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onCardClick(f);
                }
            });
            callButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showCallDialog(f, position);
                }
            });
            shareTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PBSharingManager.shareByAll(f, ctx);
                    trackEvent(Analytics.CardActions.Shared, position, f.get_id(), f.getType(), null);
                }
            });
            copyTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    trackEvent(Analytics.CardActions.Copied, position, f.get_id(), f.getType(), null);
                    ListingFeed feedCopy = (ListingFeed) f;
                    String feedCopiedStr = feedCopy.getProfile_phone() + " : \n" + feedCopy.getText() + "\nBy: " + feedCopy.getProfile_name();
                    ActivityUtils.copyFeed(feedCopiedStr, ctx);
                }
            });
            setFeedText(f, position);
        }


    }

    private void launchCall(ListingFeed f, final int position, final String phone) {
        Feeds feeds = (Feeds) f;
        HashMap<String, Object> wrData = new HashMap<String, Object>();
        wrData.put("ScreenName", TAG);
        wrData.put("callFrom", PBPreferences.getPhone());
        wrData.put("source", f.getType());
        wrData.put(Analytics.Params.CARD_POSITION, "" + position);
        feeds.setProfile_phone(phone);
        ActivityUtils.initiateCallDialog(ctx, feeds, feeds.getPhonemap(), feeds.getProfile_phone(), wrData, feeds.getIsFreeCall(), feeds.getProfile_img(), feeds.getProfile_name());

    }

    private void showCallDialog(final ListingFeed f, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);

        View dialogView = LayoutInflater.from(ctx).inflate(R.layout.layout_dialog_call, null);
        TextView title = (TextView) dialogView.findViewById(R.id.tv_title);
        title.setText(f.getTitle() + "");
        TextView ownerOrPurchaseOption = (TextView) dialogView.findViewById(R.id.tv_purchase_or_owner);
        TextView callCustomerCare = (TextView) dialogView.findViewById(R.id.tv_customerCare);
        if (f.isPayment_status()) {
            ownerOrPurchaseOption.setText("Call " + f.getOwner_name());
        } else {
            ownerOrPurchaseOption.setText("Purchase Owner Details");
        }
        ownerOrPurchaseOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (f.isPayment_status()) {
                    launchCall(f, position, f.getProfile_phone());
                } else {
                    onCardClick(f);
                }
            }
        });
        callCustomerCare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                launchCall(f, position, f.getCustomer_care_phone());
            }
        });
        builder.setView(dialogView);
        builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();

    }

    private void setFeedText(ListingFeed feed, int position) {
        String text = feed.getText();
        int start = 0;
        int end = 250;
        if (feed.getSnippet().size() > 0) {
            start = Integer.parseInt(feed.getSnippet().get(0));
            end = Integer.parseInt(feed.getSnippet().get(1)) + 1;
        }

        int occurance = text.length() - text.replace("\n", "").length();


        if (start >= 0 && end < text.length()) {
            try {
                text = text.substring(start, end);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //adding span for hashtags in post
        SpannableString hashTagText = ActivityUtils.setHashTags(feed, text, start, end, ctx, textTv, getType(), position, false, getRefKey(), mActivity);//type and position send to track analytics

    }

    @Override
    public String getTAG() {
        return "Direct Inventory";
    }

    private void onCardClick(ListingFeed feed) {
        Bundle b = new Bundle();
        b.putSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE, feed);
        Intent i = new Intent(ctx, ListingDetail.class);
        i.putExtras(b);
        ctx.startActivity(i);
    }

}
