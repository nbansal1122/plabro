package com.plabro.realestate.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.payu.india.Interfaces.PaymentRelatedDetailsListener;
import com.payu.india.Model.MerchantWebService;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PayuResponse;
import com.payu.india.Model.PostData;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.india.PostParams.MerchantWebServicePostParams;
import com.payu.india.PostParams.PaymentPostParams;
import com.payu.india.Tasks.GetPaymentRelatedDetailsTask;
import com.payu.payuui.PaymentsActivity;
import com.plabro.realestate.R;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.fragments.PaymentDialogFragment;
import com.plabro.realestate.fragments.payu.CreditDebitCardFragment;
import com.plabro.realestate.fragments.payu.NetbankingFragment;
import com.plabro.realestate.fragments.payu.StoredCardFragment;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.payment.TransactionData;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.volley.PBResponse;

import java.util.HashMap;

/**
 * Created by nitin on 17/11/15.
 */
public class PaymentmodeActivity extends PlabroBaseActivity implements PaymentRelatedDetailsListener, VolleyListener {
    PayuResponse mPayuResponse;
    Intent mIntent;
    PayuConfig payuConfig;
    private int paymentOptionSelected = 0;
    private String transactionStatus;

    //    PaymentDefaultParams mPaymentDefaultParams;
    PaymentParams mPaymentParams;
    PayuHashes mPayUHashes;
    private CreditDebitCardFragment creditDebitCardFragment;
    private StoredCardFragment storedCardFragment;
    private NetbankingFragment netbankingFragment;
    private static final int SELECTED_DRAWABLE = R.drawable.ic_invitation_selected;
    private static final int UNSELECTED_DRAWABLE = R.drawable.ic_invitation_unselected;
    private String paymentMode;
    private String response;


    Bundle bundle;


    @Override
    protected String getTag() {
        return "PaymentmodeActivity";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_payment_mode);
        Analytics.trackScreen(Analytics.Wallet.AddMoney);
        inflateToolbar();
        getSupportActionBar().setTitle("Recharge Wallet");

        // lets collect the details from bundle to fetch the payment related details for a merchant
        bundle = getIntent().getExtras();


        payuConfig = bundle.getParcelable(PayuConstants.PAYU_CONFIG);
        payuConfig = null != payuConfig ? payuConfig : new PayuConfig();

        // TODO add null pointer check here
//        mPaymentDefaultParams = bundle.getParcelable(PayuConstants.PAYMENT_DEFAULT_PARAMS);
        mPaymentParams = bundle.getParcelable(PayuConstants.PAYMENT_PARAMS); // Todo change the name to PAYMENT_PARAMS
        mPayUHashes = bundle.getParcelable(PayuConstants.PAYU_HASHES);

        MerchantWebService merchantWebService = new MerchantWebService();
        merchantWebService.setKey(mPaymentParams.getKey());
        merchantWebService.setCommand(PayuConstants.PAYMENT_RELATED_DETAILS_FOR_MOBILE_SDK);
        merchantWebService.setVar1(mPaymentParams.getUserCredentials() == null ? "default" : mPaymentParams.getUserCredentials());

        Log.d(TAG, mPaymentParams.getUserCredentials() + "");
        // hash we have to generate


        merchantWebService.setHash(mPayUHashes.getPaymentRelatedDetailsForMobileSdkHash());

//        PostData postData = new PostParams(merchantWebService).getPostParams();

        // Dont fetch the data if calling activity is PaymentActivity

        // fetching for the first time.
        if (null == savedInstanceState) { // dont fetch the data if its been called from payment activity.
            PostData postData = new MerchantWebServicePostParams(merchantWebService).getMerchantWebServicePostParams();
            if (postData.getCode() == PayuErrors.NO_ERROR) {
                // ok we got the post params, let make an api call to payu to fetch the payment related details
                payuConfig.setData(postData.getResult());
                Log.d(TAG, "Data is :" + postData.getResult());
                // lets set the visibility of progress bar
                GetPaymentRelatedDetailsTask paymentRelatedDetailsForMobileSdkTask = new GetPaymentRelatedDetailsTask(this);
                paymentRelatedDetailsForMobileSdkTask.execute(payuConfig);
            } else {
                Log.d(TAG, "Error:" + postData.getResult());
                Toast.makeText(this, postData.getResult(), Toast.LENGTH_LONG).show();
                // close the progress bar
                findViewById(com.payu.payuui.R.id.progress_bar).setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_modeType_credit_debit:
                setLeftDrawable(view.getId(), true);
                setLeftDrawable(R.id.tv_modeType_netbanking, false);
                setLeftDrawable(R.id.tv_modeType_savedCard, false);
                showViews(R.id.frame_credit_debit);
                if (creditDebitCardFragment == null) {
                    addCreditDebitFragment();
                }
                hideViews(R.id.frame_saved_cards, R.id.frame_netbanking);
                break;
            case R.id.tv_modeType_netbanking:
                setLeftDrawable(view.getId(), true);
                setLeftDrawable(R.id.tv_modeType_credit_debit, false);
                setLeftDrawable(R.id.tv_modeType_savedCard, false);
                hideViews(R.id.frame_saved_cards, R.id.frame_credit_debit);
                showViews(R.id.frame_netbanking);
                if (netbankingFragment == null) {
                    addNetBankingFragment();
                }
                break;
            case R.id.tv_modeType_savedCard:
                setLeftDrawable(view.getId(), true);
                setLeftDrawable(R.id.tv_modeType_credit_debit, false);
                setLeftDrawable(R.id.tv_modeType_netbanking, false);
                hideViews(R.id.frame_credit_debit, R.id.frame_netbanking);
                showViews(R.id.frame_saved_cards);
                if (storedCardFragment == null) {
                    addStoredCardFragment();
                }
                break;
            case R.id.tv_addAmount:
                if (paymentOptionSelected == 0) {
                    showToast(getString(R.string.please_select_a_payment_option));
                } else {
                    launchPayment();
                }
                break;
        }
    }

    private void addStoredCardFragment() {
        getSupportFragmentManager().beginTransaction().add(R.id.frame_saved_cards, getStoredCardFragment()).commit();
    }

    private StoredCardFragment getStoredCardFragment() {
        storedCardFragment = new StoredCardFragment();
        Bundle b = new Bundle();
        b.putParcelableArrayList(PayuConstants.STORED_CARD, mPayuResponse.getStoredCards());
        b.putParcelable(PayuConstants.PAYU_HASHES, mPayUHashes);
        b.putParcelable(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
        payuConfig.setData(null);
        b.putParcelable(PayuConstants.PAYU_CONFIG, payuConfig);

        // salt
        if (bundle.getString(PayuConstants.SALT) != null)
            b.putString(PayuConstants.SALT, bundle.getString(PayuConstants.SALT));
        b.putParcelableArrayList(PayuConstants.CREDITCARD, mPayuResponse.getCreditCard());
        b.putParcelableArrayList(PayuConstants.DEBITCARD, mPayuResponse.getDebitCard());
        storedCardFragment.setArguments(b);
        return storedCardFragment;
    }

    private void launchPayment() {
        switch (paymentOptionSelected) {
            case R.id.tv_modeType_credit_debit:
                creditDebitCardFragment.sendPayment();
                paymentMode = Constants.PaymentMode.DEBIT;
                break;
            case R.id.tv_modeType_netbanking:
                netbankingFragment.doPayment();
                paymentMode = Constants.PaymentMode.NETBANKING;
                break;
            case R.id.tv_modeType_savedCard:
                storedCardFragment.doPayment();
                paymentMode = Constants.PaymentMode.DEBIT;
                break;
        }
    }

    private void setLeftDrawable(int id, boolean isSelected) {
        TextView view = (TextView) findViewById(id);
        if (isSelected) {
            paymentOptionSelected = id;
            view.setCompoundDrawablesWithIntrinsicBounds(SELECTED_DRAWABLE, 0, 0, 0);
        } else {
            view.setCompoundDrawablesWithIntrinsicBounds(UNSELECTED_DRAWABLE, 0, 0, 0);
        }
    }

    @Override
    public void findViewById() {

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    private void launchPayumoney() {
        PostData postData;

        // lets try to get the post params
        mPaymentParams.setHash(mPayUHashes.getPaymentHash());

//        postData = new PayuWalletPostParams(mPaymentDefaultParams).getPayuWalletPostParams();
        postData = new PaymentPostParams(mPaymentParams, PayuConstants.PAYU_MONEY).getPaymentPostParams();
        if (postData.getCode() == PayuErrors.NO_ERROR) {
            // launch webview
            payuConfig.setData(postData.getResult());
            Intent intent = new Intent(this, PaymentsActivity.class);
            intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
            startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
        } else {
            Toast.makeText(this, postData.getResult(), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(transactionStatus)) {
            if ("completed".equalsIgnoreCase(transactionStatus)) {
                showPaymentSuccessDialog();
                sendTransactionStatus(response, transactionStatus);
            } else if ("failed".equalsIgnoreCase(transactionStatus)) {
                showPaymentErrorDialog();
                sendTransactionStatus(response, transactionStatus);
            }
            transactionStatus = "";
        }
    }

    private void launchActivity(Intent intent) {
        intent.putExtra(PayuConstants.PAYU_HASHES, mPayUHashes);
        intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
        payuConfig.setData(null);
        intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);

        // salt
        if (bundle.getString(PayuConstants.SALT) != null)
            intent.putExtra(PayuConstants.SALT, bundle.getString(PayuConstants.SALT));

        startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
    }


    @Override
    public void onPaymentRelatedDetailsResponse(PayuResponse payuResponse) {
        mPayuResponse = payuResponse;
        findViewById(R.id.progress_bar).setVisibility(View.GONE);
        if (payuResponse.isResponseAvailable() && payuResponse.getResponseStatus().getCode() == PayuErrors.NO_ERROR) {
            // ok we are good to go
            showViews(R.id.ll_payment_options);
            setClickListeners(R.id.tv_addAmount, R.id.tv_modeType_credit_debit, R.id.tv_modeType_savedCard, R.id.tv_modeType_netbanking);
            setText(R.id.tv_addAmount, getString(R.string.addRs, Double.parseDouble(mPaymentParams.getAmount())));
//            Toast.makeText(this, payuResponse.getResponseStatus().getResult(), Toast.LENGTH_LONG).show();
            if (payuResponse.isStoredCardsAvailable()) {
            } else {
                hideViews(R.id.tv_modeType_savedCard);
            }
            if (payuResponse.isNetBanksAvailable()) { // okay we have net banks now.
            } else {
                hideViews(R.id.tv_modeType_netbanking);
            }
            if (payuResponse.isCreditCardAvailable() || payuResponse.isDebitCardAvailable()) {
            } else {
                hideViews(R.id.tv_modeType_credit_debit);
            }
        } else {
            Log.d(TAG, payuResponse.getResponseStatus().getCode() + ", " + payuResponse.getResponseStatus().getStatus());
            Toast.makeText(this, "Something went wrong : " + payuResponse.getResponseStatus().getResult(), Toast.LENGTH_LONG).show();
        }

        // no mater what response i get just show this button, so that we can go further.
    }

    private void addCreditDebitFragment() {
        getSupportFragmentManager().beginTransaction().add(R.id.frame_credit_debit, getCreditDebitCardFragment()).commit();
    }

    private void addNetBankingFragment() {
        getSupportFragmentManager().beginTransaction().add(R.id.frame_netbanking, getNetBankingFragment()).commit();
    }

    private CreditDebitCardFragment getCreditDebitCardFragment() {
        if (creditDebitCardFragment == null) {
            creditDebitCardFragment = new CreditDebitCardFragment();
        }
        Bundle b = new Bundle();
        b.putParcelable(PayuConstants.PAYU_HASHES, mPayUHashes);
        b.putParcelable(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
        payuConfig.setData(null);
        b.putParcelable(PayuConstants.PAYU_CONFIG, payuConfig);

        // salt
        if (bundle.getString(PayuConstants.SALT) != null)
            b.putString(PayuConstants.SALT, bundle.getString(PayuConstants.SALT));
        b.putParcelableArrayList(PayuConstants.CREDITCARD, mPayuResponse.getCreditCard());
        b.putParcelableArrayList(PayuConstants.DEBITCARD, mPayuResponse.getDebitCard());
        creditDebitCardFragment.setArguments(b);
        return creditDebitCardFragment;
    }

    private NetbankingFragment getNetBankingFragment() {
        netbankingFragment = new NetbankingFragment();
        Bundle b = new Bundle();
        b.putParcelable(PayuConstants.PAYU_HASHES, mPayUHashes);
        b.putParcelable(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
        payuConfig.setData(null);
        b.putParcelable(PayuConstants.PAYU_CONFIG, payuConfig);

        // salt
        if (bundle.getString(PayuConstants.SALT) != null)
            b.putString(PayuConstants.SALT, bundle.getString(PayuConstants.SALT));
        b.putParcelableArrayList(PayuConstants.NETBANKING, mPayuResponse.getNetBanks());
        netbankingFragment.setArguments(b);
        return netbankingFragment;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "On Activity Result ");
        Log.d(TAG, "Result:" + resultCode);
        if (data != null) {
            Log.d(TAG, "data is not null");
            Bundle b = data.getExtras();
            if (null != b) {
                Log.d(TAG, "Bundle is not null");
                String response = b.getString("result");
                if (resultCode == RESULT_OK) {
                    transactionStatus = "completed";
                    this.response = response;
                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Analytics.Params.PayuResponse, response);
                    Analytics.trackWalletEvent(Analytics.Actions.Purchased, map);
                } else {
                    transactionStatus = "failed";
                    this.response = response;
                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Analytics.Params.PayuResponse, response);
                    Analytics.trackWalletEvent(Analytics.Actions.Failed, map);
                }

            }
        }

    }

    private void showPaymentSuccessDialog() {
        PaymentDialogFragment f = PaymentDialogFragment.getInstance(getString(R.string.payment_success), getString(R.string.msg_payment_success, mPaymentParams.getAmount()), getString(R.string.ok), getString(R.string.card_cancel), false, new PaymentDialogFragment.DialogListener() {
            @Override
            public void onOKClicked() {
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onCancelClicked() {
                setResult(RESULT_OK);
                finish();
            }
        });
        f.show(getSupportFragmentManager(), "success");
    }

    private void showPaymentErrorDialog() {
        PaymentDialogFragment f = PaymentDialogFragment.getInstance(getString(R.string.payment_error), getString(R.string.msg_payment_error), getString(R.string.payment_retry), getString(R.string.ok), true, new PaymentDialogFragment.DialogListener() {
            @Override
            public void onOKClicked() {
                launchPayment();
            }

            @Override
            public void onCancelClicked() {
                setResult(RESULT_OK);
                finish();
            }
        });
        f.show(getSupportFragmentManager(), "success");
    }

    private void sendTransactionStatus(String response, String state) {
        TransactionData t = new TransactionData();
        t.setAction("credit");
        t.setAmount(mPaymentParams.getAmount());
        t.setTxnId(mPaymentParams.getTxnId());
        t.setResponse(response);
        t.setPaymentMode(paymentMode);
        t.setStatus(state);
        t.save();
        PlabroIntentService.sendTransaction(this);
    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        Log.d(TAG, "Success Response");
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        Log.d(TAG, "Failure Response");
    }
}
