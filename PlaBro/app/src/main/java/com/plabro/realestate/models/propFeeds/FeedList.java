package com.plabro.realestate.models.propFeeds;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 01/06/15.
 */
public class FeedList {

    List<Feeds> feedsList = new ArrayList<Feeds>();

    public List<Feeds> getFeedsList() {
        return feedsList;
    }

    public void setFeedsList(List<Feeds> feedsList) {
        this.feedsList = feedsList;
    }
}
