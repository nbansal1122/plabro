package com.plabro.realestate.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.auctions.AuctionDetailResponse;
import com.plabro.realestate.models.auctions.AuctionHeader;
import com.plabro.realestate.models.auctions.AuctionInfo;
import com.plabro.realestate.models.auctions.Page_action_form;
import com.plabro.realestate.models.auctions.Section;
import com.plabro.realestate.models.auctions.SectionListData;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.receivers.PBLocalReceiver;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 25/01/16.
 */
public class AuctionDetails extends PlabroBaseActivity {

    private String auctionId;
    private AuctionDetailResponse details;
    private LinearLayout contentDetailsLayout;
    private String auctionTitle;
    private boolean isMyBid;

    @Override
    protected String getTag() {
        return "Interest Details";
    }

    @Override
    protected void loadBundleData(Bundle b) {
        super.loadBundleData(b);
        auctionId = b.getString(Constants.BUNDLE_KEYS.AUCTION_ID);
        auctionTitle = b.getString(Constants.BUNDLE_KEYS.TITLE);
        isMyBid = b.getBoolean(Constants.BUNDLE_KEYS.IS_MY_BID);
    }


    private void getAuctionDetails() {
        ParamObject obj = new ParamObject();
        obj.setClassType(AuctionDetailResponse.class);
        HashMap<String, String> params = new HashMap<>();
        // For testing purpose
//        auctionId = "46";
        //-----
        params.put(Constants.BUNDLE_KEYS.AUCTION_ID, auctionId);
        if (isMyBid) {
            params.put(Constants.BUNDLE_KEYS.BID_ID, auctionId);
            params = Utility.getInitialParams(PlaBroApi.RT.GET_BID_DETAILS, params);
        } else {
            params.put(Constants.BUNDLE_KEYS.AUCTION_ID, auctionId);
            params = Utility.getInitialParams(PlaBroApi.RT.GET_AUCTION_DETAILS, params);
        }
        obj.setParams(params);
        showDialog();
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                dismissDialog();
                AuctionDetailResponse resp = (AuctionDetailResponse) response.getResponse();
                if (null != resp && null != resp.getOutput_params() && null != resp.getOutput_params().getData()) {
                    initViews(resp);
                    setClickListeners(R.id.btn_bid_now);
                } else {
                    showToast("Unable to find details");
                    finish();
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                dismissDialog();
            }
        });
    }

    private void initViews(AuctionDetailResponse response) {
        if(isMyBid){
            Analytics.trackScreen(Analytics.Requirements.BidDetails);
        }else{
            Analytics.trackScreen(Analytics.Requirements.RequirementDetails);
        }
        details = response;
        contentDetailsLayout.removeAllViewsInLayout();
        setHeader();
        setAuctionInfo();
        List<Section> sections = details.getOutput_params().getData().getSections();
        for (Section s : sections) {
            addSection(s);
        }
    }

    private void setHeader() {
        if (details.getOutput_params() == null || details.getOutput_params().getData() == null) {
            finish();
            return;
        }
        AuctionHeader auctionHeader = details.getOutput_params().getData().getAuction_header();
        if (null == auctionHeader) return;
        View contentSection = LayoutInflater.from(this).inflate(R.layout.card_auction_detail, null);
        LinearLayout contentLayout = (LinearLayout) contentSection.findViewById(R.id.ll_content);
        contentSection.findViewById(R.id.tv_header).setVisibility(View.GONE);
        contentSection.findViewById(R.id.tv_footer).setVisibility(View.GONE);
        contentSection.findViewById(R.id.sep_footer).setVisibility(View.GONE);

        LinearLayout row = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.iv_auction_detail_item, null);
        ImageView img = (ImageView) row.findViewById(R.id.iv_auction_detail);
        TextView tv = (TextView) row.findViewById(R.id.tv_bid_time);
        if (!TextUtils.isEmpty(auctionHeader.getImg_footer())) {
            tv.setVisibility(View.VISIBLE);
            tv.setText(auctionHeader.getImg_footer());
        }
        ActivityUtils.loadImage(this, auctionHeader.getImg_url(), img, R.drawable.img_pattern_default);
        contentLayout.addView(row);
        contentDetailsLayout.addView(contentSection);
    }

    private void setAuctionInfo() {
        AuctionInfo info = details.getOutput_params().getData().getAuction_info();
        if (null == info) return;

        View contentSection = LayoutInflater.from(this).inflate(R.layout.card_auction_info_header, null);
        TextView auctionTitle = (TextView) contentSection.findViewById(R.id.tv_header);
        TextView auctionSummary = (TextView) contentSection.findViewById(R.id.tv_auction_detail_item);
        TextView auctionStatus = (TextView) contentSection.findViewById(R.id.tv_auction_status);
        TextView auctionTime = (TextView) contentSection.findViewById(R.id.tv_footer);
        TextView auctionParticipants = (TextView) contentSection.findViewById(R.id.tv_footer_participant);
        auctionStatus.setText(info.getAuction_status());
        if ("closed".equalsIgnoreCase(info.getAuction_status())) {
            auctionStatus.setBackgroundResource(R.drawable.bg_status_close);
        } else {
            auctionStatus.setBackgroundResource(R.drawable.rect_bg_btn_bid_status);
        }
        auctionTitle.setText(info.getTitle());
        auctionSummary.setText(info.getAuction_summary());

        auctionTime.setText(info.getAuction_time());
        if (!TextUtils.isEmpty(info.getAuction_participants())) {
            auctionParticipants.setVisibility(View.VISIBLE);
            auctionParticipants.setText(info.getAuction_participants());
        }

        contentDetailsLayout.addView(contentSection);

    }

    private void addSection(Section section) {
        View contentSection = LayoutInflater.from(this).inflate(R.layout.card_auction_detail, null);
        LinearLayout contentLayout = (LinearLayout) contentSection.findViewById(R.id.ll_content);
        if (TextUtils.isEmpty(section.getSection_title())) {
            contentSection.findViewById(R.id.tv_header).setVisibility(View.GONE);
//            contentSection.findViewById(R.id.sep_header).setVisibility(View.GONE);
        } else {
            contentSection.findViewById(R.id.tv_header).setVisibility(View.VISIBLE);
//            contentSection.findViewById(R.id.sep_header).setVisibility(View.VISIBLE);
            TextView header = (TextView) contentSection.findViewById(R.id.tv_header);
            header.setText(section.getSection_title());
        }
        if (TextUtils.isEmpty(section.getSection_footer())) {
            contentSection.findViewById(R.id.tv_footer).setVisibility(View.GONE);
            contentSection.findViewById(R.id.sep_footer).setVisibility(View.GONE);
        } else {
            contentSection.findViewById(R.id.tv_footer).setVisibility(View.VISIBLE);
            contentSection.findViewById(R.id.sep_footer).setVisibility(View.VISIBLE);
            TextView footer = (TextView) contentSection.findViewById(R.id.tv_footer);
            footer.setText(section.getSection_footer());
        }
        switch (section.getSection_type()) {
            case "text":
                addTextSection(section, contentLayout);
                break;
            case "image":
                addImageSection(section, contentLayout);
                break;
            case "list":
            case "text_list":
                addListSection(section, contentLayout);
                break;
        }
        contentDetailsLayout.addView(contentSection);
    }

    private void addImageSection(Section s, LinearLayout contentLayout) {
        LinearLayout row = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.iv_auction_detail_item, null);
        ImageView img = (ImageView) row.findViewById(R.id.iv_auction_detail);
        ActivityUtils.loadImage(this, s.getImg_url(), img, 0);
        contentLayout.addView(row);
    }

    private void addTextSection(Section s, LinearLayout contentLayout) {
        TextView text = (TextView) LayoutInflater.from(this).inflate(R.layout.tv_auction_detail_item, null);
        text.setText(s.getSection_data());
        contentLayout.addView(text);
    }

    private void addListSection(Section s, LinearLayout contentLayout) {
        List<SectionListData> sectionList = s.getSection_list_data();
        if (null != sectionList) {
            for (SectionListData data : sectionList) {
                View row = LayoutInflater.from(this).inflate(R.layout.view_list_auction_data, null);
                TextView title = (TextView) row.findViewById(R.id.tv_title);
                TextView text = (TextView) row.findViewById(R.id.tv_text);
                title.setText(data.getField_title());
                text.setText(data.getField_text());
                contentLayout.addView(row);
            }
        }
    }

    @Override
    public void findViewById() {
        contentDetailsLayout = (LinearLayout) findViewById(R.id.ll_auction_content);
        inflateToolbar();
        setTitle(auctionTitle);
        getAuctionDetails();
        if (isMyBid) {
            setText(R.id.btn_bid_now, "VIEW YOUR INTEREST DETAILS");
        } else {
        }
    }


    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btn_bid_now:
                if (!isMyBid) {
                    ArrayList<Page_action_form> formFields = (ArrayList<Page_action_form>) details.getOutput_params().getData().getPage_action_form();
                    Bundle b = new Bundle();
                    b.putSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE, formFields);
                    b.putString(Constants.BUNDLE_KEYS.AUCTION_ID, auctionId);
                    b.putString(Constants.BUNDLE_KEYS.TITLE, auctionTitle);
                    b.putString(Constants.BUNDLE_KEYS.INPUT_URL, details.getOutput_params().getData().getPage_action_url());
                    Intent i = new Intent(this, SubmitAuctions.class);
                    i.putExtras(b);
                    startActivityForResult(i, 1);
                } else {
                    ArrayList<Page_action_form> formFields = (ArrayList<Page_action_form>) details.getOutput_params().getData().getPage_action_form();
                    Bundle b = new Bundle();
                    b.putSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE, formFields);
                    Intent i = new Intent(this, BidForm.class);
                    i.putExtras(b);
                    startActivity(i);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) return;
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(PBLocalReceiver.PBActionListener.ACTION_REFRESH_AUCTIONS);
        ActivityUtils.sendLocalBroadcast(this, broadcastIntent);
        finish();
    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_auction_details;
    }

    @Override
    public void reloadRequest() {

    }
}
