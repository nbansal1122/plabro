package com.plabro.realestate.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.fragments.PaymentDialogFragment;
import com.plabro.realestate.fragments.ViewPagerFragment;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.payment.PaymentResponse;
import com.plabro.realestate.models.propFeeds.listings.InfoList;
import com.plabro.realestate.models.propFeeds.listings.Info;
import com.plabro.realestate.models.propFeeds.listings.ListingFeed;
import com.plabro.realestate.models.services.ListingDetailResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.receivers.PBLocalReceiver;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 30/12/15.
 */
public class ListingDetail extends PlabroBaseActivity {

    private LinearLayout listingDetails;
    private ListingFeed model;

    @Override
    protected String getTag() {
        return "Listing Details";
    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_listing_detail;
    }

    private void initSlider(ListingFeed model) {
        if (model.getDetailed_view().getImage_urls().size() > 0) {
            FrameLayout layout = (FrameLayout) findViewById(R.id.sliderFrame);
            layout.removeAllViewsInLayout();
            getSupportFragmentManager().beginTransaction().replace(R.id.sliderFrame, ViewPagerFragment.getInstance(model)).commitAllowingStateLoss();
        }
    }

    private void addListingInfo(List<InfoList> infoList) {

        for (InfoList details : infoList) {
            String header = details.getHeader();
            List<Info> infos = details.getInfo();
            View row = getDetailView(header, infos);
            listingDetails.addView(row);
        }
    }

    private View getDetailView(String header, List<Info> infoList) {
        View row = LayoutInflater.from(this).inflate(R.layout.view_listing_info, null);
        TextView tv = (TextView) row.findViewById(R.id.tv_basic_sep);
        tv.setText(header);
        LinearLayout layout = (LinearLayout) row.findViewById(R.id.ll_basic);
        for (Info info : infoList) {
            addInfo(info, layout);
        }
        return row;
    }


    private void addInfo(Info info, LinearLayout layout) {
        String value = info.getValue();
        View row = getRowView(info.getKey(), value);
        layout.addView(row);
    }


    private View getRowView(String key, String value) {
        View v = LayoutInflater.from(this).inflate(R.layout.view_tv_key_value, null);
        TextView keyTv = (TextView) v.findViewById(R.id.tv_key);
        TextView valueTv = (TextView) v.findViewById(R.id.tv_value);
        keyTv.setText(key);
        valueTv.setText(value);
        return v;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.footer:
                sendPaymentRT(Constants.ACTION_SERVICE_COST);
                break;
        }
    }


    @Override
    public void findViewById() {
        inflateToolbar();
        setTitle("Listing");
        Bundle b = getIntent().getExtras();
        listingDetails = (LinearLayout) findViewById(R.id.ll_listing_details);
        model = (ListingFeed) b.getSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE);
        getListingDetail(model.get_id());
//

    }

    private void initViews() {
        Analytics.trackScreen(Analytics.DirectListing.ListingDetails);
        listingDetails.removeAllViewsInLayout();
        addListingInfo(model.getDetailed_view().getInfo_list());
        initSlider(model);
        if (!model.isPayment_status()) {
            setClickListeners(R.id.footer);
        } else {
            hideViews(R.id.footer);
        }
    }

    private void getListingDetail(String id) {
        ParamObject obj = new ParamObject();
        obj.setClassType(ListingDetailResponse.class);
        HashMap<String, String> params = new HashMap<>();
        params.put("feature_offering_id", id);
        params = Utility.getInitialParams(PlaBroApi.RT.DIRECT_LISTING_DETAIL, params);
        obj.setParams(params);
        showDialog();
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                dismissDialog();
                Log.d(TAG, "on success:" + response.getResponse());
                ListingDetailResponse resp = (ListingDetailResponse) response.getResponse();
                if (null != resp.getOutput_params() && null != resp.getOutput_params().getData()) {
                    model = resp.getOutput_params().getData();
                    initViews();
                } else {
                    showToast("Listing Detail is not available currently.");
                    finish();
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                dismissDialog();
                Log.d(TAG, "on Failure:" + response.getCustomException().getMessage());
                showToast("Listing Detail is not available currently.");
                finish();
            }
        });
    }

    @Override
    public void reloadRequest() {

    }


    private void sendPaymentRT(final String action) {
        ParamObject obj = new ParamObject();
        obj.setTaskCode(Constants.TASK_CODES.SERVICE_PAYMENT);
        HashMap<String, String> params = new HashMap<>();
        params.put("action", action);
        params.put("state", "initiated");
        params.put("service_code", "direct_listing_service");
        params.put("payment_mode", "wallet");
        params.put("feature_offering_id", model.get_id() + "");
        params = Utility.getInitialParams(PlaBroApi.RT.PAYMENT, params);
        obj.setRequestMethod(RequestMethod.POST);
        obj.setClassType(PaymentResponse.class);
        obj.setParams(params);
        showDialog();
        HashMap<String, Object> data = new HashMap<>();
        data.putAll(params);
        Analytics.trackServiceEvent(Analytics.Actions.ServicePurchased, data);
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                dismissDialog();
                Log.d(TAG, "Response is :" + response.getResponse());
                PaymentResponse paymentResponse = (PaymentResponse) response.getResponse();
                if (paymentResponse != null) {

                    float balance = paymentResponse.getOutput_params().getData().getBalance();
                    float amount = paymentResponse.getOutput_params().getData().getAmount();
                    amount = (float) Math.ceil(amount);
                    if (amount > balance) {
                        showPaymentDialog(paymentResponse.getOutput_params().getData().getInfo(), (float) Math.ceil(amount - balance), balance);
                    } else {
                        if ("debit".equals(action)) {
                            getListingDetail(model.get_id());
                            ActivityUtils.sendLocalBroadcast(ListingDetail.this, PBLocalReceiver.PBActionListener.ACTION_REFRESH_LISTING);
                        } else {
                            showDeductionDialog(model.getDesc(), amount, balance);
                        }
                    }
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                dismissDialog();
            }
        });

    }

    private void showDeductionDialog(String msg, final float amount, final float balance) {
        String title = String.format("Your current balance is Rs %s\n\nAn amount of Rs. %s will be deducted from your balance amount", String.valueOf(balance), String.valueOf(amount));
        PaymentDialogFragment f = PaymentDialogFragment.getInstance(msg, title, "OK", "CANCEL", true, new PaymentDialogFragment.DialogListener() {
            @Override
            public void onOKClicked() {
                sendPaymentRT("debit");
            }

            @Override
            public void onCancelClicked() {

            }
        });
        f.show(getSupportFragmentManager(), "Payment");
    }

    private void showPaymentDialog(String msg, final float rechargeAmount, final float balance) {
        PaymentDialogFragment f = PaymentDialogFragment.getInstance(msg, String.format("Recharge your wallet with amount of Rs %s to avail %s", String.valueOf(rechargeAmount),
                model.getTitle()), "OK", "CANCEL", true, new PaymentDialogFragment.DialogListener() {
            @Override
            public void onOKClicked() {
                RechargeWallet.startRechargeWallet(ListingDetail.this, String.valueOf(balance), rechargeAmount);
            }

            @Override
            public void onCancelClicked() {

            }
        });
        f.show(getSupportFragmentManager(), "Payment");
    }

}
