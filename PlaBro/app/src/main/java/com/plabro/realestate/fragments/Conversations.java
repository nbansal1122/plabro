package com.plabro.realestate.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.plabro.realestate.R;

public class Conversations extends MasterFragment {

    private static Context mContext;
    private static Activity mActivity;
    private TextView chats, bookmarks, shouts;
    View chat_frag, bookmark_frag, shout_frag;
    FrameLayout fragment_container;
    android.support.v4.app.FragmentManager fm;
    private ShoutsFragment sf;
    private BookmarksFragment bf;


    @Override
    public String getTAG() {
        return "Conversations";
    }

    @Override
    protected void findViewById() {

        mContext = getActivity().getApplicationContext();
        mActivity = getActivity();

        chats = (TextView) findView(R.id.chats);
        bookmarks = (TextView) findView(R.id.bookmarks);
        shouts = (TextView) findView(R.id.shouts);


        chat_frag = findView(R.id.chat_frag);
        bookmark_frag = findView(R.id.bookmark_frag);
        shout_frag = findView(R.id.shout_frag);


        fragment_container = (FrameLayout) findView(R.id.fragment_container);

        fm = getActivity().getSupportFragmentManager();

        sf = (ShoutsFragment) fm.findFragmentById(R.id.shout_frag);
        bf = (BookmarksFragment) fm.findFragmentById(R.id.bookmark_frag);


        activateTab(0);

        chats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                activateTab(0);


            }
        });

        bookmarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                activateTab(1);


            }
        });


        shouts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                activateTab(2);


            }
        });
        setHasOptionsMenu(true);

    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_conversations;
    }

    void activateTab(int tb) {
        FragmentManager mgr = getChildFragmentManager();

        switch (tb) {
            case 0:


                ChatsFragment cf = (ChatsFragment) getChildFragmentManager().findFragmentById(R.id.chat_frag);
                cf.UpdateChatsFromOutside();

                chat_frag.setVisibility(View.VISIBLE);
                bookmark_frag.setVisibility(View.GONE);
                shout_frag.setVisibility(View.GONE);


                //deactivate others
                bookmarks.setBackgroundResource(R.drawable.gray_square_hollow);
                bookmarks.setTextColor(getResources().getColor(R.color.appColor26));

                shouts.setBackgroundResource(R.drawable.gray_square_hollow_right_curved);
                shouts.setTextColor(getResources().getColor(R.color.appColor26));

                //activate chat
                chats.setBackgroundResource(R.drawable.gray_square_filled_left_curved);
                chats.setTextColor(Color.WHITE);

                break;
            case 1:


                BookmarksFragment bf = (BookmarksFragment) getChildFragmentManager().findFragmentById(R.id.bookmark_frag);

                bf.UpdateBookmarksFromOutside();


                chat_frag.setVisibility(View.GONE);
                bookmark_frag.setVisibility(View.VISIBLE);
                shout_frag.setVisibility(View.GONE);


                //deactivate others
                chats.setBackgroundResource(R.drawable.gray_square_hollow_left_curved);
                chats.setTextColor(getResources().getColor(R.color.appColor26));

                shouts.setBackgroundResource(R.drawable.gray_square_hollow_right_curved);
                shouts.setTextColor(getResources().getColor(R.color.appColor26));

                //activate bookmarks
                bookmarks.setBackgroundResource(R.drawable.gray_square_filled);
                bookmarks.setTextColor(Color.WHITE);


                break;
            case 2:


                ShoutsFragment sf = (ShoutsFragment) getChildFragmentManager().findFragmentById(R.id.shout_frag);

                sf.updateShoutsFromOutside();


                chat_frag.setVisibility(View.GONE);
                bookmark_frag.setVisibility(View.GONE);
                shout_frag.setVisibility(View.VISIBLE);

                //deactivate others
                chats.setBackgroundResource(R.drawable.gray_square_hollow_left_curved);
                chats.setTextColor(getResources().getColor(R.color.appColor26));

                bookmarks.setBackgroundResource(R.drawable.gray_square_hollow);
                bookmarks.setTextColor(getResources().getColor(R.color.appColor26));

                //activate chat
                shouts.setBackgroundResource(R.drawable.gray_square_filled_right_curved);
                shouts.setTextColor(Color.WHITE);


                break;
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        FragmentManager mgr = getChildFragmentManager();
        BookmarksFragment fragment = (BookmarksFragment) mgr.findFragmentById(R.id.bookmark_frag);

        if (null != fragment) {
            fragment.UpdateBookmarksFromOutside();
        }

//        AppController.getInstance().showNewIMsPopup();


    }


    public void updateShoutFromOutside() {

        FragmentManager mgr = getChildFragmentManager();
        BookmarksFragment fragment = (BookmarksFragment) mgr.findFragmentById(R.id.bookmark_frag);
        if (null != fragment) {
            fragment.UpdateBookmarksFromOutside();
        }


    }


}

