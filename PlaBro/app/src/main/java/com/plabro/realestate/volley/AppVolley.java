package com.plabro.realestate.volley;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonSyntaxException;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.ResponseListener;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.ServerResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.JSONUtils;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;

import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by hemant on 07/01/15.
 */
public class AppVolley {

    private boolean isEncryptionEnabled = false;
    private static final String TAG = "AppVolley";

    public static void processRequest1(String tag, String url, final Map<String, String> mParams, RequestMethod requestMethod, final ResponseListener mListener) {
        printMap((HashMap<String, String>) mParams);

        if (requestMethod.equals(RequestMethod.GET) && mParams != null && mParams.size() != 0) { //todo change to handle get post both
            //if (mParams != null && mParams.size() != 0) {
            String params = "";
            boolean firstEntry = true;

            for (Map.Entry<String, String> entry : mParams.entrySet()) {

                String key = entry.getKey();
                String value = entry.getValue();

                if (firstEntry) {
                    params = "?" + key + "=" + value;
                    firstEntry = false;
                } else {
                    params = params + "&" + key + "=" + Util.getURLEncodedString(value);
                }
            }
            if (!TextUtils.isEmpty(params)) {
                url = url + params;
                Log.d(TAG, "URL is :" + url);
            }

        }


        StringRequest sr = new StringRequest(RequestMethod.getRequestSerial(requestMethod), url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    if (JSONUtils.isJSONValid(response)) {
                        if (mParams.containsKey(PlaBroApi.RT_KEY) && !AppController.rtSet.contains(mParams.get(PlaBroApi.RT_KEY))) {
                            HashMap<String, Object> wrData = new HashMap<>();
                            wrData.put("RT", mParams.get(PlaBroApi.RT_KEY) + "");
                            //  Log.d("AppVolley", "tracking RT");

                        }
                        mListener.onSuccessResponse(response);

                    } else {
                        if (mParams.containsKey(PlaBroApi.RT_KEY) && !AppController.rtSet.contains(mParams.get(PlaBroApi.RT_KEY))) {
                            HashMap<String, Object> wrData = new HashMap<>();
                            wrData.put("RT", mParams.get(PlaBroApi.RT_KEY));
                            //  Log.d("AppVolley", "tracking RT");

                        }
                        mListener.onFailureResponse(new VolleyError());
                    }


                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mParams.containsKey(PlaBroApi.RT_KEY) && !AppController.rtSet.contains(mParams.get(PlaBroApi.RT_KEY))) {
                    HashMap<String, Object> wrData = new HashMap<>();
                    wrData.put("RT", mParams.get(PlaBroApi.RT_KEY));
                    Analytics.trackEventWithProperties(R.string.stability, R.string.e_rt_failure, wrData);
                }
                mListener.onFailureResponse(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return mParams;
            }

        };

        // sr.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // sr.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        sr.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(sr, tag);

    }

    public static void processRequest(ParamObject paramObject, VolleyListener listener) {
        processRequest(paramObject.getTaskCode(), paramObject.getClassType(), paramObject.getTag(), paramObject.getUrl(), paramObject.getParams(), paramObject.getRequestMethod(), listener, paramObject.isEncryptionEnabled());
    }


    public static void processRequest(final int taskCode, final Class classType, String tag, String url, final Map<String, String> mParams, RequestMethod requestMethod, final VolleyListener mListener, final boolean isEncryption) {
        if (!Util.haveNetworkConnection(AppController.getInstance())) {
            PBResponse res = new PBResponse();
            res.setCustomException(new CustomException("Please check your internet connection.", VolleyListener.NO_INETERNET_EXCEPTION));
            mListener.onFailureResponse(res, taskCode);
            return;
        }
        StringBuilder builder = new StringBuilder(url);
        if (requestMethod.equals(RequestMethod.GET) && mParams != null && mParams.size() != 0) {

            boolean firstEntry = true;

            for (Map.Entry<String, String> entry : mParams.entrySet()) {

                String key = entry.getKey();
                String value = entry.getValue();

                if (firstEntry) {
                    builder.append("?").append(key).append("=").append(Util.getURLEncodedString(value));
                    firstEntry = false;
                } else {
                    builder.append("&").append(key).append("=").append(Util.getURLEncodedString(value));
                }
            }

        }

        url = builder.toString();
        final String finalURL = url;
        Log.d(TAG, "Final URL :" + url);


        StringRequest sr = new StringRequest(RequestMethod.getRequestSerial(requestMethod), url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {

                onApiResponse(mParams, classType, response, mListener, taskCode, finalURL, isEncryption);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mListener.onFailureResponse(new VolleyError(), taskCode, VolleyListener.VOLLEY_EXCEPTION);
                //Log.d(TAG, "App Volley Error status code :" + error.networkResponse.statusCode);

                PBResponse res = new PBResponse();
                res.setCustomException(new CustomException("Oops some unknown error occurred ", error));
                mListener.onFailureResponse(res, taskCode);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                return mParams;
            } // To handle post

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }
        };

        // sr.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //sr.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        sr.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(sr, tag);

    }

    private static void printMap(HashMap<String, String> map) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            Log.i(TAG, entry.getKey() + ":" + entry.getValue());
        }
    }

    public static void onApiResponse(final Map<String, String> mParams, final Class classType, final String response, final VolleyListener mListener, final int taskCode, String url, final boolean isEncryption) {
        Log.d(TAG, response);
        final PBResponse res = new PBResponse();
        res.setUrl(url);
        res.setParams(mParams);
        boolean isSuccess = true;
        if (classType == null) {
            String tempResponse = null;
            if (isEncryption) {
                tempResponse = AppVolleyCacheManager.getEncryptedData(response);
            }
            if (tempResponse != null) {
                res.setResponse(tempResponse);
            } else {
                res.setResponse(response);
            }
            trackServerError(mParams, res);
            mListener.onSuccessResponse(res, taskCode);
            return;
        }

        new AsyncTask<Object, Object, Object>() {
            @Override
            protected Object doInBackground(Object... objects) {
                Object obj = null;
                try {
                    String tempResponse = null;
                    if (isEncryption) {
                        tempResponse = AppVolleyCacheManager.getEncryptedData(response);
                    }
                    if (tempResponse != null) {
                        obj = JSONUtils.parseJson(res, tempResponse, classType);
                    } else {
                        obj = JSONUtils.parseJson(res, response, classType);
                    }
                    if (obj == null) {
                        throw new CustomException("Invalid Server Response, Object After Parsing is Null", VolleyListener.JSON_PARSE_EXCEPTION);
                    }
                    res.setResponse(obj);
                    if (obj instanceof ServerResponse) {
                        Log.d(TAG, "Instance of server Response");
                        ServerResponse resp = (ServerResponse) obj;

                        if (null != resp.getMsg()) {
                            if (resp.getMsg().contains(ErrorType.SESSION_EXPIRED) || resp.getMsg().contains(ErrorType.MISSING_AUTHKEY)) {
                                Utility.deleteLoginObject();
                                Utility.userLogin(AppController.getInstance());
                                throw new CustomException(ErrorType.SESSION_EXPIRED, VolleyListener.SESSION_EXPIRED);
                            }
                            if (resp.getMsg().toUpperCase().contains(ErrorType.TRIAL_EXPIRED)) {
                                throw new CustomException(resp.getMsg() + "", VolleyListener.TRIAL_EXPIRED_EXCEPTION);
                            }
                        }
                        if (!resp.isStatus()) {
                            throw new CustomException(resp.getMsg() + "", VolleyListener.STATUS_FALSE_EXCEPTION);
                        }
                    }
                    // return false means no error
                    return false;
                } catch (CustomException e) {
                    res.setCustomException(e);
                    e.printStackTrace();
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    res.setCustomException(new CustomException("Invalid Server Response", VolleyListener.JSON_PARSE_EXCEPTION));
                } catch (Exception e) {
                    e.printStackTrace();
                    res.setCustomException(new CustomException("Unknown Error", VolleyListener.UNKNOWN_EXCEPTION));
                }
                return true;
            }

            @Override
            protected void onPostExecute(Object o) {
                boolean isError = (boolean) o;
                trackServerError(mParams, res);
                Log.d("AsyncTask Response:", "Iserror:" + isError);
                if (isError) {
                    mListener.onFailureResponse(res, taskCode);
                } else {
                    mListener.onSuccessResponse(res, taskCode);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public static void onApiResponse(final Map<String, String> mParams, final Class classType, final String response, final VolleyListener mListener, final int taskCode) {
        onApiResponse(mParams, classType, response, mListener, taskCode, "", false);
    }

    public static void trackServerError(final Map<String, String> params, PBResponse response) {
        String rt = params.get(PlaBroApi.RT_KEY);
        if (!TextUtils.isEmpty(rt)) {
            if (AppController.rtSet.contains(rt)) {
                return;
            }
        }

        HashMap<String, Object> data = new HashMap<>();
        if (null != params && params.size() > 0) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                String key = entry.getKey();
                if (key.equals(PlaBroApi.APP_VERSION) || key.equals(PlaBroApi.AUTH_KEY) || key.equals(PlaBroApi.APP_SOURCE) || key.equals("keytime") || key.equals("hashkey")) {

                } else {
                    data.put(entry.getKey(), entry.getValue());
                }
            }

        }
        Log.d(TAG, "Exception Message :" + response.getCustomException().getMessage());
        data.put("msg", response.getCustomException().getMessage() + "");
//        Analytics.trackEventWithProperties(R.string.CAT_ServerResponse, R.string.ACTION_API, data);
    }

    public static void processRequest(final int taskCode, final Class classType, String tag, String url, final Map<String, String> mParams, RequestMethod requestMethod, final VolleyListener mListener) {
        processRequest(taskCode, classType, tag, url, mParams, requestMethod, mListener, false);
    }

    public static interface ErrorType {
        String HTTP_ERROR = "ServerError";
        String STATUS_ERROR = "StatusError";
        String JSON_ERROR = "Invalid JSON";
        String UNKNOWN_ERROR = "UnknownError";
        String SESSION_EXPIRED = "Session Expired";
        String MISSING_AUTHKEY = "Missing AuthKey";
        String TRIAL_EXPIRED = "VERSION EXPIRED";
        String FALSE_STATUS = "FALSE STATUS";
    }

    public static String getStringFromMap(HashMap<String, String> mParams) {
        String params = "";
        boolean firstEntry = true;

        for (Map.Entry<String, String> entry : mParams.entrySet()) {

            String key = entry.getKey();
            String value = entry.getValue();

            if (firstEntry) {
                params = key + "=" + value;
                firstEntry = false;
            } else {
                params = params + "&" + key + "=" + Util.getURLEncodedString(value);
            }
        }
        return params;
    }

}
