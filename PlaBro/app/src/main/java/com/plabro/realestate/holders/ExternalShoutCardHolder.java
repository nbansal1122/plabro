package com.plabro.realestate.holders;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.FeedDetails;
import com.plabro.realestate.activity.Profile;
import com.plabro.realestate.activity.Shout;
import com.plabro.realestate.activity.WebViewActivity;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.models.BookmarkTag;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.ExternalSourceFeed;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBFonts;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by nitin on 25/03/16.
 */
public class ExternalShoutCardHolder extends FeedsCardHolder {

    public ExternalShoutCardHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onBindViewHolder(int position, BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        ExternalSourceFeed f = (ExternalSourceFeed) feed;
        if (!TextUtils.isEmpty(f.getRedirect_button_name())) {
        }
    }

    //    protected void redirectToDetails(int position, View v) {
//
//        Feeds feedsVal = ((Feeds) v.getTag());
//        String id = feedsVal.get_id();
//        if(feedsVal.is_profile_enabled()){
//            FeedDetails.startFeedDetails(ctx, id, refKey + "", feedsVal);
//        }else{
//            final ExternalSourceFeed f = (ExternalSourceFeed) feedsVal;
//            WebViewActivity.startActivity(ctx, "Plabro", f.getRedirectUrl(), "", false);
//        }
//
//
//    }
    protected void setupClickListener(final FeedsCardHolder holder, final int position, final Feeds feedEnabled) {
        final ExternalSourceFeed f = (ExternalSourceFeed) feedEnabled;
        if (!TextUtils.isEmpty(f.getRedirect_button_name())) {
            holder.mForward.setText(f.getRedirect_button_name());
        }

        holder.post_content.setTypeface(PBFonts.ROBOTTO_NORMAL);

        //on click listener
        holder.mMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trackEvent(Analytics.CardActions.CardClicked, position, feedEnabled.get_id(), feedEnabled.getType(), null);
                redirectToDetails(position, v);

            }
        });


        holder.post_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //lets tell our tooltip zoo zoo that relatedtip ship has sailed
                Set<String> indexSet = PBPreferences.getHomeToolTipSet();
                indexSet.add("relatedTip");
                PBPreferences.setHomeToolTipSet(indexSet);
                trackEvent(Analytics.CardActions.CardClicked, position, feedEnabled.get_id(), feedEnabled.getType(), null);
                redirectToDetails(position, v);

            }
        });
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trackEvent(Analytics.CardActions.CardClicked, position, feedEnabled.get_id(), feedEnabled.getType(), null);
                redirectToDetails(position, v);
            }
        });


        holder.mCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Feeds feedCopy = (Feeds) view.getTag();
                String feedCopiedStr = feedCopy.getProfile_phone() + " : \n" + feedCopy.getText() + "\nBy: " + feedCopy.getProfile_name();
                trackEvent(Analytics.CardActions.Copied, position, feedCopy.get_id(), feedCopy.getType(), null);
                ActivityUtils.copyFeed(feedCopiedStr, ctx);

            }
        });

        holder.mForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ExternalSourceFeed f = (ExternalSourceFeed) feedEnabled;
                WebViewActivity.startActivity(ctx, "Plabro", f.getRedirectUrl(), "", false);
                trackEvent(Analytics.CardActions.Forward, position, feedEnabled.get_id(), feedEnabled.getType(), null);
//                Analytics.trackScreen(Analytics.ShoutActions.LaunchShare);
//                PBSharingManager.shareByAll((Feeds) view.getTag(), ctx);
            }
        });


        holder.mBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Feeds bookmarkFeed = (Feeds) view.getTag();
                //  final BookmarkTag bt = (BookmarkTag) view.getTag();
                //final String post_id = bt.getId();
                //final Boolean bookmarked = bt.getBookmarked();
                if (!bookmarkFeed.getBookmarked()) {

                    //WizRocket tracking
                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("ScreenName", TAG);
                    wrData.put("Action", "Bookmark");
                    trackEvent(Analytics.CardActions.Bookmarked, position, bookmarkFeed.get_id(), bookmarkFeed.getType(), wrData);


                    holder.mBookmark.setImageResource(heart_red);

//                                BookmarkTag bt_n = new BookmarkTag();
//                                bt_n.setId(bt.getId());
//                                bt_n.setBookmarked(true);
                    bookmarkFeed.setBookmarked(true);
                    holder.mBookmark.setTag(feedEnabled);
                    setBookmark(bookmarkFeed, holder.mBookmark, true, position);
                } else {
                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("ScreenName", TAG);
                    trackEvent(Analytics.CardActions.UnBookMarked, position, bookmarkFeed.get_id(), bookmarkFeed.getType(), wrData);

                    holder.mBookmark.setImageResource(heart_grey);
                    bookmarkFeed.setBookmarked(false);
//                                BookmarkTag bt_n = new BookmarkTag();
//                                bt_n.setId(bt.getId());
//                                bt_n.setBookmarked(false);
                    holder.mBookmark.setTag(bookmarkFeed);

                    setBookmark(bookmarkFeed, holder.mBookmark, false, position);
                }
            }
        });


        holder.mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Feeds editFeed = (Feeds) view.getTag();

                //WizRocket tracking
                HashMap<String, Object> wrdata1 = new HashMap<>();
                wrdata1.put("ScreenName", TAG);
                wrdata1.put("Action", "EditShout");
                trackEvent(Analytics.CardActions.Edit, position, editFeed.get_id(), editFeed.getType(), wrdata1);

                Intent intent = new Intent(ctx, Shout.class);
                intent.putExtra("feedObject", editFeed);
                intent.putExtra("shoutType", "edit");
                intent.putExtra("postId", editFeed.get_id());
                ctx.startActivity(intent);
                ((Activity) ctx).overridePendingTransition(R.anim.bottom_in_fast, R.anim.bottom_out_fast);


            }
        });


        holder.iv_profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String authorid = ((Feeds) view.getTag()).getAuthorid() + "";
                HashMap<String, Object> wrData = new HashMap<String, Object>();
                wrData.put("ScreenName", TAG);
                wrData.put("Action", "Image Clicked");
                trackEvent(Analytics.CardActions.ViewProfile, position, feedEnabled.get_id(), feedEnabled.getType(), wrData);

                redirectToProfile(f, authorid);
            }
        });

        holder.tv_profile_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String authorid = ((Feeds) view.getTag()).getAuthorid() + "";
                HashMap<String, Object> wrData = new HashMap<String, Object>();
                wrData.put("ScreenName", TAG);
                wrData.put("Action", "Name Clicked");

                trackEvent(Analytics.CardActions.ViewProfile, position, feedEnabled.get_id(), feedEnabled.getType(), wrData);


                redirectToProfile(f, authorid);
            }
        });
        holder.tv_post_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String authorid = ((Feeds) view.getTag()).getAuthorid() + "";
                HashMap<String, Object> wrData = new HashMap<String, Object>();
                wrData.put("ScreenName", TAG);
                wrData.put("Action", "Post Time Clicked");
                trackEvent(Analytics.CardActions.ViewProfile, position, feedEnabled.get_id(), feedEnabled.getType(), wrData);


                redirectToProfile(f, authorid);
            }
        });

//                    enableCard(holder);
//                }
//                else {
//                    disableCard(holder);
//                }
    }

    protected void setupFeedDetailsCard(Feeds feed, FeedsCardHolder holder, final int position, boolean isDisabled) {
        String imgUrl = feed.getProfile_img();

        heart_red = R.drawable.ic_card_bookmark_pressed;
        heart_grey = R.drawable.ic_card_bookmark_normal;
        cardEnabled = ctx.getResources().getDrawable(R.drawable.gen_button_selector_grey);
        cardDisabled = ctx.getResources().getDrawable(R.drawable.gen_button_selector_disabled);

        //holder.iv_profile_image.setImageResource(R.drawable.profile_default_one);
        if (!TextUtils.isEmpty(imgUrl)) {
            Picasso.with(ctx).load(imgUrl).placeholder(R.drawable.profile_default_one).into(holder.iv_profile_image);
        } else {
            holder.iv_profile_image.setImageResource(R.drawable.profile_default_one);
        }

        String mUserName = "";
        String displayName = ActivityUtils.getDisplayNameFromPhone(feed.getProfile_phone());
        if (!TextUtils.isEmpty(displayName)) {
            mUserName = displayName;
        } else {

            mUserName = feed.getProfile_name();
            if (TextUtils.isEmpty(mUserName)) {
                mUserName = feed.getProfile_phone();
            }

        }
        holder.tv_profile_name.setText(Util.toDisplayCase(mUserName));
        holder.mBusinessName.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(feed.getBusiness_name())) {
            holder.mBusinessName.setText(Util.toDisplayCase(feed.getBusiness_name()));
        } else {
            holder.mBusinessName.setVisibility(View.GONE);
        }
        holder.card_view.setTag(feed);
        holder.iv_profile_image.setTag(feed);
        holder.tv_profile_name.setTag(feed);
        holder.tv_post_time.setTag(feed);
        holder.tv_post_time.setText(feed.getTime());
        if (feed.getAvail_req().length > 0 && !TextUtils.isEmpty(feed.getAvail_req()[0])) {

            holder.mAvailReq.setText(feed.getAvail_req()[0]);
            holder.mAvailReq.setVisibility(View.VISIBLE);

        } else {
            holder.mAvailReq.setVisibility(View.GONE);
        }

        if (feed.getSale_rent().length > 0 && !TextUtils.isEmpty(feed.getSale_rent()[0])) {
            holder.mSaleRent.setText(feed.getSale_rent()[0]);
            holder.mSaleRent.setVisibility(View.VISIBLE);
        } else {
            holder.mSaleRent.setVisibility(View.GONE);

        }

        if (!TextUtils.isEmpty(feed.getTop_ht())) {
            holder.mTopHash.setText(Util.toDisplayCase(feed.getTop_ht()));
            holder.mTopHash.setVisibility(View.VISIBLE);

        } else {
            holder.mTopHash.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(feed.getSugg_info())) {
            holder.mSuggestionInfo.setText(feed.getSugg_info());
            holder.mSuggInfoLayout.setVisibility(View.VISIBLE);
        } else {
            holder.mSuggInfoLayout.setVisibility(View.GONE);
            if (!isDisabled) {
                ((LinearLayout) holder.mSuggInfoLayout.getParent()).setBackgroundColor(ctx.getResources().getColor(R.color.white));
            }
        }

        ProfileClass userProfile = AppController.getInstance().getUserProfile();
        String authId = "";
        if (null != userProfile) {
            authId = userProfile.getAuthorid();
        }

        if ((feed.getAuthorid() + "").equalsIgnoreCase(authId)) {
            holder.mEdit.setTag(feed);
            holder.mEdit.setVisibility(View.VISIBLE);

            holder.mEdit.setVisibility(View.VISIBLE);

            holder.mBookmark.setVisibility(View.GONE);


        } else {
            BookmarkTag bt = new BookmarkTag();
            bt.setId(feed.get_id());
            Log.d(TAG, "Bookmarked :" + feed.getBookmarked());
            if (null != feed.getBookmarked() && feed.getBookmarked()) {
                bt.setBookmarked(true);
                holder.mBookmark.setImageResource(heart_red);
            } else {
                bt.setBookmarked(false);
                holder.mBookmark.setImageResource(heart_grey);

            }

            holder.mBookmark.setTag(feed);
            holder.mEdit.setVisibility(View.GONE);
            holder.mBookmark.setVisibility(View.VISIBLE);

        }


        holder.iv_options.setTag(feed);
        holder.post_content.setTag(feed);
        holder.card_view.setTag(feed);
        holder.mCopy.setTag(feed);
        holder.mForward.setTag(feed);
        holder.mMore.setTag(feed);
        String text = feed.getText();
        int start = 0;
        int end = 250;
        if (feed.getSnippet().size() > 0) {
            start = Integer.parseInt(feed.getSnippet().get(0));
            end = Integer.parseInt(feed.getSnippet().get(1)) + 1;
        }

        int occurance = text.length() - text.replace("\n", "").length();

        if (text.length() <= end) { //&& occurance<4
            holder.mMore.setVisibility(View.VISIBLE);
        } else {

            if (start >= 0 && end < text.length()) {
                try {
                    text = text.substring(start, end);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            holder.mMore.setVisibility(View.VISIBLE);
        }

        //adding span for hashtags in post
        SpannableString hashTagText = ActivityUtils.setHashTags(feed, text, start, end, ctx, holder.post_content, getType(), position, false, getRefKey(), mActivity);//type and position send to track analytics

//        if (feed.getHashTagText() == null) {
//            feed.setHashTagText(hashTagText);
//
//        } else {
//
//            feed.setHashTagText(feed.getHashTagText());
//        }

    }

    private void redirectToProfile(ExternalSourceFeed f, String authorid) {
        if(f.is_profile_enabled()==1){
            Intent intent1 = new Intent(ctx, Profile.class);
            intent1.putExtra("authorid", authorid);
            ctx.startActivity(intent1);
        }else if(f.is_profile_enabled()==2){
            WebViewActivity.startActivity(ctx, "Plabro", f.getRedirectUrl(), "", false);
        }else{
//
        }

    }

}
