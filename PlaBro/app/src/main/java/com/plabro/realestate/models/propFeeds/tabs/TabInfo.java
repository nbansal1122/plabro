package com.plabro.realestate.models.propFeeds.tabs;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

@Table(name = "TabInfo", id = BaseColumns._ID)
public class TabInfo extends Model{

    @Expose
    @Column
    private String RT;
    @Expose
    @Column
    private Integer enable;
    @Expose
    @Column(unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String tag;

    /**
     * @return The RT
     */
    public String getRT() {
        return RT;
    }

    /**
     * @param RT The RT
     */
    public void setRT(String RT) {
        this.RT = RT;
    }

    /**
     * @return The enable
     */
    public Integer getEnable() {
        return enable;
    }

    /**
     * @param enable The enable
     */
    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    /**
     * @return The tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * @param tag The tag
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

}