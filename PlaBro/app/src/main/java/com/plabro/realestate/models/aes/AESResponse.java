package com.plabro.realestate.models.aes;


import com.plabro.realestate.models.ServerResponse;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class AESResponse extends ServerResponse {

    AESOutputParams output_params = new AESOutputParams();


    public AESOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(AESOutputParams output_params) {
        this.output_params = output_params;
    }

}
