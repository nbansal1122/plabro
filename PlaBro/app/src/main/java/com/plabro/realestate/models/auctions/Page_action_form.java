package com.plabro.realestate.models.auctions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Page_action_form implements Serializable{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("hint")
    @Expose
    private String hint;
    @SerializedName("value")
    @Expose
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private boolean is_compulsory;

    public boolean is_compulsory() {
        return is_compulsory;
    }

    public void setIs_compulsory(boolean is_compulsory) {
        this.is_compulsory = is_compulsory;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The _id
     */
    public String get_id() {
        return _id;
    }

    /**
     * @param _id The _id
     */
    public void set_id(String _id) {
        this._id = _id;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The hint
     */
    public String getHint() {
        return hint;
    }

    /**
     * @param hint The hint
     */
    public void setHint(String hint) {
        this.hint = hint;
    }

}