package com.plabro.realestate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.crashlytics.android.Crashlytics;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.Services.BrokerContacts;
import com.plabro.realestate.Services.CallSync;
import com.plabro.realestate.Services.ChatHeadService;
import com.plabro.realestate.Services.ContactSync;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.Services.XMPPConnectionService;
import com.plabro.realestate.activity.AuctionDetails;
import com.plabro.realestate.activity.DeepLinking;
import com.plabro.realestate.activity.FeedDetails;
import com.plabro.realestate.activity.Followers;
import com.plabro.realestate.activity.FragmentContainer;
import com.plabro.realestate.activity.GreetingActivity;
import com.plabro.realestate.activity.PlabroWebLogin;
import com.plabro.realestate.activity.Redirect;
import com.plabro.realestate.activity.RelatedTagFeeds;
import com.plabro.realestate.activity.SearchFeeds;
import com.plabro.realestate.activity.Shout;
import com.plabro.realestate.activity.UserProfileNew;
import com.plabro.realestate.activity.WebViewActivity;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.drawer.MaterialNavigationDrawer;
import com.plabro.realestate.drawer.MaterialSection;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.fragments.BlogNewsFragment;
import com.plabro.realestate.fragments.BookmarksFragment;
import com.plabro.realestate.fragments.BuilderFragment;
import com.plabro.realestate.fragments.ChatsFragment;
import com.plabro.realestate.fragments.ContactsFragment;
import com.plabro.realestate.fragments.DirectListingFragment;
import com.plabro.realestate.fragments.DraftsFragment;
import com.plabro.realestate.fragments.FeedsFragment;
import com.plabro.realestate.fragments.HomeFragment;
import com.plabro.realestate.fragments.InvitesFragment;
import com.plabro.realestate.fragments.NotifCallFragment;
import com.plabro.realestate.fragments.PBWalletFragment;
import com.plabro.realestate.fragments.QuickLinksFragment;
import com.plabro.realestate.fragments.SettingsFragment;
import com.plabro.realestate.fragments.ShoutTabFragment;
import com.plabro.realestate.fragments.ShoutsFragment;
import com.plabro.realestate.fragments.auctions.AuctionFragment;
import com.plabro.realestate.fragments.community.CommunityFragment;
import com.plabro.realestate.fragments.services.BiddingFragment;
import com.plabro.realestate.fragments.services.ServicesFragment;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.GCMDeviceToken;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.chat.ChatMessage;
import com.plabro.realestate.models.notifications.Notification;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.profile.ProfileResponse;
import com.plabro.realestate.models.propFeeds.EndorsementNotificationData;
import com.plabro.realestate.models.propFeeds.Endorsement_data;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.CleverTap;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.uiHelpers.tourguide.Overlay;
import com.plabro.realestate.uiHelpers.tourguide.Pointer;
import com.plabro.realestate.uiHelpers.tourguide.ToolTip;
import com.plabro.realestate.uiHelpers.tourguide.TourGuide;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.DeepLinkNavigationUtil;
import com.plabro.realestate.utilities.JSONUtils;
import com.plabro.realestate.utilities.PBConnectionManager;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

public class HomeActivity extends MaterialNavigationDrawer implements Handler.Callback {

    private Handler handler;
    Boolean mRegStatus;

    Toolbar toolbar;
    SearchBoxCustom searchbox;
    static boolean isFirstTimeLaunch = false;
    FeedsFragment mFeedsFragment;
    HomeFragment homeFragment;
    ChatsFragment mChatsFragment;
    Fragment mBookmarksFragment;
    ShoutsFragment mShoutsFragment;
    ShoutTabFragment mShoutsTabFragment;
    DraftsFragment mDraftsFragment;
    ContactsFragment mContactsFragment;
    InvitesFragment mInvitesFragment;
    QuickLinksFragment mQuickLinksFragment;
    NotifCallFragment mNotification;
    SettingsFragment mSettingsFragment;
    BuilderFragment mBuilderFragment;
    ServicesFragment mServiceFragment;
    PBWalletFragment pbWalletFragment;
    PlabroWebLogin plabroLoginFragment;
    BlogNewsFragment blogNewsFragment;
    private CommunityFragment communityFragment;
    int mToolbarHeight;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    RelativeLayout mDraftsOptionHeader;
    private BiddingFragment biddingFragment;
    private AuctionFragment mAuctionFragment;
    private DirectListingFragment directListingFragment;


    @Override
    public void init(Bundle savedInstanceState) {
        handler = new Handler(this);
        //set flag to recognise first time user
        PBPreferences.setFirstTimeState(false);

        //get drawer toolbar
        toolbar = getToolbar();
        searchbox = (SearchBoxCustom) findViewById(R.id.searchbox);
//        searchbox.enableVoiceRecognition(this);
        searchbox.setLogoText("Search on Plabro");

        initLayouts();
        initFragments();

        Resources mResources = getResources();
        String[] menuNames = mResources.getStringArray(R.array.nav_drawer_items);
        TypedArray menuIcons = mResources.obtainTypedArray(R.array.nav_drawer_icons);


        setProfileSectionClick();
//
        this.addSection(this.newSection(menuNames[0], ContextCompat.getDrawable(this, R.drawable.ic_sidebar_feeds), false, homeFragment, true));
        this.addSection(this.newSection(menuNames[1], ContextCompat.getDrawable(this, R.drawable.ic_sidebar_chat), false, mChatsFragment, false));
        this.addSection(this.newSection(menuNames[6], ContextCompat.getDrawable(this, R.drawable.ic_sidebar_notification), false, mNotification, false));
        this.addSection(this.newSection(menuNames[9], ContextCompat.getDrawable(this, R.drawable.ic_sidebar_services), false, mServiceFragment, false, false));
        addDivisor();
        this.addSection(this.newSection(menuNames[11], ContextCompat.getDrawable(this, R.drawable.ic_sidebar_bidding), false, mAuctionFragment, false, true));
        this.addSection(this.newSection(menuNames[2], ContextCompat.getDrawable(this, R.drawable.ic_sidebar_bookmark), false, mBookmarksFragment, false));
        this.addSection(this.newSection(menuNames[3], ContextCompat.getDrawable(this, R.drawable.ic_sidebar_myshouts), false, mShoutsTabFragment, false));
        this.addSection(this.newSection(menuNames[4], ContextCompat.getDrawable(this, R.drawable.ic_sidebar_contact), false, mContactsFragment, false));


//        this.addSection(this.newSection(menuNames[12], mResources.getDrawable(menuIcons.getResourceId(13, 0)), false, directListingFragment, false));

        addDivisor();

        this.addSection(this.newSection(menuNames[7], ContextCompat.getDrawable(this, R.drawable.ic_sidebar_settings), false, mSettingsFragment, false));
        this.addSection(this.newSection(menuNames[5], ContextCompat.getDrawable(this, R.drawable.ic_sidebar_invites), false, mInvitesFragment, false));

        if (Util.haveNetworkConnection(this)) {
            PlabroIntentService.startConnectivityChangeAction(this);
            XMPPConnectionService.startXMPPService(this);

        }

        initiateSearchToolTip();

        //initiateFeaturedToolTip();

        initGcmServices();


        //showCoach();

        //initialise connection
        PBConnectionManager.getInstance(AppController.getInstance());
        setUpChatNotification();
        setUpSettingsNotification();

        //if profile doesnt exist make a fresh call or set up data and call in parallel
        if (null == AppController.getInstance().getUserProfile()) {
            getUserProfile();
        } else {
            setDrawerUserImage();
            setDrawerUserName();
            setInvitesCount();
            getUserProfile();
        }

        getCityList();

        redirectForPush(getIntent());
        showEndorsement(getIntent().getBooleanExtra("fromNotification", false));
        initiateRatingScreen();
        XMPPConnectionService.startXMPPService(this);
//        ContactSync.startContactSyncing(AppController.getInstance());
//        BrokerContacts.stopContactSyncing(AppController.getInstance());

        boolean isCallSyncEnabled = PBPreferences.getData(PBPreferences.ENABLE_CALL_SYNC, false);
        if (isCallSyncEnabled) {
            CallSync.startService(this);
        }
//        getGoogleAdwordsId();
//        hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION, PlaBroApi.PERMISSIONS.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);
    }

    private void getGoogleAdwordsId() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final String id = AdvertisingIdClient.getAdvertisingIdInfo(HomeActivity.this).getId();
                    final boolean isLimitAdTrackingEnabled = AdvertisingIdClient.getAdvertisingIdInfo(HomeActivity.this).isLimitAdTrackingEnabled();
                    Log.d(TAG, "ID-->" + id);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pingGoogleAdwords(id, isLimitAdTrackingEnabled);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

    private void pingGoogleAdwords(String id, boolean isLimitedAdTrackingEnabled) {

        String URL = "https://www.googleadservices.com/pagead/conversion/945617553/";
        ParamObject obj = new ParamObject();
        obj.setUrl(URL);
        HashMap<String, String> params = new HashMap<>();
//        rdid=ADVERTISING_ID
        params.put("bundleid", "com.plabro.realestate");
        params.put("rdid", id);
//            idtype=advertisingid
        params.put("idtype", "advertisingid");
        params.put("remarketing_only", "1");
        params.put("appversion", AppController.getInstance().getPackageInfo().versionName + "");
//            usage_tracking_enabled
        params.put("usage_tracking_enabled", "1");
        params.put("lat", isLimitedAdTrackingEnabled ? "1" : "0");
        obj.setParams(params);

        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "Pinged Google Adwords" + response.getResponse());
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "Pinged Google Adwords Failure" + response.getCustomException().getMessage());
            }
        });

    }


    private void initLayouts() {
        mDraftsOptionHeader = (RelativeLayout) findViewById(R.id.drafts_options_header);
    }

    private void showEndorsement(boolean fromNotification) {
        Log.d(TAG, "From Notification" + fromNotification);
        if (fromNotification) return;
        EndorsementNotificationData data0 = new Select().from(EndorsementNotificationData.class).executeSingle();
        if (data0 == null) return;
        EndorsementNotificationData data = (EndorsementNotificationData) JSONUtils.parseJson(data0.getJsonString(), EndorsementNotificationData.class);
        Feeds f = new Feeds();
        f.setProfile_name(data.getProfile_name());
        f.setProfile_img(data.getImg_url());
        Endorsement_data d = new Endorsement_data();
        d.setTitle(data.getTitle());
        d.setKeys(data.getKeys());
        f.setAuthorid(data0.getContact_id());
        f.setEndorsement_data(d);
        showCallDialog(this, f, "CALL ENDED WITH", true);
        data0.delete();

    }

    private void initiateRatingScreen() {

        long currentTime = System.currentTimeMillis();
        long previousTime = PBPreferences.getRatingTimer();
        ProfileClass pc = AppController.getInstance().getUserProfile();


        if ((currentTime - previousTime > 7200000) && (pc.getRating_flag() == 1)) {
            Intent intent = new Intent(HomeActivity.this, Rating.class);
            startActivity(intent);
        } else {

        }

    }


    private void setupSearchTootlip() throws OutOfMemoryError, Exception {

        Log.d(TAG, "tooltip configured");


        final Button searchPlaceHolder = (Button) findViewById(R.id.searchPlaceHolder);
        final Button searchToolTip = (Button) findViewById(R.id.searchToolTip);


        int oColor = getResources().getColor(R.color.pbCoachMark);
        Overlay overlay = Utility.getOverLay(HomeActivity.this, oColor);
        Pointer pointer = Utility.getPointer(HomeActivity.this);
        String title = getResources().getString(R.string.search_msg_one);
        String msg = "";
        searchPlaceHolder.setVisibility(View.VISIBLE);
        searchToolTip.setVisibility(View.VISIBLE);

        ToolTip toolTip = Utility.getTooltip(HomeActivity.this, title, msg, Gravity.RIGHT, Gravity.BOTTOM);

        final TourGuide mTourGuideHandler = TourGuide.init(this).with(TourGuide.Technique.Click)
                .setPointer(pointer)
                .setOverlay(overlay)
                .playOn(searchPlaceHolder);

        final TourGuide mTourGuideHandlerToolTip = TourGuide.init(this).with(TourGuide.Technique.Click)
                .setToolTip(toolTip)
                .playOn(searchToolTip);

        overlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTourGuideHandler.cleanUp();
                mTourGuideHandlerToolTip.cleanUp();
                searchPlaceHolder.setVisibility(View.GONE);
                searchToolTip.setVisibility(View.GONE);

                Set<String> indexSet = PBPreferences.getPBToolTipSet();
                indexSet.add(Constants.ToolTip.SEARCHICON);
                PBPreferences.setPBToolTipSet(indexSet);
            }
        });


        searchPlaceHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTourGuideHandler.cleanUp();
                mTourGuideHandlerToolTip.cleanUp();
                searchPlaceHolder.setVisibility(View.GONE);
                searchToolTip.setVisibility(View.GONE);

                Set<String> indexSet = PBPreferences.getPBToolTipSet();
                indexSet.add(Constants.ToolTip.SEARCHICON);
                PBPreferences.setPBToolTipSet(indexSet);
            }
        });

        Log.d(TAG, "tooltip displayed");


    }

    private void setupFeaturedTootlip() {

        Log.d(TAG, "tooltip configured");


        final Button featuredPlaceHolder = (Button) findViewById(R.id.featuredPlaceHolder);
        final Button featuredToolTip = (Button) findViewById(R.id.featuredToolTip);


        int oColor = getResources().getColor(R.color.pbCoachMark);
        Overlay overlay = Utility.getOverLay(HomeActivity.this, oColor);
        Pointer pointer = Utility.getPointer(HomeActivity.this);
        String title = getResources().getString(R.string.featured_msg_one);
        String msg = "";
        featuredPlaceHolder.setVisibility(View.VISIBLE);
        featuredToolTip.setVisibility(View.VISIBLE);

        ToolTip toolTip = Utility.getTooltip(HomeActivity.this, title, msg, Gravity.RIGHT, Gravity.BOTTOM);

        final TourGuide mTourGuideHandler = TourGuide.init(this).with(TourGuide.Technique.Click)
                .setPointer(pointer)
                .setOverlay(overlay)
                .playOn(featuredPlaceHolder);

        final TourGuide mTourGuideHandlerToolTip = TourGuide.init(this).with(TourGuide.Technique.Click)
                .setToolTip(toolTip)
                .playOn(featuredToolTip);

        overlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTourGuideHandler.cleanUp();
                mTourGuideHandlerToolTip.cleanUp();
                featuredPlaceHolder.setVisibility(View.GONE);
                featuredToolTip.setVisibility(View.GONE);

                Set<String> indexSet = PBPreferences.getPBToolTipSet();
                indexSet.add(Constants.ToolTip.FEATUREDICON);
                PBPreferences.setPBToolTipSet(indexSet);
            }
        });


        featuredPlaceHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTourGuideHandler.cleanUp();
                mTourGuideHandlerToolTip.cleanUp();
                featuredPlaceHolder.setVisibility(View.GONE);
                featuredToolTip.setVisibility(View.GONE);

                Set<String> indexSet = PBPreferences.getPBToolTipSet();
                indexSet.add(Constants.ToolTip.FEATUREDICON);
                PBPreferences.setPBToolTipSet(indexSet);
            }
        });

        Log.d(TAG, "tooltip displayed");


    }

    private void setProfileSectionClick() {
        getmProfileSection().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, UserProfileNew.class);
                intent.putExtra("isCurrentUser", true);
                startActivity(intent);
            }
        });
    }


    public static boolean isFirstTimeLaunch() {
        return isFirstTimeLaunch;
    }

    @Override
    public void onPush() {
        displayView(getIntent());
    }


    @Override
    public Toolbar getToolbar() {
        return super.getToolbar();
    }


    @Override
    public void onBackPressed() {

        mDraftsOptionHeader.setVisibility(View.GONE);

        if (searchbox.isShown()) {
            searchbox.hideCircularly(this);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1 && lastSectionPosition != 0) {
                setInitialFragment();
            } else {
                //getSupportFragmentManager().popBackStack();

                //super.onBackPressed();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
    }

    private void setInitialFragment() {
        sectionList.get(lastSectionPosition).unSelect();
        removeFragment(sectionList.get(lastSectionPosition));

        lastSectionPosition = 0;
        MaterialSection section = sectionList.get(lastSectionPosition);
        section.select();
        setTitle(AppController.getInstance().getCityName());
        showFragment(section);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }


    public void notifyMovedOverThresholdValue() {

    }


    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
        setFeedNotification();
        checkProfileUpdate();
        if (lastSectionPosition == 0) {
            setTitle(AppController.getInstance().getCityName());
        }
    }

    private void checkProfileUpdate() {
        //if profile updated / edited update left nav details
        boolean isProfileUpdated = PBPreferences.getIsProfileEdited();

        if (isProfileUpdated) {
            setDrawerUserImage();
            setDrawerUserName();
            setInvitesCount();

            //rest flag
            PBPreferences.setIsProfileEdited(false);
        }

    }

    private void setInvitesCount() {
//
    }

    private void setFeedNotification() {

        int val = PBPreferences.getFeedNotificationVal();
        if (val > 0) {
            setNoOfNotification(0, true, val + "");
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        redirectForPush(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    private void initFragments() {

//        mFeedsFragment = FeedsFragment.getInstance(this, toolbar);
        mChatsFragment = ChatsFragment.getInstance(this, toolbar, searchbox);
        mBookmarksFragment = BookmarksFragment.getInstance(toolbar, searchbox);
//        mShoutsFragment = ShoutsFragment.getInstance(this, toolbar, searchbox);
        mShoutsTabFragment = ShoutTabFragment.getInstance(toolbar, searchbox, mDraftsOptionHeader);
//        mDraftsFragment = DraftsFragment.getInstance(this, toolbar, searchbox, mDraftsOptionHeader);
        mContactsFragment = ContactsFragment.getInstance(this, toolbar, searchbox);
        mInvitesFragment = InvitesFragment.getInstance(this, toolbar);
        mBuilderFragment = BuilderFragment.getInstance(this, toolbar);
        mQuickLinksFragment = QuickLinksFragment.getInstance(this, toolbar);
        mSettingsFragment = SettingsFragment.getInstance(this, toolbar);
        mNotification = NotifCallFragment.getInstance(toolbar, searchbox);
        homeFragment = HomeFragment.getInstance(toolbar, searchbox);
        mServiceFragment = new ServicesFragment();
        pbWalletFragment = PBWalletFragment.getInstance(this, toolbar, searchbox);

        biddingFragment = BiddingFragment.getInstance("MY BIDS", PlaBroApi.getBidsUrl(), PlaBroApi.getBidsExitUrl());
        directListingFragment = DirectListingFragment.getInstance(toolbar, searchbox);
        mAuctionFragment = AuctionFragment.getInstance(toolbar, searchbox);
        plabroLoginFragment = new PlabroWebLogin();
        blogNewsFragment = BlogNewsFragment.getInstance(toolbar, searchbox);
    }


    @Override
    public void findViewById() {


    }


    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    public void selectSection(int index) {
        MaterialSection overviewSection = getSectionList().get(index);
        overviewSection.select();
        onClick(overviewSection);
    }

    public void selectSection(int index, boolean showDialog) {
        MaterialSection overviewSection = getSectionList().get(index);
        overviewSection.select();
        onClick(overviewSection);
    }

    private void displayView(Intent intent) {

        if (intent != null && intent.hasExtra("uri")) {
            String uri = intent.getStringExtra("uri");
            String query = intent.getStringExtra("q");

            switch (uri) {
//                case DeepLinking.hostSearch:
//                    Util.e("HomeActivity : " + DOPreferences.getCityName(this));
//                    Intent intentSearch = new Intent(this, RestaurantResultActivity.class);
//                    intentSearch.putExtra(Constants.AUTO_SUGGESTION, query);
//                    intentSearch.putExtra("isFromNotification", true);
//                    startActivity(intentSearch);
//
//                    break;


                default:
                    break;

            }
        }

    }


    public void initGcmServices() {
        if (TextUtils.isEmpty(PBPreferences.getGCMRegistrarId()) || !PBPreferences.isRegistrarIdSend()) {

            try {
                GCMRegistrar.checkDevice(this);
                GCMRegistrar.checkManifest(this);

                // Check device for Play Services APK.
                if (checkPlayServices()) {
                    // If this check succeeds, proceed with normal processing.
                    // Otherwise, prompt user to get valid Play Services APK.
                    registerInBackground();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("PLABRO", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                try {
                    GoogleCloudMessaging mGoogleCloudMessaging = GoogleCloudMessaging.getInstance(HomeActivity.this);

                    String regId = mGoogleCloudMessaging.register(getString(R.string.gcm_sender_id));
                    CleverTap cleverTap = CleverTap.getInstance(HomeActivity.this);
                    cleverTap.getCleverTapAPI().data.pushGcmRegistrationId(regId, true);
                    if (regId.isEmpty()) {
                        registerInBackground();
                        return "";
                    } else {
                        return regId;

                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String regId) {
                Log.d("GCM-DEMO", "GCM Registration Status ::::: " + regId);
//                CleverTap.getInstance(HomeActivity.this).getCleverTapAPI().data.pushGcmRegistrationId(regId, true);

                if (!TextUtils.isEmpty(regId)) {
                    PBPreferences.setGCMRegistrationId(regId);
                    sendRegId();
                    CleverTap.getInstance(HomeActivity.this).getCleverTapAPI().data.pushGcmRegistrationId(regId, true);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    private void sendRegId() {

        // Device model
        String phoneModel = android.os.Build.MODEL;

        //get app version number
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionCode + "";

        // Android version
        String androidVersion = android.os.Build.VERSION.RELEASE;

        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.GCM_REG_GET_PARAM_DEVICE_REG_ID, PBPreferences.getGCMRegistrarId());
        params.put(PlaBroApi.GCM_REG_GET_PARAM_OSTTYPE, "android");
        params.put(PlaBroApi.GCM_REG_GET_PARAM_OSVERSION, androidVersion);
        params.put(PlaBroApi.GCM_REG_GET_PARAM_PHONE_MODEL, phoneModel);
        params.put(PlaBroApi.GCM_REG_GET_PARAM_PHONE_VERSION, version);
        params.put(PlaBroApi.GCM_REG_GET_PARAM_PHONE_APP_VERSION, version);


        params = Utility.getInitialParams(PlaBroApi.RT.UPDATE_REGISTER_KEY, params);

        AppVolley.processRequest(1, GCMDeviceToken.class, null, String.format(PlaBroApi.getBaseUrl(HomeActivity.this)), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                GCMDeviceToken gcmDeviceToken = (GCMDeviceToken) response.getResponse();
                if (gcmDeviceToken != null && gcmDeviceToken.isStatus()) {
                    PBPreferences.setIsRegistrarIdSend(gcmDeviceToken.isStatus());

                    mRegStatus = true;

                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                switch (response.getCustomException().getExceptionType()) {
                    case SESSION_EXPIRED:
                        Log.d(TAG, "Session Expired while updating registrar key");
                        Utility.userLogin(HomeActivity.this);
                        break;
                    case STATUS_FALSE_EXCEPTION:
                        break;
                }
                mRegStatus = false;
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!mRegStatus) {
                            sendRegId();
                        }
                    }
                }, Constants.DELAY_SEN_REG_ID_TIMEr);
            }
        });
    }


    @Override
    protected String getTag() {
        return "Home Screen";
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        stopService(new Intent(HomeActivity.this, PlabroIntentService.class));
//        stopService(new Intent(HomeActivity.this, ChatHeadService.class));
        ContactSync.startContactSyncing(AppController.getInstance());
        if (PBPreferences.getData(PBPreferences.IS_BROKER_CONTACT_ENABLED, false)) {
//            BrokerContacts.startContactSyncing(AppController.getInstance());
        }
        BrokerContacts.startContactSyncing(AppController.getInstance());

    }


    public void setUpChatNotification() {
        final String unreadMSG = new Select().from(ChatMessage.class).where("status='" + ChatMessage.STATUS_RECEIVED + "' and msgType = " + ChatMessage.MESSAGE_INCOMING).execute().size() + "";

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                int section = Constants.SectionIndex.CHATS;//index of chat in leftnav panel
                if (null != unreadMSG && !TextUtils.isEmpty(unreadMSG) && !unreadMSG.equalsIgnoreCase("0")) {
                    setNoOfNotification(section, true, unreadMSG);
                }

            }
        });


    }

    public void setUpSettingsNotification() {
        final String settingNot = PBPreferences.getSettingNotificationVal();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                int section = Constants.SectionIndex.SETTINGS;
                ;//index of settings in leftnav panel
                if (null != settingNot && !TextUtils.isEmpty(settingNot) && !settingNot.equalsIgnoreCase("0")) {
                    setNoOfNotification(section, true, settingNot);
                }

            }
        });


    }

    private void sendLaunchEvent(boolean isFromDeepLink, String act) {
        HashMap<String, Object> metaData = new HashMap<>();
        metaData.put("eventType", "AppLaunch");
        if (isFromDeepLink) {
            metaData.put("source", "DeepLink");
        } else {
            metaData.put("source", "PushNotification");
        }
        metaData.put("actType", act + "");
    }


    private void redirectForPush(Intent i) {

        Intent intentMaster = i;
        Boolean fromNotification = intentMaster.getBooleanExtra("fromNotification", false);
        String title = intentMaster.getStringExtra("title");
        String uri = intentMaster.getStringExtra("act");
        String message = intentMaster.getStringExtra("msg");
        String url = intentMaster.getStringExtra("url");
        //if app launched from push notif
        if (fromNotification) {
            boolean newIm = false;
            String localPush = intentMaster.getStringExtra("new_im");
            if (null != localPush && !"".equalsIgnoreCase(localPush)) {
                newIm = true;

            }
            boolean isFromDeepLink = intentMaster.getBooleanExtra(PBPreferences.FROM_DEEP_LINKING, false);
            sendLaunchEvent(isFromDeepLink, uri);


            PBPreferences.setNewPostState(false);

            if (null != uri && !uri.equalsIgnoreCase("")) {
                switch (uri) {
                    case "fl":
                        //Analytics.trackScreen(this, getResources().getString(R.string.track_follower_notification));
                        Intent intent = null;
                        Bundle b = intentMaster.getExtras();
                        String authorId = b.getString("authorid");
                        if (TextUtils.isEmpty(authorId)) {
                            intent = new Intent(HomeActivity.this, Followers.class);
                            authorId = PBPreferences.getNotAuthorId();
                        } else {
                            intent = new Intent(HomeActivity.this, UserProfileNew.class);
                        }
                        intent.putExtra("authorid", authorId);
                        intent.putExtra("type", "followers");
                        startActivity(intent);
                        break;
                    case "im":
                        //Analytics.trackScreen(this, getResources().getString(R.string.track_chat_notification));
                        PBPreferences.setNewIMState(false);

                        if (message.contains("brokers")) {
                            selectSection(Constants.SectionIndex.CHATS);
                            //update notif status for the user
                            new Update(Notification.class).set("status='" + Notification.STATUS_READ + "' where act = 'im'").execute();


                        } else {
                            Intent intentIM = new Intent(HomeActivity.this, XMPPChat.class);
                            intentIM.putExtras(getIntent().getExtras());
                            String num = getIntent().getExtras().getString("phone_key");

                            //update notif status for the user
                            new Update(Notification.class).set("status='" + Notification.STATUS_READ + "' where phone = '" + num + "'").execute();

                            Log.d(TAG, "NUm From Push::" + num);
                            String displayName = ActivityUtils.getDisplayNameFromPhone(num);
                            if (displayName != null && !TextUtils.isEmpty(displayName)) {
                                intentIM.putExtra("name", displayName);
                            } else {
                                intentIM.putExtra("name", num);
                            }
                            startActivity(intentIM);
                        }

                        break;
                    case "auc":
                        FragmentContainer.startActivity(this, Constants.FRAGMENT_TYPE.AUCTION, intentMaster.getExtras());
                        break;
                    case "wv":
                        String exitUrl = intentMaster.getStringExtra("exit_url");
                        String webViewTitle = intentMaster.getStringExtra("webview_title");
                        WebViewActivity.startActivity(this, webViewTitle, url, exitUrl);
                        break;
                    case "pf":
                        String messageId = getIntent().getStringExtra("message_id");
                        if (!TextUtils.isEmpty(messageId)) {
                            Redirect.redirectToShoutDetail(HomeActivity.this, messageId, "");
                        }
                        break;
                    case "pb":
                        //Analytics.trackScreen(this, getResources().getString(R.string.track_broadcast_notification));
                        String broadCastUrl = PBPreferences.getBroadCastUrl();
                        PBPreferences.setNewBroadCastState(false);

                        if (null != broadCastUrl) {
                            PBSharingManager.openWebUrl(broadCastUrl, HomeActivity.this);
                        }
                        break;
                    case "gt":
                        Intent greetingIntent = new Intent(this, GreetingActivity.class);
                        greetingIntent.putExtras(i.getExtras());
                        startActivity(greetingIntent);
                        break;
                    case "eN":
                        showEndorsement(false);
                        break;
                    case DeepLinking.ACT_TYPE.UPLOAD:
//                        startNextActivity(getIntent().getExtras(), HomeActivity.class);
                        break;
                    case DeepLinking.ACT_TYPE.SHOUT:
                        startNextActivity(intentMaster.getExtras(), Redirect.class);
                        break;
                    case DeepLinking.ACT_TYPE.HASH:
                        startNextActivity(intentMaster.getExtras(), RelatedTagFeeds.class);
                        break;
                    case DeepLinking.ACT_TYPE.HOME:
//                        startNextActivity(getIntent().getExtras(), HomeActivity.class);
                        break;
                    case DeepLinking.ACT_TYPE.CHAT:
                        Bundle extra = intentMaster.getExtras();
                        if (extra.containsKey("phone_key") && extra.containsKey("name")) {
                            startNextActivity(intentMaster.getExtras(), XMPPChat.class);
                        } else {
                            setTitle("Chats");
                            openFragment(Constants.SectionIndex.CHATS);
                        }

                        break;
                    case DeepLinking.ACT_TYPE.COMPOSE:
                        startNextActivity(intentMaster.getExtras(), Shout.class);
                        break;
                    case DeepLinking.ACT_TYPE.SEARCH:
                        startNextActivity(intentMaster.getExtras(), SearchFeeds.class);
                        break;
                    case DeepLinking.ACT_TYPE.UPDATE:
                        String version = intentMaster.getStringExtra("version");
                        if (!TextUtils.isEmpty(version)) {
                            int versionCode = AppController.getInstance().getPackageInfo().versionCode;
                            try {
                                int deepLinkingVersion = Integer.parseInt(version);
                                if (deepLinkingVersion > versionCode) {
                                    openMarketUrl();
                                }
                            } catch (Exception e) {

                            }
                        }
//                        startNextActivity(getIntent().getExtras(), HomeActivity.class);
                        break;
                    case DeepLinking.ACT_TYPE.URI_GCM_DEEP_LINK:
                        if (intentMaster.getExtras().containsKey("url")) {
                            String deepLinkUrl = intentMaster.getExtras().getString("url");
                            Bundle deepLinkBundle = DeepLinkNavigationUtil.getMap(deepLinkUrl);
                            DeepLinkNavigationUtil.navigate(deepLinkBundle, this);
                        }
                        break;
                    default:
                        DeepLinkNavigationUtil.navigate(intentMaster.getExtras(), this);
                }
            }

        }


    }

    private void openFragment(int sectionIndex) {
        MaterialNavigationDrawer act = this;
        if (null != act && null != act.getSectionList() && null != act.getSectionList().get(sectionIndex)) {
            act.onClick(act.getSectionList().get(sectionIndex));
        }
    }

    private void openMarketUrl() {
        String url = "market://details?id=com.plabro.realestate";
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    private void startNextActivity(Bundle b, Class activityClass) {
        Intent i = new Intent(this, activityClass);
        if (null != b)
            i.putExtras(b);
        startActivity(i);
    }

    void initiateFeaturedToolTip() {
        Log.d(TAG, "tooltip initiated");
        Set<String> indexSet = PBPreferences.getPBToolTipSet();
        boolean featuredTooltipShown = false;
        boolean searchTooltipShown = false;

        for (String s : indexSet) {

            if (s.equalsIgnoreCase(Constants.ToolTip.FEATUREDICON)) {
                featuredTooltipShown = true;
            }

            if (s.equalsIgnoreCase(Constants.ToolTip.SEARCHICON)) {
                searchTooltipShown = true;
            }
        }

        if (!featuredTooltipShown && searchTooltipShown) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 3000ms
                    setupFeaturedTootlip();

                }
            }, 3000);
        }

    }

    void initiateSearchToolTip() {
        Log.d(TAG, "tooltip initiated");
        Set<String> indexSet = PBPreferences.getPBToolTipSet();
        boolean searchTooltipShown = false;

        for (String s : indexSet) {

            if (s.equalsIgnoreCase(Constants.ToolTip.SEARCHICON)) {
                searchTooltipShown = true;
            }
        }

        if (!searchTooltipShown) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 3000ms
                    try {
                        setupSearchTootlip();
                    } catch (Exception e) {
                        Crashlytics.logException(e);
                    } catch (OutOfMemoryError e) {
                        Crashlytics.logException(e);
                    }

                }
            }, 3000);
        }

    }

    void getUserProfile() {

        // If internet connection is not available it will show dialog only if profile is not cached
        if (!Util.haveNetworkConnection(HomeActivity.this)) {
            setDrawerUserImage();
            setDrawerUserName();
            return;
        }

        HashMap<String, String> params
                = Utility.getInitialParams(PlaBroApi.RT.GETPROFILE, null);


        AppVolley.processRequest(2, ProfileResponse.class, null, String.format(PlaBroApi.getBaseUrl(HomeActivity.this)), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                ProfileResponse profileResponse = (ProfileResponse) response.getResponse();
                if (profileResponse.isStatus()) {
                    ProfileClass profileClass = profileResponse.getOutput_params().getData();
                    profileClass.saveData();
                    AppController.getInstance().setUserProfile(profileClass);
                    setDrawerUserImage();
                    setDrawerUserName();
                    setInvitesCount();
                    CleverTap.recordProfile(profileClass);

                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

            }

        });

    }

    private static void sendChatBroadCast(int msg) {
        Intent i = new Intent();
        i.setAction(Constants.ACTION_CHAT_COUNT);
        i.putExtra(Constants.BUNDLE_KEYS.CHAT_COUNT, msg);
        Log.d("Home Screen", "Sending chat broadcast");
        AppController.getInstance().sendBroadcast(i);
    }

    public static void setNoOfNotification(int section, boolean status, String mCount) {
        if (null == sectionList || null == sectionList.get(section)) return;
        sectionList.get(section).setNoOfPendings(status, mCount + "");
        if (section == Constants.SectionIndex.CHATS) {
            int count = Integer.parseInt(mCount);
            sendChatBroadCast(count);
        }
    }


    @Override
    public boolean handleMessage(Message message) {
        return false;
    }


    void getCityList() {

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    public void onClick(MaterialSection section) {
        super.onClick(section);
        try {
            if (searchbox != null && searchbox.getSearchOpen()) {
                searchbox.closeSearch();
                searchbox.hideCircularly(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

