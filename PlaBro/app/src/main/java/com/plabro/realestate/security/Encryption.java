package com.plabro.realestate.security;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;


public class Encryption {
	private static Cipher cipher;
	private static SecretKey secretKey;
	
	Encryption(String keyString) throws Exception
	{
		setKey(keyString);
		cipher = Cipher.getInstance("AES/ECB/noPadding");				
	}

	public static void generateKey() throws Exception
	{
		KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(128);
		secretKey = keyGenerator.generateKey();
	}

	public static String getKey() throws Exception
	{
		byte[] keyByte = secretKey.getEncoded();
		return Base64.encodeToString(keyByte,0);
	}

	public static void setKey(String keyString) throws Exception
	{
		byte[] keyByte = Base64.decode(keyString, 0);
		secretKey = new SecretKeySpec(keyByte, 0, keyByte.length, "AES");
	}

	public static String addPad(String data) throws Exception
	{
		int len= data.length();
		int padLen= len%16;
		String pad="";
		switch (padLen) 
		{
			case 0:		pad="0000000000000000";
				break;
			case 1:		pad="fffffffffffffff";
				break;
			case 2:		pad="eeeeeeeeeeeeee";
				break;
			case 3:		pad="ddddddddddddd";
				break;
			case 4:		pad="cccccccccccc";
				break;
			case 5:		pad="bbbbbbbbbbb";
				break;
			case 6:		pad="aaaaaaaaaa";
				break;
			case 7:		pad="999999999";
				break;
			case 8:		pad="88888888";
				break;
			case 9:		pad="7777777";
				break;
			case 10:	pad="666666";
				break;
			case 11:	pad="55555";
				break;
			case 12:	pad="4444";
				break;
			case 13:	pad="333";
				break;
			case 14:	pad="22";
				break;
			case 15:	pad="1";
				break;
			default:	pad="";
		}

		String finals;
		finals  = data.concat(pad);
		return finals;
	}

	public static String removePad(String data)
	{
		String lastChar = data.substring(data.length() - 1, data.length());
		int remLen=0;
		if(lastChar.equals("0"))
			remLen=16;
		else if (lastChar.equals("1"))
				remLen=1;
		else if (lastChar.equals("2"))
				remLen=2;
		else if (lastChar.equals("3"))
				remLen=3;
		else if (lastChar.equals("4"))
				remLen=4;
		else if (lastChar.equals("5"))
				remLen=5;
		else if (lastChar.equals("6"))
				remLen=6;
		else if (lastChar.equals("7"))
				remLen=7;
		else if (lastChar.equals("8"))
				remLen=8;
		else if (lastChar.equals("9"))
				remLen=9;
		else if (lastChar.equals("a"))
				remLen=10;
		else if (lastChar.equals("b"))
				remLen=11;
		else if (lastChar.equals("c"))
				remLen=12;
		else if (lastChar.equals("d"))
				remLen=13;
		else if (lastChar.equals("e"))
				remLen=14;
		else if (lastChar.equals("f"))
				remLen=15;	
		
		String finals = data.substring(0,data.length()-remLen);
		return finals;
	}

	public static String encrypt(String plainText,String key) throws Exception
	{
		cipher = Cipher.getInstance("AES/ECB/noPadding");

		String paddedText= addPad(plainText);
		byte[] plainTextByte = paddedText.getBytes();
		cipher.init(Cipher.ENCRYPT_MODE, convertStringToSecretKey(key));
		byte[] encryptedByte = cipher.doFinal(plainTextByte);		
		String encryptedText = Base64.encodeToString(encryptedByte,0);
		return encryptedText;
	}

	public static String decrypt(String encryptedText,String key) throws Exception
	{
		cipher = Cipher.getInstance("AES/ECB/noPadding");

		byte[] encryptedTextByte = Base64.decode(encryptedText, 0);
		cipher.init(Cipher.DECRYPT_MODE, convertStringToSecretKey(key));
		byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
		String decryptedText = new String(decryptedByte);
		String unPadded= removePad(decryptedText);
		return unPadded;
	}

	static SecretKey convertStringToSecretKey(String keyString)
	{
		byte[] keyByte = Base64.decode(keyString, 0);
		SecretKey secretKeyConverted = new SecretKeySpec(keyByte, 0, keyByte.length, "AES");
		return secretKeyConverted;
	}
}