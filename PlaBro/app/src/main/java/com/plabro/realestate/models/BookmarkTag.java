package com.plabro.realestate.models;

/**
 * Created by hemant on 9/2/15.
 */
public class BookmarkTag{
    String id;
    Boolean bookmarked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getBookmarked() {
        return bookmarked;
    }

    public void setBookmarked(Boolean bookmarked) {
        this.bookmarked = bookmarked;
    }
}
