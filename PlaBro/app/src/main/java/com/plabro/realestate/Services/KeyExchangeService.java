package com.plabro.realestate.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.aes.AESResponse;
import com.plabro.realestate.models.securitykeysharing.KeySharingResponse;
import com.plabro.realestate.receivers.PBLocalReceiver;
import com.plabro.realestate.security.RSAEncryption;
import com.plabro.realestate.uiHelpers.images.handlers.AsyncTask;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.HashMap;

/**
 * Created by nitin on 10/02/16.
 */
public class KeyExchangeService extends Service {
    private static final String TAG = "KeyExchangeService";
    private String clientPrivKey = "";
    private String clientPubKey;
    private int retryingSharing = 0;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static void startService(Context ctx) {
        if (!PBPreferences.getData(PBPreferences.KEY_AES_PROCESS, false)) {
            ctx.startService(new Intent(ctx, KeyExchangeService.class));
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        generateAndSaveClientKeys();
        return START_NOT_STICKY;
    }

    private void generateAndSaveClientKeys() {
        try {
            if (TextUtils.isEmpty(PBPreferences.getClientPrivateKey()) || TextUtils.isEmpty(PBPreferences.getClientPublicKey())) {

                new AsyncTask<String, String, String>() {
                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();

                    }

                    @Override
                    protected String doInBackground(String... params) {
                        RSAEncryption rsa = null;
                        try {
                            rsa = new RSAEncryption();

                            //generate public and private key for client
                            rsa.generateKey();
                            //set private key for client
                            clientPrivKey = rsa.getPrivateKey();
                            Log.d(TAG, "Clinet Private Key :" + clientPrivKey);
                            PBPreferences.setClientPrivateKey(clientPrivKey);
                            Log.d(TAG, "private key for client set successfully");

                            //set public key for client
                            clientPubKey = rsa.getPublicKey();
                            if (!TextUtils.isEmpty(clientPubKey)) {

                                PBPreferences.setClientPublicKey(clientPubKey);
                            } else {
                                PBPreferences.setClientPublicKey(PBPreferences.getImei());
                                Log.d(TAG, "client public key generation failed!!!");
                            }
                            Log.d(TAG, "public key for client set successfully");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        // get and set  public key to server
                        sharePublicKeysBWClientAndServer("", "");
                    }
                }.execute();


            } else {

                if (TextUtils.isEmpty(PBPreferences.getServerPublicKey())) {
                    sharePublicKeysBWClientAndServer("", "");

                } else if (PBPreferences.getClientKeySentToServer()) {

                    sharePublicKeysBWClientAndServer(PBPreferences.getClientPublicKey(), PBPreferences.getImei());

                } else {
                    String sign = PBPreferences.getEncSignature();
                    String aes = PBPreferences.getAESKey();

                    if (TextUtils.isEmpty(sign) || TextUtils.isEmpty(aes)) {
                        getAESKeyFromServer();
                    } else {
                    }
                }


                // boolean status = checkSignature();

//                if (!status) {
//                    getAESKeyFromServer(PBPreferences.getImei(mContext));
//                } else {
//                    redirectToHome();
//                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void sharePublicKeysBWClientAndServer(final String clientKey, final String imei) {

        if (TextUtils.isEmpty(clientKey) && null != PBPreferences.getServerPublicKey() && !TextUtils.isEmpty(PBPreferences.getServerPublicKey())) {
            Log.d(TAG, "Step 1 : public key from server already set successfully");
            //send client public key to server
            sharePublicKeysBWClientAndServer(PBPreferences.getClientPublicKey(), PBPreferences.getImei());
            return;
        }

        if (clientKey.equalsIgnoreCase("")) {
            Log.v(TAG, "Getting server public key ...........");
        } else {
            Log.v(TAG, "Sending client public key to server .........");

        }
        if (!Util.haveNetworkConnection(KeyExchangeService.this)) {
            return;
        }


        HashMap<String, String> params = new HashMap<String, String>();
        Log.d(TAG, "Key Exchanging :" + imei + ", Client Key :" + clientKey);

        if (!TextUtils.isEmpty(clientKey)) {
            params.put(PlaBroApi.PLABRO_KEY_EXCHANGE_POST_PARAM_CLIENT_KEY, clientKey);
        }
        if (!TextUtils.isEmpty(imei)) {
            params.put(PlaBroApi.PLABRO_KEY_EXCHANGE_POST_PARAM_IMEI, imei);

        }
        params = Utility.getInitialParamsWithoutIMEI(PlaBroApi.RT.KEYEXCHANGE, params);
        AppVolley.processRequest(Constants.TASK_CODES.KEY_EXCHANGE, KeySharingResponse.class, null, PlaBroApi.getBaseUrl(KeyExchangeService.this), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {

                KeySharingResponse keySharingResponse = (KeySharingResponse) response.getResponse();

                if (keySharingResponse.isStatus()) {

                    if (TextUtils.isEmpty(clientKey)) {
                        String serverPubKey = keySharingResponse.getOutput_params().getData().getKey();
                        //save server pub key
                        Log.d(TAG, "Step 1 : public key from server set successfully");
                        PBPreferences.setServerPublicKey(serverPubKey);

                        //send client public key to server
                        sharePublicKeysBWClientAndServer(PBPreferences.getClientPublicKey(), PBPreferences.getImei());
                    } else {
                        Log.d(TAG, "Step 2 : client key sent to server successfully");
                        PBPreferences.setClientKeySentToServer(true);

                        //get AES key from server
                        getAESKeyFromServer();
                    }
                } else {
                    reHitServer(clientKey);
                }

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

                reHitServer(clientKey);
            }
        });
    }


    private void reHitServer(String clientKey) {

        if (retryingSharing > 5) {
            //reset keys to start the process again
            // PBPreferences.setClientPrivateKey("");
            //PBPreferences.setClientPublicKey("");
//            showGenericErrorMessage("Error! Please try again.");
            sendFailureBroadcast();
            stopSelf();
            return;
        }
        retryingSharing++;

        if (!Util.haveNetworkConnection(KeyExchangeService.this)) {
            return;
        } else {
            Log.d(TAG, "Just hold on our system is a bit slow currently.");
//            mStatusMessage.setText("Just hold on our system is a bit slow currently.");
        }
        if (TextUtils.isEmpty(clientKey)) {
            Log.d(TAG, "getting server public key failed");

            sharePublicKeysBWClientAndServer("", "");

        } else {
            Log.d(TAG, "sending client public key failed");

            sharePublicKeysBWClientAndServer(PBPreferences.getClientPublicKey(), PBPreferences.getImei());

        }
    }

    private void getAESKeyFromServer() {

        Log.v(TAG, "Getting aes key from server  ...........");

        if (!Util.haveNetworkConnection(AppController.getInstance())) {
            return;
        }

        HashMap<String, String> params = new HashMap<String, String>();
        params = Utility.getInitialParamsWithoutAuthkey(PlaBroApi.RT.AESKEY, params);

        AppVolley.processRequest(Constants.TASK_CODES.AESKEY, AESResponse.class, null, PlaBroApi.getBaseUrl(KeyExchangeService.this), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {


                Log.d(TAG, "Success Response:AES Key");
                AESResponse aesResponse = (AESResponse) response.getResponse();

                String aesKey = aesResponse.getOutput_params().getData().getKey();
                String sign = aesResponse.getOutput_params().getData().getSignature();

                PBPreferences.setAESKey(aesKey);
                PBPreferences.setEncSignature(sign);

                Log.d(TAG, "Step 3 : aes key get from server successfully");

                try {

                    boolean status = checkSignature();
                    if (status) {

                        String clientPubKey = PBPreferences.getClientPublicKey();

                        String clientPrivateKEy = PBPreferences.getClientPrivateKey();
                        try {
                            RSAEncryption rsa = new RSAEncryption(clientPubKey, clientPrivateKEy);
                            String decryptedAes = rsa.decrypt(aesKey);
                            PBPreferences.setAESKeyDecrypted(decryptedAes);

                            Log.d(TAG, "Step 3.1 : aes key decrypted and set : we are good to go");
                            /*String et = Encryption.encrypt("pronnayuiigihb", decryptedAes);
                            String decrypted = Encryption.decrypt(et, decryptedAes);
                            Log.d(TAG, "encrypted string is :" + et);
                            Log.d(TAG, "Decrypted string is :" + decrypted);
                            testEncryption(et);*/

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d(TAG, "Step 3.1 : aes key decryption failed : proceeding without decryption");

                        }

                        PBPreferences.saveData(PBPreferences.KEY_AES_PROCESS, true);
                        sendSuccessBroadcast();
                        stopSelf();
                        return;
                        //we r good to go
//                        redirectToHome(); //todo uncooment above check code after all version shifts

                    } else {
                        Log.d(TAG, "Step 3.1 : oops there are malware who wants to get into your code, get fresh stuff here");
                        //oops there are malware who wants to get into your code, get fresh stuff here
                        //getAESKeyFromServer(PBPreferences.getImei(mContext));
//                        showGenericErrorMessage(response.getCustomException().getMessage());

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                sendFailureBroadcast();

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

                // getAESKeyFromServer(PBPreferences.getImei(mContext));
                sendFailureBroadcast();
            }
        });
    }

    private void sendSuccessBroadcast() {
        ActivityUtils.sendLocalBroadcast(KeyExchangeService.this, PBLocalReceiver.PBActionListener.ACTION_KEY_EXCHANGE_SUCCESS);
    }

    private void sendFailureBroadcast() {
        ActivityUtils.sendLocalBroadcast(KeyExchangeService.this, PBLocalReceiver.PBActionListener.ACTION_KEY_EXCHANGE_FAILURE);
    }

    private boolean checkSignature() {
        try {
            String serverPubkey = PBPreferences.getServerPublicKey();
            RSAEncryption rsa = new RSAEncryption(serverPubkey);
            String aesKey = PBPreferences.getAESKey();
            String sign = PBPreferences.getEncSignature();
            boolean status = rsa.checkSignature(aesKey, sign);
            return status;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


}
