package com.plabro.realestate.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.widget.Toast;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.HomeActivity;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.Util;

import java.util.HashMap;


public class DeepLinking extends Activity {

    public static final String TAG = "DeepLinking";
    Bundle map = new Bundle();
    // uri host & syntaxes
    public static final String host = "plabro.com";
    final String shout = "shout";
    final String user = "user";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "on create");
        if (PBPreferences.getFirstTimeState()) {
            finish();
            return;
        }
        Intent webIntent = getIntent();
        String action = webIntent.getAction();
        Uri uri = webIntent.getData();
        Log.d(TAG, "URI:" + uri);
        Util.pringIntentData(webIntent);
//        showURLDialog();

        if (null != uri) {
            String uriString = uri.toString();
            Log.d(TAG, "Path:" + uri.getPath() + ", Scheme:" + uri.getScheme());
            if (uri.getScheme().equalsIgnoreCase("app") || uri.getScheme().equalsIgnoreCase("dl")) {

                if (uriString.contains("?")) {
                    map = getMap(uriString);
                }
                map = getMap(uriString);
                redirectToActivity(map);
            } else if (uri.getPath().contains("app") || uri.getPath().contains("dl")) {
                if (uriString.contains("?")) {
                    map = getMap(uriString);
                }
                map = getMap(uriString);
                redirectToActivity(map);
            } else if (uriString.contains("?")) {
                map = getMap(uriString);
                redirectToActivity(map);
            } else if (uri.getHost().contains(host)) {

                if (uri.getPathSegments().size() > 0) {
                    String uriScheme = uri.getPathSegments().get(0);
                    Log.d(TAG, uri + "++++" + uri.getPath() + "++++" + uri.getHost());
                    String hash = "";
                    for (String val : uri.getPathSegments()) {
                        if (val.contains("html")) {
                            hash = val.replace(".html", "");
                        }
                    }

                    if (hash.equalsIgnoreCase("")) {
                        Log.d(TAG, "hash not found");
                        navigateToHomeScreen();
                    } else {

                        switch (uriScheme) {
                            case user:
                                navigateToProfile(hash);
                                break;

                            case shout:
                                navigateToSearch(hash);
                                break;

                            default:
                                Log.d(TAG, "scheme not found");
                                navigateToHomeScreen();
                                break;
                        }

                    }


                } else {
                    Log.d(TAG, "path not found");

                    navigateToHomeScreen();
                }


            } else {
                Log.d(TAG, "host not found");

                navigateToHomeScreen();

            }

        } else {
            Log.d(TAG, "URI is null");
            navigateToHomeScreen();
        }


    }

    private void showURLDialog() {
        Uri uri = getIntent().getData();

        final StringBuilder builder = new StringBuilder();
        builder.append("TAG : " + TAG + "\n");
        builder.append("URI : " + uri + "\n");
        Bundle b = getIntent().getExtras();
        if (b != null) {
            for (String key : b.keySet()) {
                builder.append("KEY : " + key + " , VALUE : " + b.get(key) + "\n");
                Log.d("Intent Data:", key + "," + b.get(key));
                if (b.get(key) != null && b.get(key) instanceof Bundle) {
                    Bundle internalBundle = (Bundle) b.get(key);
                    for (String k : internalBundle.keySet()) {
                        builder.append("INTTERNAL KEY : " + k + " , VALUE : " + internalBundle.get(k) + "\n");
                    }
                }
            }
        } else {
            builder.append("Bundle Received is null");
            Log.d("Intent Data:", "Bundle is null");
        }
        Toast.makeText(this, "Deep Link Test Data is\n" + builder.toString(), Toast.LENGTH_LONG).show();
    }

    private void redirectToActivity(Bundle b) {
        if (b != null) {
            if (b.keySet().size() > 0) {
                if (b.containsKey("act")) {
                    String type = b.getString("act");
                    b.putBoolean(PBPreferences.FROM_DEEP_LINKING, true);
                    redirectToActivity(type, b);
                    return;
                }
            }
        }
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    private void redirectToActivity(String type, Bundle b) {


        if (TextUtils.isEmpty(type)) {

        } else {
            b.putBoolean("fromNotification", true);

            switch (type) {
                case ACT_TYPE.UPDATE:
                    if (b.containsKey("version")) {
                        b.putString("act", type);
                        b.putString("version", b.getString("version"));
                    }
                    break;
                case ACT_TYPE.CHAT:
                    if (b.containsKey("phone") && b.containsKey("name")) {
                        b.putString("act", type);
                        b.putString("phone_key", b.getString("phone"));
                        b.putString("name", b.getString("name"));
                    } else {
                    }
                    break;
                case ACT_TYPE.SEARCH:
                    if (b.containsKey("searchString")) {
                        b.putString("act", type);
                        b.putString("searchText", b.getString("searchString"));
                        if (b.containsKey("filter")) {
                            b.putString("filter", b.getString("filter"));
                        }
                    } else {
                        b = new Bundle();
                    }
                    break;
                case ACT_TYPE.COMPOSE:
                    if (b.containsKey("shoutText")) {
                        b.putString("act", type);
                        // The below line is to help Shout.java to fill the text
                        // Same Code is already implemented for filling the text
                        // for search cases
                        b.putString("searchText", b.getString("shoutText"));
                        b.putBoolean("searchFlag", true);
                    }
                    break;
                case ACT_TYPE.HASH:
                    if (b.containsKey("tagString")) {
                        b.putString("act", type);
                        b.putString("refKey", "");
                        Log.d(TAG, b.getString("tagString") + "," + b.getString("messageId"));
                        b.putString("hashTitle", b.getString("tagString"));
                        b.putString("message_id", b.getString("messageId"));
                    }
                    break;
                case ACT_TYPE.HOME:
                    b.putString("act", type);
                    break;
                case ACT_TYPE.SHOUT:
                    if (b.containsKey("messageId")) {
                        b.putString("act", type);
                        b.putString("message_id", b.getString("messageId"));
                    }
                    break;
                case ACT_TYPE.UPLOAD:
                    b.putString("act", type);
                    break;
                case ACT_TYPE.AUCTION:
                    b.putString("act", type);
                    break;
                case ACT_TYPE.AUCTION_DETAIL:
                    b.putString("act", type);
                    break;
                default:
                    b.putString("act", type);
            }
        }
        Intent i = new Intent(DeepLinking.this, MainActivity.class);
        i.putExtras(b);
        startActivity(i);
        finish();
    }

    private Intent getDefaultLandingIntent() {
        Intent intent = new Intent(DeepLinking.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);

        return intent;
    }

    private Bundle getMap(String uriString) {
        try {
            String[] splittedString = uriString.split("\\?");
            String suffixString = splittedString[1];
            splittedString = suffixString.split("&");
            for (String s : splittedString) {
                String[] array = s.split("=");
                map.putString(array[0], array[1]);
            }
        } catch (Exception e) {

        }
        return map;
    }


    private void navigateToHomeScreen() {
        startActivity(getDefaultLandingIntent());
        finish();
        return;
    }

    private void navigateToProfile(String hash) {
        Intent intent = new Intent(DeepLinking.this, UserProfileNew.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
        intent.putExtra("deep_hash", hash);
        startActivity(intent);
        finish();
        return;
    }

    private void navigateToSearch(String hash) {
        Intent intent = new Intent(DeepLinking.this, SearchFeeds.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
        intent.putExtra("deep_hash", hash);
        startActivity(intent);
        finish();
        return;
    }

    public static interface ACT_TYPE {
        String UPDATE = "update";
        String UPLOAD = "upload";
        String HOME = "home";
        String SEARCH = "search";
        String COMPOSE = "compose";
        String HASH = "hash";
        String CHAT = "chat";
        String SHOUT = "shout";
        String AUCTION = "auc";
        String AUCTION_DETAIL = "ad";
        String PROFILE = "profile";
        String NOTIF = "notif";
        String WALLET = "wallet";
        String DIRECT_LISTING = "dl";
        String SERVICES = "services";
        String NEWS = "news";
        String BLOGS = "blogs";
        String PLABRO_WEB = "pweb";
        String P_SCAN = "scan";
        String URI_GCM_DEEP_LINK = "dlink";
        String GCM_AD_URL = "gcm_ad_url";
        String BROKER_CONTACTS_SERVICE = "bcs";
    }


}
