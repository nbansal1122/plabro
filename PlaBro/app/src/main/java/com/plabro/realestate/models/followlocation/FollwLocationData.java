package com.plabro.realestate.models.followlocation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollwLocationData {

    @SerializedName("RT")
    @Expose
    private String RT;
    @SerializedName("following_type")
    @Expose
    private String following_type;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("uid ")
    @Expose
    private String uid_;

    /**
     * @return The RT
     */
    public String getRT() {
        return RT;
    }

    /**
     * @param RT The RT
     */
    public void setRT(String RT) {
        this.RT = RT;
    }

    /**
     * @return The following_type
     */
    public String getFollowing_type() {
        return following_type;
    }

    /**
     * @param following_type The following_type
     */
    public void setFollowing_type(String following_type) {
        this.following_type = following_type;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The uid_
     */
    public String getUid_() {
        return uid_;
    }

    /**
     * @param uid_ The uid
     */
    public void setUid_(String uid_) {
        this.uid_ = uid_;
    }

}