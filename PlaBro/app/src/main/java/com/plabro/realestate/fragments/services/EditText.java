package com.plabro.realestate.fragments.services;

import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.plabro.realestate.R;
import com.plabro.realestate.utilities.PBPreferences;

/**
 * Created by nitin on 24/11/15.
 */
public class EditText extends BaseServiceFragment {
    private android.widget.EditText editText;

    @Override
    public String getTAG() {
        return "Edit Text";
    }

    @Override
    protected void findViewById() {
        super.findViewById();
        addLayout(getLayout());

    }

    private View getLayout() {
        editText = (android.widget.EditText) LayoutInflater.from(getActivity()).inflate(R.layout.layout_edit_text, null);
        editText.setHint(q.getTitle());
        selectedValue = PBPreferences.getData(q.getQ_id(), null);
        editText.setHint(q.getHint());
        if (!TextUtils.isEmpty(q.getInput_type())) {
            if ("number".equals(q.getInput_type())) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            }
        }
        if (!TextUtils.isEmpty(selectedValue)) {
            editText.setText(selectedValue);

        }else if(!TextUtils.isEmpty(q.getDefaultValue())){
            editText.setText(q.getDefaultValue());
        }
        return editText;
    }

    @Override
    protected String getSelectedValue() {
        selectedValue = editText.getText().toString().trim();
        if (TextUtils.isEmpty(selectedValue) && !TextUtils.isEmpty(q.getDefaultValue())) {
            selectedValue = q.getDefaultValue();
        }
        PBPreferences.saveData(q.getQ_id(), selectedValue);

        return selectedValue;
    }

    @Override
    protected String getErrorMessage() {
        return "Field can not be empty";
    }
}
