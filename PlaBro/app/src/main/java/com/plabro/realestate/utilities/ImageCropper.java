package com.plabro.realestate.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Shader;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;

import com.isseiaoki.simplecropview.CropImageView;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;

public class ImageCropper extends PlabroBaseActivity {

    public static final String PICK_FROM_GALLARY = "pickFromGallery";
    public static final String IS_SCALE_DOWN = "isScaleDown";

    public static final int REQUEST_CODE_PICK_IMAGE = 121;

    private CropImageView cropImageView;
    private Uri uri;
    public static final int REQ_CROP = 4;
    public static Bitmap croppedBitmap;
    public Bitmap bMap;
    private boolean isMediaUri;
    private boolean isScaleDown;

    public static void startActivity(Activity ctx, boolean isScaleDown, boolean isMediaUri) {
        Bundle b = new Bundle();
        b.putBoolean(PICK_FROM_GALLARY, isMediaUri);
        b.putBoolean(IS_SCALE_DOWN, isScaleDown);
        Intent i = new Intent(ctx, ImageCropper.class);
        i.putExtras(b);
        ctx.startActivityForResult(i, REQUEST_CODE_PICK_IMAGE);
    }


    @Override
    protected String getTag() {
        return "ImageCropper";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_cropper);
        inflateToolbar();
        setTitle("Crop Image");
        loadBundle(getIntent().getExtras());
        cropImageView = (CropImageView) findViewById(R.id.cropImageView);
        Log.i("msg", "oncreate is call uri=" + uri + " bMap=" + bMap);

    }

    @Override
    protected int getMenuIdToInflate() {
        return R.menu.menu_image_cropper;
    }

    @Override
    protected boolean onMenuItemClicked(MenuItem item, int id) {
        switch (id) {
            case R.id.action_done:
                onMenuClicked();
                return true;
        }
        return false;
    }

    private void onMenuClicked() {
        Bitmap bMap = cropImageView.getCroppedBitmap();
        if (bMap != null) {
            croppedBitmap = bMap;
            Intent i = new Intent();
            setResult(RESULT_OK, i);
        } else {
            setResult(RESULT_CANCELED);
        }
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.menu.menu_done:
                onMenuClicked();
                return true;
        }

        return false;
    }

    private void createCircleBitmap(Bitmap bitmap, ImageView imageView) {
//        Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        float Measuredwidth = 0;
        float Measuredheight = 0;
        Point size = new Point();
        WindowManager w = getWindowManager();

        if (Build.VERSION.SDK_INT >= 13) {
            w.getDefaultDisplay().getSize(size);
            Measuredwidth = size.x;
            Measuredheight = size.y;
        } else {
            Display d = w.getDefaultDisplay();
            Measuredwidth = d.getWidth();
            Measuredheight = d.getHeight();
        }
//        BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
//        Paint paint = new Paint();
//        paint.setShader(shader);
//
//        Canvas c = new Canvas(circleBitmap);
//        c.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2, paint);
        croppedBitmap = scaleDown(bitmap, Measuredwidth, true);
        imageView.setImageBitmap(croppedBitmap);
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    protected void loadBundle(Bundle b) {
        Log.i("msg", "loadBunble is call");
        isMediaUri = b.getBoolean(PICK_FROM_GALLARY);
        isScaleDown = b.getBoolean(IS_SCALE_DOWN);
        if (isMediaUri) {
            Util.getImageFromGallery(this);
        } else {
            uri = Util.getImageFromCamera(this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            setResult(RESULT_CANCELED);
            finish();
            return;
        }
        switch (requestCode) {
            case Util.REQUEST_CODE_CAMERA:
                Log.i("msg", "camara uri=" + uri);

                break;
            case Util.REQUEST_CODE_GALLARY:
                uri = data.getData();
                Log.i("msg", "camara uri=" + uri);

                break;
        }
        if (uri != null) {
            if (!isScaleDown) {
                Bitmap bmap = Util.getBitmapFromUri(this, uri);
                createCircleBitmap(bmap, cropImageView);
            } else {
                Bitmap bMap = Util.getBitmapFromUri(this, uri);
                bMap = scaleDown(bMap, 2048, true);
                if (bMap != null) {
                    croppedBitmap = bMap;
                    Intent i = new Intent();
                    setResult(RESULT_OK, i);
                } else {
                    setResult(RESULT_CANCELED);
                }
                finish();
            }
        }

    }

    @Override
    public void findViewById() {

    }

    @Override
    public void reloadRequest() {

    }
}
