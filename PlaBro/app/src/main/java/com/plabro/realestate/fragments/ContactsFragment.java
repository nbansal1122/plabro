package com.plabro.realestate.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.Services.ContactSync;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.adapter.PeopleCustomAdapter;
import com.plabro.realestate.listeners.HidingScrollListener;
import com.plabro.realestate.models.Contact;
import com.plabro.realestate.models.contacts.PlabroFriends;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.xmpp.RosterManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;


public class ContactsFragment extends MasterFragment {

    private OnFragmentInteractionListener mListener;
    private LayoutInflater mInflater;
    private RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    private PeopleCustomAdapter mAdapter;
    private ArrayList<Contact> contacts = new ArrayList<Contact>();
    private HashMap<String, String> contactsMap = new HashMap<>();
    private List<PlabroFriends> plabroFriends = new ArrayList<PlabroFriends>();
    List<PlabroFriends> plabroFriendsFilteredName = new ArrayList<PlabroFriends>();
    HashSet<String> mobileNumbers = new HashSet<>();

    public static final String BROADCAST_ACTION_SYNC_CONTACTS = "com.plabro.realestate.syncContacts";
    private static Context mContext;
    private static Toolbar mToolbar;
    SwipeRefreshLayout mSwipeRefreshLayout;
    RelativeLayout noFriendsLayout;
    private static SearchBoxCustom search;


    private static final String TAG = "ContactsFragment";

    @Override
    public String getTAG() {
        return "ContactsFragment";
    }

    public static ContactsFragment getInstance(Context mContext, Toolbar mToolbar, SearchBoxCustom searchbox) {
        Log.i(TAG, "Creating New Instance");
        ContactsFragment mContactsFragment = new ContactsFragment();
        ContactsFragment.mContext = mContext;
        ContactsFragment.mToolbar = mToolbar;
        search = searchbox;

        return mContactsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerMarshMallowReceiver();
        //Analytics.trackScreen(getActivity(), getActivity().getResources().getString(R.string.s_friends));
        setHasOptionsMenu(true);
        mInflater = LayoutInflater.from(getActivity());
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_contacts;
    }

    public void setRecyclerViewLayoutManager() {
        int scrollPosition = 0;

        mLayoutManager = new LinearLayoutManager(getActivity());

        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
        mRecyclerView.setOnScrollListener(hidingScrollListener);

    }

    protected HidingScrollListener hidingScrollListener = new HidingScrollListener(20) {
        @Override
        public void onHide(RecyclerView recyclerView, int dx, int dy) {
            Log.d(TAG, "ON Hide Hiding scroll listener");

        }

        @Override
        public void onShow(RecyclerView recyclerView, int dx, int dy) {
            Log.d(TAG, "ON Show Hiding scroll listener");

        }

        @Override
        public void onScrolledFire(RecyclerView recyclerView, int dx, int dy) {

//            if (mLayoutManager.findFirstCompletelyVisibleItemPosition() > 8) {
//                Analytics.trackScreen(Analytics.Contacts.ContactsScrolled);
//            }
        }
    };

    @Override
    protected void findViewById() {
        Analytics.trackScreen(Analytics.OtherScreens.Contacts);
        filter.addAction(BROADCAST_ACTION_SYNC_CONTACTS);
        getActivity().registerReceiver(rec, filter);
        mInflater = LayoutInflater.from(getActivity());
        noFriendsLayout = (RelativeLayout) findView(R.id.noFriends);

        //mProgressWheel.spin();

        //pull to refresh
        mSwipeRefreshLayout = (SwipeRefreshLayout) findView(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshLayout.setRefreshing(false);
                        try {

                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Refresh contacts.")
                                    .setMessage("Do you want to sync your plabro contacts. This will take a while.")
                                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                            HashMap<String, Object> data = new HashMap<>();
                                            data.put("ScreenName", TAG);
                                            data.put("Action", "No");
                                            Analytics.trackSocialEvent(Analytics.Actions.Refreshed, data);
                                        }
                                    }) // dismisses by default
                                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            noFriendsLayout.setVisibility(View.GONE);
                                            HashMap<String, Object> data = new HashMap<>();
                                            data.put("ScreenName", TAG);
                                            data.put("Action", "Yes");
                                            Analytics.trackSocialEvent(Analytics.Actions.Refreshed, data);

                                            if (!ContactSync.isStarted) {
                                                grantPermission(PlaBroApi.PERMISSIONS.READ_CONTACTS);

                                            } else {
                                                refreshSwipeView();
                                                showToast("Already Syncing Contacts");
                                            }
                                            //getContactsInBackground();
                                        }
                                    })
                                    .create()
                                    .show();
                        } catch (Exception e) {

                        }

                    }
                }, Constants.GOOGLE_PULL_TO_REFRESH_TIMER);

            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        mRecyclerView = (RecyclerView) findView(R.id.rv_people);
        setRecyclerViewLayoutManager();


        if (rootView != null) {

            //showPlabroContactsList(); // todo uncomment this line and comment all inside if condition after few versions

            setListAdapter();

        }
        grantPermission(PlaBroApi.PERMISSIONS.READ_CONTACTS);

    }

    void grantPermission(int permission) {

        switch (permission) {
            case PlaBroApi.PERMISSIONS.READ_CONTACTS:
                hasPermission(Manifest.permission.READ_CONTACTS, PlaBroApi.PERMISSIONS.READ_CONTACTS, PlaBroApi.PERMISSIONS_MESSAGE.READ_CONTACTS);

                break;
            case PlaBroApi.PERMISSIONS.WRITE_CONTACTS:
                hasPermission(Manifest.permission.WRITE_CONTACTS, PlaBroApi.PERMISSIONS.WRITE_CONTACTS, PlaBroApi.PERMISSIONS_MESSAGE.WRITE_CONTACTS);

                break;

        }

    }


    @Override
    public void onMMPermisssoinResult(int requestCode, boolean status, String message) {
        super.onMMPermisssoinResult(requestCode, status, message);
        Log.d(TAG, "On Permission Result" + requestCode);
        switch (requestCode) {
            case PlaBroApi.PERMISSIONS.READ_CONTACTS:
                if (!status) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    showSnackBar("Permission denied for READ_CONTACTS");
                    return;
                }

                if (noFriendsLayout.getVisibility() == View.VISIBLE) {
                    noFriendsLayout.setVisibility(View.GONE);
                }
                refreshSwipeView();
                if (!ContactSync.isStarted) {
                    ContactSync.startContactSyncing(getActivity());
                }
                break;

        }

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    int loopCounter = 0;


    private String modifyContact(String mobileNumber) {
        String mobile = mobileNumber.replace("(", "").replace(")", "").replace("-", "").replace(" ", "");
        return mobile;
    }

    private String getLastTenDigits(String number) {
        int length = number.length();
        if (length > 9) {
            String updatedNumber = number.substring(length - 10, length);
            Log.d(TAG, "Updated Number : " + updatedNumber);
            return updatedNumber;
        } else {
            return number;
        }
    }

    private void setListAdapter() {
        plabroFriends = new Select().from(PlabroFriends.class).execute();
        if (null != plabroFriends && plabroFriends.size() > 0) {
            noFriendsLayout.setVisibility(View.GONE);
            Collections.sort(plabroFriends, new MyComparator());
            mAdapter = new PeopleCustomAdapter(getActivity(), getActivity(), plabroFriends);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            noFriendsLayout.setVisibility(View.VISIBLE);
        }

    }

    synchronized void showPlabroContactsList() {

        plabroFriends = new Select().from(PlabroFriends.class).execute();
        contactsMap = new HashMap<>();
        contactsMap.putAll(ContactSync.allContacts);
        plabroFriendsFilteredName = new ArrayList<PlabroFriends>();
        if (null != plabroFriends && plabroFriends.size() > 0) {
            for (PlabroFriends pf : plabroFriends) {
                String displayName = "";

                String phoneTypeOne = pf.getPhone();
                displayName = ActivityUtils.getDisplayNameFromPhone(pf.getPhone());
                if (TextUtils.isEmpty(displayName)) {
                    if (contactsMap.containsKey(phoneTypeOne)) {
                        displayName = contactsMap.get(phoneTypeOne);
                    } else {
                        displayName = pf.getName().trim();
                        if (displayName != null && TextUtils.isEmpty(displayName)) {
                            displayName = pf.getPhone();
                        }
                    }
                }
                pf.setName(displayName);
                pf.save();
                plabroFriendsFilteredName.add(pf);

            }
            Collections.sort(plabroFriendsFilteredName, new MyComparator());
            List<PlabroFriends> clonedFriends = new ArrayList<>();
            clonedFriends.addAll(plabroFriendsFilteredName);

            mAdapter = new PeopleCustomAdapter(getActivity(), getActivity(), plabroFriendsFilteredName);
            mRecyclerView.setAdapter(mAdapter);
            if (PBPreferences.getData(PBPreferences.KEY_ERROR_SYNC_CONTACTS, false)) {
                noFriendsLayout.setVisibility(View.VISIBLE);
                ContactSync.stopContactSyncing(getActivity());
            } else {
                noFriendsLayout.setVisibility(View.GONE);
            }
            noFriendsLayout.setVisibility(View.GONE);
            addToRoster(clonedFriends);
        } else {
            noFriendsLayout.setVisibility(View.VISIBLE);
        }


    }

    private void addToRoster(final List<PlabroFriends> plabroFriends) {
        RosterManager rm = RosterManager.getInstance();
        boolean isConnected = rm.getConnection() != null && rm.getConnection().isConnected() && rm.getConnection().isAuthenticated();
        if (isConnected && plabroFriends != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (PlabroFriends pf : plabroFriends)
                        try {
                            RosterManager.createEntryIfNotFound(ActivityUtils.getTo(pf.getPhone()), pf.getName(), null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                }
            }).start();

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflateToolbar(menu);
    }

    private void inflateToolbar(Menu menu) {
        mToolbar.inflateMenu(R.menu.menu_contacts);
        Log.v(TAG, "toolbar inflated");

        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.action_search:
                        openSearch();
                        break;

                    default:
                        break;

                }
                return true;
            }

        });
    }


    public void openSearch() {
        search.clearSearchable(); // clear all the searchable history previously present

        search.revealFromMenuItem(R.id.action_search, getActivity());
        search.setMenuListener(new SearchBoxCustom.MenuListener() {

            @Override
            public void onMenuClick() {
                if (search.isShown()) {
                    if (isRunning) {
                        search.hideCircularly(getActivity());
                    }
                }

            }

        });
        search.setSearchListener(new SearchBoxCustom.SearchListener() {


            @Override
            public void onSearchOpened() {
                // Use this to tint the screen

            }

            @Override
            public void onSearchClosed() {
                // Use this to un-tint the screen
                if (mAdapter != null) {
                    mAdapter.filter("");
                }
            }

            @Override
            public void onSearchTermChanged(String searchTerm) {
                // React to the search term changing
                // Called after it has updated results
                if (mAdapter != null) {
                    mAdapter.filter(searchTerm);
                }
            }


            @Override
            public void onSearch(String searchTerm) {

                if (mAdapter != null) {
                    mAdapter.filter(searchTerm);
                    Analytics.trackScreen(Analytics.Contacts.Searched);

                }
            }

            @Override
            public void onSearchCleared() {
                if (mAdapter != null) {
                    mAdapter.filter("");
                }
            }

        });

    }


    public class MyComparator implements Comparator<PlabroFriends> {
        @Override
        public int compare(PlabroFriends p1, PlabroFriends p2) {

            if (null != p1 && null != p2 && p1.getName() != null && p2.getName() != null) {
                return p1.getName().compareTo(p2.getName());
            }
            return 0;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (null != search && search.isShown()) {
            search.hideCircularly(getActivity());
        }
    }

    private void refreshSwipeView() {
        if (!ContactSync.isStarted) {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
        }
    }

    void showNoDataDialog(final Boolean isUploadingContacts) {
        Toast.makeText(getActivity(), "Some error in syncing. Please try again.", Toast.LENGTH_LONG).show();
        stopSpinners();

    }

    public class ContactReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (BROADCAST_ACTION_SYNC_CONTACTS.equals(intent.getAction())) {
                if (intent.getBooleanExtra("isLast", false)) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    stopSpinners();

                    //showToast("Contact syncing completed");
                } else {
                    refreshSwipeView();
                }
                showPlabroContactsList();
            }
        }
    }

    ContactReceiver rec = new ContactReceiver();
    IntentFilter filter = new IntentFilter();

    @Override
    public void onStart() {
        super.onStart();
        Analytics.trackScreen(Analytics.Contacts.ContactsScreen);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegsiterMarshMallowReceiver();
        getActivity().unregisterReceiver(rec);
    }

    protected void stopSpinners() {

        try {
            mSwipeRefreshLayout.setRefreshing(false);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Profile Action Received");
        if (Constants.ACTION_PROFILE_UPDATE.equals(intent.getAction())) {
            ((PlabroBaseActivity) getActivity()).setTitle("Contacts");
        }
    }


}
