package com.plabro.realestate.models.login;


import com.plabro.realestate.models.ServerResponse;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class LoginResponse extends ServerResponse {

    LoginOutputParams output_params = new LoginOutputParams();


    public LoginOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(LoginOutputParams output_params) {
        this.output_params = output_params;
    }
}
