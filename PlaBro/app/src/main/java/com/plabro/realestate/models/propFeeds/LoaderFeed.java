package com.plabro.realestate.models.propFeeds;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by hemant on 18/08/15.
 */
@Table(name = "LoaderFeed")
public class LoaderFeed extends BaseFeed {
    @Column
    private String _id;
    private String text = "feeds";

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

}
