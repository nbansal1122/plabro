package com.plabro.realestate;

import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.activity.CardListActivity;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.LoaderFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ShoutByOthers extends CardListActivity implements ShoutObserver.PBObserver {

    private String mContactId;
    private String mContactName;
    public static final String TAG = "ShoutsByOthers";

    @Override
    public void findViewById() {
        super.findViewById();
        Analytics.trackScreen(Analytics.UserProfile.SHoutByOthers);
    }

    @Override
    protected String getTag() {
        return "ShoutByOthers";
    }


    public void getData() {

        if (addMore && baseFeeds.size() > 0) {
            if (baseFeeds.get(baseFeeds.size() - 1).getType().equalsIgnoreCase(Constants.FEED_TYPE.NO_MORE_DATA)) {
                return;
            }
        }


        Log.d(TAG, "Loading shouts of broker");

        Intent intent = getIntent();
        mContactId = intent.getStringExtra("contactId");
        mContactName = intent.getStringExtra("contactName");

        showSpinners(addMore);

        isLoading = true;

        HashMap<String, String> params = new HashMap<String, String>();

        if (!TextUtils.isEmpty(mContactId)) {
            params.put(PlaBroApi.PLABRO_SHOUT_POST_PARAM_CONTACT_ID, mContactId);
        }
        if (addMore) {
            params.put("startidx", (page) + "");
        } else {
            params.put("startidx", "0");
        }
        params = Utility.getInitialParams(PlaBroApi.RT.SHOUTS, params);

        AppVolley.processRequest(0, FeedResponse.class, null, String.format(PlaBroApi.getBaseUrl(ShoutByOthers.this)), params, RequestMethod.GET, new VolleyListener() {

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                onApiFailure(response, taskCode);
                mProgressWheel.stopSpinning();
                isLoading = false;
                setAndCheckNoDataLayout();
                onApiFailure(response, taskCode);
                try {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 2000ms
                            LoaderFeed lf = new LoaderFeed();
                            lf.setType(Constants.FEED_TYPE.NO_MORE);

                            if (addMore) {
                                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                                baseFeeds.add(lf);
                                mAdapter.notifyDataSetChanged();
                            }
                        }
                    }, 2000);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                mProgressWheel.stopSpinning();
                isLoading = false;
                Log.d(TAG, "Response :" + response);

                FeedResponse feedResponse = (FeedResponse) response.getResponse();

                List<BaseFeed> tempFeeds =
                        (ArrayList<BaseFeed>) feedResponse.getOutput_params().getData();

                if (tempFeeds.size() > 0) {

                    noFeeds.setVisibility(View.GONE);
                    LoaderFeed lf = new LoaderFeed();
                    if (tempFeeds.size() < Util.getDefaultStartIdx()) {
                        lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                    } else {
                        lf.setType(Constants.FEED_TYPE.PROGRESS_MORE);
                    }
                    if (addMore) {

                        baseFeeds.remove(baseFeeds.size() - 1);//for footer
                        baseFeeds.addAll(tempFeeds);
                        baseFeeds.add(lf);
                        mAdapter.notifyDataSetChanged();
                        page = Util.getStartIndex(page);
                        HashMap<String, Object> data = new HashMap<>();
                        data.put("ScreenName", TAG);
                        data.put("Action", "Manual");
                    } else {

                        baseFeeds.clear();
                        baseFeeds.addAll(tempFeeds);

                        baseFeeds.add(lf);
                        page = Util.getDefaultStartIdx();
                        mRecyclerView.setAdapter(mAdapter);
                        if (mAdapter != null) {
                            mAdapter.notifyDataSetChanged();
                            // mAdapter.addItems(baseFeeds);
                        }

                    }


                } else {
                    if (addMore) {
                        LoaderFeed lf = new LoaderFeed();
                        baseFeeds.remove(baseFeeds.size() - 1);//for footer
                        lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                        baseFeeds.add(lf);
                        mAdapter.notifyDataSetChanged();
                    }
                }


                setAndCheckNoDataLayout();

            }

        });


    }

    @Override
    public void inflateToolbar() {
        super.inflateToolbar();
        Intent intent = getIntent();
        mContactId = intent.getStringExtra("contactId");
        mContactName = intent.getStringExtra("contactName");
        getSupportActionBar().setTitle(mContactName);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ShoutObserver.getInstance().remove(this);


    }


    @Override
    protected int getResourceId() {
        return R.layout.activity_related_feeds;
    }
}
