package com.plabro.realestate.holders;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.UserRegister;
import com.plabro.realestate.activity.FeedDetails;
import com.plabro.realestate.activity.Profile;
import com.plabro.realestate.activity.Shout;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.entities.BookmarksDBModel;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.BookmarkTag;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.BookmarkResponse;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBFonts;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by hemantkumar on 17/08/15.
 */
public class FeedsCardHolder extends BaseFeedHolder {
    public static String typeFeedEnabled = "feeds_enabled";
    public static String typeFeedDisabled = "feeds_disabled";
    public static String typeFeedWithTrial = "feeds_with_trial";


    protected static Drawable cardEnabled, cardDisabled;
    protected static int heart_red, heart_grey;
    protected Feeds feedObject;
    protected final RoundedImageView iv_profile_image;
    protected final ImageView iv_options;
    protected final FontText tv_profile_name;
    protected final TextView tv_post_time;
    protected final FontText post_content;
    protected final CardView card_view;
    protected final LinearLayout mMore;
    protected final View mDisableAll, mSelectedIn, mSelectedOut;
    protected final FontText mCall;
    protected final FontText mChat;
    protected final ImageView mBookmark, mEdit;
    protected final LinearLayout mActionsLayout;
    protected final TextView mUserRegistration;
    protected final FontText mTopHash;
    protected final FontText mCopy, mForward;
    protected final FontText mSuggestionInfo;
    protected final LinearLayout mSuggInfoLayout;
    protected final FontText mAvailReq;
    protected final FontText mSaleRent;
    protected final FontText mBusinessName;


//    private final ProgressWheel mProgressWheelMore;
//    private final ImageView noMoreData;


    public FeedsCardHolder(View itemView) {
        super(itemView);
        tv_profile_name = (FontText) itemView.findViewById(R.id.profile_name);
        tv_post_time = (TextView) itemView.findViewById(R.id.post_time);
        post_content = (FontText) itemView.findViewById(R.id.post_content);
        iv_profile_image = (RoundedImageView) itemView.findViewById(R.id.profile_image);
        iv_options = (ImageView) itemView.findViewById(R.id.card_option);
        card_view = (CardView) itemView.findViewById(R.id.card_view);
        mMore = (LinearLayout) itemView.findViewById(R.id.more);
        mDisableAll = (View) itemView.findViewById(R.id.v_disable_all);
        mSelectedIn = (View) itemView.findViewById(R.id.v_selected_in);
        mSelectedOut = (View) itemView.findViewById(R.id.v_selected_out);
        mCall = (FontText) itemView.findViewById(R.id.tv_call);
        mChat = (FontText) itemView.findViewById(R.id.tv_chat);
        mBookmark = (ImageView) itemView.findViewById(R.id.iv_bookmark);
        mEdit = (ImageView) itemView.findViewById(R.id.iv_edit);
        mActionsLayout = (LinearLayout) itemView.findViewById(R.id.ll_actions_layout);
        mUserRegistration = (TextView) itemView.findViewById(R.id.tv_go_ahead);
        mTopHash = (FontText) itemView.findViewById(R.id.tv_top_hash);
        mCopy = (FontText) itemView.findViewById(R.id.tv_copy);
        mForward = (FontText) itemView.findViewById(R.id.tv_share);
        mSuggestionInfo = (FontText) itemView.findViewById(R.id.tv_suggestion_info);
        mSuggInfoLayout = (LinearLayout) itemView.findViewById(R.id.ll_sugg);
        mAvailReq = (FontText) itemView.findViewById(R.id.tv_req_avail);
        mSaleRent = (FontText) itemView.findViewById(R.id.tv_sale_rent);
        mBusinessName = (FontText) itemView.findViewById(R.id.tv_buis_name);

//        mProgressWheelMore = (ProgressWheel) findView(R.id.progress_wheel_load_more);
//        mProgressWheelMore.setBarColor(ctx.getResources().getColor(R.color.colorPrimary));
//        mProgressWheelMore.setProgress(0.0f);
//        noMoreData = (ImageView) itemView.findViewById(R.id.noMoreData);
    }


    @Override
    public void onBindViewHolder(final int position, final BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        if (feed instanceof Feeds) {
            Log.d("FCH", "Feed Object is an instance of Feeds Class");
            FeedsCardHolder holder = this;
            final Feeds feedEnabled = (Feeds) feed;
            feedObject = (Feeds) feed;
            if (null == cardType) {
                cardType = typeFeedEnabled;
            }
            setupFeedDetailsCard(feedObject, holder, position, false);


            if (cardType.equalsIgnoreCase(typeFeedWithTrial)) {
                Log.d(TAG, "Card Type is Type With Trial");
                if (!feedObject.isRel()) {
                    setupClickListener(holder, position, feedObject);
                } else {
                    holder.post_content.setEnabled(false);

                }

                int val = PBPreferences.getTrialVersionState();
                if (val == 0) {

                    holder.mUserRegistration.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(ctx, UserRegister.class);
                            ctx.startActivity(intent);
                        }
                    });
                } else {
                    holder.mUserRegistration.setOnClickListener(null);
                }
            } else if (cardType.equalsIgnoreCase(typeFeedEnabled)) {
                Log.d(TAG, "Card Type is Type Feed Enabled");
                setupClickListener(holder, position, feedEnabled);
            } else {
                Log.d(TAG, "Card Type is No Type, post content is false");
                holder.post_content.setEnabled(false);
            }


        } else {
            Log.d("FCH", "Feed Object is not an instance of Feeds Class");
            return;
        }
    }

    protected void setupFeedDetailsCard(Feeds feed, FeedsCardHolder holder, final int position, boolean isDisabled) {
        String imgUrl = feed.getProfile_img();

        heart_red = R.drawable.ic_card_bookmark_pressed;
        heart_grey = R.drawable.ic_card_bookmark_normal;
        cardEnabled = ctx.getResources().getDrawable(R.drawable.gen_button_selector_grey);
        cardDisabled = ctx.getResources().getDrawable(R.drawable.gen_button_selector_disabled);

        //holder.iv_profile_image.setImageResource(R.drawable.profile_default_one);
        if (!TextUtils.isEmpty(imgUrl)) {
            Picasso.with(ctx).load(imgUrl).placeholder(R.drawable.profile_default_one).into(holder.iv_profile_image);
        } else {
            holder.iv_profile_image.setImageResource(R.drawable.profile_default_one);
        }

        String mUserName = "";
        String displayName = ActivityUtils.getDisplayNameFromPhone(feed.getProfile_phone());
        if (!TextUtils.isEmpty(displayName)) {
            mUserName = displayName;
        } else {

            mUserName = feed.getProfile_name();
            if (TextUtils.isEmpty(mUserName)) {
                mUserName = feed.getProfile_phone();
            }

        }
        holder.tv_profile_name.setText(Util.toDisplayCase(mUserName));
        holder.mBusinessName.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(feed.getBusiness_name())) {
            holder.mBusinessName.setText(Util.toDisplayCase(feed.getBusiness_name()));
        } else {
            holder.mBusinessName.setVisibility(View.GONE);
        }
        holder.mCall.setTag(feed);
        holder.mChat.setTag(feed);
        holder.card_view.setTag(feed);
        holder.iv_profile_image.setTag(feed);
        holder.tv_profile_name.setTag(feed);
        holder.tv_post_time.setTag(feed);
        holder.tv_post_time.setText(feed.getTime());
        if (feed.getAvail_req().length > 0 && !TextUtils.isEmpty(feed.getAvail_req()[0])) {

            holder.mAvailReq.setText(feed.getAvail_req()[0]);
            holder.mAvailReq.setVisibility(View.VISIBLE);

        } else {
            holder.mAvailReq.setVisibility(View.GONE);
        }

        if (feed.getSale_rent().length > 0 && !TextUtils.isEmpty(feed.getSale_rent()[0])) {
            holder.mSaleRent.setText(feed.getSale_rent()[0]);
            holder.mSaleRent.setVisibility(View.VISIBLE);
        } else {
            holder.mSaleRent.setVisibility(View.GONE);

        }

        if (!TextUtils.isEmpty(feed.getTop_ht())) {
            holder.mTopHash.setText(Util.toDisplayCase(feed.getTop_ht()));
            holder.mTopHash.setVisibility(View.VISIBLE);

        } else {
            holder.mTopHash.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(feed.getSugg_info())) {
            holder.mSuggestionInfo.setText(feed.getSugg_info());
            holder.mSuggInfoLayout.setVisibility(View.VISIBLE);
        } else {
            holder.mSuggInfoLayout.setVisibility(View.GONE);
            if (!isDisabled) {
                ((LinearLayout) holder.mSuggInfoLayout.getParent()).setBackgroundColor(ctx.getResources().getColor(R.color.white));
            }
        }

        ProfileClass userProfile = AppController.getInstance().getUserProfile();
        String authId = "";
        if (null != userProfile) {
            authId = userProfile.getAuthorid();
        }

        if ((feed.getAuthorid() + "").equalsIgnoreCase(authId)) {
            holder.mEdit.setTag(feed);
            holder.mEdit.setVisibility(View.VISIBLE);
            holder.mCall.setVisibility(View.INVISIBLE);
            holder.mChat.setVisibility(View.INVISIBLE);

            holder.mEdit.setVisibility(View.VISIBLE);

            holder.mBookmark.setVisibility(View.GONE);


        } else {
            BookmarkTag bt = new BookmarkTag();
            bt.setId(feed.get_id());
            Log.d(TAG, "Bookmarked :" + feed.getBookmarked());
            if (null != feed.getBookmarked() && feed.getBookmarked()) {
                bt.setBookmarked(true);
                holder.mBookmark.setImageResource(heart_red);
            } else {
                bt.setBookmarked(false);
                holder.mBookmark.setImageResource(heart_grey);

            }
            holder.mCall.setVisibility(View.VISIBLE);
            holder.mChat.setVisibility(View.VISIBLE);

            holder.mBookmark.setTag(feed);
            holder.mEdit.setVisibility(View.GONE);
            holder.mBookmark.setVisibility(View.VISIBLE);

        }


        holder.iv_options.setTag(feed);
        holder.post_content.setTag(feed);
        holder.card_view.setTag(feed);
        holder.mCopy.setTag(feed);
        holder.mForward.setTag(feed);
        holder.mMore.setTag(feed);
        String text = feed.getText();
        int start = 0;
        int end = 250;
        if (feed.getSnippet().size() > 0) {
            start = Integer.parseInt(feed.getSnippet().get(0));
            end = Integer.parseInt(feed.getSnippet().get(1)) + 1;
        }

        int occurance = text.length() - text.replace("\n", "").length();

        if (text.length() <= end) { //&& occurance<4
            holder.mMore.setVisibility(View.VISIBLE);
        } else {

            if (start >= 0 && end < text.length()) {
                try {
                    text = text.substring(start, end);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            holder.mMore.setVisibility(View.VISIBLE);
        }

        //adding span for hashtags in post
        SpannableString hashTagText = ActivityUtils.setHashTags(feed, text, start, end, ctx, holder.post_content, getType(), position, false, getRefKey(), mActivity);//type and position send to track analytics

//        if (feed.getHashTagText() == null) {
//            feed.setHashTagText(hashTagText);
//
//        } else {
//
//            feed.setHashTagText(feed.getHashTagText());
//        }

    }

    protected void setupClickListener(final FeedsCardHolder holder, final int position, final Feeds feedEnabled) {

        holder.post_content.setTypeface(PBFonts.ROBOTTO_NORMAL);

        //on click listener
        holder.mMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trackEvent(Analytics.CardActions.CardClicked, position, feedEnabled.get_id(), feedEnabled.getType(), null);
                redirectToDetails(position, v);

            }
        });


        holder.post_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //lets tell our tooltip zoo zoo that relatedtip ship has sailed
                trackEvent(Analytics.CardActions.CardClicked, position, feedEnabled.get_id(), feedEnabled.getType(), null);
                Set<String> indexSet = PBPreferences.getHomeToolTipSet();
                indexSet.add("relatedTip");
                PBPreferences.setHomeToolTipSet(indexSet);
                redirectToDetails(position, v);

            }
        });
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trackEvent(Analytics.CardActions.CardClicked, position, feedEnabled.get_id(), feedEnabled.getType(), null);
                redirectToDetails(position, v);
            }
        });


        holder.mCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Feeds feedCopy = (Feeds) view.getTag();
                String feedCopiedStr = feedCopy.getProfile_phone() + " : \n" + feedCopy.getText() + "\nBy: " + feedCopy.getProfile_name();
                ActivityUtils.copyFeed(feedCopiedStr, ctx);
                trackEvent(Analytics.CardActions.Copied, position, feedCopy.get_id(), feedCopy.getType(), null);
            }
        });

        holder.mForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PBSharingManager.shareByAll((Feeds) view.getTag(), ctx);
                trackEvent(Analytics.CardActions.Shared, position, feedEnabled.get_id(), feedEnabled.getType(), null);
            }
        });

        holder.mCall.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Feeds feeds = (Feeds) v.getTag();
                        String phone = feeds.getProfile_phone();
                        boolean status = Util.checkIfStringIsPhone(phone);

                        HashMap<String, Object> wrData = new HashMap<String, Object>();
                        wrData.put("ScreenName", TAG);
                        wrData.put("Action", "Call");
                        wrData.put("callFrom", PBPreferences.getPhone());
                        wrData.put("callTo", feeds.getProfile_phone() + "");
                        wrData.put(Analytics.Params.SHOUT_ID, feeds.get_id());
                        wrData.put(Analytics.Params.CARD_POSITION, position);
                        ActivityUtils.initiateCallDialog(ctx, feeds, feeds.getPhonemap(), feeds.getProfile_phone(), wrData, feeds.getIsFreeCall(), feeds.getProfile_img(), feeds.getProfile_name());

                    }
                }
        );

        holder.mChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Feeds feedObject = (Feeds) view.getTag();

                if (null != feedObject && !TextUtils.isEmpty(feedObject.getProfile_phone())) {

                } else {
                    return;
                }

                String phoneNumber = feedObject.getProfile_phone().toString();
                String authorid = feedObject.getAuthorid() + "";
                List<String> userPhones = new ArrayList<String>();
                userPhones.add(phoneNumber);
                long profileId = feedObject.getAuthorid();

                Utility.userStats(ctx, "im");

                HashMap<String, Object> wrData = new HashMap<String, Object>();
                wrData.put("imTo", phoneNumber);
                wrData.put("imFrom", PBPreferences.getPhone());
                wrData.put("Action", "IM");
                trackEvent(Analytics.CardActions.Chat, position, feedEnabled.get_id(), feedEnabled.getType(), wrData);

                Intent intent = new Intent(ctx, XMPPChat.class);
                String num = userPhones.get(0);
                String displayName = ActivityUtils.getDisplayNameFromPhone(num);
                if (displayName != null && !TextUtils.isEmpty(displayName)) {
                    intent.putExtra("name", displayName);

                } else {
                    intent.putExtra("name", feedObject.getProfile_name());

                }
                intent.putExtra("img", feedObject.getProfile_img());
                intent.putExtra("last_seen", feedObject.getTime());

                intent.putExtra("phone_key", num);
                intent.putExtra("userid", profileId + "");
                intent.putExtra("feed_text", feedObject.getSumm()); //summary of hashtags
                PlabroIntentService.startForNotifyingInteraction(wrData);
                Bundle b = new Bundle();
                b.putSerializable(Constants.BUNDLE_KEY_FEED, feedObject);
                intent.putExtras(b);
                ctx.startActivity(intent);
                //startActivityForResult(intent, Constants.REVIEW_REQUEST_CODE);

            }
        });

        holder.mBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Feeds bookmarkFeed = (Feeds) view.getTag();
                //  final BookmarkTag bt = (BookmarkTag) view.getTag();
                //final String post_id = bt.getId();
                //final Boolean bookmarked = bt.getBookmarked();
                if (!bookmarkFeed.getBookmarked()) {

                    //WizRocket tracking
                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("ScreenName", TAG);
                    wrData.put("Action", "Bookmark");
                    trackEvent(Analytics.CardActions.Bookmarked, position, bookmarkFeed.get_id(), bookmarkFeed.getType(), wrData);

                    holder.mBookmark.setImageResource(heart_red);

//                                BookmarkTag bt_n = new BookmarkTag();
//                                bt_n.setId(bt.getId());
//                                bt_n.setBookmarked(true);
                    bookmarkFeed.setBookmarked(true);
                    holder.mBookmark.setTag(feedEnabled);
                    setBookmark(bookmarkFeed, holder.mBookmark, true, position);
                } else {
                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("ScreenName", TAG);
                    trackEvent(Analytics.CardActions.UnBookMarked, position, bookmarkFeed.get_id(), bookmarkFeed.getType(), wrData);

                    holder.mBookmark.setImageResource(heart_grey);
                    bookmarkFeed.setBookmarked(false);
//                                BookmarkTag bt_n = new BookmarkTag();
//                                bt_n.setId(bt.getId());
//                                bt_n.setBookmarked(false);
                    holder.mBookmark.setTag(bookmarkFeed);

                    setBookmark(bookmarkFeed, holder.mBookmark, false, position);
                }
            }
        });


        holder.mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Feeds editFeed = (Feeds) view.getTag();

                //WizRocket tracking
                HashMap<String, Object> wrdata1 = new HashMap<>();
                wrdata1.put("ScreenName", TAG);
                wrdata1.put("Action", "EditShout");
                trackEvent(Analytics.CardActions.Edit, position, editFeed.get_id(), editFeed.getType(), wrdata1);
                Intent intent = new Intent(ctx, Shout.class);
                intent.putExtra("feedObject", editFeed);
                intent.putExtra("shoutType", "edit");
                intent.putExtra("postId", editFeed.get_id());
                ctx.startActivity(intent);
                ((Activity) ctx).overridePendingTransition(R.anim.bottom_in_fast, R.anim.bottom_out_fast);


            }
        });


        holder.iv_profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String authorid = ((Feeds) view.getTag()).getAuthorid() + "";
                HashMap<String, Object> wrData = new HashMap<String, Object>();
                wrData.put("ScreenName", TAG);
                wrData.put("Action", "Image Clicked");
                trackEvent(Analytics.CardActions.ViewProfile, position, feedEnabled.get_id(), feedEnabled.getType(), wrData);

                Intent intent1 = new Intent(ctx, Profile.class);
                intent1.putExtra("authorid", authorid);
                ctx.startActivity(intent1);
            }
        });

        holder.tv_profile_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String authorid = ((Feeds) view.getTag()).getAuthorid() + "";
                HashMap<String, Object> wrData = new HashMap<String, Object>();
                wrData.put("ScreenName", TAG);
                wrData.put("Action", "Name Clicked");

                trackEvent(Analytics.CardActions.ViewProfile, position, feedEnabled.get_id(), feedEnabled.getType(), wrData);


                Intent intent1 = new Intent(ctx, Profile.class);
                intent1.putExtra("authorid", authorid);
                ctx.startActivity(intent1);
            }
        });
        holder.tv_post_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String authorid = ((Feeds) view.getTag()).getAuthorid() + "";
                HashMap<String, Object> wrData = new HashMap<String, Object>();
                wrData.put("ScreenName", TAG);
                wrData.put("Action", "Post Time Clicked");
                trackEvent(Analytics.CardActions.ViewProfile, position, feedEnabled.get_id(), feedEnabled.getType(), wrData);

                Intent intent1 = new Intent(ctx, Profile.class);
                intent1.putExtra("authorid", authorid);
                ctx.startActivity(intent1);
            }
        });

//                    enableCard(holder);
//                }
//                else {
//                    disableCard(holder);
//                }
    }

    protected void redirectToDetails(int position, View v) {

        Feeds feedsVal = ((Feeds) v.getTag());
        String id = feedsVal.get_id();

        FeedDetails.startFeedDetails(ctx, id, refKey + "", feedsVal);
    }

    protected void setBookmark(final Feeds bookmarkFeed, final ImageView iv_bookmark, final Boolean isSetBookmark, final int position) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.PLABRO_FEEDS_POST_PARAM_BOOKMARK_MESSAGE_ID, bookmarkFeed.get_id());
        if (isSetBookmark) {
            params = Utility.getInitialParams(PlaBroApi.RT.SETBOOKMARK, params);
        } else {
            params = Utility.getInitialParams(PlaBroApi.RT.DELETEBOOKMARK, params);
        }

        AppVolley.processRequest(Constants.TASK_CODES.SET_BOOKMARK, BookmarkResponse.class, null, PlaBroApi.getBaseUrl(ctx), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                BookmarkResponse bookmarkResponse = (BookmarkResponse) response.getResponse();
                if (!type.equalsIgnoreCase("Feeds")) {
                    ShoutObserver.dispatchEvent();
                }

                if (isSetBookmark) {

                    Util.showSnackBarMessage(mActivity, ctx, "Bookmarked Successfully");
                } else {
                    Util.showSnackBarMessage(mActivity, ctx, "Bookmark Removed");

                }
                if (!isSetBookmark) {
                    //todo add listener to remove bookmark list
                    new Delete().from(BookmarksDBModel.class).where("_id='" + feedObject.get_id() + "'").execute();
                    if (getBookmarkDeleteListener() != null) {
                        getBookmarkDeleteListener().onResponse(true, position);
                    }
                }

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                if (isSetBookmark) {
                    Toast.makeText(ctx, "Cannot Bookmark, Please check your network or try again", Toast.LENGTH_LONG).show();
                    iv_bookmark.setImageResource(heart_grey);
                    bookmarkFeed.setBookmarked(false);
                    iv_bookmark.setTag(bookmarkFeed);
                } else {
                    Toast.makeText(ctx, "Cannot Delete Bookmark, Please check your network or try again", Toast.LENGTH_LONG).show();
                    iv_bookmark.setImageResource(heart_red);

                    bookmarkFeed.setBookmarked(false);
                    iv_bookmark.setTag(bookmarkFeed);
                }
            }
        });
    }


}
