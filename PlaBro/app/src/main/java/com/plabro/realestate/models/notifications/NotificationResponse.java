package com.plabro.realestate.models.notifications;


import com.plabro.realestate.models.ServerResponse;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class NotificationResponse extends ServerResponse {
    NotificationOutputParams output_params = new NotificationOutputParams();

    public NotificationOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(NotificationOutputParams output_params) {
        this.output_params = output_params;
    }
}
