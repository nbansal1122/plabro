package com.plabro.realestate.utilities;

import android.text.TextUtils;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.volley.CustomException;
import com.plabro.realestate.volley.PBResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public final class JSONUtils {
    private static final GsonBuilder gson = new GsonBuilder()
            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
    private static final String TAG = "Plabro";

    private JSONUtils() {
    }

    public synchronized static GsonBuilder getGSONBuilder() {
        return gson;
    }

    public synchronized static Object parseJson(PBResponse res, String jsonString, Class<?> classType) throws CustomException, JsonSyntaxException {
        Log.d(TAG, "Server Response: " + jsonString);
        if (null != jsonString && !TextUtils.isEmpty(jsonString)) {


            Method method = null;
            try {
                method = classType.getMethod("parseJson", String.class);
                Object o = method.invoke(null, jsonString);
                return o;
            } catch (NoSuchMethodException e) {
                //e.printStackTrace();
            } catch (InvocationTargetException e) {
                //e.printStackTrace();
            } catch (IllegalAccessException e) {
                //e.printStackTrace();
            }

            return gson.create().fromJson(jsonString, classType);
        } else {
            throw new CustomException("No data found", VolleyListener.NO_DATA_EXCEPTION);
        }
    }

    public static boolean isJSONValid(String JSON_STRING) {

        Log.d(TAG, "Server Response: " + JSON_STRING);
        if (JSON_STRING.contains("Session Expired")) {
            Utility.deleteLoginObject();
        }


        try {
            new JSONObject(JSON_STRING);
        } catch (JSONException ex) {

            Log.i(TAG, "Invalid Json Object");
            Log.i(TAG, JSON_STRING);
            try {
                new JSONArray(JSON_STRING);
            } catch (JSONException ex1) {

                Log.i(TAG, "Invalid Json Array");
                Log.i(TAG, JSON_STRING);
                return false;
            }
            //return false;
        }
        return true;
    }

    public static JSONObject getJSONObjectFromString(String json) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(json);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    public static Object parseJson(String json, Class classType) {
        try {
            return gson.create().fromJson(json, classType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//
}