package com.plabro.realestate.listeners;

public interface MasterLifecycleInterface {

    public void findViewById();

    public void setViewSizes();

    public void callScreenDataRequest();

    public void reloadRequest();

    public void inflateToolbar();

}
