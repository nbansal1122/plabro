package com.plabro.realestate.models.location_auto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class LocationListAutoOutputParams {

    List<LocationsAuto> data = new ArrayList<LocationsAuto>();

    public List<LocationsAuto> getData() {
        return data;
    }

    public void setData(List<LocationsAuto> data) {
        this.data = data;
    }
}
