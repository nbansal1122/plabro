package com.plabro.realestate.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.Feeds;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class CustomListAdapter<T> extends ArrayAdapter<T> {

    protected List<T> list;
    protected List<T> arrayList;
    protected int rowlayoutID;
    protected LayoutInflater inflater;
    protected CustomListAdapterInterface ref;

    public CustomListAdapter(Context context, int resource, List<T> objects, CustomListAdapterInterface ref) {
        super(context, resource, objects);
        inflater = LayoutInflater.from(context);
        this.list = objects;
        this.arrayList = objects;
        this.ref = ref;
        this.rowlayoutID = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return ref.getView(position, convertView, parent, rowlayoutID);
    }

    public interface CustomListAdapterInterface {
        public View getView(int position, View convertView, ViewGroup parent, int resourceID);

    }

    // Filter Class
    public void filter(String charText) {
        if (charText.equalsIgnoreCase("")) {
            arrayList.clear();
            arrayList.addAll(list);
        } else {
            charText = charText.toLowerCase(Locale.getDefault());
            arrayList.clear();

            if (list.size() > 0) {
                if (charText.length() == 0) {
                    arrayList.addAll(list);
                } else {
                    for (T item : list) {
                        if (item.toString().toLowerCase(Locale.getDefault()).contains(charText)) {
                            arrayList.add((T) item);
                        }
                    }
                }
            }
        }
        notifyDataSetChanged();
    }
}
