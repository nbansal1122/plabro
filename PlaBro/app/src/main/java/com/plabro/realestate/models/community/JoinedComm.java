package com.plabro.realestate.models.community;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by nbansal2211 on 18/05/16.
 */
public class JoinedComm implements Serializable{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("usertype")
    @Expose
    private String usertype;
    @SerializedName("groupInfo")
    @Expose
    private GroupInfo groupInfo;
    @SerializedName("groupid")
    @Expose
    private Integer groupid;

    private boolean isWaiting;

    public boolean isWaiting() {
        return isWaiting;
    }

    public void setWaiting(boolean waiting) {
        isWaiting = waiting;
    }

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return The usertype
     */
    public String getUsertype() {
        return usertype;
    }

    /**
     * @param usertype The usertype
     */
    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    /**
     * @return The groupInfo
     */
    public GroupInfo getGroupInfo() {
        return groupInfo;
    }

    /**
     * @param groupInfo The groupInfo
     */
    public void setGroupInfo(GroupInfo groupInfo) {
        this.groupInfo = groupInfo;
    }

    /**
     * @return The groupid
     */
    public Integer getGroupid() {
        return groupid;
    }

    /**
     * @param groupid The groupid
     */
    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }

}
