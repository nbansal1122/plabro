package com.plabro.realestate.uiHelpers;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;

import com.plabro.realestate.R;

public class Utils2 {
    public static void setBadgeCount(Context context, LayerDrawable icon, int count) {
        setBadgeCount(context, icon, count, R.id.ic_badge);
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, int count, int iconId) {

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(iconId);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(iconId, badge);
    }


}