package com.plabro.realestate.models.googleUrl;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class ExpanderResponse   {

    String kind;
    String id;
    String longUrl;
    String status;

    public String getKind() {
        return kind;
    }

    public String getId() {
        return id;
    }

    public String getLongUrl() {
        return longUrl;
    }

    public String getStatus() {
        return status;
    }
}
