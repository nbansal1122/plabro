package com.plabro.realestate.holders;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.plabro.realestate.listeners.OnUpdateListener;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.view.FontText;

import java.util.HashMap;

/**
 * Created by nitin on 14/08/15.
 */

public abstract class BaseFeedHolder extends RecyclerView.ViewHolder implements HolderInterface {
    protected Context ctx;
    protected String TAG = getTAG();
    protected FragmentManager mFragmentManager;
    protected String type;
    protected int refKey;
    protected Activity mActivity;
    protected OnUpdateListener bookmarkDeleteListener;
    protected String cardType;
    protected BaseFeed feed;

    public OnUpdateListener getBookmarkDeleteListener() {
        return bookmarkDeleteListener;
    }

    public void setBookmarkDeleteListener(OnUpdateListener bookmarkDeleteListener) {
        this.bookmarkDeleteListener = bookmarkDeleteListener;
    }

    public String getType() {
        return feed.getType();
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public int getRefKey() {
        return refKey;
    }

    public void setRefKey(int refKey) {
        this.refKey = refKey;
    }

    public FragmentManager getmFragmentManager() {
        return mFragmentManager;
    }

    public void setmFragmentManager(FragmentManager mFragementManager) {
        this.mFragmentManager = mFragementManager;
    }

    public String getTAG() {
        return TAG;
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }

    public BaseFeedHolder(View itemView) {
        super(itemView);
        ctx = itemView.getContext();
        mActivity = (Activity) ctx;
    }


    @Override
    public void onBindViewHolder(int position, BaseFeed feed) {
        this.feed = feed;
        this.type = feed.getType();
    }

    protected View findView(int id) {
        return itemView.findViewById(id);
    }

    protected TextView findTV(int id) {
        return (FontText) findView(id);
    }

    protected void trackEvent(String eventName, int position, String id, String type, HashMap<String, Object> otherdata) {
        if (otherdata == null) {
            otherdata = new HashMap<>();
        }
        otherdata.put(Analytics.Params.SHOUT_ID, id);
        otherdata.put(Analytics.Params.CARD_POSITION, position);
        otherdata.put(Analytics.Params.FEED_TYPE, type);
        trackInteractionEvent(eventName, otherdata);
    }

    protected void trackCopy() {

    }


    protected void trackInteractionEvent(String eventName, HashMap<String, Object> data) {
        Analytics.trackInteractionEvent(eventName, data);
    }
}
