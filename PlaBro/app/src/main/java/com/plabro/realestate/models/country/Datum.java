package com.plabro.realestate.models.country;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("country_name")
    @Expose
    private String country_name;
    @SerializedName("isd_code")
    @Expose
    private String isd_code;
    @SerializedName("country_code")
    @Expose
    private String country_code;

    /**
     * @return The country_name
     */
    public String getCountry_name() {
        return country_name;
    }

    /**
     * @param country_name The country_name
     */
    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    /**
     * @return The isd_code
     */
    public String getIsd_code() {
        return isd_code;
    }

    /**
     * @param isd_code The isd_code
     */
    public void setIsd_code(String isd_code) {
        this.isd_code = isd_code;
    }

    /**
     * @return The country_code
     */
    public String getCountry_code() {
        return country_code;
    }

    /**
     * @param country_code The country_code
     */
    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

}