package com.plabro.realestate.models.services;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Services {

    @SerializedName("enabled")
    @Expose
    private Boolean enabled;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("services_list")
    @Expose
    private List<Services_list> services_list = new ArrayList<Services_list>();

    /**
     * @return The enabled
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * @param enabled The enabled
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return The img
     */
    public String getImg() {
        return img;
    }

    /**
     * @param img The img
     */
    public void setImg(String img) {
        this.img = img;
    }

    /**
     * @return The services_list
     */
    public List<Services_list> getServices_list() {
        return services_list;
    }

    /**
     * @param services_list The services_list
     */
    public void setServices_list(List<Services_list> services_list) {
        this.services_list = services_list;
    }

}