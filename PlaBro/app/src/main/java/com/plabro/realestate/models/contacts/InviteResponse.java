package com.plabro.realestate.models.contacts;

import com.google.gson.annotations.Expose;
import com.plabro.realestate.models.ServerResponse;

/**
 * Created by nitin on 07/09/15.
 */
public class InviteResponse extends ServerResponse {
    @Expose
    private String RT;
    @Expose
    private Boolean loginStatus;
    @Expose
    private Output_params output_params;


    /**
     * @return The RT
     */
    public String getRT() {
        return RT;
    }

    /**
     * @param RT The RT
     */
    public void setRT(String RT) {
        this.RT = RT;
    }

    /**
     * @return The loginStatus
     */
    public Boolean getLoginStatus() {
        return loginStatus;
    }

    /**
     * @param loginStatus The loginStatus
     */
    public void setLoginStatus(Boolean loginStatus) {
        this.loginStatus = loginStatus;
    }

    /**
     * @return The output_params
     */
    public Output_params getOutput_params() {
        return output_params;
    }

    /**
     * @param output_params The output_params
     */
    public void setOutput_params(Output_params output_params) {
        this.output_params = output_params;
    }
}
