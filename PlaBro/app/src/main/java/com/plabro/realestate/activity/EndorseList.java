package com.plabro.realestate.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.fragments.UserEndorsements;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.Endorsement;
import com.plabro.realestate.models.ServerResponse;
import com.plabro.realestate.models.profile.Endorsement_profile;
import com.plabro.realestate.models.profile.Endorser_list;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 04/08/15.
 */
public class EndorseList extends PlabroBaseActivity implements CustomListAdapter.CustomListAdapterInterface, AdapterView.OnItemClickListener {
    @Override
    protected String getTag() {
        return "EndorseList";
    }

    private ProgressDialog dialog;
    private ListView list;
    private CustomListAdapter<Endorser_list> adapter;
    private List<Endorser_list> endorsers = new ArrayList<>();
    private Endorsement_profile endProfile;
    private ProfileClass profile;
    private boolean isCurrentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_endorser_list);
        inflateToolbar();
        //setToolbarTitle("Endorsed for");
        setToolbarTitle("Recommended for"); //todo: prannoy rating changes

        
        list = (ListView) findViewById(R.id.listView);
        endProfile = (Endorsement_profile) getIntent().getSerializableExtra(Constants.BUNDLE_KEY_ENDORSER_PROFILE);
        profile = (ProfileClass) getIntent().getSerializableExtra(Constants.BUNDLE_KEY_USER_PROFILE);
        isCurrentUser = getIntent().getBooleanExtra(UserEndorsements.KEY_CURRENT_USER, false);


        endorsers = endProfile.getEndorser_list();
        adapter = new CustomListAdapter<>(this, R.layout.row_endorser_list, endorsers, this);
        list.setAdapter(adapter);
        list.setEmptyView(getEmptyViewForEndorsements());

        TextView endorseUser = (TextView) findViewById(R.id.tv_endorse_user);
        setHeader();
        Log.d(TAG,isAlreadyEndorsed()+":::::"+isCurrentUser);
        if (isAlreadyEndorsed() || isCurrentUser) {
            endorseUser.setVisibility(View.GONE);
        } else {
            //setText(R.id.tv_endorse_user, "Endorse " + profile.getName());
            setText(R.id.tv_endorse_user, "Recommend " + profile.getName());//todo: prannoy rating changes

            endorseUser.setVisibility(View.VISIBLE);
        }

        setClickListeners(R.id.tv_endorse_user);
        list.setOnItemClickListener(this);

    }

    private boolean isAlreadyEndorsed() {
        ProfileClass userProfile = AppController.getInstance().getUserProfile();
        for (Endorser_list endorser : endorsers) {
            if (!TextUtils.isEmpty(endorser.getUid())) {
//                if (endorser.getUid().equals(profile.getAuthorid())) {
//                    return true;
//                }
                if (null != userProfile && null != userProfile.getAuthorid() && userProfile.getAuthorid().equals(endorser.getUid())) {
                    return true;
                }
            }
        }
        return false;
    }

    private View getEmptyViewForEndorsements() {
        View v = findViewById(android.R.id.empty);
        TextView view = (TextView) v.findViewById(R.id.tv_help_endorsement);
        String text = String.format((getString(R.string.help_endorsement)), profile.getName());
        view.setText(text);
        return v;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_endorse_user:
                StringBuilder builder = new StringBuilder();
                for (String key : endProfile.getKeys()) {
                    builder.append(key);
                    builder.append(",");
                }
                builder.deleteCharAt(builder.lastIndexOf(","));
                showProgressDialog();
                trackEndorsement(builder.toString());
                testEndorsement(builder.toString(), profile.getAuthorid());
                break;
        }
    }

    private void trackEndorsement(String key) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("Key", key);
        map.put("Target Authkey", profile.get_id());
        map.put("Target Phone", profile.getPhone());
        map.put("User Phone", PBPreferences.getPhone());
        map.put("screen", "User Profile");
    }

    private void setHeader() {
        TextView endorsementTitle = (TextView) findViewById(R.id.tv_row_grp_endorsement);
        endorsementTitle.setText(endProfile.getTitle());
        endorsementTitle.setTextColor(getResources().getColor(R.color.white));
        RoundedImageView icon = (RoundedImageView) findViewById(R.id.iv_icon_row_grp_endorsement);
        String img = endProfile.getWhite_img_url();
        if (TextUtils.isEmpty(img)) {
            img = endProfile.getImg_url();
        }
        ActivityUtils.loadImage(this, img, icon, R.drawable.ic_plot);
        setText(R.id.tv_row_grp_endorsement_count, endProfile.getCount() + "");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        ChildViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this).inflate(resourceID, null);
            holder = new ChildViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ChildViewHolder) convertView.getTag();
        }
        Endorser_list endorser = endorsers.get(position);

        holder.endorseeName.setText(endorser.getName() + "");
        String img = endorser.getImg();
        ActivityUtils.loadImage(this, img, holder.imageView);
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Endorser_list endorser = endorsers.get(i);
        Bundle b = new Bundle();
        b.putString("authorid", endorser.getUid());
        Intent intent = new Intent(this, UserProfileNew.class);
        intent.putExtras(b);
        startActivity(intent);
        finish();
    }

    @Override
    public void findViewById() {

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }


    private class ChildViewHolder {
        TextView endorseeName;
        RoundedImageView imageView;

        public ChildViewHolder(View view) {
            this.endorseeName = (TextView) view.findViewById(R.id.tv_endorserName);
            this.imageView = (RoundedImageView) view.findViewById(R.id.iv_endorseeImage);
        }
    }

    public void testEndorsement(final String endKey, final String contactId) {
        showProgressDialog();
        HashMap<String, String> params = new HashMap<>();
        params.put(PlaBroApi.PARAMS.ENDORSEMENT_KEY, endKey);
        params.put(PlaBroApi.PARAMS.CONTACT_ID, contactId + "");
        params = Utility.getInitialParams(PlaBroApi.RT.UPDATE_ENDORSEMENT, params);
        AppVolley.processRequest(0, ServerResponse.class, null, PlaBroApi.getBaseUrl(this), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                hideProgressDialog();
                showLongToast("Endorsed Successfully");
                setResult(Activity.RESULT_OK);
                finish();
                Log.d(TAG, "EndorseSuccessfully");
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                hideProgressDialog();
                Log.d(TAG, "EndorseFailure");
                Endorsement e = new Endorsement();
                e.setAuthorId(contactId);
                e.setEndorsementKey(endKey);
                e.save();
            }
        });

    }

    private void showProgressDialog() {
        if (null != dialog && dialog.isShowing()) {
        } else {
            dialog = new ProgressDialog(this);
            dialog.setMessage("Loading...");
            dialog.show();
        }
    }

    private void hideProgressDialog() {
        if (null != dialog && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
