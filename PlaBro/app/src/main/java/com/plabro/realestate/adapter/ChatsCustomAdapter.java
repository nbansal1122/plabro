/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.plabro.realestate.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.activeandroid.query.Delete;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.listeners.OnChildListener;
import com.plabro.realestate.models.chat.ChatMessage;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.MaterialDialog;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.widgets.RelativeTimeTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ChatsCustomAdapter extends RecyclerView.Adapter<ChatsCustomAdapter.ViewHolder> {

    private static Context mContext;
    private int chatsCount = 0;
    private static final String TAG = "ChatsCustomAdapter";
    private OnChildListener childListener;

    private List<ChatMessage> chats = new ArrayList<ChatMessage>();
    private List<ChatMessage> arrayList = new ArrayList<ChatMessage>();


    public class ViewHolder extends RecyclerView.ViewHolder {
        private final RoundedImageView iv_profile_image;
        private final TextView tv_profile_name, post_content;
        private final RelativeTimeTextView tv_post_time;
        private final CardView card_view;
        private final ImageView statusImage;

        public ViewHolder(View v) {
            super(v);


            this.tv_profile_name = (TextView) itemView.findViewById(R.id.profile_name);
            this.tv_post_time = (RelativeTimeTextView) itemView.findViewById(R.id.post_time);
            this.post_content = (TextView) itemView.findViewById(R.id.post_content);
            this.iv_profile_image = (RoundedImageView) itemView.findViewById(R.id.profile_image);
            this.card_view = (CardView) itemView.findViewById(R.id.card_view);
            this.statusImage = (ImageView) itemView.findViewById(R.id.iv_statsImage);

            card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PBPreferences.setNewIMOnApp(false);

                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("ScreenName", TAG);
                    wrData.put("Action", "Chat");


                    ChatMessage chatRecord = (ChatMessage) v.getTag();
                    String phoneNumber = chatRecord.getPhone();
                    String userId = chatRecord.getAuthId();

                    List<String> userPhones = new ArrayList<String>();
                    userPhones.add(phoneNumber);
                    Intent intent = new Intent(mContext, XMPPChat.class);
                    String num = userPhones.get(0);
                    intent.putExtra("callerScreen", "chatList");

                    intent.putExtra("name", chatRecord.getName());
                    intent.putExtra("img", chatRecord.getImg());
                    intent.putExtra("last_seen", chatRecord.getTime());

                    intent.putExtra("phone_key", num);
                    intent.putExtra("userid", userId + "");
                    mContext.startActivity(intent);
                    //  mActivity.startActivityForResult(intent, Constants.CHAT_REQUEST_CODE);


                }
            });

            card_view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String phoneNumber = ((ChatMessage) v.getTag()).getPhone();

                    ChatsCustomAdapter.this.showDeleteConversationOption(phoneNumber);

                    return true;
                }
            });


        }

        public RoundedImageView getIv_profile_image() {
            return iv_profile_image;
        }


        public TextView getTv_profile_name() {
            return tv_profile_name;
        }

        public RelativeTimeTextView getTv_post_time() {
            return tv_post_time;
        }

        public TextView getPost_content() {
            return post_content;
        }

        public CardView getCard_view() {
            return card_view;
        }

        public ImageView getStatusImage() {
            return statusImage;
        }
    }


    public ChatsCustomAdapter(Context mContext, List<ChatMessage> chats, OnChildListener childListener) {
        this.mContext = mContext;
        this.chats = chats;
        chatsCount = chats.size();
        this.childListener = childListener;
        arrayList.addAll(chats);


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.chats_layout, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");
        ChatMessage chatRecord = chats.get(position);

        //viewHolder.getPoster().setImage(story.getFeaturedImg(), AppController.getInstance().getImageLoader());

        String imgUrl = chatRecord.getImg();

        ActivityUtils.loadImage(viewHolder.getCard_view().getContext(), imgUrl, viewHolder.getIv_profile_image(), R.drawable.profile_default_one);
        viewHolder.getTv_profile_name().setText(chatRecord.getName());

        viewHolder.getTv_post_time().setReferenceTime(chatRecord.getTime());
        viewHolder.getPost_content().setTag(chatRecord);
        viewHolder.getPost_content().setText(chatRecord.getMsg());
        viewHolder.getPost_content().setMaxLines(1);

        if (ChatMessage.STATUS_RECEIVED.equalsIgnoreCase(chatRecord.getStatus()) && ChatMessage.MESSAGE_INCOMING == chatRecord.getMsgType()) {
            viewHolder.getPost_content().setTextColor(mContext.getResources().getColor(R.color.appColor5));
        } else {
            viewHolder.getPost_content().setTextColor(mContext.getResources().getColor(R.color.appColor2));

        }
        viewHolder.getCard_view().setTag(chatRecord);
        int statusDrawableId = R.drawable.ic_action_pending;

        if (chatRecord.getMsgType() == chatRecord.MESSAGE_OUTGOING) {
            switch (chatRecord.getStatus()) {
                case ChatMessage.STATUS_PENDING:
                    statusDrawableId = R.drawable.ic_action_pending;
                    break;
                case ChatMessage.STATUS_DELIVERED:
                    statusDrawableId = R.drawable.ic_action_delivered;
                    break;
                case ChatMessage.STATUS_READ:
                    statusDrawableId = R.drawable.ic_action_read;
                    break;
                case ChatMessage.STATUS_SENT:
                    statusDrawableId = R.drawable.ic_action_sent;
                    break;
            }
        } else {
            statusDrawableId = R.drawable.ic_recieved;

        }
        viewHolder.getStatusImage().setImageResource(statusDrawableId);


    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    public void addItems(ArrayList<ChatMessage> chatRecords) {
        for (ChatMessage chats : chatRecords) {
            addItem(chatsCount, chats);
        }

    }

    public void addItem(int position, ChatMessage chatRecord) {
        chats.add(position, chatRecord);
        notifyItemInserted(position);
        chatsCount++;
    }

    public void removeItem(int position) {
        chats.remove(position);
        notifyItemRemoved(position);
    }


    void showDeleteConversationOption(final String phoneNumber) {
        if (null != mContext && ((PlabroBaseActivity) mContext).isRunning) {
            final MaterialDialog mMaterialDialog = new MaterialDialog(mContext);

            mMaterialDialog.setTitle("Delete?")
                    .setMessage("Are you sure you want to delete this conversation?")
                    .setPositiveButton("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteConversation(phoneNumber);
                            mMaterialDialog.dismiss();
                        }
                    })

                    .setNegativeButton("CANCEL", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMaterialDialog.dismiss();
                        }
                    });

            mMaterialDialog.show();
        }
    }

    void deleteConversation(String phoneNumber) {

        new Delete().from(ChatMessage.class).where("phone=?", phoneNumber).execute();
        childListener.onResponse(true);

    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        chats.clear();
        if (arrayList.size() > 0) {
            if (charText.length() == 0) {
                chats.addAll(arrayList);
            } else {
                for (ChatMessage gsd : arrayList) {
                    if (gsd.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        chats.add(gsd);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }


}
