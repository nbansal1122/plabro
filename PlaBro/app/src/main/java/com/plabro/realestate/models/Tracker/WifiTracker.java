package com.plabro.realestate.models.Tracker;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmd on 5/14/2015.
 */
public class WifiTracker implements Serializable {
    private int distance;
    private long timestamp;
    private double lat;
    private double lng;
    private List<WifiInfo> list = new ArrayList<>();

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public List<WifiInfo> getList() {
        return list;
    }

    public void setList(List<WifiInfo> list) {
        this.list = list;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }


}
