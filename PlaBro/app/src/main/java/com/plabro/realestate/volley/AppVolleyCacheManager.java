package com.plabro.realestate.volley;

import android.os.Environment;
import android.text.TextUtils;

import com.android.volley.VolleyError;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ServerResponse;
import com.plabro.realestate.security.RSAEncryption;
import com.plabro.realestate.utilities.PlaBroApi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jmd on 5/6/2015.
 */
public class AppVolleyCacheManager {

    private static final String TAG = "CacheManager";

    private static HashMap<String, String> urlCacheMap = new HashMap<>();


    public static void processRequest(final int taskCode, final Class classType, boolean isCache, String tag, String url, final Map<String, String> mParams, RequestMethod requestMethod, final VolleyListener mListener) {

        Log.d(TAG, "Getting feeds");
        String data = null;
        final String key = mParams.get(PlaBroApi.RT_KEY) + "_new";
        if (!isCache) {
            AppVolley.processRequest(taskCode, classType, tag, url, mParams, requestMethod, mListener);
            return;
        } else {
            if (urlCacheMap.containsKey(key)) {
                data = urlCacheMap.get(key);
                if (!TextUtils.isEmpty(data))
                    try {
                        Log.i(TAG, "Data found in Cache Map, Sending Success Response");
                        AppVolley.onApiResponse(mParams, classType, data, mListener, taskCode);
                        //mListener.onSuccessResponse(data);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            } else {
// File may present on Disk
                data = getDataFromDisk(key);
                if (data != null && !TextUtils.isEmpty(data)) {
                    try {
                        Log.i(TAG, "Data found on HardDisk, Sending Success Response");
                        urlCacheMap.put(key, data);
                        Log.d(TAG, "Data on hard disk is :" + data);
                        AppVolley.onApiResponse(mParams, classType, data, mListener, taskCode);
                        //mListener.onSuccessResponse(data);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // Data is not present on disk
                }
            }
        }

        AppVolley.processRequest(taskCode, null, tag, url, mParams, requestMethod, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {


                if (mListener != null) {
                    final String data = (String) response.getResponse();
                    AppVolley.onApiResponse(mParams, classType, data, new VolleyListener() {
                        @Override
                        public void onSuccessResponse(PBResponse response, int taskCode) {
                            if (response.getResponse() instanceof ServerResponse) {
                                if (((ServerResponse) response.getResponse()).isStatus()) {
                                    mListener.onSuccessResponse(response, taskCode);
                                    putDataOnDisk(key, data);
                                    urlCacheMap.put(key, data);
                                    return;
                                }
                            }
                            mListener.onSuccessResponse(response, taskCode);
                        }

                        @Override
                        public void onFailureResponse(PBResponse response, int taskCode) {
                            mListener.onFailureResponse(response, taskCode);
                        }
                    }, taskCode);
                }
//                    mListener.onSuccessResponse(response);

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Log.i(TAG, "Sending Failure Response");
//                mListener.onFailureResponse(volleyError);
                PBResponse res = new PBResponse();
                res.setCustomException(new CustomException("Error while downloading", new VolleyError()));
                mListener.onFailureResponse(res, taskCode);
            }
        });


    }

    private static void putDataOnDisk(String fileName, String data) {

        File f = getFile(fileName);
        FileOutputStream stream = null;
        if (f != null) {
            try {
                String tempdata = getEncryptedData(data);
                if (tempdata != null) {
                    data = tempdata;
                }
                stream = new FileOutputStream(f);
                stream.write(data.getBytes());
                stream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {

            }

        }
    }

    public static String getEncryptedData(String data) {
        try {
            RSAEncryption rsaEncryption = new RSAEncryption();
            return rsaEncryption.EncryptData(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDecryptedData(String data) {
        try {
            RSAEncryption rsaEncryption = new RSAEncryption();
            return rsaEncryption.DecryptData(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static File getFile(String name) {
        File sdcard = Environment.getExternalStorageDirectory();
        String dbfile = sdcard.getAbsolutePath() + File.separator + "cachedata" + File.separator + name + ".txt";

        Log.i(TAG, "FileName" + dbfile);
        File result = new File(dbfile);

        if (!result.getParentFile().exists()) {
            result.getParentFile().mkdirs();
        }

        if (!result.exists()) {
            try {
                result.createNewFile();
                return result;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return result;
        }

    }

    public static void deleteCacheFolder() {
        File sdcard = Environment.getExternalStorageDirectory();
        String dbfile = sdcard.getAbsolutePath() + File.separator + "cachedata";
        File result = new File(dbfile);
        if (result.exists()) {
            DeleteRecursive(result);
        }

    }

    static void DeleteRecursive(File dir) {
        Log.d("DeleteRecursive", "DELETEPREVIOUS TOP" + dir.getPath());
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                File temp = new File(dir, children[i]);
                if (temp.isDirectory()) {
                    Log.d("DeleteRecursive", "Recursive Call" + temp.getPath());
                    DeleteRecursive(temp);
                } else {
                    Log.d("DeleteRecursive", "Delete File" + temp.getPath());
                    boolean b = temp.delete();
                    if (b == false) {
                        Log.d("DeleteRecursive", "DELETE FAIL");
                    }
                }
            }

        }
        dir.delete();
    }

    private static String getDataFromDisk(String fileName) {
        File file = getFile(fileName);
        if (file == null) return null;
        int length = (int) file.length();

        byte[] bytes = new byte[length];

        try {
            FileInputStream in = new FileInputStream(file);
            in.read(bytes);
            in.close();
        } catch (FileNotFoundException e) {

            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {

        }

        String contents = new String(bytes);
        String tempData = getDecryptedData(contents);
        Log.d(TAG, "Data decrypted:" + tempData);
        if (tempData != null) {
            return tempData;
        }
        return contents;
    }


}
