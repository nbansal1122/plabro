package com.plabro.realestate.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.desmond.parallaxviewpager.ListViewFragment;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.EditProfile;
import com.plabro.realestate.activity.UserProfileNew;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by nitin on 28/07/15.
 */
public class UserInfo extends ListViewFragment implements CustomListAdapter.CustomListAdapterInterface, AdapterView.OnItemClickListener, UserProfileNew.ProfileRefreshListener {
    ProfileClass profile;
    CustomListAdapter<ProfileSegment> adapter;
    private ArrayList<ProfileSegment> list = new ArrayList<>();
    private boolean isCurrentUser;

    private String mAuthorId;

    private static final String KEY_PROFILE = "Profile";
    private static final String KEY_CURRENT_USER = "IsCurrentUser";

    private FrameLayout placeHolderView;

    private static final String TAG = "UserInfo";

    public static Fragment getInstance(ProfileClass profile, boolean isCurrentUser) {
        Bundle b = new Bundle();
        b.putSerializable(KEY_PROFILE, profile);
        b.putBoolean(KEY_CURRENT_USER, isCurrentUser);
        UserInfo info = new UserInfo();
        info.setArguments(b);
        return info;
    }

    int height;

    public void setHeight(int h) {
        height = h;
    }

    @Override
    protected void findViewById() {
        profile = (ProfileClass) getArguments().getSerializable(KEY_PROFILE);
        isCurrentUser = getArguments().getBoolean(KEY_CURRENT_USER);
        setViews();
    }

    private void setViews() {
        mAuthorId = profile.getAuthorid();
        mListView = (ListView) findView(R.id.listView);
        initUserInfoList(profile);

        mListView.removeAllViewsInLayout();
        if (null != placeHolderView) {
            mListView.removeHeaderView(placeHolderView);
        }
        placeHolderView = getHeaderView();
        //placeHolderView.getLayoutParams().height = height;
        //placeHolderView.setMinimumHeight(height);
        mListView.addHeaderView(placeHolderView);

        adapter = new CustomListAdapter<>(getActivity(), R.layout.row_user_profile_segment, list, this);
        mListView.setAdapter(adapter);
        setListViewOnScrollListener();
        mListView.setOnItemClickListener(this);
    }

    private FrameLayout getHeaderView() {
        FrameLayout placeHolderView = (FrameLayout) LayoutInflater.from(getActivity()).inflate(R.layout.header_placeholder, mListView, false);
        AbsListView.LayoutParams params = new AbsListView.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, height);
        placeHolderView.setLayoutParams(params);
        return placeHolderView;
    }


    private void initUserInfoList(ProfileClass profile) {
        list.clear();
        addProfileSegment(profile.getStatus_msg(), R.drawable.ic_about, isCurrentUser ? R.drawable.ic_edit_status : 0, "Status", Constants.ProfileParams.STATUS);
        addProfileSegment(profile.getBusiness_name(), R.drawable.ic_company, isCurrentUser ? R.drawable.ic_edit_status : 0, "Business Name", Constants.ProfileParams.BUSINESS);
        if (isCurrentUser) {
            addProfileSegment(profile.getPhone(), R.drawable.ic_phone, 0, "Phone", "");
        } else {
            addProfileSegment(profile.getPhone(), R.drawable.ic_phone, R.drawable.ic_sidebar_chat, "Phone", "");
        }
        addProfileSegment(profile.getEmail(), R.drawable.ic_email, isCurrentUser ? R.drawable.ic_edit_status : 0, "Email", Constants.ProfileParams.EMAIL);
        addProfileSegment(profile.getWebsite(), R.drawable.ic_website, isCurrentUser ? R.drawable.ic_edit_status : 0, "Website", Constants.ProfileParams.WEB);
        addProfileSegment(profile.getAddress(), R.drawable.ic_address, isCurrentUser ? R.drawable.ic_edit_status : 0, "Address", Constants.ProfileParams.ADDRESS);
        addProfileSegment("Real Estate", R.drawable.ic_company, 0, "Profession", Constants.ProfileParams.PROFESSION);
    }

    private void addProfileSegment(String text, int leftIconId, int rightIconId, String hintText, String type) {

        ProfileSegment p = new ProfileSegment();
        p.leftIconId = leftIconId;
        p.data = text;
        p.rightIconId = rightIconId;
        p.hintText = hintText;
        p.type = type;
        list.add(p);
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_user_info;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getActivity()).inflate(resourceID, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ProfileSegment segment = list.get(position);
        holder.leftIcon.setImageResource(segment.leftIconId);
        holder.info.setHint(segment.hintText);
        holder.info.setText(segment.data);
        holder.rightIcon.setImageResource(segment.rightIconId);
        holder.rightIcon.setTag(position);
        if (segment.rightIconId != 0) {
            holder.rightIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "Right Icon Clicked");
                    int pos = (int) view.getTag();
                    onRightIconClicked(pos);
                }
            });
        }
        return convertView;
    }

    private void onRightIconClicked(int i) {
        Log.d(TAG, "Item Clicked:" + i);
        switch (i) {
            case 0:
                //Status
                break;
            case 1:
                //Business Name
                break;
            case 2:
                // Phone
                onChatClicked();
                return;
            case 3:
                // Email
                break;
            case 4:
                // Website
                break;
            case 5:
                // Address
                break;
            case 6:
                return;
        }
        ProfileSegment s = list.get(i);
        Bundle b = new Bundle();
        b.putString("type", s.type);
        b.putString("val", s.data);
        Log.d(TAG, "Item Type:" + s.type + ", data:" + s.data);
        Intent editIntent = new Intent(getActivity(), EditProfile.class);
        editIntent.putExtras(b);
        startActivityForResult(editIntent, 1);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        i--;
        switch (i) {
            case 0:
                //Status
                break;
            case 1:
                // Business Name
                break;
            case 2:
                // Phone
                onCallClicked(list.get(i).data);
                break;
            case 3:
                // Email
                onEmailClicked(list.get(i).data);
                break;
            case 4:
                // Website
                onWebsiteClicked(list.get(i).data);
                break;
            case 5:
                // Address
                onAddressClicked(list.get(i).data);
                break;
        }
    }

    private void onChatClicked() {

        ProfileClass profileClass = profile;

        Utility.userStats(getActivity(), "im");

        HashMap<String, Object> dataIm = new HashMap<String, Object>();
        dataIm.put("imTo", profileClass.getPhone());
        dataIm.put("imFrom", PBPreferences.getPhone());

        Analytics.trackEventWithProperties(R.string.consumption, R.string.e_im, dataIm);

        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        if (null != mAuthorId) {
            wrData.put("UserId", mAuthorId);
        }
        wrData.put("Action", "Chat");

        Analytics.trackEventWithProperties(R.string.profile, R.string.e_profile_act, wrData);
        Intent intent = new Intent(getActivity(), XMPPChat.class);
        String num = profileClass.getPhone();
        String displayName = ActivityUtils.getDisplayNameFromPhone( num);
        if (displayName != null && !TextUtils.isEmpty(displayName)) {
            intent.putExtra("name", displayName);

        } else {
            intent.putExtra("name", profileClass.getName());

        }
        intent.putExtra("img", profileClass.getImg());
        intent.putExtra("last_seen", profileClass.getTime());

        intent.putExtra("phone_key", num);
        intent.putExtra("userid", profileClass.getAuthorid() + "");

        startActivity(intent);
        //startActivityForResult(intent, Constants.REVIEW_REQUEST_CODE);
    }

    private void onCallClicked(String phoneNumber) {
        if (isCurrentUser) return;
        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        if (null != mAuthorId) {
            wrData.put("UserId", mAuthorId);
        }


        if (!TextUtils.isEmpty(phoneNumber)) {


            // ActivityUtils.callPhone(getActivity(), phoneNumber, wrData);
            ActivityUtils.initiateCallDialog(getActivity(), profile, new ArrayList<String>(), phoneNumber, wrData, profile.getIsFreeCall(), profile.getImg(), profile.getName());


        } else {
            Toast.makeText(getActivity(),
                    "Invalid Phone", Toast.LENGTH_SHORT).show();
        }
    }

    private void onEmailClicked(String email) {
        if (null != email && !TextUtils.isEmpty(email)) {
            PBSharingManager.sendMail(getActivity(), email, "", "", "");
            trackData("Send Email");
        }
    }

    private void onAddressClicked(String addr) {
        if (null != addr && !TextUtils.isEmpty(addr)) {
            // open map
            Uri geo = Uri.parse("geo:0,0?q=" + addr);
            PBSharingManager.showMap(geo, getActivity());
            trackData("View Address");
        }
    }

    private void onWebsiteClicked(String url) {
        if (null != url && !TextUtils.isEmpty(url)) {
            PBSharingManager.openWebUrl(url, getActivity());
            trackData("Open Website");
        }
    }

    private void trackData(String action) {
        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        if (null != mAuthorId) {
            wrData.put("UserId", mAuthorId);
        }
        wrData.put("Action", action);
        Analytics.trackEventWithProperties(R.string.profile, R.string.e_profile_act, wrData);
    }

    @Override
    public void onProfileRefresh(ProfileClass profile) {
        Log.d("UserInfo", "OnProfile Refresh");
        this.profile = profile;
//        setViews();
        initUserInfoList(profile);
        adapter.notifyDataSetChanged();
    }


    private class ProfileSegment {
        int leftIconId;
        int rightIconId;
        String data;
        String hintText;
        String type;

    }

    private class ViewHolder {
        ImageView leftIcon, rightIcon;
        TextView info;

        public ViewHolder(View view) {
            leftIcon = (ImageView) view.findViewById(R.id.iv_icon_user_segment);
            info = (TextView) view.findViewById(R.id.tv_userInfo);
            rightIcon = (ImageView) view.findViewById(R.id.iv_rightIcon);
        }
    }
}
