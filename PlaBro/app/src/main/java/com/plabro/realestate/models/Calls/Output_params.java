package com.plabro.realestate.models.Calls;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Output_params {

    @SerializedName("data")
    @Expose
    private List<CallData> data = new ArrayList<CallData>();

    /**
     * @return The data
     */
    public List<CallData> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<CallData> data) {
        this.data = data;
    }

}