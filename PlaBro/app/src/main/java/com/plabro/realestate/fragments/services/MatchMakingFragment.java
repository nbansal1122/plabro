package com.plabro.realestate.fragments.services;

import android.view.View;
import android.widget.ExpandableListView;

import com.plabro.realestate.R;
import com.plabro.realestate.adapter.MatchMakingAdapter;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.matchMaking.MatchMakingResponse;
import com.plabro.realestate.models.services.ServiceResponse;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.PBConditionalDialogView;

/**
 * Created by nitin on 10/12/15.
 */
public class MatchMakingFragment extends PlabroBaseFragment implements VolleyListener, PBConditionalDialogView.OnPBConditionListener {

    private ExpandableListView listView;
    private MatchMakingResponse matchMakingResponse = new MatchMakingResponse();
    private MatchMakingAdapter adapter;
    private ProgressWheel mProgressWheel;
    PBConditionalDialogView pbConditionalDialogView;

    public void initProgressWheel() {
        mProgressWheel = (ProgressWheel) findView(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
    }

    @Override
    public String getTAG() {
        return "Match Making";
    }

    @Override
    protected void findViewById() {
        listView = (ExpandableListView) findView(R.id.expListView);
        initProgressWheel();
        pbConditionalDialogView = (PBConditionalDialogView) findView(R.id.pbConditionalView);
        setTitle("Match Making Service");
        fetchMatchMakingList();
    }

    private void setAdapter() {
        adapter = new MatchMakingAdapter(getActivity(), matchMakingResponse.getOutput_params().getData());
        listView.setAdapter(adapter);
        for (int i = 0; i < matchMakingResponse.getOutput_params().getData().size(); i++) {
            listView.expandGroup(i);
        }
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                return true;
            }
        });

    }

    private void fetchMatchMakingList() {
        mProgressWheel.spin();
        pbConditionalDialogView.setVisibility(View.GONE);
        ParamObject obj = new ParamObject();
        obj.setClassType(MatchMakingResponse.class);
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.GET_PERSONAL_PAGE, null));
        AppVolley.processRequest(obj, this);
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_matchmaking;
    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        mProgressWheel.stopSpinning();
        matchMakingResponse = (MatchMakingResponse) response.getResponse();
        if (null != matchMakingResponse && null != matchMakingResponse.getOutput_params() && null != matchMakingResponse.getOutput_params().getData()) {
            setAdapter();
        } else {
// Show empty view for this
        }
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        mProgressWheel.stopSpinning();
        onApiFailure(response, taskCode);
    }

    @Override
    public void onInternetException(PBResponse response, int taskCode) {
        super.onInternetException(response, taskCode);
        if (isRunning) {
            pbConditionalDialogView.setVisibility(View.VISIBLE);
            pbConditionalDialogView.setType(PBConditionalDialogView.ConditionalDialog.INTERNET_CONNECTION);
            pbConditionalDialogView.setOnPBConditionListener(this);
        }
    }


    @Override
    public void onRefresh() {
        fetchMatchMakingList();
    }
}
