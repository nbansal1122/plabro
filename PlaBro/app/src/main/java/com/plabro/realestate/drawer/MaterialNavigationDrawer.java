package com.plabro.realestate.drawer;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plabro.realestate.AppController;
import com.plabro.realestate.BuildConfig;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.listeners.MaterialSectionListener;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlabroLocationManagerTwo;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;
import java.util.List;

@SuppressLint("InflateParams")
public abstract class MaterialNavigationDrawer extends PlabroBaseActivity implements MaterialSectionListener, FragmentManager.OnBackStackChangedListener {


    public static final int BOTTOM_SECTION_START = 100;
    public static final int SECTION_START = 0;
    private static int indexFragment = 0;

    private CharSequence title;
    private float density;
    private int primaryColor;

    private DrawerLayout layout;
    private ActionBar actionBar;
    private ActionBarDrawerToggle pulsante;
    private Toolbar toolbar;

    private LinearLayout sections;
    private RelativeLayout drawer;
    private ProgressWheel mProgressWheel;

    private LinearLayout mProfileSection;
    private RoundedImageView userImage;
    private TextView userName;

    protected static List<MaterialSection> sectionList;
    protected int lastSectionPosition = 0;

    UserLoginReceiver loginReceiver;
    private RelativeLayout mOptionsHeader;
    private boolean isSectionClicked;

    @Override
    protected int getResourceId() {
        return R.layout.activity_material_navigation_drawer;
    }

    @Override
    /**
     * Do not Override this method!!! <br>
     * Use init() instead
     */


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PlabroLocationManagerTwo.getInstance(this.getApplicationContext()).getLastKnownLocation();

        // init toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);


        // init drawer components
        drawer = (RelativeLayout) this.findViewById(R.id.drawer);
        sections = (LinearLayout) this.findViewById(R.id.sections);

        //init options
        mOptionsHeader = (RelativeLayout) this.findViewById(R.id.options_header);

        // init lists
        sectionList = new LinkedList<MaterialSection>();
        userImage = (RoundedImageView) this.findViewById(R.id.user_cover);
        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mProgressWheel.spin();
        userName = (TextView) this.findViewById(R.id.tv_nav_username);
        mProfileSection = (LinearLayout) this.findViewById(R.id.ll_profile_layout);
        // get density
        density = this.getResources().getDisplayMetrics().density;

        // get primary color
        Resources.Theme theme = this.getTheme();
        TypedValue typedValue = new TypedValue();
        theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
        primaryColor = typedValue.data;

        init(savedInstanceState);

        // INIT ACTION BAR
        this.setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);


        // Si preleva il titolo dell'activity
        title = sectionList.get(indexFragment).getTitle();

        // si collega il DrawerLayout al codice e gli si setta l'ombra all'apertura
        layout = (DrawerLayout) this.findViewById(R.id.drawer_layout);

        pulsante = new ActionBarDrawerToggle(this, layout, R.string.nothing, R.string.nothing) {

            public void onDrawerClosed(View view) {
                actionBar.setTitle(title);

                if (info != null) {
                    setDrawerAfterClosed(info.source, info.target, info.title, info.addToBackStack);
                }
                //invalidateOptionsMenu(); // termina il comando


            }

            public void onDrawerOpened(View drawerView) {
                Util.hideKeyboard(MaterialNavigationDrawer.this);
                Analytics.trackScreen(Analytics.LeftNav.LeftNavigationBarViewed);

            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

        };

        getSupportFragmentManager().addOnBackStackChangedListener(this);

        layout.setDrawerListener(pulsante);

        MaterialSection section = sectionList.get(0);
        section.select();
        setDrawerAfterClosed(null, section, section.getTitle(), true);

        onPush();

//        loginReceiver = new UserLoginReceiver();
//        registerReceiver(loginReceiver, new IntentFilter(Util.DO_LOGIN_RECEIVER));

    }

    public RelativeLayout getOptionsHeader() {
        return mOptionsHeader;
    }


    public Toolbar getToolbar() {
        return toolbar;
    }

    // Gestione dei Menu -----------------------------------------------------------------------------------------------------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return super.onCreateOptionsMenu(menu);
    }

    /* Chiamata dopo l'invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Se dal drawer si seleziona un oggetto
        if (pulsante.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // ------------------------------------------------------------------------------------------------------------------------------------------------

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        pulsante.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {// al cambio di orientamento dello schermo
        super.onConfigurationChanged(newConfig);

        // Passa tutte le configurazioni al drawer
        pulsante.onConfigurationChanged(newConfig);

    }

    public List<MaterialSection> getSectionList() {
        return sectionList;
    }

    public void setSectionList(List<MaterialSection> sectionList) {
        this.sectionList = sectionList;
    }


    @Override
    public void setTitle(CharSequence title) {
        this.title = title;
        if (null != this.getSupportActionBar()) {
            this.getSupportActionBar().setTitle(title);
        }
    }

    private void setFragment(MaterialSection source, MaterialSection target, String title, boolean addToBackStack) {

        if (source != null && source.getTitle().equals(target.getTitle())) {
            layout.closeDrawer(drawer);
            return;
        }
        info = new DrawerInfo();
        info.source = source;
        info.target = target;
        info.title = title;
        info.addToBackStack = addToBackStack;

        if (null != layout && drawer.isShown()) {
            layout.closeDrawer(drawer);
        } else {
            setDrawerAfterClosed(info.source, info.target, info.title, info.addToBackStack);
//            if(null != drawer && null != getLayout() && drawer.isShown() ) {
//                getLayout().closeDrawer(drawer);
//            }else{
//                setDrawerAfterClosed(info.source, info.target, info.title, info.addToBackStack);
//            }
        }


    }

    private void setDrawerAfterClosed(MaterialSection source, MaterialSection target, String title, boolean addToBackStack) {
        try {
            FragmentManager fragmentManager = this.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if (source != null) {
                if (source.isHomeFragment()) {
                    hideFragment(source);
                } else {
                    fragmentTransaction.remove(source.getTargetFragment());
                }
            }

            if (fragmentManager.findFragmentByTag(title) == null) {
                fragmentTransaction.add(R.id.frame_container, target.getTargetFragment(), title);
            } else if (target.isHomeFragment()) {
                showFragment(target);
            }

            if (addToBackStack) {
                fragmentTransaction.addToBackStack(title);
            }

            fragmentTransaction.commit();

            setTitle(title);
            if (null != target && target.isHomeFragment()) {
                if (lastSectionPosition == 0) {
                    setTitle(AppController.getInstance().getCityName());
                }
            }
            info = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private DrawerInfo info = null;

    private class DrawerInfo {
        MaterialSection source;
        MaterialSection target;
        String title;
        boolean addToBackStack;
    }

    public void clearBackStack(boolean isAll) {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        while (isAll ? fragmentManager.getBackStackEntryCount() != 0 : fragmentManager.getBackStackEntryCount() != 1) {
            fragmentManager.popBackStackImmediate();
        }
    }

    protected void removeFragment(MaterialSection section) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = section.getTargetFragment();

        fragmentTransaction.remove(fragment);
        fragmentTransaction.commit();
    }

    protected void hideFragment(MaterialSection section) {
        try {
            FragmentManager fragmentManager = this.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            Fragment fragment = section.getTargetFragment();

            fragmentTransaction.hide(fragment);
            fragmentTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void showFragment(MaterialSection section) {
        Log.d(TAG, "Show Fragment");
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = section.getTargetFragment();

        fragmentTransaction.show(fragment);
        fragmentTransaction.commit();
        onClick(section);
    }

    @Override
    public void onClick(MaterialSection section) {
        int position = section.getPosition();
        //  trackGAScreen(position);
        for (MaterialSection mySection : sectionList) {
            if (position != mySection.getPosition())
                mySection.unSelect();
        }
        if (section.getTarget() == MaterialSection.TARGET_FRAGMENT) {
            setFragment(sectionList.get(lastSectionPosition), section, section.getTitle(), false);
        } else {
            this.startActivity(section.getTargetIntent());
        }
        lastSectionPosition = section.getPosition();
        //  Analytics.trackEvent(this, getString(R.string.ga_ca_navigation_menu), section.getTitle());
    }

    private Bitmap convertToBitmap(Drawable drawable) {

        Bitmap mutableBitmap = Bitmap.createBitmap(drawable.getMinimumWidth(), drawable.getMinimumHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mutableBitmap);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        drawable.draw(canvas);

        return mutableBitmap;
    }

    private Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        // Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        // return _bmp;
        return output;
    }


    public void setDrawerUserImage() {
        mAppController = AppController.getInstance();


        if (mAppController != null && mAppController.getUserProfile() != null) {

            if (!TextUtils.isEmpty(mAppController.getUserProfile().getImg())) {
                Log.d(TAG, "Trying to load profile image on Profile updation");
                mProgressWheel.spin();
                String imgUrl = mAppController.getUserProfile().getImg();
                Picasso.with(MaterialNavigationDrawer.this).invalidate(imgUrl);
                Picasso.with(MaterialNavigationDrawer.this).load(imgUrl).into(userImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        setUserImageBitmap(ActivityUtils.getBitmapFromImageView(userImage));
                        mProgressWheel.stopSpinning();
                    }

                    @Override
                    public void onError() {
                        setUserImageDrawable(getResources().getDrawable(R.drawable.profile_default_one));
                        mProgressWheel.stopSpinning();
                    }
                });
            }


//            ImageRequest request = new ImageRequest(mAppController.getUserProfile().getImg(),
//                    new Response.Listener<Bitmap>() {
//                        @Override
//                        public void onResponse(Bitmap bitmap) {
//                            setUserImageBitmap(bitmap);
//                        }
//                    }, 500, 500, null,
//                    new Response.ErrorListener() {
//                        public void onErrorResponse(VolleyError error) {
//                            setUserImageDrawable(getResources().getDrawable(R.drawable.profile_default_one));
//                        }
//                    });
//            AppController.getInstance().addToRequestQueue(request, "Image");
        } else {
            setUserImageDrawable(getResources().getDrawable(R.drawable.profile_default_one));
        }
    }

    public void setUserImageDrawable(Drawable background) {
        //Bitmap bitmap = ((BitmapDrawable)background).getBitmap();
        //Drawable d = new BitmapDrawable(getResources(),  createBlurredImage(20, bitmap).get(0));
        // mProfileSection.setBackgroundDrawable(d);
        userImage.setImageDrawable(background);

    }

    public void setUserImageBitmap(Bitmap background) {
        mAppController = AppController.getInstance();
        userImage.setImageBitmap(background);


       /* List<Bitmap> bitmaps = ImageEffects.createBlurredImage(20, background, this);
        Drawable d = new BitmapDrawable(getResources(), bitmaps.get(0));
        if (Build.VERSION.SDK_INT >= 16) {
            mProfileSection.setBackground(d);
        } else {
            mProfileSection.setBackgroundDrawable(d);
        }

        userImage.setImageBitmap(bitmaps.get(1));
*/
    }

    public void setDrawerUserName() {
        mAppController = AppController.getInstance();

        if (mAppController != null && mAppController.getUserProfile() != null) {
            userName.setText(mAppController.getUserProfile().getName());
            if (mAppController.getCrashlytics() != null && !BuildConfig.DEBUG) {
                if (!TextUtils.isEmpty(mAppController.getUserProfile().getEmail())) {
                    mAppController.getCrashlytics().setUserEmail(mAppController.getUserProfile().getEmail());

                }

                if (!TextUtils.isEmpty(mAppController.getUserProfile().getPhone())) {
                    mAppController.getCrashlytics().setUserIdentifier(mAppController.getUserProfile().getPhone());

                }

                if (!TextUtils.isEmpty(mAppController.getUserProfile().getName())) {
                    mAppController.getCrashlytics().setUserName(mAppController.getUserProfile().getName());

                }
            }

        } else {
            userName.setText("");
        }
    }

    public void addSection(MaterialSection section) {

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (48 * density));
        sectionList.add(section);
        sections.addView(section.getView(), params);
    }


    public void addDivisor() {
        View view = new View(this);
        view.setBackgroundColor(getResources().getColor(R.color.pbDivider));
        // height 1 px
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 2);
        params.setMargins(dptopx(85), (int) (8 * density), dptopx(20), (int) (8 * density));


        sections.addView(view, params);
    }

    public int dptopx(float yourdpmeasure) {
        int px = 0;
        try {
            Resources r = getResources();
            px = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    yourdpmeasure,
                    r.getDisplayMetrics()
            );


        } catch (Exception e) {
            return px;

        }

        return px;

    }
    // create sections

    public MaterialSection newSection(String title, Drawable icon, boolean showPendingStatus, Fragment target) {
        MaterialSection section = new MaterialSection(this, sectionList.size(), MaterialSection.TARGET_FRAGMENT, false, showPendingStatus);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Drawable icon, boolean showPendingStatus, Fragment target, boolean isHomeFragment) {
        MaterialSection section = new MaterialSection(this, sectionList.size(), MaterialSection.TARGET_FRAGMENT, isHomeFragment, showPendingStatus);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Drawable icon, boolean showPendingStatus, Fragment target, boolean isHomeFragment, boolean isNewSection) {
        MaterialSection section = new MaterialSection(this, sectionList.size(), MaterialSection.TARGET_FRAGMENT, isHomeFragment, showPendingStatus, isNewSection);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Drawable icon, boolean showPendingStatus, Intent target) {
        MaterialSection section = new MaterialSection(this, sectionList.size(), MaterialSection.TARGET_ACTIVITY, false, showPendingStatus);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Bitmap icon, boolean showPendingStatus, Fragment target) {
        MaterialSection section = new MaterialSection(this, sectionList.size(), MaterialSection.TARGET_FRAGMENT, false, showPendingStatus);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Bitmap icon, boolean showPendingStatus, Fragment target, boolean isHomeFragment) {
        MaterialSection section = new MaterialSection(this, sectionList.size(), MaterialSection.TARGET_FRAGMENT, isHomeFragment, showPendingStatus);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Bitmap icon, boolean showPendingStatus, Intent target) {
        MaterialSection section = new MaterialSection(this, sectionList.size(), MaterialSection.TARGET_ACTIVITY, false, showPendingStatus);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, boolean showPendingStatus, Fragment target) {
        MaterialSection section = new MaterialSection(this, sectionList.size(), MaterialSection.TARGET_FRAGMENT, false, showPendingStatus);
        section.setOnClickListener(this);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, boolean showPendingStatus, Fragment target, boolean isHomeFragment) {
        MaterialSection section = new MaterialSection(this, sectionList.size(), MaterialSection.TARGET_FRAGMENT, isHomeFragment, showPendingStatus);
        section.setOnClickListener(this);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, boolean showPendingStatus, Intent target) {
        MaterialSection section = new MaterialSection(this, sectionList.size(), MaterialSection.TARGET_ACTIVITY, false, showPendingStatus);
        section.setOnClickListener(this);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }


    // abstract methods

    public abstract void init(Bundle savedInstanceState);

    public abstract void onPush();


    @Override
    public void onBackStackChanged() {
        Log.d(TAG, "BackStack Changed");
        int count = getSupportFragmentManager().getBackStackEntryCount();
        String text = "";
        for (int i = count - 1; i >= 0; i--) {
            FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(i);
            text = text + entry.getName() + " \n";
        }

        Util.e("BackStack ::: " + text + " \n --------- \n");


    }


    public class UserLoginReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {


            setDrawerUserImage();
            setDrawerUserName();


        }
    }

    public LinearLayout getmProfileSection() {
        return mProfileSection;
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);

        if (intent.getAction().equals(Constants.ACTION_PROFILE_UPDATE)) {
            Log.d(TAG, "on Received Profile Action Update");
            if (PBPreferences.getData(PBPreferences.IS_IMAGE_UPDATED, true)) {
                setDrawerUserImage();
                PBPreferences.saveData(PBPreferences.IS_IMAGE_UPDATED, false);
            }
            setDrawerUserName();
//            if (null != sectionList.get(lastSectionPosition) && sectionList.get(lastSectionPosition).isHomeFragment()) {
//                if (lastSectionPosition == 0) {
//                    setTitle(AppController.getInstance().getCityName());
//                }
//            }
        }
    }

    public DrawerLayout getLayout() {
        layout = (DrawerLayout) this.findViewById(R.id.drawer_layout);
        return layout;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
    }
}