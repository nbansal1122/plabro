package com.plabro.realestate.activity;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.plabro.realestate.R;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.ProgressWheel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ImageCropper extends PlabroBaseActivity {

    private CropImageView mCropImageView;
    private Toolbar mToolbar;
    private ProgressWheel mProgressWheel;
    private static final String IMAGE_PATH = "path";
    public static Bitmap croppedBitmap;
    String currentPhotoPath = "";

    @Override
    protected String getTag() {
        return "ImageCropper";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_cropper);
        mCropImageView = (CropImageView) findViewById(R.id.CropImageView);
        final ImageView iv_dummy = (ImageView) findViewById(R.id.iv_dummy);
        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mProgressWheel.spin();
        Bundle b = getIntent().getExtras();
        mCropImageView.setAspectRatio(10, 10);
        String imgUrl = getIntent().getExtras() == null ? "" : getIntent().getExtras().getString("img", "");
        if (!TextUtils.isEmpty(imgUrl)) {

            Picasso.with(ImageCropper.this).load(imgUrl).into(iv_dummy, new Callback() {
                @Override
                public void onSuccess() {
                    Bitmap image = ((BitmapDrawable) iv_dummy.getDrawable()).getBitmap();
                    mCropImageView.setImageBitmap(image);
                    mProgressWheel.stopSpinning();
                }

                @Override
                public void onError() {
                    Bitmap bitmap = ((BitmapDrawable) getResources().getDrawable(R.drawable.profile_default_one)).getBitmap();
                    mCropImageView.setImageBitmap(bitmap);
                    mProgressWheel.stopSpinning();
                }
            });

        } else {
            Bitmap bitmap = null;
            if (b != null) {
                bitmap = ImageCropper.croppedBitmap;
            }
            if (null == bitmap || bitmap.isRecycled()) {
                bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.profile_default_one)).getBitmap();
            } else {
                bitmap = Bitmap.createBitmap(bitmap);
            }
            mCropImageView.setImageBitmap(bitmap);
            mProgressWheel.stopSpinning();
        }

        inflateToolbar();
    }

    /**
     * On load image button click, start pick  image chooser activity.
     */
    public void onLoadImageClick(View view) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            new TedPermission(this)
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            startActivityForResult(getPickImageChooserIntent(), 200);
                        }

                        @Override
                        public void onPermissionDenied(ArrayList<String> arrayList) {
                            showToast("Allow permission to capture image");
                        }
                    })
                    .setDeniedMessage("Permission required by Plabro to take picture form Camera")
                    .setPermissions(Manifest.permission.CAMERA)
                    .setRationaleMessage("Permission required by Plabro to take picture form Camera")
                    .check();
        } else {
            startActivityForResult(getPickImageChooserIntent(), 200);
        }


    }

    /**
     * Crop the image and set it back to the  cropping view.
     */
    public void onCropImageClick(View view) {

        Log.d(TAG, "Cropped image view height :" + mCropImageView.getHeight() + ", width:" + mCropImageView.getHeight());
        Bitmap cropped = mCropImageView.getCroppedImage(500, 500);
        if (cropped != null) {
            mCropImageView.setImageBitmap(cropped);

//            try {
//                data.putExtra("img", cropped.copy(cropped.getConfig(), false));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            setResult(Activity.RESULT_OK);
            try {
                croppedBitmap = Bitmap.createBitmap(cropped);
            } catch (Exception e) {

            } catch (OutOfMemoryError e) {

            }
            finish();
        } else {
            Log.d(TAG, "Cropped is null");
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            try {
                Uri imageUri = getPickImageResultUri(data);
                Log.d(TAG, "image URI :" + imageUri);
                mCropImageView.setImageUri(imageUri);
            } catch (Exception e) {
                showSnackBar("Error capturing image. Please try again.");
            }
        }
    }

    /**
     * Create a chooser intent to select the  source to get image from.<br/>
     * The source can be camera's  (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).<br/>
     * All possible sources are added to the  intent chooser.
     */
    public Intent getPickImageChooserIntent() {

// Determine Uri of camera image to  save.     
        Uri outputFileUri = getCaptureImageOutputUri();


        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

// collect all camera intents     
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

// collect all gallery intents     
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

// the main intent is the last in the  list (fucking android) so pickup the useless one     
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

// Create a chooser from the main  intent     
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

// Add all other intents     
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture  by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            File file = new File(getImage.getPath(), "plabroProfile.jpeg");
            currentPhotoPath = file.getAbsolutePath();
            outputFileUri = Uri.fromFile(file);
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from  {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera  and gallery image.
     *
     * @param data the returned data of the  activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        return ((data != null && data.getData() != null) ? data.getData() : getCaptureImageOutputUri());
    }

    @Override
    public void findViewById() {

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void inflateToolbar() {
        //setting action bar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);


        // Inflate a menu to be displayed in the toolbar
        mToolbar.inflateMenu(R.menu.menu_image_cropper);

        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image_cropper, menu);
        return true;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(IMAGE_PATH, currentPhotoPath);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        currentPhotoPath = savedInstanceState.getString(IMAGE_PATH);
    }
}