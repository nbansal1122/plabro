package com.plabro.realestate.widgets.dialogs;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.plabro.realestate.R;
import com.plabro.realestate.utilities.ProgressWheel;


@SuppressLint("NewApi")
public class DOLoaderDialog extends DialogFragment {

    private View mView;
    private ProgressWheel mProgressWheel;

    private boolean onBackPressedCancel = false;

    private static final String BACK_PRESSED = "ON_BACK_PRESSED";

    public static DOLoaderDialog newInstance(boolean onBackPressedCancel) {
        DOLoaderDialog fragment = new DOLoaderDialog();
        Bundle args = new Bundle();
        args.putBoolean(BACK_PRESSED, onBackPressedCancel);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            onBackPressedCancel = getArguments().getBoolean(BACK_PRESSED);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        Drawable d = new ColorDrawable();
        d.setColorFilter(Color.TRANSPARENT, Mode.CLEAR);
        getDialog().getWindow().setBackgroundDrawable(d);

        getDialog().setCanceledOnTouchOutside(false);

        mView = inflater.inflate(R.layout.view_screen_loader, container);

        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstance) {
        super.onActivityCreated(savedInstance);

        mProgressWheel = (ProgressWheel) mView.findViewById(R.id.progress_wheel_screen_loader);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mProgressWheel.spin();


    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

        if (onBackPressedCancel)
            getActivity().onBackPressed();
    }
}
