package com.plabro.realestate.models.propFeeds;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.JsonSyntaxException;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.profile.ProfileClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 30/07/15.
 */
@Table(name = "MultiBrokerFeed")
public class MultiBrokerFeed extends BaseFeed {
    List<ProfileClass> brokers = new ArrayList<>();

    @Column
    private String _id;
    private String title="";
    private String subTitle="";

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public List<ProfileClass> getBrokers() {
        return brokers;
    }

    public void setBrokers(List<ProfileClass> brokers) {
        this.brokers = brokers;
    }

    public static MultiBrokerFeed parseJson(String json) throws JSONException {
        JSONArray array = new JSONArray(json);
        Log.d("MultiBrokerFeed", "Json Array :" + array);


        if (null != array && array.length() > 0) {
            List<ProfileClass> brokers = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);

                Log.d("JSON MultiBroker", "" + obj);
                try {
                    ProfileClass broker = (ProfileClass) ProfileClass.parseJson(obj.toString());
                    broker.setType("multi_broker");
                    brokers.add(broker);
                } catch (JsonSyntaxException e) {
                    Log.d("MultiBroker", "Exception While Parsing Profile");
                    e.printStackTrace();
                }

            }

            MultiBrokerFeed feed = new MultiBrokerFeed();
            feed.brokers = brokers;
            feed.setType("multi_broker");



            return feed;

        }

        return null;
    }
}
