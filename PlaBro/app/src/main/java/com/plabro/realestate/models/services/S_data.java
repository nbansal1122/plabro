package com.plabro.realestate.models.services;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class S_data implements Serializable {

    @SerializedName("Qu")
    @Expose
    private List<Question> Q = new ArrayList<Question>();

    public List<List<Question>> getQuestionsList() {
        return questionsList;
    }

    public void setQuestionsList(List<List<Question>> questionsList) {
        this.questionsList = questionsList;
    }

    @SerializedName("Q")
    @Expose
    private List<List<Question>> questionsList = new ArrayList<List<Question>>();

    @SerializedName("enabled")
    @Expose
    private Boolean enabled;
    @SerializedName("upfront_payment")
    @Expose
    private boolean upfront_payment;
    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("service_type")
    @Expose
    private String service_type;
    @SerializedName("Service Description")
    @Expose
    private String serviceDescription;
    @SerializedName("Service Summary")
    @Expose
    private String serviceSummary;
    @SerializedName("subscription_flag")
    @Expose
    private boolean subscription_flag;
    @SerializedName("deep_link")
    @Expose
    private String deep_link;

    public String getDeep_link() {
        return deep_link;
    }

    public void setDeep_link(String deep_link) {
        this.deep_link = deep_link;
    }

    public List<String> getPayment_params() {
        return payment_params;
    }

    public void setPayment_params(List<String> payment_params) {
        this.payment_params = payment_params;
    }

    public boolean isUpfront_payment() {
        return upfront_payment;
    }

    public void setUpfront_payment(boolean upfront_payment) {
        this.upfront_payment = upfront_payment;
    }

    @SerializedName("payment_params")
    @Expose
    private List<String> payment_params;

    public String getService_title() {
        return service_title;
    }

    public void setService_title(String service_title) {
        this.service_title = service_title;
    }

    public boolean isSubscription_flag() {
        return subscription_flag;
    }

    public void setSubscription_flag(boolean subscription_flag) {
        this.subscription_flag = subscription_flag;
    }

    public String getServiceSummary() {
        return serviceSummary;
    }

    public void setServiceSummary(String serviceSummary) {
        this.serviceSummary = serviceSummary;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    @SerializedName("service_title")
    @Expose
    private String service_title;


    /**
     * @return The Question
     */
    public List<Question> getQ() {
        return Q;
    }

    /**
     * @param Q The Question
     */
    public void setQ(List<Question> Q) {
        this.Q = Q;
    }

    /**
     * @return The enabled
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * @param enabled The enabled
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return The upfront_payment
     */
    public Boolean getUpfront_payment() {
        return upfront_payment;
    }

    /**
     * @param upfront_payment The upfront_payment
     */
    public void setUpfront_payment(Boolean upfront_payment) {
        this.upfront_payment = upfront_payment;
    }

    /**
     * @return The key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key The key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return The img
     */
    public String getImg() {
        return img;
    }

    /**
     * @param img The img
     */
    public void setImg(String img) {
        this.img = img;
    }

}