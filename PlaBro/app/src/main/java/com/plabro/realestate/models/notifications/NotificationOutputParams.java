package com.plabro.realestate.models.notifications;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class NotificationOutputParams {

    List<NotificationData> data = new ArrayList<NotificationData>();

    public List<NotificationData> getData() {
        return data;
    }
}
