package com.plabro.realestate.holders;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.activity.AuctionDetails;
import com.plabro.realestate.activity.BidParticipants;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.BiddingFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;

/**
 * Created by nitin on 28/01/16.
 */
public class MyBidHolder extends BaseFeedHolder {
    private TextView title, bidInfo, desc, bidStatusButton, time, reqSaleTv, bidParticipants;
    private ImageView auctionImage;

    public MyBidHolder(View itemView) {
        super(itemView);
        title = findTV(R.id.tv_auction_title);
        desc = findTV(R.id.tv_auction_text);
        bidInfo = findTV(R.id.tv_auction_subtitle);
        bidStatusButton = findTV(R.id.tv_auction_status);
        time = findTV(R.id.tv_auction_time_info);
        reqSaleTv = findTV(R.id.tv_req_sale);
        bidParticipants = findTV(R.id.tv_participants);
        auctionImage = (ImageView) findView(R.id.iv_auction);
    }

    @Override
    public void onBindViewHolder(final int position, BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        if (feed instanceof BiddingFeed) {
            final BiddingFeed f = (BiddingFeed) feed;
            title.setText(f.getTitle());
//            bidStatusButton.setText(f.getButton_title());
            time.setText(f.getBidTimeInfo());
            bidInfo.setText(f.getSugg_info());
            setFeedText(f, position);
            String pTypeText = "";
            if (f.getAvail_req() != null && f.getAvail_req().length > 0) {
                pTypeText = f.getAvail_req()[0];
            }
            if (f.getSale_rent() != null && f.getSale_rent().length > 0) {
                pTypeText = pTypeText + " for " + f.getSale_rent()[0];
            }
            reqSaleTv.setText(pTypeText);
            bidParticipants.setVisibility(View.VISIBLE);
            if(f.getTotal_bid_count()>0) {
                bidParticipants.setText(String.valueOf(f.getTotal_bid_count()));
                bidParticipants.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        trackEvent(Analytics.CardActions.BidParticipantsClicked, position, f.getAuction_id(), f.getType(), null);
                        Bundle b = new Bundle();
                        b.putString(Constants.BUNDLE_KEYS.BID_ID, f.getAuction_id() + "");
                        Intent i = new Intent(ctx, BidParticipants.class);
                        i.putExtras(b);
                        ctx.startActivity(i);
                    }
                });
            } else {
                bidParticipants.setVisibility(View.GONE);
            }
            bidStatusButton.setText(f.getAuction_status());
            if ("closed".equalsIgnoreCase(f.getAuction_status())) {
                bidStatusButton.setBackgroundResource(R.drawable.bg_status_close);
            } else {
                bidStatusButton.setBackgroundResource(R.drawable.rect_bg_btn_bid_status);
            }
            bidStatusButton.setText(f.getAuction_status());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    trackEvent(Analytics.CardActions.CardClicked, position, f.getAuction_id(), f.getType(), null);
                    onCardClick(f);
                }
            });
            ActivityUtils.loadImage(ctx, f.getImg_url(), auctionImage, 0);
        }
    }

    private void onCardClick(BiddingFeed f) {
        Intent i = new Intent(ctx, AuctionDetails.class);
        Bundle b = new Bundle();
        b.putString(Constants.BUNDLE_KEYS.AUCTION_ID, f.get_id());
        b.putString(Constants.BUNDLE_KEYS.TITLE, f.getTitle());
        b.putString(Constants.BUNDLE_KEYS.BID_TIME_STRING, f.getBidTimeInfo());
        b.putBoolean(Constants.BUNDLE_KEYS.IS_MY_BID, true);
        i.putExtras(b);
        ctx.startActivity(i);
    }


    private void setFeedText(BiddingFeed feed, int position) {
        String text = feed.getText();
        int start = 0;
        int end = 250;
        if (feed.getSnippet().size() > 0) {
            start = Integer.parseInt(feed.getSnippet().get(0));
            end = Integer.parseInt(feed.getSnippet().get(1)) + 1;
        }

        int occurance = text.length() - text.replace("\n", "").length();


        if (start >= 0 && end < text.length()) {
            try {
                text = text.substring(start, end);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //adding span for hashtags in post
        SpannableString hashTagText = ActivityUtils.setHashTags(feed, text, start, end, ctx, desc, getType(), position, false, getRefKey(), mActivity);//type and position send to track analytics

    }
}
