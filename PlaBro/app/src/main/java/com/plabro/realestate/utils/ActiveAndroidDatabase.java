package com.plabro.realestate.utils;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.entities.BookmarksDBModel;
import com.plabro.realestate.entities.FeedsDBModel;
import com.plabro.realestate.entities.MyShoutsDBModel;
import com.plabro.realestate.models.Calls.CallInfo;
import com.plabro.realestate.models.Endorsement;
import com.plabro.realestate.models.Tracker.WifiDBModel;
import com.plabro.realestate.models.Tracker.WifiData;
import com.plabro.realestate.models.XMPP.RosterData;
import com.plabro.realestate.models.chat.ChatMessage;
import com.plabro.realestate.models.city.CityClass;
import com.plabro.realestate.models.contacts.ContactInfo;
import com.plabro.realestate.models.contacts.PlabroFriends;
import com.plabro.realestate.models.login.Login;
import com.plabro.realestate.models.profile.Invitee;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.propFeeds.BuilderFeed;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.models.propFeeds.MultiBrokerFeed;
import com.plabro.realestate.models.propFeeds.NewsFeed;
import com.plabro.realestate.models.propFeeds.tabs.TabInfo;
import com.plabro.realestate.utilities.Utility;

import java.lang.ref.WeakReference;

/**
 * Created by nitin on 02/09/15.
 */
public class ActiveAndroidDatabase {
    private static final String TAG = "ActiveAndroidDatabase";
    private WeakReference<AppController> weakContext;
    private static Configuration configuration;
    private AppController controller;
    private static ActiveAndroidDatabase db = new ActiveAndroidDatabase();

    private ActiveAndroidDatabase() {

    }

    public static void initialize(AppController context) {
        if (null == db.weakContext) {
            db.weakContext = new WeakReference<AppController>(context);
        }

        if (null != db.weakContext.get()) {
            db.controller = db.weakContext.get();
        }
    }

    private void initDB() {
        try {
            initDB(Utility.getFolderPath());
            // Log.d(TAG, "DB Created At:" + ActiveAndroid.getDatabase().getPath());
        } catch (Exception e) {
            Log.i(TAG, "Exception initDB");
            initDB("");
            e.printStackTrace();
        }
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        }).start();

    }

    private void initDB(String folderPath) {
        ActiveAndroid.dispose();
        Configuration.Builder builder = new Configuration.Builder(db.controller);
        builder.setDatabaseName(folderPath + "plabro_aa.db");
        builder.setDatabaseVersion(40);
        builder.addModelClass(Login.class);
        builder.addModelClass(TabInfo.class);
        builder.addModelClass(MultiBrokerFeed.class);
        builder.addModelClass(NewsFeed.class);
        builder.addModelClass(FeedsDBModel.class);
        builder.addModelClass(BookmarksDBModel.class);
        builder.addModelClass(Endorsement.class);
        builder.addModelClass(MyShoutsDBModel.class);
        builder.addModelClass(ChatMessage.class);
        builder.addModelClass(ProfileClass.class);
        builder.addModelClass(Feeds.class);
        builder.addModelClass(WifiData.class);
        builder.addModelClass(WifiDBModel.class);
        builder.addModelClass(PlabroFriends.class);
        builder.addModelClass(ContactInfo.class);
        builder.addModelClass(RosterData.class);
        builder.addModelClass(Invitee.class);
        builder.addModelClass(CallInfo.class);
        builder.addModelClass(CityClass.class);
        builder.addModelClass(BuilderFeed.class);
        builder.addModelClass(com.plabro.realestate.models.history.History.class);
        ActiveAndroid.initialize(builder.create());
    }


}
