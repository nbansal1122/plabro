package com.plabro.realestate.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.FeedsCustomAdapter;
import com.plabro.realestate.listeners.HidingScrollListener;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.LoaderFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.PBConditionalDialogView;
import com.software.shell.fab.ActionButton;

import java.util.ArrayList;

public abstract class CardListActivity extends PlabroBaseActivity implements PBConditionalDialogView.OnPBConditionListener, ShoutObserver.PBObserver {

    protected PBConditionalDialogView mConditionalView;
    protected FeedsCustomAdapter mAdapter;
    protected RecyclerView mRecyclerView;
    protected ProgressWheel mProgressWheel;
    protected LinearLayoutManager mLayoutManager;
    protected ImageButton scrollUpButton;
    protected int hashKey;
    protected String refKey;
    protected String message_id = "";
    protected SwipeRefreshLayout mSwipeRefreshLayout;
    protected int page = 0;
    protected boolean isLoading = false;
    protected ArrayList<BaseFeed> baseFeeds = new ArrayList<BaseFeed>();
    protected Toolbar mToolbar;
    protected RelativeLayout mOptionHeader;
    protected RelativeLayout noFeeds;
    protected RelativeLayout scroll;
    protected ActionButton mScrollButton;
    protected boolean addMore = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_related_feeds, menu);
        return true;
    }


    @Override
    public void onTrialVersionExpired(PBResponse response, int taskCode) {
        super.onTrialVersionExpired(response, taskCode);
        showRegistrationCard();
    }

    @Override
    public void onSessionExpiry(PBResponse response, int taskCode) {
        super.onSessionExpiry(response, taskCode);
        showConditionalMessage(PBConditionalDialogView.ConditionalDialog.SESSION_EXPIRED, true, false);
        mConditionalView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onInternetException(PBResponse response, int taskCode) {
        super.onInternetException(response, taskCode);
        if (baseFeeds.size() < 1) {
            showConditionalMessage(PBConditionalDialogView.ConditionalDialog.INTERNET_CONNECTION, true, false);
            mConditionalView.setVisibility(View.VISIBLE);
        }
    }

    public void setRecyclerViewLayoutManager() {
        int scrollPosition = 0;

        mLayoutManager = new LinearLayoutManager(this);

        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    @Override
    protected String getTag() {
        return null;
    }

    @Override
    public void findViewById() {
        ShoutObserver.getInstance().addListener(this);
        mOptionHeader = (RelativeLayout) findViewById(R.id.options_header);
        mConditionalView = (PBConditionalDialogView) findViewById(R.id.pbConditionalView);
        page = 0;
        isLoading = false;
        noFeeds = (RelativeLayout) findViewById(R.id.noFeeds);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        setRecyclerViewLayoutManager();
        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mProgressWheel.spin();
        mRecyclerView.setOnScrollListener(hidingScrollListener);

        mAdapter = new FeedsCustomAdapter(baseFeeds, getTag(), this, hashKey);
        addMore = false;

        //pull to refresh
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "swipe refreshed");
                        page = 0;
                        addMore = false;
                        getData();

                    }
                }, Constants.GOOGLE_PULL_TO_REFRESH_TIMER);

            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


//        } else {
//            ((ViewGroup) rootView.getParent()).removeView(rootView);

        scroll = (RelativeLayout) findViewById(R.id.rl_scroll);
        mScrollButton = (ActionButton) scroll.findViewById(R.id.rl_scroll_btn);
        Utility.setupScrollActionButton(mScrollButton, this);
        scrollToTop();
        getData();

    }

    public void scrollToTop() {
        mScrollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayoutManager.scrollToPositionWithOffset(0, 0);
                mScrollButton.playHideAnimation();
                mScrollButton.setVisibility(View.GONE);
            }
        });
    }

    public void getData() {

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {
        showSpinners(addMore);

        getData();

    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onShoutUpdate() {

    }

    @Override
    public void onShoutUpdateGen(String type) {
        Log.d(TAG, "OnShout load more: " + type + addMore);
        if (type.equalsIgnoreCase(Constants.LOAD_MORE.SHOUT)) {
            if (addMore) {
                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                LoaderFeed lf = new LoaderFeed();
                lf.setType(Constants.FEED_TYPE.PROGRESS_MORE);
                baseFeeds.add(lf);
                mAdapter.notifyDataSetChanged();
            }

            getData();

        }
    }

    protected HidingScrollListener hidingScrollListener = new HidingScrollListener(20) {
        @Override
        public void onHide(RecyclerView recyclerView, int dx, int dy) {
            mScrollButton.playHideAnimation();
            mScrollButton.setVisibility(View.GONE);
            mScrollButton.setAnimation(animFadeOut);

        }

        @Override
        public void onShow(RecyclerView recyclerView, int dx, int dy) {
            mScrollButton.playShowAnimation();
            mScrollButton.setVisibility(View.VISIBLE);


        }

        @Override
        public void onScrolledFire(RecyclerView recyclerView, int dx, int dy) {

            Log.d(TAG, "Scroll Fired");
            int visibleItemCount = mLayoutManager.getChildCount();
            int totalItemCount = mLayoutManager.getItemCount();
            int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();

            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                //reached bottom show loading

            } else {

            }

            if (!isLoading) {
                if (totalItemCount > Constants.SCROLL_ITEM_COUNT) {

                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount - Constants.SCROLL_ITEM_COUNT) {
                        addMore = true;
                        getData();
                        isLoading = true;
                        Log.d(TAG, "Loading more after scroll");
                    }
                }
            }

            if (mLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                //Its at top ..
                mScrollButton.playHideAnimation();
                mScrollButton.setVisibility(View.GONE);

            }
            onScrolledFireChild(isLoading);

        }


    };

    protected void onScrolledFireChild(boolean isLoading) {

    }

    protected void stopSpinners() {

        mProgressWheel.stopSpinning();
        isLoading = false;
        mConditionalView.setVisibility(View.GONE);

        try {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 2000ms
                    mSwipeRefreshLayout.setRefreshing(false);

                }
            }, 2000);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void showSpinners(boolean addMore) {
        noFeeds.setVisibility(View.GONE);
        isLoading = true;
        Log.d(TAG, "AddMore:" + addMore);
        if (addMore) {
        } else {
            mSwipeRefreshLayout.setRefreshing(true);
            Log.d(TAG, "BaseFeed:" + baseFeeds.size());
            if (baseFeeds.size() < 1) {
                mProgressWheel.spin();
            }
        }
    }

    void showNoDataDialog(final Boolean addMore) {
        mProgressWheel.stopSpinning();
        isLoading = false;

    }

    protected void setAndCheckNoDataLayout() {
        if (null != mAdapter) {
            if (mAdapter.getItemCount() == 0) {
                noFeeds.setVisibility(View.VISIBLE);
            } else {
                noFeeds.setVisibility(View.GONE);

            }
        }
    }

    @Override
    public void inflateToolbar() {
        //setting action bar
        mToolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(mToolbar);

        // Set an OnMenuItemClickListener to handle menu item clicks
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Handle the menu item
                int id = item.getItemId();

                switch (id) {


                }
                return true;
            }

        });


        // Inflate a menu to be displayed in the toolbar
        mToolbar.inflateMenu(R.menu.menu_shout);

        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });


    }


}
