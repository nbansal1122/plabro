package com.plabro.realestate.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by hemantkumar on 23/07/15.
 */
public class PBLocalReceiver extends BroadcastReceiver {
    private HashSet<PBActionListener> listeners = new HashSet<PBActionListener>();

    public PBLocalReceiver(PBActionListener pbActionListener) {
        addListener(pbActionListener);
    }

    public static interface PBActionListener {
        String ACTION_REGISTRATION_UPDATE = "action_registration_update";
        String ACTION_REFRESH_AUCTIONS = "action_refresh_auctions";
        String ACTION_REFRESH_LISTING = "action_refresh_listing";
        String ACTION_KEY_EXCHANGE_SUCCESS = "action_key_exchange_success";
        String ACTION_KEY_EXCHANGE_FAILURE = "action_key_exchange_failure";
        String ACTION_BADGE_UPDATE = "action_badge_update";

        public void OnReceive(Context context, Intent intent);

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        for(PBActionListener l : listeners) {
            l.OnReceive(context, intent);
        }
    }

    public static void sendBadgeUpdateBroadcast(Context ctx){
        Intent i = new Intent();
        i.setAction(PBActionListener.ACTION_BADGE_UPDATE);
        ctx.sendBroadcast(i);
    }
    public void addListener(PBActionListener listener){
        listeners.add(listener);
    }

    public void removeListener(PBActionListener listener){
        listeners.remove(listener);
    }


}
