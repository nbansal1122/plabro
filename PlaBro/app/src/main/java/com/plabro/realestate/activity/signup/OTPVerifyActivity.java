package com.plabro.realestate.activity.signup;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.plabro.realestate.AppController;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.fragments.services.EditText;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.receivers.SMSListener;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utils.PostAsyncManager;

import java.util.HashMap;

/**
 * Created by nbansal2211 on 24/07/17.
 */

public class OTPVerifyActivity extends PlabroBaseActivity {
    private String mobileNumber, otpCode, ccode;
    private SMSListener smsListener;

    @Override
    protected String getTag() {
        return "OTPVerify";
    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_otp_verify;
    }

    @Override
    public void findViewById() {
        setClickListeners(R.id.btn_continue, R.id.tv_resend_code);
        setText(R.id.tv_mobile_number, mobileNumber);
        registerOTPReceiver();
    }

    @Override
    protected void loadBundleData(Bundle b) {
        super.loadBundleData(b);
        mobileNumber = b.getString(Constants.BUNDLE_KEYS.MOBILE, "");
        otpCode = b.getString(Constants.BUNDLE_KEYS.OTP, "");
        ccode = b.getString(Constants.BUNDLE_KEYS.COUNTRY_CODE, "");
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btn_continue:
                validateAndProceed();
                break;
            case R.id.tv_resend_code:
                sendOtp(ccode, mobileNumber);
                break;
        }
    }

    private void validateAndProceed() {
        if (isValid()) {
            setResult(RESULT_OK, getIntent());
            finish();
        }
    }

    private boolean isValid() {
        String text = getEditTextString(R.id.et_mobile);
        if (text.isEmpty()) {
            showToast(R.string.error_otp);
            return false;
        } else if (!text.equalsIgnoreCase(otpCode)) {
            showToast(R.string.error_invalid_otp);
            return false;
        }
        return true;
    }

    private ProgressDialog dialog;

    private void showProgressDialog() {
        if (dialog == null) {
            dialog = new ProgressDialog(this);
        }
        dialog.setMessage("Loading...");
        dialog.show();
    }

    private void hideProgressDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private void sendOtp(String ccode, String mobileNumber) {
        if (!Util.haveNetworkConnection(AppController.getInstance())) {
            showToast(R.string.error_internet);
            return;
        }
        showProgressDialog();
        this.otpCode = Util.generatePIN();
        ParamObject obj = MobileActivity.getParamObject(ccode, mobileNumber, otpCode);
        PostAsyncManager manager = new PostAsyncManager(new PostAsyncManager.AsyncListener() {
            @Override
            public void onPostExecute(boolean result) {
                hideProgressDialog();
                if (result) {
                    showToast(R.string.resend_otp_success);
                } else {
                    showToast(R.string.error_sending_otp);
                }
            }
        }, obj);
        manager.execute();
    }

    @Override
    public void reloadRequest() {

    }

    private void registerOTPReceiver() {
        if (isPermissionGranted()) {
            if (smsListener == null) {
                smsListener = new SMSListener(new SMSListener.OTPCallback() {
                    @Override
                    public void onOTPReceived(String otp) {
                        android.widget.EditText et = (android.widget.EditText) findViewById(R.id.et_mobile);
                        et.setText(otp);
                        validateAndProceed();
                    }
                });
            }
            registerReceiver(smsListener, getTelephonyFilter());
        }
    }

    private IntentFilter getTelephonyFilter() {
        IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        return filter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (smsListener != null) {
            unregisterReceiver(smsListener);
        }
    }

    private boolean isPermissionGranted() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }
}
