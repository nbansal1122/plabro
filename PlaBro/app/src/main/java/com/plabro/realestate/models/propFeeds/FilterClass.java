package com.plabro.realestate.models.propFeeds;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 01/06/15.
 */
public class FilterClass {

    String displayname="";
    String value="";

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    String key = "";
    String title = "";
    List<FilterValues> values = new ArrayList<>();
    boolean isFirstSelected = false;
    boolean isSecondSelected = false;

    public boolean isFirstSelected() {
        return isFirstSelected;
    }

    public void setIsFirstSelected(boolean isFirstSelected) {
        this.isFirstSelected = isFirstSelected;
    }

    public boolean isSecondSelected() {
        return isSecondSelected;
    }

    public void setIsSecondSelected(boolean isSecondSelected) {
        this.isSecondSelected = isSecondSelected;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<FilterValues> getValues() {
        return values;
    }

    public void setValues(List<FilterValues> values) {
        this.values = values;
    }
}
