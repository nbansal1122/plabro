package com.plabro.realestate.models.services;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.models.ServerResponse;

/**
 * Created by nitin on 19/01/16.
 */
public class ListingDetailResponse extends ServerResponse {
    @SerializedName("output_params")
    @Expose
    private ListingDetailOutputParams output_params;

    public ListingDetailOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(ListingDetailOutputParams output_params) {
        this.output_params = output_params;
    }
}
