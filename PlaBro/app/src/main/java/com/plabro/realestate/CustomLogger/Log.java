package com.plabro.realestate.CustomLogger;

import com.plabro.realestate.AppController;

/**
 * Created by nitin on 24/06/15.
 */
public class Log {
    // To Do PlabroAPI
    // Chat URL
    // Logs
    // XMPP Debugger
    public static boolean isEnabled = true;

    public static void setEnabled(boolean enableOrDisable) {
        isEnabled = true;
    }

    public synchronized static void d(String TAG, String msg) {
        if (isEnabled)
            android.util.Log.i(TAG, ""+msg);
    }

    public synchronized static void i(String TAG, String msg) {
        if (isEnabled)
            android.util.Log.i(TAG, ""+msg);
    }

    public synchronized static void e(String TAG, String msg) {
        if (isEnabled)
            android.util.Log.e(TAG, ""+msg);
    }

    public synchronized static void w(String TAG, String msg) {
        if (isEnabled)
            android.util.Log.w(TAG, ""+msg);
    }

    public synchronized static void v(String TAG, String msg) {
        if (isEnabled)
            android.util.Log.v(TAG, ""+msg);
    }

    public synchronized static void w(String TAG, String msg, Exception e) {
        if (isEnabled)
            android.util.Log.v(TAG, ""+msg);
    }

    public synchronized static void e(String TAG, String msg, Exception e) {
        if (isEnabled)
            android.util.Log.v(TAG, ""+msg);
    }
}
