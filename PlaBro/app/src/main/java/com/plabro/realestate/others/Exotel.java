package com.plabro.realestate.others;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;


/**
 * Created by hemantkumar on 10/10/15.
 */
public class Exotel {

    private Context context;
    private ProgressDialog dialog;
    private static final String EXOTEL_TOKEN = "2540f81754953f6467093d3e527f3f895c9db352";
    private static final String EXOTEL_SID = "plabro1";
    private String CallerId = "01139595655";
    private String CallType = "trans";
    private String URL_PARAM = "http://my.exotel.in/exoml/start/54851";
    private ExotelListener listener;

    public Exotel(Context context, ExotelListener ref) {
        this.context = context;
        this.listener = ref;
    }


    void showDialog() {
        if (dialog == null || !dialog.isShowing()) {
            dialog = new ProgressDialog(context);
            dialog.setMessage("Loading...");
            dialog.show();
        }
    }

    private void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void showToast(String toastMessage) {
        Toast toast = Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);

        LinearLayout layout = (LinearLayout) toast.getView();
        if (layout.getChildCount() > 0) {
            TextView tv = (TextView) layout.getChildAt(0);
            tv.setGravity(Gravity.CENTER);
        }

        toast.show();
    }


    public String connectPhoneNumbersSync(String phoneFrom, String phoneTo) throws Exception {

        String response = "";
        if (!Util.haveNetworkConnection(context)) {

            showToast("Unable to initiate call.\nNo Internet Connection.");
            return null;
        }

        if (phoneFrom.length() > 10) {
            phoneFrom = "0" + phoneFrom.substring(3, phoneFrom.length());
        } else if (phoneFrom.length() == 10) {
            phoneFrom = "0" + phoneFrom;
        }


        if (phoneTo.length() > 10) {
            phoneTo = "0" + phoneTo.substring(3, phoneTo.length());
        } else if (phoneTo.length() == 10) {
            phoneTo = "0" + phoneTo;
        }
        response = connectTwoNumbers(phoneFrom, phoneTo);

        return response;
    }

    private static class MyHostNameVerifier implements HostnameVerifier {

        public boolean verify(String string, SSLSession ssls) {
            return true;
        }
    }
    private String connectTwoNumbers(String phoneFrom, String phoneTo) throws Exception {

        java.net.URL url = new URL("");
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setHostnameVerifier(new MyHostNameVerifier());
        if (url.getUserInfo() != null) {
            byte[] data = url.getUserInfo().getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.DEFAULT);
            String basicAuth = "Basic " + base64;
            conn.setRequestProperty("Authorization", basicAuth);
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("From", phoneFrom);
        map.put("To", phoneTo);
        map.put("CallType", CallType);
        map.put("CallerId", CallerId);

        String params = AppVolley.getStringFromMap(map);
        initiatePostMethodRequest(conn, params);

        conn.connect();
        int responseCode = conn.getResponseCode();
        InputStream is = null;
        if (responseCode == 200) {
            is = conn.getInputStream();
        }

        if (is != null) {
            String data = Utility.convertStreamToString(is);
            if (!TextUtils.isEmpty(data)) {
                return data;
            }
        } else {

        }

        return "";
    }

    protected void initiatePostMethodRequest(HttpURLConnection httpURLConnection, String paramToSend)
            throws IOException {
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setConnectTimeout(15000);

        httpURLConnection.setReadTimeout(30000);
        DataOutputStream dataOutputStream = new DataOutputStream(
                httpURLConnection.getOutputStream());
        sendDataInPostMethod(dataOutputStream, paramToSend);

        dataOutputStream.flush();
        dataOutputStream.close();
    }

    protected void sendDataInPostMethod(DataOutputStream dataOutputStream,
                                        String paramToSend) throws IOException {
        dataOutputStream.writeBytes(paramToSend);
    }


    public void connectPhoneNumbers(final String phoneNumberFrom, final String phoneNumberTo) {
        new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
//                showDialog();
            }

            @Override
            protected String doInBackground(String... params) {
                try {
                    return connectPhoneNumbersSync(phoneNumberFrom, phoneNumberTo);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
//                hideDialog();
                super.onPostExecute(s);
                if (listener != null) {
                    listener.onPostExecute(s);
                }
            }
        }.execute();
    }

    public static interface ExotelListener {
        public void onPostExecute(String response);
    }


}
