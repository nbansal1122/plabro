package com.plabro.realestate.models.propFeeds;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Endorsement_data implements Serializable {

    @Expose
    private List<String> keys = new ArrayList<String>();
    @Expose
    private String title;

    /**
     * @return The keys
     */
    public List<String> getKeys() {
        return keys;
    }

    /**
     * @param keys The keys
     */
    public void setKeys(List<String> keys) {
        this.keys = keys;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

}