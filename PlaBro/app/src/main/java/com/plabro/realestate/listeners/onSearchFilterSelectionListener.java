package com.plabro.realestate.listeners;

import com.plabro.realestate.models.propFeeds.FilterClass;

/**
 * Created by Hemant on 23-01-2015.
 */
public interface onSearchFilterSelectionListener {

    public void onSelected(Boolean response, FilterClass ff);


}
