package com.plabro.realestate.widgets.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.plabro.realestate.Featured;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.Verification;
import com.plabro.realestate.uiHelpers.LinkMovementMethodOverride;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.Constants;

/**
 * Created by Hemant on 19-01-2015.
 */
public class GenericInfoDialog extends DialogFragment {

    Button mNeg, mPos;
    String neg, pos, title, text;
    FontText mTitle, mText;
    Activity mActivity;

    @SuppressLint("ValidFragment")
    public GenericInfoDialog(String neg, String pos, String title, String text) {
        this.neg = neg;
        this.pos = pos;
        this.text = text;
        this.title = title;
//        this.mActivity = activity;
    }

    public GenericInfoDialog() {

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View convertView =  inflater.inflate(R.layout.gen_info_dialog, null);
        builder.setView(convertView);
        setCancelable(true);
        mNeg = (Button) convertView.findViewById(R.id.neg);
        mPos = (Button) convertView.findViewById(R.id.pos);
        mTitle = (FontText) convertView.findViewById(R.id.title);
        mText = (FontText) convertView.findViewById(R.id.tv_text);
        setUpFields();

        return builder.create();
    }

    private void setUpFields() {

        if (TextUtils.isEmpty(neg)) {
            mNeg.setVisibility(View.GONE);
        } else {
            mNeg.setVisibility(View.VISIBLE);
            mNeg.setText(neg);

        }


        if (TextUtils.isEmpty(pos)) {
            mPos.setVisibility(View.GONE);
        } else {
            mPos.setVisibility(View.VISIBLE);
            mPos.setText(pos);
        }

        mTitle.setText(title);
        mText.setText(text);

        mNeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mPos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
