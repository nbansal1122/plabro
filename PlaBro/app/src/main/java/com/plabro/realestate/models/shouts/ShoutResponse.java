package com.plabro.realestate.models.shouts;


import com.plabro.realestate.models.ServerResponse;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class ShoutResponse extends ServerResponse {

    ShoutOutputParams output_params = new ShoutOutputParams();

    public ShoutOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(ShoutOutputParams output_params) {
        this.output_params = output_params;
    }
}
