package com.plabro.realestate.enums;

/**
 * Created by hemantkumar on 07/01/15.
 */
public enum ResponseType {

    JSONOBJECT(0), JSONARRAY(1), STRING(2);


    private int serial;


    ResponseType(int serial) {
        this.serial = serial;
    }

    public static int getResponseSerial(ResponseType method) {
        return method.serial;
    }

    public static ResponseType getResponseType(int serial) {
        switch (serial) {
            case 0:
                return ResponseType.JSONOBJECT;
            case 1:
                return ResponseType.JSONARRAY;
            case 2:
                return ResponseType.STRING;
            default:
                return ResponseType.JSONOBJECT;
        }
    }


}
