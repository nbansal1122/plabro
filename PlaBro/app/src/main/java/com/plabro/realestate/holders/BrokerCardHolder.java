package com.plabro.realestate.holders;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.Profile;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.others.Analytics;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

/**
 * Created by hemantkumar on 17/08/15.
 */
public class BrokerCardHolder extends BaseFeedHolder {
    /*for user layout*/
    private final ImageView mBrokerImage;
    private final TextView mBrokerTitle;
    private final TextView mBrokerName;
    private final TextView mBrokerLocation;
    private final TextView mBrokerInfo;


    public BrokerCardHolder(View itemView) {
        super(itemView);

         /*for single broker card*/
        this.mBrokerImage = (ImageView) itemView.findViewById(R.id.tv_broker_image);
        this.mBrokerTitle = (TextView) itemView.findViewById(R.id.tv_title);
        this.mBrokerName = (TextView) itemView.findViewById(R.id.tv_broker_name);
        this.mBrokerLocation = (TextView) itemView.findViewById(R.id.tv_location);
        this.mBrokerInfo = (TextView) itemView.findViewById(R.id.tv_broker_info);


    }


    @Override
    public void onBindViewHolder(final int position, final BaseFeed feed) {
        Log.d("Multi Broker Card:", "ON Bind View Holder");
        super.onBindViewHolder(position, feed);
        if (feed instanceof ProfileClass) {
            BrokerCardHolder holder = this;


            final ProfileClass userProfile = (ProfileClass) feed;
            String brokerImg = userProfile.getImg();
            if (null != brokerImg && !TextUtils.isEmpty(brokerImg)) {
                Picasso.with(ctx).load(brokerImg).placeholder(R.drawable.profile_default_one).into(holder.mBrokerImage);
            }

            holder.mBrokerTitle.setText(TextUtils.isEmpty(userProfile.getTitle()) ? "New Broker" : userProfile.getTitle());
            holder.mBrokerName.setText(userProfile.getName());
            holder.mBrokerLocation.setText(userProfile.getLocation());
            holder.mBrokerInfo.setText(userProfile.getBusiness_name());  //todo change it to broker info
            holder.mBrokerImage.setTag(userProfile);
            holder.mBrokerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProfileClass userProfile = (ProfileClass) view.getTag();
                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("ScreenName", getTAG());
                    wrData.put("Action", "Single Broker Image Clicked");
                    trackEvent(Analytics.CardActions.ViewProfile, position, ((ProfileClass) feed).get_id(), feed.getType(), wrData);
                    Intent intent1 = new Intent(ctx, Profile.class);
                    intent1.putExtra("authorid", userProfile.getAuthorid());
                    ctx.startActivity(intent1);
                }
            });


        } else {
            return;
        }
    }

}
