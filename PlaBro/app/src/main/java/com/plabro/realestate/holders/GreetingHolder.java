package com.plabro.realestate.holders;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.activity.GreetingActivity;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.GreetingFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;

import java.util.HashMap;
import java.util.Objects;

/**
 * Created by nitin on 23/12/15.
 */
public class GreetingHolder extends BaseFeedHolder {
    private FontText title, subtitle, actionText;
    private ImageView img;

    public GreetingHolder(View itemView) {
        super(itemView);
        title = (FontText) findView(R.id.tv_greeting_title);
        subtitle = (FontText) findView(R.id.tv_subtitle);
        actionText = (FontText) findView(R.id.tv_greeting_send_sms);
        img = (ImageView) findView(R.id.iv_greeting_image);
    }

    @Override
    public void onBindViewHolder(final int position, BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        if (feed instanceof GreetingFeed) {
            final GreetingFeed f = (GreetingFeed) feed;
            title.setText(f.getTitle());
            subtitle.setText(f.getSubtitle());
            actionText.setText(f.getAction_text());
            ActivityUtils.loadImage(ctx, f.getImg(), img, 0);
            Log.d(TAG, "Image URL :" + f.getImg());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    trackEvent(Analytics.CardActions.CardClicked, position, f.get_id(), f.getType(), null);
                    Intent i = new Intent(ctx, GreetingActivity.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.BUNDLE_KEYS.GREETING_MSG, f.getMsg());
                    b.putString(Constants.BUNDLE_KEYS.SERVICE_CODE, f.getService_code());
                    i.putExtras(b);
                    ctx.startActivity(i);
                }
            });
        }
    }


}
