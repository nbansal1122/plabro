package com.plabro.realestate.holders;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.plabro.realestate.R;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.activity.BuilderFeeds;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ServerResponse;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.BuilderFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.HashMap;

/**
 * Created by nitin on 14/08/15.
 */
public class BuilderHolder extends BaseFeedHolder {
    private TextView chat, call, collaborate, builderDisplayName, builderInfo, desc, viewMoreFrom, time, readMoreNews, summ;
    private ImageView displayImg, builderLogo, expandFeed;

    public BuilderHolder(View v) {
        super(v);
        chat = findTV(R.id.tv_chat);
        call = findTV(R.id.tv_call);
        collaborate = findTV(R.id.tv_collaborate);
        builderDisplayName = findTV(R.id.tv_builder_name);
        builderInfo = findTV(R.id.tv_builder_info);
        desc = findTV(R.id.tv_builder_desc);
        viewMoreFrom = findTV(R.id.tv_view_more_from);
        time = findTV(R.id.tv_builderFeedTime);
        displayImg = (ImageView) findView(R.id.image_builder);
        builderLogo = (ImageView) findView(R.id.iv_builder_logo);
        readMoreNews = findTV(R.id.tv_read_more_news);
        summ = findTV(R.id.tv_builder_summ);
        expandFeed = (ImageView) findView(R.id.arrow_expand);
    }



    @Override
    public void onBindViewHolder(final int position, final BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        if (feed instanceof BuilderFeed) {
            BuilderHolder holder = this;
            final BuilderFeed f = (BuilderFeed) feed;
            holder.builderDisplayName.setText(f.getDisplayname());
            holder.desc.setText(f.getText());
            holder.builderInfo.setText(f.getUsername() + "  " + f.getTime());
            viewMoreFrom.setText("View more from " + f.getDisplayname());
            summ.setText(f.getSumm());
            String img = f.getDisplay_img();
            String logoImg = f.getLogo_img();
            ActivityUtils.loadImage(ctx, img, displayImg, R.drawable.default_bg_builder);
            displayImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityUtils.openImageGallery(ctx, position, f.getDisplay_img());
                }
            });
            ActivityUtils.loadImage(ctx, logoImg, builderLogo);
//        int elipseCount = holder.desc.getLayout().getEllipsisCount(2);
//        Log.d(TAG, "Ellipsecount" + elipseCount);

            holder.expandFeed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (f.isExpanded()) {
                        desc.setMaxLines(2);
                        expandFeed.setImageResource(R.drawable.ic_expand);
                    } else {
                        trackEvent(Analytics.CardActions.Expanded, position, f.get_id()+"", f.getType(), null);
                        expandFeed.setImageResource(R.drawable.ic_collapse);
                        desc.setMaxLines(25);
                    }
                    f.setIsExpanded(!f.isExpanded());
                }
            });
            if ("news".equalsIgnoreCase(f.getSub_type())) {
                call.setVisibility(View.GONE);
                chat.setVisibility(View.GONE);
                collaborate.setVisibility(View.GONE);
                readMoreNews.setVisibility(View.GONE);
            } else {
                call.setVisibility(View.VISIBLE);
                chat.setVisibility(View.VISIBLE);
                collaborate.setVisibility(View.VISIBLE);
                readMoreNews.setVisibility(View.GONE);
                holder.call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String phone = f.getBuilder_phone();
                        HashMap<String, Object> wrData = new HashMap<String, Object>();
                        PlabroBaseActivity act = (PlabroBaseActivity) ctx;
                        wrData.put("ScreenName", act.TAG);
                        wrData.put("Action", "Call");
                        wrData.put("callFrom", PBPreferences.getPhone());
                        wrData.put("callTo", phone);
                        wrData.put("source", "Builder");
                        wrData.put(Analytics.Params.CARD_POSITION, "" + position);
                        if (!TextUtils.isEmpty(phone)) {
                            ActivityUtils.callPhone(ctx, phone, wrData);
                        } else {
                            ActivityUtils.callPhone(ctx, Constants.PLABRO_CUSTOMER_CARE, wrData);
                        }
                    }
                });
                holder.chat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle b = new Bundle();
                        PlabroBaseActivity act = (PlabroBaseActivity) ctx;
                        HashMap<String, Object> wrData = new HashMap<String, Object>();
                        wrData.put("imTo", f.getProfile_phone());
                        wrData.put("imFrom", PBPreferences.getPhone());
                        wrData.put("ScreenName", act.TAG);
                        wrData.put("Action", "IM");
                        trackEvent(Analytics.CardActions.Chat, position, f.get_id()+"", f.getType(), wrData);
                        b.putString(Constants.BUNDLE_KEYS.PHONE_KEY, f.getProfile_phone());
                        b.putString(Constants.BUNDLE_KEYS.NAME, f.getUsername());
                        b.putBoolean(Constants.BUNDLE_KEYS.IS_FROM_BUILDER, true);
                        b.putString(Constants.BUNDLE_KEYS.BUILDER_PHONE, f.getBuilder_phone());
                        PlabroIntentService.startForNotifyingInteraction(wrData);
                        Intent i = new Intent(ctx, XMPPChat.class);
                        i.putExtras(b);
                        ctx.startActivity(i);
                    }
                });
                holder.collaborate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FontText c = (FontText) view;
                        c.setEnabled(false);
                        HashMap<String, Object> wrData = new HashMap<String, Object>();
                        wrData.put("collaboratePhone", f.getProfile_phone());
                        wrData.put("currentUserPhone", PBPreferences.getPhone());
                        wrData.put("source", "Builders");
                        wrData.put("ScreenName", TAG);
                        wrData.put("ClickedIndex", "" + position);


                        if ("collaborate".equalsIgnoreCase(c.getText().toString())) {
                            wrData.put("Action", "Collaborate");
                            trackEvent(Analytics.CardActions.Collaborated, position, f.get_id() + "", f.getType(), wrData);
                            sendCollaboration(true, f, c);
                        } else {
                            wrData.put("Action", "OptOut");
                            trackEvent(Analytics.CardActions.OptOut, position, f.get_id() + "", f.getType(), wrData);
                            sendCollaboration(false, f, c);
                        }
                    }
                });
            }
            Activity act = (Activity) ctx;
            if (act instanceof BuilderFeeds) {
                holder.viewMoreFrom.setVisibility(View.GONE);
            } else {
                holder.viewMoreFrom.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle b = new Bundle();
                        b.putString(Constants.BUNDLE_KEYS.BUILDER_ID, f.getBuilder_id() + "");
                        b.putString(Constants.BUNDLE_KEYS.BUILDER_NAME, f.getUsername() + "");
                        b.putInt(Constants.BUNDLE_KEYS.FRAGMENT_TYPE, Constants.FRAGMENT_TYPE.BUILDER_DETAIL_FRAGMENT);
                        Intent i = new Intent(ctx, BuilderFeeds.class);
                        i.putExtras(b);
                        ctx.startActivity(i);
                    }
                });
            }
        } else {
            return;
        }
    }

    private void sendCollaboration(boolean isCollaboration, BuilderFeed f, final FontText collaborationButton) {
        HashMap<String, String> params = new HashMap<>();
        params.put(Constants.BUNDLE_KEYS.BUILDER_ID, f.getBuilder_id() + "");
        params.put(Constants.BUNDLE_KEYS.SHOUT_ID, f.get_id() + "");
        String rt = PlaBroApi.RT.BUILDER_COLLABORATE;
        int taskCode = Constants.TASK_CODES.BUILDER_COLLABORATE;
        if (!isCollaboration) {
            rt = PlaBroApi.RT.BUILDER_UN_COLLABORATE;
            taskCode = Constants.TASK_CODES.BUILDER_UN_COLLABORATE;
            setCollaboratedButton(true, collaborationButton);
        } else {
            setCollaboratedButton(false, collaborationButton);
        }
        params = Utility.getInitialParams(rt, params);
        final String RT = rt;
        final int TASK_CODE = taskCode;
        AppVolley.processRequest(TASK_CODE, ServerResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                collaborationButton.setEnabled(true);
                switch (taskCode) {
                    case Constants.TASK_CODES.BUILDER_COLLABORATE:
                        ServerResponse resp = (ServerResponse) response.getResponse();
                        if (null != resp) {
                            setCollaboratedButton(true, resp, collaborationButton);
                        }
                        break;
                    case Constants.TASK_CODES.BUILDER_UN_COLLABORATE:
                        ServerResponse resp2 = (ServerResponse) response.getResponse();
                        if (null != resp2) {
                            setCollaboratedButton(false, resp2, collaborationButton);
                        }
                        break;

                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                collaborationButton.setEnabled(true);
                Toast.makeText(ctx, response.getCustomException().getMessage(), Toast.LENGTH_SHORT).show();
                switch (taskCode) {
                    case Constants.TASK_CODES.BUILDER_COLLABORATE:
                        setCollaboratedButton(false, collaborationButton);
                        break;
                    case Constants.TASK_CODES.BUILDER_UN_COLLABORATE:
                        setCollaboratedButton(true, collaborationButton);
                        break;

                }
            }
        });
    }

    private void setCollaboratedButton(boolean isCollaborated, ServerResponse response, final FontText collaboratedButton) {
        if (response.isStatus()) {
            if (isCollaborated) {
                collaboratedButton.setText("Opt - Out");
                collaboratedButton.setTextColor(ctx.getResources().getColor(R.color.grey_collaboration_label));
                collaboratedButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_opt_out, 0, 0, 0);
            } else {
                collaboratedButton.setText("Collaborate");
                collaboratedButton.setTextColor(ctx.getResources().getColor(R.color.card_collaborate));
                collaboratedButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_collaborate, 0, 0, 0);
            }
        } else {
            Toast.makeText(ctx, response.getMsg() + "", Toast.LENGTH_LONG).show();
        }
    }

    private void setCollaboratedButton(boolean isCollaborated, final FontText collaboratedButton) {
        if (isCollaborated) {
            collaboratedButton.setText("Opt - Out");
            collaboratedButton.setTextColor(ctx.getResources().getColor(R.color.grey_collaboration_label));
            collaboratedButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_opt_out, 0, 0, 0);
        } else {
            collaboratedButton.setText("Collaborate");
            collaboratedButton.setTextColor(ctx.getResources().getColor(R.color.card_collaborate));
            collaboratedButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_collaborate, 0, 0, 0);
        }
    }
}
