package com.plabro.realestate.models.notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationData {

    @SerializedName("read")
    @Expose
    private Boolean read;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("params")
    @Expose
    private Params params;
    @SerializedName("time")
    @Expose
    private Double time;
    @SerializedName("_id")
    @Expose
    private Integer _id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("delete")
    @Expose
    private Boolean delete;




    /**
     * @return The read
     */
    public Boolean getRead() {
        return read;
    }

    /**
     * @param read The read
     */
    public void setRead(Boolean read) {
        this.read = read;
    }

    /**
     * @return The priority
     */
    public Integer getPriority() {
        return priority;
    }

    /**
     * @param priority The priority
     */
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    /**
     * @return The params
     */
    public Params getParams() {
        return params;
    }

    /**
     * @param params The params
     */
    public void setParams(Params params) {
        this.params = params;
    }

    /**
     * @return The time
     */
    public Double getTime() {
        return time;
    }

    /**
     * @param time The time
     */
    public void setTime(Double time) {
        this.time = time;
    }

    /**
     * @return The _id
     */
    public Integer get_id() {
        return _id;
    }

    /**
     * @param _id The _id
     */
    public void set_id(Integer _id) {
        this._id = _id;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The delete
     */
    public Boolean getDelete() {
        return delete;
    }

    /**
     * @param delete The delete
     */
    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

}