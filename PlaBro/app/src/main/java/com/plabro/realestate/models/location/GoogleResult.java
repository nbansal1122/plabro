package com.plabro.realestate.models.location;


/**
 * Created by hemantkumar on 08/01/15.
 */
public class GoogleResult {

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
