package com.plabro.realestate.listeners;

/**
 * Created by jmd on 5/1/2015.
 */
public interface MaterialDialogListener {
    int DIALOG_HOME_STATE = 1;
    int DIALOG_SESSION_EXPIRE = 2;

    public void onOkClicked(int dialogTypeCode);
    public void onCancelClicked(int dialogTypeCode);
    public void onDismissListener(int dialogTypeCode);
}
