package com.plabro.realestate.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.crashlytics.android.Crashlytics;
import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsSession;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.HomeActivity;
import com.plabro.realestate.R;
import com.plabro.realestate.Services.KeyExchangeService;
import com.plabro.realestate.activity.signup.MobileActivity;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.GenericObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.Tracker.Campaign;
import com.plabro.realestate.models.applaunch.AppLaunchResponse;
import com.plabro.realestate.models.login.Login;
import com.plabro.realestate.models.registerUser.RegisterResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.DeepLinkNavigationUtil;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.utils.DigitVerification;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.PBConditionalDialogView;

import java.util.HashMap;

//import com.optimizely.Optimizely;

public class MainActivity extends PlabroBaseActivity {

    private Button mCallCustomerCare;
    ProgressWheel mProgressWheel;
    private RelativeLayout newVersionAvailLayout, appBlockedVersionLayout, normalLayout;
    private TextView skipUpdate;
    private Button updateNow;
    private static String TAG = "PlaBroMain";
    PBConditionalDialogView mConditionalView;
    private boolean ftUser = true;
    private boolean sendingAppVersion;

    @Override
    protected String getTag() {
        return "MainActivity";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        showURLDialog();
        setContentView(R.layout.activity_main);
        setupInitialThings();
        findViewById();
        KeyExchangeService.startService(this);
        handleUserNavigation();
    }


    private void setupInitialThings() {
        //GA
        Analytics.trackScreen(Analytics.MainActivity.MainActivity);
        // Optimizely.enableEditor();
//        Optimizely.startOptimizelyWithAPIToken("AANJVwEBMIfHG2PwTI8UEO5G22k4SNPV~4065283383", getApplication());
        hitAppLaunch();
        CleverTapAPI cleverTap = null;
        // Plabro -Content
        // Google Android first open conversion tracking snippet
        // Add this code to the onCreate() method of your application activity

//        AdWordsConversionReporter.reportWithConversionId(this.getApplicationContext(),
//                "945617553", "gbBkCK2All8QkfXzwgM", "0.00", false);

        try {
            cleverTap = CleverTapAPI.getInstance(getApplicationContext());
            cleverTap.enablePersonalization();

        } catch (CleverTapMetaDataNotFoundException e) {
            e.printStackTrace();
        } catch (CleverTapPermissionsNotSatisfied cleverTapPermissionsNotSatisfied) {
            cleverTapPermissionsNotSatisfied.printStackTrace();
        }
        //testEncryption("");
    }

    private void hitAppLaunch() {
        ParamObject obj = new ParamObject();
        obj.setTaskCode(0);
        obj.setClassType(AppLaunchResponse.class);
        HashMap<String, String> params = new HashMap<>();
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.APP_LAUNCH, params));
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                AppLaunchResponse res = (AppLaunchResponse) response.getResponse();
                if (res != null && res.output_params != null && res.output_params.data != null) {
                    int version = res.output_params.data.json_share_version;
                    int savedVersion = PBPreferences.getData(PBPreferences.JSON_SHARE_VERSION, -1);
                    if (version > savedVersion) {
                        PBPreferences.saveData(PBPreferences.REQUEST_JSON_SHARE, true);
                        PBPreferences.saveData(PBPreferences.JSON_SHARE_VERSION, version);
                    }
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.get_started:
                if (checkPhonePermissions())
                    sendDigitVerification();
                break;
        }
    }

    private void handleUserNavigation() {

        ftUser = PBPreferences.getFirstTimeState();
        Log.v("PLABRO", "first time user status" + ftUser);
        hideViews(R.id.get_started);
        if (ftUser) {
            showViews(R.id.get_started);
            setClickListeners(R.id.get_started);
            if (checkPhonePermissions())
                sendDigitVerification();
        } else {

            startNextActivity();
//            redirectToHomePage();
        }

    }


    private void sendDigitVerification() {
        Analytics.trackScreen(Analytics.Registration.REGISTRATION);
        if (true) {
            Intent i = new Intent(this, MobileActivity.class);
            startActivityForResult(i, Constants.REQ_CODE_VERIFY_OTP);
            return;
        }
        DigitVerification.getInstance(AppController.getInstance()).authenticate("+91", new DigitVerification.DigitCallback() {
            @Override
            public void success(DigitsSession session, String phoneNumber) {
                Log.d(TAG, "on Success: Phone Number is :" + phoneNumber + ":Session Phn Number" + session.getPhoneNumber());
                if (TextUtils.isEmpty(phoneNumber)) {
                    phoneNumber = session.getPhoneNumber();
                    if (TextUtils.isEmpty(phoneNumber)) {
                        phoneNumber = PBPreferences.getPhone();
                        if (TextUtils.isEmpty(phoneNumber)) {
                            Digits.getSessionManager().clearActiveSession();
                            return;
                        }
                    }
                }
                Digits.getSessionManager().clearActiveSession();
                PBPreferences.setPhone(phoneNumber);
                Analytics.trackScreen(Analytics.Registration.Verification);
                registerUser();
            }

            @Override
            public void failure(DigitsException exception) {
                showToast(getString(R.string.unable_to_login));
                exception.printStackTrace();
                showViews(R.id.get_started);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQ_CODE_VERIFY_OTP) {
                String mobileNumber = data.getStringExtra(Constants.BUNDLE_KEYS.MOBILE);
                String ccode = data.getStringExtra(Constants.BUNDLE_KEYS.COUNTRY_CODE);
                PBPreferences.setPhone(ccode + mobileNumber);
                Analytics.trackScreen(Analytics.Registration.Verification);
                registerUser();
            }
        }
    }

    private Login getLoginObject() {
        Login login = new Select().from(Login.class).executeSingle();
        return login;
    }


    void initiateLogin() {
        Log.v(TAG, "Trying to login...");

        Login login = getLoginObject();
        Log.v(TAG, login + "");
        // Check if login is expired or not
        if (login != null) {
            // Login is not expired
            PBPreferences.setAuthKey(login.getAuthkey());
            Log.v(TAG, "login sussessfully");
            Log.v(TAG, "Step 4 : login successfully from object");
            PBPreferences.setTrialVersionState(login.getTrial_version());

            int ver = PBPreferences.getTrialVersionState();
            if (ver != -1) {
                //Navigate to home
                startNextActivity();
//                redirectToHomePage();
                return;
            }
        }


    }

    private void showURLDialog() {
        Uri uri = getIntent().getData();

        final StringBuilder builder = new StringBuilder();
        builder.append("TAG : " + TAG + "\n");
        builder.append("URI : " + uri + "\n");
        Bundle b = getIntent().getExtras();
        if (b != null) {
            for (String key : b.keySet()) {
                builder.append("KEY : " + key + " , VALUE : " + b.get(key) + "\n");
                Log.d("Intent Data:", key + "," + b.get(key));
                if (b.get(key) != null && b.get(key) instanceof Bundle) {
                    Bundle internalBundle = (Bundle) b.get(key);
                    for (String k : internalBundle.keySet()) {
                        builder.append("INTTERNAL KEY : " + k + " , VALUE : " + internalBundle.get(k) + "\n");
                    }
                }
            }
        } else {
            builder.append("Bundle Received is null");
            Log.d("Intent Data:", "Bundle is null");
        }
        Toast.makeText(this, "Deep Link Test Data is\n" + builder.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStatusFalse(PBResponse response, int taskCode) {
        super.onStatusFalse(response, taskCode);
        PBPreferences.setLoggedInState(false);

        mProgressWheel.stopSpinning();

        if (response.getCustomException().getMessage().contains("Phone not registered yet")) {
            AppController.getInstance().clearUserAndLogout();
            AppController.getInstance().recreateActivityCompat(MainActivity.this);
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Registration Error !")
                    .setMessage("Looks like you have registered from other device.")
                    .setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            moveTaskToBack(true);
                        }
                    }) // dismisses by default
                    .setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // do the acknowledged action, beware, this is run on UI thread
                            PBPreferences.setDeviceState(true);
                            Log.i("PlaBro", "Step 4 : New User with different phone");
                        }
                    })
                    .create()
                    .show();


        } else {

//            showGenericErrorMessage(response.getCustomException().getMessage());
            Log.i("PlaBro", "login failed due to :::" + response.getCustomException().getMessage());
        }


    }

    @Override
    public void onSessionExpiry(PBResponse response, int taskCode) {
        super.onSessionExpiry(response, taskCode);
        Utility.deleteLoginObject();
    }

    private void startNextActivity() {

        boolean isNewVersionAvailable = PBPreferences.getIsNewVersionAvailbale();
        int ver = PBPreferences.getAppUpdateVer();

        //get app version number
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        int version = pInfo.versionCode;

        boolean versionChangeStatus = false;

        if (ver != 0) {
            if (version < ver) {
                versionChangeStatus = true;
            }
        }


        if (isNewVersionAvailable || versionChangeStatus) {

            initAppUpdateLayout();

        } else {

            //exchange keys from server and send key to server
            redirectToHomePage();
        }


    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
    }


    @Override
    protected void onResume() {
        super.onResume();
        callScreenDataRequest();
//        AppEventsLogger.activateApp(MainActivity.this);
//        AdWordsConversionReporter.registerReferrer(this.getApplicationContext(), this.getIntent().getData());
//        AdWordsAutomatedUsageReporter.enableAutomatedUsageReporting(this.getApplicationContext(), Constants.CONVERSION_ID);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        AppEventsLogger.deactivateApp(MainActivity.this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initProgressWheel() {
        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.white));
        mProgressWheel.setProgress(0.0f);
    }

    private Button loginButton;

    @Override
    public void findViewById() {

        GenericObserver.getInstance().addListener(this, GenericObserver.GenericObserverInterface.EVENT_AUTHKEY_UPDATE);
//getSupportFragmentManager().beginTransaction().add(new ContactTagFragment(), ContactTagFragment.class.getSimpleName()).commit();
        if (getIntent().getExtras() != null) {
            String num = getIntent().getExtras().getString("phone_key");
            Log.d(TAG, "NUm From Push::" + num);
            createFbDeeplinkBundle();
        }
        loginButton = (Button) findViewById(R.id.get_started);
        setClickListeners(R.id.get_started);
        //initialise

        mConditionalView = (PBConditionalDialogView) findViewById(R.id.pbConditionalView);
//        setSpinner();

    }

    private Bundle fbDeeplinkBundle = new Bundle();

    private void createFbDeeplinkBundle() {
        try {
            Bundle b = getIntent().getExtras();
            if (b != null) {
                if (b.containsKey("al_applink_data")) {
                    Bundle bundle = b.getBundle("al_applink_data");
                    String targetUrl = bundle.getString("target_url");
                    if (!TextUtils.isEmpty(targetUrl)) {
                        fbDeeplinkBundle = DeepLinkNavigationUtil.getMap(targetUrl);
                        fbDeeplinkBundle.putBoolean("fromNotification", true);
                    }
                }
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }


//


    private boolean checkPhonePermissions() {

        return hasPermission(Manifest.permission.READ_PHONE_STATE, PlaBroApi.PERMISSIONS.READ_PHONE_STATE, PlaBroApi.PERMISSIONS_MESSAGE.READ_PHONE_STATE);
    }

    @Override
    public void onMMPermisssoinResult(int requestCode, boolean status, String message) {
        super.onMMPermisssoinResult(requestCode, status, message);
        if (!status) {
            showViews(R.id.get_started);
        }
        switch (requestCode) {
            case PlaBroApi.PERMISSIONS.READ_PHONE_STATE:
                if (status) {
                    Utility.initiateReadPhone();
                } else {
                    showSnackBar("Permission denied for READ_PHONE_STATE");
                }
                grantPermission(PlaBroApi.PERMISSIONS.WRITE_EXTERNAL_STORAGE);

                break;
            case PlaBroApi.PERMISSIONS.WRITE_EXTERNAL_STORAGE:
                if (status) {
                    grantPermission(PlaBroApi.PERMISSIONS.READ_EXTERNAL_STORAGE);
                } else {
                    showSnackBar("Permission denied for WRITE_EXTERNAL_STORAGE");
                    grantPermission(PlaBroApi.PERMISSIONS.READ_EXTERNAL_STORAGE);

                }

                break;
            case PlaBroApi.PERMISSIONS.READ_EXTERNAL_STORAGE:
                if (status) {
                    if (!AppController.getInstance().isDatabaseInitialized) {
                        AppController.getInstance().initDB();
                    }
                    grantPermission(PlaBroApi.PERMISSIONS.INTERNET);
                } else {
                    showSnackBar("Permission denied for READ_EXTERNAL_STORAGE");
                    grantPermission(PlaBroApi.PERMISSIONS.INTERNET);
                }
                break;

            case PlaBroApi.PERMISSIONS.INTERNET:
                if (status) {

                } else {
                    showSnackBar("Permission denied for INTERNET");
                }
                grantPermission(PlaBroApi.PERMISSIONS.RECEIVE_SMS);
                break;
            case PlaBroApi.PERMISSIONS.RECEIVE_SMS:
//                if (status) {
//
//                } else {
//                    showSnackBar("Permission denied for INTERNET");
//                }
//                grantPermission(PlaBroApi.PERMISSIONS.RECEIVE_SMS);
                break;
        }

    }

    protected void grantPermission(int permission) {

        switch (permission) {
            case PlaBroApi.PERMISSIONS.WRITE_EXTERNAL_STORAGE:
                hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, PlaBroApi.PERMISSIONS.WRITE_EXTERNAL_STORAGE, PlaBroApi.PERMISSIONS_MESSAGE.WRITE_EXTERNAL_STORAGE);
                break;
            case PlaBroApi.PERMISSIONS.READ_EXTERNAL_STORAGE:
                hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE, PlaBroApi.PERMISSIONS.READ_EXTERNAL_STORAGE, PlaBroApi.PERMISSIONS_MESSAGE.READ_EXTERNAL_STORAGE);
                break;
            case PlaBroApi.PERMISSIONS.INTERNET:
                hasPermission(Manifest.permission.INTERNET, PlaBroApi.PERMISSIONS.INTERNET, PlaBroApi.PERMISSIONS_MESSAGE.INTERNET);
                break;
            case PlaBroApi.PERMISSIONS.RECEIVE_SMS:
                hasPermission(Manifest.permission.RECEIVE_SMS, PlaBroApi.PERMISSIONS.RECEIVE_SMS, PlaBroApi.PERMISSIONS_MESSAGE.RECEIVE_SMS);
                break;
        }

    }


    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {
        callScreenDataRequest();
    }

    @Override
    public void inflateToolbar() {

    }


    void redirectToHomePage() {

        // encryptProgressDialog.dismiss();

        Log.i("PlaBro", "Step 5 : redirecting to home");
        boolean isContactSynced = PBPreferences.getData(PBPreferences.KEY_LAUNCH, false);
        Intent intent = null;
        if (isContactSynced) {
            intent = new Intent(MainActivity.this, InvitesList.class);
            PBPreferences.saveData(PBPreferences.KEY_LAUNCH, false);
        } else {
            intent = new Intent(MainActivity.this, HomeActivity.class);
        }
        Bundle b = getIntent().getExtras();
        b = (b == null) ? new Bundle() : b;
//        if (getIntent().getExtras() != null) {
//            intent.putExtras(getIntent().getExtras());
//            Log.d(TAG, "Extras Are not null");
//        } else {
//            Log.d(TAG, "Extras Are  null");
//        }
        b.putAll(fbDeeplinkBundle);
        intent.putExtras(b);
        startActivity(intent);
        finish();
    }

    void sendAppVersion() {
        Log.d(TAG, "Sending APP Version");
        //get app version number
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        int version = pInfo.versionCode;

        // Android version
        String androidVersion = android.os.Build.VERSION.RELEASE;

        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.GCM_REG_GET_PARAM_OSVERSION, androidVersion);
        params.put(PlaBroApi.PLABRO_UPDATEDAPPVERSION_POST_PARAM_APP_VERISON, version + "");

        params = Utility.getInitialParams(PlaBroApi.RT.UPDATEAPPVERSION, params);

        AppVolley.processRequest(0, null, null, String.format(PlaBroApi.getBaseUrl(this)), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

            }

        });

    }

    @Override
    public void onEventUpdate(String event) {
        super.onEventUpdate(event);
        if (sendingAppVersion) {
            sendAppVersion();
        }
    }

    private void initBlockedLayout() {
        showViews(R.id.rl_app_blocked);
        hideViews(R.id.frame_login, R.id.frame_updateAppVersion);
        mCallCustomerCare = (Button) findViewById(R.id.btn_call_for_block);
        mCallCustomerCare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityUtils.callPhone(MainActivity.this, Constants.PLABRO_CUSTOMER_CARE);
            }
        });
    }

    private void initAppUpdateLayout() {

        showViews(R.id.frame_updateAppVersion);
        hideViews(R.id.frame_login, R.id.rl_app_blocked);
        newVersionAvailLayout = (RelativeLayout) findViewById(R.id.rl_app_new_version);

        appBlockedVersionLayout = (RelativeLayout) findViewById(R.id.rl_app_blocked);
        skipUpdate = (TextView) findViewById(R.id.btn_skip);
        updateNow = (Button) findViewById(R.id.btn_update);


        //setting on click listener
        updateNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(PBPreferences.getAuthKey())) {
                    sendingAppVersion = true;
                    Utility.userLogin(MainActivity.this);
                } else {
                    sendAppVersion();
                }
                PBPreferences.setIsNewVersionAvailbale(false);
                String appUrl = PBPreferences.getAppUpdateUrl();
                if (TextUtils.isEmpty(appUrl)) {
                    appUrl = PlaBroApi.APP_URL;
                }
                PBSharingManager.openWebUrl(appUrl, MainActivity.this);
            }
        });

        //setting on click listener
        skipUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectToHomePage();
//                startNextActivity();
            }
        });
    }


    void registerUser() {
        final ProgressDialog progress;
        progress = ProgressDialog.show(MainActivity.this, "Please wait",
                "Registration in progress...", true);

        ParamObject obj = new ParamObject();
        obj.setClassType(RegisterResponse.class);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.IMEI, PBPreferences.getImei());
        params.put(PlaBroApi.PHONE, PBPreferences.getPhone());
        params.put(PlaBroApi.ISD_CODE, PBPreferences.getCountryCode());

        String campaignUri = PBPreferences.getData(PBPreferences.CAMPAIGN_URI, null);
        if (!TextUtils.isEmpty(campaignUri)) {
            params.putAll(Campaign.parseUri(campaignUri));
        }

        params = Utility.getInitialParamsWithoutAuthkey(PlaBroApi.RT.REGISTER_USER, params);
        obj.setParams(params);
        HashMap<String, Object> metaData = new HashMap<>();
        Analytics.sendRegistrationData(metaData);

        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);
                RegisterResponse registerResponse = (RegisterResponse) response.getResponse();
                //sendScreenName(R.string.funnel_SMS_APPROVED);
                //Navigate to preferences
                if (PBPreferences.getData(PBPreferences.KEY_AES_PROCESS, false)) {
//                    PBPreferences.setFirstTimeState(false);
                    Intent intent = new Intent(MainActivity.this, Preferences.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(MainActivity.this, KeyExchangeActivity.class);
                    startActivity(intent);
                    finish();
                }

                //        Log.i("PlaBro", "Account register sussessfully" + registerResponse.isStatus());


            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                showToast(response.getCustomException().getMessage() + "\nPlease try again");
                loginButton.setVisibility(View.VISIBLE);
                Utility.dismissProgress(progress);
                Log.i("PlaBro", "onFailure : Oops...something went wrong");

            }
        });
    }


}