package com.plabro.realestate.models.tnc;


import com.plabro.realestate.models.ServerResponse;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class TNCResponse extends ServerResponse {

    TNCOutputParams output_params = new TNCOutputParams();


    public TNCOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(TNCOutputParams output_params) {
        this.output_params = output_params;
    }

}
