package com.plabro.realestate.entities;

/*created using Android Studio (Beta) 0.8.14
www.101apps.co.za*/

public class FeedsData {


    String name;
    String time;
    String desc;
    String tags;
    String bookmark_flag;
    String poster_phone;
    int image;
    int post_id;
    int id_;

    public FeedsData() {
    }

    public FeedsData(int post_id , String name, String time,String desc, String tags,String bookmark_flag, int image,String poster_phone, int id_) {
        super();
        this.post_id = post_id;
        this.name = name;
        this.time = time;
        this.desc = desc;
        this.tags = tags;
        this.bookmark_flag = bookmark_flag;
        this.image = image;
        this.poster_phone = poster_phone;
        this.id_ = id_;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getBookmark_flag() {
        return bookmark_flag;
    }

    public void setBookmark_flag(String bookmark_flag) {
        this.bookmark_flag = bookmark_flag;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }

    public int getId() {
        return id_;
    }

    public int getPost_id() {
        return post_id;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    public String getPoster_phone() {
        return poster_phone;
    }

    public void setPoster_phone(String poster_phone) {
        this.poster_phone = poster_phone;
    }
}