package com.plabro.realestate.models.payment;

import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.List;

public class PaymentFinalData {

    private String transactionId = "";
    private float balance = 0;
    private List<Transaction> transactions = new ArrayList<>();
    private PaymentPostData postData = new PaymentPostData();
    private String message;
    private String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    private String info;

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    private float amount = 0;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }


    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public PaymentPostData getPostData() {
        return postData;
    }

    public void setPostData(PaymentPostData postData) {
        this.postData = postData;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }
}