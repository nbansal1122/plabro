package com.plabro.realestate.models.notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Params {
    public boolean isReverse_follow() {
        return reverse_follow;
    }

    public void setReverse_follow(boolean reverse_follow) {
        this.reverse_follow = reverse_follow;
    }

    @SerializedName("reverse_follow")
    @Expose
    private boolean reverse_follow = false;
    @SerializedName("deep_link")
    @Expose
    private String deepLink;
    @SerializedName("updated")
    @Expose
    private Boolean updated;
    @SerializedName("short")
    @Expose
    private String _short;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("userid")
    @Expose
    private long userid;
    @SerializedName("friend_list")
    @Expose
    private List<List<Integer>> friend_list = new ArrayList<List<Integer>>();
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String get_short() {
        return _short;
    }

    public void set_short(String _short) {
        this._short = _short;
    }

    @SerializedName("short_desc")
    @Expose
    private String short_desc;

    public long getShout() {
        return shout;
    }

    public void setShout(long shout) {
        this.shout = shout;
    }

    @Expose
    private long shout;

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    @Expose

    private String message_id;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getShort_desc() {
        return short_desc;
    }

    public void setShort_desc(String short_desc) {
        this.short_desc = short_desc;
    }

    @SerializedName("text")
    @Expose
    private String text;

    /**
     * @return The updated
     */
    public Boolean getUpdated() {
        return updated;
    }

    /**
     * @param updated The updated
     */
    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    /**
     * @return The _short
     */
    public String getShort() {
        return _short;
    }

    /**
     * @param _short The short
     */
    public void setShort(String _short) {
        this._short = _short;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The img
     */
    public String getImg() {
        return img;
    }

    /**
     * @param img The img
     */
    public void setImg(String img) {
        this.img = img;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The userid
     */
    public long getUserid() {
        return userid;
    }

    /**
     * @param userid The userid
     */
    public void setUserid(long userid) {
        this.userid = userid;
    }

    /**
     * @return The friend_list
     */
    public List<List<Integer>> getFriend_list() {
        return friend_list;
    }

    /**
     * @param friend_list The friend_list
     */
    public void setFriend_list(List<List<Integer>> friend_list) {
        this.friend_list = friend_list;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDeepLink() {
        return deepLink;
    }

    public void setDeepLink(String deepLink) {
        this.deepLink = deepLink;
    }
}