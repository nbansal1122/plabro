package com.plabro.realestate.xmpp;

import android.app.Dialog;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.XMPPChat;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;

import java.io.File;

/**
 * Gather the xmpp settings and create an XMPPConnection
 */
public class SettingsDialog extends Dialog implements View.OnClickListener {
    private XMPPChat xmppClient;

    public SettingsDialog(XMPPChat xmppClient) {
        super(xmppClient);
        this.xmppClient = xmppClient;
    }

    protected void onStart() {
        super.onStart();
        setContentView(R.layout.settings);
        getWindow().setFlags(4, 4);
        setTitle("XMPP Settings");
        Button ok = (Button) findViewById(R.id.ok);
        ok.setOnClickListener(this);
    }

    public void onClick(View v) {
        new Thread(new Runnable() {
            public void run() {
                String host = getText(R.id.host);
                String port = getText(R.id.port);
                String service = getText(R.id.service);
                String username = getText(R.id.userid);
                String password = getText(R.id.password);

                host = "api.plabro.com";
                port="5222";
                service = "plabro.com";
                username ="+919945796525";
                password ="+919945796525";

                //+919945796525@plabro.com/Smack

//                username ="+919416441810";
//               password ="+919416441810";

 /*               host = "talk.google.com";
                port="5222";
                service = "google.com";
                username ="hemant@timescity.com";
                password ="";
*/
                //username ="9945796525";
                //password ="9945796525";
                // username = "919711573435@s.whatsapp.net";
                // password = "3lYC8UinfbiTag61orQKO9Nb4Ao=";


                //SASLAuthentication.supportSASLMechanism("PLAIN", 0);



                // Create a connection
                 ConnectionConfiguration connConfig = new ConnectionConfiguration(host, Integer.parseInt(port), service);
                //ConnectionConfiguration connConfig = new ConnectionConfiguration(host, Integer.parseInt(port));
                // ConnectionConfiguration connConfig = new ConnectionConfiguration("chat.facebook.com", 5222);
                // ConnectionConfiguration connConfig = new ConnectionConfiguration("c.whatsapp.net", 5222, "Android-2.10.750-5222");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                    connConfig.setTruststoreType("AndroidCAStore");
                    connConfig.setTruststorePassword(null);
                    connConfig.setTruststorePath(null);
                } else {
                    connConfig.setTruststoreType("BKS");
                    String path = System.getProperty("javax.net.ssl.trustStore");
                    if (path == null)
                        path = System.getProperty("java.home") + File.separator + "etc" + File.separator + "security" + File.separator
                                + "cacerts.bks";
                    connConfig.setTruststorePath(path);
                }

                XMPPConnection connection = new XMPPConnection(connConfig);

                try {
                    connection.connect();
                    Log.i("XMPPClient", "[SettingsDialog] Connected to " + connection.getConnectionID() + " : " + connection.getHost()
                            + " : " + connection.getServiceName());
                } catch (XMPPException ex) {
                    Log.e("XMPPClient", "[SettingsDialog] Failed to connect to " + connection.getHost());
                    Log.e("XMPPClient", ex.toString());
                   // xmppClient.setConnection(null);
                }
                try {
                    connection.login(username, password);
                    Log.i("XMPPClient", "Logged in as " + connection.getUser());

                    // Set the status to available
                    Presence presence = new Presence(Presence.Type.unavailable);
                    connection.sendPacket(presence);
                  //  xmppClient.setConnection(connection);
                    Log.i("check", "connection complete without error");
                } catch (XMPPException ex) {
                    Log.e("XMPPClient", "[SettingsDialog] Failed to log in as " + username);
                    Log.e("XMPPClient", ex.toString());
                  //  xmppClient.setConnection(null);
                }
                dismiss();

            }
        }).start();
    }

    private String getText(int id) {
        EditText widget = (EditText) this.findViewById(id);
        return widget.getText().toString();
    }
}