package com.plabro.realestate.models.history;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by hemant on 21/4/15.
 */

@Table(name = "SearchHistory")
public class History extends Model {

    @Column
    int _id ;
    @Column(unique = true,onUniqueConflict = Column.ConflictAction.REPLACE)
    String text;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
