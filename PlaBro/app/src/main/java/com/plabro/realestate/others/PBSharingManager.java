package com.plabro.realestate.others;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.models.propFeeds.ShareFeed;
import com.plabro.realestate.utilities.Constants;

import java.util.List;


public class PBSharingManager {
    public static final String WHATSAPP_PACKAGE = "com.whatsapp";

    public static void shareByWhatsapp(View view, Activity activity, String sharingText) {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(android.content.Intent.EXTRA_TEXT, sharingText);

        PackageManager pm = activity.getApplicationContext().getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(sendIntent, 0);
        boolean temWhatsApp = false;
        for (final ResolveInfo info : matches) {
            if (info.activityInfo.packageName.startsWith("com.whatsapp")) {
                final ComponentName name = new ComponentName(info.activityInfo.applicationInfo.packageName, info.activityInfo.name);
                sendIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                sendIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
                sendIntent.setComponent(name);
                temWhatsApp = true;
                break;
            }
        }

        if (temWhatsApp) {
            //abre whatsapp
            activity.startActivity(sendIntent);
        } else {
            // Toast.makeText(activity, appActivity.getString(R.string.share_whatsapp), Toast.LENGTH_SHORT).show();
        }
    }

    public static void shareByAll(Context context, String shareString) {


        String url = Constants.PLABRO_APP_URL;
        shareString = shareString + "\n" + url;

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);

        sendIntent.putExtra(Intent.EXTRA_TEXT, shareString);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

    public static void shareByAll(Feeds feedSharing, Context ctx) {
        String feedSharedStr = feedSharing.getProfile_phone() + " : \n" + feedSharing.getText() + "\nBy: " + feedSharing.getProfile_name();
        PBSharingManager.shareByAll(ctx, feedSharedStr);
        if (!TextUtils.isEmpty(feedSharing.get_id())) {
            ShareFeed f = new ShareFeed();
            f.setFeed_id(feedSharing.get_id());
            f.save();
            PlabroIntentService.shareFeed(ctx);
        }
    }

    public static void sendMail(Activity activity, String email, String subject, String extra, String title) {
        // send email
                        /* Create the Intent */
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        /* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{email});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, extra);

    /* Send it off to the Activity-Chooser */
        activity.startActivity(Intent.createChooser(emailIntent, title));


    }

    public static void openWebUrl(String url, Context context) {
        try {
            url = url.trim();
            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                url = "http://" + url;
            }

            if (!url.contains("http")) {
                url = "http://" + url;

            }

            Intent viewIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            viewIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(viewIntent);
        }catch (Exception e){
            Crashlytics.logException(e);
        }
    }

    public static void showMap(Uri geoLocation, Context context) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoLocation);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }
}
