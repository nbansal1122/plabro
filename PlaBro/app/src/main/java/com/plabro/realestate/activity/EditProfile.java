package com.plabro.realestate.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.LocationSelectionListener;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.city.CityClass;
import com.plabro.realestate.models.city.CityResponse;
import com.plabro.realestate.models.registerUser.RegisterResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.FloatingEditText;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.ListDialog;

import java.util.ArrayList;
import java.util.HashMap;


public class EditProfile extends PlabroBaseActivity {

    //FloatingEditText mEmailEditText, mWebEditText, mAddrEditText, mNameEditText,mStatusEditText;
    FloatingEditText mEditField;
    private LinearLayout mFieldLayout;


    private Toolbar toolbar;
    private Activity mActivity;
    private String type, value, city, cityId;
    //private LinearLayout mEmailLayout, mWebLayout, mAddrLayout, mNameLayout,mStatusLayout;
    MenuItem menu_btn_enable;
    MenuItem menu_btn_disabled;
    private TextView cityTV;

    private ArrayList<String> cityNames = new ArrayList<>();
    private ArrayList<CityClass> cityList = new ArrayList<>();
    private HashMap<String, String> cityVsId = new HashMap<>();
    private boolean isCityChanged;
    private boolean isCitiesFeteched;

    @Override
    protected String getTag() {
        return "EditProfile";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        Bundle bundle = getIntent().getExtras();

        type = bundle.getString("type");
        value = bundle.getString("val");


        if ("name".equals(type)) {
            setViewsForCity(bundle);
        }


        findViewById();

        inflateToolbar();


    }

    private void setViewsForCity(Bundle bundle) {
        city = bundle.getString("city");
        cityId = bundle.getString("city_id");
        cityTV = (TextView) findViewById(R.id.tv_user_city);
        cityTV.setVisibility(View.VISIBLE);
        cityTV.setText(city);
        cityTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCityClicked();

            }
        });
        setUpCityList();

        findViewById(R.id.view_city).setVisibility(View.VISIBLE);
        findViewById(R.id.tv_user_city_label).setVisibility(View.VISIBLE);

    }

    private void setUpCityList() {
        cityList = (ArrayList<CityClass>) CityResponse.getCityList();
        cityNames.clear();
        for (CityClass city : cityList) {
            cityNames.add(city.getCity_name());
            cityVsId.put(city.getCity_name(), city.get_id());
        }
    }

    private void onCityClicked() {
        //Analytics.trackScreen(R.string.edit_city);
        if (!isCitiesFeteched) {
            getCityLIst();
        } else {
            showCityDialog();
        }
    }

    private void showCityDialog() {
        ListDialog d = new ListDialog(cityNames, EditProfile.this, city, "Select your location", new LocationSelectionListener() {
            @Override
            public void onResponse(Boolean status, String response) {
                isCityChanged = true;
                cityId = cityVsId.get(response);
                city = response;
                cityTV.setText(city);
                validateFields();
            }
        },true);
        d.show(getSupportFragmentManager(), "city");
    }

    private ProgressDialog dialog;

    private void showProgressDialog() {
        if (dialog == null || !dialog.isShowing()) {
            dialog = new ProgressDialog(this);
            dialog.setMessage("Fetching Cities");
            dialog.show();
        }
    }

    private void hideProgressDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }


    private void getCityLIst() {
        showProgressDialog();
        HashMap<String, String> params = new HashMap<String, String>();
        params = Utility.getInitialParams(PlaBroApi.RT.GET_CITY_DROPDOWN, params);

        AppVolley.processRequest(Constants.TASK_CODES.GET_CITY_DROPDOWN, CityResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
                    @Override
                    public void onSuccessResponse(PBResponse response, int taskCode) {
                        hideProgressDialog();
                        setUpCityList();
                        showCityDialog();
                    }

                    @Override
                    public void onFailureResponse(PBResponse response, int taskCode) {
                        hideProgressDialog();
                        showCityDialog();
                    }
                }
        );
    }


    private void updateProfile() {

        final ProgressDialog progress;
        progress = ProgressDialog.show(EditProfile.this, "Please wait",
                "Updating Profile", true);


        String fieldVal = mEditField.getText().toString();

        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        wrData.put("type", type);
        Analytics.trackProfileEvent(Analytics.Actions.MODIFIED, wrData);

        HashMap<String, String> params = new HashMap<String, String>();

        switch (type) {
            case "email":
                params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_EMAIL, fieldVal);
                break;
            case "web":
                params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_WEBSITE, fieldVal);
                break;
            case "addr":
                params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_ADDRESS, fieldVal);
                break;
            case "name":
                params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_NAME, fieldVal);
                if (!TextUtils.isEmpty(city) && isCityChanged) {
                    params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_CITY_NAME, city);
                    params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_CITY_ID, cityId);
                }
                break;
            case "status":
                params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_STATUS_MSG, fieldVal);
                break;
            case "business":
                params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_BUSINESS_NAME, fieldVal);
                break;

            default:
                break;
        }
        params = Utility.getInitialParams(PlaBroApi.RT.SETPREF, params);
        Util.hideKeyboard(EditProfile.this);

        AppVolley.processRequest(Constants.TASK_CODES.SET_PREFS, RegisterResponse.class, null, PlaBroApi.getBaseUrl(EditProfile.this), params, RequestMethod.POST, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                if (response != null) {


                    RegisterResponse registerResponse = (RegisterResponse) response.getResponse();

                    if (registerResponse.isStatus()) {

                        Toast.makeText(EditProfile.this, "Profile updated successfully.", Toast.LENGTH_LONG).show();
                        boolean ftUser = PBPreferences.getFirstTimeState();
                        Utility.dismissProgress(progress);
                        setResult(RESULT_OK);
                        if (isCityChanged) {
                            PBPreferences.saveData(PBPreferences.IS_CITY_UPDATE, true);
                        }
                        finish();
                        Log.i("PlaBro", "Preferences set sussessfully" + registerResponse.isStatus());

                    } else {
                        Log.i("PlaBro", registerResponse.getMsg());
                        Toast.makeText(EditProfile.this, "Preferences cant be set right now. Please try again.", Toast.LENGTH_LONG).show();
                        Utility.dismissProgress(progress);


                        if (registerResponse.getMsg().equalsIgnoreCase("Session Expired") || registerResponse.getMsg().equalsIgnoreCase("Missing AuthKey in the request")) {
                            showSessionExpiredDialog(false);

                        }

                    }

                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

                Utility.dismissProgress(progress);
                onApiFailure(response, taskCode);
                Log.i("PlaBro", "onFailure : Oops...something went wrong");
                Toast.makeText(EditProfile.this, response.getCustomException().getMessage(), Toast.LENGTH_LONG).show();


            }
        });


    }


    @Override
    public void onOkClicked(boolean addMore) {
        super.onOkClicked(addMore);
        updateProfile();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        menu_btn_enable = menu.getItem(0);
        menu_btn_disabled = menu.getItem(1);
        return true;
    }


    @Override
    public void findViewById() {

        mActivity = EditProfile.this;

        mEditField = (FloatingEditText) findViewById(R.id.et_field);

        mEditField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                validateFields();


            }
        });


        switch (type) {
            case "email":
                mEditField.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                mEditField.setSingleLine();
                mEditField.setHint(getResources().getString(R.string.email_hint));

                break;
            case "web":
                mEditField.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
                mEditField.setSingleLine();
                mEditField.setHint(getResources().getString(R.string.web_hint));


                break;
            case "addr":
                mEditField.setInputType(InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS);
                mEditField.setHint(getResources().getString(R.string.addr_hint));

                break;

            case "name":
                mEditField.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
                mEditField.setSingleLine();
                mEditField.setHint(getResources().getString(R.string.name_hint));


                break;

            case "status":
                mEditField.setInputType(InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE);
                mEditField.setHint(getResources().getString(R.string.status_hint));

                break;

            case "business":
                mEditField.setInputType(InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE);
                mEditField.setHint(getResources().getString(R.string.business_hint));
                break;

            default:
                break;
        }


        if (!TextUtils.isEmpty(value)) {
            mEditField.setText(value);
            mEditField.setSelection(value.length());
        }
    }

    private void validateFields() {

        switch (type) {
            case "email":
                validate_email(mEditField);
                break;
            case "web":
                validateWebsite(mEditField);
                break;
            case "addr":
                validate_address(mEditField);
                break;

            case "name":
                validat_name(mEditField);
                break;

            case "status":
                validat_status(mEditField);
                break;

            case "business":
                validat_business(mEditField);
                break;
        }


    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void inflateToolbar() {
        //setting action bar
        toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        // Set an OnMenuItemClickListener to handle menu item clicks
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Handle the menu item
                int id = item.getItemId();

                switch (id) {

                    case R.id.action_next_enable:

                        if(type.equalsIgnoreCase("name")){
                        }
                        //update profile
                        updateProfile();

                        return true;
                }
                return true;
            }

        });

        // Inflate a menu to be displayed in the toolbar
        toolbar.inflateMenu(R.menu.menu_profile);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        switch (type) {
            case "email":
                toolbar.setSubtitle("Change email");
                break;
            case "web":
                toolbar.setSubtitle("Change website");


                break;
            case "addr":
                toolbar.setSubtitle("Change office address");

                break;

            case "name":
                toolbar.setSubtitle("Change user name & city");

                break;

            case "status":
                toolbar.setSubtitle("Change status");
                break;

            case "business":
                toolbar.setSubtitle("Change business name");

                break;

            default:
                break;
        }
    }

    void checkAndEnableNext(boolean enable) {
        if (enable) {
            if (menu_btn_disabled != null && menu_btn_enable != null) {
                menu_btn_disabled.setVisible(false);
                menu_btn_enable.setVisible(true);
            }

        } else {
            if (menu_btn_disabled != null && menu_btn_enable != null) {
                menu_btn_disabled.setVisible(true);
                menu_btn_enable.setVisible(false);
            }
        }


    }


    public void validate_email(FloatingEditText edt) {
        if (edt.getText().toString() == null) {
            edt.setValidateResult(false, "Please enter email.");
            checkAndEnableNext(false);

        } else if (isEmailValid(edt.getText().toString()) == false) {
            edt.setValidateResult(false, "Invalid Email Address");
            checkAndEnableNext(false);

        } else {
            edt.setValidateResult(true, "Valid");
            checkAndEnableNext(true);
        }
    } // email validation


    public void validateWebsite(FloatingEditText edt) {
        if (edt.getText().toString() == null) {
            edt.setValidateResult(false, "Invalid Web Address");
            checkAndEnableNext(false);

        } else if (isWebValid(edt.getText().toString()) == false) {
            edt.setValidateResult(false, "Invalid Web Address");
            checkAndEnableNext(false);

        } else {
            edt.setValidateResult(true, "Invalid Web Address");
            checkAndEnableNext(true);
        }
    }

    public void validat_name(FloatingEditText edt)
            throws NumberFormatException {
        if (edt.getText().toString().length() == 0 || edt.getText().toString().length() > 256) {
            edt.setValidateResult(false, "Name length should be between 0 and 256 character.");
            checkAndEnableNext(false);

        }
//        else if (!edt.getText().toString().matches("[a-zA-Z ]+")) {
//            edt.setValidateResult(false, "Accept Alphabets Only.");
//            checkAndEnableNext(false);
//
//        }
        else {
            edt.setValidateResult(true, "true");
            checkAndEnableNext(true);

        }

    }

    public void validat_status(FloatingEditText edt) {

        if (edt.getText().toString().trim().equalsIgnoreCase("") || edt.getText().toString().length() > 256) {
            edt.setValidateResult(false, "Status length should be between 0 and 256 character.");
            checkAndEnableNext(false);


        } else {
            edt.setValidateResult(true, "true");
            checkAndEnableNext(true);


        }

    }

    public void validat_business(FloatingEditText edt) {
        if (edt.getText().toString().trim().equalsIgnoreCase("") || edt.getText().toString().length() > 256) {

            edt.setValidateResult(false, "Business name length should be between 0 and 256 character.");
            checkAndEnableNext(false);


        } else {
            edt.setValidateResult(true, "true");
            checkAndEnableNext(true);


        }


    }

    public void validate_address(FloatingEditText edt)
            throws NumberFormatException {
        if (edt.getText().toString().trim().equalsIgnoreCase("")) {
            edt.setValidateResult(false, "Please enter something");

            checkAndEnableNext(false);

        } else if (edt.getText().toString().contains("'")
                || edt.getText().toString().contains("\"")) {
            edt.setValidateResult(false, "Quotes are not acceptible");
            checkAndEnableNext(false);


        } else {
            edt.setValidateResult(true, "true");
            checkAndEnableNext(true);


        }

    }

    public static boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isWebValid(CharSequence email) {
        return Patterns.WEB_URL.matcher(email).matches();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }
}