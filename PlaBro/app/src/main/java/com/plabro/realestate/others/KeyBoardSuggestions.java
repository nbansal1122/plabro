package com.plabro.realestate.others;

import android.content.Context;
import android.provider.UserDictionary;
import android.text.TextUtils;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.keyboardSuggestions.KeyBoardSuggestionResponse;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.HashMap;


public class KeyBoardSuggestions {

    private static Context context;
    private static String TAG = "KeyBoardSuggestions";

    public static void initialize(Context ctx) {
        context = ctx;
        boolean timestampFlag = PBPreferences.getKeyboardUpdateTimestamp();

        if (timestampFlag) {
            saveKeyWordsInBackground();
        }
    }

    private static void saveKeyWordsInBackground() {
        saveKeyBoardSuggestion();
    }

    private static void saveKeyBoardSuggestion() {

        HashMap<String, String> params = new HashMap<String, String>();

        params = Utility.getInitialParams(PlaBroApi.RT.KEYBOARDSUGGESTIONS, params);
        if (TextUtils.isEmpty(params.get(PlaBroApi.AUTH_KEY))) {
            return;
        }
        AppVolley.processRequest(0, KeyBoardSuggestionResponse.class, null, String.format(PlaBroApi.getBaseUrl(context)), params, RequestMethod.GET, new VolleyListener() {

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Log.v(TAG, response.toString());
            }

            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                try {

                    Log.v(TAG, response.toString());
                    PBPreferences.setKeyboardUpdateTimestamp(false);
                    KeyBoardSuggestionResponse keyBoardSuggestionResponse = (KeyBoardSuggestionResponse) response.getResponse();
                    if (keyBoardSuggestionResponse.getStatus()) {
                        updateDictionary(keyBoardSuggestionResponse);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });


    }

    private static void updateDictionary(final KeyBoardSuggestionResponse keyBoardSuggestionResponse) {


        Thread.UncaughtExceptionHandler h = new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread th, Throwable ex) {
                System.out.println("Uncaught exception: " + ex);
            }
        };
        //insert the word to the User Dictionary
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (String val : keyBoardSuggestionResponse.getOutput_params().getData()) {
                        deleteUserDictionary(val);
                        UserDictionary.Words.addWord(context, val, 100, UserDictionary.Words.LOCALE_TYPE_ALL);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        t.setUncaughtExceptionHandler(h);
        t.start();


    }

    public static void deleteUserDictionary(String word) {
        try {

            context.getContentResolver().delete(UserDictionary.Words.CONTENT_URI,
                    UserDictionary.Words.WORD + "=?", new String[]{word});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
