package com.plabro.realestate.models.Calls;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by nitin on 30/06/15.
 */
@Table(name = "CallInfo" , id = BaseColumns._ID)
public class CallInfo extends Model {

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Column

    private String phoneNumber;
    @Column
    private long timestamp;

}
