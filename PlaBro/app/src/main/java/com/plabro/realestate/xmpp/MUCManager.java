package com.plabro.realestate.xmpp;

import com.plabro.realestate.CustomLogger.Log;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.Form;
import org.jivesoftware.smackx.muc.InvitationRejectionListener;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.ParticipantStatusListener;

/**
 * Created by jmd on 6/10/2015.
 */
public class MUCManager {
    private static final String TAG = "MUCManager";
    private Connection connection;
    private static MUCManager manager;

    public static MUCManager getInstance(Connection connection) {
        if (manager == null) {
            manager = new MUCManager();
        }
        manager.connection = connection;

        manager.addPacketListener();
        return manager;
    }

    public MultiUserChat createGroupChat(String grpChatName, String mNickName) throws XMPPException {

        MultiUserChat mMultiUserChat = new MultiUserChat(connection, grpChatName);
        mMultiUserChat.create(mNickName);
        mMultiUserChat.sendConfigurationForm(new Form(Form.TYPE_SUBMIT));
        mMultiUserChat.join(mNickName);
        mMultiUserChat.addInvitationRejectionListener(new InvitationRejectionListener() {
            public void invitationDeclined(String invitee, String reason) {
                // Do whatever you need here...
                Log.d(TAG, "Invitation Rejected");
            }
        });

        mMultiUserChat.addParticipantListener(new PacketListener() {
            @Override
            public void processPacket(Packet packet) {
                Log.d(TAG, "AddParticipant Listener");
            }
        });
        mMultiUserChat.addParticipantStatusListener(new ParticipantStatusListener() {
            @Override
            public void joined(String s) {
                Log.d(TAG, s + ": Joined");
            }

            @Override
            public void left(String s) {
                Log.d(TAG,"left "+ s);
            }

            @Override
            public void kicked(String s, String s1, String s2) {
                Log.d(TAG, s + ": kicked");
            }

            @Override
            public void voiceGranted(String s) {
                Log.d(TAG, s + ": voice Granted");
            }

            @Override
            public void voiceRevoked(String s) {
                Log.d(TAG, s + ": voice revoked");
            }

            @Override
            public void banned(String s, String s1, String s2) {
                Log.d(TAG, s + ": Banned");
            }

            @Override
            public void membershipGranted(String s) {
                Log.d(TAG, s + ": membership granted");
            }

            @Override
            public void membershipRevoked(String s) {
                Log.d(TAG, s + ": membership revoked");
            }

            @Override
            public void moderatorGranted(String s) {
                Log.d(TAG, s + ": moderator granted");
            }

            @Override
            public void moderatorRevoked(String s) {
                Log.d(TAG, s + ": moderator revoked");
            }

            @Override
            public void ownershipGranted(String s) {
                Log.d(TAG, s + ": ownership granted");
            }

            @Override
            public void ownershipRevoked(String s) {
                Log.d(TAG, s + ": ownership revoked");
            }

            @Override
            public void adminGranted(String s) {
                Log.d(TAG, s + ": admin granted");
            }

            @Override
            public void adminRevoked(String s) {
                Log.d(TAG, s + ": admin granted");
            }

            @Override
            public void nicknameChanged(String s, String s1) {
                Log.d(TAG, s + ":"+s1+": nickname changed");
            }
        });
        return mMultiUserChat;
    }

    public void sendMessage(String to, String msgText, MultiUserChat multiUserChat) throws XMPPException {
        Message msg = new Message(to, Message.Type.groupchat);
        msg.setBody(msgText);
        multiUserChat.sendMessage(msg);
    }

    private void addPacketListener() {
        PacketFilter filter = new MessageTypeFilter(Message.Type.groupchat);
        connection.addPacketListener(new PacketListener() {
            @Override
            public void processPacket(Packet packet) {
                Log.d(TAG, "Processing MUC message");
                Message message = (Message) packet;
                if (message.getBody() != null) {
                    String from = message.getFrom();
                    String Body = message.getBody();
                    // Add incoming message to the list view or similar
                }
            }
        }, filter);
    }

    public void sendInvitation(MultiUserChat muc, String userJID, String grpName) {
        muc.invite(userJID, grpName);
    }

}
