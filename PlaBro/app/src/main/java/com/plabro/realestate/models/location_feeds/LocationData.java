package com.plabro.realestate.models.location_feeds;

import com.plabro.realestate.models.propFeeds.Feeds;

import java.util.ArrayList;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class LocationData {

    private double _id;
    private int count;
    private ArrayList<Feeds> shouts;
    private double[] coordinates = new double[2];

    public double get_id() {
        return _id;
    }

    public int getCount() {
        return count;
    }

    public ArrayList<Feeds> getShouts() {
        return shouts;
    }

    public double[] getCoordinates() {
        return coordinates;
    }
}
