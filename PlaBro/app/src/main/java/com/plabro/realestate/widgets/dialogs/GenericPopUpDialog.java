package com.plabro.realestate.widgets.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.plabro.realestate.R;

/**
 * Created by Hemant on 19-01-2015.
 */
public class GenericPopUpDialog extends DialogFragment {

    private String mText;
    private Button mClose;
    private Context mContext;
    private Activity mActivity;
    private TextView mFeedView;


    @SuppressLint("ValidFragment")
    public GenericPopUpDialog(Context context, Activity mActivity,String text) {
        this.mContext = context;
        this.mText = text;
        this.mActivity = mActivity;

    }

    public GenericPopUpDialog() {

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.feeds_pop_up_dialog, null);
        builder.setView(convertView);
        setCancelable(true);
        if (null == mText) {
            dismiss();
            return null;
        }
        mClose = (Button) convertView.findViewById(R.id.close);
        mFeedView = (TextView) convertView.findViewById(R.id.post_content_full);
        mFeedView.setText(mText);
        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return builder.create();
    }


}
