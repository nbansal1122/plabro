package com.plabro.realestate.holders;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.activity.AuctionDetails;
import com.plabro.realestate.activity.BidParticipants;
import com.plabro.realestate.holders.BaseFeedHolder;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.BiddingFeed;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.models.propFeeds.listings.ListingFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.Util;

import java.util.HashMap;

/**
 * Created by nitin on 25/01/16.
 */
public class AuctionFeedHolder extends BaseFeedHolder {
    private TextView title, bidInfo, desc, button, time, reqSaleTv, bidParticipants, callTv, shareTv;
    private ImageView auctionImage;

    public AuctionFeedHolder(View itemView) {
        super(itemView);
        title = findTV(R.id.tv_auction_title);
        desc = findTV(R.id.tv_auction_text);
        bidInfo = findTV(R.id.tv_auction_subtitle);
        button = findTV(R.id.tv_auction_details);
        time = findTV(R.id.tv_auction_time);
        reqSaleTv = findTV(R.id.tv_req_sale);
        bidParticipants = findTV(R.id.tv_participants);
        auctionImage = (ImageView) findView(R.id.iv_auction);
        callTv = findTV(R.id.tv_call);
        shareTv = findTV(R.id.tv_auction_share);
    }

    @Override
    public void onBindViewHolder(final int position, BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        if (feed instanceof BiddingFeed) {
            final BiddingFeed f = (BiddingFeed) feed;
            title.setText(f.getTitle());
            button.setText(f.getButton_title());
            time.setText(f.getTime());
            bidInfo.setText(f.getSugg_info());

            String pTypeText = "";
            if (f.getAvail_req() != null && f.getAvail_req().length > 0) {
                pTypeText = f.getAvail_req()[0];
            }
            if (f.getSale_rent() != null && f.getSale_rent().length > 0) {
                pTypeText = pTypeText + " for " + f.getSale_rent()[0];
            }
            reqSaleTv.setText(pTypeText);
            bidParticipants.setVisibility(View.VISIBLE);
            if (f.getTotal_bid_count() > 0) {
                bidParticipants.setText(String.valueOf(f.getTotal_bid_count()));
//                bidParticipants.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Bundle b = new Bundle();
//                        b.putString(Constants.BUNDLE_KEYS.BID_ID, f.getAuction_id() + "");
//                        Intent i = new Intent(ctx, BidParticipants.class);
//                        i.putExtras(b);
//                        ctx.startActivity(i);
//                    }
//                });
            } else {
                bidParticipants.setVisibility(View.GONE);
            }


            setFeedText(f, position);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    trackEvent(Analytics.CardActions.CardClicked, position, f.get_id(), f.getType(), null);
                    onCardClick(f);
                }
            });

            shareTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PBSharingManager.shareByAll(f, ctx);
                }
            });
            ActivityUtils.loadImage(ctx, f.getImg_url(), auctionImage, 0);

            if (TextUtils.isEmpty(f.getProfile_phone())) {
                callTv.setVisibility(View.GONE);
            } else {
                callTv.setVisibility(View.VISIBLE);
                callTv.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Feeds feeds = (Feeds) f;
                                String phone = feeds.getProfile_phone();

                                HashMap<String, Object> wrData = new HashMap<String, Object>();
                                wrData.put("ScreenName", TAG);
                                wrData.put("Action", "Call");
                                wrData.put("callFrom", PBPreferences.getPhone());
                                wrData.put("callTo", feeds.getProfile_phone() + "");
                                wrData.put(Analytics.Params.SHOUT_ID, feeds.get_id());
                                wrData.put(Analytics.Params.CARD_POSITION, position);
                                ActivityUtils.initiateCallDialog(ctx, feeds, feeds.getPhonemap(), feeds.getProfile_phone(), wrData, feeds.getIsFreeCall(), feeds.getProfile_img(), feeds.getProfile_name());

                            }
                        }
                );
            }


        }
    }

    private void onCardClick(BiddingFeed f) {
        Intent i = new Intent(ctx, AuctionDetails.class);
        Bundle b = new Bundle();
        b.putString(Constants.BUNDLE_KEYS.AUCTION_ID, f.get_id());
        b.putString(Constants.BUNDLE_KEYS.TITLE, f.getTitle());
        i.putExtras(b);
        ctx.startActivity(i);
    }


    private void setFeedText(BiddingFeed feed, int position) {
        String text = feed.getText();
        int start = 0;
        int end = 250;
        if (feed.getSnippet().size() > 0) {
            start = Integer.parseInt(feed.getSnippet().get(0));
            end = Integer.parseInt(feed.getSnippet().get(1)) + 1;
        }

        int occurance = text.length() - text.replace("\n", "").length();


        if (start >= 0 && end < text.length()) {
            try {
                text = text.substring(start, end);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //adding span for hashtags in post
        SpannableString hashTagText = ActivityUtils.setHashTags(feed, text, start, end, ctx, desc, getType(), position, false, getRefKey(), mActivity);//type and position send to track analytics

    }
}
