package com.plabro.realestate.volley;

import com.android.volley.VolleyError;
import com.plabro.realestate.listeners.VolleyListener;

/**
 * Created by nitin on 23/07/15.
 */
public class CustomException extends Exception {

    //private static final long serialVersionUID = 1997753363232807009L;

    private int exceptionType;

    public VolleyError getVolleyError() {
        return volleyError;
    }

    public void setVolleyError(VolleyError volleyError) {
        this.volleyError = volleyError;
    }

    public int getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(int exceptionType) {
        this.exceptionType = exceptionType;
    }

    private VolleyError volleyError;
    public CustomException() {

    }

    public CustomException(String message, int exceptionType) {
        super(message);
        this.exceptionType = exceptionType;

    }
    public CustomException(String message, VolleyError error) {
        super(message);
        this.exceptionType = VolleyListener.VOLLEY_EXCEPTION;
        this.volleyError = volleyError;
    }

    public CustomException(Throwable cause) {
        super(cause);
    }

    public CustomException(String message, Throwable cause) {
        super(message, cause);
    }


}