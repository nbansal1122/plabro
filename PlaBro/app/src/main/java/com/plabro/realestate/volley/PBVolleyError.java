package com.plabro.realestate.volley;

import com.android.volley.VolleyError;

/**
 * Created by nitin on 23/07/15.
 */
public class PBVolleyError extends VolleyError {
    int exceptionType;

    public int getExceptionType() {
        return exceptionType;
    }

    public VolleyError setExceptionType(int exceptionType) {
        this.exceptionType = exceptionType;
        return this;
    }
}
