package com.plabro.realestate.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.Shout;
import com.plabro.realestate.listeners.HidingScrollListener;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.receivers.PBLocalReceiver;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.PBConditionalDialogView;
import com.software.shell.fab.ActionButton;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nitin on 11/08/15.
 */
public abstract class CardListFragment extends MasterFragment implements PBConditionalDialogView.OnPBConditionListener {
    protected RecyclerView mRecyclerView;
    protected ProgressWheel mProgressWheel;
    protected LinearLayoutManager mLayoutManager;
    protected RelativeLayout noFeeds;
    protected RelativeLayout comingSoonLayout;
    protected ActionButton mShoutButton;
    protected ActionButton mScrollButton;
    protected SwipeRefreshLayout mSwipeRefreshLayout;
    protected RelativeLayout shout, scroll;
    protected ArrayList<BaseFeed> baseFeeds = new ArrayList<BaseFeed>();
    protected int page = 0;
    protected boolean isLoading = false;
    PBConditionalDialogView mConditionalView;
    Animation animZoomIn, animZoomOut;
    protected ToolTipRelativeLayout toolTipRelativeLayout;
    protected boolean comingSoon = false;
    protected ImageView mCMSArrow;

    public void setRecyclerViewLayoutManager() {
        int scrollPosition = 0;

        mLayoutManager = new LinearLayoutManager(getActivity());

        if (mRecyclerView.getLayoutManager() != null) {
//            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
//                    .findFirstCompletelyVisibleItemPosition();
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.scrollToPosition(scrollPosition);

    }


    @Override
    public String getTAG() {
        return "CardListFragment";
    }

    @Override
    protected void findViewById() {
        super.findViewById();
        page = 0;
        isLoading = false;

        rootView.setTag(TAG);

        //attaching on scroll floating button
        shout = (RelativeLayout) findView(R.id.rl_compose_shout);
        scroll = (RelativeLayout) findView(R.id.rl_scroll);

        mShoutButton = (ActionButton) shout.findViewById(R.id.rl_post_iv);
        mScrollButton = (ActionButton) scroll.findViewById(R.id.rl_scroll_btn);
        Utility.setupShoutActionButton(mShoutButton, getActivity());
        Utility.setupScrollActionButton(mScrollButton, getActivity());

        mShoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openShoutActivity();
            }
        });
        mScrollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayoutManager.scrollToPositionWithOffset(0, 0);
                mScrollButton.playHideAnimation();
                mScrollButton.setVisibility(View.GONE);
            }
        });


        //mFeedsFragmentView.findViewById(R.id.rl_bg_shout).setOnClickListener(MyOnClickListener);

        mRecyclerView = (RecyclerView) findView(R.id.recyclerView);
        setRecyclerViewLayoutManager();
        mRecyclerView.setAdapter(getAdapter());
        initProgressWheel();

        mCMSArrow = (ImageView) findView(R.id.iv_cms_arrow);
        mCMSArrow.setVisibility(View.GONE);
        noFeeds = (RelativeLayout) findView(R.id.noFeeds);
        setnoFeedsLayout();
        comingSoonLayout = (RelativeLayout) findView(R.id.rl_coming_soon);
        comingSoonLayout.setVisibility(View.GONE);
        noFeeds.setVisibility(View.GONE);
        mConditionalView = (PBConditionalDialogView) findView(R.id.pbConditionalView);
        mConditionalView.setOnPBConditionListener(this);

        mRecyclerView.setOnScrollListener(hidingScrollListener);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findView(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "GetData Refresh");
                        getData(false);

                    }
                }, Constants.GOOGLE_PULL_TO_REFRESH_TIMER);

            }
        });
        mRecyclerView.setVisibility(View.VISIBLE);
        getData(false);
//        } else {
//            ((ViewGroup) rootView.getParent()).removeView(rootView);
//        }


        //pull to refresh

        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    protected void setnoFeedsLayout(){
        if(getActivity() == null)return;
        TextView noFeedsText = (TextView) noFeeds.findViewById(R.id.tv_no_feeds);
        ImageView noFeedsImage = (ImageView) noFeeds.findViewById(R.id.iv_no_feeds);
        noFeedsText.setText(getNoFeedsText());
        noFeedsImage.setImageResource(getNoFeedDrawable());
    }
    protected String getNoFeedsText(){
        return getString(R.string.no_feeds);
    }
    protected int getNoFeedDrawable(){
        return R.drawable.ic_no_shout;
    }

    protected void newPostListener() {

    }

    protected HidingScrollListener hidingScrollListener = new HidingScrollListener(20) {
        @Override
        public void onHide(RecyclerView recyclerView, int dx, int dy) {
            Log.d(TAG, "ON Hide Hiding scroll listener");
            onHideRecycleView(recyclerView, dx, dy);
            mScrollButton.playHideAnimation();
            mScrollButton.setVisibility(View.GONE);
            mScrollButton.setAnimation(animFadeOut);
//            scroll.setAnimation(animFadeOut);

        }

        @Override
        public void onShow(RecyclerView recyclerView, int dx, int dy) {
            Log.d(TAG, "ON Show Hiding scroll listener");
            onShowRecycleView(recyclerView, dx, dy);
            mScrollButton.playShowAnimation();
            mScrollButton.setVisibility(View.VISIBLE);


        }

        @Override
        public void onScrolledFire(RecyclerView recyclerView, int dx, int dy) {

//            Log.d(TAG, "Scroll Fired");
            onScrollFire(recyclerView, dx, dy);
            if (mLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                //Its at top ..
                mScrollButton.playHideAnimation();
                mScrollButton.setVisibility(View.GONE);

            }
        }
    };

    @Override
    protected int getViewId() {
        return 0;
    }

    protected void openShoutActivity() {

        //WizRocket tracking

        Intent intent = new Intent(getActivity(), Shout.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.bottom_in_fast, R.anim.bottom_out_fast);

    }

    protected void onScrollFire(RecyclerView recyclerView, int dx, int dy) {

//                //WizRocket tracking
//                WizRocket.getInstance(getActivity()).recordEvent(getActivity().getResources().getString(R.string.e_scroll));
//                //GA Event Tracker
//                Analytics.trackEvent(getActivity(), getActivity().getResources().getString(R.string.consumption), getActivity().getResources().getString(R.string.e_scroll), getActivity().getResources().getString(R.string.s_my_shouts_lst));
        int totalItemCount = mLayoutManager.getItemCount();
        if (!isLoading && totalItemCount>0) {

            int visibleItemCount = mLayoutManager.getChildCount();

            int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();

            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                //reached bottom show loading
                //mProgressWheelLoadMore.setVisibility(View.VISIBLE);

            } else {
                // mProgressWheelLoadMore.setVisibility(View.GONE);

            }

            if ((visibleItemCount + pastVisibleItems) >= totalItemCount - Constants.SCROLL_ITEM_COUNT) {
                Log.d(TAG, "GetData Scroll");
                getData(true);
            }
        }

    }

    @Override
    protected void reloadRequest() {

        if (!Util.haveNetworkConnection(AppController.getInstance())) {

            if (baseFeeds.size() < 1) {
                showConditionalMessage(PBConditionalDialogView.ConditionalDialog.INTERNET_CONNECTION, true, false);
                mConditionalView.setVisibility(View.VISIBLE);
            } else {
//                Toast.makeText(mContext, "Seems there is no Internet Connection", Toast.LENGTH_LONG).show();

            }

            return;
        }

        // Request data from the network.
        Log.d(TAG, "GetData Reload Request");
        getData(false);
    }

    protected void stopSpinners() {

        mProgressWheel.stopSpinning();
        isLoading = false;
        mConditionalView.setVisibility(View.GONE);

        try {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 2000ms
                    mSwipeRefreshLayout.setRefreshing(false);

                }
            }, 2000);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    protected void showSpinners(boolean addMore) {
        noFeeds.setVisibility(View.GONE);
        isLoading = true;
        if (addMore) {
        } else {
            mSwipeRefreshLayout.setRefreshing(true);
            if (baseFeeds.size() < 1) {
                mSwipeRefreshLayout.setRefreshing(false);
                mProgressWheel.spin();
            }
        }
        //mSmoothProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        reloadRequest();
    }

    public void initProgressWheel() {
        mProgressWheel = (ProgressWheel) findView(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
    }

    public void onApiFailure(PBResponse response, int taskCode) {
        if (getActivity() == null) return;
        super.onApiFailure(response, taskCode);
        stopSpinners();


    }

    protected void showNoFeeds() {
        if (null != mRecyclerView && null != mRecyclerView.getAdapter() && mRecyclerView.getAdapter().getItemCount() > 0) {
            noFeeds.setVisibility(View.GONE);
            comingSoonLayout.setVisibility(View.GONE);

        } else {
            if (comingSoon) {
                mCMSArrow.setVisibility(View.VISIBLE);
                comingSoonLayout.setVisibility(View.VISIBLE);
                noFeeds.setVisibility(View.GONE);

            } else {
                mCMSArrow.setVisibility(View.GONE);
                noFeeds.setVisibility(View.VISIBLE);
                comingSoonLayout.setVisibility(View.GONE);

            }
        }
    }

    @Override
    public void onStatusFalse(PBResponse response, int taskCode) {
        super.onStatusFalse(response, taskCode);
        showNoFeeds();
    }

    @Override
    public void onTrialVersionExpired(PBResponse response, int taskCode) {
        super.onTrialVersionExpired(response, taskCode);
        if (!(null != baseFeeds && baseFeeds.size() > 0)) {
            showRegistrationCard();
        }
    }

    @Override
    public void onInternetException(PBResponse response, int taskCode) {
        super.onInternetException(response, taskCode);
        if (baseFeeds.size() < 1) {
            mConditionalView.setType(PBConditionalDialogView.ConditionalDialog.INTERNET_CONNECTION);
            mConditionalView.setVisibility(View.VISIBLE);
        }
    }

    protected RecyclerView.Adapter getAdapter() {
        return null;
    }

    protected void onHideRecycleView(RecyclerView recyclerView, int dx, int dy) {
    }

    protected void onShowRecycleView(RecyclerView recyclerView, int dx, int dy) {

    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        if (Constants.ACTION_FEED_REFRESH.equals(intent.getAction())) {
            getData(false);
        }
    }
}
