package com.plabro.realestate.holders;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.plabro.realestate.holders.notification.NotificationHolderInterface;
import com.plabro.realestate.models.notifications.NotificationData;

/**
 * Created by nitin on 14/08/15.
 */

public abstract class BaseNotificationHolder extends RecyclerView.ViewHolder implements NotificationHolderInterface {
    protected Context ctx;
    protected String TAG;
    protected FragmentManager mFragmentManager;
    protected String type;
    protected int refKey;
    protected Activity mActivity;
    protected String cardType;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public int getRefKey() {
        return refKey;
    }

    public void setRefKey(int refKey) {
        this.refKey = refKey;
    }

    public FragmentManager getmFragmentManager() {
        return mFragmentManager;
    }

    public void setmFragmentManager(FragmentManager mFragementManager) {
        this.mFragmentManager = mFragementManager;
    }

    public String getTAG() {
        return TAG;
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }

    public BaseNotificationHolder(View itemView) {
        super(itemView);
        ctx = itemView.getContext();
        mActivity = (Activity)ctx;
    }


    @Override
    public void onBindViewHolder(int position, NotificationData feed) {

    }




}
