package com.plabro.realestate.uiHelpers.images;

/**
 * Created by Hemant on 6/5/15.
 */
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class PBViewPager extends ViewPager {
    private boolean isPagingEnabled = true;

    public PBViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        isPagingEnabled = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent arg0) {
        if (isPagingEnabled) {
            return super.onTouchEvent(arg0);
        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent arg0) {
        if (isPagingEnabled && !ImageTouchImageView.isScaled())
            return super.onInterceptTouchEvent(arg0);
        return false;
    }

    public void setPagingEnabled(boolean isPagingEnabled) {
        this.isPagingEnabled = isPagingEnabled;
    }

}

