package com.plabro.realestate.listeners;

import com.plabro.realestate.CustomLogger.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 17/08/15.
 */
public class ShoutObserver {

    private static String TAG = "ShoutObserver";
    private static ShoutObserver shoutObserver = new ShoutObserver();
    private static List<PBObserver> list = new ArrayList<>();


    public static ShoutObserver getInstance() {
        return shoutObserver;
    }

    private ShoutObserver() {
    }

    public static interface PBObserver {
        public void onShoutUpdate();

        public void onShoutUpdateGen(String type);

    }

    public void addListener(PBObserver listener) {
        list.add(listener);
    }

    public void remove(PBObserver listener) {
        list.remove(listener);
    }

    public static void dispatchEvent() {
        Log.v(TAG, "inside shout dispatch");
        for (PBObserver o : list) {
            if (null != o) {
                        o.onShoutUpdate();
                }
            }
    }

    public static void dispatchGenEvent(String type) {
        Log.v(TAG, "inside shout dispatch");
        for (PBObserver o : list) {
            if (null != o) {
                o.onShoutUpdateGen(type);
               // break;
            }
        }
    }
}
