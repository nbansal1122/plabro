package com.plabro.realestate.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.adapter.NotificationsCustomAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.notifications.NotificationData;
import com.plabro.realestate.models.notifications.NotificationResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;

public class NotificationsFragment extends NotificationCardListFragment implements ShoutObserver.PBObserver {

    NotificationsCustomAdapter mAdapter;
    boolean addMoreGlobal = false;
    private static Toolbar mToolbar;
    private static SearchBoxCustom search;


    @Override
    public String getTAG() {
        return "NotificationsFragment";
    }

    public static Fragment getInstance(Bundle b) {
        NotificationsFragment f = new NotificationsFragment();
        f.setArguments(b);
        return f;
    }

    public static NotificationsFragment getInstance(Toolbar bar, SearchBoxCustom searchbox) {
        mToolbar = bar;
        search = searchbox;
        return new NotificationsFragment();
    }


    @Override
    protected void findViewById() {
        super.findViewById();
        Analytics.trackScreen(Analytics.OtherScreens.Notifications);
        mShoutButton.setVisibility(View.GONE);
    }


    @Override
    public NotificationsCustomAdapter getAdapter() {
        if (null == mAdapter) {
            mAdapter = new NotificationsCustomAdapter(baseFeeds, getActivity(), "Notifications");
        }
        return mAdapter;
    }

    @Override
    protected int getViewId() {
        return R.layout.notifications_fragment;
    }


    @Override
    protected void reloadRequest() {
        showSpinners(addMoreGlobal);
        getData(false);
    }


    @Override
    public void onHideRecycleView(RecyclerView recyclerView, int dx, int dy) {
        super.onHideRecycleView(recyclerView, dx, dy);

    }

    @Override
    public void onShowRecycleView(RecyclerView recyclerView, int dx, int dy) {
        super.onShowRecycleView(recyclerView, dx, dy);
    }

    @Override
    public void onScrollFire(RecyclerView recyclerView, int dx, int dy) {
        super.onScrollFire(recyclerView, dx, dy);
    }


    @Override
    public void getData(final boolean addMore) {
        isLoading = true;
        addMoreGlobal = addMore;
        if (addMoreGlobal && baseFeeds.size() > 0) {
            if (baseFeeds.get(baseFeeds.size() - 1).getType().equalsIgnoreCase(Constants.NOTIFICATION_TYPE.NO_MORE_DATA)) {
                return;
            }
        }

        showSpinners(addMore);
        HashMap<String, String> params = new HashMap<>();
        if (addMore) {
            params.put(Constants.START_IDX, "" + page);
        } else {
            params.put(Constants.START_IDX, "0");
        }

        params = Utility.getInitialParams(PlaBroApi.RT.GET_NOTIFICATIONS, params);

        AppVolley.processRequest(Constants.TASK_CODES.GET_NOTIFICATIONS, NotificationResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                stopSpinners();


                NotificationResponse notificationResponse = (NotificationResponse) response.getResponse();
                if (null != notificationResponse.getOutput_params()) {
                    noFeeds.setVisibility(View.GONE);
                    ArrayList<NotificationData> tempFeeds = (ArrayList<NotificationData>) notificationResponse.getOutput_params().getData();

                    Log.d(TAG, "Page:" + page + ", Size:" + tempFeeds.size() + ", PrevFeeds Size:" + baseFeeds.size());
                    if (tempFeeds.size() > 0) {

                        NotificationData lf = new NotificationData();
                        if (tempFeeds.size() < Util.getDefaultStartIdx()) {
                            lf.setType(Constants.NOTIFICATION_TYPE.NO_MORE_DATA);
                        } else {
                            lf.setType(Constants.NOTIFICATION_TYPE.PROGRESS_MORE);
                        }

                        if (addMore) {
                            baseFeeds.remove(baseFeeds.size() - 1);//for footer
                        } else {
                            page = 0;
                            baseFeeds.clear();
                        }
                        baseFeeds.addAll(tempFeeds);
                        baseFeeds.add(lf);
                        mAdapter.updateItems(tempFeeds, addMore);
//                        if (!TextUtils.isEmpty(mAdapter.getSearchText())) {
//                            mAdapter.filter(mAdapter.getSearchText());
//                            afterFilter();
//                        }
                        page = Util.getStartIndex(page);
                        //page scrolled for next set of data
                        mAdapter.notifyDataSetChanged();
                    } else {
                        if (addMore) {
                            NotificationData lf = new NotificationData();

                            baseFeeds.remove(baseFeeds.size() - 1);//for footer
                            lf.setType(Constants.NOTIFICATION_TYPE.NO_MORE_DATA);
                            baseFeeds.add(lf);
                            mAdapter.notifyDataSetChanged();
                        } else if (!addMore) {
                            baseFeeds.clear();
                            noFeeds.setVisibility(View.VISIBLE);
                            mAdapter.notifyDataSetChanged();
                            return;
                        }
                    }
                }
                showNoFeeds();
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                stopSpinners();
                onApiFailure(response, taskCode);

                try {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 2000ms
                            NotificationData lf = new NotificationData();
                            lf.setType(Constants.NOTIFICATION_TYPE.NO_MORE);

                            if (addMore) {
                                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                                baseFeeds.add(lf);
                                mAdapter.notifyDataSetChanged();
                            } else {
                                baseFeeds.clear();
                                noFeeds.setVisibility(View.VISIBLE);
                            }
                        }
                    }, 2000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Profile Action Received");
        if (Constants.ACTION_PROFILE_UPDATE.equals(intent.getAction())) {
            ((PlabroBaseActivity) getActivity()).setTitle("Notifications");
        }
        if (Constants.ACTION_DELETE_NOTIFICATION.equals(intent.getAction())) {
            int position = intent.getExtras().getInt(Constants.BUNDLE_KEYS.POSITION, -1);
            if (position != -1 && position < baseFeeds.size()) {
                baseFeeds.remove(position);
                mAdapter.notifyDataSetChanged();
                getData(false);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflateToolbar(menu);
    }

    private void inflateToolbar(Menu menu) {
        //mToolbar.inflateMenu(R.menu.menu_chat_list);
        Log.v(TAG, "toolbar inflated");

        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.action_search:
                        openSearch();
                        break;

                    default:
                        break;

                }
                return true;
            }

        });
    }

    public void openSearch() {
        search.clearSearchable(); // clear all the searchable history previously present

        search.revealFromMenuItem(R.id.action_search, getActivity());
        search.setMenuListener(new SearchBoxCustom.MenuListener() {

            @Override
            public void onMenuClick() {
                if (search.isShown()) {
                    search.hideCircularly(getActivity());
                }

            }

        });
        search.setSearchListener(new SearchBoxCustom.SearchListener() {


            @Override
            public void onSearchOpened() {
                // Use this to tint the screen
                if (mAdapter != null) {
                    mAdapter.filter("");
                    afterFilter();
                }

            }

            @Override
            public void onSearchClosed() {
                // Use this to un-tint the screen
                if (mAdapter != null) {
                    mAdapter.filter("");
                    afterFilter();
                }
            }

            @Override
            public void onSearchTermChanged(String searchTerm) {
                // React to the search term changing
                // Called after it has updated results
                if (mAdapter != null) {
                    mAdapter.filter(searchTerm);
                    afterFilter();
                }
            }


            @Override
            public void onSearch(String searchTerm) {

                if (mAdapter != null) {
                    mAdapter.filter(searchTerm);
                    afterFilter();
                }
            }

            @Override
            public void onSearchCleared() {
                if (mAdapter != null) {
                    mAdapter.filter("");
                    afterFilter();
                }
            }

        });

    }

    private void afterFilter() {
        if (mAdapter.getItemCount() > 0) {
            noFeeds.setVisibility(View.GONE);
        } else {
            noFeeds.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onShoutUpdate() {

    }

    @Override
    public void onShoutUpdateGen(String type) {
        Log.d(TAG, "OnShout load more");
        if (type.equalsIgnoreCase(Constants.LOAD_MORE.SHOUT)) {
            if (addMoreGlobal) {
                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                NotificationData lf = new NotificationData();
                lf.setType(Constants.NOTIFICATION_TYPE.PROGRESS_MORE);
                baseFeeds.add(lf);
                mAdapter.notifyDataSetChanged();
            }
            getData(addMoreGlobal);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShoutObserver.getInstance().addListener(this);
        registerLocalReceiver();
    }

    @Override
    public IntentFilter getFilter() {
        IntentFilter filter = super.getFilter();
        filter.addAction(Constants.ACTION_DELETE_NOTIFICATION);
        return filter;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ShoutObserver.getInstance().remove(this);
        unRegsiterLocalReceiver();
    }


}
