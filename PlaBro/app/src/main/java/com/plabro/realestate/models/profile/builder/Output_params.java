package com.plabro.realestate.models.profile.builder;

import com.google.gson.annotations.Expose;

public class Output_params {

    @Expose
    private BuilderProfile data;

    /**
     * @return The data
     */
    public BuilderProfile getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(BuilderProfile data) {
        this.data = data;
    }

}