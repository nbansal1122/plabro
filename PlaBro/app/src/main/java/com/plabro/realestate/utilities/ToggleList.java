package com.plabro.realestate.utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 19/11/15.
 */
public class ToggleList<T> {
    ArrayList<T> list;

    public ToggleList() {
        list = new ArrayList<>();
    }

    public void add(T obj) {
        list.add(obj);
    }

    public void add(List<T> objects) {
        this.list.addAll(objects);
    }

    public static interface onItemSelectionChanged{

    }

}
