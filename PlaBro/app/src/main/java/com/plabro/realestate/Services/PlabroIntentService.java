package com.plabro.realestate.Services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.crashlytics.android.Crashlytics;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.MainActivity;
import com.plabro.realestate.entities.MyDraftsDBModel;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.Calls.CallData;
import com.plabro.realestate.models.Endorsement;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.ServerResponse;
import com.plabro.realestate.models.Tracker.Campaign;
import com.plabro.realestate.models.Tracker.WifiData;
import com.plabro.realestate.models.contacts.NonPlabroContact;
import com.plabro.realestate.models.notifications.NotificationAction;
import com.plabro.realestate.models.payment.TransactionData;
import com.plabro.realestate.models.propFeeds.GreetingFeed;
import com.plabro.realestate.models.propFeeds.ShareFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class PlabroIntentService extends Service {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_TRACKER_ALARM = "com.plabro.realestate.Services.action.TrackerAlarm";
    private static final String ACTION_SEND_CHAT = "com.plabro.realestate.Services.action.chatAction";
    private static final String ACTION_CAMPAIGN_TRACKING = "com.plabro.realestate.action.CampignTracking";
    private static final String ACTION_CONNECTIVITY_CHANGE = "com.plabro.realestate.Services.action.connectivtiy";
    private static final String ACTION_NOTIFY_INVITED_USERS = "com.plabro.realestate.Services.action.notifyInvitedUsers";
    private static final String ACTION_SHARE_FEED = "com.plabro.realestate.Services.action.notifyInvitedUsers";
    private static final String ACTION_SHARE_CALL_LOGS = "com.plabro.realestate.Services.action.callLogs";
    private static final String ACTION_SEND_TRANSACTION = "com.plabro.realestate.Services.action.sendTransaction";
    private static final String ACTION_SEND_NOTIF_ACTION = "com.plabro.realestate.Services.action.sendNotifAction";
    private static final String ACTION_SEND_NOTIFY_INTERACTION = "com.plabro.realestate.Services.action.notifyInteraction";

    // TODO: Rename parameters
    private static final String LAST_KNOWN_LAT = "LAT";
    private static final String LAST_KNOWN_LONG = "LONG";
    private static final String TAG = PlabroIntentService.class.getSimpleName();
    private static final String ACTION_SHOW_NOTIFICATION = "actionNotificaion";
    private static final String ACTION_IMAGE_NOTIFICATION = "actionImageNotification";

    public static void sendTransaction(Context context) {
        Intent intent = new Intent(context, PlabroIntentService.class);
        intent.setAction(ACTION_SEND_TRANSACTION);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void senfNotificationActions(Context ctx) {
        Intent intent = new Intent(ctx, PlabroIntentService.class);
        intent.setAction(ACTION_SEND_NOTIF_ACTION);
        ctx.startService(intent);
    }

    public static void startForNotifyingInteraction(HashMap<String, Object> metaData) {
        Intent intent = new Intent(AppController.getInstance(), PlabroIntentService.class);
        Bundle b = new Bundle();
        b.putSerializable("metaData", metaData);
        intent.setAction(ACTION_SEND_NOTIFY_INTERACTION);
        intent.putExtras(b);
        AppController.getInstance().startService(intent);
    }

    // TODO: Customize helper method
    public static void startTrackerAction(Context context) {
        Intent intent = new Intent(context, PlabroIntentService.class);
        intent.setAction(ACTION_SEND_CHAT);
        context.startService(intent);
    }

    public static void startConnectivityChangeAction(Context context) {
        Log.d(TAG, "starting Connectivity Change Action");
        Intent intent = new Intent(context, PlabroIntentService.class);
        intent.setAction(ACTION_CONNECTIVITY_CHANGE);
        context.startService(intent);

        try {
            List<WifiData> data = new Select().from(WifiData.class).execute();
            if (data != null) {
                Log.d(TAG, "" + data.size());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startCampaignTracking(Context context, String data) {
        Intent intent = new Intent(context, PlabroIntentService.class);
        intent.setAction(ACTION_CAMPAIGN_TRACKING);
        intent.putExtra(PBPreferences.CAMPAIGN_URI, data);
        context.startService(intent);
    }

    public static void notifyInvitedUsers(Context context) {
        Intent intent = new Intent(context, PlabroIntentService.class);
        intent.setAction(ACTION_NOTIFY_INVITED_USERS);
        context.startService(intent);
    }

    public static void shareFeed(Context context) {
        Intent intent = new Intent(context, PlabroIntentService.class);
        intent.setAction(ACTION_SHARE_FEED);
        context.startService(intent);
    }

    public static void sendCallLogsToServer(Context context) {
        Intent intent = new Intent(context, PlabroIntentService.class);
        intent.setAction(ACTION_SHARE_CALL_LOGS);
        context.startService(intent);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "OnCReate");
        Log.d(TAG, Thread.currentThread().getName());

        //connManager.connectInBackground();

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        PlabroLocationManagerTwo.getInstance(this.getApplicationContext()).getLastKnownLocation();
        try {
            if (intent != null) {
                final String action = intent.getAction();
                if (ACTION_CONNECTIVITY_CHANGE.equals(action)) {
                    onConnectivityChange();
                    shareCallLogs();
                    sendAllTransactions();
                    sendNotifActions();
                }
                if (ACTION_TRACKER_ALARM.equals(action)) {
                    handleWifiTrackingAction();

                }
                if (ACTION_CAMPAIGN_TRACKING.equals(action)) {
                    handleCampignTracking(intent);
                }

                if (ACTION_SHARE_FEED.equals(action)) {
                    shareFeed();
                }
                if (ACTION_SHARE_CALL_LOGS.equals(action)) {
                    shareCallLogs();
                }
                if (ACTION_SEND_TRANSACTION.equals(action)) {
                    sendAllTransactions();
                }
                if (ACTION_SEND_NOTIF_ACTION.equals(action)) {
                    sendNotifActions();
                }
                if (ACTION_SEND_NOTIFY_INTERACTION.equals(action)) {
                    handleNotifyInteraction(intent);
                }
                if (ACTION_SHOW_NOTIFICATION.equals(action)) {
                    showNotification(this, (GreetingFeed) intent.getExtras().getSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE));
                }
                if (ACTION_IMAGE_NOTIFICATION.equals(action)) {
                    Bundle b = intent.getExtras();
                    String imageUrl = b.getString("img_url");
                    String subtitle = b.getString("msg");
                    String title = b.getString("title");
                    showPictureNotification(PlabroIntentService.this, imageUrl, b, title, subtitle);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return START_NOT_STICKY;
    }

    private void sendNotifActions() {
        final List<NotificationAction> actions = new Select().from(NotificationAction.class).execute();
        if (null != actions)
            for (NotificationAction ac : actions) {
                sendNotifAction(ac);
            }
    }

    private void sendNotifAction(final NotificationAction a) {
        ParamObject obj = new ParamObject();
        obj.setRequestMethod(RequestMethod.POST);
        HashMap<String, String> params = new HashMap<>();
        params.put("not_ids", a.getNotificationIdsString());
        params.put("action_type", a.getActionType());
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.NOTIFICATION_ACTIONS, params));
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                a.delete();
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

            }
        });
    }

    private void sendAllTransactions() {
        List<TransactionData> ts = new Select().from(TransactionData.class).execute();
        if (ts != null && ts.size() > 0) {
            for (TransactionData t : ts) {
                sendTransaction(t);
            }
        }
    }

    private void sendTransaction(final TransactionData t) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.PLABRO_RECHARGE_WALLET_AUTO_PARAM_AMOUNT, t.getAmount());
        params.put(PlaBroApi.PLABRO_RECHARGE_WALLET_AUTO_PARAM_ACTION, "credit");
        params.put(PlaBroApi.PLABRO_RECHARGE_WALLET_AUTO_PARAM_STATE, t.getStatus());
        params.put(PlaBroApi.PLABRO_RECHARGE_WALLET_AUTO_PARAM_PAYMENT_MODE, t.getPaymentMode());
        params.put(PlaBroApi.PLABRO_RECHARGE_WALLET_AUTO_PARAM_TXNID, t.getTxnId());
        params.put(PlaBroApi.PLABRO_RECHARGE_WALLET_AUTO_PARAM_RESPONSE, t.getResponse());
        params = Utility.getInitialParams(PlaBroApi.RT.PAYMENT, params);
        ParamObject obj = new ParamObject();
        obj.setRequestMethod(RequestMethod.POST);
        obj.setParams(params);
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "Deleting transaction object");
                t.delete();
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

            }
        });
    }

    private void handleNotifyInteraction(Intent intent) {
        Log.d(TAG, "Interaction Event");
        Bundle b = intent.getExtras();
        HashMap<String, String> params = new HashMap<String, String>();
        HashMap<String, Object> metaData = (HashMap<String, Object>) b.getSerializable("metaData");
        for (Map.Entry<String, Object> entry : metaData.entrySet()) {
            if (null != entry.getValue())
                params.put(entry.getKey(), String.valueOf(entry.getValue()));
        }
        params = Utility.getInitialParams(PlaBroApi.RT.NOTIFY_INTERACTION, params);
        ParamObject obj = new ParamObject();
        obj.setRequestMethod(RequestMethod.POST);
        obj.setParams(params);
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "Deleting transaction object");
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

            }
        });
    }

    private void shareCallLogs() {
        try {
            Log.d(TAG, "Sharing Call Logs");
            if (TextUtils.isEmpty(PBPreferences.getPhone())) return;
            sendPlabroCallLogs();
            List<CallData> calls = new Select().from(CallData.class).limit(20).execute();
            shareCallLogList(calls);
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    private void shareCallLogList(List<CallData> calls) {
        if (null != calls && calls.size() > 0) {
            JSONArray array = new JSONArray();
            for (CallData call : calls) {
                JSONObject dict = getJsonFromCall(call);
                if (null != dict) {
                    array.put(dict);
                }
            }
            Log.d(TAG, "Call Sync Data:" + array);
            if (array.length() > 0) {
                sendCallLogData(calls, array);
            }
        } else {
            Log.d(TAG, "Call data is empty");
        }
    }

    private void sendPlabroCallLogs() {
        try {
            List<CallData> calls = new Select().from(CallData.class).execute();
            List<CallData> plabroCalls = new ArrayList<>();
            for (CallData c : calls) {
                if (c.getPlabro_call()) {
                    plabroCalls.add(c);
                }
            }
            shareCallLogList(plabroCalls);
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    private void sendCallLogData(final List<CallData> calls, JSONArray array) {
        ParamObject paramObject = new ParamObject();
        paramObject.setClassType(ServerResponse.class);
        paramObject.setRequestMethod(RequestMethod.POST);
        HashMap<String, String> params = new HashMap<>();
        params.put("call_logs", array.toString());
        paramObject.setParams(Utility.getInitialParams(PlaBroApi.RT.SAVE_CALL_LOGS, params));
        AppVolley.processRequest(paramObject, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Log.d("Sending call logs", "On Success Response");
                try {
                    for (CallData call : calls) {
                        call.delete();
                    }
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Log.d("Sending call logs", "On Failure Response");
            }
        });
    }

    private JSONObject getJsonFromCall(CallData call) {
        JSONObject obj = new JSONObject();
        try {
            Log.d(TAG, "Caller is :" + call.getCaller());
//            obj.put("caller", call.getCaller());

            if (call.isFromProfile()) {
                obj.put("authorid", call.getAuthorid());
            } else {
                obj.put("shoutid", call.getShoutid());
            }
            obj.put("time", call.getCallTime());
            obj.put("duration", Long.parseLong(call.getDuration()));
            obj.put("plabro_call", call.getPlabro_call());
            obj.put("source_page", call.getSource_page());
            obj.put("call_type", call.getCall_type());
            if (TextUtils.isEmpty(call.getCall_type())) {
                if (true == call.getPlabro_call()) {
                    call.setCall_type("outgoing");
                } else {
                    String phnNumber = call.getCallee();
                    String myPhnNumber = PBPreferences.getPhone();
                    if (!TextUtils.isEmpty(myPhnNumber)) {
                        if (myPhnNumber.equals(phnNumber)) {

                        }
                    }
                }
            }
            if (!TextUtils.isEmpty(call.getCall_type())) {
                switch (call.getCall_type()) {
                    case "incoming":
                        obj.put("callee", PBPreferences.getPhone());
                        obj.put("caller", call.getCallee());
                        break;
                    case "outgoing":
                        obj.put("caller", PBPreferences.getPhone());
                        obj.put("callee", call.getCallee());
                        break;
                    case "missed":
                        obj.put("callee", PBPreferences.getPhone());
                        obj.put("caller", call.getCallee());
                        break;
                }
            }

            obj.put("isFreeCall", call.getIsFreeCall());
            Log.d(TAG, "Call Json:" + obj);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return obj;
    }

    private void shareFeed() {
        Log.d(TAG, "Sharing Feed");
        try {
            final ShareFeed f = new Select().from(ShareFeed.class).executeSingle();
            if (null != f) {
                ParamObject p = new ParamObject();
                HashMap<String, String> map = new HashMap<>();
                map.put(PlaBroApi.PLABRO_REPLY_POST_PARAM_MESSAGE_ID, f.getFeed_id());
                p.setParams(Utility.getInitialParams(PlaBroApi.RT.SET_SHARE, map));
                AppVolley.processRequest(p, new VolleyListener() {
                    @Override
                    public void onSuccessResponse(PBResponse response, int taskCode) {
                        Log.d(TAG, "OnSuccess Response Sharing Feed");
                        f.delete();
                    }

                    @Override
                    public void onFailureResponse(PBResponse response, int taskCode) {
                        Log.d(TAG, "OnFailure Response Sharing Feed");
                    }
                });
            } else {
                Log.d(TAG, "Sharing Feed is null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleCampignTracking(Intent intent) {
        String uri = intent.getStringExtra(PBPreferences.CAMPAIGN_URI);

        Log.d(TAG, "URI CAMPAIGN: " + uri);
        if (null != uri && !TextUtils.isEmpty(uri)) {
            PBPreferences.saveData(PBPreferences.CAMPAIGN_URI, uri);

            stopSelf();
        }
        sendAppVersion(uri);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void onConnectivityChange() {
        try {
            if (!PBPreferences.getData(PBPreferences.VERSION_QUERY_SENT, false)) {
                sendAppVersion(PBPreferences.getData(PBPreferences.CAMPAIGN_URI, null));
            }
            handleWifiTrackingAction();
            sendEndorsements();
            shareFeed();
            sendDrafts();
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

    }

    private void sendDrafts() {
        try {


            List<MyDraftsDBModel> drafts = new Select().from(MyDraftsDBModel.class).execute();
            for (MyDraftsDBModel df : drafts) {

                if (!df.isSending()) {
                    continue;
                }
                HashMap<String, String> params = new HashMap<String, String>();
                JSONObject obj = new JSONObject();

                try {
                    obj.put("text", df.getText());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String city_name = df.getCity_name();
                String city_id = df.getCity_id();
                final String message = obj.toString();
                params.put(PlaBroApi.PLABRO_SHOUT_POST_PARAM_MESSAGE, message);
                params.put(PlaBroApi.PLABRO_SHOUT_POST_PARAM_POST_TYPE, "Drafts");
                if (!TextUtils.isEmpty(city_name)) {
                    params.put(PlaBroApi.PLABRO_SHOUT_POST_PARAM_CITY_NAME, city_name);
                }
                if (!TextUtils.isEmpty(city_id)) {
                    params.put(PlaBroApi.PLABRO_SHOUT_POST_PARAM_CITY_ID, city_id);
                }

                startSendingDrafts(params, df.getDraftId() + 999);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void startSendingDrafts(HashMap<String, String> params, long taskCode) {

        ParamObject obj = new ParamObject();

        params = Utility.getInitialParams(PlaBroApi.RT.POSTSHOUTS, params);

        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        wrData.put("Type", "Drafts");
        Analytics.trackEventWithProperties(R.string.consumption, R.string.e_shout, wrData);

        obj.setParams(params);
        obj.setRequestMethod(RequestMethod.GET);
        obj.setTaskCode((int) taskCode);
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                String draftId = Long.valueOf(taskCode - 999) + "";
                new Delete().from(MyDraftsDBModel.class).where("draftId=?", draftId).execute();
                ShoutObserver.dispatchEvent();
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

            }
        });
    }

    private void sendInvite(JSONArray invitedArray, final List<NonPlabroContact> list) {
        ParamObject obj = new ParamObject();
        HashMap<String, String> params = new HashMap<>();
        params.put("invited_array", invitedArray.toString());
        params = Utility.getInitialParams(PlaBroApi.RT.INVITED_USERS, params);
        obj.setParams(params);
        obj.setRequestMethod(RequestMethod.POST);
        obj.setTaskCode(Constants.TASK_CODES.INVITE_USERS);
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                for (NonPlabroContact c : list) {
                    c.setIsNotifiedToServer(true);
                    c.save();
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

            }
        });
    }

    private void sendEndorsements() {
        Log.d(TAG, "Send Endorsements Test");
        try {
            List<Endorsement> endorsements = new Select().from(Endorsement.class).execute();
            if (null != endorsements && endorsements.size() > 0) {
                for (Endorsement e : endorsements) {
                    e.delete();
                    testEndorsement(e.getEndorsementKey(), e.getAuthorId());
                }
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    public void testEndorsement(final String endKey, final String contactId) {
        HashMap<String, String> params = new HashMap<>();

        params.put(PlaBroApi.PARAMS.ENDORSEMENT_KEY, endKey + "");
        params.put(PlaBroApi.PARAMS.CONTACT_ID, contactId + "");
        params = Utility.getInitialParams(PlaBroApi.RT.UPDATE_ENDORSEMENT, params);
        AppVolley.processRequest(0, null, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "Endorsements Successful");
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "Endorsement Failure");
                Endorsement e = new Endorsement();
                e.setAuthorId(contactId);
                e.setEndorsementKey(endKey);
                e.save();
            }
        });
    }

    private void handleWifiTrackingAction() {
        long currentTime = System.currentTimeMillis();
        if (currentTime > PBPreferences.getData(PBPreferences.TRACKER_TIME, 0L))
            new AsyncTask<String, String, String>() {
                @Override
                protected String doInBackground(String... objects) {
                    return createJsonFromWifiData();
                }

                @Override
                protected void onPostExecute(String json) {
                    if (!TextUtils.isEmpty(json)) {
                        sendDataToWifi(json);
                        startAlarm(PlabroIntentService.this);

                    } else {
                        Log.d(TAG, "WifiData created is Null");
                    }
                }
            }.execute();

    }

    private void startAlarm(Context ctx) {
        Log.i(TAG, "Setting Alarm");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        //c.add(Calendar.SECOND, 60); // for debug
        c.add(Calendar.MINUTE, 180); // for production

        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        PBPreferences.saveData(PBPreferences.TRACKER_TIME, c.getTimeInMillis());
        manager.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), getPendingIntent(ctx));
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "OnDestroy");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */


    private String createJsonFromWifiData() {
        try {

            Log.d(TAG, "Creating Json From Wifidata");
            List<WifiData> list = new Select().from(WifiData.class).limit(20).execute();
            if (list != null && list.size() > 0) {
                JSONArray trackerArray = new JSONArray();
                for (WifiData tracker : list) {
                    JSONObject trackerObj = new JSONObject();
                    trackerObj.put("distance", tracker.getDistance());
                    trackerObj.put("timestamp", tracker.getTimestamp());
                    trackerObj.put("lat", tracker.getLat());
                    trackerObj.put("lng", tracker.getLng());
                    trackerObj.put("ssid", tracker.getSsid());
                    trackerObj.put("bssid", tracker.getBssid());
                    trackerObj.put("level", tracker.getLevel());
                    trackerArray.put(trackerObj);
                }
                return trackerArray.toString();


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void sendDataToWifi(String wifiData) {
        Log.d(TAG, "Wifi:" + wifiData);
        ParamObject obj = new ParamObject();
        obj.setRequestMethod(RequestMethod.POST);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.PLABRO_WIFI_DATA_POST_PARAM_WIFIDATA, wifiData);
        params = Utility.getInitialParams(PlaBroApi.RT.WIFI_DATA, params);
        obj.setClassType(ServerResponse.class);
        obj.setParams(params);
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                if (response != null) {
                    //wifi data uploaded
                    Log.i(TAG, "WIFI data uploaded");
                    try {
                        new Delete().from(WifiData.class).execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

            }
        });

    }


    private PendingIntent getPendingIntent(Context ctx) {
        Intent alarmIntent = new Intent(ctx, PlabroIntentService.class);
        alarmIntent.setAction(ACTION_TRACKER_ALARM);
        PendingIntent pendingIntent = PendingIntent.getService(ctx, 0, alarmIntent, 0);
        return pendingIntent;
    }


    void sendAppVersion(String installUri) {
        PBPreferences.saveData(PBPreferences.VERSION_QUERY_SENT, false);
        Log.d(TAG, "Sending APP Version");
        //get app version number
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        int version = pInfo.versionCode;

        // Android version
        String androidVersion = android.os.Build.VERSION.RELEASE;

        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.GCM_REG_GET_PARAM_OSVERSION, androidVersion);
        params.put(PlaBroApi.PLABRO_UPDATEDAPPVERSION_POST_PARAM_APP_VERISON, version + "");
        if (installUri != null) {
            params.putAll(Campaign.parseUri(installUri));
        }
        params = Utility.getInitialParams(PlaBroApi.RT.UPDATEAPPVERSION, params);

        ParamObject obj = new ParamObject();
        obj.setParams(params);
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                PBPreferences.saveData(PBPreferences.VERSION_QUERY_SENT, true);
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

            }
        });
    }


    public static void showPushNotification(Context ctx, GreetingFeed feed) {
        Intent intent = new Intent(ctx, PlabroIntentService.class);
        intent.setAction(ACTION_SHOW_NOTIFICATION);
        Bundle b = new Bundle();
        b.putSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE, feed);
        intent.putExtras(b);
        ctx.startService(intent);
    }

    public static void showImageNotification(Context ctx, Bundle b) {
        Intent intent = new Intent(ctx, PlabroIntentService.class);
        intent.setAction(ACTION_IMAGE_NOTIFICATION);
        intent.putExtras(b);
        ctx.startService(intent);
    }

    private void showNotification(final Context ctx, final GreetingFeed feed) {
        try {
            Picasso.with(ctx).load(feed.getImg()).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    Log.d("show Picture", "on bitmap loaded");
                    showPictureNotification(ctx, feed, bitmap);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    Log.d("show Picture", "on bitmap failed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    Log.d("show Picture", "on prepare load");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showPictureNotification(final Context ctx, final GreetingFeed feed, final Bitmap bitmap) {
        try {
            Log.d("show Picture Notif", "got bitmap");
            Intent i = new Intent(ctx, MainActivity.class);
            Bundle b = new Bundle();
            b.putString("act", "gt");
            b.putString(Constants.BUNDLE_KEYS.SERVICE_CODE, feed.getService_code());
            b.putString(Constants.BUNDLE_KEYS.GREETING_MSG, feed.getMsg());
            i.putExtras(b);
            launchImageNotificationIntent(bitmap, feed.getTitle(), feed.getSubtitle(), i, ctx);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showPictureNotification(final Context ctx, final String imgUrl, final Bundle bundle, final String title, final String subtitle) {
        try {
            if (TextUtils.isEmpty(imgUrl)) {
                Intent i = new Intent(ctx, MainActivity.class);
                i.putExtras(bundle);
                launchImageNotificationIntent(null, title, subtitle, i, ctx);
                return;
            }
            Picasso.with(ctx).load(imgUrl).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    Log.d("show Picture", "on bitmap loaded");
                    Intent i = new Intent(ctx, MainActivity.class);
                    i.putExtras(bundle);
                    launchImageNotificationIntent(bitmap, title, subtitle, i, ctx);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    Log.d("show Picture", "on bitmap failed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    Log.d("show Picture", "on prepare load");
                }
            });
            Log.d("show Picture Notif", "got bitmap");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void launchImageNotificationIntent(Bitmap bitmap, String title, String subtitle, Intent i, Context ctx) {
        try {
            PendingIntent contentIntent = PendingIntent.getActivity(ctx, 100, i, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(AppController.getInstance())
                    .setContentTitle(title)
                    .setContentText(subtitle)
                    .setSmallIcon(R.drawable.notif).setContentIntent(contentIntent);

            if (null != bitmap) {
                if (android.os.Build.VERSION.SDK_INT < 16) {
                    Log.d("Do not show Picture Notif", "less than 16");
                } else {
                    Log.d("show Picture Notif", "> 16");
                    builder.setStyle(new NotificationCompat.BigPictureStyle()
                            .bigPicture(bitmap)
                            .setBigContentTitle(title)
                            .setSummaryText(subtitle));
                }
            }
            builder.setOngoing(false);
            builder.setAutoCancel(true);
            NotificationManager notificationManager = (NotificationManager) AppController.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(100, builder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
