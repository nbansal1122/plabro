package com.plabro.realestate.receivers;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.propFeeds.BaseFeed;

public class CallListener extends PhoneStateListener {
    private BaseFeed feed;
    private CallInterface listener;
    private boolean isCallConnected;
    private boolean isFreeCall;
    private static final String TAG = "Call Listener";

    public CallListener() {

    }

    public CallListener(BaseFeed feed, CallInterface listener, boolean isFreeCall) {
        this.feed = feed;
        this.listener = listener;
        this.isFreeCall = isFreeCall;
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        if (TelephonyManager.CALL_STATE_RINGING == state) {
            //Incoming call handling
            Log.d(TAG, "Call State Ringing, " + incomingNumber);
            isCallConnected = true;
            if (null != listener) {
                listener.onCallReceived(feed, isFreeCall);
            }
        }
        if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
            //Outgoing call handling
            Log.d(TAG, "Call State Offhook, " + incomingNumber);
            isCallConnected = true;
            if (null != listener) {
                listener.onCallConnected(this.feed);
            }
        }
        if (TelephonyManager.CALL_STATE_IDLE == state) {
            //Device back to normal state (not in a call)
            Log.d(TAG, "Call State Idle, " + incomingNumber);
            if (isCallConnected) {
                if (null != listener) {
                    listener.onCallEnded(this.feed, incomingNumber);
                }
                isCallConnected = false;
            }
        }
    }

    public static interface CallInterface {
        public void onCallConnected(BaseFeed feed);

        public void onCallReceived(BaseFeed feed, boolean isFreeCall);

        public void startListeningToCall(BaseFeed feed, boolean isFreeCall);

        public void onCallEnded(BaseFeed feed, String incomingNumber);
    }


}