package com.plabro.realestate.volley;

import java.util.Map;

/**
 * Created by nitin on 23/07/15.
 */
public class PBResponse {
    private Object response;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private Map<String, String> params;
    private CustomException customException = new CustomException();

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }


    public CustomException getCustomException() {
        return customException;
    }

    public void setCustomException(CustomException customException) {
        this.customException = customException;
    }
}
