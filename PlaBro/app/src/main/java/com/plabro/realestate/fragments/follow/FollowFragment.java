package com.plabro.realestate.fragments.follow;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.FollowersCustomAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.followers.FollowersResponse;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 18/02/16.
 */
public class FollowFragment extends PlabroBaseFragment {
    private Toolbar mToolbar;
    private ProgressWheel mProgressWheel;
    private RelativeLayout mNoFollowers;


    // Declare Variables
    ListView mFollowersList;
    FollowersCustomAdapter mAdapter;
    List<ProfileClass> followersList = new ArrayList<ProfileClass>();

    private String mAuthorId, mType;


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getActivity().getMenuInflater().inflate(R.menu.menu_followers, menu);
        sketchMenuItems(menu);

    }

    private void sketchMenuItems(Menu menu) {

        SearchView chatFilter = ((SearchView) menu.getItem(0).getActionView());
        chatFilter.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.filter(newText);
//                Toast.makeText(mContext, newText, Toast.LENGTH_SHORT).show();
                return true;
            }
        });

    }


    @Override
    public String getTAG() {
        return "Follow Fragment";
    }

    @Override
    public void findViewById() {
        if ("followers".equalsIgnoreCase(mType)) {
            Analytics.trackScreen(Analytics.UserProfile.Follower);
        } else {
            Analytics.trackScreen(Analytics.UserProfile.Following);
        }
        Bundle bundle = getActivity().getIntent().getExtras();

        mAuthorId = bundle.getString("authorid");
        mType = bundle.getString("type");

        setRefreshLayout();
        mProgressWheel = (ProgressWheel) findView(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mProgressWheel.spin();

        mNoFollowers = (RelativeLayout) findView(android.R.id.empty);

        // Locate the ListView in listview_main.xml
        mFollowersList = (ListView) findView(R.id.listview);

        inflateToolbar();
        getFollowers();

    }

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private void setRefreshLayout() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findView(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "GetData Refresh");
                        getFollowers();
                    }
                }, Constants.GOOGLE_PULL_TO_REFRESH_TIMER);

            }
        });
//        } else {
//            ((ViewGroup) rootView.getParent()).removeView(rootView);
//        }


        //pull to refresh

        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    @Override
    protected int getViewId() {
        return R.layout.activity_followers;
    }


    public void inflateToolbar() {
        //setting action bar
//        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

//
//        if (mType.equalsIgnoreCase("followers")) {
//            getSupportActionBar().setTitle("Followers");
//        } else {
//            getSupportActionBar().setTitle("Following");
//
//        }

//

        // Inflate a menu to be displayed in the toolbar
//        mToolbar.inflateMenu(R.menu.menu_followers);
//
//        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
//        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //What to do on back clicked
//                getActivity().finish();
//            }
//        });

    }

    void getFollowers() {

        HashMap<String, String> params = new HashMap<String, String>();
        if (!TextUtils.isEmpty(mAuthorId)) {
            params.put(PlaBroApi.PLABRO_GET_PROFILE_AUTO_PARAM_CONTACT_ID, mAuthorId);
        }
        if ("followers".equalsIgnoreCase(mType)) {
            params = Utility.getInitialParams(PlaBroApi.RT.FOLLOWER_LIST, params);
        } else {
            params = Utility.getInitialParams(PlaBroApi.RT.FOLLOWING_LIST, params);
        }

        AppVolley.processRequest(1, FollowersResponse.class, null, String.format(PlaBroApi.getBaseUrl()), params, RequestMethod.GET, new VolleyListener() {

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                if(getActivity() == null)return;
                stopSpinners();
                if (null == mAdapter || mAdapter.getCount() < 1) {
                    mNoFollowers.setVisibility(View.VISIBLE);
                } else {
                    mNoFollowers.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                if(getActivity() == null)return;
                stopSpinners();
                FollowersResponse profileResponse = (FollowersResponse) response.getResponse();
                if (profileResponse.isStatus()) {
                    followersList = profileResponse.getOutput_params().getData();
                    Log.d(TAG, followersList.size() + "");
//                    Collections.sort(followersList, new MyComparator());
                    mAdapter = new FollowersCustomAdapter(getActivity(), followersList);
                    // Binds the Adapter to the ListView
                    mFollowersList.setAdapter(mAdapter);
                }

                if (null == mAdapter || mAdapter.getCount() < 1) {
                    mNoFollowers.setVisibility(View.VISIBLE);
                } else {
                    mNoFollowers.setVisibility(View.GONE);
                }
            }


        });

    }

    private void stopSpinners(){
        mSwipeRefreshLayout.setRefreshing(false);
        if (mProgressWheel.isSpinning()) {
            mProgressWheel.stopSpinning();
        }
    }

    public static Fragment getInstance(Toolbar toolbar) {
        FollowFragment f = new FollowFragment();
        f.mToolbar = toolbar;
        return f;
    }
}
