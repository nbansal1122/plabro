package com.plabro.realestate.entities;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.plabro.realestate.models.propFeeds.BaseFeed;

import java.io.Serializable;

/**
 * Created by hemant on 18-Apr-15.
 */
@Table(name = "MyDrafts")
public class MyDraftsDBModel extends Model implements Serializable {

    @Column(name = BaseColumns._ID, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String _id;
    @Column(name = "draftId",unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private long draftId;
    @Column
    private String text;
    @Column
    private String avail_req;
    @Column
    private String sale_rent;
    @Column
    private String city_name;
    @Column
    private String city_id;
    @Column
    private String time;
    @Column
    private boolean sending;

    public long getDraftId() {
        return draftId;
    }

    public void setDraftId(long draftId) {
        this.draftId = draftId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAvail_req() {
        return avail_req;
    }

    public void setAvail_req(String avail_req) {
        this.avail_req = avail_req;
    }

    public String getSale_rent() {
        return sale_rent;
    }

    public void setSale_rent(String sale_rent) {
        this.sale_rent = sale_rent;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isSending() {
        return sending;
    }

    public void setSending(boolean sending) {
        this.sending = sending;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }
}
