package com.plabro.realestate.fragments.community;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.UserProfileNew;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.adapter.FeedsCustomAdapter;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.community.Admin;
import com.plabro.realestate.models.community.CreateCommunityResponse;
import com.plabro.realestate.models.community.JoinedComm;
import com.plabro.realestate.models.community.info.CommunityInfoResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.software.shell.fab.ActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nbansal2211 on 09/06/16.
 */
public class CommUserFragment extends PlabroBaseFragment implements VolleyListener, CustomListAdapter.CustomListAdapterInterface, AdapterView.OnItemClickListener {
    public static final String FEED_TYPE = "feed_type";
    private JoinedComm community;
    private CustomListAdapter<Admin> adapter;
    private Toolbar toolbar;
    private SearchBoxCustom searchBoxCustom;
    private String searchText;

    private List<Admin> communityUsers = new ArrayList<>();
    private ListView listView;

    @Override
    public String getTAG() {
        return "CommUserFragment";
    }

    public static Fragment getInstance(Bundle b, Toolbar toolbar, SearchBoxCustom searchBoxCustom) {
        CommUserFragment f = new CommUserFragment();
        f.setArguments(b);
        f.toolbar = toolbar;
        f.searchBoxCustom = searchBoxCustom;
        return f;
    }

    private void setAdadpter() {
        listView = (ListView) findView(R.id.listView);
        adapter = new CustomListAdapter<>(getActivity(), R.layout.row_related_brokers, communityUsers, this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

    }

    @Override
    protected void findViewById() {
        setAdadpter();
        getCommunityUsers();
    }

    private void createCommunityUsersList(CommunityInfoResponse response) {
        communityUsers.clear();
        List<Admin> admins = response.getOutput_params().getData().getAdmin();
        for (int i = 0; i < admins.size(); i++) {
            Admin a = admins.get(i);
            a.setAdmin(true);
            communityUsers.add(a);
        }
        List<Admin> activeUsers = response.getOutput_params().getData().getActiveUsers();
        for (int i = 0; i < activeUsers.size(); i++) {
            Admin a = activeUsers.get(i);
            a.setAdmin(false);
            communityUsers.add(a);
        }
        adapter.notifyDataSetChanged();
    }

    private void getCommunityUsers() {
        ParamObject obj = new ParamObject();
        obj.setClassType(CommunityInfoResponse.class);
        HashMap<String, String> map = new HashMap<>();
        map.put("groupid", community.getGroupid() + "");
        obj.setTaskCode(Constants.TASK_CODES.COMMUNITY_INFO);
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.COMMUNITY_INFO, map));

        AppVolley.processRequest(obj, this);
    }

    @Override
    protected void loadBundleData(Bundle b) {
        community = (JoinedComm) b.getSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE);
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_list;
    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        switch (taskCode) {
            case Constants.TASK_CODES.COMMUNITY_INFO:
                Log.d(TAG, response.getResponse().toString());
                createCommunityUsersList((CommunityInfoResponse) response.getResponse());
                break;
        }
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        switch (taskCode) {
            case Constants.TASK_CODES.COMMUNITY_INFO:
                Log.d(TAG, "On Failure" + response.getCustomException().getMessage());
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getActivity()).inflate(resourceID, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Admin a = communityUsers.get(position);
        holder.brokerName.setText(a.getName());
        ActivityUtils.loadImage(getActivity(), a.getImg(), holder.brokerImage);
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Admin a = communityUsers.get(position);
        UserProfileNew.startActivity(getActivity(), a.getAuthorid() + "", a.getPhone(), "");
    }

    class ViewHolder {
        private TextView brokerName;
        private ImageView brokerImage, callIcon;

        public ViewHolder(View itemView) {
            brokerName = (TextView) itemView.findViewById(R.id.tv_broker_name);
            brokerImage = (ImageView) itemView.findViewById(R.id.iv_image_broker);
            callIcon = (ImageView) itemView.findViewById(R.id.iv_call_icon);
            callIcon.setVisibility(View.GONE);
        }
    }
}
