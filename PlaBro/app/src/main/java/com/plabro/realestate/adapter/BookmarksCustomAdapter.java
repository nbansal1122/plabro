/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.plabro.realestate.adapter;

public class BookmarksCustomAdapter /*extends RecyclerView.Adapter<BookmarksCustomAdapter.ViewHolder>*/ {

   /* private static Context mContext;
    private static Activity mActivity;
    private int feedsCount = 0;
    private static final String TAG = "FeedsCustomAdapter";
    private  ArrayList<Feeds> feedsImg = new ArrayList<Feeds>();
    List<LocationsAuto> hashtags = new ArrayList<LocationsAuto>();
    static Drawable heart_red;
    static Drawable heart_grey;
    private static OnUpdateListener bookmarkDeleteListener;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final MyNetworkImageView iv_profile_image;
        private final ImageView iv_options, iv_bookmark;
        private final TextView tv_profile_name, tv_post_time, post_content, tv_post_id, user_phone;
        private final TextView anchor;
        private final CardView card_view;
        private final LinearLayout mMore;
        private final TextView postContentSummary;


        public ViewHolder(View v) {
            super(v);
            this.postContentSummary = (TextView) itemView.findViewById(R.id.post_content_title);

            this.tv_profile_name = (TextView) itemView.findViewById(R.id.profile_name);
            this.tv_post_time = (TextView) itemView.findViewById(R.id.post_time);
            this.post_content = (TextView) itemView.findViewById(R.id.post_content);
            this.iv_profile_image = (MyNetworkImageView) itemView.findViewById(R.id.profile_image);
            this.iv_options = (ImageView) itemView.findViewById(R.id.card_option);
            this.iv_bookmark = (ImageView) itemView.findViewById(R.id.bookmark);
            this.tv_post_id = (TextView) itemView.findViewById(R.id.post_id);
            this.user_phone = (TextView) itemView.findViewById(R.id.user_phone);
            this.anchor = (TextView) itemView.findViewById(R.id.popupMenyAnchor);
            this.card_view = (CardView) itemView.findViewById(R.id.card_view);
            this.mMore = (LinearLayout) itemView.findViewById(R.id.more);

            mMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Feeds feed = (Feeds) v.getTag();

                    FragmentManager mFragmentManager = mActivity.getFragmentManager();
                    DialogFragment feedPopUpDialog = new FeedPopUpDialog(feed, mContext, mActivity);
                    feedPopUpDialog.show(mFragmentManager, "feed");


                }
            });

            card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String id = ((Feeds) v.getTag()).get_id();

                    Log.d(TAG, "Element " + id + " clicked.");
                    //Toast.makeText(mContext, id, Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Element " + " clicked.");

                    Intent intent = new Intent(mContext, RelatedFeedsActivity.class);
                    intent.putExtra("message_id", id);

                    mContext.startActivity(intent);

                }
            });


            card_view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String text = ((Feeds) v.getTag()).getText();
                    ActivityUtils.copyFeed(text, mContext);
                    return true;
                }
            });

            post_content.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String text = ((Feeds) v.getTag()).getText();
                    ActivityUtils.copyFeed(text, mContext);
                    return true;
                }
            });


            post_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String id = ((Feeds) v.getTag()).get_id();

                    Log.d(TAG, "Element " + id + " clicked.");
                    //Toast.makeText(mContext, id, Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Element " + " clicked.");

                    Intent intent = new Intent(mContext, RelatedFeedsActivity.class);
                    intent.putExtra("message_id", id);

                    mContext.startActivity(intent);

                }
            });


            iv_options.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {

                                                  final Feeds feedObject = (Feeds) v.getTag();

                                                  PopupMenu popup = new PopupMenu(mContext, iv_options);
                                                  //Inflating the Popup using xml file
                                                  popup.getMenuInflater().inflate(R.menu.card_options_menu, popup.getMenu());


                                                  //registering popup with OnMenuItemClickListener
                                                  popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                                                                       public boolean onMenuItemClick(MenuItem item) {

                                                                                           String post_id = feedObject.get_id().toString();
                                                                                           String phoneNumber = feedObject.getProfile_phone().toString();
                                                                                           String authorid = feedObject.getAuthorid() + "";

                                                                                           List<String> userPhones = new ArrayList<String>();
                                                                                           userPhones.add(phoneNumber);

                                                                                           long profileId = feedObject.getAuthorid();

                                                                                           switch (item.getTitle().toString()) {

                                                                                               case "Call now":

                                                                                                   if (phoneNumber != null && phoneNumber != "") {

                                                                                                       Utility.userStats(mContext, "call");

                                                                                                       Intent phoneIntent = new Intent(Intent.ACTION_CALL);
                                                                                                       phoneIntent.setData(Uri.parse("tel:" + phoneNumber));

                                                                                                       try {

                                                                                                           mContext.startActivity(phoneIntent);
                                                                                                           ///finish();
                                                                                                           // Log.i("Finished making a call...", "");
                                                                                                       } catch (android.content.ActivityNotFoundException ex) {
                                                                                                           Toast.makeText(mContext,
                                                                                                                   "Call failed, please try again later.", Toast.LENGTH_SHORT).show();
                                                                                                       }
                                                                                                   } else {
                                                                                                       Toast.makeText(mContext,
                                                                                                               "No phone number availablle.", Toast.LENGTH_SHORT).show();
                                                                                                   }


                                                                                                   break;

                                                                                               case "Reply":

                                                                                                   Intent intent = new Intent(mActivity, XMPPClient.class);
                                                                                                   String num = userPhones.get(0);
                                                                                                   intent.putExtra("phone_key", num);
                                                                                                   intent.putExtra("userid", profileId + "");
                                                                                                   intent.putExtra("name", feedObject.getProfile_name());
                                                                                                   intent.putExtra("img", feedObject.getProfile_img());
                                                                                                   intent.putExtra("last_seen", feedObject.getCallTime());


                                                                                                   mActivity.startActivity(intent);
//                    startActivityForResult(intent, Constants.REVIEW_REQUEST_CODE);

                                                                                                   //   Toast.makeText(mContext, "Not supported currently", Toast.LENGTH_SHORT).show();


                                    *//*
                                    Intent intent = new Intent(mContext, PostReply.class);
                                    intent.putExtra("postId", post_id);

                                    mContext.startActivity(intent);*//*
                                                                                                   break;
                                                                                               case "View Profile":
                                                                                                   Intent intent1 = new Intent(mContext, Profile.class);
                                                                                                   intent1.putExtra("authorid", authorid);
                                                                                                   mContext.startActivity(intent1);
                                                                                                   break;


                                                                                           }

                                                                                           // Toast.makeText(context, "You Clicked : " + post_id + " " + item.getTitle(), Toast.LENGTH_SHORT).show();

                                                                                           return true;
                                                                                       }
                                                                                   }

                                                  );


                                                  popup.show();//showing popup menu


                                              }
                                          }

            );

            iv_bookmark.setOnClickListener(
                    new View.OnClickListener()

                    {
                        @Override
                        public void onClick(final View v) {

                            final BookmarkTag bt = (BookmarkTag) v.getTag();
                            final String post_id = bt.getId();
                            final Boolean bookmarked = bt.getBookmarked();
                            final ImageView iv_bk = (ImageView) v;
                            if (!bookmarked) {
                                //google tracker event
                                AppController.getInstance().setupEvent(v, R.id.bookmark, R.string.infoCategory, R.string.actionBookmarks, R.string.actionBookmarks);
                                iv_bk.setImageDrawable(heart_red);
                                BookmarkTag bt_n = new BookmarkTag();
                                bt_n.setId(bt.getId());
                                bt_n.setBookmarked(true);
                                iv_bk.setTag(bt_n);
                                setBookmark(post_id, iv_bk, true, bt_n,getPosition());
                            } else {
                                //google tracker event
                                AppController.getInstance().setupEvent(v, R.id.bookmark, R.string.infoCategory, R.string.actionRemoveBookmark, R.string.actionRemoveBookmark);
                                iv_bk.setImageDrawable(heart_grey);
                                BookmarkTag bt_n = new BookmarkTag();
                                bt_n.setId(bt.getId());
                                bt_n.setBookmarked(false);
                                iv_bk.setTag(bt_n);
                                setBookmark(post_id, iv_bk, false, bt_n,getPosition());
                            }
                        }
                    }
            );
        }

        public MyNetworkImageView getIv_profile_image() {
            return iv_profile_image;
        }

        public ImageView getIv_options() {
            return iv_options;
        }

        public ImageView getIv_bookmark() {
            return iv_bookmark;
        }

        public TextView getTv_profile_name() {
            return tv_profile_name;
        }

        public TextView getTv_post_time() {
            return tv_post_time;
        }

        public TextView getPost_content() {
            return post_content;
        }

        public TextView getTv_post_id() {
            return tv_post_id;
        }

        public TextView getUser_phone() {
            return user_phone;
        }

        public TextView getAnchor() {
            return anchor;
        }


        public CardView getCard_view() {
            return card_view;
        }

        public LinearLayout getmMore() {
            return mMore;
        }

    }


    public BookmarksCustomAdapter(Context mContext, ArrayList<Feeds> feedsImg, Activity activity, OnUpdateListener  bookmarkDeleteListener) {
        this.mContext = AppController.getHome();
        this.feedsImg = feedsImg;
        mActivity = AppController.getHome();
        feedsCount = feedsImg.size();
        this.bookmarkDeleteListener = bookmarkDeleteListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cards_layout, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        final Feeds feed = feedsImg.get(position);

        heart_red = mContext.getResources().getDrawable(R.drawable.ic_favorite_enabled);
        heart_grey = mContext.getResources().getDrawable(R.drawable.ic_favorite_disabled);
        //viewHolder.getPoster().setImage(story.getFeaturedImg(), AppController.getInstance().getImageLoader());

        String imgUrl = feed.getProfile_img();

        viewHolder.getIv_profile_image().setDefaultImageResId(R.drawable.profile_default_one);
        viewHolder.getIv_profile_image().setImage(imgUrl, AppController.getInstance().getImageLoader());
        viewHolder.getTv_profile_name().setText(feed.getProfile_name());
        viewHolder.getTv_post_time().setText(feed.getCallTime());
        viewHolder.getTv_post_id().setText(feed.get_id() + "");
        viewHolder.getUser_phone().setText(feed.getProfile_phone());

        BookmarkTag bt = new BookmarkTag();
        bt.setId(feed.get_id());
        if (feed.getBookmarked()) {
            bt.setBookmarked(true);
            viewHolder.getIv_bookmark().setImageDrawable(heart_red);
        } else {
            bt.setBookmarked(false);
            viewHolder.getIv_bookmark().setImageDrawable(heart_grey);
        }
        viewHolder.getIv_bookmark().setTag(bt);

        viewHolder.getIv_options().setTag(feed);
        viewHolder.getPost_content().setTag(feed);
        viewHolder.getmMore().setTag(feed);

        String text = feed.getText();

        int start = 0;
        int end = 250;

        if (feed.getSnippet().size() > 0) {

            start = Integer.parseInt(feed.getSnippet().get(0));
            end = Integer.parseInt(feed.getSnippet().get(1));
        }


        if (feed.getText().length() < end) {
            viewHolder.getmMore().setVisibility(View.GONE);

        } else {

            text = text.substring(start, end);
            //  viewHolder.getPost_content().setMaxLines(4);
            viewHolder.getmMore().setVisibility(View.VISIBLE);
        }


        //adding span for hashtags in post
       // ActivityUtils.setupHashTags(feed, text, start, end, mContext, viewHolder.getPost_content());

        String summary = feed.getSumm();
        if(summary != null){
            viewHolder.postContentSummary.setVisibility(View.VISIBLE);
         //   ActivityUtils.setupHashTags(feed, summary, 0, summary.length(), mContext, viewHolder.postContentSummary);
        }else{
            viewHolder.postContentSummary.setVisibility(View.GONE);
        }

        viewHolder.getCard_view().setTag(feed);

        if (feed.getBookmarked()) {
            Drawable heart_red = mContext.getResources().getDrawable(R.drawable.ic_favorite_enabled);
            viewHolder.getIv_bookmark().setImageDrawable(heart_red);
            viewHolder.getAnchor().setText("true");
        }



      *//*  LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        layoutParams.weight = 1;

        layoutParams.setMargins(5,0,0,0);


        for(int i=0;i<locationsAutos.size();i++ )
        {
            TextView tv = new TextView(mContext);
            tv.setLayoutParams(layoutParams);
            tv.setText(locationsAutos.get(i).getTitle());
            tv.setTextSize(15);
            tv.setTextColor(mContext.getResources().getColor(R.color.appColor5));

            viewHolder.post_tags.addView(tv);

        }

*//*
        viewHolder.getPost_content().setMaxLines(3);
    }

    @Override
    public int getItemCount() {
        return feedsImg.size();
    }

    public void addItems(ArrayList<Feeds> feedsImg) {
        for (Feeds feeds1 : feedsImg) {
            addItem(feedsCount, feeds1);
        }

    }

    public void addItem(int position, Feeds feed) {
        feedsImg.add(position, feed);
        notifyItemInserted(position);
        feedsCount++;
    }

    public void removeItem(int position) {
        feedsImg.remove(position);
        notifyItemRemoved(position);
    }


    static void setBookmark(final String post_id, final ImageView iv_bookmark, final Boolean isSetBookmark, final BookmarkTag bt_n,final int position) {
        final HashMap<String, String> params = new HashMap<String, String>();
        final Drawable iv_bg = iv_bookmark.getDrawable();
        if (isSetBookmark) {
            params.put(PlaBroApi.RT, PlaBroApi.PLABRO_AUTH_POST_PARAM_RT_SETBOOKMARK);

        } else {
            params.put(PlaBroApi.RT, PlaBroApi.PLABRO_AUTH_POST_PARAM_RT_DELETEBOOKMARK);
        }
        params.put(PlaBroApi.AUTH_KEY, PBPreferences.getAuthKey(mContext));
        params.put(PlaBroApi.PLABRO_FEEDS_POST_PARAM_BOOKMARK_MESSAGE_ID, post_id);
        long timeStamp = (System.currentTimeMillis() / 1000);
        int hashkey = Utility.create_auth_hash_key(params, timeStamp);
        params.put("keytime", timeStamp + "");
        params.put("hashkey", hashkey + "");

        AppVolley.processRequest(null, PlaBroApi.BASE_URL, params, RequestMethod.GET, new ResponseListener() {
            @Override
            public void onSuccessResponse(String response) {
                if (response != null) {
                    Gson gson = new Gson();
                    BookmarkResponse bookmarkResponse = gson.fromJson(response, BookmarkResponse.class);
                    if (bookmarkResponse.isStatus()) {
                        if (iv_bg.getConstantState().equals(heart_grey.getConstantState())) {
                            Toast.makeText(mContext, bookmarkResponse.getMsg(), Toast.LENGTH_LONG).show();
                            PBPreferences.setBookmarkState(mActivity, true);
                        } else {
                            Toast.makeText(mContext, bookmarkResponse.getMsg(), Toast.LENGTH_LONG).show();
                        }

                        if (!isSetBookmark) {
                            //todo add listener to remove bookmark list
                            // new Delete().from(BookmarksDBModel.class).where("_id="+feedsImg.get(0)).execute();
                            bookmarkDeleteListener.onResponse(true,position);


                        }
                    } else {
                        if (isSetBookmark) {
                            Toast.makeText(mContext, "Cannot Bookmark, Please check your network or try again", Toast.LENGTH_LONG).show();
                            iv_bookmark.setImageDrawable(heart_grey);
                            bt_n.setBookmarked(false);
                            iv_bookmark.setTag(bt_n);
                        } else {
                            Toast.makeText(mContext, "Cannot Delete Bookmark, Please check your network or try again", Toast.LENGTH_LONG).show();
                            iv_bookmark.setImageDrawable(heart_red);
                            bt_n.setBookmarked(false);
                            iv_bookmark.setTag(bt_n);
                        }

                        if (bookmarkResponse.getMsg().equalsIgnoreCase("Session Expired") || bookmarkResponse.getMsg().equalsIgnoreCase("Missing AuthKey in the request")) {
                            if (!PBPreferences.getSessEXpDialogState(mContext)) {
                                PBPreferences.setSessEXpDialogState(mContext, true);
                                final MaterialDialog mMaterialDialog = new MaterialDialog(mContext);

                                mMaterialDialog.setTitle("Try again")
                                        .setMessage("Oops ! Your Session Expired.")
                                        .setPositiveButton("CONNECT", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Utility.userLogin(mContext);
                                                setBookmark(post_id, iv_bookmark, isSetBookmark, bt_n,position);
                                                mMaterialDialog.dismiss();
                                            }
                                        })

                                        .setNegativeButton("CANCEL", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mMaterialDialog.dismiss();
                                            }
                                        });

                                mMaterialDialog.show();
                            }
                        }

                    }
                }
            }

            @Override
            public void onFailureResponse(VolleyError volleyError) {
                if (isSetBookmark) {
                    Toast.makeText(mContext, "Cannot Bookmark, Please check your network or try again", Toast.LENGTH_LONG).show();
                    iv_bookmark.setImageDrawable(heart_grey);
                    bt_n.setBookmarked(false);
                    iv_bookmark.setTag(bt_n);
                } else {
                    Toast.makeText(mContext, "Cannot Delete Bookmark, Please check your network or try again", Toast.LENGTH_LONG).show();
                    iv_bookmark.setImageDrawable(heart_red);
                    bt_n.setBookmarked(false);
                    iv_bookmark.setTag(bt_n);
                }
            }
        });
    }

*/
}
