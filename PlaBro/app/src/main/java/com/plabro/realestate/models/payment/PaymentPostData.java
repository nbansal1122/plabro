package com.plabro.realestate.models.payment;

public class PaymentPostData {

    private String key;
    private String salt;
    private int env;
    private String user_credentials;
    private String txnid;
    private String amount;
    private String productinfo;
    private String firstname;
    private String email;
    private String phone;
    private String surl;
    private String furl;
    private String curl;
    private String pg;
    private String hash;
    private String udf1="";
    private String udf2="";
    private String udf3="";
    private String udf4="";
    private String udf5="";
    private String offer_key;
//    String hashValues = "get_user_cards,save_user_card,edit_user_card,delete_user_card,payment_related_details_for_mobile_sdk";
    private String get_user_cards_hash;
    private String save_user_card_hash;
    private String edit_user_card_hash;

    public String getDelete_user_card_hash() {
        return delete_user_card_hash;
    }

    public void setDelete_user_card_hash(String delete_user_card_hash) {
        this.delete_user_card_hash = delete_user_card_hash;
    }

    private String delete_user_card_hash;

    public String getPayment_related_details_for_mobile_sdk_hash() {
        return payment_related_details_for_mobile_sdk_hash;
    }

    public void setPayment_related_details_for_mobile_sdk_hash(String payment_related_details_for_mobile_sdk_hash) {
        this.payment_related_details_for_mobile_sdk_hash = payment_related_details_for_mobile_sdk_hash;
    }

    public String getEdit_user_card_hash() {
        return edit_user_card_hash;
    }

    public void setEdit_user_card_hash(String edit_user_card_hash) {
        this.edit_user_card_hash = edit_user_card_hash;
    }

    public String getSave_user_card_hash() {
        return save_user_card_hash;
    }

    public void setSave_user_card_hash(String save_user_card_hash) {
        this.save_user_card_hash = save_user_card_hash;
    }

    public String getGet_user_cards_hash() {
        return get_user_cards_hash;
    }

    public void setGet_user_cards_hash(String get_user_cards_hash) {
        this.get_user_cards_hash = get_user_cards_hash;
    }

    private String payment_related_details_for_mobile_sdk_hash;

    public String getMobile_sdk_hash() {
        return mobile_sdk_hash;
    }

    public void setMobile_sdk_hash(String mobile_sdk_hash) {
        this.mobile_sdk_hash = mobile_sdk_hash;
    }

    private String mobile_sdk_hash;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public int getEnv() {
        return env;
    }

    public void setEnv(int env) {
        this.env = env;
    }

    public String getUser_credentials() {
        return user_credentials;
    }

    public void setUser_credentials(String user_credentials) {
        this.user_credentials = user_credentials;
    }

    public String getTxnid() {
        return txnid;
    }

    public void setTxnid(String txnid) {
        this.txnid = txnid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getProductinfo() {
        return productinfo;
    }

    public void setProductinfo(String productinfo) {
        this.productinfo = productinfo;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSurl() {
        return surl;
    }

    public void setSurl(String surl) {
        this.surl = surl;
    }

    public String getFurl() {
        return furl;
    }

    public void setFurl(String furl) {
        this.furl = furl;
    }

    public String getCurl() {
        return curl;
    }

    public void setCurl(String curl) {
        this.curl = curl;
    }

    public String getPg() {
        return pg;
    }

    public void setPg(String pg) {
        this.pg = pg;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getUdf1() {
        return udf1;
    }

    public void setUdf1(String udf1) {
        this.udf1 = udf1;
    }

    public String getUdf2() {
        return udf2;
    }

    public void setUdf2(String udf2) {
        this.udf2 = udf2;
    }

    public String getUdf3() {
        return udf3;
    }

    public void setUdf3(String udf3) {
        this.udf3 = udf3;
    }

    public String getUdf4() {
        return udf4;
    }

    public void setUdf4(String udf4) {
        this.udf4 = udf4;
    }

    public String getUdf5() {
        return udf5;
    }

    public void setUdf5(String udf5) {
        this.udf5 = udf5;
    }

    public String getOffer_key() {
        return offer_key;
    }

    public void setOffer_key(String offer_key) {
        this.offer_key = offer_key;
    }
}