package com.plabro.realestate.models.Tracker;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.plabro.realestate.listeners.ModelInterface;

/**
 * Created by jmd on 6/1/2015.
 */
@Table(name = "WifiData")
public class WifiData extends Model implements ModelInterface{

    @Column(unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String ssid;
    @Column
    private String bssid;
    @Column
    private long distance;
    @Column
    private long timestamp;
    @Column
    private double lat;
    @Column
    private double lng;
    @Column
    private int level;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public long getDistance() {
        return distance;
    }

    public void setDistance(long distance) {
        this.distance = distance;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public long saveData() {
        try{
            return this.save();
        }catch(Exception e){
            e.printStackTrace();
        }
        return -1;
    }
}
