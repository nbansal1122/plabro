package com.plabro.realestate.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.adapter.CustomPagerAdapter;
import com.plabro.realestate.fragments.services.DirectInventoryFragment;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.slidingTab.SlidingTabLayout;
import com.plabro.realestate.uiHelpers.view.FontManager;
import com.plabro.realestate.utilities.Constants;

import java.util.ArrayList;

/**
 * Created by nitin on 19/01/16.
 */
public class DirectListingFragment extends PlabroBaseFragment implements CustomPagerAdapter.PagerAdapterInterface<String>, ViewPager.OnPageChangeListener {
    private ViewPager mViewPager;
    private ArrayList<String> fragmentNames = new ArrayList<>();
    private CustomPagerAdapter<String> adapter;
    private Toolbar mToolbar;
    private SearchBoxCustom search;
    private static final String TYPEFACE_ROBOTTO_NORMAL = "Fonts/Roboto-Regular.ttf";

    private SlidingTabLayout mSlidingTabLayout;

    public static DirectListingFragment getInstance(Toolbar toolbar, SearchBoxCustom searchBoxCustom) {
        DirectListingFragment f = new DirectListingFragment();
        f.mToolbar = toolbar;
        f.search = searchBoxCustom;
        return f;
    }
//    public static DirectListingFragment getInstance() {
//        DirectListingFragment f = new DirectListingFragment();
//        return f;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerLocalReceiver();
    }

    @Override
    public String getTAG() {
        return "Direct Listing";
    }

    @Override
    protected void findViewById() {
        initFragmentNames();
        setTitle("Direct Listings");
        setPagerAdapter();
        setTabTextView();
        if(search != null){
            search.closeSearch();
        }
    }

    private void initFragmentNames() {
        fragmentNames.clear();
        fragmentNames.add("Purchased");
        fragmentNames.add("All");
    }

    private void setPagerAdapter() {
        if (null == getActivity()) return;
        mSlidingTabLayout = (SlidingTabLayout) findView(R.id.navig_tab);
        mSlidingTabLayout.setVisibility(View.VISIBLE);
        mViewPager = (ViewPager) findView(R.id.pager);
        adapter = new CustomPagerAdapter<>(getChildFragmentManager(), fragmentNames, this);
        mViewPager.setAdapter(adapter);
//        mViewPager.setOnPageChangeListener(this);
        mViewPager.addOnPageChangeListener(this);
        mSlidingTabLayout.setViewPager(mViewPager);
        mViewPager.setCurrentItem(1);
    }

    @Override
    protected int getViewId() {
        return R.layout.activity_search_filter;
    }

    @Override
    public Fragment getFragmentItem(int position, String listItem) {
        Fragment f = null;
        Bundle b = new Bundle();
        switch (position) {
            case 0:
                b.putString(DirectInventoryFragment.FEED_TYPE, "bought");
                return DirectInventoryFragment.getInstance(b, mToolbar, search);
            case 1:
                b.putString(DirectInventoryFragment.FEED_TYPE, "all");
                return DirectInventoryFragment.getInstance(b, mToolbar, search);
        }
        return f;
    }

    @Override
    public CharSequence getPageTitle(int position, String listItem) {
        return listItem;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    private void setTabTextView() {
        int count = mSlidingTabLayout.getTabStrip().getChildCount();
        int currentPosition = mViewPager.getCurrentItem();
        for (int i = 0; i < count; i++) {
            TextView v = (TextView) mSlidingTabLayout.getTabStrip().getChildAt(i);
            Typeface tf = FontManager.getInstance().getFont(TYPEFACE_ROBOTTO_NORMAL);
            v.setTypeface(tf, Typeface.BOLD);
            if (i == currentPosition)
                v.setTextColor(getResources().getColor(R.color.white));
            else
                v.setTextColor(getResources().getColor(R.color.tab_text_blue));
        }
    }

    @Override
    public void onPageSelected(int position) {
        if (position == 1) {
            Analytics.trackScreen(Analytics.OtherScreens.CallLogs);
        }
        Log.d(TAG, "" + position);
        mViewPager.setCurrentItem(position);
        setTabTextView();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        if (Constants.ACTION_PROFILE_UPDATE.equals(intent.getAction())) {
            ((PlabroBaseActivity) getActivity()).setTitle("Direct Listings");
        }
    }
}
