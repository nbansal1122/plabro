package com.plabro.realestate.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.plabro.realestate.R;
import com.plabro.realestate.models.history.AutoSuggestion;
import com.plabro.realestate.uiHelpers.view.FontText;

import java.util.List;

public class SuggestedSearchAdapter extends ArrayAdapter<AutoSuggestion> {
    Context mContext;
    List<AutoSuggestion> mData;

    public SuggestedSearchAdapter(Context context, List<AutoSuggestion> data) {
        super(context, R.layout.suggested_search, data);
        mContext = context;
        mData = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.suggested_search, parent, false);
        FontText textView = (FontText) rowView.findViewById(R.id.tv_label);
        textView.setText(mData.get(position).getText());
        return rowView;
    }
}