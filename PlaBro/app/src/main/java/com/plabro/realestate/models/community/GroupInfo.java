package com.plabro.realestate.models.community;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nbansal2211 on 18/05/16.
 */
public class GroupInfo implements Serializable{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("admin")
    @Expose
    private List<Admin> admin = new ArrayList<Admin>();
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("totalusers")
    @Expose
    private Integer totalusers;
    @SerializedName("activeUsers")
    @Expose
    private List<Admin> activeUsers = new ArrayList<Admin>();
    @SerializedName("active")
    @Expose
    private Integer active;
    @SerializedName("_id")
    @Expose
    private Integer _id;
    @SerializedName("desc")
    @Expose
    private String desc;

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The admin
     */
    public List<Admin> getAdmin() {
        return admin;
    }

    /**
     * @param admin The admin
     */
    public void setAdmin(List<Admin> admin) {
        this.admin = admin;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The totalusers
     */
    public Integer getTotalusers() {
        return totalusers;
    }

    /**
     * @param totalusers The totalusers
     */
    public void setTotalusers(Integer totalusers) {
        this.totalusers = totalusers;
    }

    /**
     * @return The activeUsers
     */
    public List<Admin> getActiveUsers() {
        return activeUsers;
    }

    /**
     * @param activeUsers The activeUsers
     */
    public void setActiveUsers(List<Admin> activeUsers) {
        this.activeUsers = activeUsers;
    }

    /**
     * @return The active
     */
    public Integer getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * @return The _id
     */
    public Integer get_id() {
        return _id;
    }

    /**
     * @param _id The _id
     */
    public void set_id(Integer _id) {
        this._id = _id;
    }

    /**
     * @return The desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc The desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

}
