package com.plabro.realestate.activity.CommunityActivities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.HashMap;

public class InviteCommunityUsers extends PlabroBaseActivity implements VolleyListener {

    private String commId;


    public static void startActivity(Activity ctx, String commId) {
        Bundle b = new Bundle();
        b.putString(Constants.BUNDLE_KEYS.COMMUNITY_ID, commId);
        Intent i = new Intent(ctx, InviteCommunityUsers.class);
        i.putExtras(b);
        ctx.startActivity(i);
    }

    @Override
    protected String getTag() {
        return "Invite Users";
    }

    @Override
    protected void loadBundleData(Bundle b) {
        super.loadBundleData(b);
        commId = b.getString(Constants.BUNDLE_KEYS.COMMUNITY_ID);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_community_users);
    }

    private void fetchSuggestedUsers() {
        ParamObject obj = new ParamObject();
        HashMap<String, String> map = new HashMap<>();
        map.put("group_id", commId);
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.SUGG_COMMUNITY_USERS, map));
        obj.setTaskCode(Constants.TASK_CODES.SUGG_COMMUNITY_USERS);
        AppVolley.processRequest(obj, this);
    }


    @Override
    public void findViewById() {
        fetchSuggestedUsers();
    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        switch (taskCode) {
            case Constants.TASK_CODES.SUGG_COMMUNITY_USERS:

                break;
        }
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {

    }
}
