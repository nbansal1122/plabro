package com.plabro.realestate.models.keyboardSuggestions;

public class KeyBoardSuggestionResponse {
    private String RT;

    private boolean status;


    private KSOutputParams output_params;

    public String getRT() {
        return RT;
    }

    public void setRT(String RT) {
        this.RT = RT;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }


    public KSOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(KSOutputParams output_params) {
        this.output_params = output_params;
    }

}