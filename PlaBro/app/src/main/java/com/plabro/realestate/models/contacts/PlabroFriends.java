package com.plabro.realestate.models.contacts;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by hemantkumar on 08/01/15.
 */
@Table(name = "UserFriends")
public class PlabroFriends extends Model {

   // @Column(unique = true,onUniqueConflict = Column.ConflictAction.REPLACE)
   @Column
    private int _id ;
    @Column
    private String time;
    @Column(unique = true,onUniqueConflict = Column.ConflictAction.REPLACE)
    private String phone;
    @Column
    private String area_radius;
    @Column
    private String location;
    @Column
    private String followed;
    @Column
    private String authorid;
    @Column
    private String isFollowedByMe;
    @Column
    private String name;
    @Column
    private String FOLLOWING;
    @Column
    private String img;

    public int getUserid() {
        return _id;
    }

    public void setUserid(int _id) {
        this._id = _id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getArea_radius() {
        return area_radius;
    }

    public void setArea_radius(String area_radius) {
        this.area_radius = area_radius;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFollowed() {
        return followed;
    }

    public void setFollowed(String followed) {
        this.followed = followed;
    }

    public String getAuthorid() {
        return authorid;
    }

    public void setAuthorid(String authorid) {
        this.authorid = authorid;
    }

    public String getIsFollowedByMe() {
        return isFollowedByMe;
    }

    public void setIsFollowedByMe(String isFollowedByMe) {
        this.isFollowedByMe = isFollowedByMe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFOLLOWING() {
        return FOLLOWING;
    }

    public void setFOLLOWING(String FOLLOWING) {
        this.FOLLOWING = FOLLOWING;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }



}
