/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.plabro.realestate.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.listeners.OnChildListener;
import com.plabro.realestate.models.contacts.PlabroFriends;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.Utility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class PeopleCustomAdapter extends RecyclerView.Adapter<PeopleCustomAdapter.ViewHolder> {

    private static Context mContext;
    private static Activity mActivity;
    private int contactsCount = 0;
    private static final String TAG = "ContactsCustomAdapter";
    private static OnChildListener childListener;


    private List<PlabroFriends> contacts = new ArrayList<PlabroFriends>();
    private List<PlabroFriends> arrayList = new ArrayList<PlabroFriends>();


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final RoundedImageView mProfileImage;
        private final TextView mContactName, mContactHead, mLastSeen;
        private final RelativeLayout mContactLayout;

        public ViewHolder(View v) {
            super(v);


            this.mContactHead = (TextView) itemView.findViewById(R.id.tv_name_head);
            this.mContactName = (TextView) itemView.findViewById(R.id.tv_name);
            this.mProfileImage = (RoundedImageView) itemView.findViewById(R.id.profile_image);
            this.mContactLayout = (RelativeLayout) itemView.findViewById(R.id.rl_contacts_layout);
            this.mLastSeen = (TextView) itemView.findViewById(R.id.tv_last_seen);


            mContactLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PlabroFriends plabroContacts = (PlabroFriends) v.getTag();

                    String phoneNumber = plabroContacts.getPhone();
                    String userId = plabroContacts.getAuthorid();

                    Utility.userStats(mContext, "im");

                    HashMap<String, Object> dataIm = new HashMap<String, Object>();
                    dataIm.put("imTo", phoneNumber);
                    dataIm.put("imFrom", PBPreferences.getPhone());
                    dataIm.put("Action", "Chat");
                    dataIm.put("ScreenName", TAG);
                    Analytics.trackInteractionEvent(Analytics.CardActions.Chat, dataIm);


                    List<String> userPhones = new ArrayList<String>();
                    userPhones.add(phoneNumber);
                    Intent intent = new Intent(mActivity, XMPPChat.class);
                    String num = userPhones.get(0);
                    String displayName = ActivityUtils.getDisplayNameFromPhone(num);
                    if (displayName != null && !TextUtils.isEmpty(displayName)) {
                        intent.putExtra("name", displayName);

                    } else {
                        intent.putExtra("name", plabroContacts.getName());

                    }
                    intent.putExtra("img", plabroContacts.getImg());
                    intent.putExtra("last_seen", plabroContacts.getTime());
                    intent.putExtra("phone_key", num);
                    intent.putExtra("userid", userId + "");
                    mActivity.startActivity(intent);
                }
            });


        }

        public RoundedImageView getIv_profile_image() {
            return mProfileImage;
        }

        public RoundedImageView getmProfileImage() {
            return mProfileImage;
        }

        public TextView getmContactName() {
            return mContactName;
        }

        public TextView getmContactHead() {
            return mContactHead;
        }

        public RelativeLayout getmContactLayout() {
            return mContactLayout;
        }

        public TextView getmLastSeen() {
            return mLastSeen;
        }
    }


    public PeopleCustomAdapter(Context mContext, Activity activity, List<PlabroFriends> contacts) {
        this.mContext = mContext;
        this.contacts = contacts;
        mActivity = activity;
        contactsCount = this.contacts.size();
        this.childListener = childListener;
        arrayList.addAll(contacts);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.contacts_layout, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        PlabroFriends plabroContacts = contacts.get(position);

        //viewHolder.getPoster().setImage(story.getFeaturedImg(), AppController.getInstance().getImageLoader());

        String imgUrl = plabroContacts.getImg();
        Picasso picasso = null;

        if (picasso == null) {
            picasso = Picasso.with(mContext);
        }
        if (!TextUtils.isEmpty(imgUrl)) {
            picasso.load(imgUrl).placeholder(R.drawable.profile_default_one).into(viewHolder.getIv_profile_image());
        } else {
            viewHolder.getIv_profile_image().setImageResource(R.drawable.profile_default_one);
        }
//        .setDefaultImageResId(R.drawable.profile_default_one);
//        viewHolder.getIv_profile_image().setImage(imgUrl, AppController.getInstance().getImageLoader());


        if (!TextUtils.isEmpty(plabroContacts.getTime())) {
            viewHolder.getmLastSeen().setText("Last seen " + plabroContacts.getTime());
        } else {
            viewHolder.getmLastSeen().setText("");
        }
//        String displayName = "";
//
//        if (phoneVsName.containsKey(plabroContacts.getPhone())) {
//            displayName = phoneVsName.get(plabroContacts.getPhone());
//        } else {
//            displayName = plabroContacts.getName().trim();
//            if (displayName != null && TextUtils.isEmpty(displayName)) {
//                displayName = plabroContacts.getPhone();
//            }
//        }
//        plabroContacts.setName(displayName);
        viewHolder.getmContactName().setText(plabroContacts.getName());

        if (position > 0) {

            String oldHead = contacts.get(position - 1).getName().substring(0, 1);
            String newHead = plabroContacts.getName().substring(0, 1);
            if (!oldHead.equalsIgnoreCase(newHead)) {
                viewHolder.getmContactHead().setText(newHead);

            } else {
                viewHolder.getmContactHead().setText("");

            }


        } else {
            viewHolder.getmContactHead().setText(plabroContacts.getName().substring(0, 1));

        }

        viewHolder.getmContactLayout().setTag(plabroContacts);
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public void addItems(ArrayList<PlabroFriends> plabroContactses) {
        for (PlabroFriends plabroContacts : plabroContactses) {
            addItem(contactsCount, plabroContacts);
        }

    }

    public void addItem(int position, PlabroFriends plabroContacts) {
        contacts.add(position, plabroContacts);
        notifyItemInserted(position);
        contactsCount++;
    }

    public void removeItem(int position) {
        contacts.remove(position);
        notifyItemRemoved(position);
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        contacts.clear();
        if (arrayList.size() > 0) {
            if (charText.length() == 0) {
                contacts.addAll(arrayList);
            } else {
                for (PlabroFriends gsd : arrayList) {
                    if (gsd.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        contacts.add(gsd);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

}
