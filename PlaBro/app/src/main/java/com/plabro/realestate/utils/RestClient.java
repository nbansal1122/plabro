package com.plabro.realestate.utils;

import android.text.TextUtils;
import android.util.Log;

import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.utilities.Util;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class RestClient {

    private static final String boundary = "*****";
    private static final String lineEnd = "\r\n";
    private static final String twoHyphens = "--";
    private static final String separator = twoHyphens + boundary + lineEnd;

    // constructor
    public RestClient() {

    }

    private static InputStream OpenHttpConnection(ParamObject object, File file)
            throws IOException {

        Log.d("palval", "OpenHttpConnection");
        InputStream in = null;
        int response = -1;

        URL url = new URL(object.getUrl());
        URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection))
            throw new IOException("Not an HTTP connection");

        try {
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setConnectTimeout(60000);


            httpConn.setRequestMethod("POST");
            httpConn.setUseCaches(false);
//            httpConn.setDoOutput(true); // indicates POST method
//            httpConn.setDoInput(true);
            httpConn.setRequestProperty("Content-Type",
                    "multipart/form-data; boundary=" + boundary);
//            httpConn.setRequestProperty("User-Agent", "PlabroAndroid");

            DataOutputStream dataOutputStream = new DataOutputStream(
                    httpConn.getOutputStream());
            if (object.getParams() != null) {
//                String prms = getParamsString(object.getParams());
//                dataOutputStream.writeBytes(prms);
                sendTextParams(dataOutputStream, object);
            }
            if (file != null) {
                sendFile(file, dataOutputStream);
            }
            dataOutputStream.flush();
            dataOutputStream.close();
            httpConn.connect();
            response = httpConn.getResponseCode();
            com.plabro.realestate.CustomLogger.Log.e("Connected to Stream", "YES");
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            com.plabro.realestate.CustomLogger.Log.e("Internet Connecting Exception", "Unable To Connect");
        }
        return in;
    }

    private static String getParamsString(Map<String, String> mParams) {
        String params = "", url;
        boolean firstEntry = true;

        for (Map.Entry<String, String> entry : mParams.entrySet()) {

            String key = entry.getKey();
            String value = entry.getValue();

            if (firstEntry) {
                params = "?" + key + "=" + value;
                firstEntry = false;
            } else {
                params = params + "&" + key + "=" + Util.getURLEncodedString(value);
            }
        }
        return params;
    }

    private static void sendTextParams(DataOutputStream dos, ParamObject obj) throws IOException {
        Set<String> allKeys = obj.getParams().keySet();
        for (String key : allKeys) {
            writeFormField(dos, separator, key, obj.getParams().get(key));
        }
    }

    private static void sendFile(File file, DataOutputStream dos) throws IOException {
        int maxBufferSize = 64 * 1024; // 64 kilobyes


        dos.writeBytes(twoHyphens + boundary + lineEnd);
        dos.writeBytes("Content-Disposition: form-data; name=\"whatsappfile\";filename=\"" + "form.txt" + "\"" + lineEnd);
        dos.writeBytes(lineEnd);


        // Read file and create buffer
        FileInputStream fileInputStream = new FileInputStream(file);
        int bytesAvailable = fileInputStream.available();
        int bufferSize = Math.min(bytesAvailable, maxBufferSize);
        byte[] buffer = new byte[bufferSize];
        // Send file data
        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        while (bytesRead > 0) {
            // Write buffer to socket
            dos.write(buffer, 0, bufferSize);


            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }

        // send multipart form data necesssary after file data
        dos.writeBytes(lineEnd);
        dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
//        dos.flush();
//        dos.close();
        fileInputStream.close();
    }

    private static void writeFormField(DataOutputStream dos, String separator,
                                       String fieldName, String fieldValue) throws IOException {
        dos.writeBytes(separator);
        dos.writeBytes("Content-Disposition: form-data; name=\"" + fieldName
                + "\"\r\n");
        dos.writeBytes("\r\n");
        dos.writeBytes(fieldValue);
//        dos.writeBytes("\r\n");
    }

    public static String sendHttpRequest(ParamObject object, File file) {

        InputStream in = null;
        JSONObject jObj = null;
        String json = "";
        // Making HTTP request
        try {

            in = OpenHttpConnection(object, file);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    in, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            in.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        return json;

    }
}