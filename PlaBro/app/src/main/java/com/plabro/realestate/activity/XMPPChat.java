package com.plabro.realestate.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.content.ContentProvider;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.HomeActivity;
import com.plabro.realestate.R;
import com.plabro.realestate.Services.XMPPConnectionService;
import com.plabro.realestate.adapter.CustomCursorAdapter;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.MessageListenerImpl;
import com.plabro.realestate.listeners.OnChatMessageListener;
import com.plabro.realestate.listeners.OnUpdateListener;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.XMPP.RosterData;
import com.plabro.realestate.models.chat.ChatMessage;
import com.plabro.realestate.models.chat.ChatPopUpInfo;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.profile.ProfileResponse;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.CleverTap;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBConnectionManager;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.RelativeTimeTextView;
import com.plabro.realestate.widgets.dialogs.MessageOptionDialog;
import com.plabro.realestate.xmpp.ReadReceipts.ReadReceipt;
import com.plabro.realestate.xmpp.RosterManager;
import com.plabro.realestate.xmpp.SettingsDialog;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.ChatState;
import org.jivesoftware.smackx.ChatStateManager;
import org.jivesoftware.smackx.MessageEventManager;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;


public class XMPPChat extends PlabroBaseActivity implements PBConnectionManager.XMPPConnectionListener, LoaderManager.LoaderCallbacks<Cursor>, CustomCursorAdapter.CustomCursorAdapterInterface, View.OnClickListener, CustomListAdapter.CustomListAdapterInterface {

    private List<ChatMessage> messages = new ArrayList<ChatMessage>();
    private Handler mHandler = new Handler();
    private SettingsDialog mDialog;
    private String mRecipient, to = "";
    private String mRecipientName = "", mRecImage = "", mSenderImage;
    private String mRecipientAuthId;
    private EditText mSendText;
    private ListView mList;
    private ProgressDialog progress;
    private Pattern phonePattern = Pattern.compile("\\d{10}");
    private ChatManager chatmanager;
    private CustomListAdapter<ChatMessage> listArrayAdapter;
    Chat currentChat;
    private PBConnectionManager connManager;
    ImageView send;
    private Connection conn;
    private CustomCursorAdapter listAdapter;
    private Roster roster;
    private ViewHolder holder;
    private ArrayList<ChatMessage> chats;
    private boolean isFromBuilder;
    private boolean isChatStarted;
    private ProfileClass recipientProfile;

    public XMPPChat() {
    }

    private void setAdapter(Cursor data) {
        chats = new ArrayList<>();
        while (data.moveToNext()) {
            ChatMessage msg = new ChatMessage();
            msg.loadFromCursor(data);
            chats.add(msg);
        }
        listArrayAdapter = new CustomListAdapter<>(XMPPChat.this, R.layout.message_layout, chats, this);
        mList.setAdapter(listArrayAdapter);
    }


    @Override

    protected boolean onMenuItemClicked(MenuItem item, int id) {
        switch (id) {
            case R.id.action_view_profile:
                Intent intent1 = new Intent(XMPPChat.this, UserProfileNew.class);
                if (!TextUtils.isEmpty(mRecipientAuthId) && !mRecipientAuthId.equals("null"))
                    intent1.putExtra("authorid", mRecipientAuthId);
                intent1.putExtra("phone", getIntent().getExtras().getString("phone_key").trim());
                startActivity(intent1);
                break;
            case R.id.action_call:
                if (mRecipient != null && mRecipient != "") {
                    Utility.userStats(XMPPChat.this, "call");
                    if (!isFromBuilder) {
                        if (recipientProfile != null) {
                            HashMap<String, Object> wrData = new HashMap<>();
                            wrData.put("ScreenName", TAG);
                            ActivityUtils.initiateCallDialog(XMPPChat.this, recipientProfile, wrData);
                        } else {
                            ActivityUtils.callPhone(XMPPChat.this, mRecipient);
                        }

                    } else {
                        String builderPhone = getIntent().getExtras().getString(Constants.BUNDLE_KEYS.BUILDER_PHONE);
                        if (!TextUtils.isEmpty(builderPhone)) {
                            ActivityUtils.callPhone(XMPPChat.this, builderPhone);
                        } else {
                            ActivityUtils.callPhone(XMPPChat.this, Constants.PLABRO_CUSTOMER_CARE);
                        }
                    }

                } else {
                    showShortToast("No phone number available.");
                }
                break;
        }
        return true;
    }

    @Override
    protected int getMenuIdToInflate() {
        return R.menu.menu_chat;
    }


    @Override
    protected String getTag() {
        return "XMPPChat";
    }

    public static void startChat(Context ctx, String phone, String name, String userId) {
        Intent i = new Intent(ctx, XMPPChat.class);
        Bundle b = new Bundle();
        b.putString("phone_key", phone);
        b.putString("name", name);
        b.putString("userid", userId);
        i.putExtras(b);
        ctx.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Analytics.trackScreen(Analytics.Chat.ConversationView);
        //AppController.getInstance().googleScreenTracker("Chat");
        isFromBuilder = getIntent().getExtras().getBoolean(Constants.BUNDLE_KEYS.IS_FROM_BUILDER, false);
        Util.pringIntentData(getIntent());
        mRecipient = (String) getIntent().getExtras().getString("phone_key").trim();
        mRecipientName = (String) getIntent().getExtras().getString("name").trim();
        AppController.getInstance().setCurrentChatPhoneNumber(ActivityUtils.modifyContact(mRecipient));
        sendChatEvent(mRecipientName, mRecipient);
        Log.d(TAG, "rec:" + mRecipient);
        mRecipient = ActivityUtils.getReceipient(mRecipient);
        to = ActivityUtils.getTo(mRecipient);

        mRecipientAuthId = (String) getIntent().getExtras().getString("userid");
        Log.d(TAG, "Recipient :" + mRecipient + ", AuthId:" + mRecipientAuthId + ", TO :" + to);
        // getSenderProfile();
        findViewById();
        getSavedRecProfile();
        getRecProfile();
        setChatFromFeedContext();
        setPreviousMessageStateToRead();

    }

    private void sendChatEvent(String toUserName, String toPhone) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("senderPhone", PBPreferences.getPhone());
        map.put("imTo", toUserName);
        map.put("recPhone", toPhone);
        CleverTap.recordEventWithProperties(getString(R.string.e_conversation), map);
    }

    private ProfileClass getSavedRecProfile() {

        ProfileClass profileClass = new Select().from(ProfileClass.class).where("phone = '" + mRecipient + "'").executeSingle();

        //insert into db
        if (profileClass == null) return null;
        if (mRecipientName.equalsIgnoreCase(mRecipient)) {
            mRecipientName = profileClass.getName();
            setChatTitle();

        }
        return profileClass;
    }

    private void setPreviousMessageStateToRead() {
        List<ChatMessage> messages = new Select().from(ChatMessage.class).where("status != '" + ChatMessage.STATUS_READ + "' and " + "phone = '" + mRecipient + "' and msgType = '" + ChatMessage.MESSAGE_INCOMING + "'").execute();

        for (ChatMessage msg : messages) {
            if (!msg.getStatus().equals(ChatMessage.STATUS_LOCAL_READ)) {
                sendReadRecipt(msg);
            }
//            msg.setStatus(ChatMessage.STATUS_READ);
//            msg.save();
        }
        setupChatNotification();
        //new Update(ChatMessage.class).set("status=  '" + ChatMessage.STATUS_READ + "'").where("phone = '" + mRecipient + "' and msgType = '"+ChatMessage.MESSAGE_INCOMING+"'").execute();
    }

    private void setupChatNotification() {

        final String unreadMSG = new Select().from(ChatMessage.class).where("status='" + ChatMessage.STATUS_RECEIVED + "' and msgType = " + ChatMessage.MESSAGE_INCOMING).execute().size() + "";
        if (null != unreadMSG && !TextUtils.isEmpty(unreadMSG) && !unreadMSG.equalsIgnoreCase("0")) {
            HomeActivity.setNoOfNotification(Constants.SectionIndex.CHATS, true, unreadMSG);
        } else {
            HomeActivity.setNoOfNotification(Constants.SectionIndex.CHATS, false, unreadMSG);
        }

    }

    private void setChatFromFeedContext() {
        String message = (String) getIntent().getExtras().getString("feed_text");

        if (null != message && !TextUtils.isEmpty(message)) {

            if (getIntent().getBooleanExtra(Constants.BUNDLE_KEYS.IS_CHAT_PREFIX_REQUIRED, true)) {
                message = "Chat regarding\n" + message;
            }

            Bundle b = getIntent().getExtras();
            if (null != b) {
                Feeds feed = (Feeds) b.getSerializable(Constants.BUNDLE_KEY_FEED);
                if (null != feed) {
                    ChatPopUpInfo.feed = feed;
                }
            }

            prepareMessageToSend(message);
            //ActivityUtils.printMatches(message, holder.messageTextRec, XMPPChat.this);
        }
    }

    public void findViewById() {

        inflateToolbar();
        setChatTitle();
        connManager = PBConnectionManager.getInstance(AppController.getInstance(), this);
        conn = connManager.getConnection();
        mSendText = (EditText) this.findViewById(R.id.sendText);
        mList = (ListView) this.findViewById(R.id.listMessages);
//        setAdapter();
//        listAdapter = new CustomCursorAdapter(this, null, false, R.layout.message_layout, this);
//        mList.setAdapter(listAdapter);
        send = (ImageView) this.findViewById(R.id.send);
        send.setOnClickListener(this);
        send.setColorFilter(getResources().getColor(R.color.appColor23));
        mSendText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mSendText.getText().toString().length() > 0) {
                    updateChatStatus(ChatState.composing);
                    send.setColorFilter(getResources().getColor(R.color.colorPrimary));
                    send.setEnabled(true);

                } else {
                    send.setColorFilter(getResources().getColor(R.color.appColor23));
                    send.setEnabled(false);

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        getSupportLoaderManager().initLoader(1, null, this);
        getSupportLoaderManager().initLoader(2, null, this);
    }

    private void setChatTitle() {

        Log.d(TAG, "Recipient Name :" + mRecipientName);
        if (null != mRecipientName && !TextUtils.isEmpty(mRecipientName)) {
            setToolbarTitle(mRecipientName);
            //toolbar.setTitle(mRecipientName);

        } else {
            setToolbarTitle(mRecipient + "");
        }
    }


    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    private void updateChatStatus(ChatState state) {
        try {
            Log.d(TAG, "Setting Current State");
            if (chatStateManager != null && conn != null && conn.isConnected()) {
                Log.d(TAG, "Setting Current State " + state);
                chatStateManager.setCurrentState(state, currentChat);
                Log.d(TAG, "Chat State Thread ID" + currentChat.getThreadID() + "");
            }

        } catch (XMPPException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "View Clicked");
        switch (view.getId()) {
            case R.id.send:
                String msg = mSendText.getText().toString().trim();
                if (msg.replaceAll("\\s+", "") != "") {
                    prepareMessageToSend(msg);
                }
                break;
        }
    }

    void prepareMessageToSend(String msg) {


        ChatMessage m = new ChatMessage();
        m.setMsg(msg);
        m.setPhone(mRecipient);
        m.setMsgType(ChatMessage.MESSAGE_OUTGOING);
        m.setTime(System.currentTimeMillis());

        m.setStatus(ChatMessage.STATUS_PENDING);
        updateChatStatus(ChatState.active);

        sendMessage(m);

    }

    private void sendMessage(ChatMessage msg) {
        String message = (String) getIntent().getExtras().getString("feed_text");

        if (null != message && !TextUtils.isEmpty(message)) {
            ChatPopUpInfo.isChatStarted = true;
        } else {
            ChatPopUpInfo.isChatStarted = false;
        }
        Message m = new Message();
        msg.setPacketId(m.getPacketID());
        msg.setPacketIdAndMessageType(m.getPacketID() + ChatMessage.MESSAGE_OUTGOING);


        Log.d(TAG, "PacketId " + m.getPacketID());
        if (null != conn && conn.isConnected() && msg != null && !TextUtils.isEmpty(msg.getMsg())) {
            try {
                m.setType(Message.Type.chat);
                m.setFrom(conn.getUser());
                String text = msg.getMsg();
                m.setBody(text);
                m.setTo(to);

                m.addExtension(new DeliveryReceipt(m.getPacketID()));

                Log.i(TAG, "*********************************************************Sending text [" + text + "] to [" + to + "]");
                currentChat.sendMessage(m);
                msg.setStatus(ChatMessage.STATUS_SENT);
            } catch (Exception e) {
                if (conn != null && conn.isConnected() && conn.isAuthenticated()) {

                } else {
                    if (Util.haveNetworkConnection(XMPPChat.this)) {
                        connManager.connectInBackground();
                    }
                }
                Log.d(TAG, "Exception Sending Message");
            }
        } else {
            if (Util.haveNetworkConnection(XMPPChat.this)) {
                XMPPConnectionService.startXMPPService(AppController.getInstance());
            }
        }
        mSendText.setText("");
        msg.save();
        //scrollMyListViewToBottom();
    }

    private ProfileClass senderProfile, recProfile;

    private void getSenderProfile() {
        String phone = PBPreferences.getPhone();
        senderProfile = new Select().from(ProfileClass.class).where("phone = '" + phone + "'").executeSingle();
//        if (senderProfile != null && !TextUtils.isEmpty(senderProfile.getImg()))
//            mSenderImage = senderProfile.getImg();
    }

    private void getRecProfile() {

        if (null == mRecipientAuthId) {
            mRecipientAuthId = "";
        }
        HashMap<String, String> params = new HashMap<String, String>();


        // params.put(PlaBroApi.PLABRO_GET_PROFILE_AUTO_PARAM_CONTACT_ID, mRecipientAuthId);
        params.put(PlaBroApi.PLABRO_GET_PROFILE_AUTO_PARAM_PHONE, mRecipient);

        params = Utility.getInitialParams(PlaBroApi.RT.GETPROFILE, params);

        AppVolley.processRequest(0, ProfileResponse.class, null, String.format(PlaBroApi.getBaseUrl(XMPPChat.this)), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {


                Log.d(TAG, "Profile:" + response);
                ProfileResponse profileResponse = (ProfileResponse) response.getResponse();


                ProfileClass profileClass = profileResponse.getOutput_params().getData();

                String phone = "";
                try {
                    phone = profileClass.getUsername().split("@")[0];
                } catch (Exception e) {

                }
                if (TextUtils.isEmpty(phone)) {
                    phone = profileClass.getPhone();
                }
                new Update(ProfileClass.class).set("name = '" + profileClass.getName() + "'").where("phone=?", phone).execute();
                //insert into db
                profileClass.save(); // todo update if exist
                recipientProfile = profileClass;
                if (mRecipientName.equalsIgnoreCase(mRecipient)) {
                    mRecipientName = profileClass.getName();
                    mRecipientName = ActivityUtils.getDisplayNameFromPhone(phone);
                    mRecipientAuthId = profileClass.getAuthorid();
                    setChatTitle();
                }

                // mRecImage = profileClass.getImg();


            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                //setChatTitle();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    private MessageListenerImpl messageListener;
    private ChatStateManager chatStateManager;

    private MessageListenerImpl.ChatStateChangeListener chatStateListener = new MessageListenerImpl.ChatStateChangeListener() {

        @Override
        public void onChatStateChanged(Chat chat, ChatState chatState) {
            Log.d("XMPPChat", "ChatStateChanged");
            String status = "";
            switch (chatState) {
                case active:
                    status = "online";
                    break;
                case composing:
                    Log.d("state", "typing...");
                    status = "typing...";
                    break;
                case paused:
                    Log.d("state", "away");
                    status = "away";
                    break;
                case inactive:
                    Log.d("state", "inactive");
                    status = "inactive";
                    break;
                case gone:
                    Log.d("state", "offline");
                    status = "offline";
                    break;
            }
            Log.d("XMPPChat", "Status is :" + status + " Thread:" + Thread.currentThread().getName());
            final String st = status;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toolbar.setSubtitle(st);
                }
            });
        }
    };

    private RosterManager rosterManager;

    @Override
    public void onConnectionCreated(Connection conn) {
        Log.d(TAG, "Connection Created");
        if (conn != null && conn.isConnected()) {
            try {
                this.conn = conn;
//            roster = conn.getRoster();
//            roster.addRosterListener(rosterListener);
                chatmanager = conn.getChatManager();
                chatStateManager = ChatStateManager.getInstance(conn);

                messageListener = null;
                messageListener = new MessageListenerImpl(XMPPChat.this, chatStateListener, messageUpdateListener);
                currentChat = chatmanager.createChat(to, messageListener);

                chatStateManager.setCurrentState(ChatState.active, currentChat);
                RosterManager.sendPresence(Presence.Type.available);
            } catch (Exception e) {
                e.printStackTrace();
                this.conn = null;
            }

        }
    }


    private void createEntry() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    if (null != to && null != mRecipientName) {
                        RosterManager.createEntryIfNotFound(to, mRecipientName, null);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Error While Creating Roster Entry");
                    e.printStackTrace();
                }
            }
        });
        t.start();


    }

    @Override
    public void onConnectionClosed(Connection conn) {
        Log.d(TAG, "Connection Closed");
        this.conn = null;
        //roster = null;
       /* runOnUiThread(new Runnable() {
            @Override
            public void run() {
                toolbar.setSubtitle("");
            }
        });*/
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case 1:
                return new CursorLoader(this, ContentProvider.createUri(ChatMessage.class, null), null, "phone = '" + mRecipient + "'", null, "time ASC");
            case 2:
                return new CursorLoader(this, ContentProvider.createUri(RosterData.class, null), null, "user = '" + to + "'", null, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG, "Loader iD:" + loader.getId());
        switch (loader.getId()) {
            case 1:
                if (data != null) {
                    // listAdapter.swapCursor(data);
                    setAdapter(data);
                }
                break;
            case 2:
                if (data != null) {

                    if (data.moveToFirst()) {
                        RosterData info = new RosterData();
                        info.loadFromCursor(data);
                        updateToolbarTitle(info.getUserName());
                        if (info.isAvailable()) {
                            Log.d(TAG, "Online tol bar");
                            updateToolbarSubtitle("online");
                        } else {
                            Log.d(TAG, "Offline tol bar");
                            updateToolbarSubtitle("offline");
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        //listAdapter.swapCursor(null);
    }

    @Override
    public void bindView(View view, Context arg1, Cursor c) {


    }

    private void sendReadRecipt(ChatMessage msg) {
        Log.d(TAG, "Sending read receipt");
        if (conn != null && conn.isAuthenticated() && !msg.getStatus().equals(ChatMessage.STATUS_LOCAL_READ) && !msg.getStatus().equals(ChatMessage.STATUS_READ)) {
            try {

                Message message = new Message(to);
                ReadReceipt read = new ReadReceipt(msg.getPacketId());
                message.addExtension(read);
                conn.sendPacket(message);
                if (!msg.getStatus().equals(ChatMessage.STATUS_LOCAL_READ)) {
                    msg.setStatus(ChatMessage.STATUS_LOCAL_READ);
                    msg.save();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(XMPPChat.this).inflate(resourceID, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        ChatMessage msg = chats.get(position);
        String message = msg.getMsg();
        Log.d(TAG, "" + message);


        long time = msg.getTime();
        String msgPhone = msg.getPhone();
        Log.d(TAG, "Message Type :" + msg.getMsgType());

        switch (msg.getMsgType()) {
            case ChatMessage.MESSAGE_INCOMING:

                holder.sentLayout.setVisibility(View.GONE);
                holder.receivedLayout.setVisibility(View.VISIBLE);
                holder.timeRec.setReferenceTime(time);
                //holder.messageTextRec.setText(message);
                holder.receivedLayout.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {

                        ChatMessage msg = ((ChatMessage) v.getTag());
                        MessageOptionDialog messageOptionDialog = new MessageOptionDialog(XMPPChat.this, onChatMessageListener, msg, "reciever");
                        messageOptionDialog.show(getFragmentManager(), "Options");
                        return true;
                    }
                });
                ActivityUtils.printMatches(message, holder.messageTextRec, XMPPChat.this);

//                if (null != mRecImage && !"".equalsIgnoreCase(mRecImage)) {
//                    // holder.profileImageRec.setImage(mRecImage, AppController.getInstance().getImageLoader());
//
//                } else {
//                    //  holder.profileImageRec.setDefaultImageResId(R.drawable.profile_default_one);
//                }
                if (!msg.getStatus().equals(ChatMessage.STATUS_READ) && !msg.getStatus().equals(ChatMessage.STATUS_LOCAL_READ)) {
                    sendReadRecipt(msg);
//                    msg.setStatus(ChatMessage.STATUS_READ);
//                    msg.save();
                }
                break;
            case ChatMessage.MESSAGE_OUTGOING:
                holder.sentLayout.setVisibility(View.VISIBLE);
                holder.receivedLayout.setVisibility(View.GONE);
                holder.timeSent.setReferenceTime(time);
                holder.sentLayout.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        ChatMessage msg = ((ChatMessage) v.getTag());
                        MessageOptionDialog messageOptionDialog = new MessageOptionDialog(XMPPChat.this, onChatMessageListener, msg, "sender");
                        messageOptionDialog.show(getFragmentManager(), "Options");
                        return true;
                    }
                });
                Log.d(TAG, "Status of message :" + msg.getStatus());
                // holder.timeSent.setText(holder.timeSent.getText().toString() + "..." + msg.getStatus());

                //holder.messageTextSen.setText(message);
                ActivityUtils.printMatches(message, holder.messageTextSen, XMPPChat.this);
                int statusDrawableId = R.drawable.ic_action_pending;
                switch (msg.getStatus()) {
                    case ChatMessage.STATUS_PENDING:
                    case ChatMessage.STATUS_NONE:
                    case "":
                        statusDrawableId = R.drawable.ic_action_pending;
                        break;
                    case ChatMessage.STATUS_DELIVERED:
                        statusDrawableId = R.drawable.ic_action_delivered;
                        break;
                    case ChatMessage.STATUS_READ:
                        statusDrawableId = R.drawable.ic_action_read;
                        break;
                    case ChatMessage.STATUS_SENT:
                        statusDrawableId = R.drawable.ic_action_sent;
                        break;
                }
                holder.statusIcon.setImageResource(statusDrawableId);
//                if (null != mSenderImage && !"".equalsIgnoreCase(mSenderImage)) {
//                    // holder.profileImageSen.setImage(mSenderImage, AppController.getInstance().getImageLoader());
//
//                } else {
//                    // holder.profileImageSen.setDefaultImageResId(R.drawable.profile_default_one);
//                }
                break;
        }

        holder.sentLayout.setTag(msg);
        holder.receivedLayout.setTag(msg);
        return convertView;
    }


    private static class ViewHolder {
        // MyNetworkImageView profileImageSen;
        TextView messageTextSen;
        //  MyNetworkImageView profileImageRec;
        TextView messageTextRec;
        RelativeLayout sentLayout, receivedLayout;
        RelativeTimeTextView timeSent, timeRec;
        ImageView statusIcon;

        ViewHolder(View view) {
            // profileImageRec = (MyNetworkImageView) view.findViewById(R.id.profileImageRec);
            messageTextRec = (TextView) view.findViewById(R.id.messageTextRec);
            receivedLayout = (RelativeLayout) view.findViewById(R.id.received);

            //  profileImageSen = (MyNetworkImageView) view.findViewById(R.id.profileImageSen);
            messageTextSen = (TextView) view.findViewById(R.id.messageTextSen);
            sentLayout = (RelativeLayout) view.findViewById(R.id.sent);
            timeSent = (RelativeTimeTextView) view.findViewById(R.id.timestampSen);
            timeRec = (RelativeTimeTextView) view.findViewById(R.id.timestampRec);
            statusIcon = (ImageView) view.findViewById(R.id.iv_statsImage);
        }
    }

    @Override
    public void onBackPressed() {
        //Crittercism.endTransaction("Chat");
        setResult(RESULT_OK);
        super.onBackPressed();
    }

    OnChatMessageListener onChatMessageListener = new OnChatMessageListener() {
        @Override
        public void onSelected(Boolean response, String type, ChatMessage msg) {

            if (response) {
                switch (type) {
                    case "Copy":

                        ActivityUtils.copyText(msg.getMsg(), XMPPChat.this);

                        break;
                    case "Delete":

                        new Delete().from(ChatMessage.class).where("_id='" + msg.get_id() + "'").execute();

                        break;
                    case "Resend":
                        prepareMessageToSend(msg.getMsg());
                        break;

                }

            }

        }
    };

    private void scrollMyListViewToBottom() {
        mList.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...

                mList.setSelection(listAdapter.getCount());
                Log.i(TAG, "Chat counter on scroll to bottom " + listAdapter.getCount());
            }
        });
        setResult(RESULT_OK);
    }

    OnUpdateListener messageUpdateListener = new OnUpdateListener() {
        @Override
        public void onResponse(Boolean response, String val) {
        }

        @Override
        public void onResponse(Boolean response, int val) {
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (currentChat != null) {
            currentChat.removeMessageListener(messageListener);
        }
        connManager.removeConnectionListener(this);
        roster = null;
        AppController.getInstance().setCurrentChatPhoneNumber(null);
        RosterManager.sendPresence(Presence.Type.unavailable);
    }


    private void updateToolbarSubtitle(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setToolbarSubTitle(status);
            }
        });
    }

    private void updateToolbarTitle(final String title) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setToolbarTitle(title);
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "On Config changed");
    }
}
