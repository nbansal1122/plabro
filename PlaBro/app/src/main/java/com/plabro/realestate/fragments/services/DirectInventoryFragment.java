package com.plabro.realestate.fragments.services;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.google.ads.conversiontracking.AdWordsConversionReporter;
import com.nhaarman.supertooltips.ToolTip;
import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.HomeActivity;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.adapter.FeedsCustomAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.fragments.CardListFragment;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.Tracker.Campaign;
import com.plabro.realestate.models.chat.ChatPopUpInfo;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.DirectInventory;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.LoaderFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.receivers.PBLocalReceiver;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.AppVolleyCacheManager;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by nitin on 18/01/16.
 */
public class DirectInventoryFragment extends CardListFragment {

    public static final String FEED_TYPE = "feed_type";
    FeedsCustomAdapter adapter;
    boolean addMoreGlobal = false;
    private String feedType = "all";

    private Toolbar toolbar;
    private SearchBoxCustom searchBoxCustom;
    private String searchText;
    private boolean isSearched = false;


    @Override
    public String getTAG() {
        return "FeedsFragment";
    }

    public static Fragment getInstance(Bundle b, Toolbar toolbar, SearchBoxCustom searchBoxCustom) {
        DirectInventoryFragment f = new DirectInventoryFragment();
        f.setArguments(b);
        f.toolbar = toolbar;
        f.searchBoxCustom = searchBoxCustom;
        return f;
    }


    @Override
    public FeedsCustomAdapter getAdapter() {
        if (null == adapter) {
            adapter = new FeedsCustomAdapter(baseFeeds, getActivity(), "Feeds");
        }
        return adapter;
    }

    @Override
    protected void loadBundleData(Bundle b) {
        feedType = b.getString(FEED_TYPE);
    }

    @Override
    public void getData(final boolean addMore) {
        super.getData(addMore);
        Log.d(TAG, "Ge Data");
        addMoreGlobal = addMore;
        if (addMoreGlobal && baseFeeds.size() > 0) {
            if (baseFeeds.get(baseFeeds.size() - 1).getType().equalsIgnoreCase(Constants.FEED_TYPE.NO_MORE_DATA)) {
                return;
            }
        }
        Log.d(TAG, "Ge Data " + addMore);

        showSpinners(addMore);
        HashMap<String, String> params = new HashMap<>();
        if (addMore) {
            params.put("startidx", "" + page);
        } else {
            params.put("startidx", "0");
        }
        params.put(FEED_TYPE, feedType);


        int taskCode = Constants.TASK_CODES.DIRECT_LISTING_FEEDS;
//        params.put(CITY_ID, TextUtils.isEmpty(city_id) ? "1" : city_id);
        if (TextUtils.isEmpty(searchText)) {

            params = Utility.getInitialParams(PlaBroApi.RT.DIRECT_LISTING_FEEDS, params);
        } else {
            params.put("q_str", searchText);
            params.put("s_type", "direct_listing");
            taskCode = Constants.TASK_CODES.SEARCH_AUCTION;
            params = Utility.getInitialParams(PlaBroApi.RT.SEARCH, params);
        }


        AppVolley.processRequest(taskCode, FeedResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                // Util.showSnackBarMessage(getActivity(),getActivity(),"Feeds fetched successfully");
                if (null == getActivity()) return;
                stopSpinners();

                FeedResponse feedResponse = (FeedResponse) response.getResponse();
                if (null != feedResponse.getOutput_params()) {
                    noFeeds.setVisibility(View.GONE);
                    ArrayList<BaseFeed> tempFeeds = (ArrayList<BaseFeed>) feedResponse.getOutput_params().getData();
                    if (tempFeeds.size() > 0) {

                        LoaderFeed lf = new LoaderFeed();
                        if (tempFeeds.size() < Util.getDefaultStartIdx()) {
                            lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                            lf.setText("listings");
                        } else {
                            lf.setType(Constants.FEED_TYPE.PROGRESS_MORE);
                        }

                        if (addMore) {
                            baseFeeds.remove(baseFeeds.size() - 1);//for footer
                            baseFeeds.addAll(tempFeeds);
                            baseFeeds.add(lf);
                            adapter.notifyDataSetChanged();
                        } else {
                            page = 0;
                            baseFeeds.clear();
                            baseFeeds.addAll(tempFeeds);
                            baseFeeds.add(lf);
                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            }
                        }
                        page = Util.getStartIndex(page);
                        //page scrolled for next set of data


                    } else {
                        if (!addMore) {
                            comingSoon = true;
                        }

                        if (addMore) {
                            LoaderFeed lf = new LoaderFeed();
                            baseFeeds.remove(baseFeeds.size() - 1);//for footer
                            lf.setText("listings");
                            lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                            baseFeeds.add(lf);
                            adapter.notifyDataSetChanged();
                        }
                    }
                }
                showNoFeeds();
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                if (null == getActivity()) return;
                onApiFailure(response, taskCode);
                stopSpinners();
                try {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 2000ms
                            LoaderFeed lf = new LoaderFeed();
                            lf.setType(Constants.FEED_TYPE.NO_MORE);

                            if (addMore) {
                                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                                baseFeeds.add(lf);
                                adapter.notifyDataSetChanged();

                            }
                        }
                    }, 2000);
                    showNoFeeds();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    protected void showNoFeeds() {
        Log.d(TAG, "Show No feeds:" + baseFeeds.size());
        if (baseFeeds.size() < 1) {
            noFeeds.setVisibility(View.VISIBLE);
            if ("all".equalsIgnoreCase(feedType)) {
                setText(R.id.tv_no_feeds, getString(R.string.no_listing));
            } else {
                setText(R.id.tv_no_feeds, getString(R.string.no_purchased_listing));
            }
        }
    }

    @Override
    protected int getViewId() {
        return R.layout.feeds_fragment;
    }


    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerLocalReceiver();
        // Login login = (Login) new Select().from(Login.class).executeSingle();
        //Log.d(TAG,"Authkey "+login.getAuthkey());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    protected void reloadRequest() {
        showSpinners(addMoreGlobal);
        getData(false);
    }


    @Override
    public IntentFilter getFilter() {
        filter = super.getFilter();
        filter.addAction(PBLocalReceiver.PBActionListener.ACTION_REFRESH_LISTING);
        return filter;
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Received INtent Action :" + intent.getAction());
        if (PBLocalReceiver.PBActionListener.ACTION_REFRESH_LISTING.equals(intent.getAction())) {
            getData(false);
        }
    }

    @Override
    protected void findViewById() {
        super.findViewById();
        shout.setVisibility(View.GONE);
        scroll.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        if ("all".equalsIgnoreCase(feedType)) {
            Analytics.trackScreen(Analytics.DirectListing.AllListings);
        } else {
            Analytics.trackScreen(Analytics.DirectListing.PurchasedListings);
        }
    }


    // ======== Search

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (toolbar == null) {
            toolbar = (Toolbar) getActivity().findViewById(R.id.my_awesome_toolbar);
        }
        if (searchBoxCustom == null) {
            searchBoxCustom = (SearchBoxCustom) getActivity().findViewById(R.id.searchbox);
        }
//        toolbar.getMenu().clear();
        inflateToolbar();
//        super.onCreateOptionsMenu(menu, inflater);
    }

    public void openSearch(final int id) {
        //toolbar.setTitle("");
        searchBoxCustom.enableVoiceRecognition(this);
        searchBoxCustom.revealFromMenuItem(R.id.action_search, getActivity());
        searchBoxCustom.setMenuListener(new SearchBoxCustom.MenuListener() {

            @Override
            public void onMenuClick() {
                if (searchBoxCustom.isShown()) {
                    searchBoxCustom.hideCircularly(getActivity());
                }

            }

        });
        searchBoxCustom.setSearchListener(new SearchBoxCustom.SearchListener() {


            @Override
            public void onSearchOpened() {
                Log.d(TAG, "On Search Opened");
            }

            @Override
            public void onSearchClosed() {
                // Use this to un-tint the screen
                Log.d(TAG, "On Search Closed");
                getData(false);
            }

            @Override
            public void onSearchTermChanged(String term) {
                // React to the search term changing
                // Called after it has updated results
                Log.d(TAG, "On Search Term Changed");
            }


            @Override
            public void onSearch(String searchTerm) {
                searchText = searchTerm.trim();
                getData(false);
//                searchBoxCustom.clearResults();
                searchBoxCustom.hideResults();
//                searchBoxCustom.dismissDropDown();
//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                searchText = null;
            }

            @Override
            public void onSearchCleared() {
                Log.d(TAG, "On Search Cleared");
            }

        });

    }

    private void inflateToolbar() {
        Log.v(TAG, "toolbar inflated");
        toolbar.inflateMenu(R.menu.menu_auction);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id) {

                    case R.id.action_search:
                        openSearch(id);
                        break;


                }
                return true;
            }

        });
    }


}
