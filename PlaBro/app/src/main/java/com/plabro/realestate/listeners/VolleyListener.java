package com.plabro.realestate.listeners;

import com.plabro.realestate.volley.PBResponse;

/**
 * Created by jmd on 6/10/2015.
 */
public interface VolleyListener {
    int VOLLEY_EXCEPTION = 0;
    int JSON_PARSE_EXCEPTION = 1;
    int SESSION_EXPIRED = 2;
    int UNKNOWN_EXCEPTION=3;
    int NO_DATA_EXCEPTION = 4;
    int NO_INETERNET_EXCEPTION = 5;
    int TRIAL_EXPIRED_EXCEPTION = 6;
    int STATUS_FALSE_EXCEPTION = 7;
    public void onSuccessResponse(PBResponse response, int taskCode);

    public void onFailureResponse(PBResponse response, int taskCode);
}
