package com.plabro.realestate.models.securitykeysharing;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class KeySharingClass {

    private String key = "";
    private String signature = "";

    public String getKey() {
        return key;
    }

    public String getSignature() {
        return signature;
    }
}
