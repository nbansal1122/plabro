package com.plabro.realestate.widgets;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.animation.Animation;
import android.view.animation.Interpolator;

public class WaveDrawable extends Drawable {

    private Paint wavePaint;
    private int color;
    private int radius;
    private long animationTime = 2000;

    protected float waveScale;
    protected int alpha;


    private Interpolator waveInterpolator;
    private Interpolator alphaInterpolator;

    private Animator animator;
    private AnimatorSet animatorSet;

    /**
     * @param color         color
     * @param radius        radius
     * @param animationTime time
     */
    public WaveDrawable(int color, int radius, long animationTime) {
        this(color, radius);
        this.animationTime = animationTime;
    }

    /**
     * @param color  colro
     * @param radius radius
     */
    public WaveDrawable(int color, int radius) {
        this.color = color;
        this.radius = radius;
        this.waveScale = 0f;
        this.alpha = 127;

        wavePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        animatorSet = new AnimatorSet();

    }

    @Override
    public void draw(Canvas canvas) {

        final Rect bounds = getBounds();

        // circle
        wavePaint.setStyle(Paint.Style.FILL);
        wavePaint.setColor(color);
        wavePaint.setAlpha(alpha);
        canvas.drawCircle(bounds.centerX(), bounds.centerY(), radius * waveScale, wavePaint);

    }

    /**
     * @param interpolator interpolator
     */
    public void setWaveInterpolator(Interpolator interpolator) {
        this.waveInterpolator = interpolator;
    }

    /**
     * @param interpolator interpolator
     */
    public void setAlphaInterpolator(Interpolator interpolator) {
        this.alphaInterpolator = interpolator;
    }

    public void startAnimation() {
        animator = generateAnimation();
        animator.start();
    }

    public void stopAnimation() {
        if (animator.isRunning()) {
            animator.end();
        }
    }


    public boolean isAnimationRunning() {
        if (animator != null) {
            return animator.isRunning();
        }
        return false;
    }

    @Override
    public void setAlpha(int alpha) {
        this.alpha = alpha;
        invalidateSelf();
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        wavePaint.setColorFilter(cf);
    }

    @Override
    public int getOpacity() {
        return wavePaint.getAlpha();
    }


    protected void setWaveScale(float waveScale) {
        this.waveScale = waveScale;
        invalidateSelf();
    }

    protected float getWaveScale() {
        return waveScale;
    }

    private Animator generateAnimation() {

        //Wave animation
        ObjectAnimator waveAnimator = ObjectAnimator.ofFloat(this, "waveScale", 0.7f, 1f);
        waveAnimator.setDuration(animationTime);
        if (waveInterpolator != null) {
            waveAnimator.setInterpolator(waveInterpolator);
        }
        //The animation is repeated
        waveAnimator.setRepeatCount(Animation.INFINITE);
        waveAnimator.setRepeatMode(Animation.INFINITE);


        //alpha animation
        ObjectAnimator alphaAnimator = ObjectAnimator.ofInt(this, "alpha", 255, 127);
        alphaAnimator.setDuration(animationTime);
        if (alphaInterpolator != null) {
            alphaAnimator.setInterpolator(alphaInterpolator);
        }
        alphaAnimator.setRepeatCount(Animation.INFINITE);
        alphaAnimator.setRepeatMode(Animation.INFINITE);


        ObjectAnimator waveBackAnimator = ObjectAnimator.ofFloat(this, "waveScale", 1.0f, 0.7f);
        waveBackAnimator.setDuration(animationTime);
        if (waveInterpolator != null) {
            waveBackAnimator.setInterpolator(waveInterpolator);
        }
        //The animation is repeated
        waveBackAnimator.setRepeatCount(Animation.INFINITE);
        waveBackAnimator.setRepeatMode(Animation.INFINITE);

        ObjectAnimator alphaBackAnimator = ObjectAnimator.ofInt(this, "alpha", 127, 255);
        alphaBackAnimator.setDuration(animationTime);
        if (alphaInterpolator != null) {
            alphaBackAnimator.setInterpolator(alphaInterpolator);
        }
        alphaBackAnimator.setRepeatCount(Animation.INFINITE);
        alphaBackAnimator.setRepeatMode(Animation.INFINITE);

        AnimatorSet s1 = new AnimatorSet();
        AnimatorSet s2 = new AnimatorSet();
        // s2.setStartDelay(1000);
        s1.playTogether(waveAnimator, alphaAnimator);
        s2.playTogether(waveBackAnimator, alphaBackAnimator);
        animatorSet.playSequentially(s1,s2);
        //animatorSet.playTogether(waveAnimator, alphaAnimator);
        return animatorSet;
    }
}
