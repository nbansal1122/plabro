package com.plabro.realestate.fragments.services;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.widget.LinearLayout;

import com.plabro.realestate.R;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.models.services.Question;
import com.plabro.realestate.others.Analytics;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 21/12/15.
 */
public class ParentServiceFragment extends PlabroBaseFragment {
    private List<Question> questions;
    private List<BaseServiceFragment> fragments = new ArrayList<>();
    private LinearLayout layout;

    public static ParentServiceFragment getInstance(List<Question> questions) {
        ParentServiceFragment f = new ParentServiceFragment();
        f.questions = questions;
        return f;
    }

    @Override
    public String getTAG() {
        return "ParentServiceFragment";
    }



    @Override
    protected void findViewById() {
        layout = (LinearLayout) findView(R.id.ll_parent_service);
        for (Question q : questions) {
            BaseServiceFragment f = BaseServiceFragment.getServiceFragment(q);
            fragments.add(f);
            addFragment(f);
        }
    }

    private void addFragment(BaseServiceFragment f) {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.add(R.id.ll_parent_service, f);
        ft.commit();
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_parent_service;
    }

    public boolean onNextClicked() {
        boolean flag = true;
        for (BaseServiceFragment f : fragments) {
            flag = flag && f.onNextClicked();
        }
        return flag;
    }

    public JSONObject getServiceResponseArray() throws Exception{
        JSONObject object = new JSONObject();
        for (BaseServiceFragment f : fragments) {
            object.put(f.q.getQ_id(), f.getServiceResponse());
        }
        return object;
    }
}
