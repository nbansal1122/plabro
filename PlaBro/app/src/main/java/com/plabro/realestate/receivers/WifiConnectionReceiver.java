package com.plabro.realestate.receivers;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;

import com.activeandroid.query.Select;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.Tracker.WifiData;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlabroLocationManagerTwo;
import com.plabro.realestate.utilities.Utility;

import java.util.List;

/**
 * Created by jmd on 5/13/2015.
 */
public class WifiConnectionReceiver extends BroadcastReceiver {
    private static final String TAG = "WifiConnectionReceiver";
    private static final int TRACKER_INTERVAL_IN_MIN = 5;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "isUserLogged In :" + PBPreferences.getLoggedInState());
        if (AppController.getInstance().hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            if (PBPreferences.getLoggedInState() && PBPreferences.getData(PBPreferences.IS_WIFI_ENABLED, true)) {
                int distance = PBPreferences.getLastKnownDistance();
                String lat = PBPreferences.getLastKnownLat();
                String lng = PBPreferences.getLastKnownLong();
                long timestamp = PBPreferences.getLocationUpdateTimestamp();
                Location prevLoc = new Location(LocationManager.GPS_PROVIDER);
                Location currentLoc = PlabroLocationManagerTwo.getInstance(context.getApplicationContext()).getLastKnownLocation();
                if (lat == null || lng == null || lat.equalsIgnoreCase("") || lng.equalsIgnoreCase("")) {
                    Log.d(TAG, "Lat Lng are empty or null ");
                    lat = lng = "0";
                } else {
                    prevLoc.setLatitude(Double.parseDouble(lat));
                    prevLoc.setLongitude(Double.parseDouble(lng));

                }

                if (currentLoc != null) {
                    if (prevLoc != null)
                        distance = Math.round(prevLoc.distanceTo(prevLoc));
                    lat = String.valueOf(currentLoc.getLatitude());
                    lng = String.valueOf(currentLoc.getLongitude());
                    timestamp = System.currentTimeMillis();

                    PBPreferences.setLastKnownDistance(distance);
                    PBPreferences.setLastKnownLat(lat);
                    PBPreferences.setLastKnownLong(lng);
                    PBPreferences.setLocationUpdateTimestamp(timestamp);
                }

//            long trackerTime = PBPreferences.getData(context, PBPreferences.TRACKER_TIME, 0);
//            long currentTime = System.currentTimeMillis();
                // Current Time should be greater than the tracker time
                // by <TRACKER_INTERVAL_IN_MIN> i.e by 5 min
                // if (trackerTime == 0 || trackerTime < System.currentTimeMillis()) {
                Log.d(TAG, "" + intent.getAction());
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                List<ScanResult> scanResults = wifiManager.getScanResults();

                try {

                    if (scanResults != null && scanResults.size() > 0) {
                        int count = new Select().from(WifiData.class).count();
                        if (count >= 50) {
                            return;
                        }
                        for (ScanResult scan : scanResults) {
                            WifiData data = new WifiData();
                            data.setBssid(scan.BSSID);
                            data.setSsid(scan.SSID);
                            data.setLevel(scan.level);
                            data.setLat(Double.parseDouble(lat));
                            data.setLng(Double.parseDouble(lng));
                            data.setDistance(distance);
                            data.setTimestamp(timestamp);
                            data.saveData();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utility.trackException(e);
                }
            }
        } else {
            PBPreferences.saveData(PBPreferences.REQ_LOCATION_PERMISSION, true);
        }
    }


}
