package com.plabro.realestate.utilities;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.HomeActivity;
import com.plabro.realestate.R;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.models.propFeeds.GreetingFeed;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by Hemant on 5/14/2015.
 */
public class PlabroPushNotificationManager {

    public static int sendLocalPlabroPush(Context context, String title, String msg, boolean onGoing, int notifyId) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.putExtra("new_im", "true");
        intent.putExtra("act", "gen");
        intent.putExtra("msg", msg);
        intent.putExtra("title", title);
        intent.putExtra("fromNotification", true);
        return sendNotification(context, msg, title, intent, onGoing, notifyId);
    }

    public static int sendNotification(Context context, String msg, String title, Intent intent, boolean onGoing, int notifyId) {

        if (notifyId == -1) {
            notifyId = (int) System.currentTimeMillis();
        }

        PendingIntent contentIntent = PendingIntent.getActivity(context, notifyId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder b = new NotificationCompat.Builder(context);

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

        } else {
            b.setColor(context.getResources().getColor(R.color.colorPrimary));
        }
        b.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.notif)
                .setTicker("Plabro Message")
                .setContentTitle(title)
                .setOngoing(onGoing)
                .setContentText(msg)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setContentIntent(contentIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notifyId, b.build());
        Log.d("", "PUSH:Sending notif initiated");


        return notifyId;
    }

    public static int sendLocalPlabroPush(Context context, String title, String msg, String phone, String displayName, boolean onGoing, int notifyId) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.putExtra("new_im", "true");
        intent.putExtra("fromNotification", true);
        intent.putExtra("act", "im");
        intent.putExtra("msg", msg);
        intent.putExtra("title", title);
        intent.putExtra("phone_key", phone);
        intent.putExtra("name", displayName);
        if (null != msg && !msg.equalsIgnoreCase("")) {
            return sendNotification(context, msg, title, intent, onGoing, notifyId);
        }

        return -1;
    }

    public static void cancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId);
    }


    public static void showPictureNotification(final Context ctx, GreetingFeed feed) {
        Looper.getMainLooper().getThread();
        PlabroIntentService.showPushNotification(ctx, feed);

    }


}
