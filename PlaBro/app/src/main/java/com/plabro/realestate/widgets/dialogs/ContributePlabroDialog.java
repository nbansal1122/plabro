package com.plabro.realestate.widgets.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.plabro.realestate.R;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;

/**
 * Created by Hemant on 19-01-2015.
 */
public class ContributePlabroDialog extends android.support.v4.app.DialogFragment {

    private ImageButton mWhatsapp,mGmail;
    private Button mClose;
    private Context mContext;

    @SuppressLint("ValidFragment")
    public ContributePlabroDialog(Activity mActivity) {
        this.mContext = mActivity.getApplicationContext();
    }

    public ContributePlabroDialog() {

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.contribute_plabro, null);
        builder.setView(convertView);
        setCancelable(true);
        mWhatsapp = (ImageButton) convertView.findViewById(R.id.ib_whatsapp);
        mGmail = (ImageButton) convertView.findViewById(R.id.ib_gmail);

        mGmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // send email
                //Analytics.trackScreen(R.string.MAIL_CONTRIBUTE);
                PBSharingManager.sendMail(getActivity(), Constants.UPLOAD_MAIL, "Contribute to plabro :"+ PBPreferences.getPhone(), "","Contribute to plabro");


            }
        });

        mWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Analytics.trackScreen(R.string.watsapp_contribute);
                PBSharingManager.shareByWhatsapp(view,getActivity(),"");
            }
        });
        mClose = (Button) convertView.findViewById(R.id.close);
        setUpContributionOptions();
        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return builder.create();
    }


    private void setUpContributionOptions() {



    }

}
