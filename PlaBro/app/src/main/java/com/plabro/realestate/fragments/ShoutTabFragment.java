package com.plabro.realestate.fragments;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.HomeActivity;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomPagerAdapter;
import com.plabro.realestate.entities.MyDraftsDBModel;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.slidingTab.SlidingTabLayout;
import com.plabro.realestate.uiHelpers.view.FontManager;

import java.util.ArrayList;

/**
 * Created by nitin on 02/12/15.
 */
public class ShoutTabFragment extends PlabroBaseFragment implements CustomPagerAdapter.PagerAdapterInterface<String>, ViewPager.OnPageChangeListener {
    private ViewPager mViewPager;
    private ArrayList<String> fragmentNames = new ArrayList<>();
    private CustomPagerAdapter<String> adapter;
    private static Toolbar mToolbar;
    private static SearchBoxCustom search;
    private static final String TYPEFACE_ROBOTTO_NORMAL = "Fonts/Roboto-Regular.ttf";
    private RelativeLayout mDraftsOptionHeader;
    private SlidingTabLayout mSlidingTabLayout;
    private static final String DRAFTS = "Drafts";
    private static final String SHOUTS = "Shouts";
    private ShoutsFragment shoutsFragment;
    private DraftsFragment draftsFragment;

    public static ShoutTabFragment getInstance(Toolbar toolbar, SearchBoxCustom searchBoxCustom, RelativeLayout mDraftsOptionHeader) {
        ShoutTabFragment f = new ShoutTabFragment();
        mToolbar = toolbar;
        search = searchBoxCustom;
        f.mDraftsOptionHeader = mDraftsOptionHeader;
        return f;
    }

    @Override
    public String getTAG() {
        return "Notif n Calls";
    }

    @Override
    protected void findViewById() {
        initFragmentNames();
        setPagerAdapter();
        setTabTextView();
    }

    private void initFragmentNames() {
        fragmentNames.clear();
        fragmentNames.add(SHOUTS);
    }

    private void setPagerAdapter() {
        if (null == getActivity()) return;
        mSlidingTabLayout = (SlidingTabLayout) findView(R.id.navig_tab);
        mSlidingTabLayout.setVisibility(View.VISIBLE);
        mViewPager = (ViewPager) findView(R.id.pager);
        adapter = new CustomPagerAdapter<>(getChildFragmentManager(), fragmentNames, this);
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(this);
        mSlidingTabLayout.setViewPager(mViewPager);
        if (fragmentNames.size() < 2) {
            mSlidingTabLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected int getViewId() {
        return R.layout.activity_search_filter;
    }

    @Override
    public Fragment getFragmentItem(int position, String listItem) {
        Fragment f = null;
        switch (position) {
            case 0:
                if (null == shoutsFragment) {
                    shoutsFragment = ShoutsFragment.getInstance(getActivity(), mToolbar, search);
                }
                return shoutsFragment;
            case 1:
                if (null == draftsFragment) {
                    draftsFragment = DraftsFragment.getInstance(getActivity(), mToolbar, search, mDraftsOptionHeader);
                }
                return draftsFragment;
        }
        return f;
    }

    @Override
    public CharSequence getPageTitle(int position, String listItem) {
        return listItem;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    private void setTabTextView() {
        int count = mSlidingTabLayout.getTabStrip().getChildCount();
        int currentPosition = mViewPager.getCurrentItem();
        for (int i = 0; i < count; i++) {
            TextView v = (TextView) mSlidingTabLayout.getTabStrip().getChildAt(i);
            Typeface tf = FontManager.getInstance().getFont(TYPEFACE_ROBOTTO_NORMAL);
            v.setTypeface(tf, Typeface.BOLD);
            if (i == currentPosition)
                v.setTextColor(getResources().getColor(R.color.white));
            else
                v.setTextColor(getResources().getColor(R.color.tab_text_blue));
        }
    }

    @Override
    public void onPageSelected(int position) {
        Log.d(TAG, "" + position);
        if (position == 1) {
        }
        mViewPager.setCurrentItem(position);
        setTabTextView();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (isDraftsToBeShown()) {
            if (fragmentNames.size() < 2) {
                fragmentNames.add(DRAFTS);
                setPagerAdapter();
                setTabTextView();
            }
        } else {
            if (fragmentNames.size() > 1) {
                fragmentNames.remove(1);
                setPagerAdapter();
                setTabTextView();
            }
        }
    }

    private boolean isDraftsToBeShown() {
        int count = new Select().from(MyDraftsDBModel.class).count();
//        HomeActivity.setNoOfNotification();
        return count > 0;
    }
}
