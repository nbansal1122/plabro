package com.plabro.realestate.utilities;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;

/**
 * Created by hemant on 25/4/15.
 */
public class MyContactsContentObserver extends ContentObserver {

    /**
     * Creates a content observer.
     *
     * @param handler The handler to run {@link #onChange} on, or null if none.
     */
    public MyContactsContentObserver(Handler handler) {
        super(handler);
    }

    @Override
    public void onChange(boolean selfChange) {
        this.onChange(selfChange, null);
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        // do s.th.
        // depending on the handler you might be on the UI
        // thread, so be cautious!
    }
}
