package com.plabro.realestate.fragments.community;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.CommunityActivities.CreateCommunity;
import com.plabro.realestate.activity.CommunityActivities.JoinCommunity;
import com.plabro.realestate.activity.FragmentContainer;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.community.CommunityResponse;
import com.plabro.realestate.models.community.JoinedComm;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.software.shell.fab.ActionButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nbansal2211 on 16/05/16.
 */
public class CommunityFragment extends PlabroBaseFragment implements CustomListAdapter.CustomListAdapterInterface, AdapterView.OnItemClickListener {
    private ListView listView;
    private CustomListAdapter<JoinedComm> adapter;
    private List<JoinedComm> communityList = new ArrayList<>();
    private Toolbar toolBar;
    private SearchBoxCustom searchBoxCustom;

    @Override
    public String getTAG() {
        return "CommunityFragment";
    }

    @Override
    protected void findViewById() {
        listView = (ListView) findView(R.id.listView);
        adapter = new CustomListAdapter<>(getActivity(), R.layout.row_community, communityList, this);
        listView.setAdapter(adapter);
        fetchAllCommunities();
        ActionButton mShoutButton = (ActionButton) findView(R.id.rl_post_iv);
        Utility.setupShoutActionButton(mShoutButton, getActivity());
        setClickListeners(R.id.rl_post_iv);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.rl_post_iv:
                startActivity(new Bundle(), CreateCommunity.class);
                break;
        }
    }

    private void fetchAllCommunities() {
        ParamObject obj = new ParamObject();
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.USER_COMMUNITIES, null));
        obj.setClassType(CommunityResponse.class);
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "Community Response : " + response.getResponse());
                CommunityResponse res = (CommunityResponse) response.getResponse();
                if(res != null && res.getOutput_params() != null && res.getOutput_params().getData() != null){
                    communityList.clear();
                    List<JoinedComm> comms = res.getOutput_params().getData().getWaitingComms();
                    for(JoinedComm comm : comms){
                        comm.setWaiting(true);
                        communityList.add(comm);
                    }
                    communityList.addAll(res.getOutput_params().getData().getJoinedComms());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "Failure Community Response Message: " + response.getCustomException().getMessage());
            }
        });
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_community;
    }

    public static CommunityFragment getInstance(Toolbar toolbar, SearchBoxCustom searchbox) {
        CommunityFragment f = new CommunityFragment();
        f.toolBar = toolbar;
        f.searchBoxCustom = searchbox;
        return f;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        ViewHolder holder = null;
        if(convertView == null){
            convertView = LayoutInflater.from(getActivity()).inflate(resourceID, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        JoinedComm community = communityList.get(position);
        holder.communityName.setText(community.getGroupInfo().getName());
        holder.communityDesc.setText(community.getGroupInfo().getDesc());
        ActivityUtils.loadImage(getActivity(), community.getGroupInfo().getImage(), holder.logo, R.drawable.ic_launcher);

        if(community.isWaiting()){
            holder.join.setVisibility(View.VISIBLE);
            holder.arrow.setVisibility(View.GONE);
        }else{
            holder.join.setVisibility(View.GONE);
            holder.arrow.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        JoinedComm community = communityList.get(position);
        Bundle b = new Bundle();
        b.putSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE, community);
        if(community.isWaiting()){
            JoinCommunity.startActivity(getActivity(), community);
        }else {
            FragmentContainer.startActivity(getActivity(), Constants.FRAGMENT_TYPE.COMMUNITY_SHOUTS_USERS, b);
        }
    }

    private class ViewHolder {
        private FontText communityName, communityDesc, join;
        private ImageView logo, arrow;

        public ViewHolder(View v) {
            communityName = (FontText) v.findViewById(R.id.tv_community_name);
            communityDesc = (FontText) v.findViewById(R.id.tv_community_desc);
            join = (FontText) v.findViewById(R.id.tv_join);
            logo = (ImageView) v.findViewById(R.id.iv_icon_community);
            arrow = (ImageView) v.findViewById(R.id.iv_right_arrow);
        }
    }
}
