package com.plabro.realestate.fragments.related;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.HomeActivity;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.FeedDetails;
import com.plabro.realestate.adapter.RelatedCustomAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.LoaderFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by hemant on 28/07/15.
 */
public class RelatedFeedsFragment extends BaseRelatedFragment {
    //    private RecyclerView mRecyclerView;
    public static final String TAG = "RelatedFeeds";


    public static RelatedFeedsFragment getInstance(Context mContext, String msgId, int headerHeight) {
        //  Log.i(TAG, "Creating New Instance");
        RelatedFeedsFragment mRelatedFeeds = new RelatedFeedsFragment();
        mRelatedFeeds.mContext = mContext;
        mRelatedFeeds.message_id = msgId;
        mRelatedFeeds.headerHeight = headerHeight;
        return mRelatedFeeds;
    }

    @Override
    protected int getViewId() {
        return R.layout.frag_related;
    }

    public void setRecyclerViewLayoutManager() {
        int scrollPosition = 0;
        mLayoutMgr = new LinearLayoutManager(getActivity());
//        if (mRecyclerView.getLayoutManager() != null) {
//            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
//                    .findFirstCompletelyVisibleItemPosition();
//        }
        mRecyclerView.setLayoutManager(mLayoutMgr);
//        mRecyclerView.scrollToPosition(scrollPosition);
    }

    @Override
    protected void findViewById() {
        super.findViewById();
        Analytics.trackScreen(Analytics.PropFeed.RelatedFeeds);
    }

    public void getData(final boolean addMore) {
        isLoading = true;

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("message_id", message_id);

        if (addMore) {
            params.put("startidx", (page) + "");
        } else {
            params.put("startidx", "0");
        }

        params = Utility.getInitialParams(PlaBroApi.RT.PROPFEEDS, params);
        AppVolley.processRequest(1, FeedResponse.class, null, String.format(PlaBroApi.getBaseUrl(mContext)), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {

                isLoading = false;
                FeedResponse feedResponse = (FeedResponse) response.getResponse();
                if (null != feedResponse.getOutput_params()) {
                    ArrayList<BaseFeed> tempFeeds = (ArrayList<BaseFeed>) feedResponse.getOutput_params().getData();
                    Log.d(TAG, "Temp Feed Size is :" + tempFeeds.size());
                    if (tempFeeds.size() > 0) {

                        LoaderFeed lf = new LoaderFeed();
                        if (tempFeeds.size() < Util.getDefaultStartIdx()) {
                            lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                        } else {
                            lf.setType(Constants.FEED_TYPE.PROGRESS_MORE);
                        }

                        if (addMore) {
                            feeds2.remove(feeds2.size() - 1);//for footer
                            feeds2.addAll(tempFeeds);
                            feeds2.add(lf);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            page = 0;
                            feeds2.clear();
                            feeds2.addAll(tempFeeds);
                            feeds2.add(lf);
                            if (mAdapter != null) {
                                mAdapter.notifyDataSetChanged();
                            }
                        }


                        page = Util.getStartIndex(page);


                    } else {
//                        if (!addMore) {
//                            comingSoon = true;
//                        }

                        LoaderFeed lf = new LoaderFeed();
                        feeds2.remove(feeds2.size() - 1);//for footer
                        lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                        feeds2.add(lf);
                        mAdapter.notifyDataSetChanged();
                    }
                    Log.d(TAG, "After Getting data Size is:" + feeds2.size());
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                isLoading = false;
                onApiFailure(response, taskCode);

                try {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 2000ms
                            LoaderFeed lf = new LoaderFeed();
                            lf.setType(Constants.FEED_TYPE.NO_MORE);

                            if (addMore || feeds2.size() == 1) {
                                feeds2.remove(feeds2.size() - 1);//for footer
                                feeds2.add(lf);
                                mAdapter.notifyDataSetChanged();

                            }
                        }
                    }, 2000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public void onStatusFalse(PBResponse response, int taskCode) {
        super.onStatusFalse(response, taskCode);
    }

    private void disableNewFeedsNotification() {

        PBPreferences.setFeedNotificationVal(0);
        HomeActivity.setNoOfNotification(0, false, "");

    }

    protected void showSpinners() {

    }

    private void stopSpinners() {
//                mProgressWheel.stopSpinning();
//                isLoading = false;
//                mProgressWheelLoadMore.stopSpinning();
    }


    private RelativeLayout layout;

    @Override
    public String getTAG() {
        return "Related Feeds Fragment";
    }


    @Override
    protected void setViewSizes() {

    }

    @Override
    protected void callScreenDataRequest() {

    }

    @Override
    protected void reloadRequest() {

        callScreenDataRequest();
    }


    private void setListAdapter(boolean scrollToLast) {
        mAdapter = new RelatedCustomAdapter(feeds2, "normal", getActivity(), hashKey);
        mAdapter.setHeaderHeight(headerHeight);
        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String id = null;
        int pos = 0;
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.FEEDS_REQUEST_CODE) {

            page = 0;
            reloadRequest();


        }
    }


    void showNoDataDialog(final Boolean addMore) {
        //  Log.i(TAG, "No Data Dialog" + addMore);
        stopSpinners();
        if (!addMore && feeds2.size() < 1) {
//                        mConditionalView.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void OnReceive(Context context, Intent intent) {
//        Log.d(TAG, "onReceive");
//        mAdapter = new RelatedCustomAdapter(feeds2, "normal", getActivity(), hashKey);
//        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    protected void setScrollOnLayoutManager(int scrollY) {
        Log.d(TAG, "y scrolling" + scrollY);
        FeedDetails act = (FeedDetails) getActivity();
        View mHeader = act.getHeader();

        Log.d(TAG, "Header Height:" + mHeader.getHeight() + ",Scrolling to offset" + (mHeader.getHeight() - scrollY));
        //mHeader.setTranslationY(-scrollY);
        mLayoutMgr.scrollToPositionWithOffset(0, -(scrollY));
    }


}