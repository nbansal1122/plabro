package com.plabro.realestate.activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.plabro.realestate.R;
import com.plabro.realestate.Services.KeyExchangeService;
import com.plabro.realestate.receivers.PBLocalReceiver;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.ProgressWheel;

public class KeyExchangeActivity extends PlabroBaseActivity {
    ProgressWheel mProgressWheel;

    @Override
    protected String getTag() {
        return "KeyExchange";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key_exchange);
        setClickListeners(R.id.btn_tryagain);
        initProgressWheel();
        KeyExchangeService.startService(this);
        mProgressWheel.spin();
    }

    private void moveToPreferences() {
        boolean isKeyExchangeDone = PBPreferences.getData(PBPreferences.KEY_AES_PROCESS, false);
        if (isKeyExchangeDone) {
            startActivity(new Bundle(), Preferences.class);
            finish();
        } else {
            onBackPressed();
        }

    }

    private void initProgressWheel() {
        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.white));
        mProgressWheel.setProgress(0.0f);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btn_tryagain:
                KeyExchangeService.startService(this);
                break;
        }
    }

    @Override
    public void findViewById() {

    }

    @Override
    public IntentFilter getFilter() {
        IntentFilter f = new IntentFilter();
        f.addAction(ACTION_KEY_EXCHANGE_SUCCESS);
        f.addAction(ACTION_KEY_EXCHANGE_FAILURE);
        return f;
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        mProgressWheel.stopSpinning();
        if (ACTION_KEY_EXCHANGE_SUCCESS.equals(intent.getAction())) {
            moveToPreferences();
        } else if (ACTION_KEY_EXCHANGE_FAILURE.equals(intent.getAction())) {
            showViews(R.id.btn_tryagain);
            showToast("Some error at server end. Please try again");
        }
    }

    @Override
    public void reloadRequest() {

    }
}
