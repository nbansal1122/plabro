package com.plabro.realestate.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.content.IntentCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.HomeActivity;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.GenericObserver;
import com.plabro.realestate.listeners.LocationSelectionListener;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.Tracker.Campaign;
import com.plabro.realestate.models.city.CityClass;
import com.plabro.realestate.models.city.CityResponse;
import com.plabro.realestate.models.login.Login;
import com.plabro.realestate.models.login.LoginResponse;
import com.plabro.realestate.models.registerUser.RegisterResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.EmailFetcher;
import com.plabro.realestate.uiHelpers.FloatingEditText;
import com.plabro.realestate.uiHelpers.MaterialDialog;
import com.plabro.realestate.uiHelpers.backendHelpers.IncomingSMS;
import com.plabro.realestate.uiHelpers.images.handlers.AsyncTask;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.AppLocationService;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.LocationAddress;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.ListDialog;
import com.plabro.realestate.widgets.dialogs.PBConditionalDialogView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class Preferences extends PlabroBaseActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    //for radius
    TextView seekBarValue;
    EditText userName, businessName;
    String valid_name;
    SeekBar seekBar;
    Button mLogin;
    private boolean isProceed;

    //image selector
    private Uri mImageCaptureUri;
    private ImageView mImageView;


    //image from source type
    private static final int PICK_FROM_CAMERA = 1;
    private static final int CROP_FROM_CAMERA = 2;
    private static final int PICK_FROM_FILE = 3;

    private TextView tv_location;
    private RelativeLayout rl_location;


    Boolean isValidBusinessNameEntered = false;
    Boolean isValidNameEntered = false;
    Boolean isProfileImageSelected = false;
    Boolean isLocationSelected = false;
    private boolean isCityListReceived;

    String locationSelected = "";

    String result;
    private boolean isAllFieldsValid = false;
    PBConditionalDialogView mConditionalView;

    private CityResponse registerResponse = new CityResponse();
    private HashMap<String, String> globalCityList = new HashMap<String, String>();

    final ArrayList<String> myList = new ArrayList<String>();
    final String defaultVal = "Select Location";
    private ScrollView mScrollView;
    private List<CityClass> cityResponse = new ArrayList<CityClass>();
    static final int REQUEST_TAKE_PHOTO = 2;
    String mCurrentPhotoPath;
    private Bitmap capturedBitmap;

    /**
     * Local variables *
     */
    GoogleMap googleMap;
    Circle circle;

    String email = "";
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    private boolean locationToastShown = false;

    @Override
    protected String getTag() {
        return "Preferences";
    }


    @Override
    protected int getResourceId() {
        return R.layout.activity_preferences;
    }

    @Override
    public void onEventUpdate(String event) {
        Log.d(TAG, "Event Update");
        super.onEventUpdate(event);
        if (!isCityListReceived) {
            getCityList();
        }
        Log.d(TAG, "is Proceed:" + isProceed + "is running, " + isRunning);
        if (isProceed && isRunning) {
            //setPreferences();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initiateLogin();
        buildGoogleApiClient();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Connect the client.
//        mGoogleApiClient.connect();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;
        switch (requestCode) {
            case REQUEST_TAKE_PHOTO:
                capturedBitmap = ImageCropper.croppedBitmap;
                if (capturedBitmap != null) {
                    sendMetaData(Constants.CampaignParams.USER_ACTION, Constants.RegistrationParams.IMAGE_CROPPED);
                    setPic();

                }
                if (!TextUtils.isEmpty(locationSelected)) {
                    tv_location.setText(locationSelected);
                }
                break;
        }
    }

    private void sendMetaData(String key, String value) {
        HashMap<String, Object> metaData = new HashMap<>();
        metaData.put(key, value);
        Analytics.sendRegistrationData(metaData);
    }

    public String getEncodedPicture() {
        try {
            if (null != capturedBitmap) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                if (capturedBitmap.isRecycled()) {
                    capturedBitmap = Bitmap.createBitmap(capturedBitmap);
                }
                capturedBitmap.compress(Bitmap.CompressFormat.JPEG, 60, baos);
                byte[] b = baos.toByteArray();
                String temp = Base64.encodeToString(b, Base64.DEFAULT);

                return temp;
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    //onclickListener
    private View.OnClickListener customOnClickLsitener = new View.OnClickListener() {

        @Override
        public void onClick(View c_view) {
            // switch for different onclicks
            switch (c_view.getId()) {

                case R.id.location:

                    onLocationSelect();

                    break;


            }

        }
    };

    private void onLocationSelect() {
        Log.v(TAG, myList + "");
        if (myList.isEmpty()) {

            Toast.makeText(Preferences.this, "Fetching your location. Please wait.", Toast.LENGTH_LONG).show();
            setupCityDropDown();

        } else {
            ListDialog listDialog = new ListDialog(myList, Preferences.this, locationSelected, defaultVal, locationSelectionListener, true);
            listDialog.show(getSupportFragmentManager(), "listDialog");
        }
    }


    /**
     * Initialises the mapview
     */
    private void createMapView() {
        /**
         * Catch the null pointer exception that
         * may be thrown when initialising the map
         */
        try {
            if (null == googleMap) {


                // googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapView)).getMap();
                /**
                 * If the map is still null after attempted initialisation,
                 * show an error to the user
                 */
                if (null == googleMap) {
                    Toast.makeText(getApplicationContext(),
                            "Error creating map", Toast.LENGTH_SHORT).show();
                } else {
                    addMarker();
                    addRadius();
                }
            }
        } catch (NullPointerException exception) {
            Log.e("mapApp", exception.toString());
        }
    }

    /**
     * Adds a marker to the map
     */
    private void addMarker() {

        /** Make sure that the map has been initialised **/
        if (null != googleMap) {
            googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(28.4700, 77.0300))
                            .title("Your Location")
                            .draggable(false)
            );
        }
    }

    /*
        Add  radius circle to map
     */
    void addRadius() {
        CircleOptions circleOptions = new CircleOptions().center(new LatLng(28.4700, 77.0300))
                .strokeColor(Color.RED)
                .radius(100)
                .strokeWidth(1)
                .fillColor(Color.parseColor("#95da0a0a"));


        circle = googleMap.addCircle(circleOptions);


        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                circleOptions.getCenter(), getZoomLevel(circle)));
    }

    void updateRadius(int radius) {
        circle.setRadius(100 + (radius / 5));
    }

    void setup_map() {

        if (checkMapSuppported()) {
            createMapView();
        }

    }

    @Override
    public void onBackPressed() {
        //confirm from user ofr exit
        showExitAlert();

    }

    void showExitAlert() {

        final MaterialDialog mMaterialDialog;
        mMaterialDialog = new MaterialDialog(this);

        mMaterialDialog.setTitle(R.string.exit_confirm_title)
                .setMessage(R.string.exit_confirm_msg)
                .setPositiveButton(R.string.exit, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                        // exit application
                        moveTaskToBack(true);

                    }
                })
                .setNegativeButton(R.string.cancel, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();


                    }
                });

        mMaterialDialog.show();


    }

    public int getZoomLevel(Circle circle) {
        int zoomLevel = 11;
        if (circle != null) {
            double radius = circle.getRadius() + circle.getRadius() / 2;
            double scale = radius / 500;
            zoomLevel = (int) (16 - Math.log(scale) / Math.log(2));
        }
        return zoomLevel;
    }

    public boolean validateName(EditText edt)
            throws NumberFormatException {
        if (edt.getText().toString().length() <= 0 || edt.getText().toString().length() > 256) {
            edt.setError("Name length should be between 0 and 256 character.");
            isValidNameEntered = false;

            return checkAndEnableNext();

        } /*else if (!edt.getText().toString().matches("[a-zA-Z ]+")) {
            edt.setError("Accept Alphabets Only.");
            isValidNameEntered = false;
            checkAndEnableNext();

        }*/ else {
            isValidNameEntered = true;
            return checkAndEnableNext();

        }

    }

    public void validateBusinessName(EditText edt)
            throws NumberFormatException {
        if (edt.getText().toString().length() <= 0) {
            edt.setError("Business name can't be empty");
            isValidBusinessNameEntered = false;
            checkAndEnableNext();

        } else {
            isValidBusinessNameEntered = true;
            checkAndEnableNext();
        }

    }

    boolean checkAndEnableNext() {
        if (isValidNameEntered && isLocationSelected) {
            isAllFieldsValid = true;
            if (businessName.getText().toString().equalsIgnoreCase("")) {
                isAllFieldsValid = true;
            } else {
                if (isValidBusinessNameEntered) {
                    isAllFieldsValid = true;
                }
            }
        } else {
            isAllFieldsValid = false;
        }

        return isAllFieldsValid;
    }


    void setPreferences() {


        if (TextUtils.isEmpty(PBPreferences.getAuthKey())) {
            initiateLogin();
            showToast("Please wait...");
            return;
        }

        final ProgressDialog progress;
        progress = ProgressDialog.show(Preferences.this, "Please wait",
                "Setting preferences...", true);

        String city_name = locationSelected;
        String city_id = globalCityList.get(locationSelected);
        PBPreferences.saveData(PBPreferences.SELECTED_CITY_ID, city_id);
        String name = userName.getText().toString();
        String busName = businessName.getText().toString();

        if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(busName)) {
            sendScreenName(R.string.funnel_USER_PROFILE_FILLED);
        }

        HashMap<String, String> params = new HashMap<String, String>();

        params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_NAME, name);
        if (!TextUtils.isEmpty(busName)) {
            params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_BUSINESS_NAME, busName);
        }
        if (!TextUtils.isEmpty(city_id)) {
            params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_CITY_ID, city_id);
        }
        params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_CITY_NAME, city_name);

        if (!TextUtils.isEmpty(email)) {
            params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_EMAIL, email);
        }

        if (mLastLocation != null) {
            try {
                double latitude = mLastLocation.getLatitude();
                double longitude = mLastLocation.getLongitude();
                JSONObject cord = new JSONObject();

                cord.put("lat", latitude);
                cord.put("long", longitude);
                String coordinate = cord.toString();
                Log.d(TAG, coordinate);

                if (!TextUtils.isEmpty(coordinate)) {
                    params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_COORDINATES, coordinate);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        String uri = PBPreferences.getData(PBPreferences.CAMPAIGN_URI, null);
        Log.d(TAG, "Campaign URI:" + uri);
        if (uri != null) {
            Log.d(TAG, "Tracking URL" + uri);
            //sendScreenName(R.string.f_app_campaign_found);
            params.putAll(Campaign.parseUri(uri));
        }

        if (null != capturedBitmap) {
            params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_IMG, getEncodedPicture());
            HashMap<String, Object> data = new HashMap<>();
            data.put("ScreenName", TAG);
            data.put("Action", "Upload");
        }

        params = Utility.getInitialParams(PlaBroApi.RT.SETPREF, params);

        AppVolley.processRequest(Constants.TASK_CODES.SET_PREFS, RegisterResponse.class, null, PlaBroApi.getBaseUrl(Preferences.this), params, RequestMethod.POST, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);


                RegisterResponse registerResponse = (RegisterResponse) response.getResponse();


                Toast.makeText(Preferences.this, "Preferences Saved Successfully.", Toast.LENGTH_LONG).show();

                //set flag to recognise first time user
                PBPreferences.setFirstTimeState(false);
                //track fresh install from facebook
                FacebookSdk.publishInstallAsync(getApplicationContext(), getResources().getString(R.string.facebook_app_id));

                //Navigate to home screen
                Analytics.trackScreen(Analytics.Registration.PREFERENCES);
                Intent intent = new Intent(Preferences.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                Log.i("PlaBro", "Preferences set successfully" + registerResponse.isStatus());


            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                onApiFailure(response, taskCode);
                Utility.dismissProgress(progress);


            }
        });
    }

    @Override
    public void onSessionExpiry(PBResponse response, int taskCode) {
        super.onSessionExpiry(response, taskCode);
        showConditionalMessage(PBConditionalDialogView.ConditionalDialog.SESSION_EXPIRED, true, false);
        mConditionalView.setVisibility(View.VISIBLE);
    }

    boolean checkMapSuppported() {
        // See if google play services are installed.
        boolean services = false;
        try {
            ApplicationInfo info = getPackageManager().getApplicationInfo("com.google.android.gms", 0);
            services = true;
        } catch (PackageManager.NameNotFoundException e) {
            services = false;
        }

        if (services) {
            return services;
        } else {


            final MaterialDialog mMaterialDialog;
            mMaterialDialog = new MaterialDialog(this);

            mMaterialDialog.setTitle("Google Play Services")
                    .setMessage("The map requires Google Play Services to be installed.")
                    .setCanceledOnTouchOutside(true)
                    .setPositiveButton("Install", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMaterialDialog.dismiss();

                            // Try the new HTTP method (I assume that is the official way now given that google uses it).
                            try {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.google.android.gms"));
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                intent.setPackage("com.android.vending");
                                startActivity(intent);
                            } catch (ActivityNotFoundException e) {
                                // Ok that didn't work, try the market method.
                                try {
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.gms"));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    intent.setPackage("com.android.vending");
                                    startActivity(intent);
                                } catch (ActivityNotFoundException f) {
                                    // Ok, weird. Maybe they don't have any market app. Just show the website.

                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.google.android.gms"));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    startActivity(intent);
                                }
                            }

                        }
                    })
                    .setNegativeButton("No", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMaterialDialog.dismiss();

                            finish();

                        }
                    });

            mMaterialDialog.show();


        }
        return services;
    }

    @Override
    public void findViewById() {
        IncomingSMS.disableBroadcastReciever(Preferences.this);// to disable incoming sms receiver

        GenericObserver.getInstance().addListener(this, GenericObserver.GenericObserverInterface.EVENT_AUTHKEY_UPDATE);
        mConditionalView = (PBConditionalDialogView) findViewById(R.id.pbConditionalView);
        mScrollView = (ScrollView) findViewById(R.id.sv_pref);

        mLogin = (Button) findViewById(R.id.login);
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateName(userName);
                if (checkAndEnableNext()) {
                    //setPreferences of the user to server
                    setPreferences();
                } else {
                    Toast.makeText(Preferences.this, "Please fill up all the details.", Toast.LENGTH_SHORT).show();
                    if (!isValidNameEntered) {
                        userName.requestFocus();
                    } else if (!isValidBusinessNameEntered) {
                        businessName.requestFocus();
                    } else if (!isLocationSelected) {
                        openLocation();
                    }
                }
            }
        });
        //setting location
        tv_location = (TextView) findViewById(R.id.location);
        tv_location.setOnClickListener(customOnClickLsitener);
        userName = (EditText) findViewById(R.id.userName);
        businessName = (EditText) findViewById(R.id.businessName);
        userName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                mScrollView.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
//                    }
//                });
            }

            @Override
            public void afterTextChanged(Editable s) {
//                validateName(userName);
            }
        });

        businessName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                // validateBusinessName(businessName);


            }
        });

        businessName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    if (null == locationSelected || TextUtils.isEmpty(locationSelected)) {
                        openLocation();
                    } else {
                        setPreferences();
                    }
                    return true;
                }
                return false;
            }
        });


        //set up image things
        mImageView = (ImageView) findViewById(R.id.user_image);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        findViewById(R.id.rl_edit_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        setupCityDropDown();
//        getCityList();


        fetchUserInfo();

    }

    private void checkAndRegister() {
        if (isValidNameEntered && isLocationSelected) {
            Log.d(TAG, "Register Success");
            //setPreferences();
        } else {
            Log.d(TAG, "Getting location again");
            // getLocation();
        }
    }

    private void getLocation() {
        Log.d(TAG, "fetching location...");

        //you can hard-code the lat & long if you have issues with getting it
        //remove the below if-condition and use the following couple of lines
        //double latitude = 37.422005;
        //double longitude = -122.084095

        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();


            Geocoder gc = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = gc.getFromLocation(latitude, longitude, 1);
                StringBuilder sb = new StringBuilder();
                if (addresses.size() > 0) {
                    Address address = addresses.get(0);
                    locationSelected = address.getLocality();
                    Log.d(TAG, "location detected  " + locationSelected);
                    if (null != locationSelected && !locationSelected.equalsIgnoreCase("")) {
                        tv_location.setText(locationSelected);
                        isLocationSelected = true;
//                        checkAndRegister();
                    }

                    /*for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                        sb.append(address.getAddressLine(i)).append("\n");
                        sb.append(address.getLocality()).append("\n");
                        sb.append(address.getPostalCode()).append("\n");
                        sb.append(address.getCountryName());
                        Log.d(TAG, "location detected iterated  " + address.getLocality());

                    }*/

                }

            } catch (IOException e) {
                e.printStackTrace();
            }


           /* LocationAddress locationAddress = new LocationAddress();
            locationAddress.getAddressFromLocation(latitude, longitude,
                   getApplicationContext(), new GeocoderHandler()); */
        } else {
            if (!locationToastShown) {
//                showToast("Unable to fetch location,please select location manually.");
                locationToastShown = true;
            }
        }

    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                Preferences.this);
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        Preferences.this.startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }


    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("city");
                    break;
                default:
                    locationAddress = null;
            }
            tv_location.setText(locationAddress);
        }
    }

    private void openLocation() {
        Util.hideKeyboard(Preferences.this);
        businessName.clearFocus();
        userName.clearFocus();
        onLocationSelect();

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {


    }

    @Override
    public void reloadRequest() {


        if (!Util.haveNetworkConnection(Preferences.this)) {
            showToast("Please check your internet connection");
            return;
        }
        //start fetching location
        getLocation();

        if (isAllFieldsValid) {
            //setPreferences();
        }

    }

    void getCityList() {

        HashMap<String, String> params = new HashMap<String, String>();

        // params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_NAME, name);

        params = Utility.getInitialParams(PlaBroApi.RT.GET_CITY_DROPDOWN, params);

        AppVolley.processRequest(Constants.TASK_CODES.GET_CITY_DROPDOWN, CityResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
                    @Override
                    public void onSuccessResponse(PBResponse response, int taskCode) {
                        try {
                            if (TextUtils.isEmpty(locationSelected) && !isLocationSelected) {
                                //tv_location.setText(defaultVal);
                            }
                            isCityListReceived = true;
                            cityResponse = (List<CityClass>) response.getResponse();
                            setupCityDropDown();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d(TAG, "Error: Setting city list");
                        }
                    }

                    @Override
                    public void onFailureResponse(PBResponse response, int taskCode) {
                        if (TextUtils.isEmpty(locationSelected) && !isLocationSelected) {
                            //tv_location.setText("No location");
                        }


                    }
                }
        );
    }

    private void setupCityDropDown() {
        myList.clear();
        globalCityList.clear();
        cityResponse = CityResponse.getCityList();
        for (CityClass cityClass : cityResponse) {
            if (!TextUtils.isEmpty(cityClass.getCity_name())) {
                myList.add(cityClass.getCity_name());
                globalCityList.put(cityClass.getCity_name(), cityClass.get_id());
            }
        }
    }

    LocationSelectionListener locationSelectionListener = new LocationSelectionListener() {
        @Override
        public void onResponse(Boolean status, String response) {
            //Toast.makeText(Preferences.this, response, Toast.LENGTH_LONG).show();
            if (status) {
                isLocationSelected = true;
                locationSelected = response;
                if (!TextUtils.isEmpty(response)) {
                    tv_location.setText(response);
                }
            } else {
                isLocationSelected = false;
                locationSelected = "";
            }
            checkAndEnableNext();
            Util.hideKeyboard(Preferences.this);
        }
    };


    void fetchUserInfo() {
        email = EmailFetcher.getEmailId(Preferences.this);
        String name = EmailFetcher.getName(Preferences.this);
        Cursor owner = EmailFetcher.getOwner(Preferences.this);
        userName.setText(name);
        userName.setSelection(name.length());

        // Toast.makeText(Preferences.this, email + ":::" + name, Toast.LENGTH_LONG).show();
        Uri uri = null;
        if (null != owner && owner.moveToFirst()) {
            do {
                // do what you need with the cursor here
                StringBuilder sb = new StringBuilder();

                int columnsQty = owner.getColumnCount();
                for (int idx = 0; idx < columnsQty; ++idx) {
                    sb.append(owner.getColumnName(idx) + "===" + owner.getString(idx));

                    if (owner.getColumnName(idx).equalsIgnoreCase("photo_uri")) {
                        String imgUri = owner.getString(idx);
                        if (null != imgUri && !TextUtils.isEmpty(imgUri)) {
                            uri = Uri.parse(imgUri);
                        }
                    }

                    if (idx < columnsQty - 1)
                        sb.append("; ");
                }
                //Log.v(TAG, String.format("Row: %d, Values: %s", owner.getPosition(), sb.toString()));

            } while (owner.moveToNext());
        }

        if (null != uri) {
            capturedBitmap = ActivityUtils.uriToBitmap(uri, Preferences.this);
            if (null != capturedBitmap) {
                mImageView.setImageBitmap(capturedBitmap);
            } else {
                mImageView.setImageDrawable(getResources().getDrawable(R.drawable.profile_default_one));
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        Util.hideKeyboard(businessName, Preferences.this);
        // getLocation();
    }


    //to capture image without cropping via new method
    private void dispatchTakePictureIntent() {

        //image selector
        Analytics.trackScreen(Analytics.UserProfile.ProfilePicture);

        Intent editIntent = new Intent(this, ImageCropper.class);
        editIntent.putExtra("img", "");
        if (capturedBitmap != null) {
            editIntent.putExtra("bitmap", true);
            ImageCropper.croppedBitmap = capturedBitmap;
        }
        sendMetaData(Constants.CampaignParams.USER_ACTION, Constants.RegistrationParams.IMAGE_CLICKED);
        startActivityForResult(editIntent, REQUEST_TAKE_PHOTO);
    }


    private void setPic() {
        try {
            Log.d(TAG, capturedBitmap + "");
            if (capturedBitmap != null) {
                mImageView.setImageBitmap(capturedBitmap);
                isProfileImageSelected = true;
                Log.d(TAG, capturedBitmap + "");

            } else {
                isProfileImageSelected = false;
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Toast.makeText(Preferences.this, "Error capturing picture!Please try again.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        getLocation();

        /*new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params) {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
        }.execute();*/

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    void initiateLogin() {
        HashMap<String, String> params = new HashMap<String, String>();

        if (!TextUtils.isEmpty(PBPreferences.getImei())) {
            params.put(PlaBroApi.IMEI, PBPreferences.getImei());
        }


        if (!TextUtils.isEmpty(PBPreferences.getPhone())) {
            params.put(PlaBroApi.PHONE, PBPreferences.getPhone());
        }

        params = Utility.getInitialParamsWithoutAuthkey(PlaBroApi.RT.AUTHLOGIN, params);

        AppVolley.processRequest(Constants.TASK_CODES.AUTH_LOGIN, LoginResponse.class, null, PlaBroApi.getBaseUrl(Preferences.this), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                if (response != null) {

                    Log.i("LoginResponseData", response + "");
                    LoginResponse loginResponse = (LoginResponse) response.getResponse();

                    int savedVer = PBPreferences.getTrialVersionState();
                    int serverVer = loginResponse.getOutput_params().getTrial_version();


                    if (savedVer < serverVer) {
                        //application submitted and in process
                        PBPreferences.setTrialVersionState(serverVer);
                    } else if (serverVer == -1) {
                        //user blocked from using service
                        PBPreferences.setTrialVersionState(serverVer);
                    }

                    Log.d("TRIALVERSION", "Server" + serverVer + " +++++++ Saved " + savedVer);


                    //since server blocked the user so lets not tresspass anyone now
                    if (serverVer == -1) {
                        return;

                    }


                    if (loginResponse.isStatus()) {


                        Login login = loginResponse.getOutput_params().getData();
                        login.setTrial_version(serverVer);
                        login.saveData();
                        String authkey = login.getAuthkey();

                        if ("".equalsIgnoreCase(authkey) || null == authkey) {
                            PBPreferences.setLoggedInState(false);

                        } else {

                            PBPreferences.setAuthKey(authkey);

                            PBPreferences.setLoggedInState(true);

                            Log.i("PlaBro", "login sussessfully from server");
                            getCityList();
                            //Navigate to home
                            getLocation();


                        }

                    } else {

                    }

                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                onApiFailure(response, taskCode);
                showToast(response.getCustomException().getMessage());
            }
        });
    }
}
