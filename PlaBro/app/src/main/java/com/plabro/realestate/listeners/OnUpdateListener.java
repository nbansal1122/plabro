package com.plabro.realestate.listeners;

/**
 * Created by Hemant on 23-01-2015.
 */
public interface OnUpdateListener {

    public void  onResponse(Boolean response,String val);
    public void  onResponse(Boolean response,int val);

}
