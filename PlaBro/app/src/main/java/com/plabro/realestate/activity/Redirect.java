package com.plabro.realestate.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.plabro.realestate.R;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 13/10/15.
 */
public class Redirect extends PlabroBaseActivity implements VolleyListener {
    private ProgressDialog dialog;

    public static void redirectToShoutDetail(Context ctx, String messageId, String refKey) {
        Intent i = new Intent(ctx, Redirect.class);
        Bundle b = new Bundle();
        b.putString("message_id", messageId);
        b.putString("deep_hash", refKey);
        i.putExtras(b);
        ctx.startActivity(i);
    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_redirect;
    }

    @Override
    protected String getTag() {
        return "Redirect";
    }

    private void hideDialog() {
        if (dialog != null && dialog.isShowing() && isRunning) {
            dialog.dismiss();
        }
    }

    @Override
    public void findViewById() {
        getShoutDetails(getIntent().getStringExtra("message_id"));
    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    private void getShoutDetails(String message_id) {
        ParamObject obj = new ParamObject();
        obj.setClassType(FeedResponse.class);
        obj.setTaskCode(Constants.TASK_CODES.SHOUT_DETAILS);
        HashMap<String, String> params = new HashMap<>();
        params.put("message_id", message_id);
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.SHOUT_DETAILS, params));
        showDialog();
        AppVolley.processRequest(obj, this);
    }


    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        hideDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.SHOUT_DETAILS:
                FeedResponse resp = (FeedResponse) response.getResponse();
                if (null != resp.getOutput_params() && null != resp.getOutput_params().getData()) {
                    List<BaseFeed> feeds = resp.getOutput_params().getData();
                    if (feeds.size() > 0) {
                        Feeds feedsObject = (Feeds) feeds.get(0);
                        FeedDetails.startFeedDetailsWithoutAnimation(this, feedsObject.get_id(), "", feedsObject);
                        finish();
                        return;
                    }
                }
                onBackPressed();
                break;
        }
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        try {
            hideDialog();
            switch (taskCode) {
                case Constants.TASK_CODES.SHOUT_DETAILS:
                    showFailureMessage(response);
                    onBackPressed();
                    break;
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    private void showFailureMessage(PBResponse response) {
        showToast(response.getCustomException().getMessage());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
    }
}
