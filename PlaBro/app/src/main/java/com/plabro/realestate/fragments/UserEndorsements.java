package com.plabro.realestate.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;

import com.desmond.parallaxviewpager.ListViewFragment;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.EndorseList;
import com.plabro.realestate.activity.UserProfileNew;
import com.plabro.realestate.adapter.PBExpandableListAdapter;
import com.plabro.realestate.models.profile.Endorsement_profile;
import com.plabro.realestate.models.profile.Endorser_list;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 28/07/15.
 */
public class UserEndorsements extends ListViewFragment implements UserProfileNew.ProfileRefreshListener {

    private ExpandableListView expandableListView;
    private PBExpandableListAdapter adapter;
    private ProfileClass profile;
    private boolean isCurrentUser;
    List<Endorsement_profile> list = new ArrayList<>();
    private static final String KEY_PROFILE = "Profile";
    public static final String KEY_CURRENT_USER = "IsCurrentUser";

    private static final String TAG = "UserEndorsements";
    private int headerHeight = 0;
    private static final int ENDORSE_REQUEST = 1;
    private FrameLayout placeHolderView;

    public static Fragment getInstance(ProfileClass profile, boolean isCurrentUser) {
        Bundle b = new Bundle();
        b.putSerializable(KEY_PROFILE, profile);
        b.putBoolean(KEY_CURRENT_USER, isCurrentUser);
        UserEndorsements info = new UserEndorsements();
        info.setArguments(b);
        return info;
    }

    public void setHeaderHeight(int headerHeight) {
        this.headerHeight = headerHeight;
    }

    @Override
    protected void findViewById() {
        Analytics.trackScreen(Analytics.UserProfile.Recommendations);
        profile = (ProfileClass) getArguments().getSerializable(KEY_PROFILE);
        isCurrentUser = getArguments().getBoolean(KEY_CURRENT_USER);

        initEndorsementList(profile);
        expandableListView = (ExpandableListView) findView(R.id.expListView);
        setExpandableListView();
    }

    private void setExpandableListView() {
        mListView = expandableListView;
        expandableListView.removeAllViewsInLayout();

        if (null != placeHolderView) {
            mListView.removeHeaderView(placeHolderView);
        }
        placeHolderView = getHeader();
        mListView.addHeaderView(placeHolderView);
        initViews();
    }

    private FrameLayout getHeader() {
        FrameLayout placeHolderView = (FrameLayout) LayoutInflater.from(getActivity()).inflate(R.layout.header_placeholder, mListView, false);
        AbsListView.LayoutParams params = new AbsListView.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, this.headerHeight);
        placeHolderView.setLayoutParams(params);
        return placeHolderView;
    }

    private void initViews() {


        expandableListView.setEmptyView(findView(android.R.id.empty));
        adapter = new PBExpandableListAdapter(getActivity(), list, profile.getName());
        expandableListView.setAdapter(adapter);
        for (int i = 0; i < list.size(); i++) {
            expandableListView.expandGroup(i);
        }

        setListViewOnScrollListener();


        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                Log.d(TAG, "i:" + i + ",l:" + l);
                if (list.get(i).isEmpty()) return false;
                if (expandableListView.isGroupExpanded(i) || list.get(i).getEndorser_list().size() == 0) {
                    Endorsement_profile endprofile = list.get(i);
                    Bundle b = new Bundle();
                    b.putSerializable(Constants.BUNDLE_KEY_ENDORSER_PROFILE, endprofile);
                    b.putSerializable(Constants.BUNDLE_KEY_USER_PROFILE, profile);
                    b.putBoolean(KEY_CURRENT_USER, isCurrentUser);
                    Intent intent = new Intent(getActivity(), EndorseList.class);
                    intent.putExtras(b);
                    getActivity().startActivityForResult(intent, ENDORSE_REQUEST);
                    return true;
                }
                return false;
            }
        });
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {


                Log.d(TAG, "Child Click");
                Endorser_list endorser = list.get(i).getEndorser_list().get(i1);

                Bundle b = new Bundle();
                b.putString("authorid", endorser.getUid());
                Intent intent = new Intent(getActivity(), UserProfileNew.class);
                intent.putExtras(b);
                startActivity(intent);
//                getActivity().finish();
                return true;
            }
        });
    }


    private void initEndorsementList(ProfileClass profile) {

        List<Endorsement_profile> profilesList = new ArrayList<>();

        list = (null != profile.getEndorsement_profile()) ? profile.getEndorsement_profile() : list;
        profilesList.addAll(list);
        if (list.size() > 0) {
//            for (int i = 0; i < list.size(); i++) {
//                Endorsement_profile p = list.get(i);
//
//                if (null != p.getEndorser_list() && p.getEndorser_list().size() > 0) {
//                    if (p.getEndorser_list().size() > 2) {
//                        for (int j = 2; j < p.getEndorser_list().size(); j++) {
//                            profilesList.get(i).getEndorser_list().remove(j);
//                        }
//                    } else {
//
//                    }
//                } else {
//
//                }
//            }
//            list = profilesList;
        } else {
            Endorsement_profile pr = new Endorsement_profile();
            pr.setIsEmpty(true);
            list.add(pr);
        }

    }


    public String getTAG() {
        return "UserEndorsements";
    }


    @Override
    protected int getViewId() {
        return R.layout.fragment_user_endorsements;
    }


    @Override
    public void onProfileRefresh(ProfileClass profile) {
        Log.d("UserEndorsement", "onProfile Refresh");
        this.profile = profile;
//        list = new ArrayList<>();
//        if (null != list) {
//            list.clear();
//        }
//        initEndorsementList(this.profile);
//        adapter.notifyDataSetChanged();
        initEndorsementList(this.profile);
        setExpandableListView();
    }
}
