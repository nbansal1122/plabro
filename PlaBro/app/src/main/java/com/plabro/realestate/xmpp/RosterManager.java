package com.plabro.realestate.xmpp;

import android.text.TextUtils;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.XMPP.RosterData;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.RosterPacket;
import org.jivesoftware.smackx.LastActivityManager;
import org.jivesoftware.smackx.packet.LastActivity;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by jmd on 6/10/2015.
 */
public class RosterManager {
    private static final String TAG = "RosterManager";

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    private Connection connection;

    public Roster getRoster() {
        return roster;
    }

    public void setRoster(Roster roster) {
        this.roster = roster;
    }

    private Roster roster;
    private static RosterManager rosterManager;

    private RosterManager() {

    }

    public static RosterManager getInstance() {
        if (rosterManager == null) {
            rosterManager = new RosterManager();
        }
        return rosterManager;
    }

    public static void initialize(Connection connection) {
        if (rosterManager == null) {
            rosterManager = new RosterManager();
        }
        rosterManager.connection = connection;

        rosterManager.roster = connection.getRoster();
        rosterManager.roster.addRosterListener(roasterListener);
    }

    public static synchronized void createEntryIfNotFound(String jid, String name, String[] groups) throws Exception {
        if (getInstance().roster != null)
            if (isEntryFound(jid)) {
                Log.d(TAG, "Entry Found");
            } else {
                Log.d(TAG, "Entry Not Found");
                getInstance().roster.createEntry(jid, name, groups);
            }
    }

    public static synchronized boolean isEntryFound(String jid) {
        if (null == jid || TextUtils.isEmpty(jid)) {
            return false;
        }
        try {
            return RosterManager.getInstance().roster.contains(jid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static synchronized void sendPresence(Presence.Type type) {
//        Connection connection = getInstance().getConnection();
//        if (connection != null && connection.isConnected() && connection.isAuthenticated()) {
//            Presence presence = new Presence(type);
//            Collection<RosterEntry> entries = connection.getRoster().getEntries();
//            Iterator<RosterEntry> it = entries.iterator();
//            while(it.hasNext()){
//                RosterEntry entry =it.next();
//                presence.setTo(entry.getUser());
//                presence.setFrom(connection.getUser());
//                connection.sendPacket(presence);
//            }
//
//        }
    }


    private static RosterListener roasterListener = new RosterListener() {
        @Override
        public void presenceChanged(Presence presence) {
            //Called when the presence of a roster entry is changed
            Roster roster = getInstance().roster;
            Collection<RosterEntry> entries = roster.getEntries();
            for (RosterEntry entry : entries) {
                Log.d(TAG, "Entry:---" + entry.toString());
                RosterData info = new RosterData();
                info.setUserName(entry.getName());
                info.setUser(entry.getUser());
                info.setStatus(entry.getStatus() + "");
                Log.d(TAG, "" + entry.getName() + ", " + entry.getUser() + ", " + entry.getStatus());

//                if (null == entry.getStatus() || !entry.getStatus().toString().equals(Presence.Type.subscribed)) {
//                    if (null != entry.getUser() && getInstance().getConnection() != null) {
//                        Presence p = new Presence(Presence.Type.subscribe);
//                        p.setTo(entry.getUser());
//                        getInstance().getConnection().sendPacket(p);
//                    }
//                }
                Presence entryPresence = roster.getPresence(entry.getUser());
                Log.d(TAG, "Presence Packet:" + entryPresence.toXML());
                Presence.Type type = entryPresence.getType();
                if (type == Presence.Type.available) {
                    info.setIsAvailable(true);
                    //info.setLastSeenTimeStamp(System.currentTimeMillis());
                } else if (type == Presence.Type.unavailable) {
                    info.setIsAvailable(false);
                    //info.setLastSeenTimeStamp(getLastActivity(entry.getUser()));
//                    Presence p = new Presence(Presence.Type.subscribed);
//                    p.setTo(entry.getUser());
//                    getInstance().getConnection().sendPacket(p);
                }
                info.save();
                Log.d("XMPPChatDemoActivity", "Presence : " + entryPresence);
            }

        }

        @Override
        public void entriesUpdated(Collection<String> arg0) {
            // Called when a roster entries are updated.
            Log.d(TAG, "Entries Updated");
        }

        @Override
        public void entriesDeleted(Collection<String> arg0) {
            // Called when a roster entries are removed.
            //  Log.d(TAG, "Entries Removed");
        }

        @Override
        public void entriesAdded(Collection<String> collection) {
            // Called when a roster entries are added.
//            Log.d(TAG, "Entries Added");
//            String user = "";
//            Iterator<String> it = collection.iterator();
//            if (it.hasNext()) {
//                user = it.next();
//            }
//
//            Connection connection = getInstance().getConnection();
//            if (connection != null && connection.isConnected() && connection.isAuthenticated()) {
//                Presence presence = new Presence(Presence.Type.subscribe);
//                presence.setTo(user);
//                connection.sendPacket(presence);
//            }
        }
    };

    public static long getLastActivity(String jid) {

        try {
            LastActivity lastActivity = LastActivityManager.getLastActivity(getInstance().getConnection(), jid);
            // Log.d(TAG, "LAstActivity"+lastActivity.lastActivity);
            return lastActivity.lastActivity;
        } catch (Exception e) {
            //Log.d(TAG, "Error Last Seen");
            e.printStackTrace();
        }
        return 0;
    }
}
