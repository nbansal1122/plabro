package com.plabro.realestate.utilities;

import android.content.Context;

import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.shouts.ShoutResponse;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.HashMap;

/**
 * Created by hemantkumar on 01/09/15.
 */
public class BackgroundProcessHandler {

    public static void initiateBackgroundPostShout(final Context context, final HashMap<String, String> tempParams, final int notifyId)
    {

        AppVolley.processRequest(Constants.TASK_CODES.POST_SHOUTS, ShoutResponse.class, null, PlaBroApi.getBaseUrl(), tempParams, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                PlabroPushNotificationManager.cancelNotification(context, notifyId);
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                //BackgroundProcessHandler.initiateBackgroundPostShout( context,tempParams,notifyId);
            }
        });


    }

    public static void initiateBackgroundEditShout(final Context context, final HashMap<String, String> tempParams, final int notifyId)
    {

        AppVolley.processRequest(Constants.TASK_CODES.EDIT_SHOUT, ShoutResponse.class, null, PlaBroApi.getBaseUrl(), tempParams, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                PlabroPushNotificationManager.cancelNotification(context, notifyId);
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                //BackgroundProcessHandler.initiateBackgroundPostShout( context,tempParams,notifyId);
            }
        });


    }
}
