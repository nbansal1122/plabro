/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.plabro.realestate.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.plabro.realestate.R;
import com.plabro.realestate.listeners.MyClipboardManager;
import com.plabro.realestate.models.chat.ChatMessage;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.widgets.RelativeTimeTextView;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends BaseAdapter {

    private  Context mContext;
    private  Activity mActivity;
    private int msgCount = 0;
    private static final String TAG = "MessageCustomAdapter";
    ViewHolder holder;
    String receiverImage;

    //    private MyNetworkImageView profileImageSen;
//    private TextView messageTextSen;
//
//    private MyNetworkImageView profileImageRec;
//    private TextView messageTextRec;
//
//    private RelativeLayout sent, received;
//
//    private RelativeTimeTextView timeSent, timeRec;
    private String mRecipient = "";


    private static List<ChatMessage> messages = new ArrayList<ChatMessage>();


    public MessageAdapter(Context context, Activity activity, List<ChatMessage> messages) {

        super();
        mContext = context;
        mActivity = activity;
        this.messages = messages;

    }


    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;
        LayoutInflater inflater = null;
        // First let's verify the convertView is not null
        if (convertView == null) {
            inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.message_layout, parent,false);
            holder = new ViewHolder();

         //   holder.profileImageRec = (MyNetworkImageView) view.findViewById(R.id.profileImageRec);
            holder.messageTextRec = (TextView) view.findViewById(R.id.messageTextRec);
            holder.received = (RelativeLayout) view.findViewById(R.id.received);

          //  holder.profileImageSen = (MyNetworkImageView) view.findViewById(R.id.profileImageSen);
            holder.messageTextSen = (TextView) view.findViewById(R.id.messageTextSen);
            holder.sent = (RelativeLayout) view.findViewById(R.id.sent);
            holder.timeSent = (RelativeTimeTextView) view.findViewById(R.id.timestampSen);
            holder.timeRec = (RelativeTimeTextView) view.findViewById(R.id.timestampRec);

            holder.sent.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    String text = ((String) v.getTag());

                    MyClipboardManager clipboardManager = new MyClipboardManager();

                    clipboardManager.copyToClipboard(mContext, text);
                    Toast.makeText(mContext, "Message Copied", Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            holder.received.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    String text = ((String) v.getTag());

                    MyClipboardManager clipboardManager = new MyClipboardManager();

                    clipboardManager.copyToClipboard(mContext, text);
                    Toast.makeText(mContext, "Message Copied", Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            fetchReciverImage();

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }


        String message = messages.get(position).getMsg();
        String useType = messages.get(position).getUserType();

        long time = messages.get(position).getTime();
        mRecipient = messages.get(position).getPhone();


        if (useType.equalsIgnoreCase("receiver")) {
            holder.sent.setVisibility(View.GONE);
            holder.received.setVisibility(View.VISIBLE);
            holder.timeRec.setReferenceTime(time);
            holder.messageTextRec.setText(message);
            holder.received.setTag(message);

            if (null != receiverImage && !"".equalsIgnoreCase(receiverImage)) {
              //  holder.profileImageRec.setImage(receiverImage, AppController.getInstance().getImageLoader());
            } else {
              //  holder.profileImageRec.setDefaultImageResId(R.drawable.profile_default_one);
            }


        }

        if (useType.equalsIgnoreCase("sender")) {
            holder.timeSent.setReferenceTime(time);

            holder.sent.setVisibility(View.VISIBLE);
            holder.received.setVisibility(View.GONE);
            holder.messageTextSen.setText(message);
            holder.sent.setTag(message);


            //          profileImageSen.setDefaultImageResId(R.drawable.profile_default_one);
         //   holder.profileImageSen.setImage(PBPreferences.getUserImg(mContext), AppController.getInstance().getImageLoader());

        }


        return view;
    }

    private void fetchReciverImage() {

        //first get profile of the user from the phone number provided
        ProfileClass userProfile = new Select().from(ProfileClass.class).where("phone = ?",mRecipient).executeSingle();
        if(userProfile!=null && userProfile.getImg()!=null && userProfile.getImg()!="") {
            receiverImage = userProfile.getImg(); //todo hemant remove check if not required
        }
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        if (observer != null) {
            super.unregisterDataSetObserver(observer);
        }
    }

    static class ViewHolder {


//        MyNetworkImageView profileImageSen;
        TextView messageTextSen;
        //MyNetworkImageView profileImageRec;
        TextView messageTextRec;
        RelativeLayout sent, received;
        RelativeTimeTextView timeSent, timeRec;

    }

}
