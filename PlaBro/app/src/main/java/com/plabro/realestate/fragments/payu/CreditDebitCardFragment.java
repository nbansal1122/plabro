package com.plabro.realestate.fragments.payu;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PostData;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.india.Payu.PayuUtils;
import com.payu.india.PostParams.PaymentPostParams;
import com.payu.payuui.PaymentsActivity;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.fragments.MonthYearPickerDialog;
import com.plabro.realestate.fragments.PaymentDialogFragment;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;

/**
 * Created by nitin on 18/11/15.
 */
public class CreditDebitCardFragment extends PlabroBaseFragment {
    private Button payNowButton;
    private EditText cardNameEditText;
    private EditText cardNumberEditText;
    private EditText cardCvvEditText;
    private EditText cardExpiryMonthEditText;
    private EditText cardExpiryYearEditText;
    private Bundle bundle;
    private CheckBox saveCardCheckBox;
    private ImageView cardTypeImage;

    private String cardName;
    private String cardNumber;
    private String cvv;
    private String expiryMonth;
    private String expiryYear;

    private PayuHashes mPayuHashes;
    private PaymentParams mPaymentParams;
    private PostData postData;
    private Toolbar toolbar;

    private TextView amountTextView;
    private TextView transactionIdTextView;
    private PayuConfig payuConfig;

    private PayuUtils payuUtils;


    @Override
    public String getTAG() {
        return "Credit Debit Card";
    }

    @Override
    protected void findViewById() {

        cardNameEditText = (EditText) findView(R.id.et_cardName);
        cardNumberEditText = (EditText) findView(R.id.et_cardNumber);
        cardCvvEditText = (EditText) findView(R.id.et_cardCVV);
        cardExpiryMonthEditText = (EditText) findView(R.id.et_cardExpiryMonth);
        cardExpiryYearEditText = (EditText) findView(R.id.et_cardExpiryYear);
        saveCardCheckBox = (CheckBox) findView(R.id.cv_saveCard);
        cardTypeImage = (ImageView) findView(R.id.iv_card_type);
        setClickListeners(cardExpiryMonthEditText, cardExpiryYearEditText);
        bundle = getArguments();


        // lets get payment default params and hashes
        mPayuHashes = bundle.getParcelable(PayuConstants.PAYU_HASHES);
        mPaymentParams = bundle.getParcelable(PayuConstants.PAYMENT_PARAMS);
        payuConfig = bundle.getParcelable(PayuConstants.PAYU_CONFIG);
        payuConfig = null != payuConfig ? payuConfig : new PayuConfig();


        // lets not show the save card check box if user credentials is not found!
        if (null == mPaymentParams.getUserCredentials())
            saveCardCheckBox.setVisibility(View.GONE);
        else
            saveCardCheckBox.setVisibility(View.VISIBLE);

        payuUtils = new PayuUtils();


        cardNumberEditText.addTextChangedListener(new TextWatcher() {
            String issuer;
            Drawable issuerDrawable;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 5) { // to confirm rupay card we need min 6 digit.
                    if (null == issuer) issuer = payuUtils.getIssuer(charSequence.toString());
                    if (issuer != null && issuer.length() > 1 && issuerDrawable == null) {
                        issuerDrawable = ActivityUtils.getIssuerDrawable(getActivity(), issuer);
                        if (issuer.contentEquals(PayuConstants.SMAE)) { // hide cvv and expiry
                            cardExpiryMonthEditText.setVisibility(View.GONE);
                            cardExpiryYearEditText.setVisibility(View.GONE);
                            cardCvvEditText.setVisibility(View.GONE);
                        } else { //show cvv and expiry
                            cardExpiryMonthEditText.setVisibility(View.VISIBLE);
                            cardExpiryYearEditText.setVisibility(View.VISIBLE);
                            cardCvvEditText.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    issuer = null;
                    issuerDrawable = null;
                }
                cardTypeImage.setImageDrawable(issuerDrawable);
//                cardNumberEditText.setCompoundDrawablesWithIntrinsicBounds(null, null, issuerDrawable, null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.et_cardExpiryMonth:
                MonthYearPickerDialog d = new MonthYearPickerDialog();
                d.setListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        Log.d(TAG, i + ", " + i1 + ", " + i2);
                    }
                });
                d.show(getChildFragmentManager(), "dateDialog");
                break;
            case R.id.et_cardExpiryYear:
                break;
        }
    }

    public void sendPayment() {
        // do i have to store the card
        cardNumber = String.valueOf(cardNumberEditText.getText());
        if (TextUtils.isEmpty(cardNumber)) {
            showToast("Please enter valid card number");
            return;
        }
        cardName = cardNameEditText.getText().toString();
        if (TextUtils.isEmpty(cardName)) {
            showToast("Please enter name on the card");
            return;
        }
        cvv = cardCvvEditText.getText().toString();
        if (TextUtils.isEmpty(cardName)) {
            showToast("Please enter CVV of your card");
            return;
        }
        expiryMonth = cardExpiryMonthEditText.getText().toString();
        if (TextUtils.isEmpty(expiryMonth)) {
            showToast("Please enter valid card expiry month");
            return;
        } else {
            int monthValue = Integer.parseInt(expiryMonth);
            if (monthValue > 12 || monthValue < 1) {
                showToast("Please enter valid card expiry month");
                return;
            }
            if (monthValue < 10) {
                expiryMonth = "0" + monthValue;
            }
        }
        expiryYear = cardExpiryYearEditText.getText().toString();
        if (expiryYear.length() != 4) {
            showToast("Please enter valid card expiry year");
            return;
        }


        if (saveCardCheckBox.isChecked()) {
            mPaymentParams.setStoreCard(1);
        } else {
            mPaymentParams.setStoreCard(0);
        }
        // setup the hash
        mPaymentParams.setHash(mPayuHashes.getPaymentHash());

        // lets try to get the post params

        postData = null;
        // lets get the current card number;
        cardNumber = String.valueOf(cardNumberEditText.getText());
        cardName = cardNameEditText.getText().toString();
        cvv = cardCvvEditText.getText().toString();

        // lets not worry about ui validations.
        mPaymentParams.setCardNumber(cardNumber);
        mPaymentParams.setCardName(cardName);
        mPaymentParams.setNameOnCard(cardName);
        mPaymentParams.setExpiryMonth(expiryMonth);
        mPaymentParams.setExpiryYear(expiryYear);
        Log.d(TAG, "Expiry Year:" + expiryYear);
        Log.d(TAG, "Expiry Month:" + expiryMonth);
        mPaymentParams.setCvv(cvv);
        if (saveCardCheckBox.isChecked()) {
            makePayment();
        } else {
            showStoreCardDialog();
        }

    }

    private void makePayment() {
        postData = new PaymentPostParams(mPaymentParams, PayuConstants.CC).getPaymentPostParams();
        if (postData.getCode() == PayuErrors.NO_ERROR) {
            // okay good to go.. lets make a transaction
            // launch webview
            payuConfig.setData(postData.getResult());
            Log.d(TAG, "Post Data Result:" + postData.getResult());
            Intent intent = new Intent(getActivity(), PaymentsActivity.class);
            intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
            String successUrl = mPaymentParams.getSurl();
            String failUrl = mPaymentParams.getFurl();
            intent.putExtra(Constants.BUNDLE_KEYS.S_URL, successUrl);
            intent.putExtra(Constants.BUNDLE_KEYS.F_URL, failUrl);
            startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
        } else {
            Toast.makeText(getActivity(), postData.getResult() + "\nError Code" + postData.getCode(), Toast.LENGTH_LONG).show();
        }
    }

    private void showStoreCardDialog() {
        PaymentDialogFragment f = PaymentDialogFragment.getInstance(getString(R.string.are_you_sure), getString(R.string.msg_delete_card), getString(R.string.card_store), getString(R.string.card_dont_store), true, new PaymentDialogFragment.DialogListener() {
            @Override
            public void onOKClicked() {
                mPaymentParams.setStoreCard(1);
                makePayment();
            }

            @Override
            public void onCancelClicked() {
                makePayment();
            }
        });
        f.show(getChildFragmentManager(), "delete");
    }

    @Override
    protected int getViewId() {
        return R.layout.layout_card_entry;
    }
}
