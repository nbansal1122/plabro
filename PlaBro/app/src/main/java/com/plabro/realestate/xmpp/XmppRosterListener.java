package com.plabro.realestate.xmpp;

import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.packet.Presence;

import java.util.Collection;

/**
 * Created by jmd on 6/4/2015.
 */
public class XmppRosterListener implements RosterListener {

    @Override
    public void entriesAdded(Collection<String> collection) {

    }

    @Override
    public void entriesUpdated(Collection<String> collection) {

    }

    @Override
    public void entriesDeleted(Collection<String> collection) {

    }

    @Override
    public void presenceChanged(Presence presence) {

    }
}
