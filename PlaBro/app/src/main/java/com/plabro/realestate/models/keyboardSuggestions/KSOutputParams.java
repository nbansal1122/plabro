package com.plabro.realestate.models.keyboardSuggestions;

import java.util.List;

public class KSOutputParams {
    private List<String> data;

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

}