package com.plabro.realestate.models.chat;

import com.plabro.realestate.models.propFeeds.Feeds;

/**
 * Created by nitin on 05/08/15.
 */
public class ChatPopUpInfo {

    public static boolean isChatStarted;
    public static boolean isChatCompleted;
    public static Feeds feed;
}
