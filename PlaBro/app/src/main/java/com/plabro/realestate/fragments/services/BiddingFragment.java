package com.plabro.realestate.fragments.services;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nitin on 18/01/16.
 */
public class BiddingFragment extends PlabroBaseFragment {

    private WebView webView;
    private String title, inputUrl, exitUrl;
    private ProgressWheel mProgressWheel;
    private boolean isAppendedUrl;

    public static BiddingFragment getInstance(String title, String inputUrl, String exitUrl) {
        BiddingFragment f = getInstance(title, inputUrl, exitUrl, true);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Analytics.trackScreen(Analytics.Requirements.BidDetails);
    }

    public static BiddingFragment getInstance(String title, String inputUrl, String exitUrl, boolean isAppendedUrl) {
        Bundle b = new Bundle();
        b.putString(Constants.BUNDLE_KEYS.TITLE, title);
        b.putString(Constants.BUNDLE_KEYS.INPUT_URL, inputUrl);
        b.putString(Constants.BUNDLE_KEYS.EXIT_URL, exitUrl);
        b.putBoolean(Constants.BUNDLE_KEYS.IS_APPENDED, isAppendedUrl);
        BiddingFragment f = new BiddingFragment();
        f.setArguments(b);
        return f;
    }

    @Override
    public String getTAG() {
        return "Bidding Page";
    }

    @Override
    protected void loadBundleData(Bundle b) {
        title = b.getString(Constants.BUNDLE_KEYS.TITLE);
        inputUrl = b.getString(Constants.BUNDLE_KEYS.INPUT_URL);
        exitUrl = b.getString(Constants.BUNDLE_KEYS.EXIT_URL);
        isAppendedUrl = b.getBoolean(Constants.BUNDLE_KEYS.IS_APPENDED);
    }

    private class MyWebClient extends WebViewClient {

        private static final String TAG = "MyWebClient";

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.d(TAG, "on Page started:" + url);
            if (!TextUtils.isEmpty(exitUrl)) {
                if (exitUrl.equalsIgnoreCase(url)) {
                    getActivity().finish();
                }
            }

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d(TAG, "Override URL Loading Input Url:" + url);
            if (!url.contains(PlaBroApi.AUTH_KEY) && (url.startsWith("http") || url.startsWith("www"))) {
                if (isAppendedUrl)
                    url = getAppendedUrl(url);
            }
            if (url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(intent);
                view.reload();
                return true;
            }
            if (url.startsWith("mailto:")) {
                Intent intent = new Intent(Intent.ACTION_SEND, Uri.parse(url));
                intent.setType("text/plain");
                startActivity(intent);
                view.reload();
                return true;
            }
            view.loadUrl(url);
            Log.d(TAG, "Overrided URL:" + url);
            return true;
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.d(TAG, "on Page finished:" + url);
            mProgressWheel.stopSpinning();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            if (null != error)
                Log.d(TAG, "on Received Error:" + error.getDescription());

        }
    }

    private String getAppendedUrl(String url) {
        HashMap<String, String> otherParams = new HashMap<>();
        otherParams.put(PlaBroApi.AUTH_KEY, PBPreferences.getAuthKey());
        otherParams.put(PlaBroApi.PHONE, PBPreferences.getPhone());
        otherParams.put(PlaBroApi.APP_SOURCE, "android");
        ProfileClass profile = AppController.getInstance().getUserProfile();
        if (null != profile) {
            otherParams.put("userid", profile.getAuthorid() + "");
        }
        String appendedUrl = url + getParams(otherParams);
        Log.d(TAG, "Appended URL :" + appendedUrl);
        return appendedUrl;
    }

    private String getParams(HashMap<String, String> mParams) {
        boolean firstEntry = true;
        String params = "";
        for (Map.Entry<String, String> entry : mParams.entrySet()) {

            String key = entry.getKey();
            String value = entry.getValue();

            if (firstEntry) {
                params = "?" + key + "=" + value;
                firstEntry = false;
            } else {
                params = params + "&" + key + "=" + Util.getURLEncodedString(value);
            }
        }
        return params;
    }

    public void onBackPressed() {
//        PaymentDialogFragment f = PaymentDialogFragment.getInstance(getString(R.string.are_you_sure), getString(R.string.cancel_bidding), getString(R.string.ok), getString(R.string.cancel), true, new PaymentDialogFragment.DialogListener() {
//            @Override
//            public void onOKClicked() {
//                getActivity().finish();
//            }
//
//            @Override
//            public void onCancelClicked() {
//
//            }
//        });
//        f.show(getChildFragmentManager(), "Bid Cancel");
//        webView.
//        if (webView.canGoBack()) {
//            webView.goBack();
//        } else {
//            getActivity().finish();
//        }
        getActivity().finish();
    }

    public void initProgressWheel() {
        mProgressWheel = (ProgressWheel) findView(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
    }

    @Override
    protected void findViewById() {
        webView = (WebView) findView(R.id.webview);
        webView.setWebViewClient(new MyWebClient());
        webView.getSettings().setJavaScriptEnabled(true);
        if (!inputUrl.startsWith("http")) {
            inputUrl = "http://" + inputUrl;
        }
        initProgressWheel();
        mProgressWheel.spin();
        if (isAppendedUrl) {
            webView.getSettings().setUserAgentString("PlabroAndroid");
            webView.loadUrl(getAppendedUrl(inputUrl));
        } else {
            webView.loadUrl(inputUrl);
        }
    }

    @Override
    protected int getViewId() {
        return R.layout.layout_web_view;
    }
}
