/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.plabro.realestate.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.activity.Profile;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.utilities.ActivityUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FollowersCustomAdapter extends BaseAdapter {

    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<ProfileClass> genResultList = new ArrayList<ProfileClass>();
    private List<ProfileClass> arrayList = new ArrayList<ProfileClass>();

    public FollowersCustomAdapter(Context context, List<ProfileClass> genResultList) {
        mContext = context;
        this.genResultList = genResultList;
        inflater = LayoutInflater.from(mContext);
        arrayList.addAll(genResultList);

    }

    public class ViewHolder {
        TextView mProfileName;
        RoundedImageView mProfileImg;
        RelativeLayout mfollowersLayout;
    }

    @Override
    public int getCount() {
        return genResultList.size();
    }

    @Override
    public ProfileClass getItem(int position) {
        return genResultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.row_follow, null);
            // Locate the TextViews in listview_item.xml
            holder.mProfileName = (TextView) view.findViewById(R.id.tv_name);
            holder.mProfileImg = (RoundedImageView) view.findViewById(R.id.iv_follow);
//            holder.mfollowersLayout = (RelativeLayout)view.findViewById(R.id.ll_one);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        // Set the results into TextViews
        String displayName = ActivityUtils.getDisplayNameFromPhone(genResultList.get(position).getPhone());
        if(TextUtils.isEmpty(displayName)){
            displayName = genResultList.get(position).getName();
        }
        holder.mProfileName.setText(displayName);


//        holder.mProfileImg.setDefaultImageResId(R.drawable.profile_default_one);
//        holder.mProfileImg.setImage(genResultList.get(position).getImg(), AppController.getInstance().getImageLoader());
        String imgurl = genResultList.get(position).getImg();
        if (null != imgurl && !TextUtils.isEmpty(imgurl)) {
            Picasso.with(mContext).load(imgurl).placeholder(R.drawable.profile_default_one).into(holder.mProfileImg);
        }
//        // Retrieves an image specified by the URL, displays it in the UI.
//        ImageRequest request = new ImageRequest(genResultList.get(position).getImg(),
//                new Response.Listener<Bitmap>() {
//                    @Override
//                    public void onResponse(Bitmap bitmap) {
//                        holder.mProfileImg.setImageBitmap(bitmap);
//
//                    }
//                }, 200, 200, null,
//                new Response.ErrorListener() {
//                    public void onErrorResponse(VolleyError error) {
//                        holder.mProfileImg.setImageResource();
//
//                        //  mImageView.setImageResource(R.drawable.image_load_error);
//                    }
//                });
//        // imageView.setScaleType(ImageView.ScaleType.FIT_XY);
//        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        AppController.getInstance().addToRequestQueue(request, "Image");
//
//


        // Listen for ListView Item Click
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {


                Intent intent1 = new Intent(mContext, Profile.class);
                intent1.putExtra("authorid", genResultList.get(position).getAuthorid());
                mContext.startActivity(intent1);


            }
        });

        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        genResultList.clear();
        if (arrayList.size() > 0) {
            if (charText.length() == 0) {
                genResultList.addAll(arrayList);
            } else {
                for (ProfileClass gsd : arrayList) {
                    if (gsd.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        genResultList.add(gsd);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }


    public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
        // TODO Auto-generated method stub
        int targetWidth = 50;
        int targetHeight = 50;
        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                targetHeight, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(targetBitmap);
        Path path = new Path();
        path.addCircle(((float) targetWidth - 1) / 2,
                ((float) targetHeight - 1) / 2,
                (Math.min(((float) targetWidth),
                        ((float) targetHeight)) / 2),
                Path.Direction.CCW);

        canvas.clipPath(path);
        Bitmap sourceBitmap = scaleBitmapImage;
        canvas.drawBitmap(sourceBitmap,
                new Rect(0, 0, sourceBitmap.getWidth(),
                        sourceBitmap.getHeight()),
                new Rect(0, 0, targetWidth,
                        targetHeight), null);
        return targetBitmap;
    }

}