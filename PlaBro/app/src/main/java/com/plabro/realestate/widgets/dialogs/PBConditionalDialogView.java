package com.plabro.realestate.widgets.dialogs;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.plabro.realestate.AppController;
import com.plabro.realestate.R;
import com.plabro.realestate.utilities.Util;


public class PBConditionalDialogView extends RelativeLayout implements OnClickListener {

    public enum ConditionalDialog {
        SERVER_ERROR,
        INTERNET_CONNECTION,
        INTERNET_CHECKING,
        LOADING,
        SESSION_EXPIRED

    }

    CharSequence msg = "", buttonText;
    TextView tvConditionalMsg;
    ImageView mIcon;
    TextView mRefresh;

    ConditionalDialog conditionalDialogType = ConditionalDialog.INTERNET_CONNECTION;

    public PBConditionalDialogView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(attrs, defStyle);
    }

    public PBConditionalDialogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(attrs, 0);
    }

    public PBConditionalDialogView(Context context) {
        super(context);
        initViews(null, 0);
    }

    private void initViews(AttributeSet attrs, int defStyle) {

        LayoutInflater.from(getContext()).inflate(R.layout.layout_conditional_screen, this, true);

        if (attrs != null) {
            initAttributes(attrs, defStyle);
        }

        tvConditionalMsg = (TextView) findViewById(R.id.tv_conditional_message);
        mIcon = (ImageView) findViewById(R.id.iv_conditional_icon);
        mRefresh = (TextView) findViewById(R.id.btn_conditional_button);

        tvConditionalMsg.setText(msg);
        mRefresh.setText(buttonText);

        mRefresh.setOnClickListener(this);
        setType(conditionalDialogType);

        setVisibility(View.GONE);

        setClickable(true);
        setFocusable(true);
        setFocusableInTouchMode(true);

    }

    private void initAttributes(AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.PBConditionalView, defStyle, 0);

            final int N = a.getIndexCount();
            for (int i = 0; i < N; i++) {

                int attr = a.getIndex(i);

                switch (attr) {
                    case R.styleable.PBConditionalView_viewMessage:

                        msg = a.getText(attr);

                        break;
                    case R.styleable.PBConditionalView_viewButtonMessage:

                        buttonText = a.getText(attr);

                        break;
                    case R.styleable.PBConditionalView_viewLoaderBackgroundVisible:

                        // TODO
                        break;

                    case R.styleable.PBConditionalView_viewType:

                        switch (a.getInt(attr, 0)) {
                            case 1:
                                conditionalDialogType = ConditionalDialog.SERVER_ERROR;

                                break;
                            case 2:
                                conditionalDialogType = ConditionalDialog.INTERNET_CONNECTION;
                                break;
                            case 3:
                                conditionalDialogType = ConditionalDialog.INTERNET_CHECKING;
                                break;
                            case 4:
                                conditionalDialogType = ConditionalDialog.LOADING;
                                break;
                            case 5:
                                conditionalDialogType = ConditionalDialog.SESSION_EXPIRED;
                                break;


                            default:
                                break;
                        }

                        break;

                    default:
                        break;
                }
            }
            a.recycle();
        }
    }

    public void setType(ConditionalDialog type) {
        conditionalDialogType = type;
        if (type == ConditionalDialog.SERVER_ERROR) {

            //mIcon.setImageResource(R.drawable.tc_condition_graphic_serverdown);
            tvConditionalMsg.setText(R.string.server_down);
            mRefresh.setText("TRY AGAIN");
            mRefresh.setVisibility(View.VISIBLE);

        } else if (type == ConditionalDialog.INTERNET_CONNECTION) {

            mIcon.setImageResource(R.drawable.ic_no_internet);
            tvConditionalMsg.setText(R.string.no_network_connection);
            mRefresh.setText("TRY AGAIN");
            mRefresh.setVisibility(View.VISIBLE);

        } else if (type == ConditionalDialog.INTERNET_CHECKING) {

//            mIcon.setImageResource(R.drawable.tc_condition_graphic_netdown_loading);
            tvConditionalMsg.setText(R.string.checking);
            mRefresh.setText("");
            mRefresh.setVisibility(View.GONE);

        } else if (type == ConditionalDialog.SESSION_EXPIRED) {

            //mIcon.setImageResource(R.drawable.tc_condition_graphic_serverdown);
            tvConditionalMsg.setText(R.string.session_expired);
            mRefresh.setText("TRY AGAIN");
            mRefresh.setVisibility(View.VISIBLE);

        } else if (type == ConditionalDialog.LOADING) {

            // TODO need to manage this condition as performance improvement

        }
    }

    public interface OnPBConditionListener {
        public void onRefresh();
    }

    OnPBConditionListener onPBConditionListener = null;

    public void setOnPBConditionListener(OnPBConditionListener onPBConditionListener) {
        this.onPBConditionListener = onPBConditionListener;
    }

    @Override
    public void onClick(View v) {
        if (v == mRefresh) {
            // if current dialog is Network Error, then first we need to show Internet checking dialog for some time then


            if (conditionalDialogType == ConditionalDialog.INTERNET_CONNECTION) {
                if (Util.haveNetworkConnection(AppController.getInstance())) {

                    //setType(ConditionalDialog.INTERNET_CHECKING);
                    if (onPBConditionListener != null) {
                        onPBConditionListener.onRefresh();
                    }
                } else {
                    Toast.makeText(getContext(), "No Internet Available", Toast.LENGTH_LONG).show();
                }

            } else {
                if (onPBConditionListener != null) {
                    onPBConditionListener.onRefresh();
                }
            }

        }


    }
}
