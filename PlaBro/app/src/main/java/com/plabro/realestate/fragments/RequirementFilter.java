package com.plabro.realestate.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.models.location_auto.LocationsAuto;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jmd on 5/12/2015.
 */
public class RequirementFilter extends PlabroBaseFragment implements TextWatcher, CustomListAdapter.CustomListAdapterInterface {
    private ArrayList<CheckBox> propertyTypes = new ArrayList<>();
    private AutoCompleteTextView localityET;
    private TextView priceRangeTV, areaRangeTV;
    //private RangeBar priceRangeBar, areaRangeBar;
    private Button searchPropertyBtn;
    private CustomListAdapter<LocationsAuto> localityAdapter;
    private ArrayList<LocationsAuto> localities = new ArrayList<>();

    private static String[] buyPriceText = new String[]{
            "0", "5000", "10000", "15000", "20000", "25000", "30000", "40000", "50000", "60000", "75000",
            "90000", "1 L", "1.5 L", "2 L", "5 L", "10 L", "10+ L"
    };
    private static String[] areaRangeText = new String[]{
            "0", "100", "200", "300", "500", "800", "1000", "2000", "3000", "5000", "7000",
            "10000", "12000", "15000", "15000+"
    };

    private static final int THRESHOLD = 3;
    private boolean isAvailable = true;
    private TextView tv_avail, tv_req;


    private int minPrice = 0, maxPrice = buyPriceText.length - 1, minArea = 0, maxArea = areaRangeText.length - 1;

    @Override
    protected int getViewId() {
        return R.layout.layout_search_filter;
    }

    public static PlabroBaseFragment getInstance(Bundle b) {
        RequirementFilter f = new RequirementFilter();
        f.setArguments(b);
        return f;
    }

    @Override
    protected void findViewById() {
        Log.i(TAG, "initViews");
       /* addPropertyTypeCheckBox(R.id.cv_flat);
        addPropertyTypeCheckBox(R.id.cv_house_or_villa);
        addPropertyTypeCheckBox(R.id.cv_office_space);
        addPropertyTypeCheckBox(R.id.cv_other_commercial);
        addPropertyTypeCheckBox(R.id.cv_plot);
        addPropertyTypeCheckBox(R.id.cv_shop_or_showroom);

        localityET = (AutoCompleteTextView) findView(R.id.et_locality);
        localityET.setThreshold(THRESHOLD);
        localityET.addTextChangedListener(this);
        localityET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                localityET.setText(localities.get(i).getTitle());
            }
        });
        setLocalityAdapter();
        priceRangeTV = (TextView) findView(R.id.tv_priceRange);
        areaRangeTV = (TextView) findView(R.id.tv_areaRange);
        priceRangeBar = (RangeBar) findView(R.id.rangebar_price);
        areaRangeBar = (RangeBar) findView(R.id.rangebar_area);
        setClickListeners(R.id.btn_searchPropoerty);

        initAreaRangeBar();
        initPriceRangeBar();
        priceRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int i, int i1, String s, String s1) {
                minPrice = rangeBar.getLeftIndex();
                maxPrice = rangeBar.getRightIndex();
                setPriceRangeText();
            }
        });
        areaRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int i, int i1, String s, String s1) {
                minArea = rangeBar.getLeftIndex();
                maxArea = rangeBar.getRightIndex();
                setAreaRangeText();
            }
        });*/
        setClickListeners(R.id.btn_clearProperty, R.id.btn_searchProperty, R.id.tv_filter_one, R.id.tv_filter_two);
        tv_avail = (TextView) findView(R.id.tv_filter_one);
        tv_req = (TextView) findView(R.id.tv_filter_two);
        tv_avail.setText("Purchase");
    }

    private void setLocalityAdapter() {
        localityAdapter = new CustomListAdapter<>(getActivity(), android.R.layout.simple_list_item_1, localities, this);
        localityET.setAdapter(localityAdapter);
    }

//    private void initPriceRangeBar() {
//        priceRangeBar.setTickStart(minPrice);
//        priceRangeBar.setTickEnd(maxPrice);
//        priceRangeBar.setTickInterval(1);
//        priceRangeBar.setPinColor(getResources().getColor(R.color.white));
//        setPriceRangeText();
//    }
//
//    private void initAreaRangeBar() {
//
//        areaRangeBar.setTickStart(minArea);
//        areaRangeBar.setTickEnd(maxArea);
//        areaRangeBar.setTickInterval(1);
//        areaRangeBar.setPinColor(getResources().getColor(R.color.white));
//        setAreaRangeText();
//    }

    private void setPriceRangeText() {
        String minPriceText = "Rs. " + buyPriceText[minPrice];
        String maxPriceText = "Rs. " + buyPriceText[maxPrice];
        String wholeText = "PRICE: " + minPriceText + " To " + maxPriceText;
        priceRangeTV.setText(ActivityUtils.getSppnnableStrings(wholeText, getResources().getColor(R.color.black), minPriceText, maxPriceText));

    }

    private void setAreaRangeText() {
        String minPriceText = areaRangeText[minArea];
        String maxPriceText = areaRangeText[maxArea];
        String wholeText = "AREA (Sq. ft.): " + minPriceText + " To " + maxPriceText;
        areaRangeTV.setText(ActivityUtils.getSppnnableStrings(wholeText, getResources().getColor(R.color.black), minPriceText, maxPriceText));

    }

    @Override
    public String getTAG() {
        return "RequirementFilter";
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_clearProperty:

                String filterJsonCleared = "";
                Intent i2 = new Intent();
                i2.putExtra(Constants.BUNDLE_KEY_FILTER_JSON, filterJsonCleared);
                getActivity().setResult(Activity.RESULT_OK, i2);
                getActivity().finish();

                break;
            case R.id.btn_searchProperty:
                try {

                    String filterJson = createPropertyJson("req", "");
                    Intent i = new Intent();
                    i.putExtra(Constants.BUNDLE_KEY_FILTER_JSON, filterJson);
                    getActivity().setResult(Activity.RESULT_OK, i);
                    getActivity().finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tv_filter_one:
                findView(R.id.iv_filter_one).setVisibility(View.VISIBLE);
                findView(R.id.iv_filter_two).setVisibility(View.GONE);
                tv_avail.setBackgroundColor(getResources().getColor(R.color.white));
                tv_req.setBackgroundColor(0);
                isAvailable = true;
                break;
            case R.id.tv_filter_two:
                findView(R.id.iv_filter_one).setVisibility(View.GONE);
                findView(R.id.iv_filter_two).setVisibility(View.VISIBLE);
                tv_req.setBackgroundColor(getResources().getColor(R.color.white));
                tv_avail.setBackgroundColor(0);
                isAvailable = false;
                break;
        }
    }

    private void addPropertyTypeCheckBox(int viewId) {
        CheckBox cb = (CheckBox) findView(viewId);
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

            }
        });
        propertyTypes.add(cb);
    }

    private String createPropertyJson(String filterType, String locality) throws JSONException {
        /*JSONObject rootObj = new JSONObject();
        rootObj.put("sale_rent", filterType);
        rootObj.put("locality", locality);
        JSONArray proprtyTypeArray = new JSONArray();
        for (CheckBox c : propertyTypes) {
            if (c.isChecked())
                proprtyTypeArray.put(c.getText().toString());
        }
        rootObj.put("type", proprtyTypeArray);
        JSONArray priceRangeObject = new JSONArray();
        priceRangeObject.put(getPriceValue(priceRangeBar.getLeftIndex()));
        priceRangeObject.put(getPriceValue(priceRangeBar.getRightIndex()));

        JSONArray areaRangeObject = new JSONArray();
        areaRangeObject.put(getAreaValue(areaRangeBar.getLeftIndex()));
        areaRangeObject.put(getAreaValue(areaRangeBar.getRightIndex()));

        rootObj.put("price", priceRangeObject);
        rootObj.put("area", areaRangeObject);
        Log.i("filterJSON", rootObj + "");

        rootObj.put("avail_req", "Available");
        return rootObj.toString();*/
        JSONArray array = new JSONArray();
        array.put(filterType);
        if (isAvailable) {
            array.put("purchase");
        } else {
            array.put("rent");
        }
        return array.toString();
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        if (charSequence.length() >= localityET.getThreshold()) {
            getLocalityData(charSequence.toString());
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void getLocalityData(String keyStr) {
        HashMap<String, String> params = new HashMap<>();
        params.put("key_str", keyStr);
        params = Utility.getInitialParams(PlaBroApi.RT.LOCATION_AUTO, params);

//        AppVolleyCacheManager.processRequest(false, null, PlaBroApi.getBaseUrl(getActivity()), params, RequestMethod.GET, new ResponseListener() {
//            @Override
//            public void onSuccessResponse(String response) {
//                if (response != null && !response.equals("")) {
//                    Gson gson = new Gson();
//                    Log.i(TAG, "" + response);
//                    LocationAutoResponse feedResponse = gson.fromJson(response, LocationAutoResponse.class);
//                    if (feedResponse != null && feedResponse.isStatus()) {
//                        localities = (ArrayList<LocationsAuto>) feedResponse.getOutput_params().getData();
//                        if (localities != null) {
//                            localityAdapter.notifyDataSetChanged();
//                            setLocalityAdapter();
//                        }
//                    }
//                }
//
//            }
//
//            @Override
//            public void onFailureResponse(VolleyError volleyError) {
//
//            }
//        });
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getActivity()).inflate(resourceID, null);

        }
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        tv.setText(localities.get(position).getTitle());
        return convertView;
    }

    private long getPriceValue(int index) {
        if (index == 0) {
            return 0;
        } else if (index == buyPriceText.length - 1) {
            return Long.MAX_VALUE;
        }
        String text = buyPriceText[index];
        String splittedString[] = text.split("\\s");
        if (splittedString.length == 1) {
            return Long.parseLong(splittedString[0]);
        } else {
            // If it is two
            String numPart = splittedString[0];
            String wordPart = splittedString[1];
            double number = Double.parseDouble(numPart);
            return Math.round(number * getNumberMultiplier(wordPart));

        }
    }

    private long getNumberMultiplier(String wordText) {
        if (wordText.startsWith("L")) {
            return 100000;
        } else if (wordText.startsWith("M")) {
            return 1000000;
        } else if (wordText.startsWith("Cr")) {
            return 10000000;
        } else if (wordText.startsWith("T") || wordText.startsWith("K")) {
            return 1000;
        } else if (wordText.contains("H")) {
            return 100;
        } else {
            return 1;
        }
    }

    private long getAreaValue(int index) {
        if (index == 0) {
            return 0;
        } else if (index == areaRangeText.length - 1) {
            return Long.MAX_VALUE;
        }
        return Long.parseLong(areaRangeText[index]);
    }
}
