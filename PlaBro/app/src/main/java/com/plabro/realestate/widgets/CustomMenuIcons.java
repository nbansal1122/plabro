package com.plabro.realestate.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.plabro.realestate.models.services.Widget;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 18/03/16.
 */
public class CustomMenuIcons {
    private List<Widget> widgets = new ArrayList<>();
    private List<Widget> loadedWidgets = new ArrayList<>();
    private WidgetLoadListener listener;


    private Context ctx;
    public CustomMenuIcons(Context ctx, List<Widget> widgets, WidgetLoadListener listener){
        this.ctx = ctx;
        this.widgets = widgets;
        this.listener = listener;
    }

    public void loadWidgets() {
        for (Widget w : widgets) {
            addWidget(w);
        }
    }

    private void addWidget(final Widget widget) {
        Picasso.with(ctx).load(widget.getImage()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Drawable d = new BitmapDrawable(bitmap);
                widget.setDrawable(d);
                loadedWidgets.add(widget);
                listener.onWidgetLoaded(loadedWidgets);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }


    public static interface WidgetLoadListener{
        public void onWidgetLoaded(List<Widget> loadedWidgets);
    }
}
