package com.plabro.realestate.uiHelpers.images.zoom;

public interface IDisposable {

	void dispose();
}
