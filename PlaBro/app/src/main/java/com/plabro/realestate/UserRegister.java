package com.plabro.realestate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.registerUser.RegisterResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.receivers.PBLocalReceiver;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.BitmapUtil;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;


public class UserRegister extends PlabroBaseActivity {

    private static String TAG = "UserRegister";
    private Button mCall, mEmail, mSend, mGotIt;
    private ImageButton mBCardUpload;
    private Activity mActivity;
    //image selector
    private Uri mImageCaptureUri;
    private ImageView mImageView;
    private AlertDialog dialog;
    Boolean isProfileImageSelected = false;
    private TextView mUploadMsg;
    private FrameLayout mOR;
    private RelativeLayout mSuccess, mInital;
    //image from source type
    private static final int PICK_FROM_CAMERA = 1;
    private static final int CROP_FROM_CAMERA = 2;
    private static final int PICK_FROM_FILE = 3;

    private Bitmap capturedBitmap;
    String mCurrentPhotoPath;

    static final int REQUEST_TAKE_PHOTO = 1;
    File photoFile = null;

    @Override
    protected String getTag() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);
        mActivity = UserRegister.this;
        findViewById();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_register, menu);
        return true;
    }

    public void findViewById() {
        mCall = (Button) findViewById(R.id.btn_call_customer_care);
        mEmail = (Button) findViewById(R.id.btn_email_customer_care);
        mSend = (Button) findViewById(R.id.btn_upload);
        mGotIt = (Button) findViewById(R.id.btn_got_it);
        mBCardUpload = (ImageButton) findViewById(R.id.btn_upload_business_btn);
        mInital = (RelativeLayout) findViewById(R.id.rl_initial_layout);
        mSuccess = (RelativeLayout) findViewById(R.id.rl_success);

        mImageView = (ImageView) findViewById(R.id.card_image);
        mOR = (FrameLayout) findViewById(R.id.ur_msg_4);
        mUploadMsg = (TextView) findViewById(R.id.ur_msg_3);

        setup_profile_image_listener();
        mSend.setOnClickListener(genericClickListener);
        mCall.setOnClickListener(genericClickListener);
        mEmail.setOnClickListener(genericClickListener);
        mGotIt.setOnClickListener(genericClickListener);
        mBCardUpload.setOnClickListener(genericClickListener);
        mImageView.setOnClickListener(genericClickListener);
        mBCardUpload.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {

                switch (arg1.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Animation animZoomOut = ((PlabroBaseActivity) UserRegister.this).getAnimation(PlabroBaseActivity.AnimationDef.ZOOM_OUT_LOW);
                        arg0.startAnimation(animZoomOut);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        Animation animZoomIn = ((PlabroBaseActivity) UserRegister.this).getAnimation(PlabroBaseActivity.AnimationDef.ZOOM_IN_LOW);
                        arg0.startAnimation(animZoomIn);
                        break;
                    default:
                        break;
                }

                return false;
            }

        });


    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }


    View.OnClickListener genericClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_call_customer_care:
                    ActivityUtils.callPhone(mActivity, Constants.PLABRO_CUSTOMER_CARE);
                    break;
                case R.id.btn_email_customer_care:
                    PBSharingManager.sendMail(mActivity, Constants.INVITE_MAIL, "Registration Request", "Please approve my registration. \n" + "Phone:" + PBPreferences.getPhone(), "Registration Request");
                    break;
                case R.id.btn_upload_business_btn:
                    dispatchTakePictureIntent();
                    //  dialog.show();
                    break;

                case R.id.card_image:
                    dispatchTakePictureIntent();
                    // dialog.show();
                    break;
                case R.id.btn_upload:
                    uploadImage();

                    break;
                case R.id.btn_got_it:
                    finish();
                    break;


            }
        }
    };


    //add various image source listener,cropping functionality and image show on page
    void setup_profile_image_listener() {

        //image selector
        HashMap<String, Object> data = new HashMap<>();
        data.put("ScreenName", TAG);
        data.put("Action", "Select");
        Analytics.trackEventWithProperties(R.string.profile, R.string.e_edit_image, data);

        final String[] items = new String[]{"Take from camera", "Select from gallery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) { //pick from camera
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(),
                            "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));

                    // Analytics.sendScreenName(getString(R.string.funnel_USER_IMAGE_CLICKED));
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

                    try {
                        intent.putExtra("return-data", true);

                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                } else { //pick from file

                    Intent intent = new Intent();
                    intent.setPackage("com.google.android.apps.plus");
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    //  sendScreenName(R.string.funnel_USER_IMAGE_CLICKED);
                    startActivityForResult(Intent.createChooser(intent,
                            "Select Picture"), PICK_FROM_FILE);

                }
            }
        });

        dialog = builder.create();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;

        switch (requestCode) {

            case REQUEST_TAKE_PHOTO:
                Log.i(TAG, "onActivityResult: " + this);
                if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
                    setPic();
                }

                break;

        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    /**
     * http://developer.android.com/training/camera/photobasics.html
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory() + "/plabro";
        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();

        File image = new File(storageDir + "/" + imageFileName + ".jpg");

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.i(TAG, "photo path = " + mCurrentPhotoPath);
        return image;
    }


    public String getEncodedPicture() {

        if (null != capturedBitmap) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            capturedBitmap.compress(Bitmap.CompressFormat.PNG, 0, baos);
            byte[] b = baos.toByteArray();
            String temp = Base64.encodeToString(b, Base64.DEFAULT);

            return temp;
        } else {
            return "";
        }


    }

    void uploadImage() {

        if (!Util.haveNetworkConnection(mActivity)) {

            //  showConditionalMessage(PBConditionalDialogView.ConditionalDialog.INTERNET_CONNECTION, true, false);
            // mConditionalView.setVisibility(View.VISIBLE);

            return;
        }

        final ProgressDialog progress;
        progress = ProgressDialog.show(mActivity, "Please wait",
                "Uploading business card...", true);

        HashMap<String, String> params = new HashMap<String, String>();

        params.put(PlaBroApi.BC_UPLOAD_PARAM_IMEI, PBPreferences.getImei());
        params.put(PlaBroApi.BC_UPLOAD_PARAM_PHONE, PBPreferences.getPhone());

        if (null != capturedBitmap) {
            params.put(PlaBroApi.BC_UPLOAD_PARAM_IMG, getEncodedPicture());
            HashMap<String, Object> data = new HashMap<>();
            data.put("ScreenName", TAG);
            data.put("Action", "Upload");
            // Analytics.trackEventWithProperties(R.string.profile, R.string.e_edit_image, data);

        }

        params = Utility.getInitialParams(PlaBroApi.RT.BUSINESSCARDUPLOAD, params);

        AppVolley.processRequest(Constants.TASK_CODES.BUSINESS_CARD_UPLOAD, RegisterResponse.class, null, PlaBroApi.getBaseUrl(mActivity), params, RequestMethod.POST, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);


                if (response != null) {
                    mSuccess.setVisibility(View.VISIBLE);
                    mInital.setVisibility(View.GONE);
                    PBPreferences.setTrialVersionState(1);
                    Intent i = new Intent();
                    i.setAction(PBLocalReceiver.PBActionListener.ACTION_REGISTRATION_UPDATE);
                    ActivityUtils.sendLocalBroadcast(UserRegister.this, i);
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                mSend.setText("Try Again");
                mSend.setBackgroundColor(getResources().getColor(R.color.red));
                Utility.dismissProgress(progress);


            }
        });


    }

    @Override
    public void onBackPressed() {
        if (isProfileImageSelected) {
            //reset activity
            finish();
            startActivity(getIntent());

        } else {
            super.onBackPressed();
        }
    }

    private void setPic() {
        try {
            // Get the dimensions of the View
            int targetW =
                    BitmapUtil.convertDpToPixels(this, R.dimen.wt_card_image);
            int targetH =  BitmapUtil.convertDpToPixels(this, R.dimen.ht_card_image);

            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;

            // Determine how much to scale down the image
            int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor << 1;
            bmOptions.inPurgeable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

            Matrix mtx = new Matrix();
            //  mtx.postRotate(90);
            // Rotating Bitmap
            Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);

            if (rotatedBMP != bitmap) {
                bitmap.recycle();
            }

            if (rotatedBMP != null) {
                mImageView.setImageBitmap(rotatedBMP);
                mOR.setVisibility(View.GONE);
                mSend.setVisibility(View.VISIBLE);
                mUploadMsg.setText(getResources().getString(R.string.ur_info_5));
                isProfileImageSelected = true;

            } else {
                mUploadMsg.setText(getResources().getString(R.string.ur_click_pic));
                isProfileImageSelected = false;
            }


            capturedBitmap = rotatedBMP;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Toast.makeText(UserRegister.this, "Error capturing picture!PLease try again.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

}
