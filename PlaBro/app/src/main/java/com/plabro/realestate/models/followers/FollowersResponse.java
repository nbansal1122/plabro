package com.plabro.realestate.models.followers;


import com.plabro.realestate.models.ServerResponse;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class FollowersResponse extends ServerResponse {

    FollowersOutputParams output_params = new FollowersOutputParams();


    public FollowersOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(FollowersOutputParams output_params) {
        this.output_params = output_params;
    }

}
