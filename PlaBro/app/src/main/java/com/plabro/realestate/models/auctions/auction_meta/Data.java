package com.plabro.realestate.models.auctions.auction_meta;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Data {

    @SerializedName("bids_info")
    @Expose
    private List<String> bids_info = new ArrayList<String>();

    /**
     * @return The bids_info
     */
    public List<String> getBids_info() {
        return bids_info;
    }

    /**
     * @param bids_info The bids_info
     */
    public void setBids_info(List<String> bids_info) {
        this.bids_info = bids_info;
    }

}