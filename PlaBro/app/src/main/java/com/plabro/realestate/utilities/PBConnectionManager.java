package com.plabro.realestate.utilities;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;
import android.text.TextUtils;

import com.activeandroid.query.Select;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.listeners.MessageListenerImpl;
import com.plabro.realestate.models.chat.ChatMessage;
import com.plabro.realestate.xmpp.ReadReceipts.ReadReceipt;
import com.plabro.realestate.xmpp.ReadReceipts.ReadReceiptManager;
import com.plabro.realestate.xmpp.RosterManager;

import org.jivesoftware.smack.AndroidConnectionConfiguration;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.SmackAndroid;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.OfflineMessageManager;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;

import java.io.File;
import java.security.Provider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class PBConnectionManager {

    private static final String TAG = PBConnectionManager.class.getSimpleName();
    private String host = "chat.plabro.com";
    private String port = "5222";
    private String service = "plabro.com";

    private String username;
    private String password;
    private Connection connection;
    private static AndroidConnectionConfiguration configuration;
    private static PBConnectionManager instance = null;
    private Context ctx;
    private Provider pm;
    boolean isConnecting;

    public static interface XMPPConnectionListener {
        public void onConnectionCreated(Connection conn);

        public void onConnectionClosed(Connection conn);
    }

    private ArrayList<XMPPConnectionListener> connectionCallbacks = new ArrayList<XMPPConnectionListener>();

    public void setConnectionListener(XMPPConnectionListener listener) {
        if (listener != null)
            connectionCallbacks.add(listener);
    }

    public void removeConnectionListener(XMPPConnectionListener callback) {
        connectionCallbacks.remove(callback);
    }

    protected PBConnectionManager(Context ctx) {

        SmackAndroid.init(ctx);
        this.ctx = ctx;

        username = PBPreferences.getPhone();
        password = PBPreferences.getPhone();
    }


    public static PBConnectionManager getInstance(Application ctx) {
        return getInstance(ctx, null);
    }

    public static PBConnectionManager getInstance(Application ctx, XMPPConnectionListener listener) {
        if (instance == null) {
            instance = new PBConnectionManager(ctx.getApplicationContext());
        }
        if (listener != null) {
            instance.setConnectionListener(listener);

        }
        if (instance.connection != null && instance.connection.isConnected() && instance.connection.isAuthenticated()) {
            if (listener != null)
                listener.onConnectionCreated(instance.connection);

        } else {
            //instance.connectInBackground();
        }
        return instance;
    }


    public Connection getConnection() {

        return connection;
    }

    public void setConnection() {

    }


    public void connectInBackground() {

        if (!isConnecting) {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected String doInBackground(Void... params) {
                    try {
                        if (!isConnecting) {
                            isConnecting = true;
                            if (connection == null) {
                                Log.d(TAG, "XMPP Connection is Null");
                                initialize();
                            } else if (!connection.isConnected()) {
                                Log.d(TAG, "XMPP Connection is not Null & trying to connect");
                                connect();

                                if (connection.isAuthenticated()) {
                                    Log.d(TAG, "XMPP USer is Authenticated");
                                } else {
                                    Log.d(TAG, "XMPP USer is not Authenticated");
                                    Log.d(TAG, connection.getUser() + " -- User");
                                    loginUser();
                                }

                            } else if (!connection.isAuthenticated()) {
                                loginUser();
                            } else {
                                return "-1";
                            }
                        } else {
                            Log.e(TAG, "Already Connecting");
                            return "-1";
                        }

                        return "";

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        return "-1";
                    }
                }

                @Override
                protected void onPostExecute(String val) {

                    if (!"-1".equals(val)) {
                        if (connection != null && connection.isConnected() && connection.isAuthenticated()) {
                            sendConnectionCreatedCallbacks();
                        } else {
                            sendConnectionFailed();
                        }
                    }
                    isConnecting = false;

                }
            }.execute();
        } else {
            Log.d(TAG, "Connecting to XMPP");
        }
    }

    public void initialize() {
        try {
            setConfiguration();
            connect();
            loginUser();
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
    }

    public void connect() {
        try {
            configureProvideManager();

//
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            if (connection == null)
                connection = new XMPPConnection(configuration);


            if (!connection.isConnected() && !connection.isAuthenticated())
                connection.connect();
            if (connection.isConnected()) {
                connection.addConnectionListener(listener);
            }
            Log.i(TAG, "[SettingsDialog] Connected to " + connection.getConnectionID() + " : " + connection.getHost()
                    + " : " + connection.getServiceName());


        } catch (Exception e) {
            Log.e(TAG, "[SettingsDialog] Failed to connect to ");
            //Log.e(TAG, e.toString());
            e.printStackTrace();
            //sendConnectionFailed();
        }

    }


    public void setConfiguration() {

        if (configuration == null) {
            String hostUrl = PBPreferences.getData(PBPreferences.CHAT_URL, host);
            Log.d(TAG, "Chat URL :" + hostUrl);
            hostUrl = TextUtils.isEmpty(hostUrl) ? host : hostUrl;
            configuration = new AndroidConnectionConfiguration(hostUrl, Integer.parseInt(port), service);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                Log.d(TAG, "PATH :If");
                configuration.setTruststoreType("AndroidCAStore");
                configuration.setTruststorePassword(null);
                configuration.setTruststorePath(null);
            } else {
                Log.d(TAG, "PATH :Else");
                configuration.setTruststoreType("BKS");
                String path = System.getProperty("javax.net.ssl.trustStore");
                if (path == null)
                    path = System.getProperty("java.home") + File.separator + "etc" + File.separator + "security" + File.separator
                            + "cacerts.bks";
                Log.d(TAG, "PATH :" + path);
                configuration.setTruststorePath(path);
            }
            configuration.setSendPresence(true);

            configuration.setSecurityMode(ConnectionConfiguration.SecurityMode.enabled);
            configuration.setSASLAuthenticationEnabled(true);
            //getBoolean("xmpp.sasl-enabled",false));
            configuration.setReconnectionAllowed(false);
            configuration.setDebuggerEnabled(false);
        }
    }

   /* private void addMessageProvider() {
        MessageEventManager messageEventManager = new MessageEventManager(connection);
        messageEventManager.addMessageEventNotificationListener(m_messageEventNotificationListener);
        messageEventManager.addMessageEventRequestListener(m_DefaultMessageEventRequestListener);

    }*/


    private void configureProvideManager() {
        ProviderManager pm = ProviderManager.getInstance();
        //configure(pm);
        ChatStateExtension.Provider p = new ChatStateExtension.Provider();
        pm.addExtensionProvider("active", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
        pm.addExtensionProvider("composing", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
        pm.addExtensionProvider("paused", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
        pm.addExtensionProvider("inactive", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
        pm.addExtensionProvider("gone", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
        pm.addIQProvider("offline", "http://jabber.org/protocol/offline",
                new OfflineMessageRequest.Provider()); // Offline Message
        // Indicator
        pm.addExtensionProvider("offline",
                "http://jabber.org/protocol/offline",
                new OfflineMessageInfo.Provider()); // Last Activity
        pm.addIQProvider("query", "jabber:iq:last", new LastActivity.Provider());
        pm.addExtensionProvider(DeliveryReceipt.ELEMENT, DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
        pm.addExtensionProvider(DeliveryReceiptRequest.ELEMENT, new DeliveryReceiptRequest().getNamespace(), new DeliveryReceiptRequest.Provider());

        pm.addExtensionProvider("x", "jabber:x:event", new MessageEventProvider());

//        //  XHTML
//        pm.addExtensionProvider("html","http://jabber.org/protocol/xhtml-im", new XHTMLExtensionProvider());
//
//        //  Group Chat Invitations
//        pm.addExtensionProvider("x","jabber:x:conference", new GroupChatInvitation.Provider());
//
//        //  Service Discovery # Items
//        pm.addIQProvider("query","http://jabber.org/protocol/disco#items", new DiscoverItemsProvider());
//
//        //  Service Discovery # Info
//        pm.addIQProvider("query","http://jabber.org/protocol/disco#info", new DiscoverInfoProvider());
//
//        //  Data Forms
//        pm.addExtensionProvider("x","jabber:x:data", new DataFormProvider());
//
//        //  MUC User
//        pm.addExtensionProvider("x","http://jabber.org/protocol/muc#user", new MUCUserProvider());
//
//        //  MUC Admin
//        pm.addIQProvider("query","http://jabber.org/protocol/muc#admin", new MUCAdminProvider());
//
//        //  MUC Owner
//        pm.addIQProvider("query","http://jabber.org/protocol/muc#owner", new MUCOwnerProvider());




        pm.addExtensionProvider(ReadReceipt.ELEMENT, ReadReceipt.NAMESPACE, new ReadReceipt.Provider());
    }



    public static void handleOfflineMessages(Connection connection) throws Exception {
        OfflineMessageManager offlineMessageManager = new OfflineMessageManager(connection);
        if (offlineMessageManager.getMessageCount() == 0) {
            Log.d(TAG, "No offline messages found on server");
        } else {
            Log.d(TAG, "offline messages found on server");
            Iterator<Message> msgs = offlineMessageManager.getMessages();
            while (msgs.hasNext()) {
                Message msg = msgs.next();
                String fullJid = msg.getFrom();
                String bareJid = StringUtils.parseBareAddress(fullJid);
                String messageBody = msg.getBody();
                if (messageBody != null) {
                    Log.d(TAG, "Retrieved offline message from " + messageBody);
                }
            }
            offlineMessageManager.deleteMessages();
        }
    }

    private void addPacketListener() {
        PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
        connection.addPacketListener(new PacketListener() {
            @Override
            public void processPacket(Packet packet) {
                Message msg = (Message) packet;
                Log.d(TAG, "Packet :" + packet.toXML());
                Log.d(TAG, "Packet ID :" + packet.getPacketID());
                if (msg.getBody() != null) {
                    MessageListenerImpl.processIncomingMessage(msg, connection, ctx);
                } else {
                    Collection<PacketExtension> extensions = msg.getExtensions();
                    Iterator<PacketExtension> it = extensions.iterator();
                    while (it.hasNext()) {
                        PacketExtension ext = it.next();
                        switch (ext.getElementName()) {
                            case ReadReceipt.ELEMENT:
                                handleReadReceiptMessage(msg, ext);
                                break;
                            case DeliveryReceipt.ELEMENT:
                                handleDeliveryReceiptMessage(msg, ext);
                                break;
                        }
                    }

                }
                Log.i(TAG,
                        "Reciveing text [" + msg.getBody() + "] " + msg.getFrom() + ", To :" + msg.getTo() + ", msgtype" + msg.getType());
            }
        }, filter);

    }

    public static void handleDeliveryReceiptMessage(Message msg, PacketExtension ext) {
        String packetIdAndMessageType = msg.getPacketID() + ChatMessage.MESSAGE_OUTGOING;
        List<ChatMessage> chatMessages = new Select().from(ChatMessage.class).where("packetIdAndMessageType = '" + packetIdAndMessageType + "'").execute();

        if((chatMessages == null || chatMessages.size()<1)&& ext instanceof DeliveryReceipt ){
            packetIdAndMessageType = ((DeliveryReceipt)ext).getId() + ChatMessage.MESSAGE_OUTGOING;
            chatMessages = new Select().from(ChatMessage.class).where("packetIdAndMessageType = '" + packetIdAndMessageType + "'").execute();
        }

        if (chatMessages != null && chatMessages.size() > 0) {
            for (ChatMessage m : chatMessages) {
                m.setStatus(ChatMessage.STATUS_DELIVERED);
                m.save();
            }
        }
    }

    public static void handleReadReceiptMessage(Message message, PacketExtension ext) {
        ChatMessage msg = new Select().from(ChatMessage.class).where("packetIdAndMessageType = '" + message.getPacketID() + ChatMessage.MESSAGE_OUTGOING + "'").executeSingle();
        if (msg == null && ext instanceof ReadReceipt) {
            msg = new Select().from(ChatMessage.class).where("packetIdAndMessageType = '" + ((ReadReceipt) ext).getId() + ChatMessage.MESSAGE_OUTGOING + "'").executeSingle();
        }
        if (msg != null) {
            Log.d(TAG, "Saving read message");
            msg.setStatus(ChatMessage.STATUS_READ);
            msg.save();
        } else {
            Log.d(TAG, "Read Message is null");
        }
    }

    public void loginUser() {
        if (connection != null && connection.isConnected() && !connection.isAuthenticated())
            try {
                Log.d(TAG, "Trying to Login : " + username + ".. " + password);
                Log.d(TAG, "Is Logged in : " + connection.isAuthenticated());
                if (!connection.isAuthenticated())
                    connection.login(username, password);
                //configureProvideManager();

                if (!connection.isAuthenticated()) {
                    Log.d(TAG, "Returning Back Connection is not logged in");
                    return;
                }
                Log.i(TAG, "Logged in as " + connection.getUser());

                //addMessageProvider();
                addPacketListener();
                connection.sendPacket(new Presence(Presence.Type.available));

                connection.getRoster().setSubscriptionMode(Roster.SubscriptionMode.accept_all);

                // Set the status to available
                Presence presence = new Presence(Presence.Type.available);
                connection.sendPacket(presence);
                Log.i("check", "connection complete without error");
                try {
                    handleOfflineMessages(connection);
                } catch (Exception e) {
                    Log.e(TAG, "Error While getigin offline messages");
                    e.printStackTrace();
                }
           /* if (null != connection) {
                //Read reciepts on todo
                DeliveryReceiptManager.getInstanceFor(connection).enableAutoReceipts();
            }*/
                RosterManager.initialize(connection);
                //MUCManager.getInstance(connection);
                ReadReceiptManager.getInstanceFor(connection).addReadReceivedListener(readRecListener);

                DeliveryReceiptManager dm = DeliveryReceiptManager.getInstanceFor(connection);
                dm.enableAutoReceipts();
                dm.registerReceiptReceivedListener(recieptListener);

            } catch (Exception ex) {
                //Crittercism.failTransaction("Chat");

                Log.e(TAG, "[SettingsDialog] Failed to log in as " + username);
                Log.e(TAG, ex.toString());

            }
    }

    private static DeliveryReceiptManager.ReceiptReceivedListener recieptListener = new DeliveryReceiptManager.ReceiptReceivedListener() {
        @Override
        public void onReceiptReceived(String s, String s1, String s2) {
            Log.d(TAG, "Message Received:" + s + "... " + s1 + "... " + s2);
            String packetIdAndMessageType = s2 + ChatMessage.MESSAGE_OUTGOING;
            List<ChatMessage> chatMessages = new Select().from(ChatMessage.class).where("packetIdAndMessageType = '" + packetIdAndMessageType + "'").execute();
            if (chatMessages != null && chatMessages.size() > 0) {
                for (ChatMessage m : chatMessages) {
                    m.setStatus(ChatMessage.STATUS_DELIVERED);
                    m.save();
                }
            }

        }
    };

    DeliveryReceiptManager.ReceiptReceivedListener readRecListener = new DeliveryReceiptManager.ReceiptReceivedListener() {
        @Override
        public void onReceiptReceived(String from, String to, String packetId) {
            Log.d(TAG, "On Receipt received");
            ChatMessage msg = new Select().from(ChatMessage.class).where("packetIdAndMessageType = '" + packetId + ChatMessage.MESSAGE_OUTGOING + "'").executeSingle();
            if (msg != null) {
                msg.setStatus(ChatMessage.STATUS_READ);
                msg.save();
            }
            Log.d(TAG, "Read Receipt:" + from + " -- To : " + to + " -- " + packetId);
        }
    };

    private void addChatManager() {

        connection.getChatManager().addChatListener(new ChatManagerListener() {
            @Override
            public void chatCreated(Chat chat, boolean b) {

            }
        });
    }

    private ConnectionListener listener = new ConnectionListener() {
        @Override
        public void connectionClosed() {
            Log.d(TAG, "Connection Closed");
        }

        @Override
        public void connectionClosedOnError(Exception e) {
            Log.d(TAG, "Connection ClosedOnError " + e.getLocalizedMessage());
            e.printStackTrace();
            connection.disconnect();
//            if (Util.haveNetworkConnection(AppController.getInstance().getApplicationContext())) {
//                connectInBackground();
//            }
            sendConnectionFailed();
        }

        @Override
        public void reconnectingIn(int i) {
            Log.d(TAG, "ReConnecting");
        }

        @Override
        public void reconnectionSuccessful() {
            Log.d(TAG, "ReConnection Successful");
            if (null != connection && connection.isConnected()) {
                if (connection.isAuthenticated()) {
                    sendConnectionCreatedCallbacks();
                }
            }

        }

        @Override
        public void reconnectionFailed(Exception e) {
            Log.d(TAG, "ReConnection Failed");
        }
    };

    private void sendConnectionCreatedCallbacks() {
        Log.d(TAG, "Sending Connection Created Callbacks  Current Thread : " + Thread.currentThread().getName());
        try {
            if (connection != null && connection.isConnected() && connection.isAuthenticated()) {

                ListIterator<XMPPConnectionListener> it = connectionCallbacks.listIterator();
                while (it.hasNext()) {
                    XMPPConnectionListener listener = it.next();
                    if (listener != null)
                        listener.onConnectionCreated(connection);
                }
            } else {
                //this.connectInBackground();
                Log.d(TAG, "Sending Connection Created Callbacks: Connection Null");
            }
        } catch (Exception e) {
            Log.d(TAG, "Exception at SendConnection CreatedCallbacks" + e + ";;;" + e.getLocalizedMessage());
        }
    }

    private void sendCallback(XMPPConnectionListener listener) {
        listener.onConnectionCreated(connection);
    }

    private void sendConnectionFailed() {
        Log.d(TAG, "Sending Connection Failed Callbacks");
        for (XMPPConnectionListener callbacks : connectionCallbacks) {
            callbacks.onConnectionClosed(connection);
        }
    }

//    ReadReceiptReceivedListener readListener = new ReadReceiptReceivedListener()
//    {
//        @Override
//        public void onReceiptReceived(String fromJid, String toJid, String packetId)
//        {
//            Log.i("Read", "Message Read Successfully");
//        }
//    };


}
