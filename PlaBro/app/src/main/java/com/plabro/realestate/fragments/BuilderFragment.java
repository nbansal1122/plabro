package com.plabro.realestate.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.adapter.FeedsCustomAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolleyCacheManager;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nitin on 10/08/15.
 */
public class BuilderFragment extends CardListFragment implements HomeFragment.CityRefreshListener {

    FeedsCustomAdapter adapter;
    private String builderId;
    public static final String TAG = "BuilderFragment";
    private static Context mContext;
    private static Toolbar mToolbar;

    @Override
    public String getTAG() {
        return "Builder Fragment";
    }

    public static Fragment getInstance(Bundle b) {
        BuilderFragment f = new BuilderFragment();
        f.setArguments(b);
        return f;
    }

    public static BuilderFragment getInstance(Context mContext, Toolbar mToolbar) {
        Log.i(TAG, "Creating New Instance");
        BuilderFragment mBuilderFragment = new BuilderFragment();
        BuilderFragment.mContext = mContext;
        BuilderFragment.mToolbar = mToolbar;

        return mBuilderFragment;
    }

    @Override
    protected void findViewById() {
        super.findViewById();
        Bundle b = getArguments();
        if (null != b) {
            builderId = b.getString(Constants.BUNDLE_KEYS.BUILDER_ID);
        }
        mShoutButton.setVisibility(View.GONE);

    }

    @Override
    public FeedsCustomAdapter getAdapter() {
        if (null == adapter) {
            adapter = new FeedsCustomAdapter(baseFeeds, getActivity(), "Builders");
        }
        return adapter;
    }

    @Override
    public void getData(final boolean addMore) {
        Bundle b = getArguments();
        if (null != b) {
            builderId = b.getString(Constants.BUNDLE_KEYS.BUILDER_ID);
        }
        showSpinners(addMore);
        HashMap<String, String> params = new HashMap<>();
//        params.put(Constants.CITY_ID, PBPreferences.getData(PBPreferences.CITY_ID, "1"));
        if (addMore) {
            params.put(Constants.START_IDX, "" + page);
        } else {
            params.put(Constants.START_IDX, "0");
        }
        if (!TextUtils.isEmpty(builderId)) {
            params.put(Constants.BUNDLE_KEYS.BUILDER_ID, builderId);
        }
        params = Utility.getInitialParams(PlaBroApi.RT.BUILDER_FEED, params);

        AppVolleyCacheManager.processRequest(Constants.TASK_CODES.BUILDER_FEEDS, FeedResponse.class, !addMore, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                stopSpinners();
                FeedResponse feedResponse = (FeedResponse) response.getResponse();
                if (null != feedResponse.getOutput_params()) {
                    ArrayList<BaseFeed> tempFeeds = (ArrayList<BaseFeed>) feedResponse.getOutput_params().getData();
                    if (null != tempFeeds && tempFeeds.size() > 0) {
                        if (!addMore) {
                            page = 0;
                            baseFeeds.clear();
                        }
                        page = page + 10;
                        baseFeeds.addAll(tempFeeds);
                        adapter.notifyDataSetChanged();
                    }
                }

                if (null != baseFeeds && baseFeeds.size() > 0) {
                    noFeeds.setVisibility(View.GONE);
                } else {
                    noFeeds.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                onApiFailure(response, taskCode);
                if (null != baseFeeds && baseFeeds.size() > 0) {
                    noFeeds.setVisibility(View.GONE);
                } else {
                    noFeeds.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public void onStatusFalse(PBResponse response, int taskCode) {
        super.onStatusFalse(response, taskCode);
        if (null != adapter && adapter.getItemCount() > 0) {
            noFeeds.setVisibility(View.VISIBLE);
        } else {
            noFeeds.setVisibility(View.GONE);
        }

    }

    private void setAdapter() {

    }

    @Override
    protected int getViewId() {
        return R.layout.feeds_fragment;
    }


    @Override
    public void onCityRefresh() {
        Log.d(TAG, "on City Refresh");
//        getData(false);
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Profile Action Received");
        if (Constants.ACTION_PROFILE_UPDATE.equals(intent.getAction())) {
            ((PlabroBaseActivity) getActivity()).setTitle("Builders");
        }
    }

}
