package com.plabro.realestate.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.plabro.realestate.R;

import java.util.ArrayList;

public class LocationAdapter extends BaseAdapter {
    private Context mContext;

    Activity mActivity;
    TextView mLocationItem;
    ArrayList<String> mLocations = new ArrayList<String>();



    private View.OnClickListener customOnClickLsitener = new View.OnClickListener() {

        @Override
        public void onClick(View c_view) {
            switch (c_view.getId()) {
                default:
            }
        }
    };

    public LocationAdapter(Context mContext, Activity ct_act, ArrayList<String> locations) {

        super();
        this.mContext = mContext;
        this.mActivity = ct_act;
        this.mLocations = locations;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        // First let's verify the convertView is not null
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.location_item, null);
        }


        mLocationItem = (TextView)view.findViewById(R.id.tv_location);
        mLocationItem.setText(mLocations.get(position));
        return view;
    }



    @Override
    public int getCount() {
        // return the number of records in cursor
        return mLocations.size();

    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}
