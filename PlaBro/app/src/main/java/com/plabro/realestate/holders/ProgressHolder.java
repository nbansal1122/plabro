package com.plabro.realestate.holders;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.LoaderFeed;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.ProgressWheel;

/**
 * Created by hemant on 18/08/15.
 */
public class ProgressHolder extends BaseFeedHolder {
    protected LinearLayout mLoading, mNoData, mNoMore;
    protected ProgressWheel mProgressWheelLoadMore;


    public ProgressHolder(View itemView) {
        super(itemView);
        mLoading = (LinearLayout) findView(R.id.ll_loading);
        mNoData = (LinearLayout) findView(R.id.ll_no_data);
        mNoMore = (LinearLayout) findView(R.id.ll_no_more);
        mProgressWheelLoadMore = (ProgressWheel) findView(R.id.progress_wheel_load_more);
    }


    @Override
    public void onBindViewHolder(final int position, BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        Log.d(TAG, "Progress Holder:" + feed.getType());
        if (feed instanceof LoaderFeed) {
            final LoaderFeed f = (LoaderFeed) feed;
            Log.d(TAG, "Progress Holder:" + f.getType());
            if (f.getType().equalsIgnoreCase(Constants.FEED_TYPE.PROGRESS_MORE)) {
                mProgressWheelLoadMore.setBarColor(ctx.getResources().getColor(R.color.colorPrimary));
                mProgressWheelLoadMore.setProgress(0.0f);
                mProgressWheelLoadMore.spin();
                mProgressWheelLoadMore.setVisibility(View.VISIBLE);


                mNoData.setVisibility(View.GONE);
                mNoMore.setVisibility(View.GONE);
                mLoading.setVisibility(View.VISIBLE);

            } else if (f.getType().equalsIgnoreCase(Constants.FEED_TYPE.NO_MORE_DATA)) {
                mNoData.setVisibility(View.GONE);
                mLoading.setVisibility(View.GONE);
                mNoMore.setVisibility(View.VISIBLE);
                TextView textView = (TextView) mNoMore.findViewById(R.id.tv_no_data_ing);
                textView.setText("No more " + f.getText());
            } else if (f.getType().equalsIgnoreCase(Constants.FEED_TYPE.NO_MORE)) {
                mNoMore.setVisibility(View.GONE);
                mProgressWheelLoadMore.setVisibility(View.GONE);
                mNoData.setVisibility(View.VISIBLE);
                mLoading.setVisibility(View.GONE);
                mNoData.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ShoutObserver.dispatchGenEvent(Constants.LOAD_MORE.SHOUT);
                    }
                });
            }
        }
    }

    @Override
    public String getTAG() {
        return "Progress Holder";
    }
}
