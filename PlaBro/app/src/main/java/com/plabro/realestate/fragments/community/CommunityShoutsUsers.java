package com.plabro.realestate.fragments.community;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.activity.Shout;
import com.plabro.realestate.adapter.CustomPagerAdapter;
import com.plabro.realestate.adapter.GenSpinnerAdapter;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.models.city.CityClass;
import com.plabro.realestate.models.community.JoinedComm;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.slidingTab.SlidingTabLayout;
import com.plabro.realestate.uiHelpers.view.FontManager;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.Utility;
import com.software.shell.fab.ActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nbansal2211 on 09/06/16.
 */
public class CommunityShoutsUsers extends PlabroBaseFragment implements CustomPagerAdapter.PagerAdapterInterface<String>, ViewPager.OnPageChangeListener {

    private ViewPager mViewPager;
    private ArrayList<String> fragmentNames = new ArrayList<>();
    private CommShoutFragment commShoutFragment;
    private CommUserFragment commUserFragment;
    private CustomPagerAdapter<String> adapter;
    private Toolbar mToolbar;
    private SearchBoxCustom search;
    private static final String TYPEFACE_ROBOTTO_NORMAL = "Fonts/Roboto-Regular.ttf";

    private SlidingTabLayout mSlidingTabLayout;
    private ArrayList<CityClass> cityList;
    private List<String> cityNames = new ArrayList<>();
    private HashMap<String, String> cityVsId = new HashMap<>();
    private Spinner spinner;
    private GenSpinnerAdapter spinnerAdapter;
    private JoinedComm community;

    public static CommunityShoutsUsers getInstance(Toolbar toolbar, SearchBoxCustom searchBoxCustom) {
        CommunityShoutsUsers f = new CommunityShoutsUsers();
        f.mToolbar = toolbar;
        f.search = searchBoxCustom;
        return f;
    }

    public static CommunityShoutsUsers getInstance() {
        CommunityShoutsUsers f = new CommunityShoutsUsers();
        return f;
    }

    @Override
    protected void loadBundleData(Bundle b) {
        community = (JoinedComm) b.getSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        registerLocalReceiver();
    }

    @Override
    public String getTAG() {
        return "CommShoutsUsers";
    }

    @Override
    protected void findViewById() {
        initFragmentNames();
        String cityName = AppController.getInstance().getCityNameOnly();
        setTitle(community.getGroupInfo().getName());
        setPagerAdapter();
        setTabTextView();
        showViews(R.id.frame_post_shout);
        ActionButton mShoutButton = (ActionButton) findView(R.id.rl_post_iv);
        Utility.setupShoutActionButton(mShoutButton, getActivity());
        setClickListeners(R.id.rl_post_iv);
    }

    private void initFragmentNames() {
        fragmentNames.clear();
        fragmentNames.add("Shouts");
        fragmentNames.add("Users");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rl_post_iv:
                Shout.startActivityToPostInGroup(String.valueOf(community.getGroupid()), getActivity());
                break;
        }
    }

    private View spinnerContainer;

    private void setSpinner() {
        if (mToolbar == null) {
            mToolbar = (Toolbar) getActivity().findViewById(R.id.my_awesome_toolbar);
        }
    }

    private void setPagerAdapter() {
        if (null == getActivity()) return;
        mSlidingTabLayout = (SlidingTabLayout) findView(R.id.navig_tab);
        mSlidingTabLayout.setVisibility(View.VISIBLE);
        mViewPager = (ViewPager) findView(R.id.pager);
        adapter = new CustomPagerAdapter<>(getChildFragmentManager(), fragmentNames, this);
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(this);
        mSlidingTabLayout.setViewPager(mViewPager);
    }

    @Override
    protected int getViewId() {
        return R.layout.activity_search_filter;
    }

    @Override
    public Fragment getFragmentItem(int position, String listItem) {
        Fragment f = null;
        Bundle b = new Bundle();
        b.putSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE, community);
        switch (position) {
            case 0:
                if (commShoutFragment == null) {
                    commShoutFragment = (CommShoutFragment) CommShoutFragment.getInstance(b, mToolbar, search);
                }
                return commShoutFragment;
            case 1:
                if (commUserFragment == null) {
                    commUserFragment = (CommUserFragment) CommUserFragment.getInstance(b, mToolbar, search);
                }
                return commUserFragment;
        }
        return f;
    }

    @Override
    public CharSequence getPageTitle(int position, String listItem) {
        return listItem;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    private void setTabTextView() {
        int count = mSlidingTabLayout.getTabStrip().getChildCount();
        int currentPosition = mViewPager.getCurrentItem();
        for (int i = 0; i < count; i++) {
            TextView v = (TextView) mSlidingTabLayout.getTabStrip().getChildAt(i);
            Typeface tf = FontManager.getInstance().getFont(TYPEFACE_ROBOTTO_NORMAL);
            v.setTypeface(tf, Typeface.BOLD);
            if (i == currentPosition)
                v.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            else
                v.setTextColor(getResources().getColor(R.color.tab_text_blue));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    public void onPageSelected(int position) {
        if (position == 1) {
            Analytics.trackScreen(Analytics.OtherScreens.CallLogs);
        }
        Log.d(TAG, "" + position);
        mViewPager.setCurrentItem(position);
        setTabTextView();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        if (Constants.ACTION_PROFILE_UPDATE.equals(intent.getAction())) {
            ((PlabroBaseActivity) getActivity()).setTitle(community.getGroupInfo().getName());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView");
//        mToolbar.removeView(spinnerContainer);
    }
}