package com.plabro.realestate.models.propFeeds;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class HashTags {
    private String title;

    private int _id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }
}
