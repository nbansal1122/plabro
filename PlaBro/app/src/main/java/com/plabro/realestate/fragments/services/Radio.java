package com.plabro.realestate.fragments.services;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.utilities.PBPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 24/11/15.
 */
public class Radio extends BaseServiceFragment {
    private List<String> choices = new ArrayList<>();
    private LinearLayout radioGroup;

    @Override
    public String getTAG() {
        return "Radio";
    }

    @Override
    protected void findViewById() {
        super.findViewById();
        addLayout(getLayout());
    }

    private View getLayout() {
        radioGroup = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.layout_service_linear_content, null);
        choices.clear();
        choices.addAll(q.getChoices());
        for (int i = 0; i < choices.size(); i++) {
            radioGroup.addView(getSelectorView(choices.get(i)));
        }
        return radioGroup;
    }

    private View getSelectorView(String text) {
        RadioButton b = (RadioButton) LayoutInflater.from(getActivity()).inflate(R.layout.layout_radio_button, null);
//        b.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "/Fonts/Roboto-Regular.ttf"));
        b.setText(text);
        selectedValue = PBPreferences.getData(q.getQ_id(), selectedValue);
        Log.d(TAG, "Text Value:" + text + ", selected value:" + selectedValue);
        if (null != text && text.equals(selectedValue)) {
            b.setChecked(true);
            b.setSelected(true);
        }
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < radioGroup.getChildCount(); i++) {
                    RadioButton btn = (RadioButton) radioGroup.getChildAt(i);
                    btn.setSelected(false);
                    btn.setChecked(false);
                }
                RadioButton btn = (RadioButton) view;
                btn.setSelected(true);
                btn.setChecked(true);
                selectedValue = btn.getText().toString();
                PBPreferences.saveData(q.getQ_id(), selectedValue);
            }
        });
        return b;
    }

    @Override
    protected String getSelectedValue() {
        return selectedValue;
    }

    @Override
    protected String getErrorMessage() {
        return "Please select an option";
    }
}
