package com.plabro.realestate.uiHelpers.backendHelpers;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;

import com.plabro.realestate.CustomLogger.Log;

/**
 * Created by hemant on 10/1/15.
 */
public class IncomingSMS extends BroadcastReceiver {

    // Get the object of SmsManager
    final SmsManager sms = SmsManager.getDefault();

    public void onReceive(Context context, Intent intent) {

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

                    //  Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);

                    Intent smsIntent;
                    if (senderNum.contains("PLABRO") || senderNum.contains("plabro")) {
                        if (message.contains("Verification Code")) {
                            smsIntent = getLocalIntentToReceiveSMS(message, true);
                        } else {
                            smsIntent = getLocalIntentToReceiveSMS(message, false);
                        }

                    } else {
                        smsIntent = getLocalIntentToReceiveSMS(message, false);
                    }
                    context.sendBroadcast(smsIntent);

                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" + e);

        }
    }

    private Intent getLocalIntentToReceiveSMS(String message, boolean status) {
        Intent i = new Intent();
        i.setAction("sms_received");
        i.putExtra("message", message);
        i.putExtra("status", status);
        return i;
    }

    public static void disableBroadcastReciever(Context context) {
        ComponentName component = new ComponentName(context, IncomingSMS.class);
        int status = context.getPackageManager().getComponentEnabledSetting(component);
        if (status == PackageManager.COMPONENT_ENABLED_STATE_ENABLED) {
            Log.d("", "receiver is enabled");

            //Disable
            context.getPackageManager().setComponentEnabledSetting(component, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            //Enable
            // context.getPackageManager().setComponentEnabledSetting(component, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
        } else if (status == PackageManager.COMPONENT_ENABLED_STATE_DISABLED) {
            Log.d("", "receiver is disabled");
        }
    }
}
