package com.plabro.realestate.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.fragments.BlogNewsFragment;
import com.plabro.realestate.fragments.ChatsFragment;
import com.plabro.realestate.fragments.DirectListingFragment;
import com.plabro.realestate.fragments.FollowingPagerFragment;
import com.plabro.realestate.fragments.NotifCallFragment;
import com.plabro.realestate.fragments.PBWalletFragment;
import com.plabro.realestate.fragments.auctions.AuctionFragment;
import com.plabro.realestate.fragments.community.CommunityShoutsUsers;
import com.plabro.realestate.fragments.services.MatchMakingFragment;
import com.plabro.realestate.fragments.services.ServiceInfoFragment;
import com.plabro.realestate.fragments.services.ServicesFragment;
import com.plabro.realestate.models.services.Services_list;
import com.plabro.realestate.models.services.Widget;
import com.plabro.realestate.utilities.Constants;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 25/11/15.
 */
public class FragmentContainer extends PlabroBaseActivity {

    public static void startActivity(Context ctx, int fragmentType, Bundle bundle) {
        bundle = (null != bundle) ? bundle : new Bundle();
        bundle.putInt(Constants.BUNDLE_KEYS.FRAGMENT_TYPE, fragmentType);
        Intent i = new Intent(ctx, FragmentContainer.class);
        i.putExtras(bundle);
        ctx.startActivity(i);
    }

    private static Toolbar toolbar;
    private static SearchBoxCustom searchBoxCustom;

    @Override
    protected int getResourceId() {
        return R.layout.fragment_container;
    }

    @Override
    protected String getTag() {
        return "Fragment Container";
    }

    @Override
    public void findViewById() {
        inflateToolbar();
        Bundle b = getIntent().getExtras();
        searchBoxCustom = (SearchBoxCustom) findViewById(R.id.searchbox);
        toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
        Fragment f = getFragment(this, b.getInt(Constants.BUNDLE_KEYS.FRAGMENT_TYPE), b);
        if(f == null){
            finish();
            return;
        }
        f.setArguments(b);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, f).commitAllowingStateLoss();
//        initWidgetList();
    }

    public static Fragment getFragment(Context ctx, int fragmentType, Bundle b) {
        switch (fragmentType) {
            case Constants.FRAGMENT_TYPE.FOLLOW_PAGER_FRAGMENT:
                return FollowingPagerFragment.getInstance(toolbar);
            case Constants.FRAGMENT_TYPE.AUCTION:
                return AuctionFragment.getInstance(toolbar, searchBoxCustom);
            case Constants.FRAGMENT_TYPE.COMMUNITY_SHOUTS_USERS:
                return CommunityShoutsUsers.getInstance(toolbar, searchBoxCustom);
            case Constants.FRAGMENT_TYPE.CHAT:
                return ChatsFragment.getInstance(ctx, toolbar, searchBoxCustom);
            case Constants.FRAGMENT_TYPE.WALLET:
                return PBWalletFragment.getInstance(ctx, toolbar, searchBoxCustom);
            case Constants.FRAGMENT_TYPE.NOTIFICATION:
                return NotifCallFragment.getInstance(toolbar, searchBoxCustom);
            case Constants.FRAGMENT_TYPE.DIRECT_INVENTORY:
                return DirectListingFragment.getInstance(toolbar, searchBoxCustom);
            case Constants.FRAGMENT_TYPE.NEWS:
                return BlogNewsFragment.getInstance(toolbar, searchBoxCustom);
            case Constants.FRAGMENT_TYPE.BLOGS:
                return BlogNewsFragment.getInstance(toolbar, searchBoxCustom);
            case Constants.FRAGMENT_TYPE.SERVICES:
                return new ServicesFragment();
            case Constants.FRAGMENT_TYPE.PLABRO_SCAN:
                return new PlabroWebLogin();
            case Constants.FRAGMENT_TYPE.DIRECT_LISTING:
                return DirectListingFragment.getInstance(toolbar, searchBoxCustom);

            case Constants.FRAGMENT_TYPE.MATCH_MAKING:
                return new MatchMakingFragment();
            case Constants.FRAGMENT_TYPE.SMS_SERVICE:
                return ServiceInfoFragment.getInstance(getServiceInfo(R.drawable.img_empty_screen, (Services_list) b.getSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE)));
            case Constants.FRAGMENT_TYPE.LOAN_SERVICE:
                return ServiceInfoFragment.getInstance(getServiceInfo(R.drawable.img_loan_empty, (Services_list) b.getSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE)));
        }
        return null;
    }

    private static ServiceInfoFragment.ServiceInfo getServiceInfo(int iconId, Services_list service) {
        ServiceInfoFragment.ServiceInfo info = new ServiceInfoFragment.ServiceInfo();
        info.defImageIcon = iconId;
        info.serviceType = service.getS_data().getService_type();
        info.description = service.getS_data().getServiceDescription();
        info.imageUrl = service.getS_data().getImg();
        info.summary = service.getS_data().getServiceSummary();
        return info;
    }

    @Override
    public void reloadRequest() {

    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        if (menu != null)
//            menu.clear();
//        getMenuInflater().inflate(R.menu.menu_home, menu);
//
//        for (int i = 0; i < loadedWidgets.size(); i++) {
//            MenuItem item = menu.add(0, i, i, loadedWidgets.get(i).getText());
//            item.setIcon(loadedWidgets.get(i).getDrawable());
//            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
//        }
//        return true;
//    }


}
