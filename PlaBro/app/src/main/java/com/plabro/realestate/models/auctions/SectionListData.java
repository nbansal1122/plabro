package com.plabro.realestate.models.auctions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nitin on 03/02/16.
 */
public class SectionListData {
    @SerializedName("field_text")
    @Expose
    private String field_text;
    @SerializedName("field_title")
    @Expose
    private String field_title;

    public String getField_text() {
        return field_text;
    }

    public void setField_text(String field_text) {
        this.field_text = field_text;
    }

    public String getField_title() {
        return field_title;
    }

    public void setField_title(String field_title) {
        this.field_title = field_title;
    }
}
