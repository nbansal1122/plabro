package com.plabro.realestate.models.login;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.CustomLogger.Log;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by hemantkumar on 08/01/15.
 */
@Table(name = "Login")
public class Login extends Model {

    @SerializedName("authkey")
    @Expose
    @Column(unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    String authkey = "";
    @SerializedName("expiry")
    @Expose
    @Column
    int expiry = 0;

    public long getExpiryTimeStamp() {
        return expiryTimeStamp;
    }

    public void setExpiryTimeStamp(long expiryTimeStamp) {
        this.expiryTimeStamp = expiryTimeStamp;
    }

    @SerializedName("expiryTimeStamp")
    @Expose
    @Column
    private long expiryTimeStamp;

    @SerializedName("trial_version")
    @Expose
    @Column
    private int trial_version;

    public int getTrial_version() {
//        return 0;
        return trial_version;
    }

    public void setTrial_version(int trial_version) {
        this.trial_version = trial_version;
    }

    public String getAuthkey() {
        return authkey;
    }

    public void setAuthkey(String authkey) {
        this.authkey = authkey;
    }

    public int getExpiry() {
        return expiry;
    }

    public void setExpiry(int expiry) {
        this.expiry = expiry;
    }

    public void saveData() {
        // Add Expiry Time
        Log.d("Login", "Saving Login data");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.SECOND, this.getExpiry());
        this.setExpiryTimeStamp(c.getTimeInMillis());
        //-----------
        this.save();
    }
}
