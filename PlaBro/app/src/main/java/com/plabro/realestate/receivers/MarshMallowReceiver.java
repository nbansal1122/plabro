package com.plabro.realestate.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.plabro.realestate.CustomLogger.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 02/11/15.
 */
public class MarshMallowReceiver extends BroadcastReceiver {
    MMInterFace mmInterFace;

    private List<MarshMallowReceiver> listeners = new ArrayList<MarshMallowReceiver>();

    public MarshMallowReceiver(MMInterFace mmInterFace) {
        this.mmInterFace = mmInterFace;
    }

    public static interface MMInterFace {
        public void onMMPermisssoinResult(int requestCode, boolean status, String message);

        String mmAction = "mmAction";
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("MarshMallowReceiver", intent.getAction());
        if (mmInterFace != null && intent.getExtras() != null) {
            int requestCode = intent.getExtras().getInt("requestCode");
            boolean status = intent.getExtras().getBoolean("requestCode");
            String message = intent.getExtras().getString("requestCode");
            mmInterFace.onMMPermisssoinResult(requestCode, status, message);
        }
    }


}
