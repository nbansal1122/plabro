package com.plabro.realestate.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.login.Login;
import com.plabro.realestate.models.profile.Invitee;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.profile.ProfileResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.MaterialDialog;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.UserRegistrationDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InvitesFragment extends MasterFragment implements VolleyListener {

    SwipeRefreshLayout mSwipeRefreshLayout;

    private static Activity mActivity;
    // private ProgressWheel mProgressWheel;
    private RelativeLayout mProfileLayout;
    private LinearLayout mInviteLayout;
    private Button mInvite;
    private TextView mInviteCounter;
    private String mAuthorId = "";
    public static final String TAG = "InvitesFragment";
    private static Context mContext;
    private static Toolbar mToolbar;
    private static int SECTION = 5;
    private String urlWithoutAuthKey = "";
    private String urlWithAuthKey = "";
    private String invCode;
    private ProgressDialog dialog;

    //    private static final String INVITE_URL_PREFIX = "http://plabro.com/cmpn/";
    private static final String INVITE_URL_PREFIX = "http://app.plab.ro/";


    public static InvitesFragment getInstance(Context mContext, Toolbar mToolbar) {
        Log.i(TAG, "Creating New Instance");
        InvitesFragment mSettingsFragment = new InvitesFragment();
        InvitesFragment.mContext = mContext;
        InvitesFragment.mToolbar = mToolbar;

        return mSettingsFragment;
    }

    private boolean isSettingDebugFlag = false;
    private List<Invitee> consumedIVCodes = new ArrayList<>();
    private List<Invitee> unConsumedIVCodes = new ArrayList<>();
    private int invitesLeft;
    private Invitee currentInvitee;
    private boolean isShortURL;
    private ProfileClass profileClass;

    private void showDialog() {
        if (dialog != null && dialog.isShowing()) {

        } else {
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading...");
            dialog.show();
        }
    }

    private void dismissDialog() {
        if (null != getActivity() && null != dialog && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public String getTAG() {
        return "InvitesFragment";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_invites, container, false);

        findViewById();
        setHasOptionsMenu(true);

        return rootView;

    }

    private void initConsumedInvitees() {
        consumedIVCodes = new ArrayList<>();
        unConsumedIVCodes = new ArrayList<>();
        invitesLeft = 0;
        List<Invitee> invitees = new Select().from(Invitee.class).execute();
        if (invitees != null) {
            for (Invitee i : invitees) {
                if (i.isConsumed()) {
                    Log.d(TAG, "Consumed IV Code:" + i.getInviteeCode());
                    consumedIVCodes.add(i);
                } else {
                    Log.d(TAG, "UnConsumed IV Code:" + i.getInviteeCode());
                    unConsumedIVCodes.add(i);
                    invitesLeft++;
                }
            }
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerLocalReceiver();
    }

    @Override
    protected void findViewById() {
        Analytics.trackScreen(Analytics.OtherScreens.Invites);
        //mProgressWheel = (ProgressWheel) findView(R.id.progress_wheel);
        //mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        // mProgressWheel.setProgress(0.0f);
//        mProgressWheel.spin();
        try {
            RelativeLayout background = (RelativeLayout) findView(R.id.rl_header_iv);
            background.setBackgroundResource(R.drawable.invite_bg);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mProfileLayout = (RelativeLayout) findView(R.id.rl_profile);
        mInviteLayout = (LinearLayout) findView(R.id.rl_invite);
        mInvite = (Button) findView((R.id.invite));
        mInviteCounter = (TextView) findView((R.id.tv_iv_counter));
        mInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int invites = 0;
                if (null != v.getTag()) {
                    invites = (int) v.getTag();
                }
                if (invites > 0) {
                    final MaterialDialog mMaterialDialog = new MaterialDialog(mContext);
                    mMaterialDialog.setTitle("Share Plabro Invite")
                            .setMessage("Grow your network. Invite your friend who wants to try Plabro and collaborate more efficiently. " + invites + " invitees will be auto approved to register.")
                            .setPositiveButton("SEND", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    mMaterialDialog.dismiss();
                                    sendInvite();

                                }
                            })

                            .setNegativeButton("CANCEL", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mMaterialDialog.dismiss();

                                }
                            });

                    mMaterialDialog.show();
                } else {

                    sendInvite();
                }
            }
        });
        setHasOptionsMenu(true);
        setupProfile();
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_invites;
    }

    @Override
    protected void setViewSizes() {

    }

    @Override
    protected void callScreenDataRequest() {

    }

    @Override
    protected void reloadRequest() {
        setupProfile();
    }


    void getUserProfile() {

        if (!Util.haveNetworkConnection(getActivity())) {
            //mProgressWheel.stopSpinning();
            return;
        }

        //mProgressWheel.spin();
        HashMap<String, String> params = new HashMap<String, String>();

        params = Utility.getInitialParams(PlaBroApi.RT.GETPROFILE, params);

        AppVolley.processRequest(Constants.TASK_CODES.USER_PROFILE, ProfileResponse.class, null, String.format(PlaBroApi.getBaseUrl()), params, RequestMethod.GET, this);
    }


    void setupProfile() {


        profileClass = AppController.getInstance().getUserProfile();
        if (profileClass != null) {
            setUserData(profileClass);
        }
        getUserProfile();
    }

    void setUserData(ProfileClass profileClass) {
        if (profileClass != null) { //check if fragment is attached while updating UI

            initConsumedInvitees();
            mAuthorId = profileClass.getAuthorid();
            profileClass.setInvites(0);
            mInviteCounter.setVisibility(View.INVISIBLE);
            mInvite.setTag(profileClass.getInvites());

            PBPreferences.setNotAuthorId(mAuthorId);
            AppController.getInstance().setUserProfile(profileClass);
        }
    }


    private void invitePeople(String inviteUrl) {
        sendInviteeAndConsumeCount(inviteUrl);
    }


    private void sendInviteeAndConsumeCount(String inviteUrl) {
        if (getActivity() == null) return;
        HashMap<String, Object> data = new HashMap<>();
        data.put(Analytics.Params.PHONE, PBPreferences.getPhone());
        Analytics.trackSocialEvent(Analytics.Actions.Invited, data);
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        final String shareString = "Grow your network. Invite your friend who wants to try Plabro and collaborate more efficiently. Download it today from " + inviteUrl;
        final List<ResolveInfo> activities = getActivity().getPackageManager().queryIntentActivities(sendIntent, 0);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Share with...");
        final CustomListAdapter<ResolveInfo> adapter = new CustomListAdapter<>(getActivity(), android.R.layout.simple_list_item_1, activities, new CustomListAdapter.CustomListAdapterInterface() {
            @Override
            public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
                ResolveInfo info = activities.get(position);
                convertView = LayoutInflater.from(getActivity()).inflate(resourceID, null);
                TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
                tv.setText(info.activityInfo.applicationInfo.loadLabel(getActivity().getPackageManager()).toString());
                Drawable icon = info.activityInfo.applicationInfo.loadIcon(getActivity().getPackageManager());
                tv.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null);
                tv.setCompoundDrawablePadding((int) Utility.convertDpToPixel(10, getActivity()));
                return convertView;
            }
        });
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ResolveInfo info = (ResolveInfo) adapter.getItem(which);
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setClassName(info.activityInfo.packageName, info.activityInfo.name);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, shareString);
                startActivity(intent);
            }
        });
        builder.create().show();
    }


    void sendInvite() {

        String phoneNumber = PBPreferences.getPhone();

        urlWithoutAuthKey = INVITE_URL_PREFIX + phoneNumber;
        //"http://djapi.plabro.com/cmpn/generate?utm_source=source&utm_campaign=camp&utm_term=term&utm_content=content&utm_medium=medium&authkey=
        invitePeople(urlWithoutAuthKey);
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        dismissDialog();
        //mProgressWheel.stopSpinning();
        switch (taskCode) {
            case Constants.TASK_CODES.SHORT_URL:
                invitePeople(urlWithoutAuthKey);
                break;
            case Constants.TASK_CODES.USER_PROFILE:
//                setupProfile();
                break;
        }
        onApiFailure(response, taskCode);
    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        // mProgressWheel.stopSpinning();
        dismissDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.SHORT_URL:
                if (response != null) {
                    try {
                        JSONObject obj = new JSONObject(((String) response.getResponse()));
                        String shortUrl = obj.getString("msg");
                        if (shortUrl != null && !TextUtils.isEmpty(shortUrl)) {
                            isShortURL = true;
                            invitePeople(shortUrl);
                        } else {
                            invitePeople(urlWithoutAuthKey);
                        }
                    } catch (Exception e) {
                        invitePeople(urlWithoutAuthKey);
                    }

                }
                break;
            case Constants.TASK_CODES.USER_PROFILE:
                if (null != response) {
                    ProfileResponse profileResponse = (ProfileResponse) response.getResponse();

                    profileClass = profileResponse.getOutput_params().getData();
                    profileClass.saveData();
                    //AppController.getInstance().setUserProfile(profileClass);
                    setUserData(profileClass);
                }
                break;
        }

    }

    @Override
    public void onStatusFalse(PBResponse response, int taskCode) {
        super.onStatusFalse(response, taskCode);
        //todo if  profileResponse.getMsg().equalsIgnoreCase("Missing AuthKey in the request") call userlogin again
        Utility.userLogin(getActivity());

    }

    @Override
    public void onTrialVersionExpired(PBResponse response, int taskCode) {
        super.onTrialVersionExpired(response, taskCode);
        if (null != getActivity()) {
            showRegistrationCard();
        }
    }

    @Override
    public void onSessionExpiry(PBResponse response, int taskCode) {
        super.onSessionExpiry(response, taskCode);
        Utility.userLogin(getActivity());
    }


    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Profile Action Received");
        if (Constants.ACTION_PROFILE_UPDATE.equals(intent.getAction()) && !isDetached() && !isRemoving()) {
            ((PlabroBaseActivity) getActivity()).setTitle("Invites");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegsiterLocalReceiver();
    }


    protected void showRegistrationCard() {
        int val = PBPreferences.getTrialVersionState();
        if (val == 0) {

            Login login = new Select().from(Login.class).executeSingle();
            if (null != login) {
                val = login.getTrial_version();
            }
        }
        switch (val) {
            case 0:
                Utility.userLogin(getActivity());

                if (!UserRegistrationDialog.isVisible) {
                    UserRegistrationDialog feedPopUpDialog = new UserRegistrationDialog(getActivity(), val);
                    feedPopUpDialog.show(getChildFragmentManager(), "UserRegister");
                }
                break;
            case 1:
                if (!UserRegistrationDialog.isVisible) {
                    UserRegistrationDialog feedPopUpDialog1 = new UserRegistrationDialog(getActivity(), val);
                    feedPopUpDialog1.show(getChildFragmentManager(), "UserRegister");
                }
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }
}

