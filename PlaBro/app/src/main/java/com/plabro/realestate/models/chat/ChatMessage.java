package com.plabro.realestate.models.chat;

import android.database.Cursor;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

/**
 * Created by hemant on 21/4/15.
 */


@Table(name = "ChatRecord")
public class ChatMessage extends Model {

    public static final int MESSAGE_INCOMING = 1;
    public static final int MESSAGE_OUTGOING = 2;

    public static final String STATUS_RECEIVED = "received";
    public static final String STATUS_READ = "read";
    public static final String STATUS_SENT = "sent";
    public static final String STATUS_DELIVERED = "delivered";
    public static final String STATUS_PENDING = "pending";
    public static final String STATUS_LOCAL_READ = "localRead";
    public static final String STATUS_NONE = "none";
    //@Column(unique = true,onUniqueConflict = Column.ConflictAction.REPLACE)
    @Column
    private String packetId;
    @Column
    int _id;
    @Column
    String msg;
    @Column
    String name;
    @Column
    String phone;
    @Column
    String img;
    @Column
    long time;
    @Column
    String authorid;
    @Column
    String userType;

    @Column
    String status = STATUS_PENDING;
    @Column
    int msgType = ChatMessage.MESSAGE_INCOMING;

    public String getPacketIdAndMessageType() {
        return packetIdAndMessageType;
    }

    public void setPacketIdAndMessageType(String packetIdAndMessageType) {
        this.packetIdAndMessageType = packetIdAndMessageType;
    }

    @Column(unique = true,onUniqueConflict = Column.ConflictAction.REPLACE)
    String packetIdAndMessageType;

    public String getAuthorid() {
        return authorid;
    }

    public void setAuthorid(String authorid) {
        this.authorid = authorid;
    }

    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }

    public String getRecipientAuthID() {
        return recipientAuthID;
    }

    public void setRecipientAuthID(String recipientAuthID) {
        this.recipientAuthID = recipientAuthID;
    }

    @Column
    String recipientAuthID;

    public String getRecPhone() {
        return recPhone;
    }

    public void setRecPhone(String recPhone) {
        this.recPhone = recPhone;
    }

    @Column
    String recPhone;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getPacketId() {
        return packetId;
    }

    public void setPacketId(String packetId) {
        this.packetId = packetId;
    }

    public String getAuthId() {
        return authorid;
    }

    public void setAuthId(String authId) {
        this.authorid = authId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
