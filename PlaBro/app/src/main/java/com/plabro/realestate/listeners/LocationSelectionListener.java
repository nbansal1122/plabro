package com.plabro.realestate.listeners;

/**
 * Created by Hemant on 23-01-2015.
 */
public interface LocationSelectionListener {

    public void  onResponse(Boolean status,String response);

}
