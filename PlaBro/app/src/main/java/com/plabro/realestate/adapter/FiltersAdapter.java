
package com.plabro.realestate.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.plabro.realestate.R;
import com.plabro.realestate.listeners.onSearchFilterSelectionListener;
import com.plabro.realestate.models.propFeeds.FilterClass;
import com.plabro.realestate.uiHelpers.view.FontText;

import java.util.HashMap;
import java.util.List;

public class FiltersAdapter extends ArrayAdapter<FilterClass> {
    Context mContext;
    List<FilterClass> mData;
    int selected;
    int deselected;
    Drawable greenBorder, grayBorder;
    onSearchFilterSelectionListener filterSelectionListener;
    private HashMap<String, String> selectedFilters = new HashMap<>();


    public FiltersAdapter(Context context, List<FilterClass> data, onSearchFilterSelectionListener filterSelectionListener, HashMap<String, String> selectedFilters) {
        super(context, R.layout.filter_layout, data);
        mContext = context;
        mData = data;
        selected = context.getResources().getColor(R.color.pbGreenTwo);
        deselected = context.getResources().getColor(R.color.pbGray1);
        greenBorder = context.getResources().getDrawable(R.drawable.green_square_hollow_curved);
        grayBorder = context.getResources().getDrawable(R.drawable.gray_square_hollow_curved);
        this.filterSelectionListener = filterSelectionListener;
        this.selectedFilters = selectedFilters;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.filter_layout, parent, false);


        FontText title = (FontText) rowView.findViewById(R.id.tv_fl_title);
        final FontText valOne = (FontText) rowView.findViewById(R.id.tv_op_one);
        final FontText valTwo = (FontText) rowView.findViewById(R.id.tv_op_two);


        title.setText(mData.get(position).getTitle());
        if (mData.get(position).getValues().size() > 1) {
            valOne.setText(mData.get(position).getValues().get(0).getDisplayname());
            valTwo.setText(mData.get(position).getValues().get(1).getDisplayname());
        }

        if (mData.get(position).isFirstSelected()) {
            valOne.setTextColor(selected);
            valOne.setBackgroundDrawable(greenBorder);
        }

        if (mData.get(position).isSecondSelected()) {
            valTwo.setTextColor(selected);
            valTwo.setBackgroundDrawable(greenBorder);
        }

        valOne.setTag(mData.get(position));
        valTwo.setTag(mData.get(position));

        valOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterClass ff = (FilterClass) view.getTag();


                valTwo.setTextColor(deselected);
                valTwo.setBackgroundDrawable(grayBorder);
                ff.setIsSecondSelected(false);

                if (ff.isFirstSelected()) {
                    valOne.setTextColor(deselected);
                    valOne.setBackgroundDrawable(grayBorder);
                    ff.setIsFirstSelected(false);

                } else {
                    valOne.setTextColor(selected);
                    ff.setIsFirstSelected(true);
                    valOne.setBackgroundDrawable(greenBorder);

                }

                valOne.setTag(ff);
                valTwo.setTag(ff);
                filterSelectionListener.onSelected(true, ff);
            }
        });

        valTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterClass ff = (FilterClass) view.getTag();
                valOne.setTextColor(deselected);
                valOne.setBackgroundDrawable(grayBorder);
                ff.setIsFirstSelected(false);


                if (ff.isSecondSelected()) {
                    valTwo.setTextColor(deselected);
                    valTwo.setBackgroundDrawable(grayBorder);
                    ff.setIsSecondSelected(false);
                } else {
                    valTwo.setTextColor(selected);
                    ff.setIsSecondSelected(true);
                    valTwo.setBackgroundDrawable(greenBorder);
                }

                valOne.setTag(ff);
                valTwo.setTag(ff);
                filterSelectionListener.onSelected(true, ff);
            }
        });
        return rowView;
    }


}