package com.plabro.realestate.models.Calls;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.utilities.Constants;

/**
 * Created by nitin on 20/11/15.
 */
@Table(name = "CallData", id = BaseColumns._ID)
public class CallData extends BaseFeed {

    public static final int OUTGOING_CALL = 2;
    public static final int INCOMING_CALL = 1;
    public static final int MISSED_CALL = 3;

    public static CallData getLastCallDetail(Context mContext) {
        try {
            CallData callDetail = null;
            Log.i("*****retriveCallSummary******", "Call retrive method worked");
            StringBuffer sb = new StringBuffer();
            Uri contacts = CallLog.Calls.CONTENT_URI;
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            Cursor managedCursor = mContext.getContentResolver().query(
                    contacts, null, null, null, CallLog.Calls.DATE + " DESC");
            int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
            int duration1 = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
            int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
            int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
            if (managedCursor.moveToFirst()) {
                callDetail = new CallData();
                String phNumber = managedCursor.getString(number);
                String callDuration = managedCursor.getString(duration1);
                String callType = managedCursor.getString(type);
                long callTime = Long.parseLong(managedCursor.getString(date));
                callDetail.setDuration(callDuration);
                switch (Integer.parseInt(callType)) {
                    case OUTGOING_CALL:
                        callDetail.setCall_type("outgoing");
                        break;
                    case INCOMING_CALL:
                        callDetail.setCall_type("incoming");
                        break;
                    case MISSED_CALL:
                        callDetail.setCall_type("missed");
                        break;
                }
                callDetail.setCallee(phNumber);
                callDetail.setCallTime(callTime);
                String dir = null;
                sb.append("\nPhone Number:--- " + phNumber + " \nCall duration in sec :--- " + callDuration);
                sb.append("\n----------------------------------");
                Log.i("*****Call Summary******", "Call Duration is:-------" + sb);
                Log.d("Call Util", "Call Type:" + callType);
            }
            managedCursor.close();
            return callDetail;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @SerializedName("caller")
    @Expose
    @Column
    private String caller;
    @SerializedName("authorid")
    @Expose
    @Column
    private long authorid;

    public String getSource_page() {
        return source_page;
    }

    public void setSource_page(String source_page) {
        this.source_page = source_page;
    }

    @SerializedName("source_page")
    @Expose
    @Column
    private String source_page = "";

    public long getShoutid() {
        return shoutid;
    }

    public void setShoutid(long shoutid) {
        this.shoutid = shoutid;
    }

    public long getAuthorid() {
        return authorid;
    }

    public void setAuthorid(long authorid) {
        this.authorid = authorid;
    }

    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    @SerializedName("shoutid")
    @Expose
    @Column
    private long shoutid;
    @SerializedName("callee")
    @Expose
    @Column
    private String callee;
    @SerializedName("call_type")
    @Expose
    @Column
    private String call_type;
    @SerializedName("duration")
    @Expose
    @Column
    private String duration;
    @SerializedName("plabro_call")
    @Expose
    @Column
    private Boolean plabro_call;
    @SerializedName("time")
    @Expose
    @Column
    private long callTime;
    @SerializedName("name")
    @Expose
    private String name;

    public String getFeedType() {
        return feedType;
    }

    public void setFeedType(String feedType) {
        this.feedType = feedType;
    }

    private String feedType = Constants.FEED_TYPE.CALL;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @SerializedName("desc")
    @Expose
    private String desc;

    public Boolean getIsFreeCall() {
        return isFreeCall;
    }

    public void setIsFreeCall(Boolean isFreeCall) {
        this.isFreeCall = isFreeCall;
    }

    @SerializedName("isFreeCall")
    @Expose
    @Column
    private Boolean isFreeCall = false;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @SerializedName("img")
    @Expose
    private String img;

    public boolean isFromProfile() {
        return isFromProfile;
    }

    public void setIsFromProfile(boolean isFromProfile) {
        this.isFromProfile = isFromProfile;
    }

    private boolean isFromProfile;

    public long getCallTime() {
        return callTime;
    }

    public void setCallTime(long callTime) {
        this.callTime = callTime;
    }


    public Boolean getPlabro_call() {
        return plabro_call;
    }

    public void setPlabro_call(boolean plabro_call) {
        this.plabro_call = plabro_call;
    }

    public String getCallee() {
        return callee;
    }

    public void setCallee(String callee) {
        this.callee = callee;
    }

    public String getCall_type() {
        return call_type;
    }

    public void setCall_type(String call_type) {
        this.call_type = call_type;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
