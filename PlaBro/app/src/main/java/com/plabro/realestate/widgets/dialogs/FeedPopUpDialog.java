package com.plabro.realestate.widgets.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.ProgressWheel;

/**
 * Created by Hemant on 19-01-2015.
 */
public class FeedPopUpDialog extends DialogFragment {

    private Feeds mFeed;
    private Button mClose;
    private Context mContext;
    private Activity mActivity;
    private TextView mFeedView;
    private ProgressWheel mProgressWheel;
    private View convertView;
    private int hashKey = 0;


    @SuppressLint("ValidFragment")
    public FeedPopUpDialog(Feeds feed, Context context, Activity mActivity, int hashKey) {
        this.mContext = context;
        this.mFeed = feed;
        this.mActivity = mActivity;
        this.hashKey = hashKey;

    }

    public FeedPopUpDialog() {

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        convertView = inflater.inflate(R.layout.feeds_pop_up_dialog, null);
        builder.setView(convertView);
        setCancelable(true);
        if (null == mFeed) {
            dismiss();
            return null;
        }
        mProgressWheel = (ProgressWheel) convertView.findViewById(R.id.progress_wheel);
        mProgressWheel.spin();
        mClose = (Button) convertView.findViewById(R.id.close);
        mFeedView = (TextView) convertView.findViewById(R.id.post_content_full);
        //mFeedView.setText(mFeedText);
        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        String text = mFeed.getText();

        getFeedInBackground(text);

        Dialog d = builder.create();
        d.setCanceledOnTouchOutside(true);

        return d;
    }

    private void getFeedInBackground(final String text) {
        new AsyncTask<Void, Void, SpannableString>() {
            @Override
            protected SpannableString doInBackground(Void... params) {
                try {

                    SpannableString hashTagText = ActivityUtils.setHashTags(mFeed, text, 0, text.length(), getActivity(), mFeedView, "FeedPopUp", 0, true, hashKey,mActivity);
                    return hashTagText;

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;

            }

            @Override
            protected void onPostExecute(SpannableString hashTagText) {

                if (null != hashTagText) {
                    mFeedView.setText(hashTagText, TextView.BufferType.SPANNABLE);

                } else {
                    mFeedView.setText(text);
                }
                mProgressWheel.stopSpinning();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void onStart() {
        super.onStart();

        // safety check
        if (getDialog() == null)
            return;

        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int dialogWidth = display.getWidth(); // specify a value here
        int dialogHeight = display.getHeight()*6 / 10; // specify a value here

        getDialog().getWindow().setLayout(dialogWidth, dialogHeight);
        /*
		 * getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
		 * ViewGroup.LayoutParams.MATCH_PARENT);
		 */

    }

    ;

}
