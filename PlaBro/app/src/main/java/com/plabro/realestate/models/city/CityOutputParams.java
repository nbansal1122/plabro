package com.plabro.realestate.models.city;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class CityOutputParams {

    CityData data;

    public CityData getData() {
        return data;
    }

    public void setData(CityData data) {
        this.data = data;
    }
}
