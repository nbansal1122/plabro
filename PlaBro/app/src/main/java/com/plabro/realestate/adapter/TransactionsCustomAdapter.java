/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.plabro.realestate.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.FeedDetails;
import com.plabro.realestate.entities.MyShoutsDBModel;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.models.shouts.DeleteShoutResponse;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class TransactionsCustomAdapter extends RecyclerView.Adapter<TransactionsCustomAdapter.ViewHolder> implements VolleyListener {

    private static Context mContext;
    private static Activity mActivity;
    private ProgressDialog dialog;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    private String searchText = "";
    private int feedsCount = 0;
    private static final String TAG = "TransactionsCustomAdapter";
    private static int hashKey = 0;

    private ArrayList<Feeds> feeds = new ArrayList<>();
    private ArrayList<Feeds> arrayList = new ArrayList<>();


    public static class ViewHolder extends RecyclerView.ViewHolder {

        View mDivider;
        FontText mDateSep, mDesc, mTransType, mAmount;

        public ViewHolder(View v) {
            super(v);

            mDivider = (View) itemView.findViewById(R.id.div_one);
            mDateSep = (FontText) itemView.findViewById(R.id.tv_date);
            mDesc = (FontText) itemView.findViewById(R.id.tv_desc_summ);
            mTransType = (FontText) itemView.findViewById(R.id.tv_trans_type);
            mAmount = (FontText) itemView.findViewById(R.id.tv_desc_amount);

        }

    }


    public TransactionsCustomAdapter(Context mContext, ArrayList<Feeds> feeds, Activity activity) {
        this.mContext = mContext;
        this.feeds = feeds;
        mActivity = activity;
        feedsCount = feeds.size();
        arrayList.addAll(feeds);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.transactions_layout, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");
        final Feeds feed = feeds.get(position);


    }

    @Override
    public int getItemCount() {
        return feeds.size();
    }

    public void addItems(ArrayList<Feeds> feeds) {
        for (Feeds feeds1 : feeds) {
            addItem(feedsCount, feeds1);
        }

    }

    public void addItem(int position, Feeds feed) {
        feeds.add(position, feed);
        notifyItemInserted(position);
        feedsCount++;
    }

    public void removeItem(int position) {
        feeds.remove(position);
        notifyItemRemoved(position);
    }

    public void removeItemFromList(String shout_id) {

        int pos = 0;
        for (Feeds feed : feeds) {
            if (feed.get_id().equals(shout_id))
                break;
            pos++;
        }

        if (pos < feeds.size()) {
            removeItem(pos);
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            });
        }
    }

    static void deleteShout(final String shout_id) {

        //Crittercism.beginTransaction("Delete");


        final ProgressDialog progress;
        progress = ProgressDialog.show(mActivity, "Please wait",
                "Deleting Shouts...", true);
        progress.setCancelable(true);
        progress.setCanceledOnTouchOutside(true);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.PLABRO_DELETE_SHOUT_PARAM_SHOUT_ID, shout_id);
        params = Utility.getInitialParams(PlaBroApi.RT.DELETE_SHOUTS, params);
        Log.i("PlaBro", "Message id" + shout_id);
        AppVolley.processRequest(1, DeleteShoutResponse.class, null, PlaBroApi.getBaseUrl(mContext), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);
                new Delete().from(MyShoutsDBModel.class).where("_id = ?", shout_id).execute();
                DeleteShoutResponse deleteShoutResponse = (DeleteShoutResponse) response.getResponse();

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);

                switch (response.getCustomException().getExceptionType()) {
                    case SESSION_EXPIRED:
                        showToast(response.getCustomException().getMessage());
                        Utility.userLogin(mContext);
                        break;
                    case STATUS_FALSE_EXCEPTION:
                        showToast(response.getCustomException().getMessage());
                        break;
                    case TRIAL_EXPIRED_EXCEPTION:
                        showToast(response.getCustomException().getMessage());
                        break;
                }

            }

        });

    }

    public static void showToast(String toastMessage) {
        if (null == toastMessage || TextUtils.isEmpty(toastMessage)) {
            toastMessage = "Cannot Delete Shout, Please check your network or try again";
        }
        Toast toast = Toast.makeText(mContext, toastMessage, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);

        LinearLayout layout = (LinearLayout) toast.getView();
        if (layout.getChildCount() > 0) {
            TextView tv = (TextView) layout.getChildAt(0);
            tv.setGravity(Gravity.CENTER);
        }

        toast.show();
    }

    // Filter Class
    public void filter(String charText) {
        searchText = charText.toLowerCase(Locale.getDefault());
        feeds.clear();
        if (arrayList.size() > 0) {
            if (charText.length() == 0) {
                feeds.addAll(arrayList);
            } else {
                for (Feeds gsd : arrayList) {
                    if (gsd.getText().toLowerCase(Locale.getDefault()).contains(searchText)) {
                        feeds.add(gsd);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public int getCurrentItemSize() {
        return arrayList.size();
    }

    public void updateItems(ArrayList<Feeds> feeds, boolean addMore) {
        if (!addMore)
            arrayList.clear();
        arrayList.addAll(feeds);
    }


    private void getShoutDetails(String message_id) {
        ParamObject obj = new ParamObject();
        obj.setClassType(FeedResponse.class);
        obj.setTaskCode(Constants.TASK_CODES.SHOUT_DETAILS);
        HashMap<String, String> params = new HashMap<>();
        params.put(PlaBroApi.PLABRO_FEED_DETAILS_POST_PARAM_MESSAGE_ID, message_id);
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.SHOUT_DETAILS, params));
        showDialog();
        AppVolley.processRequest(obj, this);
    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        hideDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.SHOUT_DETAILS:
                FeedResponse resp = (FeedResponse) response.getResponse();
                if (null != resp.getOutput_params() && null != resp.getOutput_params().getData()) {
                    List<BaseFeed> feeds = resp.getOutput_params().getData();
                    if (feeds.size() > 0) {
                        Feeds feedsObject = (Feeds) feeds.get(0);
                        FeedDetails.startFeedDetails(mContext, feedsObject.get_id(), "", feedsObject);
                    }
                }
                break;
        }
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        hideDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.SHOUT_DETAILS:
                showFailureMessage(response);
                break;
        }
    }

    private void showDialog() {
        if (dialog == null) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Loading...");
            dialog.show();
        } else if (!dialog.isShowing()) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Loading...");
            dialog.show();
        }
    }

    private void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private void showFailureMessage(PBResponse response) {
        showToast(response.getCustomException().getMessage());
    }
}
