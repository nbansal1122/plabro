package com.plabro.realestate.fragments.services;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.utilities.PBPreferences;

/**
 * Created by nitin on 24/11/15.
 */
public class DropDown extends BaseServiceFragment implements CustomListAdapter.CustomListAdapterInterface {
    private Spinner spinner;
    private CustomListAdapter<String> adapter;

    @Override
    public String getTAG() {
        return "Drop Down";
    }

    @Override
    protected void findViewById() {
        super.findViewById();
        addLayout(getLayout());
        selectedValue = PBPreferences.getData(q.getQ_id(), null);
        adapter = new CustomListAdapter<>(getActivity(), android.R.layout.simple_list_item_1, q.getChoices(), this);
        spinner.setAdapter(adapter);
        int selectedItemIndex = -1;
        if (null != selectedValue) {
            for (int i = 0; i < q.getChoices().size(); i++) {
                String s = q.getChoices().get(i);
                if (s.equals(selectedValue)) {
                    selectedItemIndex = i;
                }
            }
            spinner.setSelection(selectedItemIndex);
        }
    }

    private View getLayout() {
        spinner = (Spinner) LayoutInflater.from(getActivity()).inflate(R.layout.layout_drp_down, null);
        spinner.setPrompt(q.getTitle());

        return spinner;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getActivity()).inflate(resourceID, null);
        }
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        tv.setText(q.getChoices().get(position));
        return convertView;
    }


    @Override
    public String getSelectedValue() {
        selectedValue = spinner.getSelectedItem().toString();
        return
                spinner.getSelectedItem().toString();
    }

    @Override
    protected String getErrorMessage() {
        return "Please select an option";
    }
}
