package com.plabro.realestate.activity;

import android.view.View;

import com.plabro.realestate.R;

/**
 * Created by nitin on 08/02/16.
 */
public class ThankYouAuction extends PlabroBaseActivity {
    @Override
    protected String getTag() {
        return "ThankYouAuction";
    }

    @Override
    protected int getResourceId() {
        return R.layout.layout_thanku_auction;
    }

    @Override
    public void findViewById() {
        setClickListeners(R.id.btn_cool);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btn_cool:
                onBackPressed();
                break;
        }
    }

    @Override
    public void reloadRequest() {

    }
}
