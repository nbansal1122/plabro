package com.plabro.realestate.activity;

import android.content.Context;
import android.content.Intent;

import com.plabro.realestate.R;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.utilities.Constants;

/**
 * Created by nitin on 11/12/15.
 */
public class ServiceActivity extends PlabroBaseActivity {

    private int fragmentType;

    public static void startActivity(Context context, String serviceType, int fragmentType) {
        Intent i = new Intent(context, ServiceActivity.class);
        i.putExtra(Constants.BUNDLE_KEYS.FRAGMENT_TYPE, fragmentType);
        i.putExtra(Constants.BUNDLE_KEYS.SERVICE_TYPE, serviceType);
        context.startActivity(i);
    }

    @Override
    protected String getTag() {
        return "Service Info Activity";
    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_service;
    }

    @Override
    public void findViewById() {
        inflateToolbar();
        setTitle(getIntent().getExtras().getString(Constants.BUNDLE_KEYS.SERVICE_TYPE) + "");
        fragmentType = getIntent().getExtras().getInt(Constants.BUNDLE_KEYS.FRAGMENT_TYPE);

    }

    private void getServiceFragment() {
        PlabroBaseFragment f = null;
        switch (fragmentType) {
            case Constants.FRAGMENT_TYPE.LOAN_SERVICE:
                break;
            case Constants.FRAGMENT_TYPE.SMS_SERVICE:
                break;
            case Constants.FRAGMENT_TYPE.MATCH_MAKING:
                break;
            case Constants.FRAGMENT_TYPE.DIRECT_INVENTORY:
                break;
        }
    }

    @Override
    public void reloadRequest() {

    }
}
