package com.plabro.realestate.fragments.related;

import android.content.Context;
import android.os.Handler;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.LoaderFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nitin on 22/09/15.
 */
public class RelatedBrokerFragment extends BaseRelatedFragment {

    public static RelatedBrokerFragment getInstance(Context mContext, String msgId, int headerHeight) {
        //  Log.i(TAG, "Creating New Instance");
        RelatedBrokerFragment mRelatedFeeds = new RelatedBrokerFragment();
        mRelatedFeeds.mContext = mContext;
        mRelatedFeeds.message_id = msgId;
        mRelatedFeeds.headerHeight = headerHeight;
        return mRelatedFeeds;
    }

    @Override
    public String getTAG() {
        return "Related Broker";
    }

    public void getData(final boolean addMore) {
        sendScreenName(R.string.funnel_property_feed_requested);
        isLoading = true;

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("message_id", message_id);

        if (addMore) {
            params.put("startidx", (page) + "");
        } else {
            params.put("startidx", "0");
        }

        params = Utility.getInitialParams(PlaBroApi.RT.RELATED_BROKERS, params);
        AppVolley.processRequest(1, FeedResponse.class, null, String.format(PlaBroApi.getBaseUrl()), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {

                isLoading = false;

                FeedResponse feedResponse = (FeedResponse) response.getResponse();

                ArrayList<BaseFeed> tempFeeds = (ArrayList<BaseFeed>) feedResponse.getOutput_params().getData();

                if (null != tempFeeds && tempFeeds.size() > 0) {
                    for (BaseFeed f : tempFeeds) {
                        if (Constants.FEED_TYPE.BROKER.equalsIgnoreCase(f.getType())) {
                            f.setType(Constants.FEED_TYPE.RELATED_BROKER);
                        }
                    }
                }
                if (tempFeeds.size() > 0) {

                    LoaderFeed lf = new LoaderFeed();
                    if (tempFeeds.size() < Util.getDefaultStartIdx()) {
                        lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                        lf.setText(Constants.EmtyText.BROKERS);
                    } else {
                        lf.setType(Constants.FEED_TYPE.PROGRESS_MORE);
                    }

                    if (addMore) {
                        feeds2.remove(feeds2.size() - 1);//for footer
                        feeds2.addAll(tempFeeds);
                        feeds2.add(lf);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        page = 0;
                        feeds2.clear();
                        feeds2.addAll(tempFeeds);
                        feeds2.add(lf);
                        if (mAdapter != null) {
                            mAdapter.notifyDataSetChanged();
                        }
                    }


                    page = Util.getStartIndex(page);
                    //page scrolled for next set of data


                } else {
//                        if (!addMore) {
//                            comingSoon = true;
//                        }

                    LoaderFeed lf = new LoaderFeed();
                    feeds2.remove(feeds2.size() - 1);//for footer
                    lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                    lf.setText(Constants.EmtyText.BROKERS);
                    feeds2.add(lf);
                    mAdapter.notifyDataSetChanged();
                }
                Log.d(TAG, "Setting List Adapter");
                Log.d(TAG, "After Getting data Size is:" + feeds2.size());

            }


            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                isLoading = false;
                onApiFailure(response, taskCode);

                try {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 2000ms
                            LoaderFeed lf = new LoaderFeed();
                            lf.setType(Constants.FEED_TYPE.NO_MORE);

                            feeds2.remove(feeds2.size() - 1);//for footer
                            feeds2.add(lf);
                            mAdapter.notifyDataSetChanged();

                        }
                    }, 2000);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


    }

}
