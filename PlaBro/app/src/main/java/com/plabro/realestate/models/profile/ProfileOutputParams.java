package com.plabro.realestate.models.profile;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class ProfileOutputParams {

    ProfileClass data = new ProfileClass();

    public ProfileClass getData() {
        return data;
    }

    public void setData(ProfileClass data) {
        this.data = data;
    }
}
