package com.plabro.realestate.utilities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.view.Display;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.Toast;

public class DeviceResourceManager {
	@SuppressWarnings("unused")
	private static final String LOG_TAG = "DeviceResourceManager";

	private Context mContext = null;
	private Display display = null;
	private float mScreenDinsity = 0.0f;

	public DeviceResourceManager(Context context) {
		mContext = context;
	}

	/**
	 * 
	 * @param phoneNumber
	 */
	public void call(String phoneNumber) {
		try {
			Intent intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse("tel:" + phoneNumber));
			mContext.startActivity(intent);
		} catch (Exception e) {
			Toast.makeText(mContext, "Unable to call.Please try later.", Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * 
	 * @param phoneNumber
	 * @param msg
	 */
	public void sendSMS(String phoneNumber, String msg) {

	}

	/**
	 * 
	 * @param emailAddress
	 */
	public void sendEmail(String emailAddress) {
		mContext.startActivity(new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + emailAddress)));
	}

	public void sendEmail(String subject, String emailBody) {
		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.setType("message/rfc822");
		emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { "dev.android.error@gmail.com" });
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, emailBody);
		mContext.startActivity(Intent.createChooser(emailIntent, "Send feedback via..."));
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public int getScreenWidth() {
		if (display == null)
			display = ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

		int screenWidth = 0;
		if (Util.hasHoneycombMR1()) {
			Point size = new Point();
			display.getSize(size);
			screenWidth = size.x;
		} else {
			screenWidth = display.getWidth();
		}

		return screenWidth;
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public int getScreenHeight() {
		if (display == null)
			display = ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

		int screenHeight = 0;
		if (Util.hasHoneycombMR2()) {
			Point size = new Point();
			display.getSize(size);
			screenHeight = size.y;
		} else {
			screenHeight = display.getHeight();
		}

		return screenHeight;
	}

	public int getStatusBarHeight() {
		int statusBarHeight = 0;
		int resourceId = mContext.getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			statusBarHeight = mContext.getResources().getDimensionPixelSize(resourceId);
		}
		return statusBarHeight;
	}

	public float getScreenDensity() {
		if (mScreenDinsity == 0.0f)
			mScreenDinsity = mContext.getResources().getDisplayMetrics().density;
		return mScreenDinsity;
	}

	public int convertDIPToPX(int dipValue) {
		return Math.round(dipValue * getScreenDensity());
	}

	public int getSwipeMinDistance() {
		return ViewConfiguration.get(mContext).getScaledTouchSlop();
	}

}
