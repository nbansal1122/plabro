package com.plabro.realestate.activity;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.HomeActivity;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomCursorAdapter;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.ServerResponse;
import com.plabro.realestate.models.contacts.InviteResponse;
import com.plabro.realestate.models.contacts.NonPlabroContact;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.utils.ContactsQuery;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import static com.plabro.realestate.R.id.rl_select_all;

/**
 * Created by nitin on 23/12/15.
 */
public class GreetingActivity extends PlabroBaseActivity implements LoaderManager.LoaderCallbacks<Cursor>, CustomCursorAdapter.CustomCursorAdapterInterface {
    private List<PhoneContact> contacts = new ArrayList<>();
    private List<PhoneContact> selectedContacts = new ArrayList<>();
    private ListView listView;
    private List<NonPlabroContact> contactList = new ArrayList<>();
    private ProgressDialog dialog;
    private EditText msgText;
    private String msg = "";
    private RelativeLayout selectAll;
    private boolean isAllSelected = false;
    private ImageView selectAllCheckBox;

    private Cursor cursor;

    private HashSet<String> selectedContactIds = new HashSet<>();
    private CustomCursorAdapter cursorAdapter;

    @Override
    protected String getTag() {
        return "Greeting";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greeting);
        listView = (ListView) findViewById(R.id.listView);
        listView.addHeaderView(getHeaderView());
        inflateToolbar();
        setAdapter();
        setTitle(getString(R.string.new_message));
        getSupportLoaderManager().initLoader(0, null, this);
        setFooter(false);

        setClickListeners(R.id.btn_send_invite);
    }

    private View getHeaderView() {
        View headerView = LayoutInflater.from(this).inflate(R.layout.header_greeting, null);
        Log.d(TAG, "Header Height :" + headerView.getHeight());
        msgText = (EditText) headerView.findViewById(R.id.et_greeting_text);
        if (getIntent().getExtras() != null) {
            msg = getIntent().getExtras().getString(Constants.BUNDLE_KEYS.GREETING_MSG, "");
        }
        selectAll = (RelativeLayout) headerView.findViewById(rl_select_all);
        selectAll.setOnClickListener(this);
        msgText.setText(msg);
        selectAllCheckBox = (ImageView) headerView.findViewById(R.id.iv_invitation_checkbox);
//        msgText.setEnabled(false);
        return headerView;
    }

    private void setAdapter() {
        Log.d(TAG, "Setting Adapter" + contacts.size());
//        phoneAdapter = new CustomListAdapter<>(this, R.layout.row_new_invites, contacts, this);

//        selectedCount = 0;
//        setFooter(selectedCount > 0);

        cursorAdapter = new CustomCursorAdapter(this, null, false, R.layout.row_new_invites, this);
        listView.setAdapter(cursorAdapter);
    }


    @Override
    public void findViewById() {

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void bindView(final View convertView, Context arg1, Cursor cursor) {
        final ViewHolder holder = new ViewHolder(convertView);

        final PhoneContact c = new PhoneContact().loadFromCursor(cursor);
        holder.name.setText(c.contactName);
        holder.mobile.setText(c.contactNumber);
        if (selectedContactIds.contains(c.contactId)) {
            holder.imgCheckBox.setImageResource(R.drawable.ic_invitation_selected);
        } else {
            holder.imgCheckBox.setImageResource(R.drawable.ic_invitation_unselected);
        }
        if (isAllSelected) {
            holder.imgCheckBox.setImageResource(R.drawable.ic_invitation_selected);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedContactIds.contains(c.contactId)) {
                    holder.imgCheckBox.setImageResource(R.drawable.ic_invitation_unselected);
                    selectedContactIds.remove(c.contactId);
                } else {
                    holder.imgCheckBox.setImageResource(R.drawable.ic_invitation_selected);
                    selectedContactIds.add(c.contactId);
                    Log.d(TAG, "Adding Contact ID" + c.contactId);
                }
            }
        });
    }

    private class ViewHolder {
        private TextView name, mobile;
        private ImageView imgCheckBox;

        public ViewHolder(View view) {
            name = (TextView) view.findViewById(R.id.tv_inviteeName);
            mobile = (TextView) view.findViewById(R.id.tv_inviteePhone);
            imgCheckBox = (ImageView) view.findViewById(R.id.iv_invitation_checkbox);
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btn_send_invite:
                List<PhoneContact> contactsList = new ArrayList<>();
                if (selectedContactIds.size() > 0) {
                    showDialog();
                    if (cursor != null && cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        do  {
                            String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

                            Log.d(TAG, "Contact ID is :" + contactId);
                            if (selectedContactIds.contains(contactId)) {
                                PhoneContact c = new PhoneContact().loadFromCursor(cursor);
                                contactsList.add(c);
                            }
                        }while(cursor.moveToNext());
                    } else {
                        Log.d(TAG, "Cursor is null or size is zero");
                    }
                } else {
                    showToast("Please select a contact to send greeting");
                }
                msg = msgText.getText().toString();
                if (TextUtils.isEmpty(msg)) {
                    Log.d(TAG, "Please enter some message to send");
                    return;
                }
                sendInvitedUsers(contactsList);
                break;
            case R.id.rl_select_all:
                isAllSelected = !isAllSelected;

                if (null != cursor && cursor.getCount() > 0) {
                    if (isAllSelected) {
                        while (cursor.moveToNext()) {
                            selectedContactIds.add(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID)));
                        }
                    } else {
                        selectedContactIds.clear();
                    }
                }
                if (isAllSelected) {
                    selectAllCheckBox.setImageResource(R.drawable.ic_invitation_selected);
                } else {
                    selectAllCheckBox.setImageResource(R.drawable.ic_invitation_unselected);
                }
                cursorAdapter.notifyDataSetChanged();
                break;
        }
    }

    private String getJsonValue(JSONArray invitedArray) {
        JSONObject root = new JSONObject();
        try {
            root.put("contacts_arr", invitedArray);
            root.put("msg", msg);
            String serviceType = getIntent().getStringExtra(Constants.BUNDLE_KEYS.SERVICE_CODE);
            if (!TextUtils.isEmpty(serviceType))
                root.put(Constants.BUNDLE_KEYS.SERVICE_CODE, serviceType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(TAG, "JSON Value" + root);
        return root.toString();
    }

    private void sendMetaData(final List<PhoneContact> list) {
        HashMap<String, Object> metaData = new HashMap<>();
        metaData.put("source", "Greeting");
        metaData.put("contacts", list.size());
    }


    private void sendInvite(JSONArray invitedArray, final List<PhoneContact> list) {
        sendMetaData(list);
        ParamObject obj = new ParamObject();
        HashMap<String, String> params = new HashMap<>();
        params.put("service_data", getJsonValue(invitedArray));
        params = Utility.getInitialParams(PlaBroApi.RT.HANDLE_SERVICE, params);
        obj.setParams(params);
        obj.setRequestMethod(RequestMethod.POST);
        obj.setClassType(ServerResponse.class);
        obj.setTaskCode(Constants.TASK_CODES.INVITE_USERS);
        showDialog();
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "OnSucess");
                showToast(((ServerResponse) response.getResponse()).getMsg());
                if (isRunning) {
                    dismissDialog();
                    finish();
                }
                setAdapter();
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "onFailure");
                dismissDialog();
                onApiFailure(response, taskCode);
            }
        });
    }

    private void sendInvitedUsers(final List<PhoneContact> list) {

        JSONArray array = new JSONArray();
        for (PhoneContact c : list) {
            array.put(c.contactNumber);
        }

        sendInvite(array, list);

    }


    private void setFooter(boolean isSelected) {
        Button sendInvite = (Button) findViewById(R.id.btn_send_invite);
        sendInvite.setText("SEND SMS");
        hideViews(R.id.btn_skip);
        sendInvite.setVisibility(View.VISIBLE);
        sendInvite.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        sendInvite.setTextColor(getResources().getColor(R.color.white));
        sendInvite.setEnabled(true);
        sendInvite.setOnClickListener(this);
    }


    private void closeActivity() {
        if (null != contactList && contactList.size() < 1) {
            onClick(findViewById(R.id.btn_skip));
        }
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // run on background thread
        return new CursorLoader(this, ContactsQuery.CONTENT_URI,
                ContactsQuery.PROJECTION, ContactsQuery.SELECTION, null,
                ContactsQuery.SORT_ORDER);
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        // Will run on main thread
        Log.d(TAG, "On Load Finished");
        if (null != cursor && cursor.getCount() > 0) {
            this.cursor = cursor;
            cursorAdapter.swapCursor(cursor);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }


    private class ContactsTask extends AsyncTask<Cursor, Void, List<PhoneContact>> {

        @Override
        protected void onPreExecute() {
            showDialog();
        }

        @Override
        protected List<PhoneContact> doInBackground(Cursor... params) {
            Cursor c = params[0];
            List<PhoneContact> contacts = fetchContacts(c);
            return contacts;
        }

        @Override
        protected void onPostExecute(List<PhoneContact> phoneContacts) {
            dismissDialog();
            if (phoneContacts != null && phoneContacts.size() > 0) {
                contacts.addAll(phoneContacts);
                setAdapter();
            }
        }

        private List<PhoneContact> fetchContacts(Cursor cursor) {
            List<PhoneContact> contacts = new ArrayList<PhoneContact>();
            try {
                while (cursor.moveToNext()) {
                    String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    PhoneContact contact = new PhoneContact();
                    contact.contactName = (cursor.getString(ContactsQuery.DISPLAY_NAME));
                    contact.contactId =
                            cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    //
                    //  Get all phone numbers.
                    //
                    ContentResolver cr = GreetingActivity.this.getContentResolver();
                    Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                    if (null != phones && phones.moveToFirst()) {
                        contact.contactNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contacts.add(contact);
                        phones.close();
                    }
                }

                cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return contacts;
        }

    }


    private class PhoneContact {
        private String contactId;
        private String contactName;
        private String contactNumber;
        private boolean isSelected;

        public PhoneContact loadFromCursor(Cursor cursor) {
            String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            this.contactName = (cursor.getString(ContactsQuery.DISPLAY_NAME));
            this.contactId =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            //
            //  Get all phone numbers.
            //
            ContentResolver cr = GreetingActivity.this.getContentResolver();
            Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
            if (null != phones && phones.moveToFirst()) {
                this.contactNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                phones.close();
            }
            return this;
        }
    }
}
