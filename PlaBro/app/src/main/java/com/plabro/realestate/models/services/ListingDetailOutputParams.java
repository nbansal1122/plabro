package com.plabro.realestate.models.services;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.models.propFeeds.listings.ListingFeed;

/**
 * Created by nitin on 19/01/16.
 */
public class ListingDetailOutputParams {
    @SerializedName("data")
    @Expose
    private ListingFeed data;

    public ListingFeed getData() {
        return data;
    }

    public void setData(ListingFeed data) {
        this.data = data;
    }
}
