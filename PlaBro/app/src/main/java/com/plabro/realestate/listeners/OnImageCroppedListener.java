package com.plabro.realestate.listeners;

import android.graphics.Bitmap;

/**
 * Created by Hemant on 23-01-2015.
 */
public interface OnImageCroppedListener {

    public void  onResponse(Boolean response,Bitmap image);

}
