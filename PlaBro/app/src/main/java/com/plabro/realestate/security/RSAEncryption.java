package com.plabro.realestate.security;

import android.util.Base64;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.utilities.PBPreferences;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

public class RSAEncryption {
    private Cipher cipher;
    private KeyPair keys;
    private KeyPairGenerator keyGen;
    private PublicKey publicKey;
    private PrivateKey privateKey;

    public RSAEncryption() throws Exception {
        cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");
    }

    public RSAEncryption(String pubKeyString, String priKeyString) throws Exception {
        setPublicKey(pubKeyString);
        setPrivateKey(priKeyString);
        cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");
    }

    public RSAEncryption(String pubKeyString) throws Exception {
        setPublicKey(pubKeyString);
        cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");
    }

    public void setKeyPair(String pubKey, String priKey) throws Exception {
        byte[] pubKeyBytes = Base64.decode(pubKey, 0);
        byte[] priKeyBytes = Base64.decode(priKey, 0);

        // generate public key
        X509EncodedKeySpec specPub = new X509EncodedKeySpec(pubKeyBytes);
        KeyFactory keyFactoryPub = KeyFactory.getInstance("RSA");
        PublicKey tempPublicKey = keyFactoryPub.generatePublic(specPub);


        // generate private key
        PKCS8EncodedKeySpec specPri = new PKCS8EncodedKeySpec(priKeyBytes);
        KeyFactory keyFactoryPri = KeyFactory.getInstance("RSA");
        PrivateKey tempPrivateKey = keyFactoryPri.generatePrivate(specPri);

        publicKey = tempPublicKey;
        privateKey = tempPrivateKey;
        keys = new KeyPair(tempPublicKey, tempPrivateKey);
    }

    public void setPublicKey(String pubKeyString) throws Exception {
        byte[] pubKeyBytes = Base64.decode(pubKeyString, 0);

        // generate public key
        X509EncodedKeySpec specPub = new X509EncodedKeySpec(pubKeyBytes);
        KeyFactory keyFactoryPub = KeyFactory.getInstance("RSA");
        PublicKey tempPublicKey = keyFactoryPub.generatePublic(specPub);

        publicKey = tempPublicKey;
    }

    public void setPrivateKey(String priKeyString) throws Exception {
        byte[] priKeyBytes = Base64.decode(priKeyString, 0);

        // generate private key
        PKCS8EncodedKeySpec specPri = new PKCS8EncodedKeySpec(priKeyBytes);
        KeyFactory keyFactoryPri = KeyFactory.getInstance("RSA");
        PrivateKey tempPrivateKey = keyFactoryPri.generatePrivate(specPri);
        privateKey = tempPrivateKey;

    }

    public void generateKey() throws Exception {
        keyGen = KeyPairGenerator.getInstance("RSA");
        keys = keyGen.generateKeyPair();
        publicKey = keys.getPublic();
        privateKey = keys.getPrivate();
    }

    public String getPrivateKey() {
        return Base64.encodeToString(privateKey.getEncoded(), 0);
    }

    public String getPublicKey() {
        return Base64.encodeToString(publicKey.getEncoded(), 0);
    }

    public String encrypt(String plainText) throws Exception {
        byte[] plainTextByte = plainText.getBytes();
        // PublicKey key=keys.getPublic();
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] encryptedByte = cipher.doFinal(plainTextByte);
        String encryptedText = Base64.encodeToString(encryptedByte, 0);
        return encryptedText;
    }

    //decrypt aes from public key
    public String decrypt(String encryptedText) throws Exception {
        byte[] encryptedTextByte = Base64.decode(encryptedText, 0);
        // PrivateKey key=keys.getPrivate();
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
        String decryptedText = new String(decryptedByte);
        return decryptedText;
    }

    private String genSignature(String data) throws Exception {
        Signature s = Signature.getInstance("SHA256withRSA");
        // PrivateKey key=keys.getPrivate();
        s.initSign(privateKey);
        byte[] dataByte = data.getBytes();
        s.update(dataByte);
        byte[] encryptedByte = s.sign();
        String signedText = Base64.encodeToString(encryptedByte, 0);
        return signedText;
    }

    public boolean checkSignature(String data, String sign) throws Exception {
        try {
            byte[] dataByte = data.getBytes();
            Signature s = Signature.getInstance("SHA256withRSA");
            s.initVerify(publicKey);
            s.update(dataByte);
            byte[] signByte = Base64.decode(sign, 0);
            boolean status = s.verify(signByte);
            return status;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String EncryptData(String data) {
        String encryptedData = data;

        try {
            String decryptedAes = PBPreferences.getAESKeyDecrypted();
            encryptedData = Encryption.encrypt(data, decryptedAes);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return encryptedData;
    }

    public String DecryptData(String data) {
        String decryptedData = data;

        try {
            String decryptedAes = PBPreferences.getAESKeyDecrypted();
            decryptedData = Encryption.decrypt(data, decryptedAes);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return decryptedData;
    }
}