/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.plabro.realestate.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.Shout;
import com.plabro.realestate.entities.MyDraftsDBModel;
import com.plabro.realestate.listeners.OnChildListener;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DraftsCustomAdapter extends RecyclerView.Adapter<DraftsCustomAdapter.ViewHolder> {

    private Context mContext;
    private Activity mActivity;
    RelativeLayout mDraftsOptionHeader;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    private String searchText = "";
    private int feedsCount = 0;
    private static final String TAG = "DraftsCustomAdapter";
    private List<MyDraftsDBModel> feeds = new ArrayList<>();
    private List<MyDraftsDBModel> arrayList = new ArrayList<>();
    private List<MyDraftsDBModel> feedsSelected = new ArrayList<>();
    OnChildListener childListener;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final RoundedImageView profile_image;
        private final FontText draftText;
        private final RelativeLayout draftLayout, isSendingLayout;


        public ViewHolder(View v) {
            super(v);

            profile_image = (RoundedImageView) itemView.findViewById(R.id.profile_image);
            draftText = (FontText) itemView.findViewById(R.id.tv_draft_text);
            draftLayout = (RelativeLayout) itemView.findViewById(R.id.rl_draft);
            isSendingLayout = (RelativeLayout) itemView.findViewById(R.id.rl_sending);

        }

    }


    public DraftsCustomAdapter(Context mContext, List<MyDraftsDBModel> feeds, Activity activity, RelativeLayout mDraftsOptionHeader, OnChildListener childListener) {
        this.mContext = mContext;
        this.feeds = feeds;
        this.mDraftsOptionHeader = mDraftsOptionHeader;
        mActivity = activity;
        feedsCount = feeds.size();
        arrayList.addAll(feeds);
        setupOptionListener();
        this.childListener = childListener;
    }

    private void setupOptionListener() {

        ((ImageButton) mDraftsOptionHeader.findViewById(R.id.ib_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feedsSelected.clear();
                mDraftsOptionHeader.setVisibility(View.GONE);
                childListener.onResponse(true);

            }
        });

        ((ImageButton) mDraftsOptionHeader.findViewById(R.id.ib_delete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDraftsOptionHeader.setVisibility(View.GONE);
                for (MyDraftsDBModel fd : feedsSelected) {
                    new Delete().from(MyDraftsDBModel.class).where("draftId = ?", fd.getDraftId()).execute();
                    removeItemFromList(fd.getDraftId());
                }
                TextView mSelected = (TextView) mDraftsOptionHeader.findViewById(R.id.tv_selected);
                mSelected.setText(feedsSelected.size() + " Selected");
                childListener.onResponse(true);
            }
        });

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.drafts_cards_layout, viewGroup, false);


        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");
        final MyDraftsDBModel feed = feeds.get(position);
        viewHolder.draftText.setText(feed.getText());

        final AppController mAppController = AppController.getInstance();

        if (mAppController != null && mAppController.getUserProfile() != null) {
            String imgUrl = mAppController.getUserProfile().getImg();
            Picasso.with(mContext).invalidate(imgUrl);
            Picasso.with(mContext).load(imgUrl).into(viewHolder.profile_image, new Callback() {
                @Override
                public void onSuccess() {
                }

                @Override
                public void onError() {
                }
            });
        } else {
            viewHolder.profile_image.setImageResource(R.drawable.profile_default_one);
        }

        if (feed.isSending()) {
            viewHolder.isSendingLayout.setVisibility(View.VISIBLE);
        } else {
            viewHolder.isSendingLayout.setVisibility(View.GONE);
        }

        viewHolder.draftLayout.setTag(position);
        viewHolder.draftLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyDraftsDBModel myDraftsDBModel = feeds.get((int) view.getTag());
                TextView mSelected = (TextView) mDraftsOptionHeader.findViewById(R.id.tv_selected);

                if (feedsSelected.size() < 1) {
                    if (!myDraftsDBModel.isSending()) {
                        Bundle b = new Bundle();
                        b.putSerializable("draftObject", myDraftsDBModel);
                        Intent intent = new Intent(mActivity, Shout.class);
                        intent.putExtra("shoutType", "draft");
                        intent.putExtras(b);
                        mActivity.startActivity(intent);
                        mActivity.overridePendingTransition(R.anim.bottom_in_fast, R.anim.bottom_out_fast);
                    } else {
                        Toast.makeText(mContext, "Cannot edit shout", Toast.LENGTH_SHORT).show();
                    }

                } else {

                    boolean isPreviouslyAdded = false;
                    List<MyDraftsDBModel> selectedFeeds = new ArrayList<MyDraftsDBModel>(feedsSelected);
                    feedsLoop:
                    for (MyDraftsDBModel fd : selectedFeeds) {
                        if (fd.getDraftId() == myDraftsDBModel.getDraftId()) {
                            feedsSelected.remove(fd);
                            isPreviouslyAdded = true;
                        }
                    }
                    if (!isPreviouslyAdded) {
                        feedsSelected.add(myDraftsDBModel);
                        view.setBackgroundColor(mContext.getResources().getColor(R.color.pbGray7));
                        viewHolder.profile_image.setImageResource(R.drawable.ic_check_enabled);
                        viewHolder.profile_image.setPadding(20, 20, 20, 20);

                    } else {
                        viewHolder.profile_image.setPadding(0, 0, 0, 0);
                        view.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
                        if (mAppController != null && mAppController.getUserProfile() != null) {
                            String imgUrl = mAppController.getUserProfile().getImg();
                            Picasso.with(mContext).invalidate(imgUrl);
                            Picasso.with(mContext).load(imgUrl).into(viewHolder.profile_image, new Callback() {
                                @Override
                                public void onSuccess() {
                                }

                                @Override
                                public void onError() {
                                }
                            });
                        } else {
                            viewHolder.profile_image.setImageResource(R.drawable.profile_default_one);
                        }

                    }
                    if (feedsSelected.size() < 1) {
                        mDraftsOptionHeader.setVisibility(View.GONE);
                    } else {
                        mDraftsOptionHeader.setVisibility(View.VISIBLE);

                    }
                    mSelected.setText(feedsSelected.size() + " Selected");

                }

            }
        });

        viewHolder.draftLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                MyDraftsDBModel myDraftsDBModel = feeds.get((int) view.getTag());

                mDraftsOptionHeader.setVisibility(View.VISIBLE);
                TextView mSelected = (TextView) mDraftsOptionHeader.findViewById(R.id.tv_selected);
                List<MyDraftsDBModel> selectedFeeds = new ArrayList<MyDraftsDBModel>(feedsSelected);
                boolean isPreviouslyAdded = false;
                feedsLoop:
                for (MyDraftsDBModel fd : selectedFeeds) {
                    if (fd.getDraftId() == myDraftsDBModel.getDraftId()) {
                        feedsSelected.remove(fd);
                        isPreviouslyAdded = true;
                    }
                }
                if (!isPreviouslyAdded) {
                    feedsSelected.add(myDraftsDBModel);
                    view.setBackgroundColor(mContext.getResources().getColor(R.color.pbGray7));
                    viewHolder.profile_image.setImageResource(R.drawable.ic_check_enabled);
                    viewHolder.profile_image.setPadding(20, 20, 20, 20);

                } else {
                    viewHolder.profile_image.setPadding(0, 0, 0, 0);

                    view.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
                    if (mAppController != null && mAppController.getUserProfile() != null) {
                        String imgUrl = mAppController.getUserProfile().getImg();
                        Picasso.with(mContext).invalidate(imgUrl);
                        Picasso.with(mContext).load(imgUrl).into(viewHolder.profile_image, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
                    } else {
                        viewHolder.profile_image.setImageResource(R.drawable.profile_default_one);
                    }

                }

                if (feedsSelected.size() < 1) {
                    mDraftsOptionHeader.setVisibility(View.GONE);
                } else {
                    mDraftsOptionHeader.setVisibility(View.VISIBLE);
                }

                mSelected.setText(feedsSelected.size() + " Selected");
                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return feeds.size();
    }

    public void addItems(ArrayList<MyDraftsDBModel> feeds) {
        for (MyDraftsDBModel feeds1 : feeds) {
            addItem(feedsCount, feeds1);
        }

    }

    public void addItem(int position, MyDraftsDBModel feed) {
        feeds.add(position, feed);
        notifyItemInserted(position);
        feedsCount++;
    }

    public void removeItem(int position) {
        feeds.remove(position);
        notifyItemRemoved(position);
    }

    public void removeItemFromList(long draftId) {

        int pos = 0;
        for (MyDraftsDBModel feed : feeds) {
            if (feed.getDraftId() == draftId)
                break;
            pos++;
        }

        if (pos < feeds.size()) {
            removeItem(pos);
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            });
        }
    }

    // Filter Class
    public void filter(String charText) {
        searchText = charText.toLowerCase(Locale.getDefault());
        feeds.clear();
        if (arrayList.size() > 0) {
            if (charText.length() == 0) {
                feeds.addAll(arrayList);
            } else {
                for (MyDraftsDBModel gsd : arrayList) {
                    if (gsd.getText().toLowerCase(Locale.getDefault()).contains(searchText)) {
                        feeds.add(gsd);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public int getCurrentItemSize() {
        return arrayList.size();
    }

    public void updateItems(ArrayList<MyDraftsDBModel> feeds, boolean addMore) {
        if (!addMore) {
            arrayList.clear();
        }
        arrayList.addAll(feeds);
    }
}
