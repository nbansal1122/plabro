package com.plabro.realestate.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.HomeActivity;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.LoaderFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RelatedTagFeeds extends CardListActivity implements ShoutObserver.PBObserver {


    protected String hashTitle;


    @Override
    public String getTag() {
        return AppController.getInstance().getResources().getString(R.string.s_related_tag_feeds);
    }


    @Override
    public void findViewById() {
        super.findViewById();
        Analytics.trackScreen(Analytics.PropFeed.HashTagView);
        setClickListeners(R.id.btn_followLocation);
        ShoutObserver.getInstance().addListener(this);
        getFollowingStatus();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_followLocation:
                followLocation(!isFollowed);
                break;
        }
    }


    @Override
    protected int getResourceId() {
        return R.layout.activity_related_feeds;
    }


    @Override
    public void inflateToolbar() {
        //setting action bar
        mToolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(mToolbar);


        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_header_logo));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Send user to home
                navigateToHomeScreen();
            }

        });

        Intent intent = getIntent();
        hashTitle = intent.getStringExtra("hashTitle");
        getSupportActionBar().setTitle(hashTitle.toUpperCase());
//        mToolbar.setTitle(hashTitle.toUpperCase());
    }

    private void getFollowingStatus() {
        ParamObject obj = new ParamObject();
        HashMap<String, String> map = new HashMap<>();
        map.put("follow_type", "hashtag");
        map.put("follow_hash", hashTitle);
        map = Utility.getInitialParams(PlaBroApi.RT.FOLLOWING_STATUS, map);
        obj.setParams(map);
        obj.setTaskCode(Constants.TASK_CODES.FOLLOWING_STATUS);
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "" + response.getResponse());
                boolean isFollowing = false;
                try {
                    JSONObject rootObject = new JSONObject(response.getResponse().toString());
                    if (rootObject != null && rootObject.optJSONObject("output_params") != null) {
                        JSONObject data = rootObject.optJSONObject("output_params").optJSONObject("data");
                        isFollowing = data.optBoolean("isFollowing");
                        setFollowLocationButton(isFollowing);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                setFollowLocationButton(false);
            }
        });
    }

    private boolean isFollowed;

    private void setFollowLocationButton(boolean isFollowed) {
        this.isFollowed = isFollowed;
        Button followLocationButton = (Button) findViewById(R.id.btn_followLocation);
        followLocationButton.setVisibility(View.VISIBLE);
        followLocationButton.setOnClickListener(this);
        if (isFollowed) {
            followLocationButton.setText("UNFOLLOW " + hashTitle.toUpperCase());
        } else {
            followLocationButton.setText("FOLLOW " + hashTitle.toUpperCase());
        }
    }

    private void followLocation(final boolean isFollow) {
        showDialog();
        ParamObject obj = new ParamObject();
        HashMap<String, String> map = new HashMap<>();
        map.put("follow_type", "hashtag");
        map.put("follow_hash", hashTitle);

        HashMap<String, Object> metaData = new HashMap<>();
        map.put(Analytics.Params.Location, hashTitle);
        metaData.putAll(map);
        if (isFollow) {
            Analytics.trackProfileEvent(Analytics.Actions.FollowLocation, metaData);
            map = Utility.getInitialParams(PlaBroApi.RT.FOLLOW, map);
        } else {
            Analytics.trackProfileEvent(Analytics.Actions.UnfollowLocation, metaData);
            map = Utility.getInitialParams(PlaBroApi.RT.UN_FOLLOW, map);
        }
        obj.setParams(map);
        obj.setTaskCode(Constants.TASK_CODES.FOLLOW);
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                dismissDialog();
                Log.d(TAG, "On Success:" + response.getResponse());
                if (isFollow)
                    showToast(getString(R.string.location_follow_success, hashTitle));
                setFollowLocationButton(isFollow);
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                dismissDialog();
                if (isFollow)
                    showToast(getString(R.string.location_follow_failure, hashTitle));
                Log.d(TAG, "On Success:" + response.getCustomException().getMessage());
                setFollowLocationButton(!isFollow);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ShoutObserver.getInstance().remove(this);
    }

    @Override
    public void scrollToTop() {
        super.scrollToTop();
        mScrollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayoutManager.scrollToPositionWithOffset(0, 0);
                mScrollButton.playHideAnimation();
                mScrollButton.setVisibility(View.GONE);
            }
        });
    }

    public void getData() {

        if (addMore && baseFeeds.size() > 0) {
            if (baseFeeds.get(baseFeeds.size() - 1).getType().equalsIgnoreCase(Constants.FEED_TYPE.NO_MORE_DATA)) {
                return;
            }
        }

        showSpinners(addMore);

        isLoading = true;


        Intent intent = getIntent();
        hashTitle = intent.getStringExtra("hashTitle");
        refKey = intent.getExtras().get("refKey") + "";
        message_id = intent.getStringExtra("message_id");

        String type = intent.getStringExtra("type");
        hashTitle = getIntent().getStringExtra("hashTitle");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.PLABRO_SEARCH_HASH_FEEDS_POST_PARAM_HASHTAG, hashTitle);
        if (addMore) {
            params.put("startidx", (page) + "");
        } else {
            params.put("startidx", "0");
        }
        if (!TextUtils.isEmpty(refKey)) {
            params.put("refKey", refKey + "");
        }
        if (!TextUtils.isEmpty(message_id)) {
            params.put("message_id", message_id + "");
        }
        if (!TextUtils.isEmpty(type)) {
            params.put("type", type);
        }


        params = Utility.getInitialParams(PlaBroApi.RT.SEARCH_HASH, params);

        AppVolley.processRequest(0, FeedResponse.class, null, String.format(PlaBroApi.getBaseUrl(RelatedTagFeeds.this)), params, RequestMethod.GET, new VolleyListener() {

                    @Override
                    public void onFailureResponse(PBResponse response, int taskCode) {
                        stopSpinners();
                        if (!addMore) {
                            noFeeds.setVisibility(View.VISIBLE);
                        }
                        onApiFailure(response, taskCode);


                        setAndCheckNoDataLayout();

                        try {
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 2000ms
                                    LoaderFeed lf = new LoaderFeed();
                                    lf.setType(Constants.FEED_TYPE.NO_MORE);

                                    if (addMore) {
                                        baseFeeds.remove(baseFeeds.size() - 1);//for footer
                                        baseFeeds.add(lf);
                                        mAdapter.notifyDataSetChanged();
                                    }
                                }
                            }, 2000);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onSuccessResponse(PBResponse response, int taskCode) {
                        stopSpinners();

                        FeedResponse feedResponse = (FeedResponse) response.getResponse();
                        List<BaseFeed> tempFeeds =
                                (ArrayList<BaseFeed>) feedResponse.getOutput_params().getData();

                        if (tempFeeds.size() > 0) {

                            noFeeds.setVisibility(View.GONE);
                            LoaderFeed lf = new LoaderFeed();
                            if (tempFeeds.size() < Util.getDefaultStartIdx()) {
                                lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                            } else {
                                lf.setType(Constants.FEED_TYPE.PROGRESS_MORE);
                            }

                            if (addMore) {

                                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                                baseFeeds.addAll(tempFeeds);
                                baseFeeds.add(lf);
                                mAdapter.notifyDataSetChanged();
                                page = Util.getStartIndex(page);
                                HashMap<String, Object> data = new HashMap<>();
                                data.put("ScreenName", TAG);
                                data.put("Action", "Manual");
                            } else {
                                baseFeeds.clear();
                                baseFeeds.addAll(tempFeeds);
                                if (baseFeeds.size() > mLayoutManager.getChildCount()) {
                                    baseFeeds.add(lf);
                                }
                                page = Util.getDefaultStartIdx();
                                mRecyclerView.setAdapter(mAdapter);
                                if (mAdapter != null) {
                                    mAdapter.notifyDataSetChanged();
                                    // mAdapter.addItems(baseFeeds);
                                }

                            }

                        } else {
                            if (addMore) {
                                LoaderFeed lf = new LoaderFeed();
                                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                                lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                                baseFeeds.add(lf);
                                mAdapter.notifyDataSetChanged();
                            }
                        }

                        setAndCheckNoDataLayout();

                    }


                }
        );


    }

    private Intent getDefaultLandingIntent() {
        Intent intent = new Intent(RelatedTagFeeds.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);

        return intent;
    }


    private void navigateToHomeScreen() {
        startActivity(getDefaultLandingIntent());
        finish();
        return;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.trackScreen(Analytics.PropFeed.HashTagView);

    }
}
