package com.plabro.realestate.models.notifications;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by nitin on 27/11/15.
 */
@Table(name = "NotificationAction", id = BaseColumns._ID)
public class NotificationAction extends Model {
    @Column
    private String actionType;
    @Column
    private String notificationIdsString;

    private JSONArray notifIds = new JSONArray();

    public String getNotificationIdsString() {
        return notificationIdsString;
    }

    public void setNotificationIdsString(String notificationIdsString) {
        this.notificationIdsString = notificationIdsString;
    }


    public void addNotificationId(String id) {
        notifIds.put(id);
    }

    public JSONArray getNotifIds() {
        return notifIds;
    }

    public void setNotifIds(JSONArray notifIds) {
        this.notifIds = notifIds;
    }


    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public void saveData() {
        this.setNotificationIdsString(this.getNotifIds().toString());
        this.save();
    }

}