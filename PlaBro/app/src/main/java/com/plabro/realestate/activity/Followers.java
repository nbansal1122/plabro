package com.plabro.realestate.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.FollowersCustomAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.followers.FollowersResponse;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class Followers extends PlabroBaseActivity {

    private Toolbar mToolbar;
    private ProgressWheel mProgressWheel;
    private RelativeLayout mNoFollowers;


    // Declare Variables
    ListView mFollowersList;
    FollowersCustomAdapter mAdapter;
    List<ProfileClass> followersList = new ArrayList<ProfileClass>();

    private String mAuthorId, mType;

    @Override
    protected String getTag() {
        return AppController.getInstance().getResources().getString(R.string.s_followers_following);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getIntent().getExtras();
        if (b == null) {
            b = new Bundle();
        }
        FragmentContainer.startActivity(this, Constants.FRAGMENT_TYPE.FOLLOW_PAGER_FRAGMENT, b);
        finish();
        return;
    }

    @Override
    protected int getResourceId() {
        return 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_followers, menu);
        sketchMenuItems(menu);

        return true;
    }

    private void sketchMenuItems(Menu menu) {

        SearchView chatFilter = ((SearchView) menu.getItem(0).getActionView());
        chatFilter.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.filter(newText);
//                Toast.makeText(mContext, newText, Toast.LENGTH_SHORT).show();
                return true;
            }
        });

    }


    @Override
    public void findViewById() {


        Bundle bundle = getIntent().getExtras();

        mAuthorId = bundle.getString("authorid");
        mType = bundle.getString("type");


        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mProgressWheel.spin();

        mNoFollowers = (RelativeLayout) findViewById(android.R.id.empty);

        // Locate the ListView in listview_main.xml
        mFollowersList = (ListView) findViewById(R.id.listview);

        inflateToolbar();
        getFollowers();

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }


    @Override
    public void inflateToolbar() {
        //setting action bar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);


        if (mType.equalsIgnoreCase("followers")) {


            getSupportActionBar().setTitle("Followers");
        } else {
            getSupportActionBar().setTitle("Following");

        }

//

        // Inflate a menu to be displayed in the toolbar
        mToolbar.inflateMenu(R.menu.menu_followers);

        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });

    }

    void getFollowers() {

        HashMap<String, String> params = new HashMap<String, String>();
        if (!TextUtils.isEmpty(mAuthorId)) {
            params.put(PlaBroApi.PLABRO_GET_PROFILE_AUTO_PARAM_CONTACT_ID, mAuthorId);
        }
        if (mType.equalsIgnoreCase("followers")) {
            params = Utility.getInitialParams(PlaBroApi.RT.FOLLOWER_LIST, params);
        } else {
            params = Utility.getInitialParams(PlaBroApi.RT.FOLLOWING_LIST, params);
        }

        AppVolley.processRequest(1, FollowersResponse.class, null, String.format(PlaBroApi.getBaseUrl(Followers.this)), params, RequestMethod.GET, new VolleyListener() {

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                if (mProgressWheel.isSpinning()) {
                    mProgressWheel.stopSpinning();
                }
                if (null == mAdapter || mAdapter.getCount() < 1) {
                    mNoFollowers.setVisibility(View.VISIBLE);
                } else {
                    mNoFollowers.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                if (mProgressWheel.isSpinning()) {
                    mProgressWheel.stopSpinning();
                }
                FollowersResponse profileResponse = (FollowersResponse) response.getResponse();
                if (profileResponse.isStatus()) {
                    followersList = profileResponse.getOutput_params().getData();
                    Log.d(TAG, followersList.size() + "");
//                    Collections.sort(followersList, new MyComparator());
                    mAdapter = new FollowersCustomAdapter(Followers.this, followersList);
                    // Binds the Adapter to the ListView
                    mFollowersList.setAdapter(mAdapter);
                }

                if (null == mAdapter || mAdapter.getCount() < 1) {
                    mNoFollowers.setVisibility(View.VISIBLE);
                } else {
                    mNoFollowers.setVisibility(View.GONE);
                }
            }

        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
