/*
 * Copyright (C) 2011 The Android Open Source Project
 * Copyright (C) 2011 Jake Wharton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.plabro.realestate.uiHelpers;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plabro.realestate.R;

import java.util.ArrayList;
import java.util.List;


public class ViewPagerIndicator extends LinearLayout implements OnPageChangeListener {
    private static final String TAG = ViewPagerIndicator.class.getSimpleName();

    private Context context;
    private ViewPager pager;
    private OnPageChangeListener onPageChangeListener;
    public static LinearLayout itemContainer;
    private List<RelativeLayout> items;
    int height;
    int width;
    private OnClickListener itemClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (Integer) v.getTag();

            pager.setCurrentItem(position);
        }
    };

    public ViewPagerIndicator(Context context) {
        super(context);
        this.context = context;
        setup();
    }

    public ViewPagerIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setup();
    }

    public ViewPagerIndicator(Context context, AttributeSet attrs,
                              int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        setup();
    }

    private void setup() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (inflater != null) {
            inflater.inflate(R.layout.view_pager_indicator, this);

            itemContainer = (LinearLayout) findViewById(R.id.pager_indicator_container);

            items = new ArrayList<RelativeLayout>();
        }
    }

    /**
     * Notifies the pager indicator that the data set has changed.
     * Be sure to notify the pager as well (though you may wish to place
     * that call in here yourself).
     */
    public void notifyDataSetChanged() {
        if (pager != null && pager.getAdapter() != null) {

            setupPagerHead(pager.getCurrentItem());


            // remove the old items (if any exist)
            //itemContainer.removeAllViews();

            // I'm sure this could be optimised a lot settingsFragment, eg,
            // by reusing existing ImageViews, but it
            // does the job well enough for now.
            //items.removeAll(items);

            // now create the new items.
            //for (int i = 0; i < pager.getAdapter().getCount(); i++) {

//                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//
//                // Set only target params:
//
//
//                itemContainer.post(new Runnable() {
//                    public void run() {
//                        height = itemContainer.getHeight();
//                        width = itemContainer.getWidth();
//                    }
//                });
//
//
//
//                layoutParams.weight = 1;
//
//                TextView item = new TextView(context);
//                item.setLayoutParams(layoutParams);
//
//                Drawable img = null;
//
//
//                Drawable img2 = getContext().getResources().getDrawable(R.drawable.indicator);
//                img2.setBounds(0, 0, 900, 10);
//
//
//                item.setCompoundDrawablePadding(0);
//               // item.setPadding(-180, -20, 0, 0);
//
//                if (i == pager.getCurrentItem()) {
//
//                    switch (i) {
//                        case 0:
//                            img = getContext().getResources().getDrawable(R.drawable.ic_feed_selected);
//                            break;
//
//                        case 1:
//                            img = getContext().getResources().getDrawable(R.drawable.ic_conversations_selected);
//                            break;
//
//                        case 2:
//                            img = getContext().getResources().getDrawable(R.drawable.ic_contacts_selected);
//                            break;
//                        case 3:
//                            img = getContext().getResources().getDrawable(R.drawable.ic_more_selected);
//                            break;
//
//                    }
//
//                    //img.mutate().setColorFilter(0xff33b5e5, PorterDuff.Mode.MULTIPLY);
//
//
//                    item.setCompoundDrawables(null, null, null, img2);
//                    item.setBackgroundDrawable(img);
//
//                } else {
//                    switch (i) {
//                        case 0:
//                            img = getContext().getResources().getDrawable(R.drawable.ic_feed_normal);
//                            break;
//
//                        case 1:
//                            img = getContext().getResources().getDrawable(R.drawable.ic_conversations_normal);
//                            break;
//
//                        case 2:
//                            img = getContext().getResources().getDrawable(R.drawable.ic_contacts_normal);
//                            break;
//                        case 3:
//                            img = getContext().getResources().getDrawable(R.drawable.ic_more_normal);
//                            break;
//
//                    }
//                   item.setCompoundDrawables(null, null, null, null);
//                    item.setBackgroundDrawable(img);
//
//                }
//                //item.setText(pager.getAdapter().getPageTitle(i));
//                //item.setTextColor(Color.WHITE);
//               // img.setBounds(0, 0, 180, 180);
//                item.setGravity(Gravity.BOTTOM);
//                item.setGravity(Gravity.CENTER);

//                RelativeLayout item = (RelativeLayout) View.inflate(context,R.layout.indicator_icon,null);
//
//                items.add(item);
//
//                itemContainer.addView(item);
        }


    }

    public ViewPager getViewPager() {
        return pager;
    }

    public void setViewPager(ViewPager pager) {
        this.pager = pager;
        this.pager.setOnPageChangeListener(this);
    }

    public OnPageChangeListener getOnPageChangeListener() {
        return onPageChangeListener;
    }

    public void setOnPageChangeListener(OnPageChangeListener onPageChangeListener) {
        this.onPageChangeListener = onPageChangeListener;
    }

    public void setCurrentItem(int position) {
        if (pager != null && pager.getAdapter() != null) {
            int numberOfItems = pager.getAdapter().getCount();

            setupPagerHead(position);
           // AppController.getInstance().showNewIMsPopup();

//            for (int i = 0; i < numberOfItems; i++) {
//                TextView item = items.get(i);
//                if (item != null) {
//                    Drawable img = null;
//
//
//                    Drawable img2 = getContext().getResources().getDrawable(R.drawable.indicator);
//                    img2.setBounds(0, 0, 900, 10);
//
//                    //item.setPadding(-60, -10, 70, 0);
//
//                    item.setCompoundDrawablePadding(0);
//
//
//
//
//                    if (i == position) {
//                        switch (i) {
//                            case 0:
//                                img = getContext().getResources().getDrawable(R.drawable.ic_feed_selected);
//                                break;
//
//                            case 1:
//                                img = getContext().getResources().getDrawable(R.drawable.ic_conversations_selected);
//                                break;
//
//                            case 2:
//                                img = getContext().getResources().getDrawable(R.drawable.ic_contacts_selected);
//                                break;
//                            case 3:
//                                img = getContext().getResources().getDrawable(R.drawable.ic_more_selected);
//                                break;
//
//                        }
//                        //img.mutate().setColorFilter(0xff33b5e5, PorterDuff.Mode.MULTIPLY);
//
//                        item.setCompoundDrawables(null, null, null, img2);
//                        item.setBackgroundDrawable(img);
//
//
//                    } else {
//
//                        switch (i) {
//                            case 0:
//                                img = getContext().getResources().getDrawable(R.drawable.ic_feed_normal);
//                                break;
//
//                            case 1:
//                                img = getContext().getResources().getDrawable(R.drawable.ic_conversations_normal);
//                                break;
//
//                            case 2:
//                                img = getContext().getResources().getDrawable(R.drawable.ic_contacts_normal);
//                                break;
//                            case 3:
//                                img = getContext().getResources().getDrawable(R.drawable.ic_more_normal);
//                                break;
//
//                        }
//                        item.setCompoundDrawables(null, null, null, null);
//                        item.setBackgroundDrawable(img);
//
//                    }

//                    img.setBounds(0, 0, 180, 180);


        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (this.onPageChangeListener != null) {
            this.onPageChangeListener.onPageScrollStateChanged(state);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (this.onPageChangeListener != null) {
            this.onPageChangeListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
        }
    }

    @Override
    public void onPageSelected(int position) {
        setCurrentItem(position);
        if (this.onPageChangeListener != null) {
            this.onPageChangeListener.onPageSelected(position);
        }
    }




    void setupPagerHead(int position)
    {

        RelativeLayout feed = (RelativeLayout) itemContainer.getChildAt(0);
        View feed_indicator = feed.findViewById(R.id.feeds_indicator);
        TextView feed_icon = (TextView) feed.findViewById(R.id.icon_feeds);
        RelativeLayout rl_feed = (RelativeLayout) feed.findViewById(R.id.rl_feed_notif_layout);

        feed.setTag(0);
        feed.setOnClickListener(itemClickListener);

        RelativeLayout conv = (RelativeLayout) itemContainer.getChildAt(1);
        View conv_indicator = conv.findViewById(R.id.conversation_indicator);
        TextView conv_icon = (TextView) conv.findViewById(R.id.icon_conversation);
        RelativeLayout rl_conv = (RelativeLayout) feed.findViewById(R.id.rl_conv_notif_layout);


        conv.setTag(1);
        conv.setOnClickListener(itemClickListener);

        RelativeLayout people = (RelativeLayout) itemContainer.getChildAt(2);
        View people_indicator = people.findViewById(R.id.people_indicator);
        TextView people_icon = (TextView) people.findViewById(R.id.icon_people);
        RelativeLayout rl_people = (RelativeLayout) feed.findViewById(R.id.rl_people_notif_layout);


        people.setTag(2);
        people.setOnClickListener(itemClickListener);

        RelativeLayout more = (RelativeLayout) itemContainer.getChildAt(3);
        View more_indicator = more.findViewById(R.id.more_indicator);
        TextView more_icon = (TextView) more.findViewById(R.id.icon_more);
        RelativeLayout rl_more = (RelativeLayout) feed.findViewById(R.id.rl_more_notif_layout);



        more.setTag(3);
        more.setOnClickListener(itemClickListener);

        switch (position) {
            case 0:

                feed_indicator.setVisibility(VISIBLE);
                feed_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_feed_selected));
                conv_indicator.setVisibility(GONE);
                conv_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_conversations_normal));
                people_indicator.setVisibility(GONE);
                people_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_contacts_normal));
                more_indicator.setVisibility(GONE);
                more_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_more_normal));

                rl_feed.setVisibility(View.GONE);


                break;
            case 1:

                feed_indicator.setVisibility(GONE);
                feed_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_feed_normal));
                conv_indicator.setVisibility(VISIBLE);
                conv_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_conversations_selected));
                people_indicator.setVisibility(GONE);
                people_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_contacts_normal));
                more_indicator.setVisibility(GONE);
                more_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_more_normal));


                break;
            case 2:
                feed_indicator.setVisibility(GONE);
                feed_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_feed_normal));
                conv_indicator.setVisibility(GONE);
                conv_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_conversations_normal));
                people_indicator.setVisibility(VISIBLE);
                people_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_contacts_selected));
                more_indicator.setVisibility(GONE);
                more_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_more_normal));

                break;
            case 3:
                feed_indicator.setVisibility(GONE);
                feed_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_feed_normal));
                conv_indicator.setVisibility(GONE);
                conv_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_conversations_normal));
                people_indicator.setVisibility(GONE);
                people_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_contacts_normal));
                more_indicator.setVisibility(VISIBLE);
                more_icon.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_more_selected));

                break;
        }


    }


}