package com.plabro.realestate.models.countries;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nbansal2211 on 24/07/17.
 */

public class Country {
    //    {"name":"Israel","dial_code":"+972","code":"IL"}
    private String name;
    @SerializedName("dial_code")
    private String dialCode;
    private String code;

    public String getDisplayString(){
        return name + " ("+dialCode+")";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDialCode() {
        return dialCode;
    }

    public void setDialCode(String dialCode) {
        this.dialCode = dialCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
