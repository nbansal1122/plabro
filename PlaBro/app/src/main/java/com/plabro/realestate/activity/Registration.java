package com.plabro.realestate.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.plabro.realestate.Featured;
import com.plabro.realestate.R;
import com.plabro.realestate.uiHelpers.LinkMovementMethodOverride;
import com.plabro.realestate.uiHelpers.MaterialDialog;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.Util;


public class Registration extends PlabroBaseActivity {

    private EditText et_phone;
    private String valid_phone;
    private TextView mTNC;
    EditText isd_code;
    MenuItem menu_btn_enable;
    MenuItem menu_btn_disabled;
    boolean isAllFieldsValid = false;

    @Override
    protected String getTag() {
        return "Registration";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);


//        boolean status = PBPreferences.getFirstTimeState(Registration.this);
//        Toast.makeText(Registration.this, status + "", Toast.LENGTH_LONG).show();


        //setting action bar
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        // Set an OnMenuItemClickListener to handle menu item clicks
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Handle the menu item
                int id = item.getItemId();

                switch (id) {
                    case R.id.action_next_enable:


                        Util.hideKeyboard(et_phone, Registration.this);


                        verifyPhoneNumber();

                        return true;
                }
                return true;
            }

        });

        // Inflate a menu to be displayed in the toolbar
        toolbar.inflateMenu(R.menu.menu_preferences);

        //initialise and add default functionality
        setDefauts();

    }

    private void setDefauts() {
        et_phone = (EditText) findViewById(R.id.phone);
        isd_code = (EditText) findViewById(R.id.isd_code);
        mTNC = (TextView) findViewById(R.id.tv_tnc);


        setTNCLink();

        // adding listeners
        // phone validation
        et_phone.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) { // TODO Auto-generated method stub
                // TODO Auto-generated method stub
                // min lenth 10 and max lenth 12 (2 extra for - as per phone //
                // matcher format)

                validate_phone(10, 10, et_phone);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) { // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
        et_phone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        et_phone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {

                    if (isAllFieldsValid) {

                        verifyPhoneNumber();

                    } else {
                        Toast.makeText(Registration.this, "Please clear all validation error.", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                return false;
            }
        });


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        /*InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et_phone, InputMethodManager.SHOW_IMPLICIT);
*/

//        DigitsAuthButton digitsButton = (DigitsAuthButton) findViewById(R.id.auth_button);
//
//        if (AppController.getInstance().getCrashlytics() == null) {
//            AppController.getInstance().initializeFabric();
//            digitsButton.setCallback(new AuthCallback() {
//                @Override
//                public void success(DigitsSession session, String phoneNumber) {
//                    // Do something with the session and phone number
//                }
//
//                @Override
//                public void failure(DigitsException exception) {
//                    // Do something on failure
//                }
//            });
//        } else {
//
//            digitsButton.setCallback(new AuthCallback() {
//                @Override
//                public void success(DigitsSession session, String phoneNumber) {
//                    // Do something with the session and phone number
//                }
//
//                @Override
//                public void failure(DigitsException exception) {
//                    // Do something on failure
//                }
//            });
//        }


    }

    private void setTNCLink() {

        String tncString = getResources().getString(R.string.tnc_agree_msg);
        final SpannableString hashTagText = new SpannableString(tncString);

        final int indexStart = tncString.indexOf("Terms of Use");
        if (indexStart > 0) {
            final int indexEnd = indexStart + 11;
            mTNC.setOnTouchListener((new LinkMovementMethodOverride()));


            hashTagText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), indexStart, indexEnd + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            final ClickableSpan hashClick =
                    new ClickableSpan() {
                        @Override
                        public void onClick(View v) {
                            Featured.openWeb(Registration.this, Constants.PLABRO_APP_TNC, "Terms and Conditions");
                        }

                    };


            hashTagText.setSpan(hashClick, indexStart, indexEnd + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        }
        mTNC.setText(hashTagText, TextView.BufferType.SPANNABLE);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_registration, menu);
        menu_btn_enable = menu.getItem(0);
        menu_btn_disabled = menu.getItem(1);
        return true;
    }


    //validate 10 digit phone number
    public void validate_phone(int MinLen, int MaxLen,
                               EditText edt) throws NumberFormatException {
        String edt_phone = edt.getText().toString().replace("(", "").replace(")", "").replace("-", "").replace(" ", "");
        if (edt_phone.length() <= 0) {
            edt.setError("Number Only");
            valid_phone = null;
            disable_menu_btn();
            et_phone.setBackgroundResource(R.drawable.gray_line);
            isAllFieldsValid = false;

        } else if (edt_phone.length() < MinLen) {
            edt.setError("Minimum length " + MinLen);
            valid_phone = null;
            disable_menu_btn();
            et_phone.setBackgroundResource(R.drawable.blue_line);
            isAllFieldsValid = false;


        } else if (edt_phone.length() > MaxLen) {

            edt.setError("Maximum length " + MaxLen);
            valid_phone = null;
            disable_menu_btn();
            et_phone.setBackgroundResource(R.drawable.blue_line);
            isAllFieldsValid = false;

        }
//         else if (edt.getText().toString().contains("(") && edt.getText().toString().contains(")") && edt.getText().toString().contains("-")){
//            edt.setError("Invalid phone " + MaxLen);
//            valid_phone = null;
//            disable_menu_btn();
//            et_phone.setBackgroundResource(R.drawable.blue_line);
//            isAllFieldsValid = false;
//        }
        else {

            isAllFieldsValid = true;

            valid_phone = edt_phone;
//            if (valid_phone.length() > 10) {
//                valid_phone = valid_phone.substring(2, valid_phone.length());
//            }
            String phone = PBPreferences.getCountryCode() + valid_phone;
            PBPreferences.setPhone(phone);
            enable_menu_btn();
            edt.setError(null);
            et_phone.setBackgroundResource(R.drawable.blue_line);
        }

    }

    @Override
    public void onBackPressed() {
        //confirm from user ofr exit
        showExitAlert();

    }

    void showExitAlert() {

        final MaterialDialog mMaterialDialog;
        mMaterialDialog = new MaterialDialog(this);

        mMaterialDialog.setTitle(R.string.exit_confirm_title)
                .setMessage(R.string.exit_confirm_msg)
                .setPositiveButton(R.string.exit, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                        // exit application
                        exitApp();

                    }
                })
                .setNegativeButton(R.string.cancel, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();


                    }
                });

        mMaterialDialog.show();


    }

    void verifyPhoneNumber() {

        //show alert for phone confirmation
        final MaterialDialog mMaterialDialog;
        mMaterialDialog = new MaterialDialog(this);

        String con_msg = isd_code.getText().toString() + "" + valid_phone + "\n" + "Do you confirm it is your correct phone number?" + "\n\n"
                + "An sms will be sent with an activation code.";

        mMaterialDialog.setTitle(R.string.confirmation)
                .setMessage(con_msg)
                .setPositiveButton(R.string.confirm, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();

                        //Navigate to Verification screen
                        Intent intent = new Intent(Registration.this, Verification.class);
                        startActivity(intent);


                    }
                })
                .setNegativeButton(R.string.edit, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();


                    }
                });

        mMaterialDialog.show();
    }

    void exitApp() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
        startActivity(intent);
        finish();
        System.exit(0);
    }


    void disable_menu_btn() {
        if (menu_btn_enable != null && menu_btn_disabled != null) {
            //disable menu button next
            menu_btn_enable.setVisible(false);
            menu_btn_disabled.setVisible(true);
        }

    }

    void enable_menu_btn() {

        if (menu_btn_enable != null && menu_btn_disabled != null) {
            //disable menu button next
            menu_btn_enable.setVisible(true);
            menu_btn_disabled.setVisible(false);
        }


    }


    @Override
    public void findViewById() {

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void inflateToolbar() {

    }


}
