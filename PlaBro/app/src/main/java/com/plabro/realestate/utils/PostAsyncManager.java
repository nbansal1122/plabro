package com.plabro.realestate.utils;

import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.uiHelpers.images.handlers.AsyncTask;
import com.plabro.realestate.volley.AppVolley;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * Created by nbansal2211 on 24/07/17.
 */

public class PostAsyncManager extends AsyncTask<Void, Void, Object> {
    private WeakReference<AsyncListener> listener;
    private ParamObject paramObject;

    public PostAsyncManager(AsyncListener listener, ParamObject paramObject) {
        this.listener = new WeakReference<AsyncListener>(listener);
        this.paramObject = paramObject;
    }

    @Override
    protected Object doInBackground(Void... params) {
        return getData(paramObject);
    }

    @Override
    protected void onPostExecute(Object aVoid) {
        super.onPostExecute(aVoid);
        boolean result = (boolean) aVoid;
        if (listener.get() != null) {
            listener.get().onPostExecute(result);
        }

    }

    private boolean getData(ParamObject paramObject) {
        try {
            URL url = new URL(paramObject.getUrl());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            for (Map.Entry<String, String> entry : paramObject.getHeaders().entrySet()) {
                conn.setRequestProperty(entry.getKey(), entry.getValue());
            }
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(AppVolley.getStringFromMap(paramObject.getParams()));
            writer.flush();
            writer.close();
            os.close();
            conn.connect();
            if (conn.getResponseCode() >= 200 && conn.getResponseCode() < 300) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return false;
    }

    public static interface AsyncListener {
        public void onPostExecute(boolean result);
    }

}
