package com.plabro.realestate.listeners;

/**
 * Created by Hemant on 23-01-2015.
 */
public interface MyOptionsListener {

    int BACK_CLICK = 1;
    int COPY_CLICK = 2;

    public void  onResponse(Boolean response,int action);

}
