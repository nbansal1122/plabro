package com.plabro.realestate.utilities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.MainActivity;
import com.plabro.realestate.activity.Registration;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.GenericObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.login.Login;
import com.plabro.realestate.models.login.LoginResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.tourguide.Overlay;
import com.plabro.realestate.uiHelpers.tourguide.Pointer;
import com.plabro.realestate.uiHelpers.tourguide.ToolTip;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.CustomException;
import com.plabro.realestate.volley.PBResponse;
import com.software.shell.fab.ActionButton;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;


/**
 * Created by hemant on 15/12/14.
 */
public class Utility {

    private static int authLoginHit = 0;
    private static boolean isAuthLoginInitiated = false;

    public static synchronized HashMap<String, String> getInitialParams(String rt, HashMap<String, String> otherParams) {

        if (otherParams == null) {
            otherParams = new HashMap<String, String>();
        }
        otherParams.put(PlaBroApi.RT_KEY, rt);
        otherParams.put(PlaBroApi.APP_SOURCE, "android");
        if (!TextUtils.isEmpty(PBPreferences.getDeviceId())) {
            otherParams.put(PlaBroApi.DEVICE_ID, PBPreferences.getDeviceId());
        }
        ;
        otherParams.put(PlaBroApi.APP_VERSION, AppController.getInstance().getPackageInfo().versionCode + "");

        if (!TextUtils.isEmpty(PBPreferences.getImei())) {
            otherParams.put(PlaBroApi.IMEI, PBPreferences.getImei());
        }

        if (!TextUtils.isEmpty(PBPreferences.getAuthKey())) {
            otherParams.put(PlaBroApi.AUTH_KEY, PBPreferences.getAuthKey());
        }
        long timeStamp = (System.currentTimeMillis() / 1000);
        int hashkey = Utility.create_auth_hash_key(otherParams, timeStamp);

        otherParams.put("keytime", timeStamp + "");
        otherParams.put("hashkey", hashkey + "");


        return otherParams;
    }

    public static synchronized HashMap<String, String> getInitialParamsWithoutAuthkey(String rt, HashMap<String, String> otherParams) {
        if (otherParams == null) {
            otherParams = new HashMap<String, String>();
        }
        if (!TextUtils.isEmpty(PBPreferences.getDeviceId())) {
            otherParams.put(PlaBroApi.DEVICE_ID, PBPreferences.getDeviceId());
        } else if (!TextUtils.isEmpty(PBPreferences.getImei())) {
            otherParams.put(PlaBroApi.DEVICE_ID, PBPreferences.getImei());
        }

        if (!TextUtils.isEmpty(PBPreferences.getImei())) {
            otherParams.put(PlaBroApi.PLABRO_KEY_EXCHANGE_POST_PARAM_IMEI, PBPreferences.getImei());
        }

        otherParams.put(PlaBroApi.RT_KEY, rt);
        otherParams.put(PlaBroApi.APP_SOURCE, "android");
        otherParams.put(PlaBroApi.APP_VERSION, AppController.getInstance().getPackageInfo().versionCode + "");
        // otherParams.put(PlaBroApi.APP_VERSION, "200");
        long timeStamp = (System.currentTimeMillis() / 1000);
        int hashkey = Utility.create_auth_hash_key(otherParams, timeStamp);

        otherParams.put("keytime", timeStamp + "");
        otherParams.put("hashkey", hashkey + "");
        return otherParams;
    }


    public static synchronized HashMap<String, String> getInitialParamsWithoutIMEI(String rt, HashMap<String, String> otherParams) {
        if (otherParams == null) {
            otherParams = new HashMap<String, String>();
        }
        if (!TextUtils.isEmpty(PBPreferences.getDeviceId())) {
            otherParams.put(PlaBroApi.DEVICE_ID, PBPreferences.getDeviceId());
        } else if (!TextUtils.isEmpty(PBPreferences.getImei())) {
            otherParams.put(PlaBroApi.DEVICE_ID, PBPreferences.getImei());
        }

        otherParams.put(PlaBroApi.RT_KEY, rt);
        otherParams.put(PlaBroApi.APP_SOURCE, "android");
        otherParams.put(PlaBroApi.APP_VERSION, AppController.getInstance().getPackageInfo().versionCode + "");
        long timeStamp = (System.currentTimeMillis() / 1000);
        int hashkey = Utility.create_auth_hash_key(otherParams, timeStamp);

        otherParams.put("keytime", timeStamp + "");
        otherParams.put("hashkey", hashkey + "");
        return otherParams;
    }


    //generate 4 digit random number
    public static String getRandomFourDigits() {
        Random r = new Random();
        List<Integer> codes = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            int x = r.nextInt(9999);
            while (codes.contains(x))
                x = r.nextInt(9999);
            codes.add(x);
        }
        String str = String.format("%04d", codes.get(0));
        return str;
    }


    // validates name format
    public static boolean is_valid_person_name(String name)
            throws NumberFormatException {
        if (name.length() <= 0) {
            return false;
        } else if (!name.matches("[a-zA-Z ]+")) {
            return false;
        } else {
            return true;
        }

    }


    public static int create_auth_hash_key(HashMap dictionary, Long timeStamp) {
        Long utc_time = new Long(0);


        /* Add the utc time in epoch as the key value pair */
//        if (dictionary.containsKey("keytime") == false) {
//
//        }
        utc_time = new Long(timeStamp);
        dictionary.put("keytime", utc_time);
        //utc_time = (Long) dictionary.get("keytime");

        /* We dont want to consider hashkey's value while computing the hash */
        if (dictionary.containsKey("hashkey")) {
            dictionary.remove("hashkey");
        }

        int hashval = 0;
        Set<String> ss = (Set<String>) dictionary.keySet();
        for (String k : ss) {
            int rot = (int) (utc_time % 10);
            String v = String.valueOf(dictionary.get(k));
            if (null == v || "" == v) {
                return -1;
            }
            byte[] utf8Bytes = new byte[0];


            try {
                utf8Bytes = v.getBytes("UTF8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if (utf8Bytes.length == 0) {
                Log.e("PLABDO:HASHVAL", dictionary.toString());
                return -1;
            }

            for (int i = 0; i < 5; i++) {
                int idx = (i + utf8Bytes.length + rot) % utf8Bytes.length;
                int ii = ((int) utf8Bytes[idx]) & 0x000000FF;
                hashval += ii;
            }
        }

        return (hashval % 10000);


    }


    /**
     * Convert Dp to Pixel
     */
    public static int dpToPx(float dp, Resources resources) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, dp, resources.getDisplayMetrics());
        return (int) px;
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        Log.d("Pixels", "DP:" + dp + "->" + px);
        return px;
    }


    //login user
    public static void userLogin(final Context mContext) {

        Log.d("Login", "Fetching login");
        if (mContext == null || authLoginHit > 5) {
            authLoginHit = 0;
            return;
        }
        final WeakReference<Context> weakContext = new WeakReference<Context>(mContext);

        authLoginHit++;

        Login login = getLoginObject();
        // Check if login is expired or not
        if (login != null) {
            // Login is not expired
            PBPreferences.setAuthKey(login.getAuthkey());
            PBPreferences.setLoggedInState(true);
            PBPreferences.setTrialVersionState(login.getTrial_version());


            Log.i("PlaBro", "login successfully from local object");
            return;

        }

        if (!Util.haveNetworkConnection(weakContext.get())) {

            return;
        }

        if (isAuthLoginInitiated) {
            return;
        } else {
            isAuthLoginInitiated = true;
        }

//        if((TextUtils.isEmpty(PBPreferences.getImei(weakContext.get()))) || (TextUtils.isEmpty(PBPreferences.getPhone(weakContext.get()))))
//        {
//            return;
//        }
        HashMap<String, String> params = new HashMap<String, String>();

        if (!TextUtils.isEmpty(PBPreferences.getImei())) {
            params.put(PlaBroApi.IMEI, PBPreferences.getImei());
        }

        if (!TextUtils.isEmpty(PBPreferences.getPhone())) {
            params.put(PlaBroApi.PHONE, PBPreferences.getPhone());
        }
        params = Utility.getInitialParamsWithoutAuthkey(PlaBroApi.RT.AUTHLOGIN, params);

        Log.i("PlaBro", "auth login initiated");

        AppVolley.processRequest(Constants.TASK_CODES.AUTH_LOGIN, LoginResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                isAuthLoginInitiated = false;
                if (response != null) {
                    LoginResponse loginResponse = (LoginResponse) response.getResponse();


                    int savedVer = PBPreferences.getTrialVersionState();
                    int serverVer = loginResponse.getOutput_params().getTrial_version();

                    if (savedVer < serverVer) {
                        //application submitted and in process
                        PBPreferences.setTrialVersionState(serverVer);
                    } else if (serverVer == -1) {
                        //user blocked from using service
                        PBPreferences.setTrialVersionState(serverVer);
                    }
                    PBPreferences.setTrialVersionState(serverVer);
                    Log.d("TRIALVERSION", "Server" + serverVer + " +++++++ Saved " + savedVer);


                    Login login = loginResponse.getOutput_params().getData();
                    login.setTrial_version(serverVer);
                    Log.d("TIMER", login.getExpiryTimeStamp() + "+++++++++++++++");

                    login.saveData();

                    String authkey = login.getAuthkey();

                    if ("".equalsIgnoreCase(authkey) || null == authkey) {
                        Log.i("PlaBro", "onFailure : Oops...something went wrong");
                        PBPreferences.setLoggedInState(false);


                    } else {
                        authLoginHit = 0;
                        Log.d("Auth Key", "Auth key is:" + authkey);
                        PBPreferences.setAuthKey(authkey);
                        PBPreferences.setLoggedInState(true);

                        Log.i("PlaBro", "login sussessfully from server");
                        GenericObserver.dispatchEvent(GenericObserver.GenericObserverInterface.EVENT_AUTHKEY_UPDATE);


                    }


                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Log.i("PlaBro", "onFailure : Oops...something went wrong");
                isAuthLoginInitiated = false;
                switch (response.getCustomException().getExceptionType()) {
                    case STATUS_FALSE_EXCEPTION:

                        try {
                            PBPreferences.setLoggedInState(false);


                            if (response.getCustomException().getMessage().contains("Phone not registered yet")) {
                                Toast.makeText(AppController.getInstance(), "Looks like you have registered from other device.", Toast.LENGTH_SHORT).show();
                                AppController.getInstance().clearUserAndLogout();

                                if (null != weakContext.get() && weakContext.get() instanceof AppCompatActivity) {
                                    try {


                                        new AlertDialog.Builder(weakContext.get())
                                                .setTitle("Registration Error !")
                                                .setCancelable(false)
                                                .setMessage("Looks like you have registered from other device.")
                                                .setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        Intent intent = new Intent(Intent.ACTION_MAIN);
                                                        intent.addCategory(Intent.CATEGORY_HOME);
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        weakContext.get().startActivity(intent);
                                                    }
                                                }) // dismisses by default
                                                .setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // do the acknowledged action, beware, this is run on UI thread
                                                        PBPreferences.setDeviceState(true);
                                                        //Navigate to terms and condition screen
                                                        Intent intent = new Intent(weakContext.get(), MainActivity.class);
                                                        weakContext.get().startActivity(intent);
                                                    }
                                                })
                                                .create()
                                                .show();
                                    } catch (Exception e) {
                                        PBPreferences.setDeviceState(true);
                                        //Navigate to terms and condition screen
                                        Intent intent = new Intent(weakContext.get(), MainActivity.class);
                                        weakContext.get().startActivity(intent);
                                    }


                                }

                            } else {
                                if (mContext != null) {
                                    userLogin(mContext);
                                }
                            }
                        } catch (Exception e) {
                            Utility.trackException(e);
                            userLogin(mContext);
                        }
                        break;
                    default:
                        userLogin(mContext);
                }
            }
        });

    }


    //stats of user
    public static void userStats(final Context mContext, String type) {
        //blocking stats as of now
        return;

        /*HashMap<String, String> params = new HashMap<String, String>();

        params.put(PlaBroApi.PLABRO_STATS_POST_PARAM_TYPE, type);
        params.put(PlaBroApi.PLABRO_STATS_POST_PARAM_PHONE, PBPreferences.getPhone(mContext));
        params = Utility.getInitialParams(PlaBroApi.RT.STATS, params);
        AppVolley.processRequest(null, PlaBroApi.getBaseUrl(mContext), params, RequestMethod.GET, new ResponseListener() {
            @Override
            public void onSuccessResponse(String response) {
                if (response != null) {

                    Gson gson = new Gson();


                }

            }


            @Override
            public void onFailureResponse(VolleyError volleyError) {
                Log.i("PlaBro", "onFailure : Oops...something went wrong");
            }
        });*/

    }

    public static String getFolderPath() {
        try {
            File sdcard = Environment.getExternalStorageDirectory();
            String dbfile = sdcard.getAbsolutePath() + File.separator + "plabrodb" + File.separator;
            File result = new File(dbfile);

            if (!result.getParentFile().exists()) {
                result.getParentFile().mkdirs();
            }
            String path = result.getPath();
            if (path != null && !TextUtils.isEmpty(path)) {
                Log.d("DB Path", "" + result.getPath());
                return result.getPath() + File.separator;
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }
    }

    private static Login getLoginObject() {
        Login login = new Select().from(Login.class).executeSingle();
        return login;
    }

    public static void deleteLoginObject() {
        try {
            new Delete().from(Login.class).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static int getPageIndex(int feedSize) {
        return (feedSize % 10 == 0) ? feedSize : ((feedSize / 10) + 1) * 10;
    }

    public static void setupShoutActionButton(ActionButton mShout, Context mContext) {

        mShout.setType(ActionButton.Type.BIG);

        // To set button color for normal state:
        mShout.setButtonColor(mContext.getResources().getColor(R.color.colorPrimary));
        mShout.setImageSize(24.0f);
        mShout.setSize(60.0f);
        mShout.setImageDrawable(mContext.getResources().getDrawable(R.drawable.fab_plus_icon));
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
        } else {
            // To set button color for pressed state:
            mShout.setButtonColorPressed(mContext.getResources().getColor(R.color.colorPrimary));
//        // To set button color ripple:
//        mShout.setButtonColorRipple(mContext.getResources().getColor(R.color.colorPrimaryDark));
//
//        mShout.setRippleEffectEnabled(true);


        }
//

    }

    public static void setupScrollActionButton(ActionButton mScroll, Context mContext) {
        mScroll.setType(ActionButton.Type.MINI);
        mScroll.setButtonColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        mScroll.setImageSize(18.0f);
        //mShout.setSize(60.0f);
        mScroll.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_scroll_top));
        mScroll.setHideAnimation(ActionButton.Animations.JUMP_TO_DOWN);
        mScroll.setShowAnimation(ActionButton.Animations.JUMP_FROM_DOWN);

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {

        } else {


            // To set button color for pressed state:
            mScroll.setButtonColorPressed(ContextCompat.getColor(mContext, R.color.colorPrimary));


            // To set button color ripple:
//        mScroll.setButtonColorRipple(mContext.getResources().getColor(R.color.colorPrimaryDark));
//
//        mScroll.setRippleEffectEnabled(true);

        }

    }

    public static Drawable getDrawableWithDiffColor(Drawable drawable, int iColor) {

        int red = (iColor & 0xFF0000) / 0xFFFF;
        int green = (iColor & 0xFF00) / 0xFF;
        int blue = iColor & 0xFF;

        float[] matrix = {0, 0, 0, 0, red
                , 0, 0, 0, 0, green
                , 0, 0, 0, 0, blue
                , 0, 0, 0, 1, 0};

        ColorFilter colorFilter = new ColorMatrixColorFilter(matrix);

        drawable.setColorFilter(colorFilter);
        return drawable;
    }

    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static void trackException(Exception e) {
        HashMap<String, Object> data = new HashMap<>();
        StackTraceElement[] traces = e.getStackTrace();
        if (null != traces) {
            String className = "", stackTrace = "", lineNumber = "", methodName = "";
            for (StackTraceElement s : traces) {
                if (null != s) {
                    className = className + ", " + s.getClassName();
                    stackTrace = stackTrace + ", " + s.toString();
                    lineNumber = lineNumber + ", " + s.getLineNumber();
                    methodName = methodName + ", " + s.getMethodName();
                }
            }
            data.put("Stack Trace", stackTrace);
            data.put("lineNumber", lineNumber);
            data.put("className", className);
            data.put("methodName", methodName);
            Analytics.trackEventWithProperties(R.string.cat_exception, R.string.e_exception, data);
        }
    }

    public static void dismissProgress(ProgressDialog progress) {
        if (progress != null && progress.isShowing()) {
            try {
                progress.dismiss();
            } catch (final IllegalArgumentException e) {
                // Handle or log or ignore
            } catch (final Exception e) {
                // Handle or log or ignore
            } finally {
                progress = null;
            }
        }
    }

    public static Overlay getOverLay(Context context, int color) {
        Overlay overlay = new Overlay()
                .setBackgroundColor(color)
                .disableClick(true)
                .setStyle(Overlay.Style.Circle);
        return overlay;
    }

    public static Pointer getPointer(Context context) {
        Pointer pointer = new Pointer();
        pointer.setGravity(Gravity.CENTER);
        pointer.setColor(context.getResources().getColor(R.color.pbPointer));
        return pointer;

    }

    public static ToolTip getTooltip(Context context, String title, String msg, int g1, int g2) {
        Animation animation = new TranslateAnimation(0f, 0f, 200f, 0f);
        animation.setDuration(1000);
        animation.setFillAfter(true);
        animation.setInterpolator(new BounceInterpolator());

        ToolTip toolTip = new ToolTip()
                .setTitle(title)
                .setDescription(msg)
                .setTextColor(Color.parseColor("#ffffff"))
                .setBackgroundColor(context.getResources().getColor(R.color.transparent))
                .setShadow(false)
                .setGravity(g1 | g2)
                .setEnterAnimation(animation);
        return toolTip;
    }

    public static void initiateReadPhone() {
        TelephonyManager telephonyManager = (TelephonyManager) AppController.getInstance().getSystemService(Context.TELEPHONY_SERVICE);

        String imei = telephonyManager.getDeviceId();

        if (null == imei || "" == imei) {
            imei = Settings.Secure.getString(AppController.getInstance().getContentResolver(), Settings.Secure.ANDROID_ID);
        }

        PBPreferences.setImei(imei);

        if (TextUtils.isEmpty(PBPreferences.getDeviceId())) {

            String android_id = Settings.Secure.getString(AppController.getInstance().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            Log.d("Utility", "Device ID:" + android_id);
            PBPreferences.setDeviceId(android_id);
        }
    }
}

