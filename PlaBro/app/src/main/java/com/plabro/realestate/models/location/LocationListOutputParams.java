package com.plabro.realestate.models.location;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class LocationListOutputParams {

    List<Locations> data = new ArrayList<Locations>();

    public List<Locations> getData() {
        return data;
    }

    public void setData(List<Locations> data) {
        this.data = data;
    }
}
