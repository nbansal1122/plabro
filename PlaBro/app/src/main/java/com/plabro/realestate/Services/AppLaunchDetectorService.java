package com.plabro.realestate.Services;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.utilities.PBPreferences;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class AppLaunchDetectorService extends Service {

    private static final String TAG = AppLaunchDetectorService.class.getSimpleName();
    private boolean isRunning = false;

    public static void startAppLaunchDetectorService(Context context) {
        Log.d(TAG, "starting AppLaunchDetectorService");
        Intent intent = new Intent(context, AppLaunchDetectorService.class);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "Service onCreate");

        isRunning = true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.i(TAG, "Service onStartCommand");

        //Creating new thread for my service
        //Always write your long running tasks in a separate thread, to avoid ANR
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//
//                try {
//                    Thread.sleep(1000);
//                } catch (Exception e) {
//                }
//
//                if (isRunning) {
//                    Log.i(TAG, "Service running");
//                }
//                //Stop service once it finishes its task
//                //stopSelf();
//            }
//        }).start();

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                trackAppLaunched();

            }

        }, 0, 5000);


        return Service.START_STICKY;
    }


    @Override
    public IBinder onBind(Intent arg0) {
        Log.i(TAG, "Service onBind");
        return null;
    }

    @Override
    public void onDestroy() {

        isRunning = false;

        Log.i(TAG, "Service onDestroy");
    }


    private void trackAppLaunched() {
        ActivityManager am = (ActivityManager) this
                .getSystemService(ACTIVITY_SERVICE);

        String[] activePackages;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            activePackages = getActivePackages(am);
        } else {
            activePackages = getActivePackagesCompat(am);
        }
        if (activePackages != null) {
            boolean isWhatsAppFound = false;
            for (String activePackage : activePackages) {
                if (activePackage.equals("com.whatsapp")) {
                    isWhatsAppFound = true;
                }
            }
            boolean whatsappPrevState = PBPreferences.getIsWhatsappLaunched();
            PBPreferences.setIsWhatsappLaunched( isWhatsAppFound);
            if (!whatsappPrevState && isWhatsAppFound) {
                //whatsapp launched, do your mojo
                Log.v(TAG, "whatsapp on");
                //ChatHeadService.startChatHeadService(this); todo uncomment for chat head to show


            } else if (whatsappPrevState && !isWhatsAppFound) {
                //whatsapp closed, do your jojo mojo
                Log.v(TAG, "whatsapp off");
                ChatHeadService.hidePlabroIcon();

            }


        }
    }

    String[] getActivePackagesCompat(ActivityManager am) {
        final List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        final ComponentName componentName = taskInfo.get(0).topActivity;
        final String[] activePackages = new String[1];
        activePackages[0] = componentName.getPackageName();
        return activePackages;
    }

    String[] getActivePackages(ActivityManager am) {
        final Set<String> activePackages = new HashSet<String>();
        final List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : processInfos) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                activePackages.addAll(Arrays.asList(processInfo.pkgList));
            }
        }
        return activePackages.toArray(new String[activePackages.size()]);
    }

}