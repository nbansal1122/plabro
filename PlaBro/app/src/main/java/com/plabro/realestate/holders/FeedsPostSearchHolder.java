package com.plabro.realestate.holders;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.UserRegister;
import com.plabro.realestate.activity.FeedDetails;
import com.plabro.realestate.activity.Profile;
import com.plabro.realestate.activity.Shout;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.entities.BookmarksDBModel;
import com.plabro.realestate.entities.MyDraftsDBModel;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.BookmarkTag;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.BookmarkResponse;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.models.shouts.ShoutResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBFonts;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.ConfirmationDialog;
import com.plabro.realestate.widgets.dialogs.GenericInfoDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by hemantkumar on 17/08/15.
 */
public class FeedsPostSearchHolder extends BaseFeedHolder {
    public static String typeFeedEnabled = "feeds_enabled";
    public static String typeFeedDisabled = "feeds_disabled";
    public static String typeFeedWithTrial = "feeds_with_trial";
    FeedsPostSearchHolder holder;

    public static FragmentManager mFragmentManager;


    private static Drawable cardEnabled, cardDisabled;
    private static int heart_red, heart_grey;
    private Feeds feedObject;
    private final RoundedImageView iv_profile_image;
    private final ImageView iv_options;
    private final FontText tv_profile_name;
    private final FontText post_content;
    private final CardView card_view;
    private final LinearLayout mMore;
    private final TextView mUserRegistration;
    private final FontText mTopHash;
    private final FontText mSuggestionInfo;
    private final LinearLayout mSuggInfoLayout;
    private final FontText mAvailReq;
    private final FontText mSaleRent;
    private final FontText mBusinessName;
    private final FontText mPost, mEdit, mSkip;
    private final ImageView mInfo;
    private final ProgressWheel mProgressWheel;
    private final RelativeLayout mAction, mProcessing;


//    private final ProgressWheel mProgressWheelMore;
//    private final ImageView noMoreData;


    public FeedsPostSearchHolder(View itemView) {
        super(itemView);
        tv_profile_name = (FontText) itemView.findViewById(R.id.profile_name);
        post_content = (FontText) itemView.findViewById(R.id.post_content);
        iv_profile_image = (RoundedImageView) itemView.findViewById(R.id.profile_image);
        iv_options = (ImageView) itemView.findViewById(R.id.card_option);
        card_view = (CardView) itemView.findViewById(R.id.card_view);
        mMore = (LinearLayout) itemView.findViewById(R.id.more);
        mTopHash = (FontText) itemView.findViewById(R.id.tv_top_hash);
        mSuggestionInfo = (FontText) itemView.findViewById(R.id.tv_suggestion_info);
        mSuggInfoLayout = (LinearLayout) itemView.findViewById(R.id.ll_sugg);
        mAvailReq = (FontText) itemView.findViewById(R.id.tv_req_avail);
        mSaleRent = (FontText) itemView.findViewById(R.id.tv_sale_rent);
        mBusinessName = (FontText) itemView.findViewById(R.id.tv_buis_name);
        mUserRegistration = (TextView) itemView.findViewById(R.id.tv_go_ahead);
        mPost = (FontText) itemView.findViewById(R.id.tv_post);
        mEdit = (FontText) itemView.findViewById(R.id.tv_edit);
        mSkip = (FontText) itemView.findViewById(R.id.tv_skip);
        mInfo = (ImageView) itemView.findViewById(R.id.info);
        mAction = (RelativeLayout) itemView.findViewById(R.id.rl_actions);
        mProcessing = (RelativeLayout) itemView.findViewById(R.id.rl_processing);
        mProgressWheel = (ProgressWheel) findView(R.id.progress_wheel);
        mProgressWheel.setBarColor(ctx.getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);

    }


    @Override
    public void onBindViewHolder(final int position, final BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        if (feed instanceof Feeds) {
            Log.d("FCH", "Feed Object is an instance of Feeds Class");
            holder = this;
            final Feeds feedEnabled = (Feeds) feed;
            feedObject = (Feeds) feed;

            setupFeedDetailsCard(feedObject, holder, position, false);

            if (cardType.equalsIgnoreCase(typeFeedWithTrial)) {
                Log.d(TAG, "Card Type is Type With Trial");
                if (!feedObject.isRel()) {
                    setupClickListener(holder, position, feedObject);
                } else {
                    holder.post_content.setEnabled(false);

                }

                int val = PBPreferences.getTrialVersionState();
                if (val == 0) {

                    holder.mUserRegistration.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(ctx, UserRegister.class);
                            ctx.startActivity(intent);
                        }
                    });
                } else {
                    holder.mUserRegistration.setOnClickListener(null);
                }
            } else if (cardType.equalsIgnoreCase(typeFeedEnabled)) {
                Log.d(TAG, "Card Type is Type Feed Enabled");
                setupClickListener(holder, position, feedEnabled);
            } else {
                Log.d(TAG, "Card Type is No Type, post content is false");
                holder.post_content.setEnabled(false);
            }


        } else {
            Log.d("FCH", "Feed Object is not an instance of Feeds Class");
            return;
        }
    }

    private void setupFeedDetailsCard(Feeds feed, FeedsPostSearchHolder holder, final int position, boolean isDisabled) {
        String imgUrl = feed.getProfile_img();

        heart_red = R.drawable.ic_card_bookmark_pressed;
        heart_grey = R.drawable.ic_card_bookmark_normal;
        cardEnabled = ctx.getResources().getDrawable(R.drawable.gen_button_selector_grey);
        cardDisabled = ctx.getResources().getDrawable(R.drawable.gen_button_selector_disabled);

        //holder.iv_profile_image.setImageResource(R.drawable.profile_default_one);
        if (!TextUtils.isEmpty(imgUrl)) {
            Picasso.with(ctx).load(imgUrl).placeholder(R.drawable.profile_default_one).into(holder.iv_profile_image);
        } else {
            holder.iv_profile_image.setImageResource(R.drawable.profile_default_one);
        }

        String mUserName = "";
        String displayName = ActivityUtils.getDisplayNameFromPhone(feed.getProfile_phone());
        if (!TextUtils.isEmpty(displayName)) {
            mUserName = displayName;
        } else {
            mUserName = feed.getProfile_name();
            if (TextUtils.isEmpty(mUserName)) {
                mUserName = feed.getProfile_phone();
            }
        }
        holder.tv_profile_name.setText(Util.toDisplayCase(mUserName));
        holder.mBusinessName.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(feed.getBusiness_name())) {
            holder.mBusinessName.setText(Util.toDisplayCase(feed.getBusiness_name()));
        } else {
            holder.mBusinessName.setVisibility(View.GONE);
        }
        holder.card_view.setTag(feed);
        holder.iv_profile_image.setTag(feed);
        holder.tv_profile_name.setTag(feed);
        if (feed.getAvail_req().length > 0 && !TextUtils.isEmpty(feed.getAvail_req()[0])) {

            holder.mAvailReq.setText(feed.getAvail_req()[0]);
            holder.mAvailReq.setVisibility(View.VISIBLE);

        } else {
            holder.mAvailReq.setVisibility(View.GONE);
        }

        if (feed.getSale_rent().length > 0 && !TextUtils.isEmpty(feed.getSale_rent()[0])) {
            holder.mSaleRent.setText(feed.getSale_rent()[0]);
            holder.mSaleRent.setVisibility(View.VISIBLE);
        } else {
            holder.mSaleRent.setVisibility(View.GONE);

        }

        if (!TextUtils.isEmpty(feed.getTop_ht())) {
            holder.mTopHash.setText(Util.toDisplayCase(feed.getTop_ht()));
            holder.mTopHash.setVisibility(View.VISIBLE);

        } else {
            holder.mTopHash.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(feed.getSugg_info())) {
            holder.mSuggestionInfo.setText(feed.getSugg_info());
            holder.mSuggInfoLayout.setVisibility(View.VISIBLE);
        } else {
            holder.mSuggInfoLayout.setVisibility(View.GONE);
            if (!isDisabled) {
                ((LinearLayout) holder.mSuggInfoLayout.getParent()).setBackgroundColor(ctx.getResources().getColor(R.color.white));
            }
        }

        ProfileClass userProfile = AppController.getInstance().getUserProfile();
        String authId = "";
        if (null != userProfile) {
            authId = userProfile.getAuthorid();
        }


        holder.iv_options.setTag(feed);
        holder.post_content.setTag(feed);
        holder.card_view.setTag(feed);
        holder.mMore.setTag(feed);
        String text = feed.getText();
        int start = 0;
        int end = 250;
        if (feed.getSnippet().size() > 0) {
            start = Integer.parseInt(feed.getSnippet().get(0));
            end = Integer.parseInt(feed.getSnippet().get(1)) + 1;
        }

        int occurance = text.length() - text.replace("\n", "").length();

        if (text.length() <= end) { //&& occurance<4
            holder.mMore.setVisibility(View.VISIBLE);
        } else {

            if (start >= 0 && end < text.length()) {
                try {
                    text = text.substring(start, end);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            holder.mMore.setVisibility(View.VISIBLE);
        }

        //adding span for hashtags in post
        SpannableString hashTagText = ActivityUtils.setHashTags(feed, text, start, end, ctx, holder.post_content, getType(), position, false, getRefKey(), mActivity);//type and position send to track analytics
    }

    private void setupClickListener(final FeedsPostSearchHolder holder, final int position, final Feeds feedEnabled) {

        holder.post_content.setTypeface(PBFonts.ROBOTTO_NORMAL);

        //on click listener
        holder.mMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trackEvent(Analytics.CardActions.CardClicked, position, feedEnabled.get_id(), feedEnabled.getType(), null);
                redirectToDetails(position, v);

            }
        });


        holder.post_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //lets tell our tooltip zoo zoo that relatedtip ship has sailed
                Set<String> indexSet = PBPreferences.getHomeToolTipSet();
                indexSet.add("relatedTip");
                PBPreferences.setHomeToolTipSet(indexSet);
                trackEvent(Analytics.CardActions.CardClicked, position, feedEnabled.get_id(), feedEnabled.getType(), null);
                redirectToDetails(position, v);

            }
        });
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trackEvent(Analytics.CardActions.CardClicked, position, feedEnabled.get_id(), feedEnabled.getType(), null);
                redirectToDetails(position, v);
            }
        });


        holder.iv_profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String authorid = ((Feeds) view.getTag()).getAuthorid() + "";

                HashMap<String, Object> wrData = new HashMap<String, Object>();
                wrData.put("ScreenName", TAG);
                wrData.put("Action", "Image Clicked");
                trackEvent(Analytics.CardActions.ViewProfile, position, feedEnabled.get_id(), feedEnabled.getType(), wrData);

                Intent intent1 = new Intent(ctx, Profile.class);
                intent1.putExtra("authorid", authorid);
                ctx.startActivity(intent1);
            }
        });

        holder.tv_profile_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String authorid = ((Feeds) view.getTag()).getAuthorid() + "";
                HashMap<String, Object> wrData = new HashMap<String, Object>();
                wrData.put("ScreenName", TAG);
                wrData.put("Action", "Name Clicked");

                trackEvent(Analytics.CardActions.ViewProfile, position, feedEnabled.get_id(), feedEnabled.getType(), wrData);


                Intent intent1 = new Intent(ctx, Profile.class);
                intent1.putExtra("authorid", authorid);
                ctx.startActivity(intent1);
            }
        });

        holder.mPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postShout(feedEnabled);
            }
        });
        holder.mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPost(feedEnabled);
            }
        });
        holder.mSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skipPost(feedEnabled);
            }
        });
        holder.mInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInfo();
            }
        });

    }

    private void redirectToDetails(int position, View v) {

        Feeds feedsVal = ((Feeds) v.getTag());
        String id = feedsVal.get_id();

        FeedDetails.startFeedDetails(ctx, id, refKey + "", feedsVal);
    }


    void postShout(Feeds feed) {
        sendStatus("post", feed.getType(), feed.get_id());
        trackEvent(Analytics.CardActions.Post, 0, feed.get_id(), feed.getType(), null);
        postNewShout(feed.getText());
    }

    void editPost(Feeds feed) {
        sendStatus("edit", feed.getType(), feed.get_id());
        PBPreferences.setComposedShoutText(feed.getText());
        //WizRocket tracking
        Intent i = new Intent(ctx, Shout.class);
        i.putExtra("shoutType", "postSearch");
        ctx.startActivity(i);
        trackEvent(Analytics.CardActions.Edit, 0, feed.get_id(), feed.getType(), null);
        ((Activity) ctx).overridePendingTransition(R.anim.bottom_in_fast, R.anim.bottom_out_fast);

    }

    void showInfo() {

        String con_msg = "We see that you are searching\nfor these specifics.Create a POST\nand get better results from brokers. ";
        android.app.DialogFragment feedPopUpDialog = new GenericInfoDialog("", "OK", "Post Templates", con_msg);
        feedPopUpDialog.show(getmFragmentManager(), "Info");
    }

    void skipPost(Feeds feed) {
        trackEvent(Analytics.CardActions.Skip, 0, feed.get_id(), feed.getType(), null);
        sendStatus("skip", feed.getType(), feed.get_id());
        PBPreferences.setPostSearchStatus(true);
        ShoutObserver.dispatchEvent();
    }


    void postNewShout(String shout) {
        startProcessing();
        ParamObject obj = new ParamObject();
        HashMap<String, String> params = new HashMap<String, String>();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("text", shout);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        final String message = jsonObject.toString();
        params.put(PlaBroApi.PLABRO_SHOUT_POST_PARAM_MESSAGE, message);
        params.put(PlaBroApi.PLABRO_SHOUT_POST_PARAM_POST_TYPE, "New");
        params = Utility.getInitialParams(PlaBroApi.RT.POSTSHOUTS, params);

        obj.setParams(params);
        obj.setClassType(ShoutResponse.class);
        obj.setRequestMethod(RequestMethod.GET);
        obj.setTaskCode((int) Constants.TASK_CODES.POST_SHOUTS);
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                stopProcessing();
                ShoutResponse shoutResponse = (ShoutResponse) response.getResponse();

                final Toast toast = Toast.makeText(ctx, shoutResponse.getMsg(), Toast.LENGTH_LONG);
                //Navigate to previous screen
                if (TextUtils.isEmpty(shoutResponse.getMsg())) {
                    toast.setText("You just shouted successfully");
                }
                toast.show();
                ShoutObserver.dispatchEvent();
                PBPreferences.setPostSearchStatus(true);
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                stopProcessing();
                final Toast toast = Toast.makeText(ctx, "Unable to process your request.", Toast.LENGTH_LONG);
                toast.show();
            }
        });


    }

    void sendStatus(String action, String cardType, String postId) {
        ParamObject obj = new ParamObject();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.PLABRO_USER_SUGGESTION_ACTION_TYPE, cardType);
        params.put(PlaBroApi.PLABRO_USER_SUGGESTION_ACTION_ACTION, action);
        params.put(PlaBroApi.PLABRO_USER_SUGGESTION_ACTION_MSG_ID, postId);

        params = Utility.getInitialParams(PlaBroApi.RT.USER_SUGGESTION_ACTION, params);

        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        wrData.put("Type", type);

        obj.setParams(params);
        obj.setRequestMethod(RequestMethod.GET);
        obj.setTaskCode(Constants.TASK_CODES.USER_SUGGESTION_ACTION);
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "on Success Skip Feed");
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                Log.d(TAG, "on Failure Skip Feed");

            }
        });
    }

    void startProcessing() {
        holder.mProcessing.setVisibility(View.VISIBLE);
        holder.mAction.setVisibility(View.GONE);
        holder.mProgressWheel.spin();
    }

    void stopProcessing() {
        holder.mProcessing.setVisibility(View.GONE);
        holder.mAction.setVisibility(View.VISIBLE);
        holder.mProgressWheel.stopSpinning();
    }
}