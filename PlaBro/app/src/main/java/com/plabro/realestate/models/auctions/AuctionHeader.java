package com.plabro.realestate.models.auctions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nitin on 02/02/16.
 */
public class AuctionHeader {
    @SerializedName("img_url")
    @Expose
    private String img_url;
    @SerializedName("img_footer")
    @Expose
    private String img_footer;

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getImg_footer() {
        return img_footer;
    }

    public void setImg_footer(String img_footer) {
        this.img_footer = img_footer;
    }
}
