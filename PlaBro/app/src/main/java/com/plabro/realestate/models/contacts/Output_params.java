package com.plabro.realestate.models.contacts;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class Output_params {

    @Expose
    private List<String> data = new ArrayList<String>();

    /**
     * @return The data
     */
    public List<String> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<String> data) {
        this.data = data;
    }

}