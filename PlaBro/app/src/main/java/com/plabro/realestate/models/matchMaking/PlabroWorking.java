package com.plabro.realestate.models.matchMaking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlabroWorking {

    @SerializedName("userid_working")
    @Expose
    private Integer userid_working;
    @SerializedName("profile_phone")
    @Expose
    private String profile_phone;
    @SerializedName("shout_id_working")
    @Expose
    private Integer shout_id_working;
    @SerializedName("listing_working")
    @Expose
    private String listing_working;

    /**
     * @return The userid_working
     */
    public Integer getUserid_working() {
        return userid_working;
    }

    /**
     * @param userid_working The userid_working
     */
    public void setUserid_working(Integer userid_working) {
        this.userid_working = userid_working;
    }

    /**
     * @return The profile_phone
     */
    public String getProfile_phone() {
        return profile_phone;
    }

    /**
     * @param profile_phone The profile_phone
     */
    public void setProfile_phone(String profile_phone) {
        this.profile_phone = profile_phone;
    }

    /**
     * @return The shout_id_working
     */
    public Integer getShout_id_working() {
        return shout_id_working;
    }

    /**
     * @param shout_id_working The shout_id_working
     */
    public void setShout_id_working(Integer shout_id_working) {
        this.shout_id_working = shout_id_working;
    }

    /**
     * @return The listing_working
     */
    public String getListing_working() {
        return listing_working;
    }

    /**
     * @param listing_working The listing_working
     */
    public void setListing_working(String listing_working) {
        this.listing_working = listing_working;
    }

}