package com.plabro.realestate.listeners;

import com.android.volley.VolleyError;

import java.text.ParseException;

/**
 * Created by hemant on 07/01/15.
 */
public interface ResponseListener {

    public void onSuccessResponse(String response) throws ParseException;

    public void onFailureResponse(VolleyError volleyError);

}
