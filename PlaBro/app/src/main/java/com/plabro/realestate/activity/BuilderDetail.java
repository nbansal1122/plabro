package com.plabro.realestate.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.desmond.parallaxviewpager.NotifyingScrollView;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ServerResponse;
import com.plabro.realestate.models.profile.builder.BuilderProfile;
import com.plabro.realestate.models.profile.builder.BuilderProfileResponse;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.BuilderFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 11/08/15.
 */
public class BuilderDetail extends PlabroBaseActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener, VolleyListener {

    private SliderLayout mDemoSlider;
    private String builderId, shoutId, cityId;
    private ArrayList<BaseFeed> builderFeeds;
    private TableLayout tableLayout;
    private ProgressDialog dialog;
    Button collaboratedButton;
    private NotifyingScrollView scrollView;
    FrameLayout frame;
    RelativeLayout parent;

    @Override
    protected int getResourceId() {
        return R.layout.builder_detail;
    }

    @Override
    public void findViewById() {
        parent = (RelativeLayout) findViewById(R.id.parent_view);
        frame = (FrameLayout) findViewById(R.id.frame_image_gallery);
        scrollView = (NotifyingScrollView) findViewById(R.id.scrollView);
//        scrollView.setOnScrollChangedListener(new NotifyingScrollView.OnScrollChangedListener() {
//            @Override
//            public void onScrollChanged(ScrollView who, int l, int t, int oldl, int oldt) {
//                Log.d(TAG, "l :" + ", t:" + t + ", oldl:" + oldl + ", oldt:" + oldt + ", Height:" + who.getHeight() + "Y:" + scrollView.getY());
////                who.scrollTo(0, -t);
//                Log.d(TAG, "Scroll Amount:" + scrollView.getMaxScrollAmount());
////                frame.setY(frame.getY() - (t - oldt));
//                frame.setTranslationY(-t);
//                parent.requestLayout();
//
//
////                frame.se
////                scrollView.setTranslationY(-t);
////                scrollView.scrollTo(0, t);
////                scrollView.setY(scrollView.getY()-t);
//            }
//        });

        mDemoSlider = (SliderLayout) findViewById(R.id.slider);

        Bundle b = getIntent().getExtras();
        if (null != b) {
            shoutId = b.getString(Constants.BUNDLE_KEYS.SHOUT_ID);
//            cityId = b.getString(Constants.BUNDLE_KEYS.CITY_ID);
            builderId = b.getString(Constants.BUNDLE_KEYS.BUILDER_ID);
        }
        tableLayout = (TableLayout) findViewById(R.id.table_builder_feed);
        HashMap<String, String> url_maps = new HashMap<String, String>();
        url_maps.put("Hannibal", "http://static2.hypable.com/wp-content/uploads/2013/12/hannibal-season-2-release-date.jpg");
        url_maps.put("Big Bang Theory", "http://tvfiles.alphacoders.com/100/hdclearart-10.png");
        url_maps.put("House of Cards", "http://cdn3.nflximg.net/images/3093/2043093.jpg");
        url_maps.put("Game of Thrones", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");


//        for (String name : url_maps.keySet()) {
//            TextSliderView textSliderView = new TextSliderView(this);
//            // initialize a SliderLayout
//            textSliderView
//                    .description(name)
//                    .image(url_maps.get(name))
//                    .setScaleType(BaseSliderView.ScaleType.Fit)
//                    .setOnSliderClickListener(this);
//
//            //add your extra information
//            textSliderView.bundle(new Bundle());
//            textSliderView.getBundle()
//                    .putString("extra", name);
//
//            mDemoSlider.addSlider(textSliderView);
//        }
//        setDemoSlider(null);
//        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
//        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
//        mDemoSlider.setDuration(4000);
        mDemoSlider.stopAutoCycle();
        mDemoSlider.addOnPageChangeListener(this);
        showDialog();
        getBuilderProfile();
        getBuilderFeed(false);

    }

    private void addBuilderFeed(final BuilderFeed feed) {
        View row = LayoutInflater.from(this).inflate(R.layout.row_builder_feed, null);
        boolean isCollaborate = "collaborate".equalsIgnoreCase(feed.getSub_type()) ? true : false;
        TextView title = (TextView) row.findViewById(R.id.tv_builderFeedTitle);
        TextView time = (TextView) row.findViewById(R.id.tv_builderFeedTime);
        TextView desc = (TextView) row.findViewById(R.id.tv_builderFeedDesc);
        time.setText(feed.getTime());
        TextView info = (TextView) row.findViewById(R.id.tv_interestedInCollabroation);
        final Button btn = (Button) row.findViewById(R.id.btn_collaborate);

        title.setText(feed.getDisplayname());
        desc.setText(feed.getText());
        if (isCollaborate) {
            btn.setVisibility(View.GONE);
            info.setVisibility(View.GONE);
        } else {
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    collaboratedButton = (Button) view;
                    if ("collaborate".equalsIgnoreCase(btn.getText().toString())) {
                        sendCollaboration(true);
                    } else {
                        sendCollaboration(false);
                    }
                }
            });
        }
        tableLayout.addView(row);

    }

    private void sendCollaboration(boolean isCollaboration) {
        HashMap<String, String> params = new HashMap<>();
        params.put(Constants.BUNDLE_KEYS.BUILDER_ID, builderId);
        params.put(Constants.BUNDLE_KEYS.SHOUT_ID, shoutId);
        if (isCollaboration) {
            params = Utility.getInitialParams(PlaBroApi.RT.BUILDER_COLLABORATE, params);
            AppVolley.processRequest(Constants.TASK_CODES.BUILDER_COLLABORATE, ServerResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, this);
        } else {
            params = Utility.getInitialParams(PlaBroApi.RT.BUILDER_UN_COLLABORATE, params);
            AppVolley.processRequest(Constants.TASK_CODES.BUILDER_UN_COLLABORATE, ServerResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, this);
        }
    }

    private void setDemoSlider(BuilderProfile profile) {
        ImageSliderView imageSliderView = new ImageSliderView(this);
        String image = "http://plabro.com/images/3.jpg";
        imageSliderView
                .description("")
                .image(profile.getCompany_dp())
                .setScaleType(BaseSliderView.ScaleType.Fit)
                .setOnSliderClickListener(this);
        mDemoSlider.addSlider(imageSliderView);

    }


    private void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private void setBuilderProfile(BuilderProfile profile) {
        setDemoSlider(profile);
        String logoImg = profile.getCompany_logo();
        RoundedImageView imgView = (RoundedImageView) findViewById(R.id.iv_builder_logo);
        ActivityUtils.loadImage(BuilderDetail.this, logoImg, imgView);
        TextView nameTV = (TextView) findViewById(R.id.tv_builder_name);
        nameTV.setText(profile.getCompany_name());
        findViewById(R.id.btn_customer_support).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityUtils.callPhone(BuilderDetail.this, Constants.PLABRO_CUSTOMER_CARE);
            }
        });
    }

    private void getBuilderProfile() {
        HashMap<String, String> params = new HashMap<>();
        params.put(Constants.BUNDLE_KEYS.BUILDER_ID, builderId);
        params.put(Constants.BUNDLE_KEYS.SHOUT_ID, shoutId);
        params = Utility.getInitialParams(PlaBroApi.RT.GET_BUILDER_PROFILE, params);
        AppVolley.processRequest(Constants.TASK_CODES.GET_BUILDER_PROFILE, BuilderProfileResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, this);
    }

    private void getBuilderFeed(boolean addMore) {
        HashMap<String, String> params = new HashMap<>();
        params.put(Constants.BUNDLE_KEYS.BUILDER_ID, builderId);
//        params.put(Constants.BUNDLE_KEYS.CITY_ID, cityId);
        params = Utility.getInitialParams(PlaBroApi.RT.BUILDER_FEED, params);
        int taskCode = Constants.TASK_CODES.BUILDER_FEEDS;
        if (addMore) {
            taskCode = Constants.TASK_CODES.BUILDER_FEEDS_MORE;
        }
        AppVolley.processRequest(taskCode, FeedResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, this);
    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    @Override
    protected String getTag() {
        return "Builder Detail";
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        hideDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.BUILDER_FEEDS:
                addBuilderShouts(response, false);
                break;
            case Constants.TASK_CODES.BUILDER_FEEDS_MORE:
                addBuilderShouts(response, true);
                break;
            case Constants.TASK_CODES.GET_BUILDER_PROFILE:
                BuilderProfileResponse res = (BuilderProfileResponse) response.getResponse();
                if (res.isStatus()) {
                    if (null != res.getOutput_params() && null != res.getOutput_params().getData()) {
                        BuilderProfile profile = res.getOutput_params().getData();
                        setBuilderProfile(profile);
                    }
                } else {

                }
                break;
            case Constants.TASK_CODES.BUILDER_COLLABORATE:
                ServerResponse resp = (ServerResponse) response.getResponse();
                if (null != resp) {
                    setCollaboratedButton(true, resp);
                }
                break;
            case Constants.TASK_CODES.BUILDER_UN_COLLABORATE:
                ServerResponse resp2 = (ServerResponse) response.getResponse();
                if (null != resp2) {
                    setCollaboratedButton(false, resp2);
                }
                break;
        }
    }

    private void setCollaboratedButton(boolean isCollaborated, ServerResponse response) {
        if (response.isStatus()) {
            if (isCollaborated) {
                collaboratedButton.setText("OPT - OUT");
            } else {
                collaboratedButton.setText("COLLABORATE");
            }
        } else {
            Toast.makeText(BuilderDetail.this, response.getMsg() + "", Toast.LENGTH_LONG).show();
        }
    }

    private void addBuilderShouts(PBResponse response, final boolean addMore) {
        FeedResponse res = (FeedResponse) response.getResponse();
        if (res.isStatus()) {
            if (null != res.getOutput_params() && null != res.getOutput_params().getData()) {
                List<BaseFeed> feedList = res.getOutput_params().getData();
                for (BaseFeed f : feedList) {
                    if (f instanceof BuilderFeed) {
                        addBuilderFeed((BuilderFeed) f);
                    }
                }
            }
        }
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        hideDialog();
        switch (response.getCustomException().getExceptionType()) {
            case NO_INETERNET_EXCEPTION:
                break;
        }
    }

    private class ImageSliderView extends BaseSliderView {

        protected ImageSliderView(Context context) {
            super(context);
        }

        @Override
        public View getView() {
            View v = LayoutInflater.from(getContext()).inflate(com.daimajia.slider.library.R.layout.render_type_text, null);
            ImageView target = (ImageView) v.findViewById(com.daimajia.slider.library.R.id.daimajia_slider_image);
            v.findViewById(R.id.description_layout).setBackgroundColor(getResources().getColor(R.color.transparent));
            TextView description = (TextView) v.findViewById(com.daimajia.slider.library.R.id.description);

//            description.setText(getDescription());
            bindEventAndShow(v, target);
            return v;
        }
    }
}
