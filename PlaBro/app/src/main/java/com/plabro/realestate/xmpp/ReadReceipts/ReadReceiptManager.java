package com.plabro.realestate.xmpp.ReadReceipts;

import com.plabro.realestate.CustomLogger.Log;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.filter.PacketExtensionFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public class ReadReceiptManager implements PacketListener {

    private static final String TAG = ReadReceiptManager.class.getSimpleName();


    private static Map<Connection, ReadReceiptManager> instances = Collections.synchronizedMap(new WeakHashMap<Connection, ReadReceiptManager>());

    static {
        Connection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(Connection connection) {
                getInstanceFor(connection);
            }
        });
    }

    private Set<DeliveryReceiptManager.ReceiptReceivedListener> receiptReceivedListeners = Collections.synchronizedSet(new HashSet<DeliveryReceiptManager.ReceiptReceivedListener>());

    private ReadReceiptManager(Connection connection) {
        Log.d(TAG, "iS Authenticated"+connection.isAuthenticated());
        ServiceDiscoveryManager sdm = ServiceDiscoveryManager.getInstanceFor(connection);
        sdm.addFeature(ReadReceipt.NAMESPACE);
        instances.put(connection, this);

        Log.d(TAG, "Adding Packet Listener");
        connection.addPacketListener(this, new PacketExtensionFilter(ReadReceipt.NAMESPACE));
    }

    public static synchronized ReadReceiptManager getInstanceFor(Connection connection) {
        ReadReceiptManager receiptManager = instances.get(connection);

        if (receiptManager == null) {
            receiptManager = new ReadReceiptManager(connection);
        }

        return receiptManager;
    }

    @Override
    public void processPacket(Packet packet) {
        ReadReceipt dr = (ReadReceipt) packet.getExtension(ReadReceipt.ELEMENT, ReadReceipt.NAMESPACE);

        if (dr != null) {
            for (DeliveryReceiptManager.ReceiptReceivedListener l : receiptReceivedListeners) {

                Log.d(TAG, "Packet XML :"+packet.toXML());
                l.onReceiptReceived(packet.getFrom(), packet.getTo(), dr.getId());
            }
        }
    }

    public void addReadReceivedListener(DeliveryReceiptManager.ReceiptReceivedListener listener) {
        Log.d(TAG, "Adding Read Received Listener");
        receiptReceivedListeners.add(listener);
    }

    public void removeRemoveReceivedListener(DeliveryReceiptManager.ReceiptReceivedListener listener) {
        receiptReceivedListeners.remove(listener);
    }
}