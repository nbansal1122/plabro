package com.plabro.realestate.models.propFeeds;

import android.text.SpannableString;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.location_auto.LocationsAuto;
import com.plabro.realestate.utilities.JSONUtils;
import com.plabro.realestate.utilities.Util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 08/01/15.
 */
@Table(name = "Feeds")
public class Feeds extends BaseFeed implements Serializable {

    private static final String TAG = "Feeds";
    @Column
    private String _id;
    private Boolean bookmarked;
    private String profile_img;
    private String profile_name = "";
    private String displayname;
    private String text;
    private String profile_phone;
    private long authorid;
    private String time;
    private int forall;
    private int delete;
    private transient SpannableString hashTagSummary;
    private transient SpannableString hashTagText;
    private String summ;
    private String sugg_info = "";
    private List<String> snippet = new ArrayList<String>();
    private List<LocationsAuto> hashtags = new ArrayList<LocationsAuto>();
    private List<String> phonemap = new ArrayList<String>();
    private List<String> webmap = new ArrayList<String>();
    private List<String> emailmap = new ArrayList<String>();
    private List<String> hl = new ArrayList<String>();
    private Boolean rel = false;
    private Boolean selected = false;
    private String top_ht = "";
    private String avail_req[] = new String[5];
    private String sale_rent[] = new String[5];
    private String city_id = "";
    private String city_name = "";
    private String business_name = "";
    private int is_profile_enabled = 0;

    public int is_profile_enabled() {
        return is_profile_enabled;
    }

    public void setIs_profile_enabled(int is_profile_enabled) {
        this.is_profile_enabled = is_profile_enabled;
    }

    public String getPost_time() {
        return post_time;
    }

    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }

    private String post_time = "";
    private int isFreeCall = 0;


    public long getBookmark_count() {
        return bookmark_count;
    }

    public void setBookmark_count(long bookmark_count) {
        this.bookmark_count = bookmark_count;
    }

    public long getShares_count() {
        return shares_count;
    }

    public void setShares_count(long shares_count) {
        this.shares_count = shares_count;
    }

    public long getView_count() {
        return views_count;
    }

    public void setView_count(long view_count) {
        this.views_count = view_count;
    }

    private long bookmark_count = 0;
    private long shares_count = 0;
    private long views_count = 0;

    private List<List<String>> hashmaps = new ArrayList<List<String>>();
    private List<List<String>> phonemaps = new ArrayList<List<String>>();
    private List<List<String>> webmaps = new ArrayList<List<String>>();
    private List<List<String>> emailmaps = new ArrayList<List<String>>();

    private List<List<String>> hashmaps_snippet = new ArrayList<List<String>>();
    private List<List<String>> phonemaps_snippet = new ArrayList<List<String>>();
    private List<List<String>> webmaps_snippet = new ArrayList<List<String>>();
    private List<List<String>> emailmaps_snippet = new ArrayList<List<String>>();

    private String incomingNumber = "";
    private boolean isFromProfile;

    public boolean isFromProfile() {
        return isFromProfile;
    }

    public void setIsFromProfile(boolean isFromProfile) {
        this.isFromProfile = isFromProfile;
    }

    public String getIncomingNumber() {
        return incomingNumber;
    }

    public void setIncomingNumber(String incomingNumber) {
        this.incomingNumber = incomingNumber;
    }
    // private transient JSONObject feedJson;


    public SpannableString getHashTagText() {
        return hashTagText;
    }

    public void setHashTagText(SpannableString hashTagText) {
        this.hashTagText = hashTagText;
    }

    public SpannableString getHashTagSummary() {
        return hashTagSummary;
    }

    public void setHashTagSummary(SpannableString hashTagSummary) {
        this.hashTagSummary = hashTagSummary;
    }


    public String getSumm() {
        return summ;
    }

    public void setSumm(String summ) {
        this.summ = summ;
    }


    public String get_id() {
        Log.d(Feeds.class.getSimpleName(), "Getting ID:" + _id);
        return _id;
    }

    public void set_id(String _id) {
        Log.d(Feeds.class.getSimpleName(), "Setting ID:" + _id);
        this._id = _id;
    }

    public Boolean getBookmarked() {
        return bookmarked;
    }

    public void setBookmarked(Boolean bookmarked) {
        this.bookmarked = bookmarked;
    }

    public String getProfile_img() {
        return profile_img;
    }

    public void setProfile_img(String profile_img) {
        this.profile_img = profile_img;
    }

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public String getText() {
        return Util.decodeFromUTF(text);
    }


    public String getProfile_phone() {
        return profile_phone;
    }

    public void setProfile_phone(String profile_phone) {
        this.profile_phone = profile_phone;
    }

    public long getAuthorid() {
        return authorid;
    }

    public void setAuthorid(long authorid) {
        this.authorid = authorid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getForall() {
        return forall;
    }

    public void setForall(int forall) {
        this.forall = forall;
    }

    public int getDelete() {
        return delete;
    }

    public void setDelete(int delete) {
        this.delete = delete;
    }

    public List<LocationsAuto> getHashtags() {
        return Util.getNewHashTagsIndices(text, hashtags, phonemap, webmap);
    }

    public List<LocationsAuto> getHashtags(String text) {
        return Util.getNewHashTagsIndices(text, hashtags, phonemap, webmap);
    }

    public List<String> getHashingList() {
        List<String> hashList = new ArrayList<String>();
        for (LocationsAuto la : hashtags) {
            hashList.add(la.getTitle());
        }

        hashList.addAll(phonemap);
        hashList.addAll(webmap);
        hashList.addAll(emailmap);

        return hashList;
    }

    public List<String[]> getHashingListIndices() {
        List<String[]> hashList = new ArrayList<String[]>();

        for (List<String> val : hashmaps) {
            String[] indices = new String[2];
            indices[0] = val.get(0);
            indices[1] = val.get(1);
            hashList.add(indices);
        }


        for (List<String> val : webmaps) {
            String[] indices = new String[2];
            indices[0] = val.get(0);
            indices[1] = val.get(1);
            hashList.add(indices);
        }

        for (List<String> val : emailmaps) {
            String[] indices = new String[2];
            indices[0] = val.get(0);
            indices[1] = val.get(1);
            hashList.add(indices);
        }

        for (List<String> val : phonemaps) {
            String[] indices = new String[2];
            indices[0] = val.get(0);
            indices[1] = val.get(1);
            hashList.add(indices);
        }

        return hashList;
    }

    public List<String[]> getHashingListIndicesSnippet() {
        List<String[]> hashList = new ArrayList<String[]>();

        for (List<String> val : hashmaps_snippet) {
            String[] indices = new String[2];
            indices[0] = val.get(0);
            indices[1] = val.get(1);
            hashList.add(indices);
        }


        for (List<String> val : webmaps_snippet) {
            String[] indices = new String[2];
            indices[0] = val.get(0);
            indices[1] = val.get(1);
            hashList.add(indices);
        }

        for (List<String> val : emailmaps_snippet) {
            String[] indices = new String[2];
            indices[0] = val.get(0);
            indices[1] = val.get(1);
            hashList.add(indices);
        }

        for (List<String> val : phonemaps_snippet) {
            String[] indices = new String[2];
            indices[0] = val.get(0);
            indices[1] = val.get(1);
            hashList.add(indices);
        }

        return hashList;
    }

//    public JSONObject getFeedJson() {
//        return feedJson;
//    }
//
//    public void setFeedJson(JSONObject feedJson) {
//        this.feedJson = feedJson;
//    }

    public void setHashtags(List<LocationsAuto> hashtags) {
        this.hashtags = hashtags;
    }

    public String[] getSale_rent() {
        return sale_rent;
    }

    public String[] getAvail_req() {
        return avail_req;
    }

    public List<String> getSnippet() {
        return snippet;
    }

    public void setSnippet(List<String> snippet) {
        this.snippet = snippet;
    }

    public List<String> getPhonemap() {
        return phonemap;
    }

    public List<String> getWebmap() {
        return webmap;
    }

    public List<String> getHl() {
        return hl;
    }

    public Boolean isRel() {
        return rel;
    }

    public Boolean isBookmarked() {
        return bookmarked;
    }

    public String getDisplayname() {
        return displayname;
    }

    public Boolean isSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getSugg_info() {
        return sugg_info;
    }

    public String getTop_ht() {
        return top_ht;
    }

    public List<String> getEmailmap() {
        return emailmap;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public int getIsFreeCall() {
        return isFreeCall;
    }

    public void setIsFreeCall(int isFreeCall) {
        this.isFreeCall = isFreeCall;
    }

    public static BaseFeed parseJson(String json) {
        Feeds f = JSONUtils.getGSONBuilder().create().fromJson(json, Feeds.class);
//        try {
//            f.setFeedJson(new JSONObject(json));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        return f;

    }
}
