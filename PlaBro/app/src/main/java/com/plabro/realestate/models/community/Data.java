package com.plabro.realestate.models.community;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nbansal2211 on 18/05/16.
 */
public class Data {

    @SerializedName("waitingComms")
    @Expose
    private List<JoinedComm> waitingComms = new ArrayList<JoinedComm>();
    @SerializedName("joinedComms")
    @Expose
    private List<JoinedComm> joinedComms = new ArrayList<JoinedComm>();

    /**
     * @return The waitingComms
     */
    public List<JoinedComm> getWaitingComms() {
        return waitingComms;
    }

    /**
     * @param waitingComms The waitingComms
     */
    public void setWaitingComms(List<JoinedComm> waitingComms) {
        this.waitingComms = waitingComms;
    }

    /**
     * @return The joinedComms
     */
    public List<JoinedComm> getJoinedComms() {
        return joinedComms;
    }

    /**
     * @param joinedComms The joinedComms
     */
    public void setJoinedComms(List<JoinedComm> joinedComms) {
        this.joinedComms = joinedComms;
    }

}
