package com.plabro.realestate.models.community;

import com.google.gson.annotations.SerializedName;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Admin implements Serializable{

    @SerializedName("s3CompressUrl")
    @Expose
    private String s3CompressUrl;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("authorid")
    @Expose
    private Integer authorid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("uid")
    @Expose
    private Integer uid;

    private boolean isAdmin;

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    /**
     * @return The s3CompressUrl
     */
    public String getS3CompressUrl() {
        return s3CompressUrl;
    }

    /**
     * @param s3CompressUrl The s3CompressUrl
     */
    public void setS3CompressUrl(String s3CompressUrl) {
        this.s3CompressUrl = s3CompressUrl;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The authorid
     */
    public Integer getAuthorid() {
        return authorid;
    }

    /**
     * @param authorid The authorid
     */
    public void setAuthorid(Integer authorid) {
        this.authorid = authorid;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The img
     */
    public String getImg() {
        return img;
    }

    /**
     * @param img The img
     */
    public void setImg(String img) {
        this.img = img;
    }

    /**
     * @return The uid
     */
    public Integer getUid() {
        return uid;
    }

    /**
     * @param uid The uid
     */
    public void setUid(Integer uid) {
        this.uid = uid;
    }

}
