package com.plabro.realestate.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.holders.BaseFeedHolder;
import com.plabro.realestate.listeners.OnUpdateListener;
import com.plabro.realestate.models.propFeeds.BaseFeed;

import java.util.ArrayList;

/**
 * Created by nitin on 10/09/15.
 */
public class RelatedCustomAdapter extends FeedsCustomAdapter {

    private View footerView;
    private static final String TAG = "Related Adapter";

    public int getFooterViewType() {
        return footerViewType;
    }

    public void setFooterViewType(int footerViewType) {
        this.footerViewType = footerViewType;
    }

    private int footerViewType;

    public int getHeaderHeight() {
        return headerHeight;
    }

    public View getFooterView() {

        return footerView;
    }

    public void setFooterView(View footerView) {
        this.footerView = footerView;
    }

    private int headerHeight;

    public RelatedCustomAdapter(ArrayList<BaseFeed> feeds, String type, Activity activity, Context context, int refKey) {
        super(feeds, type, activity, context, refKey);
    }

    public RelatedCustomAdapter(ArrayList<BaseFeed> feeds, Activity mActivity, String type) {
        super(feeds, mActivity, type);
    }

    public RelatedCustomAdapter(ArrayList<BaseFeed> feeds, String type, Activity activity, int refKey) {
        super(feeds, type, activity, refKey);
    }

    public RelatedCustomAdapter(ArrayList<BaseFeed> feeds, String type, Activity activity, OnUpdateListener bookmarkDeleteListener) {
        super(feeds, type, activity, bookmarkDeleteListener);
    }


    @Override
    public int getItemCount() {
        Log.d(TAG, type + ":Related Feeds Size:" + feeds.size());
        return feeds == null ? 1 : feeds.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        Log.d(TAG, "Get Item View Type : Asking For position :" + position);
        if (position == 0) {
            return TYPE_HEADER;
        }
        int vt = super.getItemViewType(position - 1);
        Log.d(TAG, "Position :" + position + ", View Type" + vt);
        return vt;
    }

    @Override
    public BaseFeedHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Log.d(TAG, " onCreateViewHolder View Type:" + viewType);
        switch (viewType) {
            case TYPE_HEADER:
                View v = new View(viewGroup.getContext());
                Log.d(TAG, "Header Height:" + getHeaderHeight());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getHeaderHeight());
                v.setLayoutParams(params);
                return new RecyclerHeaderViewHolder(v);
        }
        return super.onCreateViewHolder(viewGroup, viewType);
    }

    @Override
    public void onBindViewHolder(BaseFeedHolder holder, int position) {
        Log.d(TAG, "OnBind ViewHolder : Asking For position :" + position);
        if (position == 0) return;
        super.onBindViewHolder(holder, position - 1);
    }

    public void setHeaderHeight(int headerHeight) {
        this.headerHeight = headerHeight;
    }

    private static class RecyclerHeaderViewHolder extends BaseFeedHolder {

        public RecyclerHeaderViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "Calling Header");
        }
    }
}
