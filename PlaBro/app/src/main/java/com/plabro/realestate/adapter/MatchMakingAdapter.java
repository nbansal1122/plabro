package com.plabro.realestate.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.models.matchMaking.MatchMakingData;
import com.plabro.realestate.models.matchMaking.PlabroWorking;
import com.plabro.realestate.uiHelpers.RoundedImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 10/12/15.
 */
public class MatchMakingAdapter extends BaseExpandableListAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    ArrayList<MatchMakingData> list;
    private String emptyName = "";


    public MatchMakingAdapter(Activity c, List<MatchMakingData> list) {
        this.inflater = LayoutInflater.from(c);
        this.activity = c;
        this.list = (ArrayList<MatchMakingData>) list;
    }


    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        ChildViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_child_matchmaking, null);
            holder = new ChildViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ChildViewHolder) convertView.getTag();
        }

        PlabroWorking child = this.list.get(groupPosition).getListing_working().get(childPosition);
        holder.workingResponse.setText(child.getListing_working());
        holder.ineterested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("MMA", "Interested clicked");
            }
        });
        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("MMA", "Call clicked");
            }
        });
        return convertView;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder holder;
        MatchMakingData group = this.list.get(groupPosition);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_group_matchmaking, null);
            holder = new GroupViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (GroupViewHolder) convertView.getTag();
        }


        holder.listingTitle.setText(group.getListing_src());
        holder.workingCount.setText(group.getListing_working().size() + "");

        return convertView;
    }

    private View getEmptyViewForEndorsements() {
        View v = inflater.inflate(R.layout.view_empty_endorsements, null);
        TextView view = (TextView) v.findViewById(R.id.tv_help_endorsement);
        String text = String.format((this.activity.getString(R.string.help_endorsement)), this.emptyName);
        view.setText(text);
        return v;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        //return (list.get(groupPosition).getEndorser_list()).size();
        return this.list.get(groupPosition).getListing_working().size(); //todo: prannoy rating changes
    }

    @Override
    public Object getGroup(int groupPosition) {
        return list.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return list.size();
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    private class ChildViewHolder {
        TextView workingResponse, ineterested, call;

        public ChildViewHolder(View view) {
            this.workingResponse = (TextView) view.findViewById(R.id.tv_working_summary);
            this.ineterested = (TextView) view.findViewById(R.id.tv_interested);
            this.call = (TextView) view.findViewById(R.id.tv_call);
        }
    }

    private class GroupViewHolder {
        TextView listingTitle, workingCount;
        ImageView expandIcon;

        public GroupViewHolder(View view) {
            this.listingTitle = (TextView) view.findViewById(R.id.tv_title);
            this.workingCount = (TextView) view.findViewById(R.id.tv_working_count);
            this.expandIcon = (ImageView) view.findViewById(R.id.iv_expand);
        }
    }

}