package com.plabro.realestate.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.FeedsCustomAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.HidingScrollListener;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.location_feeds.LocationData;
import com.plabro.realestate.models.location_feeds.LocationFeedsResponse;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.PlabroLocationManagerTwo;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MapsMarkerActivity extends PlabroBaseActivity implements GoogleMap.OnMarkerClickListener {

    private FeedsCustomAdapter mAdapter;
    protected SupportMapFragment mapFullFragment;
    protected PlabroBaseActivity mMasterActivity;
    protected static CameraPosition cameraPosition = null;
    protected GoogleMap mMapFullScreen;
    //protected Category mCategory = Category.RESTAURANTS;
    LayoutInflater mInflator;
    ArrayList<Feeds> results = new ArrayList<>();
    ArrayList<Marker> markers = new ArrayList<>();

    Map<Marker, LocationData> theMap = new HashMap<>();

    protected Feeds selectedFeed = null;

    LatLng focus = null;
    private RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    private ArrayList<Feeds> feeds2 = new ArrayList<Feeds>();
    private int hashKey = 0;
    private RelativeLayout mHeaderLayout;


    @Override
    protected String getTag() {
        return "MapsMarkerActivity";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Util.i("MarMarkerActivity");
        super.onCreate(savedInstanceState, R.layout.activity_maps_marker);

        //results = getIntent().getExtras().getParcelableArrayList(Constants.LOCATION_MAP_VIEW_KEY);

        /*Feeds resto = results.get(0);
        float[] loc_long_lat = new float[2];
        loc_long_lat[0] = Float.parseFloat(resto.getLongitude());
        loc_long_lat[1] = Float.parseFloat(resto.getLatitude());
        LatLng location = new LatLng(loc_long_lat[1], loc_long_lat[0]);
        focus = location;
        Log.d("util", "focus = " + focus.toString());

        mapFullFragment = ((MapFragment) getFragmentManager().findFragmentById(
                R.id.mapFullScreen));
        setUpMapIfNeeded();


        mMasterActivity = this;*/
        mHeaderLayout = (RelativeLayout) findViewById(R.id.ll_header_layout);

        mapFullFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(
                R.id.mapFullScreen));
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        setRecyclerViewLayoutManager();

        focus = new LatLng(28.4700, 77.0300);
        setUpMapIfNeeded();
        showFullScreenMap();
        mMasterActivity = this;

        mRecyclerView.setOnScrollListener(hSL);


    }


    public void setRecyclerViewLayoutManager() {
        int scrollPosition = 0;
        mLayoutManager = new LinearLayoutManager(MapsMarkerActivity.this);
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }


    private void setListAdapter(boolean scrollToLast) {
       /* int pos;
        feeds2 = getFeedsFromDB();
        mAdapter = new FeedsCustomAdapter(feeds2, "normal",getActivity(),getActivity());
        mRecyclerView.setAdapter(mAdapter);
        if (mAdapter.getItemCount() > 0) {
            noFeeds.setVisibility(View.GONE);
        } else {
            noFeeds.setVisibility(View.VISIBLE);
        }*/

//        mAdapter = new FeedsCustomAdapter(feeds2, "normal", MapsMarkerActivity.this, MapsMarkerActivity.this, hashKey);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void showFullScreenMap() {

        Log.d("util", focus.toString());
        getSupportFragmentManager().beginTransaction().show(mapFullFragment).commit();

        cameraPosition = new CameraPosition.Builder().target(focus)
                .zoom(16.5f).bearing(0).tilt(0).build();

        if (mMapFullScreen != null) {
            mMapFullScreen.animateCamera(
                    CameraUpdateFactory.newCameraPosition(cameraPosition), null);
            setUpFullScreenMap();
        }


    }

    protected void addMarkerToFullScreenMap(LocationData locationData) {

//        float[] loc_long_lat = new float[2];
//        loc_long_lat[0] = Float.parseFloat(lat);
//        loc_long_lat[1] = Float.parseFloat(lng);


        View markerView = ((LayoutInflater) this.
                getSystemService(
                        this.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.map_marker_layout, null);
        ((TextView) markerView.findViewById(R.id.tv_marker_text)).setText("3");


        // LatLng location = new LatLng(28.4700, 77.0300);

        LatLng location = new LatLng(locationData.getCoordinates()[0], locationData.getCoordinates()[1]);
//
//        Marker markerFullScreenMap = mMapFullScreen
//                .addMarker(new MarkerOptions()
//                        .position(location)
//                        .icon(BitmapDescriptorFactory
//                                .fromResource(R.drawable.ic_map_pin)));


        Marker markerFullScreenMap = mMapFullScreen
                .addMarker(new MarkerOptions()
                        .position(location)
                        .icon(BitmapDescriptorFactory
                                .fromBitmap(createDrawableFromView(
                                        this,
                                        markerView))));

        markers.add(markerFullScreenMap);

        theMap.put(markerFullScreenMap, locationData);

        mMapFullScreen.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {
                // TODO Auto-generated method stub

                for (int i = 0; i < markers.size(); i++) {
                    markers.get(i).setIcon(BitmapDescriptorFactory
                            .fromResource(R.drawable.ic_map_pin));
                }

                marker.setIcon(BitmapDescriptorFactory
                        .fromResource(R.drawable.ic_map_pin)); //pressed

                // findViewById(R.id.mapMarkerPopup).setVisibility(View.VISIBLE);
                //  TextView name_tv = (TextView) findViewById(R.id.resto_name_tv);
                //  TextView address_tv = (TextView) findViewById(R.id.resto_address_tv);

                LocationData ld = theMap.get(marker);
                // selectedFeed = ld.getShouts().get(0); todo uncomment


                //   addFeedsToView(ld.getShouts());

                setListAdapter(false);
                // feeds2 = ld.getShouts();
                if (feeds2.size() > 0) {
                    //findViewById(R.id.ll_feed_list).setVisibility(View.VISIBLE);
                    setListAdapter(false);
                }

                return true;
            }
        });


//		mMapFullScreen.setInfoWindowAdapter(new InfoWindowAdapter() {
//
//			// Use default InfoWindow frame
//			@Override
//			public View getInfoWindow(Marker marker) {
//
////				// Getting view from the layout file info_window_layout
//				View v = getLayoutInflater().inflate(R.layout.map_window_popup,
//						null);
////
//				RestoShort resto = theMap.get(marker);
////				// Getting reference to the TextView to set title
////
//				TextView title = (TextView) v.findViewById(R.id.popup_title);
//				TextView address = (TextView) v
//						.findViewById(R.id.popup_address);
//				title.setText(resto.getName());
//				title.setTypeface(TcFonts.ROBOTTO_NORMAL);
//				address.setText(resto.getAddress().getFullAddress());
//				address.setTypeface(TcFonts.ROBOTTO_LIGHT);
////				// Returning the view containing InfoWindow contents
//
//				return v;
//
//			}
//
//			// Defines the contents of the InfoWindow
//			@Override
//			public View getInfoContents(Marker marker) {
//
//				return null;
//
//			}
//
//		});

    }

//    void addFeedsToView(ArrayList<Feeds> shouts)
//    {
//        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View v = vi.inflate(R.layout.cards_layout, null);
//        View v1 = vi.inflate(R.layout.cards_layout, null);
//
//// fill in any details dynamically here
//
//
//// insert into main view
//        ViewGroup insertPoint = (ViewGroup) findViewById(R.id.ll_feed_list);
//        insertPoint.removeAllViews();
//        insertPoint.addView(v, 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
//        insertPoint.addView(v1, 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
//
//        findViewById(R.id.ll_feed_list).setVisibility(View.VISIBLE);
//
//
//    }

    protected void setUpFullScreenMap() {
        // Hide the zoom controls as the button panel will cover it.
        mMapFullScreen.getUiSettings().setZoomControlsEnabled(false);
        mMapFullScreen.setTrafficEnabled(true);
        mMapFullScreen.setMyLocationEnabled(false);
        mMapFullScreen.setIndoorEnabled(true);
        mMapFullScreen.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMapFullScreen.getUiSettings().setAllGesturesEnabled(true);

        mMapFullScreen.moveCamera(CameraUpdateFactory.newLatLngZoom(focus,
                10));

        // Add lots of markers to the map.
        fetchFeedsToShow();


        // Pan to see all markers in view.
        // Cannot zoom to bounds until the map has a size.
        final View mapView = getSupportFragmentManager().findFragmentById(
                R.id.mapFullScreen).getView();
        checkMapObserver(mapView);

    }

    private void fetchFeedsToShow() {
        Location location = PlabroLocationManagerTwo.getInstance(MapsMarkerActivity.this).getLastKnownLocation();
        getData(location.getLatitude(), location.getLongitude());
//        LocationData ld = new LocationData();
//        addMarkerToFullScreenMap(ld);

    }

    protected void checkMapObserver(final View mapView) {
        if (mapView.getViewTreeObserver().isAlive()) {
            mapView.getViewTreeObserver().addOnGlobalLayoutListener(
                    new ViewTreeObserver.OnGlobalLayoutListener() {
                        @SuppressWarnings("deprecation")
                        @SuppressLint("NewApi")
                        // We check which build version we are using.
                        @Override
                        public void onGlobalLayout() {
                            // LatLngBounds bounds = new
                            // LatLngBounds.Builder().include(restaurantLocation).build();
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                mapView.getViewTreeObserver()
                                        .removeGlobalOnLayoutListener(this);
                            } else {
                                mapView.getViewTreeObserver()
                                        .removeOnGlobalLayoutListener(this);
                            }
                        }
                    });
        }
    }

    protected void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the
        // map.
        if (mMapFullScreen == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMapFullScreen = mapFullFragment.getMap();
            // Check if we were successful in obtaining the map.
        }

    }

    @Override
    public void onBackPressed() {

//		if (mapFullFragment != null && mapFullFragment.isVisible()) {
//
//			FragmentTransaction mFragmentTransaction = getFragmentManager()
//					.beginTransaction();
//
//			mFragmentTransaction.hide(mapFullFragment).commit();
//
//		} else {
//
//			ActivityManager am = (ActivityManager) this
//					.getSystemService(Context.ACTIVITY_SERVICE);
//
//			int sizeStack = am.getRunningTasks(2).size();
//
//			for (int i = 0; i < sizeStack; i++) {
//
//				ComponentName cn = am.getRunningTasks(2).get(i).topActivity;
//				Util.d("BackStack Activity Name" + cn.getClassName());
//			}

        super.onBackPressed();
//		}
    }

    public void DirectionsButtonClicked(View view) {

      /*  LatLng location = null;
        location = new LatLng(Double.parseDouble(selectedResto.getLongitude()), Double.parseDouble(selectedResto.getLatitude()));
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?saddr="
                        + LocationDetails.getInstance().getLatitude() + ","
                        + LocationDetails.getInstance().getLongitude() + "&daddr="
                        + location.latitude + "," + location.longitude));
        startActivity(intent);
*/
    }

    public void DetailsButtonClicked(View view) {
        Toast.makeText(this, "detail clicked", Toast.LENGTH_LONG).show();
        /*if (selectedResto.isResturant())
            mMasterActivity.navigateToDetailsScreen(Category.RESTAURANTS, selectedResto.getId(), selectedResto.getName());
        else
            mMasterActivity.navigateToDetailsScreen(Category.NIGHTLIFES, selectedResto.getId(), selectedResto.getName());*/
    }

    public void BackArrowButtonClicked(View view) {
        finish();
    }

    public void onButtonClick(View view) {
        showFullScreenMap();
    }

    @Override
    public boolean onMarkerClick(Marker arg0) {
        // TODO Auto-generated method stub
        return false;
    }


    @Override
    public void findViewById() {

        // getData();

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void inflateToolbar() {

    }

    public void getData(Double lat, Double lng) {


        if (!Util.haveNetworkConnection(MapsMarkerActivity.this)) {


        }


        HashMap<String, String> params = new HashMap<String, String>();

        params.put(PlaBroApi.PLABRO_MAP_POST_PARAM_LOC_LAT, lat + "");
        params.put(PlaBroApi.PLABRO_MAP_POST_PARAM_LOC_LONG, lng + "");

        params = Utility.getInitialParams(PlaBroApi.RT.GRID_RECORDS, params);


        AppVolley.processRequest(Constants.TASK_CODES.GRID_RECORDS, LocationFeedsResponse.class, null, String.format(PlaBroApi.getBaseUrl(MapsMarkerActivity.this)), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                if (response != null) {


                    LocationFeedsResponse locationFeedsResponse = (LocationFeedsResponse) response.getResponse();


                        ArrayList<LocationData> locationDatas = (ArrayList<LocationData>) locationFeedsResponse.getOutput_params().getData();

                        // addMarkersToMap();
                        for (int i = 0; i < locationDatas.size(); i++) {
                            LocationData locationData = locationDatas.get(i);
                            addMarkerToFullScreenMap(locationData);
                        }

//                        if (feeds2 != null && feeds2.size() > 0) {
//
//                            noFeeds.setVisibility(View.GONE);
//                            if (feeds2 != null && feeds2.size() != 0) {
//                                if (!addMore) {
//                                    mAdapter = new FeedsCustomAdapter(mContext, feeds2, getActivity());
//                                    mRecyclerView.setAdapter(mAdapter);
//
//                                } else {
//                                    mAdapter.addItems(feeds2);
//                                    mProgressWheelLoadMore.stopSpinning();
//                                    mProgressWheelLoadMore.setVisibility(View.GONE);
//
//                                }
//                                page = page + 10;
//                            }
//
//                        } else {
//
//                            if (page < 11) {
//                                noFeeds.setVisibility(View.VISIBLE);
//                            } else {
//
//                                noFeeds.setVisibility(View.GONE);
//
//                            }
//                        }
//
//                        mProgressWheel.stopSpinning();
//                        isLoading = false;
//
//                        Log.i("PlaBro", "Feeds : " + feeds2.size());
//                    } else {
//                        if (feedResponse.getMsg().contains("Session Expired") || feedResponse.getMsg().equalsIgnoreCase("Missing AuthKey in the request")) {
//                            Utility.userLogin(getActivity());
//
//                            if (addMore) {
//                                mProgressWheelLoadMore.stopSpinning();
//                                mProgressWheelLoadMore.setVisibility(View.GONE);
//                            } else {
//                                final MaterialDialog mMaterialDialog = new MaterialDialog(getActivity());
//
//                                mMaterialDialog.setTitle("Try again")
//                                        .setMessage("Oops ! Your Session Expired.")
//                                        .setPositiveButton("CONNECT", new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View v) {
//                                                Utility.userLogin(getActivity());
//                                                getData(addMore);
//                                                mMaterialDialog.dismiss();
//                                            }
//                                        })
//
//                                        .setNegativeButton("CANCEL", new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View v) {
//                                                mMaterialDialog.dismiss();
//                                            }
//                                        });
//
//                                mMaterialDialog.show();
//                            }
//
//                        }
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

                Toast.makeText(MapsMarkerActivity.this, "Oops ! Something went wrong.", Toast.LENGTH_SHORT).show();
//
//                if (addMore) {
////
//                    mProgressWheelLoadMore.stopSpinning();
//                    mProgressWheelLoadMore.setVisibility(View.GONE);
//                } else {
//                    final MaterialDialog mMaterialDialog = new MaterialDialog(getActivity());
//
//                    mMaterialDialog.setTitle("Try again")
//                            .setMessage("Oops ! Something went wrong.")
//                            .setPositiveButton("OK", new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    getData(addMore);
//                                    mMaterialDialog.dismiss();
//                                }
//                            })
//
//                            .setNegativeButton("CANCEL", new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    mMaterialDialog.dismiss();
//                                }
//                            });
//
//                    mMaterialDialog.show();
//                }
//
//            }
            }
        });

    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay()
                .getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels,
                displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }


    HidingScrollListener hSL = new HidingScrollListener(300) {
        @Override
        public void onHide(RecyclerView recyclerView, int dx, int dy) {

            mHeaderLayout.animate().translationY(-mHeaderLayout.getHeight()).setInterpolator(new AccelerateInterpolator(2));

        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
        @Override
        public void onShow(RecyclerView recyclerView, int dx, int dy) {
            mHeaderLayout.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));

        }

        @Override
        public void onScrolledFire(RecyclerView recyclerView, int dx, int dy) {


            if (checkIfLayoutOnTop()) {
                mHeaderLayout.setAnimation(animTopInSlow);
                mHeaderLayout.setVisibility(View.VISIBLE);
            }
        }
    };

    boolean checkIfLayoutOnTop() {
        if (mLayoutManager.findViewByPosition(mLayoutManager.findFirstVisibleItemPosition()).getTop() == 0 && mLayoutManager.findFirstVisibleItemPosition() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
