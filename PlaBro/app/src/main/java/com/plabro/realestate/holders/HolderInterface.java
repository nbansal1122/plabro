package com.plabro.realestate.holders;

import com.plabro.realestate.models.propFeeds.BaseFeed;

/**
 * Created by nitin on 14/08/15.
 */
public interface HolderInterface {
    public void onBindViewHolder(int position, BaseFeed feed);
}
