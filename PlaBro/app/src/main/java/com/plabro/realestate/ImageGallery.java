package com.plabro.realestate;


/**
 * Created by hemant on 2/6/15.
 */

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.uiHelpers.images.ImageDetailFragment;
import com.plabro.realestate.uiHelpers.images.PBViewPager;
import com.plabro.realestate.utilities.PBFonts;

import java.util.ArrayList;
import java.util.List;


public class ImageGallery extends PlabroBaseActivity implements View.OnClickListener {

    public static final String EXTRA_IMAGE = "position";

    private ImagePagerAdapter mAdapter;
    // private ImageFetcher mImageFetcher;
    private PBViewPager mPager;
    private TextView showingImageNumber;
    private String type;

    List<String> menus = new ArrayList<String>();
    int position = 0;

    @Override
    protected String getTag() {
        return "ImageGallery";
    }


    @Override
    protected int getResourceId() {
        return R.layout.activity_image_gallery;
    }

    @Override
    public void onResume() {
        super.onResume();
        //    mImageFetcher.setExitTasksEarly(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //    mImageFetcher.setExitTasksEarly(true);
        // mImageFetcher.flushCache();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setDisplayUseLogoEnabled(false);
        }
        return true;
    }

    /**
     * Called by the ViewPager child fragments to load images via the one ImageFetcher
     */
    // public ImageFetcher getImageFetcher() {
    //  return mImageFetcher;
    //}

    /**
     * The main adapter that backs the ViewPager. A subclass of FragmentStatePagerAdapter as there could be a large number of items in the ViewPager
     * and we don't want to retain them all in memory at once but create/destroy them on the fly.
     */
    private class ImagePagerAdapter extends FragmentStatePagerAdapter {
        private final int mSize;

        public ImagePagerAdapter(FragmentManager fm, int size) {
            super(fm);
            mSize = size;
        }

        @Override
        public int getCount() {
            return mSize;
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return ImageDetailFragment.newInstance(menus.get(position));
        }
    }

    /**
     * Set on the ImageView in the ViewPager children fragments, to enable/disable low profile mode when the ImageView is touched.
     */
    @SuppressLint("InlinedApi")
    @TargetApi(11)
    @Override
    public void onClick(View v) {
        final int vis = mPager.getSystemUiVisibility();
        if ((vis & View.SYSTEM_UI_FLAG_LOW_PROFILE) != 0) {
            mPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        } else {
            mPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
        }
    }

    @SuppressLint("InlinedApi")
    @Override
    public void findViewById() {

        menus = getIntent().getStringArrayListExtra("menus");
        position = getIntent().getIntExtra("position", 0);
        type = getIntent().getStringExtra("type");
        showingImageNumber = ((TextView) findViewById(R.id.tv_showing_menu_image));
        showingImageNumber.setTypeface(PBFonts.GEORGIA_ITALIC, Typeface.ITALIC);
        showingImageNumber.setText(String.format(getString(R.string.gallery_counter), position + 1, menus.size()));

        ((ImageButton) findViewById(R.id.ib_menu_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                ImageGallery.this.onBackPressed();
            }
        });

        // Set up ViewPager and backing adapter
        mAdapter = new ImagePagerAdapter(getSupportFragmentManager(), menus.size());
        mPager = (PBViewPager) findViewById(R.id.pagerFullScreen);
        mPager.setPagingEnabled(true);

        mPager.setAdapter(mAdapter);
        mPager.setPageMargin((int) getResources().getDimension(R.dimen.image_detail_pager_margin));
        mPager.setOffscreenPageLimit(2);

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int arg0) {
                showingImageNumber.setText(String.format(getString(R.string.gallery_counter), arg0 + 1, menus.size()));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });

        // Set up activity to go full screen
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Set the current item based on the extra passed in to this activity
        final int extraCurrentItem = getIntent().getIntExtra(EXTRA_IMAGE, -1);
        if (extraCurrentItem != -1) {
            mPager.setCurrentItem(extraCurrentItem);
        }

    }

    @Override
    public void setViewSizes() {
        // TODO Auto-generated method stub

    }

    @Override
    public void callScreenDataRequest() {
        // setTitle("Menus & Photos");
        // getActionBar().setDisplayShowTitleEnabled(true);
        // getActionBar().setDisplayHomeAsUpEnabled(true);
        // getActionBar().setDisplayShowHomeEnabled(false);
        // getActionBar().setDisplayUseLogoEnabled(false);
    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void inflateToolbar() {

    }

}
