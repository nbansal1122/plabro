package com.plabro.realestate.enums;


/**
 * Created by hemantkumar on 07/01/15.
 */
public enum RequestMethod {

    GET(0), POST(1);

    private int serial;


    RequestMethod(int serial) {
        this.serial = serial;
    }

    public static int getRequestSerial(RequestMethod method) {
        return method.serial;
    }

    public static RequestMethod getRequestMethod(int serial) {

        switch (serial) {
            case 0:
                return RequestMethod.GET;
            case 1:
                return RequestMethod.POST;
            default:
                return RequestMethod.GET;
        }

    }


}
