/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.plabro.realestate.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.activity.Profile;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.widgets.RelativeTimeTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class XMPPCustomAdapter extends BaseAdapter {

    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<ProfileClass> genResultList = new ArrayList<ProfileClass>();
    private List<ProfileClass> arrayList = new ArrayList<ProfileClass>();

    public XMPPCustomAdapter(Context context, List<ProfileClass> genResultList) {
        mContext = context;
        this.genResultList = genResultList;
        inflater = LayoutInflater.from(mContext);
        arrayList.addAll(genResultList);

    }

    public class ViewHolder {
        TextView messageTextSen;
        TextView messageTextRec;
        RelativeLayout sentLayout, receivedLayout;
        RelativeTimeTextView timeSent, timeRec;
        ImageView statusIcon;
    }

    @Override
    public int getCount() {
        return genResultList.size();
    }

    @Override
    public ProfileClass getItem(int position) {
        return genResultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.message_layout, null);
            // Locate the TextViews in listview_item.xml
            holder.messageTextRec = (TextView) view.findViewById(R.id.messageTextRec);
            holder.receivedLayout = (RelativeLayout) view.findViewById(R.id.received);
            holder.messageTextSen = (TextView) view.findViewById(R.id.messageTextSen);
            holder.sentLayout = (RelativeLayout) view.findViewById(R.id.sent);
            holder.timeSent = (RelativeTimeTextView) view.findViewById(R.id.timestampSen);
            holder.timeRec = (RelativeTimeTextView) view.findViewById(R.id.timestampRec);
            holder.statusIcon = (ImageView) view.findViewById(R.id.iv_statsImage);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        // Listen for ListView Item Click
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {


                Intent intent1 = new Intent(mContext, Profile.class);
                intent1.putExtra("authorid", genResultList.get(position).getAuthorid());
                mContext.startActivity(intent1);


            }
        });

        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        genResultList.clear();
        if(arrayList.size()>0) {
            if (charText.length() == 0) {
                genResultList.addAll(arrayList);
            } else {
                for (ProfileClass gsd : arrayList) {
                    if (gsd.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        genResultList.add(gsd);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }


    public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
        // TODO Auto-generated method stub
        int targetWidth = 50;
        int targetHeight = 50;
        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                targetHeight,Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(targetBitmap);
        Path path = new Path();
        path.addCircle(((float) targetWidth - 1) / 2,
                ((float) targetHeight - 1) / 2,
                (Math.min(((float) targetWidth),
                        ((float) targetHeight)) / 2),
                Path.Direction.CCW);

        canvas.clipPath(path);
        Bitmap sourceBitmap = scaleBitmapImage;
        canvas.drawBitmap(sourceBitmap,
                new Rect(0, 0, sourceBitmap.getWidth(),
                        sourceBitmap.getHeight()),
                new Rect(0, 0, targetWidth,
                        targetHeight), null);
        return targetBitmap;
    }

}