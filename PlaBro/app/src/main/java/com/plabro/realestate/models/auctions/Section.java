package com.plabro.realestate.models.auctions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Section {

    @SerializedName("img_url")
    @Expose
    private String img_url;
    @SerializedName("section_type")
    @Expose
    private String section_type;
    @SerializedName("section_title")
    @Expose
    private String section_title;
    @SerializedName("section_data")
    @Expose
    private String section_data;
    @SerializedName("section_footer")
    @Expose
    private String section_footer;
    @SerializedName("section_value")
    @Expose
    private String section_value;
    @SerializedName("section_list_data")
    @Expose
    private List<SectionListData> section_list_data;

    public String getSection_value() {
        return section_value;
    }

    public void setSection_value(String section_value) {
        this.section_value = section_value;
    }

    public List<SectionListData> getSection_list_data() {
        return section_list_data;
    }

    public void setSection_list_data(List<SectionListData> section_list_data) {
        this.section_list_data = section_list_data;
    }

    /**
     * @return The img_url
     */
    public String getImg_url() {
        return img_url;
    }

    /**
     * @param img_url The img_url
     */
    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    /**
     * @return The section_type
     */
    public String getSection_type() {
        return section_type;
    }

    /**
     * @param section_type The section_type
     */
    public void setSection_type(String section_type) {
        this.section_type = section_type;
    }

    /**
     * @return The section_title
     */
    public String getSection_title() {
        return section_title;
    }

    /**
     * @param section_title The section_title
     */
    public void setSection_title(String section_title) {
        this.section_title = section_title;
    }

    /**
     * @return The section_data
     */
    public String getSection_data() {
        return section_data;
    }

    /**
     * @param section_data The section_data
     */
    public void setSection_data(String section_data) {
        this.section_data = section_data;
    }

    /**
     * @return The section_footer
     */
    public String getSection_footer() {
        return section_footer;
    }

    /**
     * @param section_footer The section_footer
     */
    public void setSection_footer(String section_footer) {
        this.section_footer = section_footer;
    }


}