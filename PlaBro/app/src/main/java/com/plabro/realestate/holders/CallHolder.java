package com.plabro.realestate.holders;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.UserProfileNew;
import com.plabro.realestate.models.Calls.CallData;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.widgets.RelativeTimeTextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by nitin on 24/10/15.
 */
public class CallHolder extends BaseFeedHolder {
    private ImageView userImage, callTypeIcon, freeCallIcon;
    private FontText userName, shoutDesc;
    private RelativeTimeTextView callTime;
    private String phone;

    public CallHolder(View itemView) {
        super(itemView);
        userName = (FontText) findTV(R.id.tv_user_name);
        userImage = (ImageView) findView(R.id.iv_user_image);
        callTypeIcon = (ImageView) findView(R.id.iv_call_icon);
        freeCallIcon = (ImageView) findView(R.id.iv_free_call_icon);
        callTime = (RelativeTimeTextView) findView(R.id.tv_call_timing);
        shoutDesc = (FontText) findTV(R.id.tv_shout_desc);
    }

    @Override
    public void onBindViewHolder(final int position, BaseFeed feed) {
        super.onBindViewHolder(position, feed);


        if (feed instanceof CallData) {
            Log.d("Call holder", "feed instance of Call util");
            final CallData call = (CallData) feed;
            String phone = PBPreferences.getPhone();

            Log.d(TAG, call.getIsFreeCall() + "," + call.getDesc() + "," + call.getName() + ", " + call.getCallTime());
            if (call.getIsFreeCall()) {
                freeCallIcon.setVisibility(View.VISIBLE);
            } else {
                freeCallIcon.setVisibility(View.GONE);
            }

            if ("incoming".equalsIgnoreCase(call.getCall_type())) {
                // incoming call
                callTypeIcon.setImageResource(R.drawable.ic_in_call);
                phone = call.getCaller();
            } else if ("missed".equalsIgnoreCase(call.getCall_type())) {
                callTypeIcon.setImageResource(R.drawable.ic_out_call);
                phone = call.getCallee();
            } else {
                callTypeIcon.setImageResource(R.drawable.ic_out_call);
                phone = call.getCallee();
            }
            final String ph = phone;

            callTime.setReferenceTime(call.getCallTime());
            String timeString = getTimeString(call.getCallTime());
            callTime.setSuffix(", " + timeString);
            if (TextUtils.isEmpty(call.getImg())) {
                userImage.setImageResource(R.drawable.profile_default_one);
            } else {
                ActivityUtils.loadImage(ctx, call.getImg(), userImage, R.drawable.profile_default_one);
            }
            userName.setText(call.getName());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("source", "Call Holder Card View");
                    wrData.put("ScreenName", TAG);
                    trackEvent(Analytics.CardActions.CardClicked, position, call.getId()+"",call.getType(), wrData );
                    UserProfileNew.startActivity(ctx, (call.getAuthorid() == 0 ? "" : call.getAuthorid()) + "", ph, "");
                }
            });
            if (TextUtils.isEmpty(call.getDesc())) {
                shoutDesc.setVisibility(View.GONE);
            } else {
                shoutDesc.setVisibility(View.VISIBLE);
                shoutDesc.setText(call.getDesc());
            }
        } else {
            Log.d("Call holder", "feed not an instance of Call util");
        }
    }

    private String getTimeString(long time) {
        Date d = new Date(time);
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        return format.format(d);
    }
}
