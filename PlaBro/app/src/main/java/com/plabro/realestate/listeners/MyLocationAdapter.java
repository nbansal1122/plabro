package com.plabro.realestate.listeners;

/**
 * Created by hemant on 17/1/15.
 */


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.models.location.Locations;

import java.util.List;

public class MyLocationAdapter extends BaseAdapter {
    private final Context context;
    private final  List<Locations> values;

    public MyLocationAdapter(Context context, List<Locations> values) {
        super();
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cards_layout, parent, false);



        View rowView = inflater.inflate(R.layout.location_layout, parent, false);

        TextView textView = (TextView) rowView.findViewById(R.id.locationItem);

        textView.setText(values.get(position).getLocation());


        return view;
    }
}
