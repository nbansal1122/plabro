package com.plabro.realestate.activity.CommunityActivities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.plabro.realestate.R;
import com.plabro.realestate.activity.FragmentContainer;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.fragments.community.CommunityShoutsUsers;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.community.CreateCommunityResponse;
import com.plabro.realestate.models.community.JoinedComm;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import org.json.JSONArray;

import java.util.HashMap;

/**
 * Created by nbansal2211 on 14/06/16.
 */
public class JoinCommunity extends PlabroBaseActivity implements VolleyListener {
    private ImageView communityLogo;
    private EditText nameEt, descEt;
    private JoinedComm community;

    public static void startActivity(Activity ctx, JoinedComm comm) {
        Bundle b = new Bundle();
        b.putSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE, comm);
        Intent i = new Intent(ctx, JoinCommunity.class);
        i.putExtras(b);
        ctx.startActivity(i);
    }

    @Override
    protected String getTag() {
        return "Join Community";
    }

    @Override
    protected void loadBundleData(Bundle b) {
        community = (JoinedComm) b.getSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE);

    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_join_community;
    }

    @Override
    public void findViewById() {
        communityLogo = (ImageView) findViewById(R.id.iv_logo_community);
        nameEt = (EditText) findViewById(R.id.et_communityName);
        descEt = (EditText) findViewById(R.id.et_communityDesc);

        nameEt.setText(community.getGroupInfo().getName());
        descEt.setText(community.getGroupInfo().getDesc());
        ActivityUtils.loadImage(this, community.getGroupInfo().getImage(), communityLogo, R.drawable.ic_launcher);

        setClickListeners(R.id.btn_create_community);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_create_community:
                joinCommunity();
                break;
        }
    }

    private void joinCommunity() {
        ParamObject obj = new ParamObject();
        obj.setClassType(CreateCommunityResponse.class);
        HashMap<String, String> map = new HashMap<>();
        map.put("action", "join");
        map.put("groupid", community.getGroupid() + "");
        obj.setTaskCode(Constants.TASK_CODES.JOIN_COMMUNITY);
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.UPDATE_COMMUNITY, map));

        AppVolley.processRequest(obj, this);
    }

    private void moveToCommunityShouts() {
        FragmentContainer.startActivity(this, Constants.FRAGMENT_TYPE.COMMUNITY_SHOUTS_USERS, getIntent().getExtras());
        finish();
    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        switch (taskCode) {
            case Constants.TASK_CODES.JOIN_COMMUNITY:
                showToast(R.string.successsfully_joined);
                moveToCommunityShouts();
                break;
        }
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        onApiFailure(response, taskCode);
    }
}
