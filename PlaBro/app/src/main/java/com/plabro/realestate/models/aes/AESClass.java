package com.plabro.realestate.models.aes;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class AESClass {

    private String key = "";
    private String signature = "";
    private String message = "";


    public String getKey() {
        return key;
    }

    public String getSignature() {
        return signature;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
