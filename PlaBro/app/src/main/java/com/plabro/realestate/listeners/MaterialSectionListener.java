package com.plabro.realestate.listeners;


import com.plabro.realestate.drawer.MaterialSection;

public interface MaterialSectionListener {

    public void onClick(MaterialSection section);

}
