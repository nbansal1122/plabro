package com.plabro.realestate.models.city;


import android.text.TextUtils;

import com.activeandroid.query.Delete;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.ServerResponse;
import com.plabro.realestate.utilities.JSONUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class CityResponse extends ServerResponse {

    CityOutputParams output_params = new CityOutputParams();

    public static List<CityClass> getCityList() {
        return CityClass.getCitiesList();
    }


    public CityOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(CityOutputParams output_params) {
        this.output_params = output_params;
    }

    public static List<CityClass> parseJson(String json) {
        return parseJson(json, true);
    }

    public static List<CityClass> parseJson(String json, boolean isSaved) {
        Log.d("Parsing City JSON:", json + "");

        List<CityClass> cityList = new ArrayList<CityClass>();

        CityResponse res = JSONUtils.getGSONBuilder().create().fromJson(json, CityResponse.class);
        if (res.isStatus()) {
            CityData data = res.getOutput_params().getData();
            if (data != null) {
                if (isSaved) {
                    new Delete().from(CityClass.class).execute();
                }
                ArrayList<ArrayList<String>> otherCities = data.getOther_cities();
                ArrayList<ArrayList<String>> popularCities = data.getPopular_cities();
                ArrayList<String> defaultCities = data.getDefault_city();

                if (defaultCities != null && defaultCities.size() > 0) {
                    CityClass defCity = new CityClass();
                    defCity.set_id(defaultCities.get(1));
                    defCity.setCity_name(defaultCities.get(0));
                    defCity.setCity_type("default");
                    if (!TextUtils.isEmpty(defCity.getCity_name()) && isSaved) {
                        defCity.save();
                    }
                    cityList.add(defCity);
                }
                if (popularCities != null && popularCities.size() > 0) {
                    for (ArrayList<String> cities : popularCities) {
                        CityClass popCity = new CityClass();
                        popCity.set_id(cities.get(1));
                        popCity.setCity_name(cities.get(0));
                        popCity.setCity_type("popular");
                        if (!TextUtils.isEmpty(popCity.getCity_name()) && isSaved) {
                            popCity.save();
                        }
                        cityList.add(popCity);

                    }
                }
                if (otherCities != null && otherCities.size() > 0) {
                    for (ArrayList<String> oCities : otherCities) {
                        CityClass oCity = new CityClass();
                        oCity.set_id(oCities.get(1));
                        oCity.setCity_name(oCities.get(0));
                        oCity.setCity_type("other");
                        if (!TextUtils.isEmpty(oCity.getCity_name()) && isSaved) {
                            oCity.save();
                        }
                        cityList.add(oCity);
                    }
                }
            }
        }

        return cityList;
    }


}
