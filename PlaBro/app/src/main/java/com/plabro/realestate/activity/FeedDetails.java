package com.plabro.realestate.activity;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.desmond.parallaxviewpager.ParallaxFragmentPagerAdapter;
import com.desmond.parallaxviewpager.ParallaxViewPagerBaseActivity;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomPagerAdapter;
import com.plabro.realestate.entities.BookmarksDBModel;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.fragments.related.RelatedBrokerFragment;
import com.plabro.realestate.fragments.related.RelatedFeedsFragment;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.Calls.CallPopUpInfo;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.profile.ProfileResponse;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.BookmarkResponse;
import com.plabro.realestate.models.propFeeds.Endorsement_data;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.receivers.CallListener;
import com.plabro.realestate.uiHelpers.ViewPagerIndicator;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.BitmapUtil;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.FeedPopUpDialog;
import com.software.shell.fab.ActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FeedDetails extends ParallaxViewPagerBaseActivity implements View.OnClickListener, VolleyListener, ShoutObserver.PBObserver, CallListener.CallInterface {
    private ViewPagerIndicator pageIndicator;
    private List<String> tabs = new ArrayList<String>();
    private CustomPagerAdapter<String> adapter;
    private boolean isExpanded;
    private ProgressDialog dialog;
    private boolean isFabActionClicked;


    private boolean isCurrentUser, isRunning = true;
    private com.plabro.realestate.uiHelpers.slidingTab.SlidingTabLayout mNavigBar;
    private ProfileClass profile;
    private int scrollY;
    private String message_id = "";
    private int hashKey;
    private String refKey;
    private Feeds feedsObject;

    private TextView mSummary;
    private TextView mAuthor, mAvailReq, mSaleRent, mTopHt;
    private Button mViews, mShares, mBookmarks;
    private TextView mPostText;
    private TextView mPostTime;
    private RelativeLayout mMore;
    private boolean isHeaderSet;
    private ImageView expandIcon;
    private ImageView editOrBookmark, back;
    private int icon_bookmark = R.drawable.shout_bookmark_active;
    private int icon_no_bookmark = R.drawable.shout_bookmark_inactive;

    private boolean isAlreadyResumed;

    public static void startFeedDetails(Context ctx, String message_id, String refKey, Feeds feedsObject) {
        Bundle b = new Bundle();
        b.putString("message_id", message_id);
        b.putString("refKey", refKey == null ? "" : refKey);
        b.putSerializable("feedsObject", feedsObject);
        Intent i = new Intent(ctx, FeedDetails.class);
        i.putExtras(b);
        ctx.startActivity(i);
    }

    public static void startFeedDetailsWithoutAnimation(Context ctx, String message_id, String refKey, Feeds feedsObject) {
        Bundle b = new Bundle();
        b.putString("message_id", message_id);
        b.putString("refKey", refKey == null ? "" : refKey);
        b.putSerializable("feedsObject", feedsObject);
        Intent i = new Intent(ctx, FeedDetails.class);
        //i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        i.putExtras(b);
        ctx.startActivity(i);
    }

    private void setTabTextView() {
        int count = mNavigBar.getTabStrip().getChildCount();
        int currentPosition = mViewPager.getCurrentItem();

        //related brokers selected
        if (currentPosition == 1) {
        }
        for (int i = 0; i < count; i++) {
            TextView v = (TextView) mNavigBar.getTabStrip().getChildAt(i);
            if (i == currentPosition)
                v.setTextColor(getResources().getColor(R.color.color_tab_selected_text));
            else
                v.setTextColor(getResources().getColor(R.color.color_tab_text));
        }
    }

    protected String getTAG() {
        return "Feed Details";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_detail);
        Analytics.trackScreen(Analytics.PropFeed.ShoutDetails);
        setHeaderBackground();
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mNavigBar = (com.plabro.realestate.uiHelpers.slidingTab.SlidingTabLayout) findViewById(R.id.navig_tab);
        mHeader = findViewById(R.id.header);

        Bundle bundle = getIntent().getExtras();

        message_id = bundle.getString("message_id");
        refKey = bundle.get("refKey") + "";
        feedsObject = (Feeds) getIntent().getSerializableExtra("feedsObject");

        Endorsement_data endorsement_data = feedsObject.getEndorsement_data();
        Log.d(TAG, "Endorsement Data :" + endorsement_data);

        if (feedsObject != null) {
            findViewById();
        }
        if (savedInstanceState != null) {
            mHeader.setTranslationY(savedInstanceState.getFloat(HEADER_TRANSLATION_Y));
        }
        setClickListeners(R.id.iv_back);

    }

    private void setHeaderBackground() {
        try {
            RelativeLayout layout = (RelativeLayout) findViewById(R.id.rl_feed_header);
            int drawableId = R.drawable.header_detail;
            Bitmap bMap = BitmapUtil.getBackgroundBitmap(this, R.dimen.ht_feed_header, drawableId);
            if(Build.VERSION.SDK_INT>=16) {
                layout.setBackground(new BitmapDrawable(getResources(), bMap));
            }else{
                layout.setBackgroundDrawable(new BitmapDrawable(getResources(), bMap));
            }
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getShoutDetails();
    }

    private void getShoutDetails() {
        ParamObject obj = new ParamObject();
        obj.setClassType(FeedResponse.class);
        obj.setTaskCode(Constants.TASK_CODES.SHOUT_DETAILS);
        HashMap<String, String> params = new HashMap<>();
        params.put("message_id", message_id);
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.SHOUT_DETAILS, params));
        AppVolley.processRequest(obj, this);
    }

    public View getHeader() {
        return mHeader;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "" + CallPopUpInfo.isCallClicked);
        if (null != CallPopUpInfo.feeds && CallPopUpInfo.isCallClicked) {
            CallPopUpInfo.isCallClicked = false;
            sendCallLogsAfterOneSecond();
        }
    }

    public void sendCallLogsAfterOneSecond() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (ActivityUtils.isCallConnected(FeedDetails.this, CallPopUpInfo.feeds, getTag()) && isRunning) {
                    PlabroBaseActivity.showCallDialog(FeedDetails.this, CallPopUpInfo.feeds, "CALL ENDED WITH", false);
                }
            }
        }, Constants.CALL_LOG_TIME_GAP);
    }

    @Override
    protected void initValues() {
        int tabHeight = getResources().getDimensionPixelSize(R.dimen.tab_height) + getResources().getDimensionPixelSize(R.dimen.view_height);
        mMinHeaderHeight = mHeader.getHeight();
        mHeaderHeight = mMinHeaderHeight;
        mMinHeaderTranslation = -mMinHeaderHeight + tabHeight;
        getHeaderHeight();
        Log.d(TAG, mHeader.getMeasuredHeight() + ", " + mHeader.getMeasuredHeightAndState());
        Log.d(TAG, "TabHeight:" + tabHeight + ", MinHeaderHeight:" + mMinHeaderHeight + ", mHeaderHeight:" + mHeaderHeight + ", MinHeaderTranslation:" + mMinHeaderTranslation);
        mNumFragments = 2;
    }

    private void getHeaderHeight() {
        Log.d(TAG, "HeaderHeight:" + mHeader.getHeight() + ", MeasuredHeight:" + mHeader.getMeasuredHeight() + ", LayoutParamsHeight:" + mHeader.getLayoutParams().height);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Log.d(TAG, "Window Focus Changed Header Height :" + mHeader.getHeight());
        if (!isHeaderSet) {
            setupAdapter();
            isHeaderSet = true;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //outState.putFloat(IMAGE_TRANSLATION_Y, mTopImage.getTranslationY());
        outState.putFloat(HEADER_TRANSLATION_Y, mHeader.getTranslationY());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void setupAdapter() {
        Log.d(TAG, "Setting Adapter");
        initValues();
        mAdapter = new ViewPagerAdapter(getSupportFragmentManager(), mNumFragments);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(mNumFragments);
        mNavigBar.setOnPageChangeListener(getViewPagerChangeListener());
        mNavigBar.setViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setTabTextView();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setTabTextView();
    }

    @Override
    protected void scrollHeader(int scrollY) {
        this.scrollY = scrollY;
        float translationY = Math.max(-scrollY, mMinHeaderTranslation);
        if (translationY <= 0.0)
            mHeader.setTranslationY(translationY);
        //mTopImage.setTranslationY(-translationY / 3);
    }

    private boolean isShoutUpodated;

    @Override
    public void onShoutUpdate() {
        isShoutUpodated = true;
        getShoutDetails();
    }

    @Override
    public void onShoutUpdateGen(String type) {

    }


    private class ViewPagerAdapter extends ParallaxFragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm, int numFragments) {
            super(fm, numFragments);
        }

        @Override
        public Fragment getItem(int position) {
            Log.d(TAG, "position" + position);
            Fragment fragment;
            switch (position) {
                case 0:
                    fragment = RelatedFeedsFragment.getInstance(FeedDetails.this, message_id, mHeader.getHeight());
                    break;

                case 1:
                    fragment = RelatedBrokerFragment.getInstance(FeedDetails.this, message_id, mHeader.getHeight());
                    break;

                default:
                    throw new IllegalArgumentException("Wrong page given " + position);
            }
            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Related Shouts";

                case 1:
                    return "Related Users";


                default:
                    throw new IllegalArgumentException("wrong position for the fragment in vehicle page");
            }
        }
    }


    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
//        hideProgressDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.USER_PROFILE:
                ProfileResponse res = (ProfileResponse) response.getResponse();
                if (res.isStatus() && null != res.getOutput_params()) {
                    profile = res.getOutput_params().getData();
                    if (null != profile) {
                        setViews(profile);
                    }
                } else {
                    // Status is false
//                    Toast.makeText(UserProfileNew.this, res.getMsg() + "", Toast.LENGTH_LONG).show();
                }
                break;
            case Constants.TASK_CODES.SHOUT_DETAILS:
                FeedResponse resp = (FeedResponse) response.getResponse();
                if (null != resp.getOutput_params() && null != resp.getOutput_params().getData()) {
                    List<BaseFeed> feeds = resp.getOutput_params().getData();
                    if (feeds.size() > 0) {
                        Endorsement_data eData = feedsObject.getEndorsement_data();
                        feedsObject = (Feeds) feeds.get(0);
                        if (null != feedsObject && feedsObject.getEndorsement_data() == null) {
                            feedsObject.setEndorsement_data(eData);
                        }
                        isHeaderSet = true;
                        if (isShoutUpodated) {
                            FeedDetails.startFeedDetails(FeedDetails.this, message_id, refKey, feedsObject);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();
                        } else {
                            setCounts(feedsObject);
                        }

                    }
                }
                break;

        }

    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
//        hideProgressDialog();
        switch (response.getCustomException().getExceptionType()) {
            case VolleyListener.SESSION_EXPIRED:
                onPlabroSessionExpired();
                break;
        }
        switch (taskCode) {
            case Constants.TASK_CODES.USER_PROFILE:
                break;
        }
    }

    public void onPlabroSessionExpired() {
        Utility.userLogin(this);
    }

    private void setViews(ProfileClass profile) {
        setupAdapter();
    }

    public void setText(int textViewId, String text) {
        TextView textView = (TextView) findViewById(textViewId);
        textView.setText(text);
    }

    protected void setClickListeners(int... viewIds) {
        for (int i : viewIds) {
            findViewById(i).setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_expand:
                int key = 0;
                try {
                    key = Integer.parseInt(refKey);
                    Analytics.trackInteractionEvent(Analytics.CardActions.Expanded, new HashMap<String, Object>());
                    DialogFragment feedPopUpDialog = new FeedPopUpDialog(feedsObject, FeedDetails.this, FeedDetails.this, key);
                    feedPopUpDialog.show(getFragmentManager(), "feed");
                } catch (Exception e) {

                }

                break;
            case R.id.iv_edit_or_bookmark:
                if (isCurrentUser) {
                    // Means editing of shout
                    onEditShoutClicked();
                } else {
                    // Means bookmarking a shout
                    onBookmarkClicked(feedsObject.isBookmarked());
                }

                break;
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.tv_user_name:
            case R.id.tv_feed_type:
                if (feedsObject != null)
                    UserProfileNew.startActivity(this, feedsObject.getAuthorid() + "", feedsObject.getProfile_phone(), "");
                break;
        }
    }

    private void onEditShoutClicked() {

        //WizRocket tracking
        HashMap<String, Object> wrdata1 = new HashMap<>();
        wrdata1.put("ScreenName", TAG);
        wrdata1.put("Action", "EditShout");
        wrdata1.put("Position", "0");

        Intent intent = new Intent(this, Shout.class);
        intent.putExtra("feedObject", feedsObject);
        intent.putExtra("shoutType", "edit");
        intent.putExtra("postId", feedsObject.get_id());
        startActivity(intent);
        overridePendingTransition(R.anim.bottom_in_fast, R.anim.bottom_out_fast);
    }

    private void onBookmarkClicked(boolean isBookmarked) {

        if (isBookmarked) {
            editOrBookmark.setImageResource(icon_no_bookmark);
        } else {
            editOrBookmark.setImageResource(icon_bookmark);
        }
        feedsObject.setBookmarked(!isBookmarked);
        setBookmark(feedsObject, editOrBookmark, !isBookmarked);
    }

    void setBookmark(final Feeds bookmarkFeed, final ImageView iv_bookmark, final Boolean isSetBookmark) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(PlaBroApi.PLABRO_FEEDS_POST_PARAM_BOOKMARK_MESSAGE_ID, bookmarkFeed.get_id());
        if (isSetBookmark) {
            params = Utility.getInitialParams(PlaBroApi.RT.SETBOOKMARK, params);
        } else {
            params = Utility.getInitialParams(PlaBroApi.RT.DELETEBOOKMARK, params);
        }

        AppVolley.processRequest(Constants.TASK_CODES.SET_BOOKMARK, BookmarkResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                if (!isRunning) return;
                BookmarkResponse bookmarkResponse = (BookmarkResponse) response.getResponse();
                ShoutObserver.dispatchEvent();
                if (isSetBookmark) {

                    Util.showSnackBarMessage(FeedDetails.this, FeedDetails.this, "Bookmarked Successfully");
                    feedsObject.setBookmark_count(feedsObject.getBookmark_count() + 1);
                } else {
                    Util.showSnackBarMessage(FeedDetails.this, FeedDetails.this, "Bookmark Removed");
                    new Delete().from(BookmarksDBModel.class).where("_id='" + feedsObject.get_id() + "'").execute();
                    feedsObject.setBookmark_count(feedsObject.getBookmark_count() - 1);
                }
                setCounts(feedsObject);

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                if (!isRunning) return;
                if (isSetBookmark) {
                    Toast.makeText(FeedDetails.this, "Cannot Bookmark, Please check your network or try again", Toast.LENGTH_LONG).show();
                    iv_bookmark.setImageResource(icon_no_bookmark);
                    bookmarkFeed.setBookmarked(false);
                    iv_bookmark.setTag(bookmarkFeed);
                } else {
                    Toast.makeText(FeedDetails.this, "Cannot Delete Bookmark, Please check your network or try again", Toast.LENGTH_LONG).show();
                    iv_bookmark.setImageResource(icon_bookmark);
                    bookmarkFeed.setBookmarked(true);
                    iv_bookmark.setTag(bookmarkFeed);
                }
            }
        });
    }

    private void showProgressDialog() {
        if (null != dialog && dialog.isShowing()) {
        } else {
            dialog = new ProgressDialog(FeedDetails.this);
            dialog.setMessage("Loading...");
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    private void hideProgressDialog() {
        if (null != dialog && dialog.isShowing()) {
            dialog.dismiss();
        }
    }


    public void findViewById() {
        Log.d(TAG, "findView By ID");
        tabs.add("Related Shouts");
        tabs.add("Related Brokers");
        mSummary = (TextView) findViewById(R.id.tv_feed_type);
        mAuthor = (TextView) findViewById(R.id.tv_user_name);
        mViews = (Button) findViewById(R.id.btn_views);
        mShares = (Button) findViewById(R.id.btn_shares);
        mBookmarks = (Button) findViewById(R.id.btn_bookmarks);
        mPostText = (TextView) findViewById(R.id.post_content);
        mPostTime = (TextView) findViewById(R.id.post_time);
        mMore = (RelativeLayout) findViewById(R.id.rl_down);
        mAvailReq = (TextView) findViewById(R.id.tv_req_avail);
        mSaleRent = (TextView) findViewById(R.id.tv_sale_rent);
        mTopHt = (TextView) findViewById(R.id.tv_top_hash);
        expandIcon = (ImageView) findViewById(R.id.iv_expand);
        editOrBookmark = (ImageView) findViewById(R.id.iv_edit_or_bookmark);
        back = (ImageView) findViewById(R.id.iv_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backpressed();
            }
        });
        setClickListeners(R.id.tv_feed_type, R.id.tv_user_name);

        if (feedsObject == null) return;
        editOrBookmark.setImageResource((feedsObject.isBookmarked() == null || feedsObject.isBookmarked() == false) ? icon_no_bookmark : icon_bookmark);
        setClickListeners(R.id.iv_expand, R.id.iv_edit_or_bookmark);


        if (feedsObject.getAvail_req().length > 0 && !TextUtils.isEmpty(feedsObject.getAvail_req()[0])) {

            mAvailReq.setText(feedsObject.getAvail_req()[0]);
            mAvailReq.setVisibility(View.VISIBLE);

        } else {
            mAvailReq.setVisibility(View.GONE);
        }

        if (feedsObject.getSale_rent().length > 0 && !TextUtils.isEmpty(feedsObject.getSale_rent()[0])) {
            mSaleRent.setText(feedsObject.getSale_rent()[0]);
            mSaleRent.setVisibility(View.VISIBLE);
        } else {
            mSaleRent.setVisibility(View.GONE);

        }

        if (!TextUtils.isEmpty(feedsObject.getTop_ht())) {
            mTopHt.setText(feedsObject.getTop_ht());
            mTopHt.setVisibility(View.VISIBLE);

        } else {
            mTopHt.setVisibility(View.GONE);
        }


//        getProfile();
        setFeedDetails();
        if (null != profile) {
            setViews(profile);
        }


    }

    private void setCounts(Feeds feedsObject) {
        String suffix = (feedsObject.getView_count() + 1) > 0 ? "VIEWS" : "VIEW";
        mViews.setText((feedsObject.getView_count() + 1) + " " + suffix);
        suffix = feedsObject.getShares_count() > 0 ? "SHARES" : "SHARE";
        mShares.setText(feedsObject.getShares_count() + " " + suffix);
        suffix = feedsObject.getBookmark_count() > 0 ? "BOOKMARKS" : "BOOKMARK";
        mBookmarks.setText(feedsObject.getBookmark_count() + " " + suffix);
    }

    private void setFeedDetails() {

        mSummary.setText(Util.toDisplayCase(feedsObject.getBusiness_name()));
        mAuthor.setText(Util.toDisplayCase(feedsObject.getProfile_name()));

        setCounts(feedsObject);

        setFeedText();

        String time = feedsObject.getPost_time();
        if (TextUtils.isEmpty(time)) {
            time = feedsObject.getTime();
        }
        mPostTime.setText(time);

        ProfileClass profileClass = AppController.getInstance().getUserProfile();
        if (null != profileClass) {
            String authId = profileClass.getAuthorid();

            String feedAuthId = feedsObject.getAuthorid() + "";
            if (!TextUtils.isEmpty(authId) && authId.equalsIgnoreCase(feedAuthId)) {
                isCurrentUser = true;
                editOrBookmark.setImageResource(R.drawable.edit_shout);
            } else {
                isCurrentUser = false;
                editOrBookmark.setImageResource(feedsObject.isBookmarked() ? icon_bookmark : icon_no_bookmark);
            }
        }
        getHeaderHeight();
        setFabButtons();
        if (isCurrentUser) {
            ShoutObserver.getInstance().addListener(FeedDetails.this);
        }

    }

    private void setFeedText() {
        String text = feedsObject.getText();

        int start = 0;
        int end = 250;
        if (feedsObject.getSnippet().size() > 0) {
            start = Integer.parseInt(feedsObject.getSnippet().get(0));
            end = Integer.parseInt(feedsObject.getSnippet().get(1)) + 1;
        }

        int occurance = text.length() - text.replace("\n", "").length();

        if (text.length() <= end) { //&& occurance<4
            expandIcon.setVisibility(View.GONE);
        } else {

            if (start >= 0 && end < text.length()) {
                text = text.substring(start, end);
            }
            expandIcon.setVisibility(View.VISIBLE);
        }

        //adding span for hashtags in post
        if (feedsObject.getHashTagText() == null) {
            int hashingSize = feedsObject.getHashingListIndices().size();
            if (hashingSize != 0) {
                int refk = 1;
                try {
                    refk = Integer.parseInt(refKey);
                } catch (Exception e) {

                }
                SpannableString hashTagText = ActivityUtils.setHashTags(feedsObject, text, start, end, FeedDetails.this, mPostText, "FeedDetails", -1, false, refk, FeedDetails.this);//type and position send to track analytics
                feedsObject.setHashTagText(hashTagText);
            } else {
                mPostText.setText(text);
            }

        } else {

            int hashingSize = feedsObject.getHashingListIndices().size();
            if (hashingSize != 0) {
                feedsObject.setHashTagText(feedsObject.getHashTagText());
            } else {
                mPostText.setText(text);
            }
        }
    }

    private void setFabButtons() {
        final ActionButton fabAction = (ActionButton) findViewById(R.id.fab_action);
        final ActionButton callAction = (ActionButton) findViewById(R.id.fab_call);
        final ActionButton chatAction = (ActionButton) findViewById(R.id.fab_chat);
        final ActionButton copyAction = (ActionButton) findViewById(R.id.fab_copy);
        final ActionButton shareAction = (ActionButton) findViewById(R.id.fab_share);

        fabAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFabActionClicked) {
                    findViewById(R.id.rl_fab_shout_detail).setBackgroundColor(getResources().getColor(R.color.alpha_44_white));
                    fabAction.setButtonColor(getResources().getColor(R.color.fab_material_red_900));
                    fabAction.setImageResource(R.drawable.ic_cross);
                    callAction.playShowAnimation();
                    chatAction.playShowAnimation();
                    copyAction.playShowAnimation();
                    shareAction.playShowAnimation();
                    ActivityUtils.showViews(callAction, chatAction, copyAction, shareAction);
                    ActivityUtils.showViews(FeedDetails.this, R.id.tv_floating_call, R.id.tv_floating_chat, R.id.tv_floating_copy, R.id.tv_floating_share);
                    setText(R.id.tv_floating_chat, "Chat with " + (TextUtils.isEmpty(feedsObject.getProfile_name()) ? "broker" : feedsObject.getProfile_name()));
                    setText(R.id.tv_floating_call, "Call " + (TextUtils.isEmpty(feedsObject.getProfile_name()) ? "this broker" : feedsObject.getProfile_name()));
                } else {
                    findViewById(R.id.rl_fab_shout_detail).setBackgroundColor(0);
                    fabAction.setButtonColor(getResources().getColor(R.color.colorPrimary));
                    fabAction.setImageResource(R.drawable.fab_plus_icon);
                    callAction.playHideAnimation();
                    chatAction.playHideAnimation();
                    copyAction.playHideAnimation();
                    shareAction.playHideAnimation();
                    ActivityUtils.hideViews(callAction, chatAction, copyAction, shareAction);
                    ActivityUtils.hideViews(FeedDetails.this, R.id.tv_floating_call, R.id.tv_floating_chat, R.id.tv_floating_copy, R.id.tv_floating_share);
                }

                isFabActionClicked = !isFabActionClicked;

            }
        });

        setFabButtonListener(R.id.fab_call, R.id.fab_chat, R.id.fab_share, R.id.fab_copy, R.id.tv_floating_call, R.id.tv_floating_chat, R.id.tv_floating_copy, R.id.tv_floating_share);
        int counter = PBPreferences.getFeedDetailsFabCounter();
        if (counter < 2) {
            fabAction.performClick();
            counter++;
            PBPreferences.setFeedDetailsFabCounter(counter);
        }

    }

    private void setFabButtonListener(int... ids) {
        for (int i : ids) {
            findViewById(i).setOnClickListener(fabButtonListener);
        }
    }

    private View.OnClickListener fabButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            closeActionButtons();
            switch (v.getId()) {
                case R.id.fab_call:
                case R.id.tv_floating_call:
                    if (isCurrentUser) return;
                    HashMap<String, Object> wrData = ActivityUtils.getCallStats(feedsObject, TAG, TAG, 0);
                    ActivityUtils.initiateCallDialog(FeedDetails.this, feedsObject, wrData);
                    break;
                case R.id.fab_chat:
                case R.id.tv_floating_chat:
                    if (isCurrentUser) return;
                    trackEvent(Analytics.CardActions.Chat, 0, feedsObject.get_id(), feedsObject.getType(), null);
                    ActivityUtils.onChatClicked(feedsObject, FeedDetails.this, TAG, 0);
                    break;
                case R.id.fab_copy:
                case R.id.tv_floating_copy:
                    trackEvent(Analytics.CardActions.Copied, 0, feedsObject.get_id(), feedsObject.getType(), null);
                    ActivityUtils.copyFeed(feedsObject, FeedDetails.this);
                    break;
                case R.id.fab_share:
                case R.id.tv_floating_share:
                    trackEvent(Analytics.CardActions.Shared, 0, feedsObject.get_id(), feedsObject.getType(), null);
                    PBSharingManager.shareByAll(feedsObject, FeedDetails.this);
                    feedsObject.setShares_count(feedsObject.getShares_count() + 1);
                    setCounts(feedsObject);
                    break;
            }
        }
    };

    protected void trackEvent(String eventName, int position, String id, String type, HashMap<String, Object> otherdata) {
        if (otherdata == null) {
            otherdata = new HashMap<>();
        }
        otherdata.put(Analytics.Params.SHOUT_ID, id);
        otherdata.put(Analytics.Params.CARD_POSITION, position);
        otherdata.put(Analytics.Params.FEED_TYPE, type);
        Analytics.trackInteractionEvent(eventName, otherdata);
    }


    private void closeActionButtons() {
        ((ActionButton) findViewById(R.id.fab_action)).performClick();
    }

    @Override
    protected String getTag() {
        return "Feed Details";
    }

    @Override
    public void onBackPressed() {
        backpressed();
    }

    private void backpressed() {
        if (isFabActionClicked) {
            closeActionButtons();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShoutObserver.getInstance().remove(this);
        isRunning = false;
    }

    private CallListener phoneListener;

    @Override
    public void startListeningToCall(BaseFeed feed, boolean isFreeCall) {
        if (!isFreeCall) {
            phoneListener = new CallListener(feed, this, isFreeCall);
            TelephonyManager telephony = (TelephonyManager)
                    getSystemService(Context.TELEPHONY_SERVICE);
            telephony.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
            CallPopUpInfo.isCallClicked = false;
        } else {
            ActivityUtils.saveFreeCallLogsToUtil(this, feed, TAG);
        }
    }

    public void stopCallListener() {
        TelephonyManager telephony = (TelephonyManager)
                getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(phoneListener, PhoneStateListener.LISTEN_NONE);
    }

    @Override
    public void onCallConnected(BaseFeed feed) {
        Log.d(TAG, "Call Connected:");
    }

    @Override
    public void onCallEnded(BaseFeed feed, String incomingNumber) {
        Log.d(TAG, "Call Ended:");
        stopCallListener();
        CallPopUpInfo.isCallClicked = true;
        if (feed instanceof Feeds) {
            CallPopUpInfo.feeds = (Feeds) feed;
            CallPopUpInfo.feeds.setIncomingNumber(incomingNumber);
        } else if (feed instanceof ProfileClass) {
            CallPopUpInfo.feeds = ActivityUtils.createFeedObjectFromProfile((ProfileClass) feed);
            CallPopUpInfo.feeds.setIncomingNumber(incomingNumber);
        }

    }

    @Override
    public void onCallReceived(BaseFeed feed, boolean isFreeCall) {
        Log.d(TAG, "Call Received:");
        Log.d(TAG, "Free Call Received:" + isFreeCall);

    }
}
