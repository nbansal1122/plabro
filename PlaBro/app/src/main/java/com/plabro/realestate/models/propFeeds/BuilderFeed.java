package com.plabro.realestate.models.propFeeds;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.JSONUtils;

/**
 * Created by nitin on 11/08/15.
 */
@Table(name = "BuilderFeeds")
public class BuilderFeed extends BaseFeed {

    @Expose
    private String username;
    @Expose
    private String displayname;
    @Expose
    private String display_img;
    @Expose
    private Integer city_id;
    @Expose
    private String text;
    @Expose
    private String time;
    @Expose
    private Integer builder_id;
    @Expose
    private String summ;
    @Expose
    private String profile_phone;
    @Expose
    private String builder_phone;

    public String getBuilder_phone() {
        return builder_phone;
    }

    public void setBuilder_phone(String builder_phone) {
        this.builder_phone = builder_phone;
    }

    @Column
    @Expose
    private Integer _id;

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setIsExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }

    private boolean isExpanded;

    @Expose
    private String sub_type;

    public String getLogo_img() {
        return logo_img;
    }

    public void setLogo_img(String logo_img) {
        this.logo_img = logo_img;
    }

    public String getSub_type() {
        return sub_type;
    }

    public void setSub_type(String sub_type) {
        this.sub_type = sub_type;
    }

    @Expose
    private String logo_img;

    /**
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return The displayname
     */
    public String getDisplayname() {
        return displayname;
    }

    /**
     * @param displayname The displayname
     */
    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    /**
     * @return The display_img
     */
    public String getDisplay_img() {
        return display_img;
    }

    /**
     * @param display_img The display_img
     */
    public void setDisplay_img(String display_img) {
        this.display_img = display_img;
    }

    /**
     * @return The city_id
     */
    public Integer getCity_id() {
        return city_id;
    }

    /**
     * @param city_id The city_id
     */
    public void setCity_id(Integer city_id) {
        this.city_id = city_id;
    }

    /**
     * @return The text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return The time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time The time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return The builder_id
     */
    public Integer getBuilder_id() {
        return builder_id;
    }

    /**
     * @param builder_id The builder_id
     */
    public void setBuilder_id(Integer builder_id) {
        this.builder_id = builder_id;
    }

    /**
     * @return The summ
     */
    public String getSumm() {
        return summ;
    }

    /**
     * @param summ The summ
     */
    public void setSumm(String summ) {
        this.summ = summ;
    }

    /**
     * @return The profile_phone
     */
    public String getProfile_phone() {
        return profile_phone;
    }

    /**
     * @param profile_phone The profile_phone
     */
    public void setProfile_phone(String profile_phone) {
        this.profile_phone = profile_phone;
    }

    /**
     * @return The _id
     */
    public Integer get_id() {
        return _id;
    }

    /**
     * @param _id The _id
     */
    public void set_id(Integer _id) {
        this._id = _id;
    }

    public static BuilderFeed parseJson(String json) {
        Log.d("BuilderJSON", "Builder JSON::" + json);
        BuilderFeed f = JSONUtils.getGSONBuilder().create().fromJson(json, BuilderFeed.class);
        f.setType(Constants.FEED_TYPE.BUILDER_SHOUT);
        return f;
    }
}
