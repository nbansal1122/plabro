package com.plabro.realestate.models.community;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.models.ServerResponse;

/**
 * Created by nbansal2211 on 18/05/16.
 */
public class CreateCommunityResponse extends ServerResponse {

    @SerializedName("output_params")
    @Expose
    private Output_params output_params;

    public Output_params getOutput_params() {
        return output_params;
    }

    public void setOutput_params(Output_params output_params) {
        this.output_params = output_params;
    }

    public  static class Output_params{
        @SerializedName("groupid")
        @Expose
        private String groupid;

        public String getGroup_id() {
            return groupid;
        }

        public void setGroup_id(String group_id) {
            this.groupid = group_id;
        }
    }
}
