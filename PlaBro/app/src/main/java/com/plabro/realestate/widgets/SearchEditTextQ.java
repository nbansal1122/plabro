package com.plabro.realestate.widgets;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.activeandroid.query.Select;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.SearchHistorySuggestionAdapter;
import com.plabro.realestate.models.history.History;
import com.plabro.realestate.utilities.Util;

import java.util.ArrayList;
import java.util.List;


public class SearchEditTextQ extends RelativeLayout {

    AutoCompleteTextView searchText;
    ImageButton btnClearText, btnVoice, btnSearchNow;
    SearchHistorySuggestionAdapter adapter;
    List<History> histories = new ArrayList<History>();
    List<History> historiesFiltered = new ArrayList<History>();


    public SearchEditTextQ(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public SearchEditTextQ(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SearchEditTextQ(Context context) {
        super(context);
        init();
    }

    public void requestTextFocus() {
        searchText.requestFocus();
    }

    public void setThreshold(int thr) {
        searchText.setThreshold(thr);
    }

    public void setFocusable(boolean val) {
        searchText.setFocusable(val);

    }

    public void setFocusableInTouchMode(boolean val) {
        searchText.setFocusableInTouchMode(val);

    }

    public View getVoiceButton() {
        return btnVoice;
    }

    private void init() {

        LayoutInflater.from(getContext()).inflate(R.layout.view_search_edit_text, this);

        searchText = (AutoCompleteTextView) findViewById(R.id.et_search);
        //setThreshold(3);

        btnClearText = (ImageButton) findViewById(R.id.btn_search_text_cancel);
        btnVoice = (ImageButton) findViewById(R.id.btn_search_text_voice);
        btnClearText.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                searchText.setText("");

                if (onSearchActionListener != null) {
                    onSearchActionListener.onSearchClearText();
                }


            }
        });

        btnVoice.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (onSearchActionListener != null)
                    onSearchActionListener.onSearchViaVoice();

            }
        });

        searchText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if(b)
                {
                    fetchInitialHistory();
                }
            }
        });
        searchText.addTextChangedListener(watcher);

        searchText.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {

                    if (onSearchActionListener != null) {
                        //add to history
                        String searchText = v.getText().toString();
                        insertHistoryRecord(searchText);
                        onSearchActionListener.onSearchCalled(v.getText().toString());
                    }

                    searchText.dismissDropDown();
                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    return true;
                }

                return false;
            }
        });


    }

    TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            historiesFiltered.clear();
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
            if (s.toString().length() > 1) {

                fetchHistory(s.toString());
            }


        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {

            if (TextUtils.isEmpty(s.toString())) {
                btnClearText.setVisibility(View.INVISIBLE);
                //btnSearchNow.setVisibility(View.INVISIBLE);
                btnVoice.setVisibility(View.VISIBLE);

            } else {

                if (onSearchActionListener != null)
                    onSearchActionListener.onSearchTypo();
                btnClearText.setVisibility(View.VISIBLE);
                //btnSearchNow.setVisibility(View.VISIBLE);
                btnVoice.setVisibility(View.GONE);
            }


            //  String text = searchText.getText().toString().toLowerCase(Locale.getDefault());
            // adapter.filter(text);
            // searchText.setSelection(searchText.getText().toString().length());


        }
    };


    OnSearchActionListener onSearchActionListener;

    public void setOnCancelSearch(OnSearchActionListener onCancelSearch) {

        this.onSearchActionListener = onCancelSearch;

    }


    public interface OnSearchActionListener {
        public void onSearchCalled(String searchString);

        public void onSearchClearText();

        public void onSearchViaVoice();

        public void onSearchTypo();

    }


    public String getSearchText() {
        return searchText.getText().toString().trim();
    }

    public void setSearchText(String text) {

        if(null!=text && !TextUtils.isEmpty(text)) {
            Util.w("Search : setSearchText From SearchEditText");

            searchText.setText(text == null ? "" : text);
            searchText.setSelection(text.length());
            insertHistoryRecord(text);

        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        searchText.setEnabled(enabled);
        btnClearText.setEnabled(enabled);
    }

    void insertHistoryRecord(String text) {

        String filteredText = removeFilterTags(text);

        text = filteredText;

        boolean is_added = false;
        for (int i = 0; i < histories.size(); i++) {
            if (text.equalsIgnoreCase(histories.get(i).getText())) {
                is_added = true;
                break;
            }
        }

        if (!is_added) {
            if (text.length() > 0 && !text.equalsIgnoreCase("")) {
                History history = new History();
                history.setText(text);
                history.save();
            }
        }

    }

    private String removeFilterTags(String text) {

        String filteredText = text;
        int index = text.lastIndexOf(" > ");
        if (index != -1) {

            filteredText = text.substring(index, text.length());
        }
        return filteredText;


    }

    void fetchHistory(String searchTextString) {
        searchTextString = searchTextString.replace("'", "");
        Log.d("Search EditText", "Fetching History");
        searchTextString = searchTextString.trim();
        if (!TextUtils.isEmpty(searchTextString)) {
            histories = new Select().from(History.class).where("text LIKE '%" + searchTextString + "%'").limit(3).execute();

        } else {
            histories = new Select().from(History.class).groupBy("text").execute();

        }

        historiesFiltered = new ArrayList<History>();

        if (searchTextString.equalsIgnoreCase("")) {
            int x = 4;

            for (int i = histories.size() - 1; i >= 0; i--) {
                if (histories.get(i).getText().toLowerCase().contains(searchTextString.toLowerCase())) {


                    if (histories.get(i).getText().toLowerCase().equalsIgnoreCase("")) {
                        x++;
                    } else {
                        historiesFiltered.add(histories.get(i));
                    }

                    if (i == histories.size() - x) {
                        break;
                    }

                }
            }

        } else {

            for (int i = 0; i < histories.size(); i++) {
                if (histories.get(i).getText().toLowerCase().contains(searchTextString.toLowerCase())) {
                    historiesFiltered.add(histories.get(i));

                }
            }
        }

        adapter = null;
        adapter = new SearchHistorySuggestionAdapter(getContext(), historiesFiltered);
        searchText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                String suggestion = (String) ((TextView) view.findViewById(R.id.tv_auto_suggest)).getText();
                searchText.setText(suggestion);
                searchText.setSelection(suggestion.length());
                onSearchActionListener.onSearchCalled(suggestion);


                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);


            }
        });
        //searchText.setDropDownHeight(500);
        searchText.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        searchText.setThreshold(1);

    }

    public void fetchInitialHistory() {
        Log.v("SearchEditText", "prefilling history");

        historiesFiltered = new Select().from(History.class).orderBy("text DESC LIMIT 3").execute();

        adapter = null;
        adapter = new SearchHistorySuggestionAdapter(getContext(), historiesFiltered);
        searchText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                String suggestion = (String) ((TextView) view.findViewById(R.id.tv_auto_suggest)).getText();
                searchText.setText(suggestion);
                searchText.setSelection(suggestion.length());
                onSearchActionListener.onSearchCalled(suggestion);

                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);


            }
        });
        //searchText.setDropDownHeight(500);
        searchText.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        searchText.setThreshold(1);

    }


}
