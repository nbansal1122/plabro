package com.plabro.realestate;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.plabro.realestate.activity.PBWeb;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.Util;

public class Featured extends PlabroBaseActivity {
    private Toolbar mToolbar;
    RelativeLayout smsDash, smsReq, smsLock;

    @Override
    protected String getTag() {
        return "Featured";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_featured);
        inflateToolbar();
        findViewById();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_featured, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void findViewById() {

        smsDash = (RelativeLayout) findViewById(R.id.rl_sms_dash);
        smsReq = (RelativeLayout) findViewById(R.id.rl_sms_req);
        smsLock = (RelativeLayout) findViewById(R.id.rl_lock);

        smsDash.setOnClickListener(customClickListener);
        smsReq.setOnClickListener(customClickListener);
        smsLock.setOnClickListener(customClickListener);

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void inflateToolbar() {
        //setting action bar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);


        // Inflate a menu to be displayed in the toolbar
        mToolbar.inflateMenu(R.menu.menu_image_cropper);

        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });

    }

    View.OnClickListener customClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.rl_sms_dash:
                    openWeb(Featured.this, Constants.FEATURED_URL.SMS_DASH, Constants.FEATURED_URL_TITLE.SMS_DASH);
                    break;
                case R.id.rl_sms_req:
                    openWeb(Featured.this, Constants.FEATURED_URL.SMS_REQ, Constants.FEATURED_URL_TITLE.SMS_REQ);
                    break;
                case R.id.rl_lock:
                    Util.showSnackBarMessage(Featured.this, Featured.this, "Feature coming soon.");
                    break;
            }
        }
    };

    public static void openWeb(Context ctx, String url, String title) {
        Intent intentFeatured = new Intent(ctx, PBWeb.class);
        intentFeatured.putExtra("url", url);
        intentFeatured.putExtra("title", title);
        ctx.startActivity(intentFeatured);
    }
}
