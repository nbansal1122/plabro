package com.plabro.realestate.holders;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.activity.WebViewActivity;
import com.plabro.realestate.activity.XMPPChat;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.BiddingFeed;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.models.propFeeds.listings.ListingFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.PBSharingManager;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 11/01/16.
 */
public class BiddingFeedHolder extends BaseFeedHolder {
    private TextView title, bidDesc, bidButton, bidTime, bidSummary, bidSubtitle, callTv, chatTv, shareTv, copyTv;
    private ImageView bidImage;

    public BiddingFeedHolder(View itemView) {
        super(itemView);
        title = findTV(R.id.tv_bid_title);
        bidDesc = findTV(R.id.tv_suggestion_info);
        bidTime = findTV(R.id.tv_bid_time);
        bidSummary = findTV(R.id.tv_detail_text);
        bidButton = findTV(R.id.tv_bid);
        bidSubtitle = findTV(R.id.tv_bid_subtitle);
        callTv = findTV(R.id.tv_call);
        chatTv = findTV(R.id.tv_chat);
        shareTv = findTV(R.id.tv_share);
        copyTv = findTV(R.id.tv_copy);
        bidImage = (ImageView) findView(R.id.iv_bid);
    }

    @Override
    public void onBindViewHolder(final int position, final BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        if (feed instanceof BiddingFeed) {
            final BiddingFeed f = (BiddingFeed) feed;
            title.setText(f.getTitle());
            bidDesc.setText(f.getSugg_info());
            if (!TextUtils.isEmpty(f.getTime())) {
                bidTime.setVisibility(View.VISIBLE);
                bidTime.setText(f.getTime());
            } else {
                bidTime.setVisibility(View.GONE);
            }
            bidSummary.setText(f.getDescription());
            bidButton.setText(f.getButton_title());
            bidSubtitle.setText(f.getSubtitle());
            ActivityUtils.loadImage(ctx, f.getImg_url(), bidImage, 0);
            bidButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    WebViewActivity.startActivity(ctx, f.getTitle(), f.getButton_action_url(), f.getExit_url());
                }
            });
            setFeedText(f, position);

            callTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callAction(f, f.getProfile_phone(), position);
                }
            });

            chatTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    chatAction(f, position, f.getProfile_phone().toString());
                }
            });
            shareTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PBSharingManager.shareByAll(f, ctx);
                    trackEvent(Analytics.CardActions.Shared, position, f.get_id(), f.getType(), null);
                }
            });
            copyTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    BiddingFeed feedCopy = (BiddingFeed) f;
                    String feedCopiedStr = feedCopy.getProfile_phone() + " : \n" + feedCopy.getText() + "\nBy: " + feedCopy.getProfile_name();
                    ActivityUtils.copyFeed(feedCopiedStr, ctx);
                    trackEvent(Analytics.CardActions.Copied, position, f.get_id(), f.getType(), null);
                }
            });

        }
    }

    private void chatAction(Feeds feedObject, int position, String phoneNumber) {

        if (null != feedObject && !TextUtils.isEmpty(feedObject.getProfile_phone())) {

        } else {
            return;
        }

        String authorid = feedObject.getAuthorid() + "";
        List<String> userPhones = new ArrayList<String>();
        userPhones.add(phoneNumber);
        long profileId = feedObject.getAuthorid();

        Utility.userStats(ctx, "im");

        HashMap<String, Object> wrData = new HashMap<String, Object>();
        wrData.put("imTo", phoneNumber);
        wrData.put("imFrom", PBPreferences.getPhone());
        wrData.put("source", type);
        wrData.put("ScreenName", TAG);
        wrData.put("Action", "IM");
        trackEvent(Analytics.CardActions.Chat, position, feedObject.get_id(), feedObject.getType(), wrData);

        Intent intent = new Intent(ctx, XMPPChat.class);
        String num = userPhones.get(0);
        String displayName = ActivityUtils.getDisplayNameFromPhone(num);
        if (displayName != null && !TextUtils.isEmpty(displayName)) {
            intent.putExtra("name", displayName);

        } else {
            intent.putExtra("name", feedObject.getProfile_name());

        }
        intent.putExtra("img", feedObject.getProfile_img());
        intent.putExtra("last_seen", feedObject.getTime());

        intent.putExtra("phone_key", num);
        intent.putExtra("userid", profileId + "");
        intent.putExtra("feed_text", feedObject.getSumm()); //summary of hashtags
        PlabroIntentService.startForNotifyingInteraction(wrData);
        Bundle b = new Bundle();
        b.putSerializable(Constants.BUNDLE_KEY_FEED, feedObject);
        intent.putExtras(b);
        ctx.startActivity(intent);
    }

    private void callAction(Feeds feeds, String phone, int position) {
        boolean status = Util.checkIfStringIsPhone(phone);

        Utility.userStats(ctx, "call");


        HashMap<String, Object> wrData = new HashMap<String, Object>();

        wrData.put("ScreenName", "BiddingFeed");
        wrData.put("callFrom", PBPreferences.getPhone());
        wrData.put("source", "BiddingCard");
        wrData.put("ClickedIndex", "" + position);

        ActivityUtils.initiateCallDialog(ctx, feeds, feeds.getPhonemap(), feeds.getProfile_phone(), wrData, feeds.getIsFreeCall(), feeds.getProfile_img(), feeds.getProfile_name());
    }

    private void setFeedText(BiddingFeed feed, int position) {
        String text = feed.getText();
        int start = 0;
        int end = 250;
        if (feed.getSnippet().size() > 0) {
            start = Integer.parseInt(feed.getSnippet().get(0));
            end = Integer.parseInt(feed.getSnippet().get(1)) + 1;
        }

        int occurance = text.length() - text.replace("\n", "").length();


        if (start >= 0 && end < text.length()) {
            try {
                text = text.substring(start, end);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //adding span for hashtags in post
        SpannableString hashTagText = ActivityUtils.setHashTags(feed, text, start, end, ctx, bidSummary, getType(), position, false, getRefKey(), mActivity);//type and position send to track analytics

    }
}
