package com.plabro.realestate.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.desmond.parallaxviewpager.ParallaxFragmentPagerAdapter;
import com.desmond.parallaxviewpager.ParallaxViewPagerBaseActivity;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.ImageGallery;
import com.plabro.realestate.R;
import com.plabro.realestate.ShoutByOthers;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.fragments.UserEndorsements;
import com.plabro.realestate.fragments.UserInfo;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.Calls.CallPopUpInfo;
import com.plabro.realestate.models.follow_unfollow.FollowUnFollowResponse;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.profile.ProfileResponse;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.registerUser.RegisterResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.receivers.CallListener;
import com.plabro.realestate.uiHelpers.CropOption;
import com.plabro.realestate.uiHelpers.CropOptionAdapter;
import com.plabro.realestate.uiHelpers.slidingTab.SlidingTabLayout;
import com.plabro.realestate.uiHelpers.view.FontManager;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.PBConditionalDialogView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 02/08/15.
 */
public class UserProfileNew extends ParallaxViewPagerBaseActivity implements View.OnClickListener, VolleyListener, CallListener.CallInterface {
    private LinearLayout headerLayout;
    private ProgressDialog dialog;
    private PBConditionalDialogView conditionalView;
    private boolean isContactAdded;
    private String mAuthorId, mPhone, mWebHash;
    private ImageView userImg;
    private ProgressWheel mProgressWheel;
    private RelativeLayout mEditimage;
    private boolean isCurrentUser;
    private boolean isFollowedByMe;
    private ImageView mTopImage;
    private SlidingTabLayout mNavigBar;
    private ProfileClass profile;
    public static final String TAG = "UserProfileNew";
    private Toolbar toolbar;
    private int scrollY;
    //    private RelativeLayout mNoProfile;
    int addressHeight = 0;
    private Fragment userInfoFragment, userEndorsementFragment;

    private int currentPosition = -1;

    private static final int FOLLOWED_BACKGROUND = R.drawable.fab_unfollow;
    private static final int UN_FOLLOWED_BACKGROUND = R.drawable.drawable_fab;


    private static final int FOLLOWED_IMAGE = R.drawable.ic_follow_broker;
    private static final int UN_FOLLOWED_IMAGE = R.drawable.ic_following_broker;
    private static final String TYPEFACE_FILENAME = "Fonts/Roboto-Medium.ttf";


    private boolean profileCacheExist = false;
    //image selector
    private Uri mImageCaptureUri;

    //image from source type
    static final int REQUEST_TAKE_PHOTO = 2;
    private static final int PICK_FROM_CAMERA = 3;
    private static final int CROP_FROM_CAMERA = 4;
    private static final int PICK_FROM_FILE = 5;
    private static final int SELECT_PICTURE_CODE = 6;
    static final int REQUEST_IMAGE_CAPTURE = 12;

    private Bitmap capturedBitmap;
    String mCurrentPhotoPath;
    Boolean isProfileImageSelected = false, isRunning = true;

    public static void startActivity(Context ctx, String authorID, String phone, String deephash) {
        Bundle b = new Bundle();
        b.putString("authorid", authorID);
        b.putString("phone", phone);
        b.putString("deep_hash", deephash);
        Intent i = new Intent(ctx, UserProfileNew.class);
        i.putExtras(b);
        ctx.startActivity(i);
    }

    public static String getTAG() {
        return "User Profile";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Analytics.trackScreen(Analytics.UserProfile.UserProfile);
        setContentView(R.layout.profile_user_new);
        headerLayout = (LinearLayout) findViewById(R.id.header);


        inflateToolbar();
        //profile = AppController.getInstance().getUserProfile();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mAuthorId = bundle.getString("authorid");
            Log.d(TAG, "Author ID is null or not :- " + mAuthorId);
            mPhone = bundle.getString("phone");
            mWebHash = bundle.getString("deep_hash");
            isCurrentUser = bundle.getBoolean("isCurrentUser", false);

            // check again wether user is current user or not
            if (!isCurrentUser) {
                isCurrentUser = isCurrentUser();
            }


        } else {
            // isCurrentUser = true;
        }

        conditionalView = (PBConditionalDialogView) findViewById(R.id.pbConditionalView);
        conditionalView.setOnPBConditionListener(new PBConditionalDialogView.OnPBConditionListener() {
            @Override
            public void onRefresh() {
                // getProfile();
                reload();
            }
        });
        // mTopImage = (ImageView) findViewById(R.id.image);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                setTabTextView();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mNavigBar = (SlidingTabLayout) findViewById(R.id.navig_tab);
        mHeader = findViewById(R.id.header);
        //mNoProfile = (RelativeLayout) findViewById(R.id.rl_no_profile);
        if (savedInstanceState != null) {
            //mTopImage.setTranslationY(savedInstanceState.getFloat(IMAGE_TRANSLATION_Y));
            mHeader.setTranslationY(savedInstanceState.getFloat(HEADER_TRANSLATION_Y));
        }

        //setupAdapter();
//
//        profile = AppController.getInstance().getUserProfile();
//        if (null != profile) {
//            setupProfile();
//        }
        getProfile();
    }


    private void setTabTextView() {
        int count = mNavigBar.getTabStrip().getChildCount();
        int currentPosition = mViewPager.getCurrentItem();
        if (currentPosition == 1) {
            Analytics.trackScreen(Analytics.UserProfile.UserProfile);
        }
        for (int i = 0; i < count; i++) {
            TextView v = (TextView) mNavigBar.getTabStrip().getChildAt(i);
            Typeface tf = FontManager.getInstance().getFont(TYPEFACE_FILENAME);
            v.setTypeface(tf, Typeface.NORMAL);
            if (i == currentPosition)
                v.setTextColor(getResources().getColor(R.color.color_tab_selected_text));
            else
                v.setTextColor(getResources().getColor(R.color.color_tab_text));
        }
    }


    @Override
    protected void initValues() {
        int tabHeight = getResources().getDimensionPixelSize(R.dimen.tab_height);
//        mMinHeaderHeight = getResources().getDimensionPixelSize(R.dimen.min_header_height);
        mMinHeaderHeight = mHeader.getHeight() - addressHeight;
        mHeaderHeight = mMinHeaderHeight;

        mMinHeaderTranslation = -mMinHeaderHeight + tabHeight;

        Log.d(TAG, "TabHeight:" + tabHeight + ", MinHeaderHeight:" + mMinHeaderHeight + ", mHeaderHeight:" + mHeaderHeight + ", MinHeaderTranslation:" + mMinHeaderTranslation);
        mNumFragments = 2;
        showHeaderHeight();
        if (addressHeight != 0) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, mMinHeaderHeight);
            mHeader.setLayoutParams(params);
        }
        showHeaderHeight();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //outState.putFloat(IMAGE_TRANSLATION_Y, mTopImage.getTranslationY());
        //outState.putFloat(HEADER_TRANSLATION_Y, mHeader.getTranslationY());
        super.onSaveInstanceState(outState);
    }

    private void showHeaderHeight() {
        Log.d(TAG, "HeaderHeight:" + mHeader.getHeight() + ", LP:" + mHeader.getLayoutParams().height + ", measuredHeight:" + mHeader.getMeasuredHeight());
    }

    @Override
    protected void setupAdapter() {
        initValues();
        Log.d(TAG, "Setting Adapter");
        mViewPager.removeAllViewsInLayout();

        mAdapter = new ViewPagerAdapter(getSupportFragmentManager(), mNumFragments);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(mNumFragments);
        mNavigBar.setOnPageChangeListener(getViewPagerChangeListener());
        mNavigBar.setViewPager(mViewPager);
        setTabTextView();
//        showHeaderHeight();
    }


    @Override
    protected void scrollHeader(int scrollY) {
        Log.d(TAG, scrollY + ":ScrollY");
        this.scrollY = scrollY;
        float translationY = Math.max(-scrollY, mMinHeaderTranslation);
        mHeader.setTranslationY(translationY);
//        Log.d(TAG, "MinHeader Height:" + mMinHeaderHeight);
//        Log.d(TAG, "Divider:" + (scrollY / mMinHeaderHeight));
//        int alpha = (scrollY / mMinHeaderHeight) * 256;
//        Log.d(TAG, "Alpha:" + alpha);
//        this.toolbar.getBackground().setAlpha(alpha);
        //mTopImage.setTranslationY(-translationY / 3);
    }


    private class ViewPagerAdapter extends ParallaxFragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm, int numFragments) {
            super(fm, numFragments);
        }

        private int currentSelected = 0;

        @Override
        public Fragment getItem(int position) {

            Log.d(TAG, "Page Get Item" + position);
            Fragment fragment;
            switch (position) {
                case 0:
                    if (userInfoFragment == null) {
                        userInfoFragment = (UserInfo) UserInfo.getInstance(profile, isCurrentUser);
                    }
                    ((UserInfo) userInfoFragment).setHeight(mHeaderHeight);
                    fragment = userInfoFragment;
                    break;

                case 1:
                    Log.d(TAG, "UserEndorsements Fragment");
                    if (userEndorsementFragment == null) {
                        userEndorsementFragment = (UserEndorsements) UserEndorsements.getInstance(profile, isCurrentUser);
                    }
                    ((UserEndorsements) userEndorsementFragment).setHeaderHeight(mHeaderHeight);
                    fragment = userEndorsementFragment;
                    break;

                default:
                    throw new IllegalArgumentException("Wrong page given " + position);
            }
            currentFragment = (ProfileRefreshListener) fragment;
            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Details";

                case 1:
                    //return "Endorsements";
                    return "Recommendations"; //todo: prannoy rating changes


                default:
                    throw new IllegalArgumentException("wrong position for the fragment in vehicle page");
            }
        }
    }


    public void getProfile() {

        HashMap<String, String> params = new HashMap<String, String>();
        if (!TextUtils.isEmpty(mAuthorId) && !mAuthorId.equals("null")) {
            params.put(PlaBroApi.PLABRO_GET_PROFILE_AUTO_PARAM_CONTACT_ID, mAuthorId);
        }
        if (!TextUtils.isEmpty(mPhone)) {
            params.put(PlaBroApi.PLABRO_GET_PROFILE_AUTO_PARAM_PHONE, mPhone);
        }
        if (!TextUtils.isEmpty(mWebHash)) {
            params.put(PlaBroApi.PLABRO_GET_PROFILE_AUTO_PARAM_WEB_HASH, mWebHash);
        }
        params = Utility.getInitialParams(PlaBroApi.RT.GETPROFILE, params);
        showProgressDialog();

        AppVolley.processRequest(Constants.TASK_CODES.USER_PROFILE, ProfileResponse.class, null, PlaBroApi.getBaseUrl(this), params, RequestMethod.GET, this);
    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        if (!isRunning) return;
        Log.d(TAG, "OnSuccess");
        hideProgressDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.USER_PROFILE:
                ProfileResponse res = (ProfileResponse) response.getResponse();
                profile = res.getOutput_params().getData();
                if (profile != null) {
                    setupProfile();
                }


                break;

            case Constants.TASK_CODES.FOLLOW:
                FollowUnFollowResponse followResponse = (FollowUnFollowResponse) response.getResponse();

                Log.d(TAG, "" + followResponse.isStatus());
                if (followResponse.isStatus()) {
                    isFollowedByMe = true;
                    setFabIcon();
                }
                break;
            case Constants.TASK_CODES.UNFOLLOW:
                FollowUnFollowResponse unFollowResponse = (FollowUnFollowResponse) response.getResponse();
                Log.d(TAG, "" + unFollowResponse.isStatus());
                isFollowedByMe = false;
                setFabIcon();
                break;
        }

    }

    private void setupProfile() {

        mHeader.setVisibility(View.VISIBLE);
        conditionalView.setVisibility(View.GONE);

        //mNoProfile.setVisibility(View.GONE);
        isCurrentUser = isCurrentUser(profile);

        if (null != profile) {
            Log.d(TAG, "Refreshing Profile");
            if (isCurrentUser) {
                AppController.getInstance().setUserProfile(profile);
            }
            setViews(profile);
            if (null != currentFragment && isProfileRefreshed) {
                if (userInfoFragment != null) {
                    ((ProfileRefreshListener) userInfoFragment).onProfileRefresh(profile);
                }
                if (userEndorsementFragment != null) {
                    ((ProfileRefreshListener) userEndorsementFragment).onProfileRefresh(profile);
                }
            } else {
                Log.d(TAG, "" + isProfileRefreshed + ", current Fragment:" + currentFragment);
            }
        }

    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        //mNoProfile.setVisibility(View.VISIBLE);
        if (!isRunning) return;
        Log.d(TAG, "OnFailure");
        hideProgressDialog();
        switch (response.getCustomException().getExceptionType()) {
            case VolleyListener.SESSION_EXPIRED:
                onPlabroSessionExpired();
                conditionalView.setVisibility(View.VISIBLE);
                conditionalView.setType(PBConditionalDialogView.ConditionalDialog.SESSION_EXPIRED);

                break;
            case VolleyListener.NO_INETERNET_EXCEPTION:
                mHeader.setVisibility(View.GONE);
                conditionalView.setVisibility(View.VISIBLE);
                conditionalView.setType(PBConditionalDialogView.ConditionalDialog.INTERNET_CONNECTION);
                break;
            case VolleyListener.STATUS_FALSE_EXCEPTION:
                conditionalView.setVisibility(View.VISIBLE);
                conditionalView.setType(PBConditionalDialogView.ConditionalDialog.SERVER_ERROR);
                Toast.makeText(UserProfileNew.this, response.getCustomException().getMessage(), Toast.LENGTH_LONG).show();
            case VolleyListener.JSON_PARSE_EXCEPTION:
                conditionalView.setVisibility(View.VISIBLE);
                conditionalView.setType(PBConditionalDialogView.ConditionalDialog.SERVER_ERROR);
            default:
                Toast.makeText(UserProfileNew.this, response.getCustomException().getMessage(), Toast.LENGTH_LONG).show();

                mHeader.setVisibility(View.GONE);
                conditionalView.setVisibility(View.VISIBLE);
                conditionalView.setType(PBConditionalDialogView.ConditionalDialog.SERVER_ERROR);
                break;
        }
        switch (taskCode) {
            case Constants.TASK_CODES.USER_PROFILE:
                mHeader.setVisibility(View.GONE);
                break;
            case Constants.TASK_CODES.FOLLOW:
                isFollowedByMe = false;
                setFabIcon();
                break;
            case Constants.TASK_CODES.UNFOLLOW:
                isFollowedByMe = true;
                setFabIcon();
                break;
        }
    }

    public void onPlabroSessionExpired() {
        Utility.userLogin(this);
    }

    private void setViews(ProfileClass profile) {
        setHeader(profile);
        mAdapter = null;
        setFabIcon();
        if (isProfileRefreshed) {
            addressHeight = 0;
        }
        setupAdapter();
    }

    public void setText(int textViewId, String text) {
        TextView textView = (TextView) findViewById(textViewId);
        textView.setText(text);
    }

    protected void setClickListeners(int... viewIds) {
        for (int i : viewIds) {
            findViewById(i).setOnClickListener(this);
        }
    }

    private void setHeader(ProfileClass profile) {
        Log.d(TAG, "Setting Header");
        userImg = (ImageView) findViewById(R.id.iv_profileImage);
        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mProgressWheel.spin();


        mEditimage = (RelativeLayout) findViewById(R.id.rl_edit_image);
        String imgUrl = profile.getImg();
        setup_profile_image_listener();
        if (!TextUtils.isEmpty(imgUrl)) {
            Picasso.with(UserProfileNew.this).load(imgUrl).into(userImg, new Callback() {
                @Override
                public void onSuccess() {
                    mProgressWheel.stopSpinning();
                }

                @Override
                public void onError() {
                    userImg.setImageDrawable(getResources().getDrawable(R.drawable.profile_default_one));
                    mProgressWheel.stopSpinning();
                }
            });
        }
        String displayName = "";
        if(!TextUtils.isEmpty(profile.getPhone())){
            displayName = ActivityUtils.getDisplayNameFromPhone(profile.getPhone());
        }
        if(TextUtils.isEmpty(displayName)){
            displayName = profile.getName();
        }
        profile.setName(displayName);
        setText(R.id.tv_user_name, Util.toDisplayCase(displayName));
        TextView tv = (TextView) findViewById(R.id.tv_user_addr);
        tv.setText(Util.toDisplayCase(profile.getCity_name()) + "");
//        addressHeight = tv.getHeight();
        Log.d(TAG, "Address Height:" + addressHeight);
//        findViewById(R.id.tv_user_addr).setVisibility(View.GONE);
        setText(R.id.tv_last_seen, "last seen " + profile.getTime());
        setText(R.id.tv_count_followers, profile.getFollowed() + " Followers");
        setText(R.id.tv_count_following, profile.getFollowing() + " Following");
        setText(R.id.tv_count_shouts, profile.getShout_count() + " Shouts");
        isFollowedByMe = profile.isFollowedByMe();
        setClickListeners(R.id.tv_count_shouts, R.id.tv_count_followers, R.id.tv_count_following, R.id.btn_follow);
        //setFabIcon();
        //showHeaderHeight();

    }


    private void setFabIcon() {
        Button btn = (Button) findViewById(R.id.btn_follow);
        if (btn.getVisibility() == View.GONE) {
            addressHeight = 0;
            return;
        }
        if (isCurrentUser) {
            addressHeight = btn.getHeight();
            Log.d(TAG, "Btn Height :" + btn.getHeight());
            btn.setOnClickListener(null);
            btn.setVisibility(View.GONE);
            return;
        }
//        FrameLayout frame = (FrameLayout) findViewById(R.id.frame_follow_unfollow);
//        frame.setBackgroundResource(0);
//        ImageView img = (ImageView) findViewById(R.id.iv_follow_unfollow);
//        img.setImageResource(0);
        if (isFollowedByMe) {
            btn.setText("FOLLOWING");
            btn.setTextColor(getResources().getColor(R.color.white));
            btn.setBackgroundColor(getResources().getColor(R.color.green));
//            frame.setBackgroundResource(FOLLOWED_BACKGROUND);
//            img.setImageResource(FOLLOWED_IMAGE);
        } else {
            btn.setText("FOLLOW");
            btn.setTextColor(getResources().getColor(R.color.color_tab_text));
            btn.setBackgroundColor(getResources().getColor(R.color.white));
//            frame.setBackgroundResource(UN_FOLLOWED_BACKGROUND);
//            img.setImageResource(UN_FOLLOWED_IMAGE);
        }
    }

    private boolean isCurrentUser() {
        String phone = PBPreferences.getPhone();
        if (null != phone && phone.equals(mPhone)) {
            return true;
        } else {
            ProfileClass userProfile = AppController.getInstance().getUserProfile();
            if (null != userProfile && null != mAuthorId && mAuthorId.equals(userProfile.getAuthorid()))
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_count_followers:
                onFollowingClicked(false);
                break;
            case R.id.tv_count_following:
                onFollowingClicked(true);
                break;
            case R.id.tv_count_shouts:
                onOtherShoutsClicked();
                break;

            case R.id.btn_follow:
                onFollowButtonClicked();
                break;
        }
    }

    private void onOtherShoutsClicked() {
        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        wrData.put("Action", "View Others Shouts");
        if (null != mAuthorId) {
            wrData.put("UserId", mAuthorId);
        }



        Intent intent = new Intent(this, ShoutByOthers.class);
        intent.putExtra("contactId", mAuthorId);
        if (null != profile) {
            intent.putExtra("contactName", profile.getName() + "");
        }
        startActivity(intent);
    }

    private void onFollowingClicked(boolean isFollowingClicked) {
        Intent intent = new Intent(this, Followers.class);
        HashMap<String, Object> data = new HashMap<>();
        data.put("ScreenName", TAG);
        intent.putExtra("authorid", mAuthorId);
        if (isFollowingClicked) {
            data.put("Action", "Following Clicked");
            intent.putExtra("type", "following");
        } else {
            data.put("Action", "Followers Clicked");
            intent.putExtra("type", "followers");
        }
        startActivity(intent);
    }

    private void onFollowButtonClicked() {
        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        if (null != mAuthorId) {
            wrData.put(Analytics.Params.USER_ID, mAuthorId);
        }
        if (isFollowedByMe) {
            Analytics.trackSocialEvent(Analytics.Actions.UnfollowUser, wrData);
        } else {
            Analytics.trackSocialEvent(Analytics.Actions.FollowUser, wrData);
        }
        follow_user(isFollowedByMe);
    }

    private void follow_user(boolean isFollowedByMe) {

        HashMap<String, String> params = new HashMap<String, String>();

        if (!TextUtils.isEmpty(mAuthorId)) {
            params.put(PlaBroApi.PLABRO_FOLLOW_UNFOLLOW_POST_PARAM_CONTACT_ID, mAuthorId);
        } else if (!TextUtils.isEmpty(profile.getAuthorid())) {
            mAuthorId = profile.getAuthorid();
            params.put(PlaBroApi.PLABRO_FOLLOW_UNFOLLOW_POST_PARAM_CONTACT_ID, profile.getAuthorid());
        } else if (!TextUtils.isEmpty(profile.getPhone())) {
            params.put(PlaBroApi.PHONE, mAuthorId);
        }


        int taskCode = 1;
        if (!isFollowedByMe) {
            isFollowedByMe = true;
            taskCode = Constants.TASK_CODES.FOLLOW;
            params = Utility.getInitialParams(PlaBroApi.RT.FOLLOW, params);
        } else {
            isFollowedByMe = false;
            taskCode = Constants.TASK_CODES.UNFOLLOW;
            params = Utility.getInitialParams(PlaBroApi.RT.UN_FOLLOW, params);
        }
        setFabIcon();

        AppVolley.processRequest(taskCode, FollowUnFollowResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, this);
    }

    private void showProgressDialog() {
        if (null != dialog && dialog.isShowing()) {
        } else {
            dialog = new ProgressDialog(UserProfileNew.this);
            dialog.setMessage("Loading...");
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    private void hideProgressDialog() {
        if (null != dialog && dialog.isShowing()) {
            Utility.dismissProgress(dialog);
        }
    }

    private void onUserImageClicked() {
        ProfileClass profileClass = profile;
        ArrayList<String> imageUrlList = new ArrayList<String>();
        if (profileClass == null || TextUtils.isEmpty(profileClass.getImg())) {
            return;
        }

        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        if (null != mAuthorId) {
            wrData.put("UserId", mAuthorId);
        }
        wrData.put("Action", "View Image");

        Analytics.trackProfileEvent(Analytics.Actions.ImageClicked, wrData);
        imageUrlList.add(profileClass.getImg());
        Intent intent = new Intent(this, ImageGallery.class);
        intent.putStringArrayListExtra("menus", imageUrlList);

        intent.putExtra("position", 0);
        startActivity(intent);
    }

    public void inflateToolbar() {
        //setting action bar
        toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        if (toolbar == null) return;
        setSupportActionBar(toolbar);

        // Set an OnMenuItemClickListener to handle menu item clicks
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Handle the menu item
                int id = item.getItemId();
                if (id == R.id.action_addToContacts) {
                    if (null != profile && null != profile.getPhone() && null != profile.getPhone()) {
                        if (!isContactAdded) {
                            checkPermission();
                        } else {
                            Toast.makeText(UserProfileNew.this, "Already Added", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Util.showSnackBarMessage(UserProfileNew.this, UserProfileNew.this, "Could not add contacts. Please try again later.");
                    }
                    return true;
                }
                if (id == R.id.action_edit_profile) {
                    if (null != profile) {
                        Bundle b = new Bundle();
                        b.putString("type", Constants.ProfileParams.NAME);
                        b.putString("val", profile.getName());
                        b.putString("city", profile.getCity_name());
                        b.putString("city_id", profile.getCity_id());
                        Intent editIntent = new Intent(UserProfileNew.this, EditProfile.class);
                        editIntent.putExtras(b);
                        startActivityForResult(editIntent, 1);
                    }
                    return true;
                }
                return false;
            }

        });

        // Inflate a menu to be displayed in the toolbar
        if (isCurrentUser) {
            toolbar.inflateMenu(R.menu.menu_current_user_profile);
        } else {
            toolbar.inflateMenu(R.menu.menu_user_profile_new);
        }

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                onBackPressed();
            }
        });
        getSupportActionBar().setTitle("");
    }


    private void checkPermission() {

        hasPermission(Manifest.permission.WRITE_CONTACTS, PlaBroApi.PERMISSIONS.WRITE_CONTACTS, PlaBroApi.PERMISSIONS_MESSAGE.WRITE_CONTACTS);


    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission, int PMCode, String pmMessage) {
        this.PMCode = PMCode;
        this.pmMessage = pmMessage;
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) {
            //Build version below Marshmallow
            onMMPermisssoinResult(PMCode, true, pmMessage);
            return true;
        }


        if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            //permission is already available, proceed normally
            onMMPermisssoinResult(PMCode, true, pmMessage);
            return true;

        } else {
            //Permission not granted, lets ask user for the permissions
            if (shouldShowRequestPermissionRationale(permission)) {
                Toast.makeText(this, pmMessage, Toast.LENGTH_SHORT).show();
            }

            requestPermissions(new String[]{permission}, PMCode);

        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PMCode
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onMMPermisssoinResult(requestCode, true, pmMessage);
        } else {
            onMMPermisssoinResult(requestCode, false, pmMessage);
        }
    }

    public void onMMPermisssoinResult(int requestCode, boolean status, String message) {

        if (status) {
            if (ActivityUtils.WritePhoneContact(profile.getName(), profile.getPhone())) {
                Toast.makeText(UserProfileNew.this, "Successfully added to contacts", Toast.LENGTH_LONG).show();
                isContactAdded = true;
            } else {
                Toast.makeText(UserProfileNew.this, "Can not add to contacts", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(UserProfileNew.this, "Permission required to add to contacts", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (isCurrentUser) {
            getMenuInflater().inflate(R.menu.menu_current_user_profile, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_user_profile_new, menu);
        }

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "OnActivityResult" + requestCode + "--> Req:" + requestCode);
        if (resultCode != Activity.RESULT_OK) return;
        if (requestCode == 65537 || requestCode == 1) {
            isProfileRefreshed = true;
            getProfile();
        }

        switch (requestCode) {

            case REQUEST_IMAGE_CAPTURE:

                capturedBitmap = ImageCropper.croppedBitmap;
                if (capturedBitmap != null) {
                    setPic();
                    updateProfileImage();
                    ImageCropper.croppedBitmap = null;
                }
                break;

        }


    }

    private boolean isProfileRefreshed;
    private ProfileRefreshListener currentFragment;

    public static interface ProfileRefreshListener {
        public void onProfileRefresh(ProfileClass profile);
    }


    private boolean isCurrentUser(ProfileClass profile) {
        String phone = PBPreferences.getPhone();
        if (null != phone && phone.equalsIgnoreCase(profile.getPhone())) {
            return true;
        }
        return false;
    }


    private void doCrop() {

        //image cropped
        HashMap<String, Object> data = new HashMap<>();
        data.put("ScreenName", TAG);
        data.put("Action", "Crop");

        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);

        int size = list.size();

        if (size == 0) {
            Toast.makeText(this, "Can not find image crop app", Toast.LENGTH_SHORT).show();

            return;
        } else {
            intent.setData(mImageCaptureUri);

            intent.putExtra("outputX", 400);
            intent.putExtra("outputY", 250);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);
            intent.putExtra("return-data", true);

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));

                startActivityForResult(i, CROP_FROM_CAMERA);
            } else {
                for (ResolveInfo res : list) {
                    final CropOption co = new CropOption();

                    co.title = getPackageManager().getApplicationLabel(res.activityInfo.applicationInfo);
                    co.icon = getPackageManager().getApplicationIcon(res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);

                    co.appIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));

                    cropOptions.add(co);
                }

                CropOptionAdapter adapter = new CropOptionAdapter(getApplicationContext(), cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose Crop App");
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        startActivityForResult(cropOptions.get(item).appIntent, CROP_FROM_CAMERA);
                    }
                });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        try {
                            if (mImageCaptureUri != null) {
                                getContentResolver().delete(mImageCaptureUri, null, null);
                                mImageCaptureUri = null;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                AlertDialog alert = builder.create();

                alert.show();
            }
        }
    }


    //add various image source listener,cropping functionality and image show on page
    void setup_profile_image_listener() {

        //image selected
        HashMap<String, Object> data = new HashMap<>();
        data.put("ScreenName", TAG);
        data.put("Action", "Select");


        //image selector


        userImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUserImageClicked();
//                if (isCurrentUser) {
//                    dispatchTakePictureIntent();
//                } else {
//                    onUserImageClicked();
//                }

            }
        });

        if (isCurrentUser) {
            mEditimage.setVisibility(View.VISIBLE);
        } else {
            mEditimage.setVisibility(View.GONE);

        }
        mEditimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isCurrentUser) {
                    dispatchTakePictureIntent();
                }
            }
        });


    }


    //to capture image without cropping via new method
    private void dispatchTakePictureIntent() {

        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        if (null != mAuthorId) {
            wrData.put("UserId", mAuthorId);
        }
        wrData.put("Action", "Edit Image");

        Analytics.trackProfileEvent(Analytics.Actions.ImageClicked, wrData);
        Intent editIntent = new Intent(UserProfileNew.this, ImageCropper.class);
        editIntent.putExtra("img", profile.getImg());
        startActivityForResult(editIntent, REQUEST_IMAGE_CAPTURE);
        //image selector
//
//        final String[] items = new String[]{"Take from camera", "Select from gallery"};
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, items);
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//
//        builder.setTitle("Select Image");
//        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int item) { //pick from camera
//                if (item == 0) {
//                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
//                    }
//                } else { //pick from file
//
//                    Intent intent = new Intent(Intent.ACTION_PICK);
//                    //intent.setPackage("com.google.android.apps.plus");
//                    intent.setType("image/*");
//                    //intent.setAction(Intent.ACTION_GET_CONTENT);
//                    startActivityForResult(Intent.createChooser(intent,
//                            "Select Picture"), PICK_FROM_FILE);
//
//                }
//            }
//        });
//
//        final AlertDialog dialog = builder.create();
//        dialog.show();


    }

    /**
     * http://developer.android.com/training/camera/photobasics.html
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory() + "/plabro";
        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();

        File image = new File(storageDir + "/" + imageFileName + ".jpg");

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.i(TAG, "photo path = " + mCurrentPhotoPath);
        return image;
    }

    public String getEncodedPicture() {

        try {
            if (null != capturedBitmap) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                capturedBitmap.compress(Bitmap.CompressFormat.JPEG, 60, baos);
                byte[] b = baos.toByteArray();
                String temp = Base64.encodeToString(b, Base64.DEFAULT);

                return temp;
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";

    }


    private void updateProfileImage() {

        final ProgressDialog progress;
        progress = ProgressDialog.show(UserProfileNew.this, "Please wait",
                "Updating profile image...", true);

        progress.setCancelable(true);
        progress.setCanceledOnTouchOutside(true);
        HashMap<String, String> params = new HashMap<String, String>();

        if (null != capturedBitmap) {
            params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_IMG, getEncodedPicture());
            //image uploaded
            HashMap<String, Object> data = new HashMap<>();
            data.put("ScreenName", TAG);
            data.put("Action", "Upload");

        }
        params = Utility.getInitialParams(PlaBroApi.RT.SETPREF, params);
//        params.put("debug", "3");

        AppVolley.processRequest(Constants.TASK_CODES.SET_PREFS, RegisterResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.POST, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);
                RegisterResponse registerResponse = (RegisterResponse) response.getResponse();
                PBPreferences.saveData(PBPreferences.IS_IMAGE_UPDATED, true);


                Toast.makeText(UserProfileNew.this, "Profile pic updated successfully.", Toast.LENGTH_LONG).show();

                getProfile();

                Log.i("PlaBro", "Preferences set sussessfully" + registerResponse.isStatus());


            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
//                onApiFailure(response, taskCode);
                Utility.dismissProgress(progress);
                Toast.makeText(UserProfileNew.this, "Some error occurred. Please try again.", Toast.LENGTH_LONG).show();
                Log.i("PlaBro", "onFailure : Oops...something went wrong");


            }
        });
    }

    private void setPic() {

        try {
            if (capturedBitmap != null) {
                userImg.setVisibility(View.VISIBLE);
                userImg.setImageBitmap(capturedBitmap);
                isProfileImageSelected = true;

            } else {
                isProfileImageSelected = false;
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Toast.makeText(UserProfileNew.this, "Error capturing picture!Please try again.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }

    private CallListener phoneListener;

    @Override
    public void startListeningToCall(BaseFeed feed, boolean isFreeCall) {
        if (isFreeCall) {
            ActivityUtils.saveFreeCallLogsToUtil(this, feed, TAG);
            return;
        }
        phoneListener = new CallListener(feed, this, isFreeCall);
        TelephonyManager telephony = (TelephonyManager)
                getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
        CallPopUpInfo.isCallClicked = false;

    }

    public void stopCallListener() {
        TelephonyManager telephony = (TelephonyManager)
                getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(phoneListener, PhoneStateListener.LISTEN_NONE);
    }

    @Override
    public void onCallConnected(BaseFeed feed) {
        Log.d(TAG, "Call Connected:");
    }

    @Override
    public void onCallEnded(BaseFeed feed, String incomingNumber) {
        Log.d(TAG, "Call Ended:");
        stopCallListener();

        if (feed instanceof ProfileClass) {

            CallPopUpInfo.isCallClicked = true;
            CallPopUpInfo.feeds = ActivityUtils.createFeedObjectFromProfile((ProfileClass) feed);
            CallPopUpInfo.feeds.setIncomingNumber(incomingNumber);

        }

    }

    @Override
    public void onCallReceived(BaseFeed feed, boolean isFreeCall) {
        Log.d(TAG, "Call Received:");
        Log.d(TAG, "Free Call Received:" + isFreeCall);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (null != CallPopUpInfo.feeds && CallPopUpInfo.isCallClicked) {
            CallPopUpInfo.isCallClicked = false;
            sendCallLogsAfterOneSecond();
        }
    }

    public void sendCallLogsAfterOneSecond() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (ActivityUtils.isCallConnected(UserProfileNew.this, CallPopUpInfo.feeds, getTag()) && isRunning) {
                    PlabroBaseActivity.showCallDialog(UserProfileNew.this, CallPopUpInfo.feeds, "CALL ENDED WITH", false);
                }
            }
        }, Constants.CALL_LOG_TIME_GAP);
    }


    public void reload() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        isRunning = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}
