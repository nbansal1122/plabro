package com.plabro.realestate.models.contacts;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by nitin on 07/09/15.
 */
@Table(name = "NonPlabroContact", id = BaseColumns._ID)
public class NonPlabroContact extends Model {
    @Column(unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String phoneNumber;
    @Column
    private String contactName;
    @Column
    private boolean isSelected;
    @Column
    private boolean isInvited;

    public boolean isNotifiedToServer() {
        return isNotifiedToServer;
    }

    public void setIsNotifiedToServer(boolean isNotifiedToServer) {
        this.isNotifiedToServer = isNotifiedToServer;
    }

    @Column
    private boolean isNotifiedToServer;

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public boolean isInvited() {
        return isInvited;
    }

    public void setIsInvited(boolean isInvited) {
        this.isInvited = isInvited;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }
}
