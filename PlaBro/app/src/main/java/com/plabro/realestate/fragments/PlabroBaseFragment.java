package com.plabro.realestate.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.GenericObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.login.Login;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.receivers.MarshMallowReceiver;
import com.plabro.realestate.receivers.PBLocalReceiver;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.UserRegistrationDialog;

import java.util.Map;

/**
 * Created by jmd on 5/4/2015.
 */
public abstract class PlabroBaseFragment extends MarshMallowFragments implements View.OnClickListener, PBLocalReceiver.PBActionListener, GenericObserver.GenericObserverInterface {

    public final String TAG = getTAG();
    protected View rootView;
    protected IntentFilter filter;
    PBLocalReceiver rec;
    MarshMallowReceiver marshMallowReceiver;
    private UserRegistrationDialog feedPopUpDialog;
    protected boolean isRunning = true;


    public abstract String getTAG();


    @Override
    public void onClick(View view) {

    }

    public void sendScreenNameC(String screen) {
        if (null != getActivity()) {
            Analytics.trackScreen(screen);
        }
    }

    public void sendScreenNameC(int screenStringID) {
        // sendScreenName(getResources().getString(screenStringID));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Tracking Screen", getTAG() + "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (null != getArguments()) {
            loadBundleData(getArguments());
        }
        if (getViewId() != 0) {
            rootView = inflater.inflate(getViewId(), null);
            findViewById();
            return rootView;
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    protected void loadBundleData(Bundle b) {

    }

    abstract protected void findViewById();

    public View findView(int id) {
        return rootView.findViewById(id);
    }

    public void setClickListeners(int... viewIds) {
        for (int id : viewIds) {
            rootView.findViewById(id).setOnClickListener(this);
        }
    }

    public void setClickListeners(View... views) {
        for (View view : views) {
            view.setOnClickListener(this);
        }
    }

    protected void setText(int textViewID, String text) {
        TextView textView = (TextView) findView(textViewID);
        textView.setText(text);
    }

    protected void setText(int textViewID, String text, View row) {
        TextView textView = (TextView) row.findViewById(textViewID);
        textView.setText(text);
    }

    abstract protected int getViewId();


    public void registerLocalReceiver() {
        if (rec == null) {
            rec = new PBLocalReceiver(this);
        } else {
            rec.addListener(this);
        }
        getActivity().registerReceiver(rec, getFilter());
    }

    public void unRegsiterLocalReceiver() {
        if (rec != null) {
            getActivity().unregisterReceiver(rec);
            rec = null;
        }
    }

    public void registerMarshMallowReceiver() {
        if (marshMallowReceiver == null) {
            marshMallowReceiver = new MarshMallowReceiver(this);
        }
        getActivity().registerReceiver(marshMallowReceiver, getFilter());
    }

    public void unRegsiterMarshMallowReceiver() {
        if (marshMallowReceiver != null) {
            getActivity().unregisterReceiver(marshMallowReceiver);
            marshMallowReceiver = null;
        }
    }


    public IntentFilter getFilter() {
        filter = new IntentFilter();
        filter.addAction(ACTION_REGISTRATION_UPDATE);
        filter.addAction(Constants.ACTION_PROFILE_UPDATE);
        filter.addAction(Constants.ACTION_FEED_REFRESH);
        return filter;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        unRegsiterLocalReceiver();
    }

    @Override
    public void OnReceive(Context context, Intent intent) {

    }

    public void processRequest(int taskCode, Class classType, String tag, String baseUrl, Map<String, String> params, RequestMethod method, VolleyListener listener) {
        AppVolley.processRequest(taskCode, classType, tag, baseUrl, params, method, listener);
    }

    public void processRequest(Class classType, String tag, Map<String, String> params, VolleyListener listener) {
        processRequest(1, classType, tag, PlaBroApi.getBaseUrl(getActivity()), params, RequestMethod.GET, listener);
    }

    public void processRequest(int taskCode, Class classType, Map<String, String> params, VolleyListener listener) {
        processRequest(taskCode, classType, null, PlaBroApi.getBaseUrl(getActivity()), params, RequestMethod.GET, listener);
    }

    public String getToolBarTilte() {
        return "";
    }

    protected void registerObserver(String event) {
        GenericObserver.getInstance().addListener(this, event);
    }

    @Override
    public void onEventUpdate(String event) {

    }


    protected void showRegistrationCard() {
        try {
            int val = PBPreferences.getTrialVersionState();
            if (val == 0) {

                Login login = new Select().from(Login.class).executeSingle();
                if (null != login) {
                    val = login.getTrial_version();
                }
            }

            switch (val) {
                case 0:
                    Utility.userLogin(getActivity());

                    if (!UserRegistrationDialog.isVisible) {
                        feedPopUpDialog = new UserRegistrationDialog(getActivity(), val);
                        feedPopUpDialog.show(getChildFragmentManager(), "UserRegister");
                    }
                    break;
                case 1:
                    if (!UserRegistrationDialog.isVisible) {
                        feedPopUpDialog = new UserRegistrationDialog(getActivity(), val);
                        feedPopUpDialog.show(getChildFragmentManager(), "UserRegister");
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void dismissCard() {
        if (null != feedPopUpDialog && feedPopUpDialog.isVisible()) {
            feedPopUpDialog.dismiss();
        }
    }

    public void onApiFailure(PBResponse response, int taskCode) {
        switch (response.getCustomException().getExceptionType()) {
            case VolleyListener.SESSION_EXPIRED:
                Utility.userLogin(getActivity());
                onSessionExpiry(response, taskCode);
                break;
            case VolleyListener.NO_INETERNET_EXCEPTION:
                onInternetException(response, taskCode);
                break;
            case VolleyListener.JSON_PARSE_EXCEPTION:
                break;
            case VolleyListener.TRIAL_EXPIRED_EXCEPTION:
                onTrialVersionExpired(response, taskCode);
                showToast(response.getCustomException().getMessage());
                break;
            case VolleyListener.STATUS_FALSE_EXCEPTION:
                onStatusFalse(response, taskCode);
                showToast(response.getCustomException().getMessage());
                break;
        }
    }

    public void onInternetException(PBResponse response, int taskCode) {

    }

    public void onTrialVersionExpired(PBResponse response, int taskCode) {

    }

    public void onStatusFalse(PBResponse response, int taskCode) {
        if (getActivity() == null) return;
        PBPreferences.setLoggedInState(false);


        if (response.getCustomException().getMessage().contains("Phone not registered yet")) {
            AppController.getInstance().clearUserAndLogout();
            new AlertDialog.Builder(getActivity())
                    .setTitle("Registration Error !")
                    .setMessage("Looks like you have registered from other device.")
                    .setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getActivity().moveTaskToBack(true);
                        }
                    }) // dismisses by default
                    .setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // do the acknowledged action, beware, this is run on UI thread
                            PBPreferences.setDeviceState(true);
                            Log.i("PlaBro", "Step 4 : New User with different phone");
                        }
                    })
                    .create()
                    .show();


        } else {

//            showGenericErrorMessage(response.getCustomException().getMessage());
            Log.i("PlaBro", "login failed due to :::" + response.getCustomException().getMessage());
        }
    }

    public void onSessionExpiry(PBResponse response, int taskCode) {

    }

    public void showToast(String toastMessage) {
        if (TextUtils.isEmpty(toastMessage)) return;
        try {
            Toast toast = Toast.makeText(getActivity(), toastMessage, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);

            LinearLayout layout = (LinearLayout) toast.getView();
            if (layout.getChildCount() > 0) {
                TextView tv = (TextView) layout.getChildAt(0);
                tv.setGravity(Gravity.CENTER);
            }

            toast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showSnackBar(String toastMessage) {
        try {

            Util.showSnackBarMessage(getActivity(), getActivity(), toastMessage);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void hideViews(View... views) {
        for (View v : views) {
            v.setVisibility(View.GONE);
        }
    }

    protected void showViews(View... views) {
        for (View v : views) {
            v.setVisibility(View.VISIBLE);
        }
    }

    protected void hideViews(int... ids) {
        for (int i : ids) {
            findView(i).setVisibility(View.GONE);
        }
    }

    protected void showViews(int... ids) {
        for (int i : ids) {
            findView(i).setVisibility(View.VISIBLE);
        }
    }

    public void startActivity(Bundle b, Class activityClass) {
        Intent i = new Intent(getActivity(), activityClass);
        i.putExtras(b);
        startActivity(i);
    }

    public void startNextActivity(Class activityClass) {
        Intent i = new Intent(getActivity(), activityClass);
        startActivity(i);
    }

    public void startActivityAndFinish(Bundle b, Class activityClass) {
        startActivity(b, activityClass);
        getActivity().finish();
    }

    protected void setTitle(String title) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
    }
}
