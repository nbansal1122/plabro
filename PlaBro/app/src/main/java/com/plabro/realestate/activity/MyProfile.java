package com.plabro.realestate.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.LocationSelectionListener;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.city.CityClass;
import com.plabro.realestate.models.city.CityResponse;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.profile.ProfileResponse;
import com.plabro.realestate.models.registerUser.RegisterResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.CropOption;
import com.plabro.realestate.uiHelpers.CropOptionAdapter;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.ListDialog;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MyProfile extends PlabroBaseActivity {


    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Toolbar toolbar;
    private ScrollView sv_profile;
    private RoundedImageView user_image;
    private TextView mUserNameDisplay, mFollowers, mFollowing,
            tv_user_phone, tv_user_mail,
            tv_user_web, tv_user_addr, tv_user_business, tv_no_profile, mProfileStatus, tv_user_city;
    private ProgressWheel mProgressWheel;
    private String mAuthorId;
    private ImageView mImageEditEmail, mImageEditWeb, mImageEditAddr, mImageEditBusiness, mImageEditStatus, mImageEditCity;
    private RelativeLayout mStatusLayout, mEmailLayout, mWebLayout, mAddrLayout, mBusinessLayout, mCityLayout;
    private boolean profileCacheExist = false;
    //image selector
    private Uri mImageCaptureUri;

    //image from source type
    private static final int PICK_FROM_CAMERA = 1;
    private static final int CROP_FROM_CAMERA = 2;
    private static final int PICK_FROM_FILE = 3;
    private static final int SELECT_PICTURE_CODE = 4;


    private Bitmap photo;
    Boolean isProfileImageSelected = false;

    private HashMap<String, String> globalCityList = new HashMap<String, String>();
    final ArrayList<String> myList = new ArrayList<String>();
    private Boolean isLocationSelected = false;
    private String locationSelected = "";
    private List<CityClass> cityClassList;
    private ProfileClass profileClass;


    @Override
    protected String getTag() {
        return "MyProfile";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

//        Intent i = new Intent(this, ProfileEndorsements.class);
//        startActivity(i);
//        finish();
        findViewById();

        inflateToolbar();

        profileClass = AppController.getInstance().getUserProfile();
        if (null == profileClass) {
            getUserProfile();
        } else {
            profileCacheExist = true;
            setupProfile(profileClass);
            getUserProfile();
        }

    }

    @Override
    public void inflateToolbar() {
        //setting action bar
        toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        // Set an OnMenuItemClickListener to handle menu item clicks
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Handle the menu item
                int id = item.getItemId();

                switch (id) {


                }
                return true;
            }

        });

        // Inflate a menu to be displayed in the toolbar
        toolbar.inflateMenu(R.menu.menu_my_profile);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_profile, menu);
        return true;
    }


    @Override
    public void findViewById() {

        sv_profile = (ScrollView) findViewById(R.id.sv_profile);
        user_image = (RoundedImageView) findViewById(R.id.user_image);
        mUserNameDisplay = (TextView) findViewById(R.id.tv_user_name);
        mFollowers = (TextView) findViewById(R.id.tv_profile_user_followers);
        mFollowing = (TextView) findViewById(R.id.tv_profile_user_following);
        tv_user_phone = (TextView) findViewById(R.id.tv_user_phone);
        tv_user_mail = (TextView) findViewById(R.id.tv_user_mail);
        tv_user_web = (TextView) findViewById(R.id.tv_user_web);
        tv_user_addr = (TextView) findViewById(R.id.tv_user_addr);
        tv_user_business = (TextView) findViewById(R.id.tv_user_business);
        tv_user_city = (TextView) findViewById(R.id.tv_user_city);

        tv_no_profile = (TextView) findViewById(R.id.tv_no_profile);
        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mImageEditEmail = (ImageView) findViewById(R.id.iv_edit_1);
        mImageEditWeb = (ImageView) findViewById(R.id.iv_edit_web);
        mImageEditAddr = (ImageView) findViewById(R.id.iv_edit_3);
        mImageEditBusiness = (ImageView) findViewById(R.id.iv_edit_4);
        mImageEditStatus = (ImageView) findViewById(R.id.iv_edit_status);
        mImageEditCity = (ImageView) findViewById(R.id.iv_edit_5);

        mProfileStatus = (TextView) findViewById((R.id.tv_profile_status));

        mStatusLayout = (RelativeLayout) findViewById(R.id.rl_profile_status);

        mEmailLayout = (RelativeLayout) findViewById(R.id.rl_email);
        mWebLayout = (RelativeLayout) findViewById(R.id.rl_social_web);
        mAddrLayout = (RelativeLayout) findViewById(R.id.rl_addr);
        mBusinessLayout = (RelativeLayout) findViewById(R.id.rl_business);
        mCityLayout = (RelativeLayout) findViewById(R.id.rl_city);


        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mProgressWheel.spin();

        //pull to refresh
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshLayout.setRefreshing(false);

                        mProgressWheel.spin();

                        getUserProfile();

                    }
                }, Constants.GOOGLE_PULL_TO_REFRESH_TIMER);

            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        mFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> data = new HashMap<>();
                data.put("ScreenName", TAG);
                data.put("Action", "Followers Clicked");

                Intent intent = new Intent(MyProfile.this, Followers.class);
                intent.putExtra("authorid", mAuthorId);
                intent.putExtra("type", "followers");
                startActivity(intent);

//                startActivityForResult(intent, Constants.FEEDS_REQUEST_CODE);

            }
        });
        mFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> data = new HashMap<>();
                data.put("ScreenName", TAG);
                data.put("Action", "Following Clicked");

                Intent intent = new Intent(MyProfile.this, Followers.class);
                intent.putExtra("authorid", mAuthorId);
                intent.putExtra("type", "following");
                startActivity(intent);
//                startActivityForResult(intent, Constants.FEEDS_REQUEST_CODE);

            }
        });


        mStatusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //Navigate to preferences
                Intent intent = new Intent(MyProfile.this, EditProfile.class);
                intent.putExtra("type", "status");
                intent.putExtra("val", mProfileStatus.getText());
                startActivityForResult(intent, Constants.STATUS_REQUEST_CODE);


            }

        });


        mUserNameDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //Navigate to EditProfile
                Intent intent = new Intent(MyProfile.this, EditProfile.class);
                intent.putExtra("type", "name");
                intent.putExtra("val", mUserNameDisplay.getText());
                startActivityForResult(intent, Constants.NAME_REQUEST_CODE);


            }

        });


        mEmailLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //CleverTap tracking

                //Navigate to EditProfile
                Intent intent = new Intent(MyProfile.this, EditProfile.class);
                intent.putExtra("type", "email");
                intent.putExtra("val", tv_user_mail.getText());
                startActivityForResult(intent, Constants.EMAIL_REQUEST_CODE);


            }

        });
        mImageEditWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //WizRocket tracking
                //Navigate to EditProfile
                Intent intent = new Intent(MyProfile.this, EditProfile.class);
                intent.putExtra("type", "web");
                intent.putExtra("val", tv_user_web.getText());
                startActivityForResult(intent, Constants.WEB_REQUEST_CODE);
                sendScreenName(R.string.funnel_USER_IMAGE_CLICKED);


            }

        });

        mWebLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String website = tv_user_web.getText().toString().trim();
                if (!TextUtils.isEmpty(website)) {
                    if (!website.startsWith("http")) {
                        website = "http://" + website;
                    }
                    if (URLUtil.isValidUrl(website)) {
                        Intent browserIntent = new Intent();
                        browserIntent.setAction(Intent.ACTION_VIEW);
                        browserIntent.setData(Uri.parse(website));
                        startActivity(browserIntent);
                    }
                }
            }
        });
        mAddrLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //Navigate to EditProfile
                Intent intent = new Intent(MyProfile.this, EditProfile.class);
                intent.putExtra("type", "addr");
                intent.putExtra("val", tv_user_addr.getText());
                startActivityForResult(intent, Constants.ADDR_REQUEST_CODE);


            }

        });

        mBusinessLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //WizRocket tracking
                //Navigate to EditProfile
                Intent intent = new Intent(MyProfile.this, EditProfile.class);
                intent.putExtra("type", "business");
                intent.putExtra("val", tv_user_business.getText());
                startActivityForResult(intent, Constants.BUSINESS_REQUEST_CODE);


            }

        });

        mCityLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListDialog listDialog = new ListDialog(myList, MyProfile.this, locationSelected, "Select your location", locationSelectionListener,true);
                listDialog.show(getSupportFragmentManager(), "listDialog");
            }
        });

        setup_profile_image_listener();
        setupCityDropDown();

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }


    void getUserProfile() {
        if (null != mProgressWheel && !mProgressWheel.isSpinning()) {
            mProgressWheel.spin();
        }
        HashMap<String, String> params = new HashMap<String, String>();
        params = Utility.getInitialParams(PlaBroApi.RT.GETPROFILE, params);

        AppVolley.processRequest(Constants.TASK_CODES.USER_PROFILE, ProfileResponse.class, null, String.format(PlaBroApi.getBaseUrl(MyProfile.this)), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                mProgressWheel.stopSpinning();
                if (response != null) {


                    ProfileResponse profileResponse = (ProfileResponse) response.getResponse();

                    ProfileClass profileClass = profileResponse.getOutput_params().getData();
                    profileClass.saveData();

                    AppController.getInstance().setUserProfile(profileClass);
                    setupProfile(profileClass);

                    mProgressWheel.stopSpinning();
                    //profile updatedBy
                    PBPreferences.setIsProfileEdited(true);

                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                onApiFailure(response, taskCode);
                if (!profileCacheExist) {
                    sv_profile.setVisibility(View.GONE);
                    tv_no_profile.setVisibility(View.VISIBLE);
                }

                mProgressWheel.stopSpinning();

            }
        });


    }

    @Override
    public void onStatusFalse(PBResponse response, int taskCode) {
        super.onStatusFalse(response, taskCode);
        if (!profileCacheExist) {
            sv_profile.setVisibility(View.GONE);
            tv_no_profile.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onTrialVersionExpired(PBResponse response, int taskCode) {
        super.onTrialVersionExpired(response, taskCode);
        showRegistrationCard();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode != RESULT_OK) {
            return;
        }

        switch (requestCode) {

            case SELECT_PICTURE_CODE:
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }

                sendScreenName(R.string.funnel_USER_IMAGE_CAPTURED);
                if (isCamera) {
                    doCrop();

                } else {
                    if (data != null) {
                        mImageCaptureUri = data.getData();

                        doCrop();
                    }

                }
                break;

            case PICK_FROM_CAMERA:
                sendScreenName(R.string.funnel_USER_IMAGE_CAPTURED);
                doCrop();

                break;

            case PICK_FROM_FILE:

                if (data != null) {
                    sendScreenName(R.string.funnel_USER_IMAGE_CAPTURED);
                    mImageCaptureUri = data.getData();

                    doCrop();
                }

                break;


            case CROP_FROM_CAMERA:
                try {
                    if (null == data) return;
                    Bundle extras = data.getExtras();

                    if (extras != null) {
                        photo = extras.getParcelable("data");
                        user_image.setImageBitmap(photo);
                        isProfileImageSelected = true;
                    } else {
                        isProfileImageSelected = false;
                    }

                    File f = new File(mImageCaptureUri.getPath());

                    if (f.exists()) f.delete();
                    sendScreenName(R.string.goal_user_image_cropped);
                    updateProfileImage();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case Constants.WEB_REQUEST_CODE:
                getUserProfile();
                break;

            case Constants.EMAIL_REQUEST_CODE:
                getUserProfile();

                break;

            case Constants.ADDR_REQUEST_CODE:
                getUserProfile();

                break;

            case Constants.NAME_REQUEST_CODE:
                getUserProfile();

                break;

            case Constants.BUSINESS_REQUEST_CODE:
                getUserProfile();

                break;
            default:
                getUserProfile();
                break;

        }


    }

    private void updateProfileImage() {

        final ProgressDialog progress;
        progress = ProgressDialog.show(MyProfile.this, "Please wait",
                "Updating profile...", true);

        progress.setCancelable(true);
        progress.setCanceledOnTouchOutside(true);
        HashMap<String, String> params = new HashMap<String, String>();

        if (null != photo) {
            params.put(PlaBroApi.PLABRO_SET_PREF_POST_PARAM_IMG, getEncodedPicture());
            //image uploaded
            HashMap<String, Object> data = new HashMap<>();
            data.put("ScreenName", TAG);
            data.put("Action", "Upload");

        }
        params = Utility.getInitialParams(PlaBroApi.RT.SETPREF, params);
//        params.put("debug", "3");

        AppVolley.processRequest(Constants.TASK_CODES.SET_PREFS, RegisterResponse.class, null, PlaBroApi.getBaseUrl(MyProfile.this), params, RequestMethod.POST, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);


                RegisterResponse registerResponse = (RegisterResponse) response.getResponse();


                Toast.makeText(MyProfile.this, "Profile Pic Updated Successfully.", Toast.LENGTH_LONG).show();


                Log.i("PlaBro", "Preferences set sussessfully" + registerResponse.isStatus());


            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                onApiFailure(response, taskCode);
                Utility.dismissProgress(progress);

                Log.i("PlaBro", "onFailure : Oops...something went wrong");

                switch (response.getCustomException().getExceptionType()) {
                    case SESSION_EXPIRED:
                        showSessionExpiredDialog(false);
                        break;
                    default:
                        Toast.makeText(MyProfile.this, "Some error occurred. Please try again.", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });


    }

    @Override
    public void onOkClicked(boolean addMore) {
        super.onOkClicked(addMore);
        updateProfileImage();
    }

    private void doCrop() {

        //image cropped
        HashMap<String, Object> data = new HashMap<>();
        data.put("ScreenName", TAG);
        data.put("Action", "Crop");

        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);

        int size = list.size();

        if (size == 0) {
            Toast.makeText(this, "Can not find image crop app", Toast.LENGTH_SHORT).show();

            return;
        } else {
            intent.setData(mImageCaptureUri);

            intent.putExtra("outputX", 400);
            intent.putExtra("outputY", 250);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);
            intent.putExtra("return-data", true);

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));

                startActivityForResult(i, CROP_FROM_CAMERA);
            } else {
                for (ResolveInfo res : list) {
                    final CropOption co = new CropOption();

                    co.title = getPackageManager().getApplicationLabel(res.activityInfo.applicationInfo);
                    co.icon = getPackageManager().getApplicationIcon(res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);

                    co.appIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));

                    cropOptions.add(co);
                }

                CropOptionAdapter adapter = new CropOptionAdapter(getApplicationContext(), cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose Crop App");
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        startActivityForResult(cropOptions.get(item).appIntent, CROP_FROM_CAMERA);
                    }
                });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        try {
                            if (mImageCaptureUri != null) {
                                getContentResolver().delete(mImageCaptureUri, null, null);
                                mImageCaptureUri = null;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                AlertDialog alert = builder.create();

                alert.show();
            }
        }
    }


    //add various image source listener,cropping functionality and image show on page
    void setup_profile_image_listener() {

        //image selected
        HashMap<String, Object> data = new HashMap<>();
        data.put("ScreenName", TAG);
        data.put("Action", "Select");


        //image selector


        final String[] items = new String[]{"Take from camera", "Select from gallery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) { //pick from camera
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(),
                            "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

                    try {
                        intent.putExtra("return-data", true);

                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                } else { //pick from file

                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(Intent.createChooser(i, "Select Image"), PICK_FROM_FILE);

//                    Intent intent = new Intent();
//                    intent.setPackage("com.google.android.apps.plus");
//                    intent.setType("image/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    startActivityForResult(Intent.createChooser(intent, "Select Image"),PICK_FROM_FILE);
                }
            }
        });

        final AlertDialog dialog = builder.create();


        user_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.show();
            }
        });


    }


    public String getEncodedPicture() {


        if (null != photo) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.PNG, 0, baos);
            byte[] b = baos.toByteArray();
            String temp = Base64.encodeToString(b, Base64.DEFAULT);

            return temp;
        } else {
            return "";
        }


    }

    void setupProfile(ProfileClass profileClass) {
        if (null != profileClass) {
            sv_profile.setVisibility(View.VISIBLE);
            mAuthorId = profileClass.getAuthorid();
            String email = profileClass.getEmail();
            String phone = profileClass.getPhone();

            if (!email.equalsIgnoreCase("")) {
                tv_user_mail.setText(email);
            }

            if (!phone.equalsIgnoreCase("")) {
                tv_user_phone.setText(phone);
            }

            mFollowers.setText(profileClass.getFollowed() + " Followers");
            mFollowing.setText(profileClass.getFollowing() + " Following");
            mUserNameDisplay.setText(profileClass.getName());


            String imgUrl = profileClass.getImg();


            if (imgUrl != null) {
                Picasso.with(MyProfile.this).load(imgUrl).placeholder(R.drawable.profile_default_one).into(user_image);
            }


            tv_no_profile.setVisibility(View.GONE);

            Drawable edit = getResources().getDrawable(R.drawable.ic_edit_status);
            Drawable add = getResources().getDrawable(R.drawable.ic_item_add);

            if (!profileClass.getEmail().equalsIgnoreCase("")) {
                tv_user_mail.setText(profileClass.getEmail());
                mImageEditEmail.setImageDrawable(edit);

            } else {
                mImageEditEmail.setImageDrawable(add);
            }

            if (!profileClass.getStatus_msg().equalsIgnoreCase("")) {
                mProfileStatus.setText(profileClass.getStatus_msg());
                mImageEditStatus.setImageDrawable(edit);

            } else {
                mImageEditStatus.setImageDrawable(add);
            }


            if (!profileClass.getWebsite().equalsIgnoreCase("")) {
                tv_user_web.setText(profileClass.getWebsite());
                mImageEditWeb.setImageDrawable(edit);
            } else {
                mImageEditWeb.setImageDrawable(add);
            }

            if (!profileClass.getAddress().equalsIgnoreCase("")) {
                tv_user_addr.setText(profileClass.getAddress());
                mImageEditAddr.setImageDrawable(edit);

            } else {
                mImageEditAddr.setImageDrawable(add);
            }

            if (!profileClass.getBusiness_name().equalsIgnoreCase("")) {
                tv_user_business.setText(profileClass.getBusiness_name());
                mImageEditBusiness.setImageDrawable(edit);

            } else {
                mImageEditBusiness.setImageDrawable(add);
            }

            if (!profileClass.getCity_name().equalsIgnoreCase("")) {
                tv_user_city.setText(profileClass.getCity_name());
                mImageEditCity.setImageDrawable(edit);
                locationSelected = profileClass.getCity_name();


            } else {
                mImageEditCity.setImageDrawable(add);
            }

            mProgressWheel.stopSpinning();
        }
    }


    private void setupCityDropDown() {

        if (null == cityClassList || cityClassList.size() < 1) {
            cityClassList = CityResponse.getCityList();
        }

        myList.clear();

        for (CityClass cityClass : cityClassList) {
            String cityName = cityClass.getCity_name();
            myList.add(cityName);
            globalCityList.put(cityName, cityClass.get_id());
        }


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, myList);

        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);


    }

    LocationSelectionListener locationSelectionListener = new LocationSelectionListener() {
        @Override
        public void onResponse(Boolean status, String response) {
            //Toast.makeText(Preferences.this, response, Toast.LENGTH_LONG).show();
            if (status) {
                isLocationSelected = true;
                locationSelected = response;
                tv_user_city.setText(response);
                updateCity();
            } else {
                isLocationSelected = false;
                locationSelected = "";
            }
        }
    };

    private void updateCity() {
        final ProgressDialog progress;
        progress = ProgressDialog.show(MyProfile.this, "Please wait",
                "Updating city...", true);
        progress.setCancelable(true);

        String city_name = locationSelected;
        String city_id = globalCityList.get(locationSelected);

        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        wrData.put("type", "city");



        HashMap<String, String> params = new HashMap<String, String>();

        if (!TextUtils.isEmpty(city_id)) {
            params.put(PlaBroApi.PLABRO_SHOUT_POST_PARAM_CITY_ID, city_id);
        }

        if (!TextUtils.isEmpty(city_name)) {
            params.put(PlaBroApi.PLABRO_SHOUT_POST_PARAM_CITY_NAME, city_name);
        }

        params = Utility.getInitialParams(PlaBroApi.RT.SETPREF, params);

        AppVolley.processRequest(Constants.TASK_CODES.SET_PREFS, RegisterResponse.class, null, PlaBroApi.getBaseUrl(MyProfile.this), params, RequestMethod.POST, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Utility.dismissProgress(progress);

                RegisterResponse registerResponse = (RegisterResponse) response.getResponse();
                Toast.makeText(MyProfile.this, "Profile updated successfully.", Toast.LENGTH_LONG).show();
                Log.i("PlaBro", "Preferences set sussessfully" + registerResponse.isStatus());
                getUserProfile();
                PBPreferences.saveData(PBPreferences.IS_CITY_UPDATE, true);


            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                onApiFailure(response, taskCode);
                setupProfile(profileClass);
                Utility.dismissProgress(progress);

                Log.i("PlaBro", "onFailure : Oops...something went wrong");
                Toast.makeText(MyProfile.this, "Some error occurred. Please try again.", Toast.LENGTH_LONG).show();

            }
        });
    }

}
