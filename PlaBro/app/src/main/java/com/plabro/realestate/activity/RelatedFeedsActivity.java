package com.plabro.realestate.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.LoaderFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RelatedFeedsActivity extends CardListActivity implements ShoutObserver.PBObserver {

    public static final String TAG = "RelatedFeedsActivity";
    private String message_id = "";
    private String refKey;

    @Override
    protected String getTag() {
        return AppController.getInstance().getResources().getString(R.string.s_related_feeds);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        redirectActivity(getIntent(), FeedDetails.class);
        return;
    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_related_feeds;
    }

    @Override
    public void findViewById() {
        super.findViewById();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ShoutObserver.getInstance().remove(this);
    }

    public void getData() {

        Log.d(TAG, "calling find view by id");
        Bundle bundle = getIntent().getExtras();
        message_id = bundle.getString("message_id");
        refKey = bundle.get("refKey") + "";

        if (addMore && baseFeeds.size() > 0) {
            if (baseFeeds.get(baseFeeds.size() - 1).getType().equalsIgnoreCase(Constants.FEED_TYPE.NO_MORE_DATA)) {
                return;
            }
        }

        showSpinners(addMore);

        Log.d(TAG, "getting related feeds....");
        isLoading = true;


        HashMap<String, String> params = new HashMap<String, String>();

        if (!addMore) {
            params.put("startidx", "0");

        } else {
            params.put("startidx", (page) + "");
        }


        params.put("message_id", message_id);
        params.put("refKey", refKey + "");

        params = Utility.getInitialParams(PlaBroApi.RT.PROPFEEDS, params);

        AppVolley.processRequest(Constants.TASK_CODES.PROPFEEDS, FeedResponse.class, null, String.format(PlaBroApi.getBaseUrl(RelatedFeedsActivity.this)), params, RequestMethod.GET, new VolleyListener() {

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                onApiFailure(response, taskCode);
                mProgressWheel.stopSpinning();
                isLoading = false;
                setAndCheckNoDataLayout();
                onApiFailure(response, taskCode);
                showNoDataDialog(addMore);
                try {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 2000ms
                            LoaderFeed lf = new LoaderFeed();
                            lf.setType(Constants.FEED_TYPE.NO_MORE);

                            if (addMore) {
                                baseFeeds.remove(baseFeeds.size() - 1);//for footer
                                baseFeeds.add(lf);
                                mAdapter.notifyDataSetChanged();
                            }
                        }
                    }, 2000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                mProgressWheel.stopSpinning();
                isLoading = false;
                Log.d(TAG, "Response :" + response);

                FeedResponse feedResponse = (FeedResponse) response.getResponse();

                List<BaseFeed> tempFeeds =
                        (ArrayList<BaseFeed>) feedResponse.getOutput_params().getData();

                if (tempFeeds.size() > 0) {

                    noFeeds.setVisibility(View.GONE);
                    LoaderFeed lf = new LoaderFeed();
                    if (tempFeeds.size() < 10) {
                        lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                    } else {
                        lf.setType(Constants.FEED_TYPE.PROGRESS_MORE);
                    }

                    if (addMore) {

                        baseFeeds.remove(baseFeeds.size() - 1);//for footer
                        baseFeeds.addAll(tempFeeds);
                        baseFeeds.add(lf);
                        mAdapter.notifyDataSetChanged();
                        page = page + 10;
                        HashMap<String, Object> data = new HashMap<>();
                        data.put("ScreenName", TAG);
                        data.put("Action", "Manual");
                    } else {

                        baseFeeds.clear();
                        baseFeeds.addAll(tempFeeds);
                        if (baseFeeds.size() > 10) {
                            baseFeeds.add(lf);
                        }
                        page = 10;
                        mRecyclerView.setAdapter(mAdapter);
                        if (mAdapter != null) {
                            mAdapter.notifyDataSetChanged();
                            // mAdapter.addItems(baseFeeds);
                        }

                    }

                } else {
                    if (addMore) {
                        LoaderFeed lf = new LoaderFeed();
                        baseFeeds.remove(baseFeeds.size() - 1);//for footer
                        lf.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                        baseFeeds.add(lf);
                        mAdapter.notifyDataSetChanged();
                    }
                }


                setAndCheckNoDataLayout();
            }


        });


    }

}
