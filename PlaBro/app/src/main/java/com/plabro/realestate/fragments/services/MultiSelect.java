package com.plabro.realestate.fragments.services;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.plabro.realestate.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 24/11/15.
 */
public class MultiSelect extends BaseServiceFragment {
    private List<String> choices = new ArrayList<>();

    @Override
    public String getTAG() {
        return "Multi Select";
    }

    private View getLayout() {
        LinearLayout v = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.layout_service_linear_content, null);
        choices.clear();
        choices.addAll(q.getChoices());
        for (int i = 0; i < choices.size(); i++) {
            v.addView(getSelectorView(choices.get(i)));
        }
        return v;
    }

    private View getSelectorView(String text) {
        CheckBox b = (CheckBox) LayoutInflater.from(getActivity()).inflate(R.layout.layout_multi_select, null);
        b.setText(text);
        return b;
    }

}
