package com.plabro.realestate.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.crashlytics.android.Crashlytics;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.activity.SearchFeeds;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.adapter.CustomPagerAdapter;
import com.plabro.realestate.drawer.MaterialNavigationDrawer;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.fragments.services.ServicesFragment;
import com.plabro.realestate.listeners.GenericObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.chat.ChatMessage;
import com.plabro.realestate.models.city.CityClass;
import com.plabro.realestate.models.history.History;
import com.plabro.realestate.models.login.Login;
import com.plabro.realestate.models.propFeeds.tabs.TabInfo;
import com.plabro.realestate.models.propFeeds.tabs.TabsInfoResponse;
import com.plabro.realestate.models.services.ServiceResponse;
import com.plabro.realestate.models.services.Widget;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.receivers.PBLocalReceiver;
import com.plabro.realestate.uiHelpers.Utils2;
import com.plabro.realestate.uiHelpers.ViewPagerIndicator;
import com.plabro.realestate.uiHelpers.slidingTab.SlidingTabLayout;
import com.plabro.realestate.uiHelpers.view.FontManager;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.JSONUtils;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.CustomMenuIcons;
import com.plabro.realestate.widgets.dialogs.UserRegistrationDialog;
import com.quinny898.library.persistentsearch.SearchResult;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by nitin on 10/08/15.
 */
public class HomeFragment extends MasterFragment implements CustomPagerAdapter.PagerAdapterInterface<String>, ViewPager.OnPageChangeListener, CustomListAdapter.CustomListAdapterInterface, VolleyListener {

    private ViewPagerIndicator pageIndicator;
    private ViewPager mViewPager;
    private ArrayList<String> filterTypes = new ArrayList<>();
    private CustomPagerAdapter<String> adapter;
    private ArrayList<CityClass> cities = new ArrayList<>();

    private SlidingTabLayout mSlidingTabLayout;
    private CustomListAdapter<CityClass> spinnerAdapter;
    private Spinner spinner;
    CityRefreshListener cityRefreshListener;
    private List<Widget> loadedWidgets = new ArrayList<>();

    private static final String TYPEFACE_FILENAME = "Fonts/Roboto-Medium.ttf";
    private static final String TYPEFACE_ROBOTTO_NORMAL = "Fonts/Roboto-Regular.ttf";

    private static Toolbar toolbar;

    private int chatCount = 0;
    private int badgeCount = 0;
    private ProgressDialog dialog;
    private HashMap<String, String> rtVsTabs = new HashMap<>();
    private UserRegistrationDialog feedPopUpDialog;
    private FragmentManager mFragmentManager;
    private int currentItem;
    private static SearchBoxCustom search;
    List<History> historiesFiltered = new ArrayList<>();
    List<String> historyArray = new ArrayList<>();
    private FeedsFragment feedsFragment;
    private BuilderFragment builderFragment;
    private Menu menu;


    public static HomeFragment getInstance(Toolbar bar, SearchBoxCustom searchbox) {
        toolbar = bar;
        search = searchbox;
        return new HomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerLocalReceiver();
        registerObserver(GenericObserver.GenericObserverInterface.EVENT_AUTHKEY_UPDATE);
        getSearchHistory();
//        if (PBPreferences.getData(PBPreferences.REQUEST_JSON_SHARE, true)) {
        getJsonShare();
//        }
        resetDefaults();

    }

    private void resetDefaults() {
        PBPreferences.setPostSearchStatus(false);
    }

    @Override
    public IntentFilter getFilter() {
        IntentFilter filter = super.getFilter();
        filter.addAction(Constants.ACTION_CHAT_COUNT);
        filter.addAction(Constants.ACTION_BADGE_COUNT);
        return filter;
    }

    private void getSearchHistory() {
        historiesFiltered.clear();
        historyArray.clear();
        search.clearSearchable();
        try {
            historiesFiltered = new Select().from(History.class).orderBy("text DESC ").execute();
            for (int x = 0; x < historiesFiltered.size(); x++) {
                String val = historiesFiltered.get(x).getText();
                historyArray.add(val);
                SearchResult option = new SearchResult(val, getResources().getDrawable(
                        R.drawable.ic_history));
                search.addSearchable(option);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void setUpViewPager() {
        if (null == getActivity()) return;
        mViewPager = (ViewPager) findView(R.id.pager);
        adapter = new CustomPagerAdapter<>(getChildFragmentManager(), filterTypes, this);
        mViewPager.removeAllViewsInLayout();
        mViewPager.setAdapter(adapter);
        mViewPager.setOnPageChangeListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Fragment getFragmentItem(int position, String listItem) {
        Fragment f = null;
        String key = getKey(listItem);
        switch (key) {
            default:
            case PlaBroApi.RT.PROPFEEDS:
                Log.d(TAG, "Feeds Fragment");
                f = FeedsFragment.getInstance(new Bundle());
                feedsFragment = (FeedsFragment) f;
                break;
            case PlaBroApi.RT.BUILDER_FEED:
                Log.d(TAG, "Builder Fragment");
                f = BuilderFragment.getInstance(new Bundle());
                builderFragment = (BuilderFragment) f;
                break;
            case PlaBroApi.RT.JSON_SHARE:
            case PlaBroApi.RT.GET_PERSONAL_PAGE:
                Log.d(TAG, "Builder Fragment");
                f = new ServicesFragment();
        }
        cityRefreshListener = (CityRefreshListener) f;
        return f;
    }

    private String getKey(String value) {
        if (rtVsTabs.containsValue(value)) {
            for (Map.Entry<String, String> entry : rtVsTabs.entrySet()) {
                if (entry.getValue().equalsIgnoreCase(value)) {
                    return entry.getKey();
                }
            }
        }
        return "";
    }

    @Override
    public CharSequence getPageTitle(int position, String listItem) {
        return listItem;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    private void setTabTextView() {
        int count = mSlidingTabLayout.getTabStrip().getChildCount();
        int currentPosition = mViewPager.getCurrentItem();
        for (int i = 0; i < count; i++) {
            TextView v = (TextView) mSlidingTabLayout.getTabStrip().getChildAt(i);
            Typeface tf = FontManager.getInstance().getFont(TYPEFACE_ROBOTTO_NORMAL);
            v.setTypeface(tf, Typeface.BOLD);
            if (i == currentPosition)
                v.setTextColor(getResources().getColor(R.color.white));
            else
                v.setTextColor(getResources().getColor(R.color.tab_text_blue));
        }
    }

    @Override
    public void onPageSelected(int position) {
        Log.d(TAG, "" + position);
        mViewPager.setCurrentItem(position);
        setTabTextView();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public String getTAG() {
        return "Home Fragment";
    }


    @Override
    protected void findViewById() {
        Log.d(TAG, "OnCreateView");
        rtVsTabs.clear();
        rtVsTabs.put(PlaBroApi.RT.PROPFEEDS, "PropertyFeeds");
        rtVsTabs.put(PlaBroApi.RT.BUILDER_FEED, "Projects");
        rtVsTabs.put(PlaBroApi.RT.GET_PERSONAL_PAGE, "PersonalPage");
        rtVsTabs.put(PlaBroApi.RT.JSON_SHARE, "Services");
        if (null != toolbar) {
            inflateToolbar();
        }
        mSlidingTabLayout = (SlidingTabLayout) findView(R.id.navig_tab);
        List<TabInfo> tabs = new Select().from(TabInfo.class).execute();
        if (null != tabs && tabs.size() > 0) {
            setTabs(tabs);
            if (PBPreferences.getData(PBPreferences.IS_CITY_UPDATE, false)) {
                getTabsInfo();
            }
        } else {
            filterTypes.clear();
            filterTypes.add("Property Feeds");
            setTabViews();
            getTabsInfo();
        }
        if (PBPreferences.getData(PBPreferences.CHECK_BADGE_COUNT, false)) {
            getNotificationCount();
        }
        setBadgeCount(R.id.action_featured, 3, R.id.ic_badge);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "on Start");
    }

    private void setTabViews() {
//        mSlidingTabLayout.setViewPager(null);
        mSlidingTabLayout.setVisibility(View.VISIBLE);
        setUpViewPager();
//        mSlidingTabLayout.removeAllViews();
        mSlidingTabLayout.setViewPager(mViewPager);
        if (filterTypes.size() < 2) {
            mSlidingTabLayout.setVisibility(View.GONE);
        } else {
            setTabTextView();
        }
    }

    private void getTabsInfo() {
        HashMap<String, String> params = new HashMap<>();
        params.put(Constants.CITY_ID, AppController.getInstance().getCityId());
        params = Utility.getInitialParams(PlaBroApi.RT.GET_TABS_INFO, params);
        AppVolley.processRequest(Constants.TASK_CODES.GET_TABS_INFO, TabsInfoResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                if (null == getActivity()) return;
                TabsInfoResponse res = (TabsInfoResponse) response.getResponse();
                if (null != res.getOutput_params() && null != res.getOutput_params().getData()) {
                    List<TabInfo> tabs = res.getOutput_params().getData();

                    Log.d(TAG, "Tab Size :" + tabs.size());
                    if (null != tabs && tabs.size() > 0) {
                        currentItem = mViewPager.getCurrentItem();

                        if (checkIfNewTabsAreDifferent(tabs)) {
                            setTabs(tabs);
                        } else {
                            boolean isCityUpdated = PBPreferences.getData(PBPreferences.IS_CITY_UPDATE, false);
                            Log.d(TAG, "IsCityUpdated :" + isCityUpdated);
                            if (isCityUpdated) {
                                if (null != feedsFragment) {
                                    Log.d(TAG, "Refresh Listener not Null");
                                    feedsFragment.onCityRefresh();

                                }
                                if (null != builderFragment) {
                                    builderFragment.onCityRefresh();
                                }
                                PBPreferences.saveData(PBPreferences.IS_CITY_UPDATE, false);
                            }
                        }
                    }
                } else {

                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                onApiFailure(response, taskCode);
            }
        });
    }

    private boolean checkIfNewTabsAreDifferent(List<TabInfo> tabs) {
        HashSet<String> tabSet = new HashSet<>();
        for (TabInfo t : tabs) {
            if (t.getEnable() == 1)
                tabSet.add(t.getTag());
        }
        if (tabSet.size() > 0) {
            if (tabSet.size() != filterTypes.size()) {
                return true;
            }
            for (String s : filterTypes) {
                if (tabSet.contains(s)) {

                } else {
                    return true;
                }
            }

        }
        return false;
    }

    private void setTabs(List<TabInfo> tabs) {
        filterTypes.clear();
        new Delete().from(TabInfo.class).execute();
        for (TabInfo tab : tabs) {
            tab.save();
            if (tab.getEnable() == 1 && rtVsTabs.containsKey(tab.getRT())) {
                filterTypes.add(tab.getTag());
            }

        }
        setTabViews();
        if (null != feedsFragment) {
            feedsFragment.onCityRefresh();
        }
        if (null != builderFragment) {
            builderFragment.onCityRefresh();
        }
        if (currentItem < mSlidingTabLayout.getTabStrip().getChildCount()) {
            mViewPager.setCurrentItem(currentItem);
        }
    }

    private void getCities() {
        cities.clear();
        cities.addAll(CityClass.getCitiesList());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (null != search && search.isShown()) {
            search.hideCircularly(getActivity());
        }
        getSearchHistory();

    }

    private void setSpinnerAdapter() {
        getCities();
        spinnerAdapter = new CustomListAdapter<CityClass>(getActivity(), R.layout.item_spinner, cities, this);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textView = (TextView) view.findViewById(R.id.tv_city);
                textView.setTextColor(getResources().getColor(R.color.white));
                String selectedCity = PBPreferences.getData(PBPreferences.SELECTED_CITY_ID, null);
                if (null != selectedCity && selectedCity.equals(cities.get(i).get_id())) {

                } else {
                    PBPreferences.saveData(PBPreferences.SELECTED_CITY_ID, cities.get(i).get_id());
                    cityRefreshListener.onCityRefresh();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });
    }

    @Override
    protected int getViewId() {
        return R.layout.activity_search_filter;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent, int resourceID) {
        TextView textView = (TextView) LayoutInflater.from(getActivity()).inflate(resourceID, null);
        textView.setText(cities.get(position).getCity_name());
        textView.setTextColor(getResources().getColor(R.color.black));
        return textView;
    }


    public static interface CityRefreshListener {
        public void onCityRefresh();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    private void showDialog() {
        if (dialog == null) {
            dialog = new ProgressDialog(getActivity());
        }
        dialog.setMessage("Loading...");
        dialog.show();
    }

    private void hideDialog() {
        if (null != dialog && dialog.isShowing()) {
            dialog.dismiss();
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        updateChatCount();
        toolbar.getMenu().clear();
        inflateToolbar();
//        setBadgeCount(toolbar.getMenu(), R.id.action_featured, badgeCount, R.id.ic_badge);
//        setBadgeCount(toolbar.getMenu(), R.id.action_chat, chatCount, R.id.ic_chat_badge);
        // Get the notifications MenuItem and LayerDrawable (layer-list)
    }

    private int updateChatCount() {
        final String unreadMSG = new Select().from(ChatMessage.class).where("status='" + ChatMessage.STATUS_RECEIVED + "' and msgType = " + ChatMessage.MESSAGE_INCOMING).execute().size() + "";
        if (null != unreadMSG && !TextUtils.isEmpty(unreadMSG) && !unreadMSG.equalsIgnoreCase("0")) {
            chatCount = Integer.parseInt(unreadMSG);
        } else {
            chatCount = 0;
        }
        return chatCount;

    }

    private void setBadgeCount(Menu menu, int menuIconId, int count, int iconId) {
        if (true) return;
        MenuItem item = null;
        this.menu = menu;
        if (this.menu != null) {
            item = this.menu.findItem(menuIconId);
        } else {
            Log.d(TAG, "Menu is null");
        }
        try {
            if (null != item) {
                LayerDrawable icon = (LayerDrawable) item.getIcon();
                // Update LayerDrawable's BadgeDrawable
                Utils2.setBadgeCount(getActivity(), icon, count, iconId);
            } else {
                Log.d(TAG, "Item is null");
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    private void setBadgeCount(int menuIconId, int count, int iconId) {
        setBadgeCount(this.menu, menuIconId, count, iconId);
    }

    private void openFragment(int sectionIndex) {
        MaterialNavigationDrawer act = ((MaterialNavigationDrawer) getActivity());
        if (null != act && null != act.getSectionList() && null != act.getSectionList().get(sectionIndex)) {
            act.onClick(act.getSectionList().get(sectionIndex));
        }
    }

    private void inflateToolbar(final List<Widget> loadedWidgets) {
        Menu menu = toolbar.getMenu();
        if (menu != null)
            menu.clear();
        toolbar.inflateMenu(R.menu.menu_guided_tour);
        menu = toolbar.getMenu();

        for (int i = 0; i < loadedWidgets.size(); i++) {
            MenuItem item = menu.add(0, i, i, loadedWidgets.get(i).getText());
            item.setIcon(loadedWidgets.get(i).getDrawable());
            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    Log.d(TAG, "Menu Item Clicked: ID:" + menuItem.getItemId());
                    return false;
                }
            });
        }

//        return true;
    }


    private void inflateToolbar() {
        Log.v(TAG, "toolbar inflated");
        toolbar.inflateMenu(R.menu.menu_home);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id) {

                    case R.id.action_featured:
                        openFragment(Constants.SectionIndex.NOTIFICATIONS);
                        badgeCount = 0;
                        break;
                    case R.id.action_chat:
                        openFragment(Constants.SectionIndex.CHATS);
                        break;
                    case R.id.action_search:
                        try {
                            int val = PBPreferences.getTrialVersionState();
                            if (val == 0) {
                                Login login = new Select().from(Login.class).executeSingle();
                                if (null != login) {
                                    val = login.getTrial_version();
                                }
                            }
                            Log.d(TAG, "VErsion:" + val);
                            inner:
                            switch (val) {
                                case 0:
                                    Utility.userLogin(getActivity());

                                    mFragmentManager = getChildFragmentManager();
                                    feedPopUpDialog = new UserRegistrationDialog(getActivity(), val);
                                    feedPopUpDialog.show(mFragmentManager, "UserRegister");//todo illegal state exception
                                    break inner;
                                case 1:
                                    mFragmentManager = getChildFragmentManager();
                                    feedPopUpDialog = new UserRegistrationDialog(getActivity(), val);
                                    feedPopUpDialog.show(mFragmentManager, "UserRegister");
                                    break inner;

                                case 2:
                                    //lets tell our tooltip zoo zoo that searchtip ship has sailed
                                    Set<String> indexSet = PBPreferences.getHomeToolTipSet();
                                    indexSet.add("searchTip");
                                    PBPreferences.setHomeToolTipSet(indexSet);
                                    openSearch(id);

                                    break inner;

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        break;


                    case R.id.action_feed_map:

                        HashMap<String, Object> wrData = new HashMap<>();
                        wrData.put("ScreenName", TAG);

                        Analytics.trackEventWithProperties(R.string.consumption, R.string.e_map_clicked, wrData);

                        Toast.makeText(getActivity(), "Feature coming soon.", Toast.LENGTH_LONG).show();

                        //lets tell our tooltip zoo zoo that mapTip ship has sailed
                        Set<String> indexSetm = PBPreferences.getHomeToolTipSet();
                        indexSetm.add("mapTip");
                        PBPreferences.setHomeToolTipSet(indexSetm);

                        break;

                    default:
                        break;


                }
                return true;
            }

        });
//        mToolbar.setSubtitle("in "+LocationDetails.getInstance().getCityName());
        if (isRunning && ((PlabroBaseActivity) getActivity()) != null) {
            ((PlabroBaseActivity) getActivity()).setTitle(AppController.getInstance().getCityName());
        }
    }

    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Profile Action Received" + intent.getAction());
        if (Constants.ACTION_PROFILE_UPDATE.equals(intent.getAction())) {
            ((PlabroBaseActivity) getActivity()).setTitle(AppController.getInstance().getCityName());
            boolean isCityUpdated = PBPreferences.getData(PBPreferences.IS_CITY_UPDATE, false);
            if (isCityUpdated)
                getTabsInfo();
        }
        if (PBLocalReceiver.PBActionListener.ACTION_REGISTRATION_UPDATE.equals(intent.getAction())) {
            dismissCard();
        }
        if (Constants.ACTION_CHAT_COUNT.equals(intent.getAction())) {
            chatCount = intent.getExtras().getInt(Constants.BUNDLE_KEYS.CHAT_COUNT, 0);
            getActivity().invalidateOptionsMenu();
        }
        if (Constants.ACTION_BADGE_COUNT.equals(intent.getAction())) {
            getNotificationCount();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
//        if (true) {
//            inflateToolbar(loadedWidgets);
//            return;
//        }

        Log.d(TAG, "On Prepare Options menu");
        if (menu != null) {
            menu.clear();
        }

        updateChatCount();
        toolbar.getMenu().clear();
        inflateToolbar();
        PBPreferences.saveData(PBPreferences.NOTIF_BADGE_COUNT, badgeCount);
        PBPreferences.saveData(PBPreferences.CHAT_BADGE_COUNT, chatCount);
//        setBadgeCount(toolbar.getMenu(), R.id.action_featured, badgeCount, R.id.ic_badge);
//        setBadgeCount(toolbar.getMenu(), R.id.action_chat, chatCount, R.id.ic_chat_badge);
//        PBLocalReceiver.sendBadgeUpdateBroadcast(getActivity());
        super.onPrepareOptionsMenu(menu);
    }

    private void getNotificationCount() {
        ParamObject obj = new ParamObject();
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.GET_NOTIFICATIONS_BADGE, null));

        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                getActivity().invalidateOptionsMenu();
                Log.d(TAG, "Response" + response.getResponse());
                PBPreferences.saveData(PBPreferences.CHECK_BADGE_COUNT, false);
                try {
                    JSONObject object = JSONUtils.getJSONObjectFromString(response.getResponse().toString());
                    badgeCount = object.getJSONObject("output_params").getJSONObject("data").getInt("not_count");

                    getActivity().invalidateOptionsMenu();
                } catch (Exception e) {

                }

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegsiterLocalReceiver();

    }

    @Override
    public void onEventUpdate(String event) {
        Log.d(TAG, "ONEventUpdate:" + event);
        if (GenericObserver.GenericObserverInterface.EVENT_AUTHKEY_UPDATE.equals(event)) {
            if (PBPreferences.getData(PBPreferences.IS_CITY_UPDATE, false)) {
                getTabsInfo();
            }
        }
    }

    public void openSearch(final int id) {
        //toolbar.setTitle("");
        if (search.getSearchables().size() < 1) {
            getSearchHistory();
        }
        search.enableVoiceRecognition(this);
        search.revealFromMenuItem(R.id.action_search, getActivity());
        search.setMenuListener(new SearchBoxCustom.MenuListener() {

            @Override
            public void onMenuClick() {
                if (search.isShown()) {
                    search.hideCircularly(getActivity());
                }

            }

        });
        search.setSearchListener(new SearchBoxCustom.SearchListener() {


            @Override
            public void onSearchOpened() {
            }

            @Override
            public void onSearchClosed() {
                // Use this to un-tint the screen
            }

            @Override
            public void onSearchTermChanged(String term) {
                // React to the search term changing
                // Called after it has updated results
            }


            @Override
            public void onSearch(String searchTerm) {

                if (searchTerm.equalsIgnoreCase("Clear history")) {
                    clearHistory();
                    search.clearSearchable();
                    search.setSearchString("");
                    historyArray.clear();
                    historiesFiltered.clear();

                } else {
                    insertHistory(searchTerm);
                    Intent intent = new Intent(getActivity(), SearchFeeds.class);
                    intent.putExtra("message_id", id);
                    if (null != mViewPager && mViewPager.getCurrentItem() == 1) {
                        intent.putExtra(Constants.BUNDLE_KEYS.IS_BUILDER_SEARCH, true);
                    }
                    intent.putExtra("searchText", searchTerm);
                    getActivity().startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    if (null != search && search.isShown()) {
                        search.hideCircularly(getActivity());
                    }
//                Toast.makeText(getActivity(), searchTerm + " Searched",
//                        Toast.LENGTH_LONG).show();
//                toolbar.setTitle(searchTerm);
                }
            }

            @Override
            public void onSearchCleared() {

            }

        });

    }

    private void clearHistory() {
        new Delete().from(History.class).execute();
    }

    private void insertHistory(String searchTerm) {

        if (!historyArray.contains(searchTerm) && !TextUtils.isEmpty(searchTerm)) {
            History history = new History();
            history.setText(searchTerm);
            history.save();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (isAdded() && requestCode == SearchBoxCustom.VOICE_RECOGNITION_CODE && resultCode == getActivity().RESULT_OK) {
            ArrayList<String> matches = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            search.populateEditText(matches);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getJsonShare() {
        ParamObject obj = new ParamObject();
        obj.setClassType(ServiceResponse.class);
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.JSON_SHARE, null));
        AppVolley.processRequest(obj, this);
    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
// Response for JSON_SHARE
        try {
            ServiceResponse serviceResponse = (ServiceResponse) response.getResponse();
            String eJabberUrl = serviceResponse.getOutput_params().getData().getUrls().getEjabber().getUrl();
            String payment = serviceResponse.getOutput_params().getData().getUrls().getPayment().getUrl();
            String bidsUrl = serviceResponse.getOutput_params().getData().getUrls().getMyBids().getUrl();
            String bidsExitUrl = serviceResponse.getOutput_params().getData().getUrls().getMyBids().getExit_url();
            int pageCount = (int) serviceResponse.getOutput_params().getData().getCall_log().getNum_calls();
            Log.d(TAG, "Page Count:" + pageCount);
            PBPreferences.saveData(PBPreferences.CHAT_URL, eJabberUrl);
            PBPreferences.saveData(PBPreferences.START_IDX, pageCount);
            PBPreferences.saveData(PBPreferences.PAYMENT_URL, payment);
            PBPreferences.saveData(PBPreferences.MY_BIDS_URL, bidsUrl);
            PBPreferences.saveData(PBPreferences.MY_BIDS_EXIT_URL, bidsExitUrl);
            PBPreferences.saveData(PBPreferences.REQUEST_JSON_SHARE, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {

    }
}
