package com.plabro.realestate.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.PersistenceSearch.SearchBoxCustom;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.PlabroBaseActivity;
import com.plabro.realestate.activity.Shout;
import com.plabro.realestate.adapter.ShoutsCustomAdapter;
import com.plabro.realestate.entities.MyShoutsDBModel;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.HidingScrollListener;
import com.plabro.realestate.listeners.OnChildListener;
import com.plabro.realestate.listeners.ShoutObserver;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.FeedResponse;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.software.shell.fab.ActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ShoutsFragment extends MasterFragment implements ShoutObserver.PBObserver {

    ShoutsCustomAdapter mAdapter;
    RecyclerView mRecyclerView;
    ProgressWheel mProgressWheelLoadMore;
    LinearLayoutManager mLayoutManager;
    RelativeLayout noFeeds;
    private RelativeLayout shout;
    private ActionButton mShoutButton;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private ArrayList<Feeds> feeds2 = new ArrayList<Feeds>();
    private static Toolbar mToolbar;
    private static SearchBoxCustom search;
    private int page = 0;
    private boolean isLoading = false;
    public static final String TAG = "ShoutsFragment";
    static Context mContext;


    public static ShoutsFragment newInstance(Context mContext, Activity mActivity) {

        ShoutsFragment.mContext = mContext;
        ShoutsFragment mFeedsFragment = new ShoutsFragment();
        return mFeedsFragment;
    }

    public static ShoutsFragment getInstance(Context mContext, Toolbar mToolbar, SearchBoxCustom searchbox) {
        Log.i(TAG, "Creating New Instance");
        ShoutsFragment mShoutsFragment = new ShoutsFragment();
        ShoutsFragment.mContext = mContext;
        ShoutsFragment.mToolbar = mToolbar;
        search = searchbox;

        return mShoutsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShoutObserver.getInstance().addListener(this);
        registerLocalReceiver();
    }

    @Override
    protected int getViewId() {
        return R.layout.shouts_view_frag;
    }

    private void setListAdapter() {
        int pos;
        feeds2 = getFeedsFromDB();
        mAdapter = new ShoutsCustomAdapter(getActivity(), feeds2, getActivity(), updateShouts);
        mRecyclerView.setAdapter(mAdapter);
        if (mAdapter.getItemCount() > 0) {
            noFeeds.setVisibility(View.GONE);
        } else {
            noFeeds.setVisibility(View.VISIBLE);
        }
    }

    private ArrayList<Feeds> getFeedsFromDB() {
        List<MyShoutsDBModel> dbFeeds = new Select().from(MyShoutsDBModel.class).execute();
        return (ArrayList<Feeds>) MyShoutsDBModel.getMyShouts(dbFeeds);
    }


    public void setRecyclerViewLayoutManager() {
        int scrollPosition = 0;

        mLayoutManager = new LinearLayoutManager(getActivity());

        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);

    }


    public void getData(final boolean addMore) {
        mSwipeRefreshLayout.setRefreshing(true);
        isLoading = true;
        HashMap<String, String> params = new HashMap<String, String>();
        if (!Util.haveNetworkConnection(getActivity())) {

            showNoDataDialog(addMore);
            return;
        } else if (addMore) {
            mProgressWheelLoadMore.spin();
            params.put("startidx", (page) + "");
        } else {
            params.put("startidx", "0");
        }
        params = Utility.getInitialParams(PlaBroApi.RT.SHOUTS, params);
        AppVolley.processRequest(Constants.TASK_CODES.SHOUTS
                , FeedResponse.class, null, String.format(PlaBroApi.getBaseUrl(getActivity())), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                stopSpinners();
                FeedResponse feedResponse;
                feedResponse = (FeedResponse) response.getResponse();
                if (feedResponse.isStatus()) {
                    if (!addMore) {
                        new Delete().from(MyShoutsDBModel.class).execute();
                    }
                    ArrayList<BaseFeed> tempBaseFeeds =
                            (ArrayList<BaseFeed>) feedResponse.getOutput_params().getData();
                    ArrayList<Feeds> tempFeeds = new ArrayList<Feeds>();
                    for (BaseFeed f : tempBaseFeeds) {
                        if ("shout".equals(f.getType()))
                            tempFeeds.add((Feeds) f);
                    }


                    for (BaseFeed feed : tempFeeds) {
                        MyShoutsDBModel.insertFeed(feed);
                    }

                    Log.d(TAG, "Shout Count :" + mAdapter.getItemCount());
                    if (addMore) {
                        feeds2.addAll(tempFeeds);
//                        mAdapter.notifyDataSetChanged();
                    } else {
                        page = 0;
                        feeds2.clear();
                        feeds2.addAll(tempFeeds);
//                        if (mAdapter != null) {
//                            mAdapter.notifyDataSetChanged();
//                        }

                    }
                    page = Util.getStartIndex(page);//to do check paging index :hemant
                    Log.d(TAG, "Shout Count :" + mAdapter.getItemCount());
                    mAdapter.updateItems(tempFeeds, addMore);
                    Log.d(TAG, "Shout Count :" + mAdapter.getItemCount());
                    showNoFeeds();

                    tempFeeds.clear();
                    mAdapter.filter(mAdapter.getSearchText());
                    Log.d(TAG, "Shout Count :" + mAdapter.getItemCount());
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                if (null == getActivity()) return;
                stopSpinners();
                onApiFailure(response, taskCode);
            }


        });

    }

    private void showNoFeeds() {
        if (mAdapter.getCurrentItemSize() > 0) {
            noFeeds.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            noFeeds.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStatusFalse(PBResponse response, int taskCode) {
        super.onStatusFalse(response, taskCode);
        showNoFeeds();
    }

    @Override
    public void onTrialVersionExpired(PBResponse response, int taskCode) {
        super.onTrialVersionExpired(response, taskCode);
        showNoFeeds();

    }

    View.OnClickListener MyOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.rl_post_iv:
                    openShoutActivity();
                    break;

            }

        }
    };

    private void openShoutActivity() {

        //WizRocket tracking
        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        Analytics.trackEventWithProperties(R.string.consumption, R.string.e_shout, wrData);

        Intent intent = new Intent(getActivity(), Shout.class);
        startActivity(intent);
        ((Activity) mContext).overridePendingTransition(R.anim.bottom_in_fast, R.anim.bottom_out_fast);

    }


    @Override
    public String getTAG() {
        return "ShoutsFragment";
    }

    @Override
    protected void findViewById() {
        Analytics.trackScreen(Analytics.OtherScreens.MyShouts);
        page = 0;
        isLoading = false;

        rootView.setTag(TAG);

        //attaching on scroll floating button
        shout = (RelativeLayout) findView(R.id.rl_compose_shout);

        mShoutButton = (ActionButton) shout.findViewById(R.id.rl_post_iv);
        Utility.setupShoutActionButton(mShoutButton, mContext);
        mShoutButton.setOnClickListener(MyOnClickListener);


        //mFeedsFragmentView.findViewById(R.id.rl_bg_shout).setOnClickListener(MyOnClickListener);

        mRecyclerView = (RecyclerView) findView(R.id.recyclerView);
        setRecyclerViewLayoutManager();

        mProgressWheelLoadMore = (ProgressWheel) findView(R.id.progress_wheel_load_more);
        mProgressWheelLoadMore.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheelLoadMore.setProgress(0.0f);

        noFeeds = (RelativeLayout) findView(R.id.noFeeds);
        noFeeds.setVisibility(View.GONE);

        mRecyclerView.setOnScrollListener(new HidingScrollListener(20) {
            @Override
            public void onHide(RecyclerView recyclerView, int dx, int dy) {
            }

            @Override
            public void onShow(RecyclerView recyclerView, int dx, int dy) {

            }

            @Override
            public void onScrolledFire(RecyclerView recyclerView, int dx, int dy) {

//                //WizRocket tracking
//                WizRocket.getInstance(getActivity()).recordEvent(getActivity().getResources().getString(R.string.e_scroll));
//                //GA Event Tracker
//                Analytics.trackEvent(getActivity(), getActivity().getResources().getString(R.string.consumption), getActivity().getResources().getString(R.string.e_scroll), getActivity().getResources().getString(R.string.s_my_shouts_lst));


                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();

                if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                    //reached bottom show loading
                    mProgressWheelLoadMore.setVisibility(View.VISIBLE);

                } else {
                    mProgressWheelLoadMore.setVisibility(View.GONE);

                }

                if (!isLoading) {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount - Constants.SCROLL_ITEM_COUNT) {
                        getData(true);
                    }
                }

            }
        });


        mRecyclerView.setVisibility(View.VISIBLE);
        setListAdapter();
//        } else {
//            ((ViewGroup) rootView.getParent()).removeView(rootView);
//        }


        //pull to refresh
        mSwipeRefreshLayout = (SwipeRefreshLayout) findView(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        page = 0;
                        mSwipeRefreshLayout.setRefreshing(false);
                        mProgressWheelLoadMore.stopSpinning();

                        getData(false);

                    }
                }, Constants.GOOGLE_PULL_TO_REFRESH_TIMER);

            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        getData(false);


    }

    @Override
    public void onResume() {
        super.onResume();

        if (null != search && search.isShown()) {
            search.hideCircularly(getActivity());
        }
        //mAdapter = new ShoutsCustomAdapter(getActivity(), feeds2, getActivity(), updateShouts);

        Boolean refreshMyShouts = PBPreferences.getShoutState();

        if (refreshMyShouts) {

            PBPreferences.setShoutState(false);
            //page = 0;
            getData(false);

        }
    }


    OnChildListener updateShouts = new OnChildListener() {
        @Override
        public void onResponse(Boolean response) {
            // mAdapter = new ShoutsCustomAdapter(getActivity(), feeds2, getActivity(), updateShouts);
            // page = 0;
            getData(false);

        }
    };


    public void updateShoutsFromOutside() {
        // mAdapter = new ShoutsCustomAdapter(getActivity(), feeds2, getActivity(), updateShouts);

        //page = 0;
        getData(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflateToolbar(menu);

    }

    private void inflateToolbar(Menu menu) {
        mToolbar.inflateMenu(R.menu.menu_contacts);
        Log.v(TAG, "toolbar inflated");

        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.action_search:
                        openSearch();
                        break;

                    default:
                        break;

                }
                return true;
            }

        });
    }


    public void openSearch() {
        search.clearSearchable(); // clear all the searchable history previously present

        search.revealFromMenuItem(R.id.action_search, getActivity());
        search.setMenuListener(new SearchBoxCustom.MenuListener() {

            @Override
            public void onMenuClick() {
                if (search.isShown()) {
                    search.hideCircularly(getActivity());
                }

            }

        });
        search.setSearchListener(new SearchBoxCustom.SearchListener() {


            @Override
            public void onSearchOpened() {
                // Use this to tint the screen

            }

            @Override
            public void onSearchClosed() {
                // Use this to un-tint the screen
                if (mAdapter != null) {
                    mAdapter.filter("");
                }
            }

            @Override
            public void onSearchTermChanged(String searchTerm) {
                // React to the search term changing
                // Called after it has updated results
                if (mAdapter != null) {
                    mAdapter.filter(searchTerm);
                }
            }


            @Override
            public void onSearch(String searchTerm) {

                if (mAdapter != null) {
                    mAdapter.filter(searchTerm);
                }
            }

            @Override
            public void onSearchCleared() {
                mAdapter.filter("");
            }

        });

    }


    void showNoDataDialog(final Boolean addMore) {

        stopSpinners();

    }


    protected void stopSpinners() {
        isLoading = false;
        mProgressWheelLoadMore.stopSpinning();

        try {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 2000ms
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }, 2000);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onShoutUpdate() {
        getData(false);
    }

    @Override
    public void onShoutUpdateGen(String type) {

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        ShoutObserver.getInstance().remove(this);
    }


    @Override
    public void OnReceive(Context context, Intent intent) {
        super.OnReceive(context, intent);
        Log.d(TAG, "Profile Action Received");
        if (Constants.ACTION_PROFILE_UPDATE.equals(intent.getAction())) {
            ((PlabroBaseActivity) getActivity()).setTitle("My Shouts");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }
}
