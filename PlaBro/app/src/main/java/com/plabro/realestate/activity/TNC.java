package com.plabro.realestate.activity;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.uiHelpers.MaterialDialog;
import com.plabro.realestate.widgets.dialogs.GenericPopUpDialog;


public class TNC extends PlabroBaseActivity {

    private TextView tv_tnc_content, tnc_default;
    private TextView tv_accept;
    private TextView tv_cancel;
    private LinearLayout tncButtonsLayout;
    ScrollView tncView;
    String tnc = "";


    @Override
    protected String getTag() {
        return "TNC";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tnc);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);


        //initialise elements
        tv_accept = (TextView) findViewById(R.id.tnc_accept);
        tv_cancel = (TextView) findViewById(R.id.tnc_decline);
        tncButtonsLayout = (LinearLayout) findViewById(R.id.tnc_buttons);
        tv_tnc_content = (TextView) findViewById(R.id.tnc_details);
        tnc_default = (TextView) findViewById(R.id.tnc_default);
        tncView = (ScrollView) findViewById(R.id.tnc_view);


        //binding onlick
        tv_accept.setOnClickListener(customOnClickLsitener);
        tv_cancel.setOnClickListener(customOnClickLsitener);

        tv_tnc_content.setOnClickListener(customOnClickLsitener);

        loadTNC();

    }


    //custom on click listener to handle all clicks
    private View.OnClickListener customOnClickLsitener = new View.OnClickListener() {

        @Override
        public void onClick(View c_view) {
            // switch for different onclicks
            switch (c_view.getId()) {

                case R.id.tnc_accept:

                    //Crittercism.endTransaction("tnc");

                    //Navigate to terms and condition screenSc
                    sendScreenName(R.string.funnel_tnc_accepted);
                    Intent intent = new Intent(TNC.this, Registration.class);
                    startActivity(intent);
                    //finish();

                    break;

                case R.id.tnc_decline:

                    //Crittercism.failTransaction("tnc");
                    //exit application

                    exitApp();

                    break;

                case R.id.tnc_details:

                    FragmentManager mFragmentManager = getFragmentManager();
                    DialogFragment feedPopUpDialog = new GenericPopUpDialog( TNC.this,TNC.this,tnc);
                    feedPopUpDialog.show(mFragmentManager, "feed");

                    break;

                default:


            }

        }
    };


    @Override
    public void onBackPressed() {
        //confirm from user ofr exit
        showExitAlert();

    }

    void showExitAlert() {

        final MaterialDialog mMaterialDialog;
        mMaterialDialog = new MaterialDialog(this);

        mMaterialDialog.setTitle(R.string.exit_confirm_title)
                .setMessage(R.string.exit_confirm_msg)
                .setPositiveButton(R.string.exit, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                        exitApp();


                    }
                })
                .setNegativeButton(R.string.cancel, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();


                    }
                });

        mMaterialDialog.show();


    }



    void loadTNC() {

        //tnc = getResources().getString(R.string.tnc_val);

        //StringBuilder builder = new StringBuilder(getResources().getString(R.string.tnc_val));
        //tv_tnc_content.setText(builder.toString());

        /*String tncTrimmed = tnc;//+" more...";

        SpannableString hashTagText = new SpannableString(tncTrimmed);
        try {
            tv_tnc_content.setMovementMethod(LinkMovementMethod.getInstance());

            SpannableString moreText = new SpannableString(tncTrimmed);

            int start = tncTrimmed.indexOf("more...");
            if(start != -1) {
                int end = start + 7;
                moreText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), start, end, 0);
            }
            tv_tnc_content.setText(moreText, TextView.BufferType.SPANNABLE);


        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    void exitApp() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
        startActivity(intent);
        finish();
        System.exit(0);
    }


    @Override
    public void findViewById() {

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void inflateToolbar() {

    }
}