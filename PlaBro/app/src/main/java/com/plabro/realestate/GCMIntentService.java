package com.plabro.realestate;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.clevertap.android.sdk.CleverTapAPI;
import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.Services.BrokerContacts;
import com.plabro.realestate.Services.PlabroIntentService;
import com.plabro.realestate.Services.XMPPConnectionService;
import com.plabro.realestate.activity.DeepLinking;
import com.plabro.realestate.activity.MainActivity;
import com.plabro.realestate.activity.Preferences;
import com.plabro.realestate.models.chat.ChatMessage;
import com.plabro.realestate.models.propFeeds.EndorsementNotificationData;
import com.plabro.realestate.models.propFeeds.GreetingFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.JSONUtils;
import com.plabro.realestate.utilities.PBConnectionManager;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlabroPushNotificationManager;
import com.plabro.realestate.volley.AppVolleyCacheManager;

import java.util.HashMap;

public class GCMIntentService extends GCMBaseIntentService {

    private static final int MY_NOTIFICATION_ID = 0;
    private NotificationManager notificationManager;
    private Notification myNotification;
    private int notificationCount = 0;


    @Override
    protected String[] getSenderIds(Context context) {

        try {
            return new String[]{context.getString(R.string.gcm_sender_id)};
        } catch (Exception e) {
            e.printStackTrace();
            return new String[0];
        }
    }

    @Override
    protected void onError(Context arg0, String arg1) {
        Log.i("check", "In onError" + arg1);
    }

    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.d(TAG, "PUSH:Incoming notif initiated");

        // Log.v("check", "onMessage : " + intent.getExtras().toString());
        try {
            Bundle b = intent.getExtras();
            if (b != null) {
                for (String key : b.keySet()) {
                    Log.d(TAG, key + ",,," + b.get(key) + "");
                }
            }


            String title = intent.getExtras().getString("title");
            String uri = intent.getExtras().getString("act");
            String message = intent.getExtras().getString("msg");
            String url = intent.getExtras().getString("url");
            String phone = intent.getExtras().getString("phone");

            Bundle extras = intent.getExtras();
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
            String messageType = gcm.getMessageType(intent);
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

                if (extras.containsKey("wzrk_dl")) {
                    String deepLink = extras.getString("wzrk_dl");
                    String defaultHandling = extras.get("default") + "";
                    if ("false".equalsIgnoreCase(defaultHandling)) {
                        uri = DeepLinking.ACT_TYPE.URI_GCM_DEEP_LINK;
                        title = extras.getString("nt");
                        message = extras.getString("nm");
                        url = deepLink;
                        b.putString("url", url);
                    } else {
                        CleverTapAPI.createNotification(context, extras);
                    }
                } else {
                    CleverTapAPI.createNotification(context, extras);
                }


            }


            if (AppController.isActivityVisible()) {

                PBPreferences.setNewIMOnApp(true);
                AppController.getInstance().showNewIMsPopup();

                if (null != uri && !uri.equalsIgnoreCase("")) {
                    if (uri.equalsIgnoreCase("fl")) {
                        PBPreferences.setNewFollowerState(true);
                        showNotification(title, message, uri, b);
                    } else if (uri.equalsIgnoreCase("pf")) {

                        int prevVal = PBPreferences.getFeedNotificationVal();
                        prevVal = prevVal + 1;
                        PBPreferences.setFeedNotificationVal(prevVal);

                        showNotification(title, message, uri, b);
                        PBPreferences.setNewPostState(true);

                    } else if (uri.equalsIgnoreCase("pb")) {


                        PBPreferences.setNewBroadCastState(true);
                        PBPreferences.setBroadCastUrl(url);
                        showNotification(title, message, uri, null);

                    } else if (uri.equalsIgnoreCase("upv")) {


                        if (!TextUtils.isEmpty(url)) {
                            int ver = Integer.parseInt(intent.getExtras().getString("ver"));
                            PBPreferences.setAppUpdateVer(ver);
                            PBPreferences.setIsNewVersionAvailbale(true);
                            PBPreferences.setAppUpdateUrl(url);
                            showNotification(title, message, uri, null);

                        }


                    } else if (uri.equalsIgnoreCase("kun")) {

                        PBPreferences.setKeyboardUpdateTimestamp(true);

                    }
                    // Cache Delete
                    else if (uri.equalsIgnoreCase("cd")) {

                        AppVolleyCacheManager.deleteCacheFolder();

                    }

                    Log.v("Plabro", "Inside Application");
                }

            } else {

                if (null != uri && !uri.equalsIgnoreCase("")) {

                    if (uri.equalsIgnoreCase("fl")) {
                        PBPreferences.setNewFollowerState(true);
                        showNotification(title, message, uri, b);
                    } else if (uri.equalsIgnoreCase("im")) {

                        setNewImNotification(intent, context, message, title, uri, true);

                    } else if (uri.equalsIgnoreCase("pf")) {

                        showNotification(title, message, uri, b);
                        PBPreferences.setNewPostState(true);
                        return;
                    } else if (uri.equalsIgnoreCase("pb")) {

                        PBPreferences.setNewBroadCastState(true);
                        PBPreferences.setBroadCastUrl(url);
                        showNotification(title, message, uri, null);


                    } else if (uri.equalsIgnoreCase("upv")) {

                        if (!TextUtils.isEmpty(url)) {
                            int ver = Integer.parseInt(intent.getExtras().getString("ver"));
                            PBPreferences.setAppUpdateVer(ver);
                            PBPreferences.setIsNewVersionAvailbale(true);
                            PBPreferences.setAppUpdateUrl(url);
                            showNotification(title, message, uri, null);

                        }


                    } else if (uri.equalsIgnoreCase("kun")) {

                        PBPreferences.setKeyboardUpdateTimestamp(true);

                    }

                }


            }
            if (null != uri && uri.equalsIgnoreCase(DeepLinking.ACT_TYPE.GCM_AD_URL)) {
//                showNotification(title, message, uri, b);
                String cat = b.getString("category");
                String event = b.getString("event");
                String label = b.getString("label");
                HashMap<String, Object> map = new HashMap<>();
                if (!TextUtils.isEmpty(label)) {
                    map.put("label", label);
                }
                if (!TextUtils.isEmpty(cat) && !TextUtils.isEmpty(event)) {
                    Analytics.trackEventWithProperties(cat, event, map);
                }

            }
            if (null != uri && uri.equalsIgnoreCase(DeepLinking.ACT_TYPE.BROKER_CONTACTS_SERVICE)) {
                PBPreferences.saveData(PBPreferences.IS_BROKER_CONTACT_ENABLED, true);
                BrokerContacts.startContactSyncing(context);
                return;
            }
            if (null != uri && uri.equalsIgnoreCase(DeepLinking.ACT_TYPE.URI_GCM_DEEP_LINK)) {
                showNotification(title, message, uri, b);
            }
            if (null != uri && uri.equalsIgnoreCase(DeepLinking.ACT_TYPE.NEWS)) {
                showNotification(title, message, uri, b);
            }
            if (null != uri && uri.equalsIgnoreCase(DeepLinking.ACT_TYPE.AUCTION)) {
                showNotification(title, message, uri, b);
            }
            if (null != uri && uri.equalsIgnoreCase(DeepLinking.ACT_TYPE.WALLET)) {
                showNotification(title, message, uri, b);
            }
            if (null != uri && uri.equalsIgnoreCase(DeepLinking.ACT_TYPE.BLOGS)) {
                showNotification(title, message, uri, b);
            }
            if (null != uri && uri.equalsIgnoreCase(DeepLinking.ACT_TYPE.DIRECT_LISTING)) {
                showNotification(title, message, uri, b);
            }
            if (null != uri && uri.equalsIgnoreCase(DeepLinking.ACT_TYPE.SERVICES)) {
                showNotification(title, message, uri, b);
            }
            if (null != uri && uri.equalsIgnoreCase(DeepLinking.ACT_TYPE.PLABRO_WEB)) {
                showNotification(title, message, uri, b);
            }
            if (null != uri && uri.equalsIgnoreCase(DeepLinking.ACT_TYPE.NOTIF)) {
                showNotification(title, message, uri, b);
            }

            if (null != uri && uri.equalsIgnoreCase("js")) {
                PBPreferences.saveData(PBPreferences.REQUEST_JSON_SHARE, true);
            }
            if (null != uri && uri.equalsIgnoreCase("im")) {
                setNewImNotification(intent, context, message, title, uri, false);
            }

            if (null != uri && uri.equalsIgnoreCase("cd")) {

                AppVolleyCacheManager.deleteCacheFolder();

            }
            if (null != uri && uri.equalsIgnoreCase("gt")) {
                Log.d(TAG, "received Notification GT");
                String greetingJsonString = intent.getStringExtra("greetings_data");
                if (!TextUtils.isEmpty(greetingJsonString)) {
                    GreetingFeed f = GreetingFeed.parseJson(greetingJsonString);
                    if (null != f)
                        PlabroIntentService.showPushNotification(context, f);
                    else {
                        Log.d(TAG, "Greeting Feed is null");
                    }
                }

            }
            if (null != uri && uri.equalsIgnoreCase("wifi")) {
                if (b != null && b.containsKey("isEnabled")) {
                    boolean isWifiEnabled = b.getBoolean("isEnabled");
                    PBPreferences.saveData(PBPreferences.IS_WIFI_ENABLED, isWifiEnabled);
                }

            }
            if (null != uri && uri.equalsIgnoreCase("call_sync")) {
                boolean isCallSyncEnable = b.getBoolean("isEnabled");
                PBPreferences.saveData(PBPreferences.ENABLE_CALL_SYNC, isCallSyncEnable);
            }
            if (null != uri && uri.equalsIgnoreCase("auc")) {
                showNotification(title, message, uri, b);
            }
            if (null != uri && uri.equalsIgnoreCase("ad")) {
                showNotification(title, message, uri, b);
            }
            if (null != uri && uri.equalsIgnoreCase("iL")) {
                PBPreferences.saveData(PBPreferences.KEY_LAUNCH, true);
            }
            if (null != uri && uri.equalsIgnoreCase("wv")) {
//                PBPreferences.saveData(PBPreferences.KEY_LAUNCH, true);
                if (TextUtils.isEmpty(url)) {
                    return;
                }
                b.putBoolean("fromNotification", true);
                PlabroIntentService.showImageNotification(context, b);
            }
            if (null != uri && uri.equalsIgnoreCase("bdg")) {
                int badgeCount = Integer.parseInt(extras.getString(Constants.BUNDLE_KEYS.BADGE_COUNT));
                Log.d(TAG, "Badge Count" + badgeCount);
                PBPreferences.saveData(PBPreferences.CHECK_BADGE_COUNT, true);
                sendBadgeCountBroadCast(badgeCount);
            }
            if (null != uri && uri.equalsIgnoreCase("eN")) {
                Log.d(TAG, "Endorsement Notification Received:");
                String endorsementData = intent.getExtras().getString("endorsement_data");
                Log.d(TAG, "Endorsement Data :" + endorsementData);
                if (!TextUtils.isEmpty(endorsementData)) {
                    EndorsementNotificationData data = (EndorsementNotificationData) JSONUtils.parseJson(endorsementData, EndorsementNotificationData.class);
                    data.setJsonString(endorsementData);
                    data.save();
                }
                showNotification(title, message, uri, new Bundle());
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Error in clevertap GCM Handling");
        }
    }

    private void sendBadgeCountBroadCast(int count) {
        Intent i = new Intent();
        i.setAction(Constants.ACTION_BADGE_COUNT);
        i.putExtra(Constants.BUNDLE_KEYS.BADGE_COUNT, count);
        sendBroadcast(i);
    }

    private void setNewImNotification(Intent intent, Context context, String message, String title, String uri, boolean isSendPush) {

        //todo nitin check this with messagelistenerImpl class

        if (null != message && !TextUtils.isEmpty(message)) {
            if (PBConnectionManager.getInstance(AppController.getInstance()).getConnection().isConnected()) {
                return;
            }
            String id = intent.getExtras().getString("id");
            String phone = intent.getExtras().getString("ph");

            for (String extra : intent.getExtras().keySet()) {
                Log.d(TAG, "Extra Key :" + extra + ",, Value :" + intent.getExtras().get(extra));
            }


            PBPreferences.setIMAuthorId(id);
            PBPreferences.setIMPhone(phone);

            ChatMessage cm = new ChatMessage();
            cm.setPhone(phone);
            cm.setMsg(message);
            cm.setMsgType(ChatMessage.MESSAGE_INCOMING);
            cm.setTime(System.currentTimeMillis());

            cm.setPacketId(id);
            cm.setPacketIdAndMessageType(id + ChatMessage.MESSAGE_INCOMING);

            //insert into database
            //cm.save();
            PBPreferences.setNewIMState(isSendPush);
            Boolean chatNotificationStatus = PBPreferences.getIsChatNotificationEnable();

            if (chatNotificationStatus && isSendPush) {

                Bundle b = new Bundle();
                b.putString("phone_key", phone);
                b.putString("userid", id);
                XMPPConnectionService.startXMPPService(AppController.getInstance());
                showNotification(title, message, uri, b);

            }
        }
    }

    @Override
    protected void onRegistered(Context context, String gcmId) {
        // Log.i("check", "GCMID is:" + gcmId);

        // TODO save GCM id

    }

    @Override
    protected void onUnregistered(Context arg0, String arg1) {
        //   Log.i("chk", "In onUnregistered");

    }

    void showNotification(String title, String message, String uri, Bundle b) {
        Log.d(TAG, "PUSH:Showing notif initiated");
        if (PBPreferences.getIsPushNotificationEnable()) {
            if (TextUtils.isEmpty(title)) {
                title = "New Plabro Notification";
            }

            int icon = R.drawable.notif;
//            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//            myNotification = new Notification(icon, title, System.currentTimeMillis());
//
//
//            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
//
//            } else {
//                myNotification.color = getResources().getColor(R.color.colorPrimary);
//            }
//            myNotification.number = notificationCount++;
            Context context = getApplicationContext();
            Intent notifyIntent = getDefaultIntent(title, message, uri, b);
            notifyIntent.putExtra("fromNotification", true);

            PlabroPushNotificationManager.sendNotification(context, message, title, notifyIntent, false, -1);

        } else {
            Log.v(TAG, "push notif disabled");
        }
    }

    private Intent getDefaultIntent(String title, String message, String uri, Bundle b) {

        Intent intent = new Intent(this, MainActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        if (b == null) {
            b = new Bundle();
        }
        b.putString("title", title);
        b.putString("act", uri);
        b.putString("msg", message);

        intent.putExtras(b);

        return intent;

/*
        if (!PBPreferences.getLoggedInState(getApplicationContext())) {

            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            return intent;

        } else {
            Intent resultIntent = new Intent(Intent.ACTION_VIEW);
            resultIntent.setData(Uri.parse(uri));

            return resultIntent;
        }
*/

    }


}

