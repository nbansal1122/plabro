package com.plabro.realestate.fragments.payu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.payu.india.Model.PaymentDetails;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PostData;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.india.PostParams.PaymentPostParams;
import com.payu.payuui.PaymentsActivity;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.utilities.Constants;

import java.util.ArrayList;

/**
 * Created by nitin on 18/11/15.
 */
public class NetbankingFragment extends PlabroBaseFragment implements CustomListAdapter.CustomListAdapterInterface {
    private Spinner spinner;
    private String bankcode;
    private Bundle bundle;
    CustomListAdapter<String> adapter;
    private ArrayList<PaymentDetails> netBankingList;
    private ArrayList<String> bankNames = new ArrayList<>();
    private Spinner spinnerNetbanking;
    //    private String[] netBanksNamesArray;
//    private String[] netBanksCodesArray;
    private PaymentParams mPaymentParams;
    private PayuHashes payuHashes;

    private Button payNowButton;

    private PayuConfig payuConfig;

    @Override
    public String getTAG() {
        return "Netbanking Fragment";
    }

    @Override
    protected void findViewById() {
        bundle = getArguments();

        if (bundle != null && bundle.getParcelableArrayList(PayuConstants.NETBANKING) != null) {
            netBankingList = new ArrayList<PaymentDetails>();
            netBankingList = bundle.getParcelableArrayList(PayuConstants.NETBANKING);
            spinnerNetbanking = (Spinner) findView(R.id.spinner_netbanking);

            for (PaymentDetails p : netBankingList) {
                bankNames.add(p.getBankName());
            }

            adapter = new CustomListAdapter<>(getActivity(), android.R.layout.simple_list_item_1, bankNames, this);
            spinnerNetbanking.setAdapter(adapter);
            spinnerNetbanking.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int index, long l) {
                    bankcode = netBankingList.get(index).getBankCode();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } else {
            Toast.makeText(getActivity(), "Could not get netbanking list Data from the previous activity", Toast.LENGTH_LONG).show();
        }

        mPaymentParams = bundle.getParcelable(PayuConstants.PAYMENT_PARAMS);
        payuHashes = bundle.getParcelable(PayuConstants.PAYU_HASHES);
        payuConfig = bundle.getParcelable(PayuConstants.PAYU_CONFIG);
        payuConfig = null != payuConfig ? payuConfig : new PayuConfig();
    }

    public void doPayment() {
        if (TextUtils.isEmpty(bankcode)) {
            showToast(getString(R.string.select_bank_error));
            return;
        }
        PostData postData = new PostData();
        mPaymentParams.setHash(payuHashes.getPaymentHash());
        mPaymentParams.setBankCode(bankcode);

        postData = new PaymentPostParams(mPaymentParams, PayuConstants.NB).getPaymentPostParams();

//            postData = new NBPostParams(mPaymentParams, mNetBank).getNBPostParams();
        if (postData.getCode() == PayuErrors.NO_ERROR) {
            // launch webview
            payuConfig.setData(postData.getResult());
            Log.d(TAG, "Post Data Result:" + postData.getResult());
            Intent intent = new Intent(getActivity(), PaymentsActivity.class);
            intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
            String successUrl = mPaymentParams.getSurl();
            String failUrl = mPaymentParams.getFurl();
            intent.putExtra(Constants.BUNDLE_KEYS.S_URL, successUrl);
            intent.putExtra(Constants.BUNDLE_KEYS.F_URL, failUrl);
            startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
        } else {
            Toast.makeText(getActivity(), postData.getResult(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_netbanking;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        NetbankingViewHolder netbankingViewHolder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(resourceID, null);
            netbankingViewHolder = new NetbankingViewHolder(convertView);
            convertView.setTag(netbankingViewHolder);
        } else {
            netbankingViewHolder = (NetbankingViewHolder) convertView.getTag();
        }

//        PaymentDetails paymentDetails = netBankingList.get(position);

        // set text here
        netbankingViewHolder.netbankingTextView.setText(bankNames.get(position));
        return convertView;
    }


    class NetbankingViewHolder {
        TextView netbankingTextView;

        NetbankingViewHolder(View view) {
            netbankingTextView = (TextView) view.findViewById(android.R.id.text1);
        }
    }
}
