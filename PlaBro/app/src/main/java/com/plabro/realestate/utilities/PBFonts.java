package com.plabro.realestate.utilities;

import android.content.Context;
import android.graphics.Typeface;

public class PBFonts {
    public static Typeface ROBOTTO_NORMAL = null;
    public static Typeface ROBOTTO_BOLD = null;
    public static Typeface ROBOTTO_LIGHT = null;
    public static Typeface ROBOTTO_MEDIUM = null;
    public static Typeface ROBOTTO_CONDENSED = null;
    public static Typeface RUPEE_FORADIAN = null;

    public static Typeface GEORGIA = null;
    public static Typeface GEORGIA_ITALIC = null;

    public PBFonts(Context context) {
        initFonts(context);
    }

    private void initFonts(Context context) {

        if (ROBOTTO_NORMAL == null) {
            ROBOTTO_NORMAL = Typeface.createFromAsset(context.getAssets(), "Fonts/Roboto-Regular.ttf");
        }

        if (ROBOTTO_BOLD == null) {
            ROBOTTO_BOLD = Typeface.createFromAsset(context.getAssets(), "Fonts/Roboto-Bold.ttf");
        }

        if (ROBOTTO_MEDIUM == null) {
            ROBOTTO_MEDIUM = Typeface.createFromAsset(context.getAssets(), "Fonts/Roboto-Medium.ttf");
        }

        if (ROBOTTO_LIGHT == null) {
            ROBOTTO_LIGHT = Typeface.createFromAsset(context.getAssets(), "Fonts/Roboto-Light.ttf");
        }

        if (GEORGIA == null) {
            GEORGIA = Typeface.createFromAsset(context.getAssets(), "Fonts/Georgia.ttf");
        }

        if (ROBOTTO_CONDENSED== null) {
            ROBOTTO_CONDENSED = Typeface.createFromAsset(context.getAssets(), "Fonts/Georgia.ttf");
        }

        if(RUPEE_FORADIAN == null){
            RUPEE_FORADIAN = Typeface.createFromAsset(context.getAssets(),"Fonts/Rupee_Foradian.ttf");
        }

        if(GEORGIA_ITALIC == null){
            GEORGIA_ITALIC = Typeface.createFromAsset(context.getAssets(),"Fonts/Georgia_Italic.ttf");
        }

    }
}
