package com.plabro.realestate.models.propFeeds.listings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 11/01/16.
 */
public class ListingDetail implements Serializable {
    @SerializedName("image_urls")
    @Expose
    private List<String> image_urls = new ArrayList<>();
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("info_list")
    @Expose
    private List<InfoList> info_list = new ArrayList<>();
    @SerializedName("title")
    @Expose
    private String title;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<InfoList> getInfo_list() {
        return info_list;
    }

    public void setInfo_list(List<InfoList> info_list) {
        this.info_list = info_list;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getImage_urls() {
        return image_urls;
    }

    public void setImage_urls(List<String> image_urls) {
        this.image_urls = image_urls;
    }
}
