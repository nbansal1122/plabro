package com.plabro.realestate.security;

import android.content.Context;
import android.text.TextUtils;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.securitykeysharing.KeySharingResponse;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.HashMap;


public class KeyExchange {

    private static String TAG = KeyExchange.class.getSimpleName();
    private static Context mContext;
    private static String clientPubKey = "";
    private static String clientPrivKey = "";

    public static void initKeyExchangeProcess(Context mContext) {
        KeyExchange.mContext = mContext;
        //get server public key
        sharePublicKeysBWClientAndServer("", "");


    }


    static void sharePublicKeysBWClientAndServer(final String clientKey, String imei) {

        if (!Util.haveNetworkConnection(mContext)) {
            return;
        }

        HashMap<String, String> params = new HashMap<String, String>();
        if (!TextUtils.isEmpty(clientKey)) {
            params.put(PlaBroApi.PLABRO_KEY_EXCHANGE_POST_PARAM_CLIENT_KEY, clientKey);
        }
        if (!TextUtils.isEmpty(imei)) {
            params.put(PlaBroApi.PLABRO_KEY_EXCHANGE_POST_PARAM_IMEI, imei);

        }

        params = Utility.getInitialParamsWithoutAuthkey(PlaBroApi.RT.KEYEXCHANGE, params);

        AppVolley.processRequest(Constants.TASK_CODES.KEY_EXCHANGE, KeySharingResponse.class, null, PlaBroApi.getBaseUrl(mContext), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {

                KeySharingResponse keySharingResponse = (KeySharingResponse) response.getResponse();


                if (TextUtils.isEmpty(clientKey)) {
                    String serverPubKey = keySharingResponse.getOutput_params().getData().getKey();
                    //save server pub key
                    Log.d(TAG, "public key from server set successfully");
                    PBPreferences.setServerPublicKey(serverPubKey);

                    //send client public key to server
                    sharePublicKeysBWClientAndServer(PBPreferences.getImei(), clientPubKey);
                } else {
                    Log.d(TAG, "client key sent to server successfully");

                    //get AES key from server
                    getAESKeyFromServer(PBPreferences.getImei());
                }


            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {


                switch (response.getCustomException().getExceptionType())
                {
                    case NO_INETERNET_EXCEPTION:
                        break;
                    default:
                        reHitServer(clientKey);
                        break;
                }


            }
        });
    }


    private static void reHitServer(String clientKey) {

        if (TextUtils.isEmpty(clientKey)) {
            Log.d(TAG, "getting server public key failed");

            sharePublicKeysBWClientAndServer("", "");

        } else {
            Log.d(TAG, "sending client public key failed");

            sharePublicKeysBWClientAndServer(PBPreferences.getImei(), clientPubKey);

        }
    }

    private static void getAESKeyFromServer(String imei) {

        if (!Util.haveNetworkConnection(mContext)) {
            return;
        }

        HashMap<String, String> params = new HashMap<String, String>();

        params.put(PlaBroApi.RT_KEY, PlaBroApi.RT.AESKEY);
        params.put(PlaBroApi.PLABRO_KEY_EXCHANGE_POST_PARAM_IMEI, imei);


        AppVolley.processRequest(0, null, null, PlaBroApi.getBaseUrl(mContext), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {

            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {

            }
        });
    }

    public static String getDecryptedAESKey() {
        String serverPubkey = PBPreferences.getServerPublicKey();
        try {
            RSAEncryption rsa = new RSAEncryption(serverPubkey);
            String aesKey = PBPreferences.getAESKey();
            return rsa.decrypt(aesKey);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}