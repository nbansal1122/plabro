package com.plabro.realestate.models.location_auto;

import java.io.Serializable;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class LocationsAuto implements Serializable {


    String _id;
    String title;
    int[] indices = new int[2];
    String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int[] getIndices() {
        return indices;
    }

    public void setIndices(int[] indices) {
        this.indices = indices;
    }




}
