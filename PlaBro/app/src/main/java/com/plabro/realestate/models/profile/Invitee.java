package com.plabro.realestate.models.profile;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by nitin on 20/06/15.
 */
@Table(name = "Invitee")
public class Invitee extends Model {
    @Column(unique = true, onUniqueConflict = Column.ConflictAction.IGNORE)
    private String inviteeCode;
    @Column
    private boolean isConsumed;

    public String getInviteeCode() {
        return inviteeCode;
    }

    public void setInviteeCode(String inviteeCode) {
        this.inviteeCode = inviteeCode;
    }

    public boolean isConsumed() {
        return isConsumed;
    }

    public void setIsConsumed(boolean isConsumed) {
        this.isConsumed = isConsumed;
    }
}
