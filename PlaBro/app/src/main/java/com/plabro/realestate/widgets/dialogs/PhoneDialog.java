package com.plabro.realestate.widgets.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.adapter.PhoneAdapter;
import com.plabro.realestate.listeners.PhoneConnectListener;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.receivers.CallListener;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.uiHelpers.view.FontText;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Hemant on 23-03-2015.
 */
public class PhoneDialog extends DialogFragment implements CustomListAdapter.CustomListAdapterInterface, AdapterView.OnItemClickListener {

    ListView mListView;
    ItemViewHolder holder;
    private String title;
    int freeCall;
    private String mSelectedItem;
    Context mContext;
    private List<String> arr_phone = new ArrayList<String>();
    HashMap<String, Object> wrData;
    String profile_img;
    String profile_name;
    BaseFeed feed;
    RoundedImageView mProfileImage;
    FontText mTitle;

    @SuppressLint("ValidFragment")
    public PhoneDialog(Context context, BaseFeed feed, List arr_phone, int freeCall, HashMap<String, Object> wrData, String profile_img, String profile_name) {
        this.mContext = context;
        this.arr_phone = arr_phone;
        this.freeCall = freeCall;
        this.wrData = wrData;
        this.profile_img = profile_img;
        this.profile_name = profile_name;
        this.feed = feed;

    }

    public PhoneDialog() {

    }

    DialogInterface.OnCancelListener onCancelListener = null;

    public void setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
        this.onCancelListener = onCancelListener;
    }


    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if (onCancelListener != null)
            onCancelListener.onCancel(getDialog());
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.phone_dialog, null);
        builder.setView(convertView);

        mListView = (ListView) convertView.findViewById(R.id.lv_item_list);
        mProfileImage = (RoundedImageView) convertView.findViewById(R.id.profile_image);
        mTitle = (FontText) convertView.findViewById(R.id.tv_title);
        mTitle.setText("Call " + profile_name);
        if (!TextUtils.isEmpty(profile_img)) {
            Picasso.with(mContext).load(profile_img).placeholder(R.drawable.profile_default_one).into(mProfileImage);
        }

        setUpPhoneList();
        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    private void setUpPhoneList() {

        PhoneAdapter mPhoneAdapter = new PhoneAdapter(mContext, arr_phone, freeCall, wrData, profile_img, profile_name, phoneConnectListener);
        mPhoneAdapter.notifyDataSetChanged();

        // Assign adapter to ListView
        mListView.setAdapter(mPhoneAdapter);


    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceID, null);
            holder = new ItemViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ItemViewHolder) convertView.getTag();
        }
        String item = arr_phone.get(position);

        holder.item.setText(item);
        holder.item.setTag(item);
        holder.itemCheck.setTag(item);


        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String selectedItem = arr_phone.get(i);
        //holder.
    }

    private class ItemViewHolder {
        TextView item;
        CheckBox itemCheck;

        public ItemViewHolder(View view) {
        }
    }

    PhoneConnectListener phoneConnectListener = new PhoneConnectListener() {
        @Override
        public void onResponse(Boolean isFreeCall) {
            AppCompatActivity act = (AppCompatActivity) getActivity();
            if (act instanceof CallListener.CallInterface) {
                Log.d("Phone Connect Listener", "instance of call Listener");
                CallListener.CallInterface callInterface = (CallListener.CallInterface) act;
                callInterface.startListeningToCall(feed, isFreeCall);
            } else {
                Log.d("Phone Connect Listener", "not an instance of call Listener");
            }
            dismiss();
        }
    };


}
