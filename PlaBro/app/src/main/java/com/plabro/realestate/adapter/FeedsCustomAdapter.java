
package com.plabro.realestate.adapter;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.holders.AdvertisementFeedHolder;
import com.plabro.realestate.holders.AuctionFeedHolder;
import com.plabro.realestate.holders.BaseFeedHolder;
import com.plabro.realestate.holders.BiddingFeedHolder;
import com.plabro.realestate.holders.BrokerCardHolder;
import com.plabro.realestate.holders.BrokerPromoteHolder;
import com.plabro.realestate.holders.BuilderHolder;
import com.plabro.realestate.holders.DirectInventoryHolder;
import com.plabro.realestate.holders.ExternalShoutCardHolder;
import com.plabro.realestate.holders.FeedsCardHolder;
import com.plabro.realestate.holders.FeedsPostSearchHolder;
import com.plabro.realestate.holders.GreetingHolder;
import com.plabro.realestate.holders.MultiBrokerCardHolder;
import com.plabro.realestate.holders.MyBidHolder;
import com.plabro.realestate.holders.NewsHolder;
import com.plabro.realestate.holders.ProgressHolder;
import com.plabro.realestate.holders.RelatedBrokerHolder;
import com.plabro.realestate.holders.WidgetFeedHolder;
import com.plabro.realestate.listeners.OnUpdateListener;
import com.plabro.realestate.models.location_auto.LocationsAuto;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FeedsCustomAdapter extends RecyclerView.Adapter<BaseFeedHolder> {


    public static final int TRIAL_VERSION_CARD = 0;
    public static final int FEEDS_CARD = 1;
    public static final int BROKER_SINGLE_CARD = 2;
    public static final int BROKER_MULTI_CARD = 3;
    public static final int BUILDER_CARD = 4;
    public static final int NEWS_CARD = 5;
    private static final int RELATED_BROKER = 6;
    private static final int GREETING = 7;
    private static final int DIRECT_INVENTORY = 8;
    private static final int BIDDING_FEED = 9;
    private static final int ADVERTISEMENT_FEED = 10;
    private static final int AUCTION_FEED = 11;
    private static final int MY_BID = 12;
    private static final int BROKER_PROMOTE = 13;
    private static final int FEED_POST_SEARCH = 14;
    private static final int WIDGET_FEED = 15;
    private static final int EXTERNAL_SOURCE = 16;

    public static final int FEEDS_CARD_DISABLED = -1;
    public static final int PROGRESS_MORE = -2;
    public static final int NO_MORE = -3;
    public static final int NO_MORE_DATA = -4;
    public static final int TYPE_HEADER = -5;


    private Context mContext;
    private Activity mActivity;
    private int feedsCount = 0;
    private static final String TAG = "FeedsCustomAdapter";
    protected ArrayList<BaseFeed> feeds = new ArrayList<BaseFeed>();
    protected ArrayList<BaseFeed> arrayList = new ArrayList<BaseFeed>();
    private ArrayList<BaseFeed> selectedFeedsObject = new ArrayList<BaseFeed>();

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    private String searchText = "";

    List<LocationsAuto> hashtags = new ArrayList<LocationsAuto>();
    protected String type = "";
    private OnUpdateListener bookmarkDeleteListener;
    private static int refKey;
    View v = null;

    public class ViewHolder extends BaseFeedHolder {


        public ViewHolder(View v) {
            super(v);
        }


    }


    public FeedsCustomAdapter(ArrayList<BaseFeed> feeds, String type, Activity activity, Context context, int refKey) {
        this.feeds = feeds;
        feedsCount = feeds.size();
        this.type = type;
        this.mContext = context;
        this.mActivity = activity;
        this.refKey = refKey;
        arrayList.addAll(feeds);
    }

    public FeedsCustomAdapter(ArrayList<BaseFeed> feeds, Activity mActivity, String type) {
        this.feeds = feeds;
        this.feedsCount = feeds.size();
        this.type = type;
        this.mContext = mActivity;
        this.mActivity = mActivity;
        this.refKey = refKey;
        arrayList.addAll(feeds);
    }


    public FeedsCustomAdapter(ArrayList<BaseFeed> feeds, String type, Activity activity, int refKey) {
        this.feeds = feeds;
        feedsCount = feeds.size();
        this.type = type;
        this.mContext = activity;
        this.mActivity = activity;
        this.refKey = refKey;
        arrayList.addAll(feeds);

    }


    public FeedsCustomAdapter(ArrayList<BaseFeed> feeds, String type, Activity activity, OnUpdateListener bookmarkDeleteListener) {
        this.feeds = feeds;
        feedsCount = feeds.size();
        this.type = type;
        this.mContext = activity;
        this.mActivity = activity;
        this.bookmarkDeleteListener = bookmarkDeleteListener;
        arrayList.addAll(feeds);

    }

    @Override
    public BaseFeedHolder onCreateViewHolder(ViewGroup viewGroup, final int viewType) {

        switch (viewType) {
            case FEEDS_CARD_DISABLED:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.cards_layout_new, viewGroup, false);
                FeedsCardHolder fchd = new FeedsCardHolder(v);
                fchd.setCardType(fchd.typeFeedDisabled);
                return fchd;

            case TRIAL_VERSION_CARD:

                int val = PBPreferences.getTrialVersionState();
                inner:
                switch (val) {
                    case 0:
                        if ("RelatedFeedsActivity".equalsIgnoreCase(type)) {
                            v = LayoutInflater.from(viewGroup.getContext())
                                    .inflate(R.layout.feeds_card_user_trial_disabled, viewGroup, false);
                        } else {
                            v = LayoutInflater.from(viewGroup.getContext())
                                    .inflate(R.layout.feeds_card_user_trial, viewGroup, false);
                        }
                        break inner;
                    case 1:
                        if ("RelatedFeedsActivity".equalsIgnoreCase(type)) {

                            v = LayoutInflater.from(viewGroup.getContext())
                                    .inflate(R.layout.feeds_card_user_waiting_disabled, viewGroup, false);
                        } else {
                            v = LayoutInflater.from(viewGroup.getContext())
                                    .inflate(R.layout.feeds_card_user_waiting, viewGroup, false);
                        }
                        break inner;
                }

                FeedsCardHolder fchdt = new FeedsCardHolder(v);
                fchdt.setCardType(fchdt.typeFeedWithTrial);
                return fchdt;

            case FEEDS_CARD:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.cards_layout_new, viewGroup, false);
                FeedsCardHolder fch = new FeedsCardHolder(v);
                fch.setTAG("FeedCard");
                fch.setCardType(fch.typeFeedEnabled);
                return fch;
            case FEED_POST_SEARCH:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.cards_layout_search_history, viewGroup, false);
                FeedsPostSearchHolder fcps = new FeedsPostSearchHolder(v);
                fcps.setCardType(fcps.typeFeedEnabled);
                fcps.setTAG("FeedCardPostSearch");
                return fcps;


            case BROKER_SINGLE_CARD:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.broker_layout_single, viewGroup, false);
                return new BrokerCardHolder(v);
            case BROKER_PROMOTE:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.card_broker_promote, viewGroup, false);
                return new BrokerPromoteHolder(v);

            case BROKER_MULTI_CARD:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.broker_layout_triple, viewGroup, false);
                MultiBrokerCardHolder h = new MultiBrokerCardHolder(v);
                h.setTAG("MultiBroker");
                return h;
            case BUILDER_CARD:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_builder, viewGroup, false);
                BuilderHolder builderHolder = new BuilderHolder(v);
                builderHolder.setTAG("Builders");
                return builderHolder;
            case NEWS_CARD:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_news, viewGroup, false);
                NewsHolder holder = new NewsHolder(v);
                holder.setTAG("NewsHolder");
                return holder;

            case PROGRESS_MORE:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.loader_layout, viewGroup, false);
                ProgressHolder ph = new ProgressHolder(v);
                ph.setType("progress");
                return ph;
            case NO_MORE:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.loader_layout, viewGroup, false);
                ProgressHolder phn = new ProgressHolder(v);
                phn.setType("noMore");
                return phn;
            case NO_MORE_DATA:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.loader_layout, viewGroup, false);
                ProgressHolder phnd = new ProgressHolder(v);
                phnd.setType(Constants.FEED_TYPE.NO_MORE_DATA);
                return phnd;
            case RELATED_BROKER:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.row_related_brokers, viewGroup, false);
                return new RelatedBrokerHolder(v);
            case GREETING:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_greeting, viewGroup, false);
                return new GreetingHolder(v);
            case DIRECT_INVENTORY:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_direct_inventory, viewGroup, false);
                return new DirectInventoryHolder(v);
            case BIDDING_FEED:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_bidding, viewGroup, false);
                return new BiddingFeedHolder(v);
            case AUCTION_FEED:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_auction, viewGroup, false);
                return new AuctionFeedHolder(v);
            case ADVERTISEMENT_FEED:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_intro_auction, viewGroup, false);
                return new AdvertisementFeedHolder(v);
            case MY_BID:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_my_bid, viewGroup, false);
                return new MyBidHolder(v);
            case WIDGET_FEED:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_widget_launchers, viewGroup, false);
                return new WidgetFeedHolder(v);
            case EXTERNAL_SOURCE:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_external_shout, viewGroup, false);
                return new ExternalShoutCardHolder(v);
        }

        return new ViewHolder(v);
    }

    private boolean isTrialCardSet = false;

    public void setIsTrialCardSet(boolean isTrialCardSet) {
        this.isTrialCardSet = isTrialCardSet;
    }

    @Override
    public int getItemViewType(int position) {

        int viewType = TRIAL_VERSION_CARD;
        int trial = PBPreferences.getTrialVersionState();
        if (trial == 0 || trial == 1) {
            if (Constants.FEED_TYPE.SHOUT.equalsIgnoreCase(feeds.get(position).getType()) && (feeds.get(position).isTrialFeed() || position == 0)) {

                viewType = TRIAL_VERSION_CARD;
                return viewType;
//            Log.d(TAG, "Value of trial version: Position is 0" + val);
//            if ((val == 0 || val == 1)) {
//                viewType = TRIAL_VERSION_CARD;
//                return viewType;
//            } else {
//                viewType = FEEDS_CARD;
//            }

            }
        }
        switch (feeds.get(position).getType()) {
            case Constants.FEED_TYPE.SHOUT:
                if (position == 0) {
                    if ("RelatedFeedsActivity".equalsIgnoreCase(type)) {
                        viewType = FEEDS_CARD_DISABLED;
                    } else {
                        viewType = FEEDS_CARD;
                    }
                } else {
                    viewType = FEEDS_CARD;
                }
                break;
            case Constants.FEED_TYPE.BROKER:
                viewType = BROKER_SINGLE_CARD;
                break;
            case Constants.FEED_TYPE.MULTI_BROKER:
                viewType = BROKER_MULTI_CARD;
                break;
            case Constants.FEED_TYPE.BUILDER_SHOUT:
                viewType = BUILDER_CARD;
                break;
            case Constants.FEED_TYPE.NEWS:
                viewType = NEWS_CARD;
                break;
            case Constants.FEED_TYPE.PROGRESS_MORE:
                viewType = PROGRESS_MORE;
                break;
            case Constants.FEED_TYPE.NO_MORE:
                viewType = NO_MORE;
                break;
            case Constants.FEED_TYPE.NO_MORE_DATA:
                viewType = NO_MORE_DATA;
                break;
            case Constants.FEED_TYPE.RELATED_BROKER:
                viewType = RELATED_BROKER;
                break;
            case Constants.FEED_TYPE.GT:
                viewType = GREETING;
                break;
            case Constants.FEED_TYPE.DIRECT_INVENTORY:
                viewType = DIRECT_INVENTORY;
                break;
            case Constants.FEED_TYPE.WEB_VIEW:
                viewType = BIDDING_FEED;
                break;
            case Constants.FEED_TYPE.ADVERTISEMENT:
                viewType = ADVERTISEMENT_FEED;
                break;
            case Constants.FEED_TYPE.AUCTION:
                viewType = AUCTION_FEED;
                break;
            case Constants.FEED_TYPE.MY_BID:
                viewType = MY_BID;
                break;
            case Constants.FEED_TYPE.BIDDING_PROMOTION:
                viewType = BROKER_PROMOTE;
                break;
            case Constants.FEED_TYPE.FEED_POST_SEARCH:
                viewType = FEED_POST_SEARCH;
                break;
            case Constants.FEED_TYPE.WIDGETS:
                viewType = WIDGET_FEED;
                break;
            case Constants.FEED_TYPE.EXTERNAL_SOURCE:
                viewType = EXTERNAL_SOURCE;
                break;
            default:
                viewType = FEEDS_CARD;
                break;
        }

        Log.d(TAG, "View Type :" + viewType);
        return viewType;
    }

    @Override
    public void onBindViewHolder(final BaseFeedHolder holder, final
    int position) {

        int viewType = super.getItemViewType(position);
        FeedsCustomAdapter.ViewHolder viewHolder = null;

        FragmentManager mFragmentManager = getCurrentActivity().getFragmentManager();


        switch (viewType) {
            case FEEDS_CARD_DISABLED:

                holder.setmFragmentManager(mFragmentManager);
                holder.setRefKey(refKey);
                holder.setType(type);
                holder.setBookmarkDeleteListener(bookmarkDeleteListener);
                holder.onBindViewHolder(position, feeds.get(position));


                break;
            case TRIAL_VERSION_CARD:
                holder.setmFragmentManager(mFragmentManager);
                holder.setRefKey(refKey);
                holder.setType(type);
                holder.setBookmarkDeleteListener(bookmarkDeleteListener);
                holder.onBindViewHolder(position, feeds.get(position));


                break;
            case FEEDS_CARD:
                holder.setmFragmentManager(mFragmentManager);
                holder.setRefKey(refKey);
                holder.setType(type);
                holder.setBookmarkDeleteListener(bookmarkDeleteListener);
                holder.onBindViewHolder(position, feeds.get(position));

                Log.d(TAG, "Bind View Holder Shouttt");
                break;

            case BROKER_SINGLE_CARD:
                holder.onBindViewHolder(position, feeds.get(position));
                break;

            case BROKER_MULTI_CARD:
                holder.setTAG(TAG);
                holder.onBindViewHolder(position, feeds.get(position));
                break;
            case BUILDER_CARD:
                holder.onBindViewHolder(position, feeds.get(position));
                break;
            case NEWS_CARD:
                holder.onBindViewHolder(position, feeds.get(position));
                break;
            case PROGRESS_MORE:
                holder.onBindViewHolder(position, feeds.get(position));
                break;
            case NO_MORE:
                holder.onBindViewHolder(position, feeds.get(position));
                break;
            case NO_MORE_DATA:
                holder.onBindViewHolder(position, feeds.get(position));
                break;
            case RELATED_BROKER:
                holder.onBindViewHolder(position, feeds.get(position));
                break;
            case ADVERTISEMENT_FEED:
                holder.onBindViewHolder(position, feeds.get(position));
                break;
            case FEED_POST_SEARCH:
                holder.setmFragmentManager(mFragmentManager);
                holder.setRefKey(refKey);
                holder.setType(type);
                holder.setBookmarkDeleteListener(bookmarkDeleteListener);
                holder.onBindViewHolder(position, feeds.get(position));
                Log.d(TAG, "Bind View Holder Shout");
                break;

            default:
                holder.onBindViewHolder(position, feeds.get(position));
                break;
        }
        //viewHolder.post_content.setOnTouchListener((new LinkMovementMethodOverride()));
    }


    @Override
    public int getItemCount() {

        return feeds.size();
    }

    public void addItems(ArrayList<BaseFeed> feeds) {
        for (BaseFeed feeds1 : feeds) {
            addItem(feedsCount, feeds1);

        }

    }

    public void addItem(int position, BaseFeed feed) {
        if (position <= feeds.size()) {
            feeds.add(position, feed);
            notifyItemInserted(position);
            feedsCount++;
        }
    }

    public void removeItem(int position) {
        if (null != feeds && position < feeds.size()) {
            feeds.remove(position);
            notifyItemRemoved(position);
        }
    }


    private Activity getCurrentActivity() {
        return mActivity;
    }

    private Context getCurrentContext() {
        return mContext;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        searchText = charText;
        feeds.clear();

        if (arrayList.size() > 0) {
            if (charText.length() == 0) {
                feeds.addAll(arrayList);
            } else {
                for (BaseFeed gsd : arrayList) {
                    if (((Feeds) gsd).getProfile_name().toLowerCase(Locale.getDefault()).contains(charText) || ((Feeds) gsd).getText().toLowerCase(Locale.getDefault()).contains(charText)) {
                        feeds.add(gsd);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    View.OnTouchListener myTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View arg0, MotionEvent arg1) {

            return false;
        }

    };

    public int getCurrentItemSize() {
        return arrayList.size();
    }

    public void updateItems(ArrayList<BaseFeed> feeds, boolean addMore) {
        if (!addMore) {
            arrayList.clear();
        }
        arrayList.addAll(feeds);
    }

    public void OnReceive(Context context, Intent intent) {

    }


    public View getView() {
        //do some other stuff with the views
        return v;
    }


}