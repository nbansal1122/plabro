package com.plabro.realestate.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.activity.UserProfileNew;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.PBPreferences;

import java.util.HashMap;

/**
 * Created by nitin on 22/09/15.
 */
public class RelatedBrokerHolder extends BaseFeedHolder {

    private TextView brokerName;
    private ImageView brokerImage, callIcon;

    @Override
    public String getTAG() {
        return "Related Broker";
    }

    public RelatedBrokerHolder(View itemView) {
        super(itemView);
        brokerName = findTV(R.id.tv_broker_name);
        brokerImage = (ImageView) findView(R.id.iv_image_broker);
        callIcon = (ImageView) findView(R.id.iv_call_icon);
    }

    @Override
    public void onBindViewHolder(final int position, final BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        if (feed instanceof ProfileClass) {
            final ProfileClass f = (ProfileClass) feed;
            brokerName.setText(f.getName());
            ActivityUtils.loadImage(ctx, f.getImg(), brokerImage, R.drawable.profile_default_one);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // Analytics.trackScreen(R.string.related_click);
                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("ScreenName", TAG);
                    trackEvent(Analytics.CardActions.ViewProfile, position,f.get_id(), feed.getType(), wrData);
                    UserProfileNew.startActivity(ctx, f.getAuthorid(), f.getPhone(), "");
                }
            });
            callIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String phone = f.getPhone();
                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("ScreenName", TAG);
                    wrData.put("callFrom", PBPreferences.getPhone());
                    wrData.put("callTo", phone);
                    ActivityUtils.initiateCallDialog(ctx, feed, wrData);
//                    ActivityUtils.callPhone(ctx, phone, wrData);

                }
            });
        }
    }
}
