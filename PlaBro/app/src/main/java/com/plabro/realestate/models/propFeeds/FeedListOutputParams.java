package com.plabro.realestate.models.propFeeds;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class FeedListOutputParams {

    //List<Feeds> data = new ArrayList<Feeds>();
    List<FilterClass> filter_arr = new ArrayList<FilterClass>();
    List<BaseFeed> data = new ArrayList<BaseFeed>();

//    public List<Feeds> getData() {
//        return data;
//    }
//
//    public void setData(List<Feeds> data) {
//        this.data = data;
//    }

    public List<FilterClass> getFilter() {
        return filter_arr;
    }

    public List<FilterClass> getFilter_arr() {
        return filter_arr;
    }

    public void setFilter_arr(List<FilterClass> filter_arr) {
        this.filter_arr = filter_arr;
    }

    public List<BaseFeed> getData() {
        return data;
    }

    public void setData(List<BaseFeed> feedsData) {
        this.data = feedsData;
    }
}
