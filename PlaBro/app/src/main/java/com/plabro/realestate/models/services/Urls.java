package com.plabro.realestate.models.services;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Urls {

    @SerializedName("ejabber")
    @Expose
    private ServiceUrlData ejabber;
    @SerializedName("mybids")
    @Expose
    private ServiceUrlData myBids;

    public ServiceUrlData getMyBids() {
        return myBids;
    }

    public void setMyBids(ServiceUrlData myBids) {
        this.myBids = myBids;
    }

    public ServiceUrlData getPayment() {
        return payment;
    }

    public void setPayment(ServiceUrlData payment) {
        this.payment = payment;
    }

    public ServiceUrlData getEjabber() {
        return ejabber;
    }

    public void setEjabber(ServiceUrlData ejabber) {
        this.ejabber = ejabber;
    }

    @SerializedName("payment")
    @Expose
    private ServiceUrlData payment;


    public static class ServiceUrlData {
        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Expose
        @SerializedName("url")
        private String url;

        public String getExit_url() {
            return exit_url;
        }

        public void setExit_url(String exit_url) {
            this.exit_url = exit_url;
        }

        @Expose
        @SerializedName("exit_url")
        private String exit_url;
    }
}