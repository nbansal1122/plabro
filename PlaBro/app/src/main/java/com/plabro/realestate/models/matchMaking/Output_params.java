package com.plabro.realestate.models.matchMaking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Output_params {

    @SerializedName("data")
    @Expose
    private List<MatchMakingData> data = new ArrayList<MatchMakingData>();

    /**
     * @return The data
     */
    public List<MatchMakingData> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<MatchMakingData> data) {
        this.data = data;
    }

}