package com.plabro.realestate.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.ShoutByOthers;
import com.plabro.realestate.adapter.CustomPagerAdapter;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.fragments.UserEndorsements;
import com.plabro.realestate.fragments.UserInfo;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.follow_unfollow.FollowUnFollowResponse;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.profile.ProfileResponse;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.SlidingTabLayout;
import com.plabro.realestate.uiHelpers.ViewPagerIndicator;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by nitin on 28/07/15.
 */
public class ProfileEndorsements extends PlabroBaseActivity implements CustomPagerAdapter.PagerAdapterInterface<String>, VolleyListener {
    private ViewPagerIndicator pageIndicator;
    private ViewPager mViewPager;
    private List<String> tabs = new ArrayList<String>();
    private CustomPagerAdapter<String> adapter;

    private SlidingTabLayout mSlidingTabLayout;
    private ProgressDialog dialog;

    private String mAuthorId, mPhone;
    private ImageView userImg;
    private ProfileClass profile;
    private boolean isCurrentUser;
    private boolean isFollowedByMe;


    @Override
    protected String getTag() {
        return "ProfileEndorsements";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mAuthorId = bundle.getString("authorid");
            mPhone = bundle.getString("phone");
            isCurrentUser = isCurrentUser();
        } else {
            // isCurrentUser = true;
        }

        Log.d(TAG, "AuthorID :" + mAuthorId);
        Log.d(TAG, "Phone :" + mPhone);


        tabs.add("Details");
        tabs.add("Endorsements");
        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
//        profile = AppController.getInstance().getUserProfile();
        if (isCurrentUser) {
            //profile = AppController.getInstance().getUserProfile();
        } else {
//            if (null != mPhone)
//                profile = new Select().from(ProfileClass.class).where("phone = '" + mPhone + "'").executeSingle();
        }
        if (null != profile) {
            setViews(profile);
        }
        getProfile();

    }

    void setUpViewPager() {
        mViewPager = (ViewPager) findViewById(R.id.pager);

        adapter = new CustomPagerAdapter<String>(getSupportFragmentManager(), tabs, this);

        mViewPager.setAdapter(adapter);
        mSlidingTabLayout.setViewPager(mViewPager);

    }

    @Override
    public Fragment getFragmentItem(int position, String listItem) {
        Fragment f = null;
        switch (position) {
            case 0:
                f = UserInfo.getInstance(profile, isCurrentUser);
                break;
            case 1:
                f = UserEndorsements.getInstance(profile, isCurrentUser);
                break;
        }
        return f;
    }

    @Override
    public CharSequence getPageTitle(int position, String listItem) {
        return tabs.get(position);
    }

    private void getProfile() {
        HashMap<String, String> params = new HashMap<String, String>();
        if (!TextUtils.isEmpty(mAuthorId) || null != mAuthorId) {
            params.put(PlaBroApi.PLABRO_GET_PROFILE_AUTO_PARAM_CONTACT_ID, mAuthorId);
        }
        if (!TextUtils.isEmpty(mPhone) || null != mPhone) {
            params.put(PlaBroApi.PLABRO_GET_PROFILE_AUTO_PARAM_PHONE, mPhone);
        }
        params = Utility.getInitialParams(PlaBroApi.RT.GETPROFILE, params);
        AppVolley.processRequest(Constants.TASK_CODES.USER_PROFILE, ProfileResponse.class, null, PlaBroApi.getBaseUrl(this), params, RequestMethod.GET, this);
    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        hideProgressDialog();
        switch (taskCode) {
            case Constants.TASK_CODES.USER_PROFILE:
                ProfileResponse res = (ProfileResponse) response.getResponse();
                if ( null != res.getOutput_params()) {
                    profile = res.getOutput_params().getData();
                    if (null != profile) {
                        setViews(profile);
                    }
                }
                break;

            case Constants.TASK_CODES.FOLLOW:
                FollowUnFollowResponse followResponse = (FollowUnFollowResponse) response.getResponse();

                break;
            case Constants.TASK_CODES.UNFOLLOW:
                FollowUnFollowResponse unFollowResponse = (FollowUnFollowResponse) response.getResponse();
                break;
        }

    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        hideProgressDialog();
        onApiFailure(response, taskCode);

    }

    private void setViews(ProfileClass profile) {
        setHeader(profile);
        setUpViewPager();
    }

    private void setHeader(ProfileClass profile) {
        userImg = (ImageView) findViewById(R.id.iv_profileImage);
        String imgUrl = profile.getImg();
        if (imgUrl != null) {
            Picasso.with(ProfileEndorsements.this).load(imgUrl).placeholder(R.drawable.profile_default_one).into(userImg);
        }
        setText(R.id.tv_user_name, profile.getName() + " " + profile.getBusiness_name());
        setText(R.id.tv_user_addr, profile.getAddress() + "");
        setText(R.id.tv_last_seen, "Last seen " + profile.getTime());
        setText(R.id.tv_count_followers, profile.getFollowed() + " Followers");
        setText(R.id.tv_count_following, profile.getFollowing() + " Following");
        setText(R.id.tv_count_shouts, profile.getShout_count() + " Shouts");
        setClickListeners(R.id.tv_count_shouts, R.id.tv_count_followers, R.id.tv_count_following, R.id.iv_profileImage);
    }

    private boolean isCurrentUser() {
        String phone = PBPreferences.getPhone();
        if (null != phone && phone.equals(mPhone)) {
            return true;
        } else {
            ProfileClass userProfile = AppController.getInstance().getUserProfile();
            if (null != mAuthorId && mAuthorId.equals(userProfile.getAuthorid()))
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_count_followers:
                onFollowingClicked(false);
                break;
            case R.id.tv_count_following:
                onFollowingClicked(true);
                break;
            case R.id.tv_count_shouts:
                onOtherShoutsClicked();
                break;
            case R.id.iv_profileImage:
                break;
        }
    }

    private void onOtherShoutsClicked() {
        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        if (null != mAuthorId) {
            wrData.put("UserId", mAuthorId);
        }
        wrData.put("Action", "View Others Shouts");


        Intent intent = new Intent(this, ShoutByOthers.class);
        intent.putExtra("contactId", mAuthorId);
        if (null != profile) {
            intent.putExtra("contactName", profile.getName() + "");
        }
        startActivity(intent);
    }

    private void onFollowingClicked(boolean isFollowingClicked) {
        Intent intent = new Intent(this, Followers.class);
        HashMap<String, Object> data = new HashMap<>();
        data.put("ScreenName", TAG);
        intent.putExtra("authorid", mAuthorId);
        if (isFollowingClicked) {
            data.put("Action", "Following Clicked");
            intent.putExtra("type", "following");
        } else {
            data.put("Action", "Followers Clicked");
            intent.putExtra("type", "followers");
        }
        startActivity(intent);
    }

    private void onFollowButtonClicked() {
        HashMap<String, Object> wrData = new HashMap<>();
        wrData.put("ScreenName", TAG);
        if (null != mAuthorId) {
            wrData.put("UserId", mAuthorId);
        }
        if (isFollowedByMe) {
            wrData.put("Action", "Follow");
        } else {
            wrData.put("Action", "Un Follow");
        }
        follow_user(isFollowedByMe);
    }

    private void follow_user(boolean isFollowedByMe) {

        HashMap<String, String> params = new HashMap<String, String>();

        params.put(PlaBroApi.PLABRO_FOLLOW_UNFOLLOW_POST_PARAM_CONTACT_ID, mAuthorId);

        int taskCode = 1;
        if (isFollowedByMe) {
            taskCode = Constants.TASK_CODES.FOLLOW;
            params = Utility.getInitialParams(PlaBroApi.RT.FOLLOW, params);
        } else {
            taskCode = Constants.TASK_CODES.UNFOLLOW;
            params = Utility.getInitialParams(PlaBroApi.RT.UN_FOLLOW, params);
        }


        AppVolley.processRequest(taskCode, FollowUnFollowResponse.class, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, this);
    }

    private void showProgressDialog() {
        if (null != dialog && dialog.isShowing()) {
        } else {
            dialog = new ProgressDialog(ProfileEndorsements.this);
            dialog.setMessage("Loading...");
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    private void hideProgressDialog() {
        if (null != dialog && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void findViewById() {

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }
}
