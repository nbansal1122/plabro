package com.plabro.realestate.models.propFeeds;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.utilities.JSONUtils;

/**
 * Created by nitin on 18/08/15.
 */
@Table(name = "NewsFeed")
public class NewsFeed extends BaseFeed {
    @Column
    private String _id;
    private String summ;
    private String text;
    private String time;
    private String img;
    private boolean isExpanded;

    public String getImg() {
        return img;
    }

    public void setImg_url(String img_url) {
        this.img = img_url;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSumm() {
        return summ;
    }

    public void setSumm(String summ) {
        this.summ = summ;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public static NewsFeed parseJson(String json) {
        Log.d("NewsFeed", "News JSON :: "+json);
        return JSONUtils.getGSONBuilder().create().fromJson(json, NewsFeed.class);
    }


    public boolean isExpanded() {
        return isExpanded;
    }

    public void setIsExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }
}
