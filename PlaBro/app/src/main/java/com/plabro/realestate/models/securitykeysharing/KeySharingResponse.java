package com.plabro.realestate.models.securitykeysharing;


import com.plabro.realestate.models.ServerResponse;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class KeySharingResponse extends ServerResponse {

    KeySharingOutputParams output_params = new KeySharingOutputParams();


    public KeySharingOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(KeySharingOutputParams output_params) {
        this.output_params = output_params;
    }

}
