package com.plabro.realestate.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;

/**
 * Created by nitin on 20/11/15.
 */
public class PaymentDialogFragment extends DialogFragment implements View.OnClickListener {

    private String title, message, okText, cancelText;
    private boolean isCancelReq;
    private DialogListener listener;

    public static PaymentDialogFragment getInstance(String title, String message, String okText, String cancelText, boolean isCancelReq, DialogListener listener) {
        PaymentDialogFragment f = new PaymentDialogFragment();
        f.cancelText = cancelText;
        f.isCancelReq = isCancelReq;
        f.message = message;
        f.okText = okText;
        f.title = title;
        f.listener = listener;
        f.setCancelable(false);
        return f;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = View.inflate(getActivity(), R.layout.dialog_payment, null);
        builder.setView(v);
        setText(v, R.id.tv_dialog_title, title);
        setText(v, R.id.tv_dialog_message, message);
        setText(v, R.id.tv_dialog_ok, okText);
        builder.setCancelable(false);
        v.findViewById(R.id.tv_dialog_ok).setOnClickListener(this);
        if (isCancelReq) {
            setText(v, R.id.tv_dialog_cancel, cancelText);
            v.findViewById(R.id.tv_dialog_cancel).setOnClickListener(this);
        } else {
            v.findViewById(R.id.view_sep).setVisibility(View.GONE);
            v.findViewById(R.id.tv_dialog_cancel).setVisibility(View.GONE);
        }

        return builder.create();
    }

    private void setText(View v, int id, String text) {
        TextView tv = (TextView) v.findViewById(id);
        tv.setText(text);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_dialog_ok:
                dismiss();
                if (listener != null) {
                    listener.onOKClicked();
                }
                break;
            case R.id.tv_dialog_cancel:
                dismiss();
                if (listener != null) {
                    listener.onCancelClicked();
                }
                break;
        }
    }

    public static interface DialogListener {
        public void onOKClicked();

        public void onCancelClicked();
    }
}
