package com.plabro.realestate.holders;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.activity.Profile;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.MultiBrokerFeed;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.ActivityUtils;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

/**
 * Created by hemantkumar on 17/08/15.
 */
public class MultiBrokerCardHolder extends BaseFeedHolder {
    /*for multiple brokers*/
    private final CardView mBrokerOne;
    private final CardView mBrokerTwo;
    private final CardView mBrokerThree;
    private final Button mMultiMoreBrokers;
    private final TextView mMultiBrokersTitle;
    private final TextView mMultiBrokersSubTitle;


    public MultiBrokerCardHolder(View itemView) {
        super(itemView);

          /*for multiple broker*/
        mBrokerOne = (CardView) itemView.findViewById(R.id.ll_broker_one);
        mBrokerTwo = (CardView) itemView.findViewById(R.id.ll_broker_two);
        mBrokerThree = (CardView) itemView.findViewById(R.id.ll_broker_three);
        mMultiMoreBrokers = (Button) itemView.findViewById(R.id.bt_more_brokers);
        mMultiBrokersTitle = (TextView) itemView.findViewById(R.id.broker_title);
        mMultiBrokersSubTitle = (TextView) itemView.findViewById(R.id.broker_sub_title);

    }


    @Override
    public void onBindViewHolder(final int position, BaseFeed feed) {
        super.onBindViewHolder(position, feed);
        if (feed instanceof MultiBrokerFeed) {
            MultiBrokerCardHolder holder = this;

            final MultiBrokerFeed multiBrokerFeed = (MultiBrokerFeed) feed;
            final List<ProfileClass> brokersList = multiBrokerFeed.getBrokers();
            //String imageUrl = "https://images-na.ssl-images-amazon.com/images/I/51Tdmu3B3RL._PJStripe-Prime-Only-500px,TopLeft,0,0._AC_SY220_.jpg";

            if (brokersList.size() > 3) {
                holder.mMultiMoreBrokers.setVisibility(View.VISIBLE);
            }

            holder.mMultiBrokersTitle.setText(multiBrokerFeed.getTitle());
            holder.mMultiBrokersSubTitle.setText(multiBrokerFeed.getSubTitle());


            String brokerImg_one = brokersList.get(0).getImg();
            String brokerImg_two = brokersList.get(1).getImg();
            String brokerImg_three = brokersList.get(2).getImg();


//                String brokerImg_one = imageUrl;
//                String brokerImg_two = imageUrl;
//                String brokerImg_three = imageUrl;

            ImageView imageViewOne = (ImageView) holder.mBrokerOne.findViewById(R.id.iv_image_broker);
            ImageView imageViewTwo = (ImageView) holder.mBrokerTwo.findViewById(R.id.iv_image_broker);
            ImageView imageViewThree = (ImageView) holder.mBrokerThree.findViewById(R.id.iv_image_broker);

            if (null != brokerImg_one && !TextUtils.isEmpty(brokerImg_one)) {
                Picasso.with(ctx).load(brokerImg_one).placeholder(R.drawable.profile_default_one).into(imageViewOne);
            }

            if (null != brokerImg_two && !TextUtils.isEmpty(brokerImg_two)) {
                Picasso.with(ctx).load(brokerImg_two).placeholder(R.drawable.profile_default_one).into(imageViewTwo);
            }

            if (null != brokerImg_three && !TextUtils.isEmpty(brokerImg_three)) {
                Picasso.with(ctx).load(brokerImg_three).placeholder(R.drawable.profile_default_one).into(imageViewThree);
            }


            ((TextView) holder.mBrokerOne.findViewById(R.id.tv_broker_name)).setText(brokersList.get(0).getName());
            ((TextView) holder.mBrokerTwo.findViewById(R.id.tv_broker_name)).setText(brokersList.get(1).getName());
            ((TextView) holder.mBrokerThree.findViewById(R.id.tv_broker_name)).setText(brokersList.get(2).getName());

            holder.mBrokerOne.findViewById(R.id.btn_call_broker).setTag(brokersList.get(0).getPhone());
            holder.mBrokerTwo.findViewById(R.id.btn_call_broker).setTag(brokersList.get(1).getPhone());
            holder.mBrokerThree.findViewById(R.id.btn_call_broker).setTag(brokersList.get(2).getPhone());

            holder.mBrokerOne.findViewById(R.id.btn_call_broker).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String phone = (String) view.getTag();
                    ActivityUtils.callPhone(ctx, phone);
                }
            });
            holder.mBrokerTwo.findViewById(R.id.btn_call_broker).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String phone = (String) view.getTag();
                    ActivityUtils.callPhone(ctx, phone);
                }
            });
            holder.mBrokerThree.findViewById(R.id.btn_call_broker).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String phone = (String) view.getTag();
                    ActivityUtils.callPhone(ctx, phone);
                }
            });


            holder.mBrokerOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("ScreenName", getTAG());
                    wrData.put("Action", "1st Broker Clicked");

                    trackEvent(Analytics.CardActions.ViewProfile, position, multiBrokerFeed.get_id(), multiBrokerFeed.getType(), null);


                    Intent intent1 = new Intent(ctx, Profile.class);
                    intent1.putExtra("authorid", brokersList.get(0).getAuthorid());
                    ctx.startActivity(intent1);
                }
            });
            holder.mBrokerTwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("ScreenName", getTAG());
                    wrData.put("Action", "2nd Broker Clicked");

                    trackEvent(Analytics.CardActions.ViewProfile, position, multiBrokerFeed.get_id(), multiBrokerFeed.getType(), null);


                    Intent intent1 = new Intent(ctx, Profile.class);
                    intent1.putExtra("authorid", brokersList.get(1).getAuthorid());
                    ctx.startActivity(intent1);
                }
            });
            holder.mBrokerThree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HashMap<String, Object> wrData = new HashMap<String, Object>();
                    wrData.put("ScreenName", getTAG());
                    wrData.put("Action", "3rd Broker Clicked");
                    trackEvent(Analytics.CardActions.ViewProfile, position, multiBrokerFeed.get_id(), multiBrokerFeed.getType(), null);

                    Intent intent1 = new Intent(ctx, Profile.class);
                    intent1.putExtra("authorid", brokersList.get(2).getAuthorid());
                    ctx.startActivity(intent1);
                }
            });


        } else {
            return;
        }
    }

}
