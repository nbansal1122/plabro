package com.plabro.realestate.models.profile.builder;

import com.google.gson.annotations.Expose;

public class BuilderProfile {

    @Expose
    private String username;
    @Expose
    private String company_logo;
    @Expose
    private Integer builder_id;
    @Expose
    private String phone;
    @Expose
    private String company_name;
    @Expose
    private String company_dp;

    /**
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return The company_logo
     */
    public String getCompany_logo() {
        return company_logo;
    }

    /**
     * @param company_logo The company_logo
     */
    public void setCompany_logo(String company_logo) {
        this.company_logo = company_logo;
    }

    /**
     * @return The builder_id
     */
    public Integer getBuilder_id() {
        return builder_id;
    }

    /**
     * @param builder_id The builder_id
     */
    public void setBuilder_id(Integer builder_id) {
        this.builder_id = builder_id;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The company_name
     */
    public String getCompany_name() {
        return company_name;
    }

    /**
     * @param company_name The company_name
     */
    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    /**
     * @return The company_dp
     */
    public String getCompany_dp() {
        return company_dp;
    }

    /**
     * @param company_dp The company_dp
     */
    public void setCompany_dp(String company_dp) {
        this.company_dp = company_dp;
    }

}