package com.plabro.realestate.fragments.follow;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.activity.RelatedTagFeeds;
import com.plabro.realestate.adapter.CustomListAdapter;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.followlocation.FollowListResponse;
import com.plabro.realestate.models.followlocation.FollwLocationData;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.uiHelpers.RoundedImageView;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.ProgressWheel;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nitin on 18/02/16.
 */
public class LocationTagFragment extends PlabroBaseFragment implements CustomListAdapter.CustomListAdapterInterface, VolleyListener, AdapterView.OnItemClickListener {
    private ProgressWheel mProgressWheel;
    private RelativeLayout emptyView;
    private ListView locationsListView;
    private List<FollwLocationData> locations = new ArrayList<>();
    private CustomListAdapter<FollwLocationData> listAdapter;


    @Override
    public String getTAG() {
        return "Followed Locations";
    }

    @Override
    protected void findViewById() {
        Analytics.trackScreen(Analytics.UserProfile.FollowLocation);
        mProgressWheel = (ProgressWheel) findView(R.id.progress_wheel);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);

        setRefreshLayout();
        emptyView = (RelativeLayout) findView(android.R.id.empty);

        TextView tv = (TextView) emptyView.findViewById(R.id.tv_empty);
        tv.setText(getString(R.string.no_location_followed));
//        setText(R.id.tv_empty, getString(R.string.no_location_followed));
        // Locate the ListView in listview_main.xml
        locationsListView = (ListView) findView(R.id.listview);
        setAdapter();
        getLocationsList();
    }

    private void getLocationsList() {

        if (!mSwipeRefreshLayout.isRefreshing()) {
            mProgressWheel.spin();
        }
        ParamObject obj = new ParamObject();
        HashMap<String, String> map = new HashMap<>();
        map.put("follow_type", "hashtag");
        map = Utility.getInitialParams(PlaBroApi.RT.FOLLOWING_LIST, map);
        obj.setParams(map);
        obj.setClassType(FollowListResponse.class);
        obj.setTaskCode(Constants.TASK_CODES.FOLLOW);
        AppVolley.processRequest(obj, this);
    }

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private void setRefreshLayout() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findView(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "GetData Refresh");
                        getLocationsList();
                    }
                }, Constants.GOOGLE_PULL_TO_REFRESH_TIMER);

            }
        });
//        } else {
//            ((ViewGroup) rootView.getParent()).removeView(rootView);
//        }


        //pull to refresh

        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void setAdapter() {
        listAdapter = new CustomListAdapter<>(getActivity(), R.layout.row_follow, locations, this);
        locationsListView.setAdapter(listAdapter);
        locationsListView.setEmptyView(emptyView);
        locationsListView.setOnItemClickListener(this);
    }

    @Override
    protected int getViewId() {
        return R.layout.activity_followers;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getActivity()).inflate(resourceID, null);
        }
        TextView locationHashTag = (TextView) convertView.findViewById(R.id.tv_name);
//        locationHashTag.setText(locations.get(position).getUid_());
        String text = locations.get(position).getUid_();
        SpannableString str = new SpannableString(locations.get(position).getUid_());
        str.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.appColor3)), 0, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        locationHashTag.setText(str);
        String imgUrl = locations.get(position).getImage();
        RoundedImageView iv = (RoundedImageView) convertView.findViewById(R.id.iv_follow);
        if (TextUtils.isEmpty(imgUrl)) {
            iv.setImageResource(R.drawable.direct_inventory_icon_default);
        } else {
            ActivityUtils.loadImage(getActivity(), imgUrl, iv, R.drawable.direct_inventory_icon_default);
        }
        return convertView;
    }

    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        if (getActivity() == null) return;
        mSwipeRefreshLayout.setRefreshing(false);
        mProgressWheel.stopSpinning();
        FollowListResponse r = (FollowListResponse) response.getResponse();
        if (r != null && r.getOutput_params() != null && r.getOutput_params().getData() != null) {
            List<FollwLocationData> list = r.getOutput_params().getData();
            if (list != null && list.size() > 0) {
                locations.clear();
                locations.addAll(list);
                listAdapter.notifyDataSetChanged();
            }
        }

    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        if (getActivity() == null) return;
        mSwipeRefreshLayout.setRefreshing(false);
        mProgressWheel.stopSpinning();
        Log.d(TAG, "on Failure : Response = " + response);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        FollwLocationData data = locations.get(i);
        Intent intent = new Intent(getActivity(), RelatedTagFeeds.class);
        intent.putExtra("hashTitle", data.getUid_());
        intent.putExtra("type", "follow_location");
        startActivity(intent);
    }

}
