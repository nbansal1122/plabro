package com.plabro.realestate.adapter;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.holders.BaseFeedHolder;
import com.plabro.realestate.holders.BaseNotificationHolder;
import com.plabro.realestate.holders.CallHolder;
import com.plabro.realestate.holders.ProgressHolder;
import com.plabro.realestate.models.Calls.CallData;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.utilities.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by nitin on 23/10/15.
 */
public class CallLogAdapter extends RecyclerView.Adapter<BaseFeedHolder> {
    public static final int CALL_NOTIFICATION = 1;

    public static final int PROGRESS_MORE = -2;
    public static final int NO_MORE = -3;
    public static final int NO_MORE_DATA = -4;


    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    private String searchText = "";


    private Context mContext;
    private Activity mActivity;
    private int feedsCount = 0;
    private static final String TAG = "CAllLogAdapter";
    private List<BaseFeed> notifications = new ArrayList<BaseFeed>();
    private List<BaseFeed> arrayList = new ArrayList<BaseFeed>();
    private String type = "";
    private String refKey;

    public class ViewHolder extends BaseNotificationHolder {


        public ViewHolder(View v) {
            super(v);
        }

    }


    public CallLogAdapter(ArrayList<BaseFeed> notifications, Activity mActivity, String type) {
        this.notifications = notifications;
        this.type = type;
        this.mContext = mActivity;
        this.mActivity = mActivity;
        feedsCount = notifications.size();
        this.type = type;
        arrayList.addAll(notifications);
    }


    @Override
    public BaseFeedHolder onCreateViewHolder(ViewGroup viewGroup, final int viewType) {
        View v = null;
        BaseFeedHolder nh = null;
        switch (viewType) {


            case PROGRESS_MORE:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.loader_layout, viewGroup, false);
                ProgressHolder ph = new ProgressHolder(v);
                ph.setType("progress");
                return ph;
            case NO_MORE:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.loader_layout, viewGroup, false);
                ProgressHolder phn = new ProgressHolder(v);
                phn.setType("noMore");
                return phn;
            case NO_MORE_DATA:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.loader_layout, viewGroup, false);
                ProgressHolder phnd = new ProgressHolder(v);
                phnd.setType(Constants.NOTIFICATION_TYPE.NO_MORE_DATA);
                return phnd;

            case CALL_NOTIFICATION:
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.row_call_info, viewGroup, false);
                nh = new CallHolder(v);
                return nh;

        }
        return nh;
    }


    @Override
    public int getItemViewType(int position) {

        int viewType = CALL_NOTIFICATION;

        switch (notifications.get(position).getType()) {
            default:
                viewType = CALL_NOTIFICATION;
                break;

            case Constants.NOTIFICATION_TYPE.PROGRESS_MORE:
                viewType = PROGRESS_MORE;
                break;
            case Constants.NOTIFICATION_TYPE.NO_MORE:
                viewType = NO_MORE;
                break;
            case Constants.NOTIFICATION_TYPE.NO_MORE_DATA:
                viewType = NO_MORE_DATA;
                break;

        }

        return viewType;
    }

    @Override
    public void onBindViewHolder(final BaseFeedHolder holder, final int position) {

        int viewType = getItemViewType(position);
        FragmentManager mFragmentManager = getCurrentActivity().getFragmentManager();

        switch (viewType) {
            case PROGRESS_MORE:
                holder.onBindViewHolder(position, notifications.get(position));
                break;
            case NO_MORE:
                holder.onBindViewHolder(position, notifications.get(position));
            case NO_MORE_DATA:
                holder.onBindViewHolder(position, notifications.get(position));
                break;
            case CALL_NOTIFICATION:
                Log.d(TAG, "Bind View HOlder Call Log");

                holder.setmFragmentManager(mFragmentManager);
                holder.onBindViewHolder(position, notifications.get(position));
                break;

        }
    }


    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public void addItems(ArrayList<CallData> notifications) {
        for (CallData notification : notifications) {
            addItem(feedsCount, notification);
        }

    }

    public void addItem(int position, CallData notification) {
        if (position <= notifications.size()) {
            notifications.add(position, notification);
            notifyItemInserted(position);
            feedsCount++;
        }
    }

    public void removeItem(int position) {
        notifications.remove(position);
        notifyItemRemoved(position);
    }


    private Activity getCurrentActivity() {
        return mActivity;
    }

    private Context getCurrentContext() {
        return mContext;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        searchText = charText;
//        arrayList.clear();
//        arrayList.addAll(notifications);
        notifications.clear();

        if (arrayList.size() > 0) {
            if (charText.length() == 0) {
                notifications.addAll(arrayList);
            } else {
                for (BaseFeed f : arrayList) {
                    if (f instanceof CallData) {
                        CallData callUtil = (CallData) f;
                        if (callUtil.getName().contains(searchText)) {
                            notifications.add(f);
                        }
                    }
                }
//                notifications.add(arrayList.get(arrayList.size() - 1));
            }
        }
        notifyDataSetChanged();
    }


    public int getCurrentItemSize() {
        return arrayList.size();
    }

    public void updateItems(ArrayList<CallData> feeds, boolean addMore) {
        if (!addMore)
            arrayList.clear();
        arrayList.addAll(feeds);
    }

    public void OnReceive(Context context, Intent intent) {

    }

}
