package com.plabro.realestate.models.location_auto;


import com.plabro.realestate.models.ServerResponse;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class LocationAutoResponse extends ServerResponse {

    LocationListAutoOutputParams output_params = new LocationListAutoOutputParams();


    public LocationListAutoOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(LocationListAutoOutputParams output_params) {
        this.output_params = output_params;
    }
}
