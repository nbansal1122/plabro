package com.plabro.realestate.models.city;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.plabro.realestate.AppController;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.Utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hemantkumar on 08/01/15.
 */
@Table(name = "CityList")
public class CityClass extends Model {
    @Column(unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String _id;
    @Column(unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String city_name;
    @Column
    private String city_type;


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getCity_type() {
        return city_type;
    }

    public void setCity_type(String city_type) {
        this.city_type = city_type;
    }

    public static List<CityClass> getCitiesList() {
        List<CityClass> cities = new Select().from(CityClass.class).orderBy("city_name").execute();
        if (null != cities && cities.size() > 0) {
            return cities;
        } else {
            return getCitiesList(Constants.FILE_CITY_JSON);
        }
    }

    public static List<CityClass> getCitiesList(String fileName) {
        try {
            InputStream inputStream = AppController.getInstance().getAssets().open(fileName);
            String json = Utility.convertStreamToString(inputStream);
            if (Constants.FILE_SHOUT_CITY_JSON.equalsIgnoreCase(fileName)) {
                return CityResponse.parseJson(json, false);
            }
            return CityResponse.parseJson(json);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }
        return new ArrayList<>();
    }


}
