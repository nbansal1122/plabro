package com.plabro.realestate.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.payu.india.Payu.PayuConstants;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.fragments.PaymentDialogFragment;
import com.plabro.realestate.fragments.services.ParentServiceFragment;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.payment.PaymentResponse;
import com.plabro.realestate.models.services.Question;
import com.plabro.realestate.models.services.S_data;
import com.plabro.realestate.models.services.Services_list;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by nitin on 24/11/15.
 */
public class BaseServiceActivity extends PlabroBaseActivity implements VolleyListener {

    private static final int REQ_UPFRONT_PAYMENT = 1;
    private List<ParentServiceFragment> fragmentList = new ArrayList<>();
    List<Services_list> servicesLists = new ArrayList<>();
    int top = -1;
    private Button nextButton, prevButton;
    private ParentServiceFragment currentFragment;
    private Services_list service;

    public static void startActivity(Context context, Services_list service) {
        Bundle b = new Bundle();
        b.putSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE, service);
        Intent serviceIntent = new Intent(context, BaseServiceActivity.class);
        serviceIntent.putExtras(b);
        context.startActivity(serviceIntent);
    }

    @Override
    protected int getResourceId() {
        return R.layout.fragment_service;
    }

    @Override
    protected String getTag() {
        return "Service Screen";
    }

    @Override
    public void findViewById() {
        service = (Services_list) getIntent().getExtras().getSerializable(Constants.BUNDLE_KEYS.SERIALIZABLE);
        inflateToolbar();
        setTitle(service.getS_data().getService_type() + "");
        nextButton = (Button) findViewById(R.id.btn_next);
        prevButton = (Button) findViewById(R.id.btn_prev);
        setClickListeners(R.id.btn_prev, R.id.btn_next);
        setFragmentList(service.getS_data());
        pushFragment();

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_prev:
                popFragment();
                break;
            case R.id.btn_next:
                Log.d(TAG, "Next Clicked");
                if (top == fragmentList.size() - 1) {
                    Log.d(TAG, "Top = Final");
                    if (null != currentFragment && currentFragment.onNextClicked())
                        if (service.getS_data().getUpfront_payment() == true) {
                            sendPaymentRT("service_cost");
                        } else {
                            sendServiceResponse();
                        }

                } else {
                    Log.d(TAG, "Top != Final");
                    if (null != currentFragment && currentFragment.onNextClicked()) {
                        pushFragment();
                    }
                }

                break;
        }
    }

    private void sendPaymentRT(final String action) {
        ParamObject obj = new ParamObject();
        obj.setTaskCode(Constants.TASK_CODES.SERVICE_PAYMENT);
        HashMap<String, String> params = new HashMap<>();
        params.put("action", action);
        params.put("state", "initiated");
        params.put("service_code", service.getService_code());
        params.put("payment_mode", "wallet");
        String serviceResponseData = getServiceResponseData();
        Log.d(TAG, "ServiceResponseData:" + serviceResponseData);
        params.put("service_data", serviceResponseData);
        params = Utility.getInitialParams(PlaBroApi.RT.PAYMENT, params);
        obj.setRequestMethod(RequestMethod.POST);
        obj.setClassType(PaymentResponse.class);
        obj.setParams(params);
        showDialog();
        HashMap<String, Object> metaData = new HashMap<>();
        metaData.putAll(params);
        Analytics.trackServiceEvent(Analytics.Actions.ServicePurchased, metaData);
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                dismissDialog();
                Log.d(TAG, "Response is :" + response.getResponse());
                PaymentResponse paymentResponse = (PaymentResponse) response.getResponse();
                if (paymentResponse != null) {

                    float balance = paymentResponse.getOutput_params().getData().getBalance();
                    float amount = paymentResponse.getOutput_params().getData().getAmount();
                    amount = (float) Math.ceil(amount);
                    if (amount > balance) {
                        showPaymentDialog(paymentResponse.getOutput_params().getData().getInfo(), (float) Math.ceil(amount - balance), balance);
                    } else {
                        if ("debit".equals(action)) {
                            sendServiceResponse();
                        } else {
                            showDeductionDialog(service.getS_data().getService_title(), amount, balance);
                        }
                    }
                }
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                dismissDialog();
            }
        });

    }

    private void showDeductionDialog(String msg, final float amount, final float balance) {
        String title = String.format("Your current balance is Rs %s\n\nAn amount of Rs. %s will be deducted from your balance amount", String.valueOf(balance), String.valueOf(amount));
        PaymentDialogFragment f = PaymentDialogFragment.getInstance(msg, title, "OK", "CANCEL", true, new PaymentDialogFragment.DialogListener() {
            @Override
            public void onOKClicked() {
                sendPaymentRT("debit");
            }

            @Override
            public void onCancelClicked() {

            }
        });
        f.show(getSupportFragmentManager(), "Payment");
    }

    private void showPaymentDialog(String msg, final float rechargeAmount, final float balance) {
        PaymentDialogFragment f = PaymentDialogFragment.getInstance(msg, String.format("Recharge your wallet with amount of Rs %s to avail %s", String.valueOf(rechargeAmount), service.getS_data().getService_title()), "OK", "CANCEL", true, new PaymentDialogFragment.DialogListener() {
            @Override
            public void onOKClicked() {
                RechargeWallet.startRechargeWallet(BaseServiceActivity.this, String.valueOf(balance), rechargeAmount);
            }

            @Override
            public void onCancelClicked() {

            }
        });
        f.show(getSupportFragmentManager(), "Payment");
    }

    private String getServiceResponseData() {
        try {
            List<String> paymentparams = service.getS_data().getPayment_params();

            if (paymentparams != null && paymentparams.size() > 0) {
                HashSet<String> paramsSet = new HashSet<>();
                paramsSet.addAll(paymentparams);
                JSONObject serviceResponseObject = new JSONObject();

                for (ParentServiceFragment f : fragmentList) {
                    JSONObject object = f.getServiceResponseArray();
                    Iterator<String> it = object.keys();
                    while (it.hasNext()) {
                        String key = it.next();
                        if (paramsSet.contains(key)) {
                            serviceResponseObject.put(key, object.get(key));
                        }
                    }
                }
                return serviceResponseObject.toString();
            } else {
            }
        } catch (Exception e) {

        }
        return "";
    }

    private void sendServiceResponse() {
        try {
            showDialog();
            Log.d(TAG, "Sending Service Response");
            ParamObject obj = new ParamObject();
            obj.setRequestMethod(RequestMethod.POST);
            HashMap<String, String> map = new HashMap<>();
            map.put("service_data", getServiceResponseJson());
            obj.setParams(Utility.getInitialParams(PlaBroApi.RT.HANDLE_SERVICE, map));
            AppVolley.processRequest(obj, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getServiceResponseJson() throws Exception {
        JSONObject root = new JSONObject();
        JSONArray responseArray = new JSONArray();
        for (ParentServiceFragment f : fragmentList) {
            responseArray.put(f.getServiceResponseArray());
        }
        root.put("service_code", service.getService_code());
        root.put("service_type", service.getType());
        root.put("Q", responseArray);
        Log.d(TAG, "Service Data:" + root);
        return root.toString();
    }

    @Override
    public void reloadRequest() {

    }

    private void setFragmentList(S_data data) {
        List<List<Question>> questions = data.getQuestionsList();
        fragmentList.clear();
        for (List<Question> q : questions) {
            ParentServiceFragment f = ParentServiceFragment.getInstance(q);
            fragmentList.add(f);
        }
    }

    @Override
    public void onBackPressed() {
        if (!popFragment()) {
            super.onBackPressed();
        }
    }


    private void pushFragment() {
        if (top == -1 || top < fragmentList.size() - 1) {
            top++;
            ParentServiceFragment f = fragmentList.get(top);
            insertFragment(f);
            setNextPreviousViews();
        } else {
            return;
        }

    }

    private void setNextPreviousViews() {
        showViews(R.id.btn_prev);
        nextButton.setText("NEXT");
        if (top == 0) {
            hideViews(R.id.btn_prev);
        } else if (top == fragmentList.size() - 1) {
            nextButton.setText("SUBMIT");
        }
    }

    private boolean popFragment() {
        if (top == 0) {
            return false;
        }
        top = top - 1;
        insertFragment(fragmentList.get(top));
        setNextPreviousViews();
        return true;
    }

    private void insertFragment(ParentServiceFragment f) {
        currentFragment = f;
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_service_fragment, f).commitAllowingStateLoss();
    }


    @Override
    public void onSuccessResponse(PBResponse response, int taskCode) {
        Log.d(TAG, "Response is:" + response.getResponse());
        dismissDialog();
        showToast("Your response has been submitted, we will call get back to you shortly");
        finish();
    }

    @Override
    public void onFailureResponse(PBResponse response, int taskCode) {
        dismissDialog();
        Log.d(TAG, "On Failure:" + response.getCustomException().getMessage());
        showToast(response.getCustomException().getMessage());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) return;
        switch (requestCode) {
            case PayuConstants.PAYU_REQUEST_CODE:
                sendPaymentRT("debit");
                break;
        }
    }
}
