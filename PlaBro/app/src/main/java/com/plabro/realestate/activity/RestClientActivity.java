package com.plabro.realestate.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nitin on 15/10/15.
 */
public class RestClientActivity extends PlabroBaseActivity {
    private ArrayList<RCParam> list = new ArrayList<>();
    RequestMethod methodType = RequestMethod.GET;
    private View dialogView;
    private AutoCompleteTextView rtET;
    private String URL = PBPreferences.getBaseUrl();
    private HashMap<String, String> params = new HashMap<>();
    private LinearLayout paramLayout;

    private ArrayList<String> rtList = new ArrayList<>();

    @Override
    protected int getResourceId() {
        return R.layout.activity_rest_client;
    }

    @Override
    protected String getTag() {
        return "Rest Client";
    }

    @Override
    public void findViewById() {
        rtET = (AutoCompleteTextView) findViewById(R.id.actv_rt);
        paramLayout = (LinearLayout) findViewById(R.id.ll_params);
        setClickListeners(R.id.btn_addParam, R.id.btn_submit, R.id.rb_get, R.id.rb_post);
        initRTList();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, rtList);
        rtET.setAdapter(adapter);
    }

    private void initRTList() {
        try {
            rtList.clear();
            Field[] interfaceFields = PlaBroApi.RT.class.getFields();
            for (Field f : interfaceFields) {
                String rt = f.get(null) + "";
                Log.d(TAG, "RT is :" + rt);
                rtList.add(rt);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btn_addParam:
                showDialogForParam();
                break;
            case R.id.btn_submit:
                String rt = rtET.getText().toString().trim();
                if (!TextUtils.isEmpty(rt)) {
                    sendRequest(rt);
                } else {
                    showToast("RT can not be empty");
                }
                break;
            case R.id.rb_get:
                methodType = RequestMethod.GET;
                break;
            case R.id.rb_post:
                methodType = RequestMethod.POST;
                break;
        }
    }


    private void sendRequest(String rt) {
        showDialog();
        for (RCParam p : list) {
            params.put(p.key, p.value);
        }
        ParamObject paramObject = new ParamObject();
        paramObject.setRequestMethod(methodType);
        paramObject.setParams(Utility.getInitialParams(rt, params));
        AppVolley.processRequest(paramObject, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                dismissDialog();
                showResultDialogView(response, false);
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
                dismissDialog();
                showResultDialogView(response, true);
            }
        });
    }

    private void showDialogForParam() {
        dialogView = LayoutInflater.from(this).inflate(R.layout.row_param, null);
        final EditText keyET = (EditText) dialogView.findViewById(R.id.et_paramKey);
        final EditText valueET = (EditText) dialogView.findViewById(R.id.et_paramValue);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        builder.setCancelable(false);
        builder.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                boolean flag = true;
                String key = keyET.getText().toString().trim();
                String value = valueET.getText().toString().trim();
                if (TextUtils.isEmpty(key)) {
                    flag = false;
                    dialogView.findViewById(R.id.tv_error_param_key).setVisibility(View.VISIBLE);
                } else {
                    dialogView.findViewById(R.id.tv_error_param_key).setVisibility(View.GONE);
                }
                if (TextUtils.isEmpty(value)) {
                    flag = false;
                    dialogView.findViewById(R.id.tv_error_param_value).setVisibility(View.VISIBLE);
                } else {
                    dialogView.findViewById(R.id.tv_error_param_value).setVisibility(View.GONE);
                }
                if (flag) {
                    addParams(key, value);
                    dialog.dismiss();
                }

            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    private void addParams(String key, String value) {
        RCParam p = new RCParam();
        p.key = key;
        p.value = value;
        list.add(p);
        View row = LayoutInflater.from(this).inflate(R.layout.row_restclient_param, null);
        TextView tv = (TextView) row.findViewById(R.id.tv_key_value);
        tv.setText(key + " = " + value);
        Button delete = (Button) row.findViewById(R.id.btn_delete);
        delete.setTag(list.size() - 1);
        row.setTag(list.size() - 1);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = (int) v.getTag();
                list.remove(index);
                paramLayout.removeViewAt(index);
                paramLayout.requestLayout();
            }
        });
        paramLayout.addView(row);
    }

    private class RCParam {
        String key, value;
    }

    private void showResultDialogView(final PBResponse response, boolean isError) {
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        View v = LayoutInflater.from(this).inflate(R.layout.layout_rc_result, null);

        TextView tv = (TextView) v.findViewById(R.id.tv_rc_result);
        final String text = getResponseText(response, isError);
        tv.setText(text);
        b.setView(v);
        b.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Util.hideKeyboard(RestClientActivity.this);
            }
        });
        b.setNeutralButton("Share via Email", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                shareResultOnEmail(text);
            }
        });
        b.create().show();
    }

    private String getResponseText(PBResponse response, boolean isError) {
        StringBuilder b = new StringBuilder();
        b.append("URL :\n\n");
        b.append(response.getUrl() + "");
        b.append("\n\n\n");
        b.append("Result : " + (!isError ? "SUCCESS" : response.getCustomException().getMessage()));
        b.append("\n\n\n\n");
        b.append("JSON \n\n");
        b.append(response.getResponse() + "");
        return b.toString();
    }

    private void shareResultOnEmail(String text) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        String email = "";
        if (AppController.getInstance().getUserProfile() != null) {
            email = AppController.getInstance().getUserProfile().getEmail() + "";
        }
        intent.putExtra(Intent.EXTRA_EMAIL, email == null ? "" : email);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Plabro Rest Client Response");
        intent.putExtra(Intent.EXTRA_TEXT, text);

        startActivity(Intent.createChooser(intent, "Share on Email"));
    }
}
