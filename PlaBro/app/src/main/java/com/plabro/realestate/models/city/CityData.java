package com.plabro.realestate.models.city;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class CityData {

    private ArrayList<String> default_city = new ArrayList<String>();

    private ArrayList<ArrayList<String>> other_cities= new ArrayList<ArrayList<String>>();

    @SerializedName("popular_cities")
    @Expose
    private ArrayList<ArrayList<String>> popular_cities= new ArrayList<ArrayList<String>>();

    public ArrayList<String> getDefault_city() {
        return default_city;
    }

    public void setDefault_city(ArrayList<String> default_city) {
        this.default_city = default_city;
    }

    public ArrayList<ArrayList<String>> getOther_cities() {
        return other_cities;
    }

    public void setOther_cities(ArrayList<ArrayList<String>> other_cities) {
        this.other_cities = other_cities;
    }

    public ArrayList<ArrayList<String>> getPopular_cities() {
        return popular_cities;
    }

    public void setPopular_cities(ArrayList<ArrayList<String>> popular_cities) {
        this.popular_cities = popular_cities;
    }
}
