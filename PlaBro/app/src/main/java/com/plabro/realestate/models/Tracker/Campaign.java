package com.plabro.realestate.models.Tracker;

import android.text.TextUtils;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.utilities.Constants.CampaignParams;

import java.util.HashMap;


/**
 * Created by jmd on 5/29/2015.
 */
public class Campaign {
    private static final String TAG = "Campaign";
    private String utm_source;
    private String utm_medium;
    private String utm_term;
    ;
    private String utm_content;
    private String utm_campaign;

    public static void testParseUri() {
        String uri = "https://play.google.com/store/apps/details?id=com.example.app\n" +
                "&referrer=utm_source%3Dgoogle\n" +
                "%26utm_medium%3Dcpc\n" +
                "%26utm_content%3DdisplayAd1\n" +
                "%26utm_campaign%3Dpodcast%252Bgeneralkeywords";
        parseUri(uri);
    }

    public static HashMap<String, String> parseUri(String uri) {
        Log.d(TAG, "Campaign Data:" + uri);
        Campaign c = new Campaign();
        if (uri.contains("referrer=")) {
            String[] splittedUri = uri.split("referrer=");
            if (splittedUri.length > 1) {
                uri = splittedUri[1];
                splittedUri = uri.split("%26");
                HashMap<String, String> map = new HashMap<>();
                for (String s : splittedUri) {
                    Log.d(TAG, "" + s);
                    String[] keyValue = s.split("%3D");
                    String key = keyValue[0];
                    String value = keyValue[1];
                    value = value.replaceAll("%252B", " ").trim();
                    Log.d(TAG, key + ":" + value);
                    map.put(key, value);
                }
                if (map.containsKey(CampaignParams.UTM_SOURCE)) {
                    c.utm_source = map.get(CampaignParams.UTM_SOURCE);
                }
                if (map.containsKey(CampaignParams.UTM_TERM)) {
                    c.utm_term = map.get(CampaignParams.UTM_TERM);
                }
                if (map.containsKey(CampaignParams.UTM_MEDIUM)) {
                    c.utm_medium = map.get(CampaignParams.UTM_MEDIUM);
                }
                if (map.containsKey(CampaignParams.UTM_CONTENT)) {
                    c.utm_content = map.get(CampaignParams.UTM_CONTENT);
                }
                if (map.containsKey(CampaignParams.UTM_CAMPAIGN)) {
                    c.utm_campaign = map.get(CampaignParams.UTM_CAMPAIGN);
                }
                return map;
            }
        } else {
            return parseCampaignData(uri);
        }
        return null;
    }

    public static HashMap<String, String> parseCampaignData(String uri) {
        Log.d(TAG, uri);
        String[] splittedUri = uri.split("&");
        HashMap<String, String> map = new HashMap<>();
        for (String s : splittedUri) {
            Log.d(TAG, "" + s);
            String[] keyValue = s.split("=");
            String key = "";
            String value = "";
            if (keyValue.length > 1) {
                key = keyValue[0];
                value = keyValue[1];
            } else if (keyValue.length > 0) {
                key = keyValue[0];
            }

            Log.d(TAG, key + ":" + value);
            if (!TextUtils.isEmpty(key) && !TextUtils.isEmpty(value)) {
                map.put(key, value);
            }

        }
        return map;
    }
}
