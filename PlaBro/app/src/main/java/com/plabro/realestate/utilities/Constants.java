package com.plabro.realestate.utilities;

import android.os.Environment;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hemant on 16/12/14.
 */
public class Constants {

    public static final String BUNDLE_KEY_ENDORSER_PROFILE = "EndorserProfile";
    public static final String BUNDLE_KEY_USER_PROFILE = "UserProfile";
    public static final String BUNDLE_KEY_INDEX = "list_index";
    public static final String BUNDLE_KEY_FEED = "FeedObject";
    public static final String CITY_ID = "city_id";
    public static final String START_IDX = "start_idx";
    public static final String ACTION_PROFILE_UPDATE = "profileUpdate";
    public static final String FILE_CITY_JSON = "files/cities.json";
    public static final String FILE_SHOUT_CITY_JSON = "files/shout_cities.json";
    public static final String FILE_COUNTRY_JSON = "files/country.json";
    public static final String FILE_COUNTRY_CODE_JSON = "files/country_codes.json";
    public static final String CONVERSION_ID = "945617553";

    public static final int CALL_LOG_TIME_GAP = 3000;
    public static final String ACTION_DELETE_NOTIFICATION = "action_delete_notification";
    public static final String ACTION_CHAT_COUNT = "action_chat_count";
    public static final String ACTION_BADGE_COUNT = "action_badge_count";
    public static final String DEF_COUNTRY_CODE = "+91";
    public static final String ACTION_SERVICE_COST = "service_cost";
    public static final String ACTION_FEED_REFRESH = "refresh_feed";
    public static final int REQ_CODE_VERIFY_OTP = 12;

    public static interface CampaignParams {
        String UTM_SOURCE = "utm_source";
        String UTM_MEDIUM = "utm_medium";
        String UTM_TERM = "utm_term";
        String UTM_CONTENT = "utm_content";
        String UTM_CAMPAIGN = "utm_campaign";
        String USER_ACTION = "UserAction";
    }

    public static interface SectionIndex {
        int FEEDS = 0;
        int CHATS = 1;
        int NOTIFICATIONS = 2;
        int SERVICES = 3;

        int MY_BIDS = 4;
        int BOOKMARKS = 5;
        int MY_SHOUTS = 6;
        int CONTACTS = 7;

        int SETTINGS = 8;
        int INVITES = 9;

//
//
//
//        int DIRECT_LISTING = 4;
//
//
//        int WALLET = 7;



    }

    public static interface LoaderId {
        int CHAT_LIST = 1;
    }

    public static interface FEATURED_URL {
        String SMS_DASH = "http://www.plabro.com/smsdashboard/?uid=2867";
        String SMS_REQ = "http://www.plabro.com/smsrequest/?name_1=Modify";
    }


    public static interface FEATURED_URL_TITLE {
        String SMS_DASH = "SMS Dashboard";
        String SMS_REQ = "SMS Request";
    }

    public static interface TASK_CODES {
        int USER_PROFILE = 1;
        int FOLLOW = 2;
        int UNFOLLOW = 3;
        int SHORT_URL = 4;
        int SHOUTS = 5;
        int SET_BOOKMARK = 6;
        int BUSINESS_CARD_UPLOAD = 7;
        int LOCATION_NEARBY = 8;
        int SET_PREFS = 9;
        int UPDATE_REGISTER_KEY = 10;
        int AUTH_LOGIN = 11;
        int KEY_EXCHANGE = 12;
        int AESKEY = 13;
        int ENCRYPTMESSAGE = 14;
        int GRID_RECORDS = 15;
        int POST_SHOUTS = 16;
        int REGISTER_USER = 17;
        int CONFIRMATION_SMS = 18;
        int CONTACT_UPLOAD = 19;
        int GET_PLABRO_FRIENDS = 20;
        int WIFI_DATA = 21;
        int GET_CITY_DROPDOWN = 22;
        int BUILDER_FEEDS = 23;
        int GET_BUILDER_PROFILE = 24;
        int BUILDER_FEEDS_MORE = 25;
        int BUILDER_COLLABORATE = 26;
        int BUILDER_UN_COLLABORATE = 27;
        int GET_TABS_INFO = 28;
        int EDIT_SHOUT = 29;
        int PROPFEEDS = 30;
        int BOOKMARKSLIST = 31;
        int GET_NOTIFICATIONS = 32;
        int GET_NON_PLABRO_FRIENDS = 33;
        int INVITE_USERS = 34;
        int SHOUT_DETAILS = 35;
        int RATING_STATE = 36;
        int EXOTEL_CALL = 37;
        int GET_CALL_LOG = 38;
        int DECRYPTMESSAGE = 39;
        int TRANSACTIONS = 40;
        int PAYMENT = 41;
        int REPOST_SHOUT = 42;

        int SERVICE_PAYMENT = 43;
        int DIRECT_LISTING_FEEDS = 44;
        int GET_LIVE_AUCTIONS = 45;
        int MY_BIDS = 46;
        int SEARCH_AUCTION = 47;
        int USER_SUGGESTION_ACTION = 48;
        int FOLLOWING_STATUS = 49;
        int SUGG_COMMUNITY_USERS = 50;
        int JOIN_COMMUNITY = 51;
        int COMMUNITY_SHOUTS = 52;
        int COMMUNITY_INFO = 53;
        int SEND_OTP = 54;
    }

    public static interface FEED_TYPE {
        String SHOUT = "shout";
        String BROKER = "broker";
        String BIDDING_PROMOTION = "bidding_promotion";
        String MULTI_BROKER = "multi_broker";
        String BUILDER_SHOUT = "builder_shout";
        String NEWS = "news";
        String PROGRESS_MORE = "progressMore";
        String NO_MORE = "noMore";
        String NO_MORE_DATA = "noMoreData";
        String DIRECT_INVENTORY = "listing_property";

        String RELATED_BROKER = "related_broker";
        String CALL = "Call";
        String GT = "gt";
        String WEB_VIEW = "WebView";
        String ADVERTISEMENT = "advertisement";
        String AUCTION = "auction_card";
        String MY_BID = "user_bidding";
        String FEED_POST_SEARCH = "post_search";
        String EXTERNAL_SOURCE = "external_src";
        String WIDGETS = "widgets";
    }

    public static interface RATING_STATE_TYPE {
        String LATER = "later";
        String RATED = "rated";

    }

    public static interface LOAD_MORE {
        String SHOUT = "shout";
        String BROKER = "broker";
        String MULTI_BROKER = "multi_broker";
        String BUILDER_SHOUT = "builder_shout";
        String NEWS = "news";
        String PROGRESS_MORE = "progressMore";
        String NO_MORE = "noMore";

    }


    public static interface NOTIFICATION_TYPE {
        String FOLLOWERS = "fl";
        String IM = "im";
        String FEEDS = "pf";
        String BROADCAST = "pb";
        String UPDATE = "upv";
        String BOOKMARK = "bkm";
        String NEW_CONTACT = "nc";
        String OFFER = "off";
        String PROGRESS_MORE = "progressMore";
        String NO_MORE = "noMore";
        String NO_MORE_DATA = "noMoreData";
    }

    public static final String PLABRO_URL = "http://www.plabro.com";
    public static final String PLABRO_TITLE = "Plabro";
    public static final String PLABRO_APP_URL = "http://plabro.com/app";
    public static final String PLABRO_APP_TNC = "http://plabro.com/tc.html";
    public static final String ADMIN_MAIL = "admin@plabro.com";
    public static final String INVITE_MAIL = "invite@plabro.com";

    public static final String SHARE_MAIL = "share@plabro.com";
    public static final String UPLOAD_MAIL = "upload@plabro.com";
    public static final String PLABRO_CUSTOMER_CARE = PBPreferences.getPlabroCustomerCare();


    public static final String[] HOME_SLIDER_ARRAYS = {"Property Feeds", "Conversations", "ContactsFragment", "Settings"};
    public static final String BUNDLE_KEY_FILTER_JSON = "FilterJson";

    public static String VERIFICATIONCODE = "9876";
    public static final ArrayList<String> skippingSMSCodeArr = new ArrayList<String>();


    public static int REVIEW_REQUEST_CODE = 3;
    public static int DELAY_SHOW_NEW_NOTIFICATION = 3000;
    public static int DELAY_SEN_REG_ID_TIMEr = 21600000;


    public static final String ERROR_MSG_1 = "Expired HashKey";
    public static final String ERROR_MSG_2 = "Session Expired";
    public static final String ERROR_MSG_3 = "Missing AuthKey in the request";
    public static final int GOOGLE_PULL_TO_REFRESH_TIMER = 5;
    public static final int RESULT_OK = -1;
    public static final int FEEDS_REQUEST_CODE = 9;
    public static final int LOCATION_REQUEST_CODE = 11;
    public static final int NAME_REQUEST_CODE = 12;
    public static final int EMAIL_REQUEST_CODE = 13;
    public static final int WEB_REQUEST_CODE = 14;
    public static final int ADDR_REQUEST_CODE = 15;
    public static final int STATUS_REQUEST_CODE = 16;
    public static final int CHAT_REQUEST_CODE = 17;
    public static final int BUSINESS_REQUEST_CODE = 18;
    public static final int FILTER_REQ_CODE = 19;
    public static final int SCROLL_ITEM_COUNT = 6; //NO of items before which automatically next query results fired up
    public static final long KEYBOARD_UPDATE_TIME_DIFF = 604800000;


    // All Static variables
    // Database Version
    public static final int DATABASE_VERSION = 2;

    // Database Name
    public static final String DATABASE_NAME = "PLABRO.db";

    public static Map<String, String> table_name_map = new HashMap<String, String>();
    // table columns map
    public static Map<String, String> chat_record = new HashMap<String, String>();
    public static Map<String, String> friends_record = new HashMap<String, String>();
    public static Map<String, String> profile_record = new HashMap<String, String>();


    public static Map<String, String> history = new HashMap<String, String>();


    // table primary key and column no map
    public static Map<String, String> arr_tbl_pkey_map = new HashMap<String, String>();
    public static Map<String, Integer> arr_tbl_no_column_map = new HashMap<String, Integer>();


    public static final String LOCATION_MAP_VIEW_KEY = "map_view_details";
    public static final String LOCATION_MAP_API_KEY = "AIzaSyC02QG3u2wV81YPRfLAhapYANqnVYUWBXY";
    public static final String LOCATION_SERVER_NEARBY_API_KEY = "AIzaSyCgdX2HpPIdoOQpWxi2OvWWo1M_zYTS49s";
    public static final long WIFITIMER = 300000;
    public static final long WIFIMINDISTANCE = 15; // in meters
    public static final long HOMETOOLTIPTIMER = 3600000; //in milliseconds // 1hr

    public static String getDatabaseName() {
        String dbfile = Constants.DATABASE_NAME;
        if (Environment.isExternalStorageEmulated()) {
            File sdcard = Environment.getExternalStorageDirectory();
            dbfile = sdcard.getAbsolutePath() + File.separator + "databases" + File.separator + "PLABRO" + File.separator + Constants.DATABASE_NAME;
        } else {
            dbfile = Constants.DATABASE_NAME;
        }

        return dbfile;
    }

    public static ArrayList<String> getSkippingSMSCodeArr() {
        skippingSMSCodeArr.add("+912275752276");
        skippingSMSCodeArr.add("+914455752276");
        skippingSMSCodeArr.add("+918378752276");

        return skippingSMSCodeArr;
    }

    public static interface BUNDLE_KEYS {
        String CITY_ID = "city_id";
        String SHOUT_ID = "shout_id";
        String BUILDER_ID = "builder_id";
        String PHONE_KEY = "phone_key";
        String NAME = "name";
        String IS_BUILDER_SEARCH = "isBuilderSearch";
        String BUILDER_NAME = "BuilderName";
        String IS_FROM_BUILDER = "isFromBuilder";
        String BUILDER_PHONE = "builder_phone";
        String FRAGMENT_TYPE = "fragmentType";
        String BALANCE = "balance";
        String SERIALIZABLE = "serializable";
        java.lang.String POSITION = "position";
        java.lang.String BADGE_COUNT = "badge_count";
        String CHAT_COUNT = "chat_count";
        String SERVICE_TYPE = "serviceType";
        java.lang.String GREETING_MSG = "greetingMsg";
        String SERVICE_CODE = "service_code";
        String S_URL = "surl";
        String F_URL = "furl";
        String AMOUNT = "amount";
        String TITLE = "Title";
        String INPUT_URL = "InputUrl";
        String EXIT_URL = "ExitUrl";
        java.lang.String AUCTION_ID = "auction_id";
        String BID_TIME_STRING = "bidTimeInfo";
        String IS_MY_BID = "isMyBid";
        java.lang.String BID_ID = "bid_id";
        java.lang.String AD_TITLE = "ad_title";
        String IS_CHAT_PREFIX_REQUIRED = "isChatPrefixRequired";
        String IS_APPENDED = "isAppended";
        String COMMUNITY_ID = "communityId";
        String GROUP_ID = "group_id";
        String GROUPID = "groupid";
        String GROUP_IDS = "group_ids";
        java.lang.String MOBILE = "mobileNumber";
        java.lang.String OTP = "Otp";
        String COUNTRY_CODE = "CountryCode";
    }

    public static interface FRAGMENT_TYPE {
        int BUILDER_DETAIL_FRAGMENT = 1;
        int FEED_FRAGMENT = 2;
        int LOAN_SERVICE = 3;
        int SMS_SERVICE = 4;
        int DIRECT_INVENTORY = 5;
        int MATCH_MAKING = 6;
        int AUCTION = 7;
        int FOLLOW_PAGER_FRAGMENT = 8;
        int CHAT = 9;
        int NOTIFICATION = 10;
        int WALLET = 11;
        int DIRECT_LISTING = 12;
        int NEWS = 13;
        int BLOGS = 14;
        int SERVICES = 15;
        int AUCTION_DETAIL = 16;
        int PLABRO_SCAN = 17;
        int COMMUNITY_SHOUTS_USERS = 18;
    }

    public static interface ProfileParams {
        String ADDRESS = "addr";
        String EMAIL = "email";
        String WEB = "web";
        String NAME = "name";
        String STATUS = "status";
        String BUSINESS = "business";
        String CITY = "city";
        String PROFESSION = "Profession";
    }

    public static interface ToolTip {
        String SEARCHICON = "searchicontip";
        String FILTERICON = "filtericontip";
        String SELECTINGFILTER = "selectingfiltertip";
        String APPLYINGFILTER = "applyingfiltertip";
        String FEATUREDICON = "featuredicontip";

    }

    public static interface NotifType {
        String FOLLOW_USER = "FollowUser";
        String FRIEND_JOINED = "FriendJoined";
        String POST_SHOUT_FOLLOWER = "PostShoutFollower";
        String SET_BOOKMARK = "SetBookmark";
        String USER_SIGNATURE = "UserSignature";
        String IN_APP = "inapp";
        String ENDORSEMENT = "Endorsement";
        String DEEP_LINK = "DeepLink";
    }

    public static interface EmtyText {
        String BROKERS = "brokers";
        String CALLS = "calls";
    }

    public static interface RegistrationParams {
        String PhoneFilled = "PhoneFilled";
        String TNC_VIEWED = "TnC Viewed";
        String TNC_TIME = "TnC Time";
        String OTP_FILLED = "OTP Filled";
        String SMS_RETRIED = "SMS Retried";
        String CALL_BACK = "Callback";
        String IMAGE_UPLOADED = "ImageUploaded";
        String IMAGE_CLICKED = "ImageClicked";


        String IMAGE_CROPPED = "ImageCropped";
    }

    public static interface PaymentMode {
        String DEBIT = "debitcard";
        String CREDIT = "creditcard";
        String CASH = "cash";
        String NETBANKING = "netbanking";
        String WALLET = "wallet";

    }

    public static interface NotifActionTypes {
        String READ = "Read";
        String DELETE = "Delete";

//        ReadOne, DeleteOne, ReadAll, DeleteAll
    }

    public static interface ServiceTypes {
        String LOAN_SERVICE = "loan_service";
        String SMS_SERVICE = "sms_service";
        String MATCH_MAKING = "match_making";
        String DIRECT_INVENTORY = "direct_listing_service";
        String BIDDING_SERVICE = "bidding_service";
    }
}
