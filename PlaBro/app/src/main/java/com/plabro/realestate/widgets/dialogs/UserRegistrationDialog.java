package com.plabro.realestate.widgets.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.plabro.realestate.R;
import com.plabro.realestate.UserRegister;
import com.plabro.realestate.receivers.PBLocalReceiver;

/**
 * Created by Hemant on 19-01-2015.
 */
public class UserRegistrationDialog extends android.support.v4.app.DialogFragment implements PBLocalReceiver.PBActionListener {

    private TextView mGoAhead;
    private Context mContext;
    private int userType = 0;
    public static boolean isVisible = false;

    @SuppressLint("ValidFragment")
    public UserRegistrationDialog(Activity mActivity, int userType) {
        this.mContext = mActivity;
        this.userType = userType;
    }

    public UserRegistrationDialog() {

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View convertView = null;
        if (userType == 0) {
            convertView = (View) inflater.inflate(R.layout.user_reg_card, null);
            mGoAhead = (TextView) convertView.findViewById(R.id.tv_go_ahead);
            mGoAhead.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, UserRegister.class);
                    startActivity(intent);
                }
            });

        } else if (userType == 1) {
            convertView = (View) inflater.inflate(R.layout.user_reg_waiting_card, null);
            mGoAhead = (TextView) convertView.findViewById(R.id.tv_go_ahead);
            mGoAhead.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });

        }
        builder.setView(convertView);
        setCancelable(true);
        isVisible = true;
        return builder.create();
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        isVisible = false;
    }

    @Override
    public void OnReceive(Context context, Intent intent) {

    }

}
