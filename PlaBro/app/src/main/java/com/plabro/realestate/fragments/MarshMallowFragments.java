package com.plabro.realestate.fragments;

import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.plabro.realestate.MarshMallowActivity;
import com.plabro.realestate.receivers.MarshMallowReceiver;


/**
 * Created by hemantkumar on 02/11/15.
 */
public abstract class MarshMallowFragments extends Fragment implements MarshMallowReceiver.MMInterFace{

    private int PMCode;
    private String pmMessage;
    private MarshMallowActivity activity;


    @TargetApi(Build.VERSION_CODES.M)
    public void hasPermission(String permission, int PMCode, String pmMessage) {
        activity = (MarshMallowActivity) getActivity();
        this.PMCode = PMCode;
        this.pmMessage = pmMessage;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            //Build version below Marshmallow
            onMMPermisssoinResult(PMCode, true, pmMessage);
            return;
        }


        if (activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            //permission is already available, proceed normally
            onMMPermisssoinResult(PMCode, true, pmMessage);

        } else {
            //Permission not granted, lets ask user for the permissions
            if (activity.shouldShowRequestPermissionRationale(permission)) {
                Toast.makeText(activity, pmMessage, Toast.LENGTH_SHORT).show();
            }

            activity.requestPermissions(new String[]{permission}, PMCode);

        }

    }


    @Override
    public void onMMPermisssoinResult(int requestCode, boolean status, String message) {

    }

}
