package com.plabro.realestate.models;


import com.plabro.realestate.models.login.LoginOutputParams;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class GCMDeviceToken {

    boolean status;
    LoginOutputParams output_params = new LoginOutputParams();
    String msg;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public LoginOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(LoginOutputParams output_params) {
        this.output_params = output_params;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
