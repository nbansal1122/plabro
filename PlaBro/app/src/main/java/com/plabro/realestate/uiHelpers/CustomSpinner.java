package com.plabro.realestate.uiHelpers;

/**
 * Created by hemant on 22/12/14.
 */

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

@SuppressLint("ValidFragment")
public class CustomSpinner extends DialogFragment {

    public View view;
    public TextView spTV;
    public String title;
    public String[] dataArray;

    public CustomSpinner() {

    }

    public CustomSpinner(View view, TextView spTV, String title, String[] dataArray) {
        this.view = view;
        this.spTV = spTV;
        this.title = title;
        this.dataArray = dataArray;


    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setItems(dataArray, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        spTV.setText(dataArray[which]);

                    }
                });
        return builder.create();

    }


}
