package com.plabro.realestate.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.plabro.realestate.R;
import com.plabro.realestate.fragments.BuilderFragment;
import com.plabro.realestate.fragments.FeedsFragment;
import com.plabro.realestate.utilities.Constants;

/**
 * Created by nitin on 17/08/15.
 */
public class BuilderFeeds extends PlabroBaseActivity {
    @Override
    protected String getTag() {
        return "Builder Feeds";
    }

    @Override
    protected int getResourceId() {
        return R.layout.fragment_container;
    }

    @Override
    public void findViewById() {
        inflateToolbar();
        setTitle(getIntent().getStringExtra(Constants.BUNDLE_KEYS.BUILDER_NAME));
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, getBuilderFeedFragment()).commit();
    }

    @Override
    protected int getMenuIdToInflate() {
        return R.menu.menu_my_profile;
    }

    private Fragment getBuilderFeedFragment() {
        int fragmentType = getIntent().getIntExtra(Constants.BUNDLE_KEYS.FRAGMENT_TYPE, Constants.FRAGMENT_TYPE.FEED_FRAGMENT);
        switch (fragmentType){
            case Constants.FRAGMENT_TYPE.BUILDER_DETAIL_FRAGMENT:
                return BuilderFragment.getInstance(getIntent().getExtras());
            case Constants.FRAGMENT_TYPE.FEED_FRAGMENT:
                return FeedsFragment.getInstance(new Bundle());
        }
        return null;
    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }
}
