package com.plabro.realestate.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.R;
import com.plabro.realestate.adapter.CustomPagerAdapter;
import com.plabro.realestate.fragments.AvailableFilter;
import com.plabro.realestate.fragments.PlabroBaseFragment;
import com.plabro.realestate.fragments.RequirementFilter;
import com.plabro.realestate.uiHelpers.SlidingTabLayout;
import com.plabro.realestate.uiHelpers.ViewPagerIndicator;

import java.util.ArrayList;

public class SearchFilter extends PlabroBaseActivity implements CustomPagerAdapter.PagerAdapterInterface<String>, ViewPager.OnPageChangeListener {

    private ViewPagerIndicator pageIndicator;
    private ViewPager mViewPager;
    private ArrayList<String> filterTypes = new ArrayList<>();
    private CustomPagerAdapter<String> adapter;

    private SlidingTabLayout mSlidingTabLayout;


    private Toolbar toolbar;

    @Override
    protected String getTag() {
        return "SearchFilter";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_filter);
        toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Search Filters");
        filterTypes.add("Available");
        filterTypes.add("Requirement");
        setUpViewPager();
        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);
    }

    private void addActionBarTabs() {
    }

    void setUpViewPager() {
        mViewPager = (ViewPager) findViewById(R.id.pager);

        adapter = new CustomPagerAdapter<>(getSupportFragmentManager(), filterTypes, this);


        mViewPager.setAdapter(adapter);
        //mViewPager.setOnPageChangeListener(this);


        // We set this on the indicator, NOT the pager


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search_filter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Fragment getFragmentItem(int position, String listItem) {
        PlabroBaseFragment f = null;
        switch (position) {
            case 0:
                Log.d(TAG, "AvailableFilter");
                f = AvailableFilter.getInstance(new Bundle());
                break;
            case 1:
                Log.d(TAG, "RequirementFilter");
                f = RequirementFilter.getInstance(new Bundle());
                break;
        }
        return f;
    }

    @Override
    public CharSequence getPageTitle(int position, String listItem) {
        return listItem;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.d(TAG, "" + position);
        mViewPager.setCurrentItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void findViewById() {

    }

    @Override
    public void setViewSizes() {

    }

    @Override
    public void callScreenDataRequest() {

    }

    @Override
    public void reloadRequest() {

    }
}
