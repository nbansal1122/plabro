package com.plabro.realestate.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.appsflyer.AppsFlyerLib;
import com.clevertap.android.sdk.CleverTapAPI;
import com.plabro.realestate.AppController;
import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.MarshMallowActivity;
import com.plabro.realestate.R;
import com.plabro.realestate.enums.RequestMethod;
import com.plabro.realestate.listeners.GenericObserver;
import com.plabro.realestate.listeners.MasterLifecycleInterface;
import com.plabro.realestate.listeners.SessionExpiredDialogListener;
import com.plabro.realestate.listeners.VolleyListener;
import com.plabro.realestate.models.Calls.CallPopUpInfo;
import com.plabro.realestate.models.Endorsement;
import com.plabro.realestate.models.ParamObject;
import com.plabro.realestate.models.login.Login;
import com.plabro.realestate.models.profile.ProfileClass;
import com.plabro.realestate.models.propFeeds.BaseFeed;
import com.plabro.realestate.models.propFeeds.Endorsement_data;
import com.plabro.realestate.models.propFeeds.Feeds;
import com.plabro.realestate.others.Analytics;
import com.plabro.realestate.others.CleverTap;
import com.plabro.realestate.receivers.CallListener;
import com.plabro.realestate.receivers.PBLocalReceiver;
import com.plabro.realestate.uiHelpers.MaterialDialog;
import com.plabro.realestate.utilities.ActivityUtils;
import com.plabro.realestate.utilities.Constants;
import com.plabro.realestate.utilities.PBPreferences;
import com.plabro.realestate.utilities.PlaBroApi;
import com.plabro.realestate.utilities.Util;
import com.plabro.realestate.utilities.Utility;
import com.plabro.realestate.volley.AppVolley;
import com.plabro.realestate.volley.PBResponse;
import com.plabro.realestate.widgets.dialogs.DOLoaderDialog;
import com.plabro.realestate.widgets.dialogs.PBConditionalDialogView;
import com.plabro.realestate.widgets.dialogs.UserRegistrationDialog;
import com.software.shell.fab.ActionButton;

import java.util.HashMap;

/**
 * Created by jmd on 5/4/2015.
 */
public abstract class PlabroBaseActivity extends MarshMallowActivity implements PBLocalReceiver.PBActionListener, View.OnClickListener, SessionExpiredDialogListener, GenericObserver.GenericObserverInterface, MasterLifecycleInterface, CallListener.CallInterface {

    public final String TAG = getTag();
    protected Toolbar toolbar;
    IntentFilter filter;
    PBLocalReceiver rec;
    private boolean onPauseFlag;
    protected CleverTapAPI mCleverTapAPI;
    protected CleverTap mCleverTap;
    protected AppController mAppController;
    public boolean isRunning = true;
    private CallListener phoneListener;
    private boolean isCallConnected;
    static Animation animFadeIn, animFadeOut, animTopIn, animTopOut, animZoomIn, animZoomOut, animTopInSlow, animTopOutSlow;
    DOLoaderDialog progressScreenDialog = null;


    PBConditionalDialogView mTcConditionalDialogView = null;

    public enum AnimationDef {
        ALL, FADE_IN, FADE_OUT, TOP_IN, TOP_OUT, ZOOM_IN, ZOOM_OUT, ZOOM_IN_LOW, ZOOM_OUT_LOW, TOP_IN_LOW, TOP_OUT_LOW;
    }


//    protected final String finishAction = "com.plabro.realestate.Action_Finish";

    protected boolean isReg = false;

    protected abstract String getTag();

    public String getName() {
        return getTag();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreateActivity");
        if (getIntent().getExtras() != null) {
            loadBundleData(getIntent().getExtras());
        }

        registerLocalReceiver();
        mAppController = AppController.getInstance();
        initializeCleverTap();
        Log.d("Tracking Screen", getTag() + "");
        if (0 != getResourceId()) {
            setContentView(getResourceId());
            loadDefaultData();
        }


    }

    protected void loadBundleData(Bundle b) {

    }


    @Override
    public void setViewSizes() {
    }

    @Override
    public void callScreenDataRequest() {

    }



    protected void onCreate(Bundle savedInstanceState, int layoutResourceId) {
        super.onCreate(savedInstanceState);
        setContentView(layoutResourceId);
        Log.d("Tracking Screen", getTag() + "");
        loadDefaultData();
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "" + CallPopUpInfo.isCallClicked);
        if (null != CallPopUpInfo.feeds && CallPopUpInfo.isCallClicked) {
            CallPopUpInfo.isCallClicked = false;
            sendCallLogsAfterOneSecond();
        }
        AppController.activityResumed();
//        AppsFlyerLib.onActivityResume(this);
    }

    public void sendCallLogsAfterOneSecond() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (ActivityUtils.isCallConnected(PlabroBaseActivity.this, CallPopUpInfo.feeds, getTag()) && isRunning) {
                    PlabroBaseActivity.showCallDialog(PlabroBaseActivity.this, CallPopUpInfo.feeds, "CALL ENDED WITH", false);
                }
            }
        }, Constants.CALL_LOG_TIME_GAP);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "OnPause");
        super.onPause();
        onPauseFlag = true;
        AppController.activityPaused();
        // mCleverTapAPI.activityPaused(this);
//        AppsFlyerLib.onActivityPause(this);
        // AppEventsLogger.deactivateApp(this);
    }

//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        if(getIntent().getExtras() != null){
//            outState.putAll(getIntent().getExtras());
//        }
//        super.onSaveInstanceState(outState);
//    }

    public void sendScreenNameC(String screenName) {

//        Analytics.trackScreen(screenName);
    }

    public void sendScreenName(int screenNameStringId) {
//        sendScreenName(getString(screenNameStringId));
    }


    public void inflateToolbar() {
        //setting action bar
        toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        if (toolbar == null) return;
        setSupportActionBar(toolbar);

        // Set an OnMenuItemClickListener to handle menu item clicks
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Handle the menu item
                int id = item.getItemId();
                return onMenuItemClicked(item, id);
            }

        });

        // Inflate a menu to be displayed in the toolbar
        if (0 != getMenuIdToInflate())
            toolbar.inflateMenu(getMenuIdToInflate());

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                onNavigationItemClicked();
            }
        });


        //getProfileFromDB();

    }

    protected boolean onMenuItemClicked(MenuItem item, int id) {
        return true;
    }

    protected int getMenuIdToInflate() {
        return 0;
    }

    protected void onNavigationItemClicked() {
        finish();
    }

    protected void showShortToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    protected void showLongToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    public void setToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void setToolbarSubTitle(String subtitle) {
        getSupportActionBar().setTitle(subtitle);
    }


    public void registerLocalReceiver() {
        if (rec == null) {
            rec = new PBLocalReceiver(this);
        }
        registerReceiver(rec, getFilter());
    }

    public void unRegsiterLocalReceiver() {
        if (rec != null) {
            unregisterReceiver(rec);
        }
    }

    public IntentFilter getFilter() {
        filter = new IntentFilter();
        filter.addAction(ACTION_REGISTRATION_UPDATE);
        filter.addAction(Constants.ACTION_PROFILE_UPDATE);
        return filter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isRunning = false;
        unRegsiterLocalReceiver();
    }

    @Override
    public void OnReceive(Context context, Intent intent) {

    }

    public void setText(int textViewId, String text) {
        TextView textView = (TextView) findViewById(textViewId);
        textView.setText(text);
    }

    private static void trackEndorsement(String key, Feeds f, String endorsementText) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("Key", key);
        map.put("Target Authkey", f.get_id());
        map.put("Target Phone", f.getProfile_phone());
        map.put("User Phone", PBPreferences.getPhone());
        map.put("Endorsement Reason", endorsementText);
    }

    public static void showCallDialog(final Activity ctx, final Feeds f, final String infoText, final boolean isEndorsementNotification) {
        if (null != f && null != f.getEndorsement_data()) {
            final Endorsement_data eData = f.getEndorsement_data();

            AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
            builder.setCancelable(false);
            View view = LayoutInflater.from(ctx).inflate(R.layout.pop_up_endorsement, null);
            builder.setView(view);

            TextView userName = (TextView) view.findViewById(R.id.tv_user_name);
            TextView eventType = (TextView) view.findViewById(R.id.tv_event_type);
            if (infoText != null && infoText.contains("CHAT")) {
                eventType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_card_chat_normal, 0, 0, 0);
            }
            eventType.setText(infoText);
            TextView userAddress = (TextView) view.findViewById(R.id.tv_user_addr);
            TextView endorseLocality = (TextView) view.findViewById(R.id.tv_endorse_locality);
            ImageView imgView = (ImageView) view.findViewById(R.id.iv_profileImage);

            Log.d("HomeActivity", "Not Null::");
            userName.setText(f.getProfile_name() + "");
            if (null != eData && null != eData.getKeys() && eData.getKeys().size() > 0 && null != eData.getTitle()) {
//                String title = eData.getTitle();
//                if(title.contains("Call")){
//                     title =
//                }
                endorseLocality.setText(eData.getTitle() + " ?");
            } else {
                Log.d("Endorsement:", "End Data is Null:or keys are null");

                return;
            }
            userAddress.setVisibility(View.GONE);
            ActivityUtils.loadImage(ctx, f.getProfile_img(), imgView);
            final Dialog dialog = builder.create();
            dialog.show();
            view.findViewById(R.id.tv_skip).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map.put("endorseTitle", eData.getTitle());
                    map.put("endorseKey", eData.getKeys().toString());
                    Analytics.trackProfileEvent(Analytics.Actions.Skipped, map);
                    if (!isEndorsementNotification) {
                        sendEndorsementNotification(f);
                    }
                    dialog.dismiss();
                }
            });

            ActionButton endorse = (ActionButton) view.findViewById(R.id.btn_fab_dialog_endorsement);
            endorse.setType(ActionButton.Type.BIG);

            // To set button color for normal state:
            endorse.setButtonColor(ctx.getResources().getColor(R.color.fab_material_blue_500));


            // To set button color for pressed state:
            endorse.setButtonColorPressed(ctx.getResources().getColor(R.color.fab_material_blue_900));


            // To set button color ripple:
            endorse.setButtonColorRipple(ctx.getResources().getColor(R.color.fab_material_blue_500));

            endorse.setRippleEffectEnabled(true);

            endorse.setShowAnimation(ActionButton.Animations.JUMP_FROM_DOWN);
            endorse.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_action_1));

            endorse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    StringBuilder builder = new StringBuilder();
                    for (String key : eData.getKeys()) {
                        builder.append(key);
                        builder.append(",");
                    }
                    builder.deleteCharAt(builder.lastIndexOf(","));
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map.put("endorseTitle", eData.getTitle());
                    map.put("endorseKey", eData.getKeys().toString());
                    Analytics.trackProfileEvent(Analytics.Actions.Endorsed, map);
                    testEndorsement(ctx, builder.toString(), f.getAuthorid() + "");
                    trackEndorsement(builder.toString(), f, infoText);
                    if (!isEndorsementNotification) {
                        sendEndorsementNotification(f);
                    }
                    dialog.dismiss();

                }
            });
        } else {
            Log.d("Endorsement", "Feed Object is null or End Data is null");
        }

    }

    public static void sendEndorsementNotification(Feeds feeds) {
        HashMap<String, String> map = new HashMap<>();
        if (!TextUtils.isEmpty(feeds.get_id())) {
            map.put("shout_id", feeds.get_id() + "");
        } else if (!TextUtils.isEmpty(feeds.getAuthorid() + "")) {
            Log.d("Author ID : Endorsement:", feeds.getAuthorid() + "");
            map.put("contact_id", feeds.getAuthorid() + "");
        } else {
            Log.d("Endorsement:", "Author id and shout id are both null");
            return;
        }
        ParamObject obj = new ParamObject();
        obj.setParams(Utility.getInitialParams(PlaBroApi.RT.ENDORSEMENT_NOTIFICATION, map));
        AppVolley.processRequest(obj, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
            }
        });
    }

    public static void testEndorsement(final Activity ctx, final String endKey, final String contactId) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PlaBroApi.PARAMS.ENDORSEMENT_KEY, endKey);
        params.put(PlaBroApi.PARAMS.CONTACT_ID, contactId + "");
        params = Utility.getInitialParams(PlaBroApi.RT.UPDATE_ENDORSEMENT, params);
        AppVolley.processRequest(0, null, null, PlaBroApi.getBaseUrl(), params, RequestMethod.GET, new VolleyListener() {
            @Override
            public void onSuccessResponse(PBResponse response, int taskCode) {
                Toast.makeText(ctx, "Endorsed Successfully", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailureResponse(PBResponse response, int taskCode) {
//                Log.d(TAG, "EndorseFailure");
                Endorsement e = new Endorsement();
                e.setAuthorId(contactId);
                e.setEndorsementKey(endKey);
                e.save();
            }
        });
    }

    protected void setClickListeners(int... viewIds) {
        for (int i : viewIds) {
            findViewById(i).setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {

    }


    public void onPlabroSessionExpired(boolean addMOre) {

    }

    public void onPlabroSessionExpired() {
        Utility.userLogin(this);
    }

    public void showSessionExpiredDialog(final boolean addMore) {
        if (!PBPreferences.getSessEXpDialogState()) {

            PBPreferences.setSessEXpDialogState(true);

            final MaterialDialog mMaterialDialog = new MaterialDialog(this);

            mMaterialDialog.setTitle("Try again")
                    .setMessage("Oops ! Your Session Expired.")
                    .setPositiveButton("CONNECT", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMaterialDialog.dismiss();
                            onOkClicked(addMore);
                        }
                    })

                    .setNegativeButton("CANCEL", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMaterialDialog.dismiss();
                            onCancelClicked(addMore);
                        }
                    });

            mMaterialDialog.show();
        }
    }

    @Override
    public void onOkClicked(boolean addMore) {
        Utility.userLogin(this);

    }

    @Override
    public void onCancelClicked(boolean addMore) {

    }

    public void startActivity(Bundle b, Class activityClass) {
        Intent i = new Intent(this, activityClass);
        i.putExtras(b);
        startActivity(i);
    }

    public void startActivityAndFinish(Bundle b, Class activityClass) {
        startActivity(b, activityClass);
        finish();
    }

    public void redirectActivity(Intent incomingIntent, Class activityClass) {
        Bundle b = incomingIntent.getExtras();
        if (null == b) {
            b = new Bundle();
        }
        startActivityAndFinish(b, activityClass);
    }

    private void initializeCleverTap() {
        mCleverTap = CleverTap.getInstance(this);
        mCleverTapAPI = CleverTap.getmCleverTapAPI();
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
    }

    @Override
    public void onEventUpdate(String event) {

    }


    public void onApiFailure(PBResponse response, int taskCode) {
        switch (response.getCustomException().getExceptionType()) {
            case VolleyListener.SESSION_EXPIRED:
                Utility.userLogin(this);
                onSessionExpiry(response, taskCode);
                break;
            case VolleyListener.NO_INETERNET_EXCEPTION:
                // showToast(response.getCustomException().getMessage());
                onInternetException(response, taskCode);
                break;
            case VolleyListener.JSON_PARSE_EXCEPTION:
                break;
            case VolleyListener.TRIAL_EXPIRED_EXCEPTION:
                onTrialVersionExpired(response, taskCode);
                showToast(response.getCustomException().getMessage());
                break;
            case VolleyListener.STATUS_FALSE_EXCEPTION:
                onStatusFalse(response, taskCode);
                showToast(response.getCustomException().getMessage());
                break;
        }
    }

    public void onInternetException(PBResponse response, int taskCode) {

    }

    public void onTrialVersionExpired(PBResponse response, int taskCode) {


    }

    public void onStatusFalse(PBResponse response, int taskCode) {

    }

    public void onSessionExpiry(PBResponse response, int taskCode) {

    }

    public void showToast(String toastMessage) {
        Toast toast = Toast.makeText(this, toastMessage, Toast.LENGTH_SHORT);
//        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
//
//        LinearLayout layout = (LinearLayout) toast.getView();
//        if (layout.getChildCount() > 0) {
//            TextView tv = (TextView) layout.getChildAt(0);
//            tv.setGravity(Gravity.CENTER);
//        }

        toast.show();
    }

    public void showToast(int stringId){
        Toast.makeText(this, stringId, Toast.LENGTH_LONG).show();
    }
    public void showSnackBar(String toastMessage) {
        Util.showSnackBarMessage(this, this, toastMessage);
    }

    public void showRegistrationCard() {
        try {
            int val = PBPreferences.getTrialVersionState();
            if (val == 0) {

                Login login = new Select().from(Login.class).executeSingle();
                if (null != login) {
                    val = login.getTrial_version();
                }
            }
            switch (val) {
                case 0:
                    Utility.userLogin(this);

                    if (!UserRegistrationDialog.isVisible) {
                        try {
                            UserRegistrationDialog feedPopUpDialog = new UserRegistrationDialog(this, val);//todo throwing illegal state exception
                            feedPopUpDialog.show(getSupportFragmentManager(), "UserRegister");
                        } catch (Exception e) {

                        }

                    }
                    break;
                case 1:
                    if (!UserRegistrationDialog.isVisible) {
                        try {
                            UserRegistrationDialog feedPopUpDialog1 = new UserRegistrationDialog(this, val);
                            feedPopUpDialog1.show(getSupportFragmentManager(), "UserRegister");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected int getResourceId() {
        return 0;
    }

    protected void loadDefaultData() {

        if (animFadeIn == null || animFadeOut == null || animTopIn == null || animTopOut == null || animZoomIn == null || animZoomOut == null || animTopInSlow == null || animTopOutSlow == null) {
            getAnimation(AnimationDef.ALL);
        }

        findViewById();
        inflateToolbar();
        setViewSizes();

        if (findViewById(R.id.pbConditionalView) != null) {
            mTcConditionalDialogView = (PBConditionalDialogView) findViewById(R.id.pbConditionalView);

            mTcConditionalDialogView.setOnPBConditionListener(new PBConditionalDialogView.OnPBConditionListener() {

                @Override
                public void onRefresh() {

                    showConditionalMessage(null, false, false);

                    reloadRequest();

                }
            });
        }

    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {

        if (intent != null) {

            super.startActivityForResult(intent, requestCode);
        }
    }


    @Override
    public void finish() {
        super.finish();
    }

    public void finishWithReverseAnimation1() {
        super.finish();
    }


    public void showLoadingDialog(boolean isShow, boolean onBackPressedCancel) {

        if (progressScreenDialog == null && isShow) {
            try {
                progressScreenDialog = DOLoaderDialog.newInstance(onBackPressedCancel);
                progressScreenDialog.setCancelable(onBackPressedCancel);
                // progressScreenDialog.show(getSupportFragmentManager(), null);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(progressScreenDialog, null);
                ft.commitAllowingStateLoss();
                //getSupportFragmentManager().executePendingTransactions();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (progressScreenDialog != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        progressScreenDialog.dismiss();
                        progressScreenDialog = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 500);

        }
    }

    protected void showConditionalMessage(PBConditionalDialogView.ConditionalDialog conditionalDialog, boolean isShow, boolean isShiftLayout) {

        try {

            if (mTcConditionalDialogView == null)
                return;

            if (isShow) {
                mTcConditionalDialogView.setType(conditionalDialog);
                mTcConditionalDialogView.setVisibility(View.VISIBLE);

                findViewById(R.id.pbConditionalView).setVisibility(View.VISIBLE);
                if (isShiftLayout) {
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    params.setMargins(0, 0, 0, (int) getResources().getDimension(R.dimen.conditional_dialog_margin_bottom));
                    mTcConditionalDialogView.setLayoutParams(params);
                }
            } else {
                mTcConditionalDialogView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Animation getAnimation(AnimationDef animationDef) {

        switch (animationDef) {
            case FADE_IN:
                if (animFadeIn == null)
                    animFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);

                return animFadeIn;

            case FADE_OUT:
                if (animFadeOut == null)
                    animFadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);

                return animFadeOut;

            case TOP_IN:
                if (animTopIn == null)
                    animTopIn = AnimationUtils.loadAnimation(this, R.anim.top_in);
                return animTopIn;

            case TOP_OUT:
                if (animTopOut == null)
                    animTopOut = AnimationUtils.loadAnimation(this, R.anim.bottom_out);
                return animTopOut;

            case ZOOM_IN:
                if (animZoomIn == null)
                    animZoomIn = AnimationUtils.loadAnimation(this, R.anim.zoom_in);

                return animZoomIn;

            case ZOOM_OUT:
                if (animZoomOut == null)
                    animZoomOut = AnimationUtils.loadAnimation(this, R.anim.zoom_out);

                return animZoomOut;
            case ZOOM_IN_LOW:
                if (animZoomIn == null)
                    animZoomIn = AnimationUtils.loadAnimation(this, R.anim.zoom_in_low);

                return animZoomIn;

            case ZOOM_OUT_LOW:
                if (animZoomOut == null)
                    animZoomOut = AnimationUtils.loadAnimation(this, R.anim.zoom_out_low);

                return animZoomOut;

            case TOP_IN_LOW:
                if (animTopInSlow == null)
                    animTopInSlow = AnimationUtils.loadAnimation(this, R.anim.top_in_slow);

                return animTopInSlow;

            case TOP_OUT_LOW:
                if (animTopOutSlow == null)
                    animTopOutSlow = AnimationUtils.loadAnimation(this, R.anim.bottom_out_slow);

                return animTopOutSlow;

            case ALL:
                animFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
                animFadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
                animTopIn = AnimationUtils.loadAnimation(this, R.anim.top_in);
                animTopOut = AnimationUtils.loadAnimation(this, R.anim.bottom_out);
                animZoomIn = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
                animZoomOut = AnimationUtils.loadAnimation(this, R.anim.zoom_out);
                animTopInSlow = AnimationUtils.loadAnimation(this, R.anim.top_in_slow);
                animTopOutSlow = AnimationUtils.loadAnimation(this, R.anim.bottom_out_slow);

                return null;
        }
        return null;
    }

    protected void hideViews(View... views) {
        for (View v : views) {
            v.setVisibility(View.GONE);
        }
    }

    protected void showViews(View... views) {
        for (View v : views) {
            v.setVisibility(View.VISIBLE);
        }
    }

    protected void hideViews(int... ids) {
        for (int i : ids) {
            findViewById(i).setVisibility(View.GONE);
        }
    }

    protected void showViews(int... ids) {
        for (int i : ids) {
            findViewById(i).setVisibility(View.VISIBLE);
        }
    }


    public void getData(boolean addMore) {

    }

    @Override
    public void startListeningToCall(BaseFeed feed, boolean isFreeCall) {
        if (!isFreeCall) {
            phoneListener = new CallListener(feed, this, isFreeCall);
            TelephonyManager telephony = (TelephonyManager)
                    getSystemService(Context.TELEPHONY_SERVICE);
            telephony.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
            CallPopUpInfo.isCallClicked = false;
        } else {
            ActivityUtils.saveFreeCallLogsToUtil(this, feed, getTag());
        }
    }

    public void stopCallListener() {
        TelephonyManager telephony = (TelephonyManager)
                getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(phoneListener, PhoneStateListener.LISTEN_NONE);
    }

    @Override
    public void onCallConnected(BaseFeed feed) {
        Log.d(TAG, "Call Connected:");
    }

    @Override
    public void onCallEnded(BaseFeed feed, String incomingNumber) {
        Log.d(TAG, "Call Ended:");
        stopCallListener();

        if (feed instanceof Feeds) {
            CallPopUpInfo.isCallClicked = true;
            CallPopUpInfo.feeds = (Feeds) feed;
            CallPopUpInfo.feeds.setIncomingNumber(incomingNumber);
        } else if (feed instanceof ProfileClass) {
            CallPopUpInfo.isCallClicked = true;
            CallPopUpInfo.feeds = ActivityUtils.createFeedObjectFromProfile((ProfileClass) feed);
            CallPopUpInfo.feeds.setIncomingNumber(incomingNumber);
        }

    }

    @Override
    public void onCallReceived(BaseFeed feed, boolean isFreeCall) {
        Log.d(TAG, "Call Received:");
        Log.d(TAG, "Free Call Received:" + isFreeCall);

    }

    protected ProgressDialog dialog;

    protected void showDialog() {
        if (dialog == null || !dialog.isShowing()) {
            dialog = new ProgressDialog(this);
            dialog.setMessage("Loading...");
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    protected void dismissDialog() {
        if (null != dialog && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {

            }
        }
    }

    protected String getEditTextString(int editTextViewId) {
        EditText et = (EditText) findViewById(editTextViewId);
        return et.getText().toString().trim();
    }



}
