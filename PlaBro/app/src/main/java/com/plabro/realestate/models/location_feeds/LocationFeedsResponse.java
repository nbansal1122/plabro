package com.plabro.realestate.models.location_feeds;


import com.plabro.realestate.models.ServerResponse;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class LocationFeedsResponse extends ServerResponse {

    LocationFeedsOutputParams output_params = new LocationFeedsOutputParams();

    public LocationFeedsOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(LocationFeedsOutputParams output_params) {
        this.output_params = output_params;
    }

}
