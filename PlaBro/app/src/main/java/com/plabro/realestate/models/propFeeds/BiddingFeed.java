package com.plabro.realestate.models.propFeeds;

import android.provider.BaseColumns;

import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plabro.realestate.utilities.JSONUtils;

@Table(name = "BiddingFeed")
public class BiddingFeed extends Feeds {

    @SerializedName("button_action")
    @Expose
    private String button_action;
    @SerializedName("button_title")
    @Expose
    private String button_title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("title_time")
    @Expose
    private String title_time;
    @SerializedName("img_url")
    @Expose
    private String img_url;
    @SerializedName("button_action_url")
    @Expose
    private String button_action_url;
    @SerializedName("exit_url")
    @Expose
    private String exit_url;
    @SerializedName("subtitle")
    @Expose
    private String subtitle;
    @SerializedName("footer")
    @Expose
    private String bidTimeInfo;
    @SerializedName("total_bid_count")
    @Expose
    private int total_bid_count;
    @SerializedName("auction_id")
    @Expose
    private String auction_id;
    @SerializedName("auction_status")
    @Expose
    private String auction_status;

    public String getAuction_status() {
        return auction_status;
    }

    public void setAuction_status(String auction_status) {
        this.auction_status = auction_status;
    }

    public String getAuction_id() {
        return auction_id;
    }

    public void setAuction_id(String auction_id) {
        this.auction_id = auction_id;
    }

    public int getTotal_bid_count() {
        return total_bid_count;
    }

    public void setTotal_bid_count(int total_bid_count) {
        this.total_bid_count = total_bid_count;
    }

    public String getBidTimeInfo() {
        return bidTimeInfo;
    }

    public void setBidTimeInfo(String bidTimeInfo) {
        this.bidTimeInfo = bidTimeInfo;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * @return The button_action
     */
    public String getButton_action() {
        return button_action;
    }

    /**
     * @param button_action The button_action
     */
    public void setButton_action(String button_action) {
        this.button_action = button_action;
    }

    /**
     * @return The button_title
     */
    public String getButton_title() {
        return button_title;
    }

    /**
     * @param button_title The button_title
     */
    public void setButton_title(String button_title) {
        this.button_title = button_title;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The title_time
     */
    public String getTitle_time() {
        return title_time;
    }

    /**
     * @param title_time The title_time
     */
    public void setTitle_time(String title_time) {
        this.title_time = title_time;
    }

    /**
     * @return The img_url
     */
    public String getImg_url() {
        return img_url;
    }

    /**
     * @param img_url The img_url
     */
    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    /**
     * @return The button_action_url
     */
    public String getButton_action_url() {
        return button_action_url;
    }

    /**
     * @param button_action_url The button_action_url
     */
    public void setButton_action_url(String button_action_url) {
        this.button_action_url = button_action_url;
    }

    public static BaseFeed parseJson(String json) {
        return (BaseFeed) JSONUtils.parseJson(json, BiddingFeed.class);
    }

    public String getExit_url() {
        return exit_url;
    }

    public void setExit_url(String exit_url) {
        this.exit_url = exit_url;
    }
}