package com.plabro.realestate.models.applaunch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppLaunchResponse {

    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("RT")
    @Expose
    public String RT;
    @SerializedName("loginStatus")
    @Expose
    public Boolean loginStatus;
    @SerializedName("output_params")
    @Expose
    public Output_params output_params;


    public static class Data {

        @SerializedName("json_share_version")
        @Expose
        public int json_share_version;

    }

    public static class Output_params {

        @SerializedName("data")
        @Expose
        public Data data;

    }
}