package com.plabro.realestate.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.text.TextUtils;

import com.plabro.realestate.AppController;
import com.plabro.realestate.utils.Hash;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class PBPreferences {

    private static final String PREFS_NAME = "PlaBro";
    private static final String AUTH_KEY = "auth_key";
    private static final String USER_IMG = "user_img";
    private static final String IMEI = "imei";
    private static final String DEVICE_ID = "device_id";
    private static final String PHONE = "phone";
    private static final String IS_CONTACTS_SEND = "isContactsSend";
    private static final String IS_REFRESH_BOOKMARK = "isRefreshBookmark";
    private static final String IS_REFRESH_SHOUT = "isRefreshShout";
    private static final String IS_REGISTRAR_ID_SEND = "is_reg_id_send";
    private static final String GCM_REGISTRAR_ID = "gcm_registrar_id_";
    private static final String IS_PUSH_NOTIFICATION_ENABLE = "is_push_notification_enable";
    private static final String IS_CHAT_NOTIFICATION_ENABLE = "is_chat_notification_enable";
    private static final String IS_DEBUG_ENABLE = "is_debug_enable";
    private static final String IS_LOGGED_IN = "is_logged_in";
    private static final String IS_NEW_POST = "is_new_post";
    private static final String IS_NEW_BROADCAST = "is_new_broadcast";
    private static final String BROADCAST_LINK = "broadcast_link";
    private static final String IS_NEW_FOLLOWER = "is_new_follower";
    private static final String IS_NEW_IM = "is_new_im";
    private static final String IS_COACH_SEEN = "is_coach_seen";
    private static final String IS_SESSION_EXPIRE_DIALOG_ACTIVE = "is_sess_exp";
    private static final String IS_HOME_DIALOG_ACTIVE = "is_home_dialog_active";
    private static final String IS_CURREnt_DEVICE_SELECTED = "is_curr_device_selected";
    private static final String IS_FIRST_TIME_USER = "is_first_time_user";
    private static final String NOTIFICATION_AUTHOR_ID = "mAuthorId";
    private static final String IM_AUTHOR = "im_author";
    private static final String IM_PHONE = "im_phone";
    private static final String NEW_IM_ON_APP = "new_im_on_app";
    private static final String CHAT_SIZE = "chat_size";
    private static final String TAT = "tat";
    private static final String FEEDS_CACHE = "feeds_cache";
    public static final String LAST_KNOWN_LAT = "LAT";
    public static final String LAST_KNOWN_LONG = "LONG";
    public static final String LAST_KNOWN_DISTANCE = "DISTANCE";
    public static final String LOCATION_UPDATE_TIMESTAMP = "LOCATION_UPDATE_TIMESTAMP";
    public static final String KEYBOARD_UPDATE_TIMESTAMP_FLAG = "keyboard_update_timestamp_flag";
    public static final String IS_CONTACTS_CLEARED = "is_contacts_cleared";
    public static final String BASE_URL = "base_url";
    public static final String TRACKER_TIME = "trackerTime1";
    public static final String APP_UPD_URL = "app_upd_url";
    public static final String APP_UPD_VER_NO = "app_upd_ver_no";
    private static final String IS_NEW_VERSION_AVAILABLE = "is_new_version_available";
    public static final String IS_SHOUT_CLICKED = "isShoutClicked4";
    public static final String SHORT_URL = "SHORT_URL";
    public static final String IS_DB_DISPOSED = "isDbDisposed";
    public static final String VERSION_QUERY_SENT = "isVersionNumberSent";
    public static final String KEY_ERROR_SYNC_CONTACTS = "errorSyncingContacts";
    public static final String KEY_VERSION_CODE = "KEY_VERSION_NAME";
    public static final String SELECTED_CITY_ID = "SelectedCityId";
    public static final String IS_CITY_UPDATE = "isCityUpdate";
    public static final String LAST_CITY_UPDATE_TIME = "cityUpdateTime";
    public static final String INVITE_SCREEN_COUNTER = "inviteScreenCounter";
    public static final String KEY_BROKER_CONTACT_JSON = "brokerContactJSON";
    public static final String HOME_COUNTER = "HomeCounter";
    public static final String IMAGE_URL = "imageUrl";
    public static final String IS_IMAGE_UPDATED = "isImageUpdated";
    public static final String KEY_LAUNCH = "keyInviteLaunch";
    public static final String NOTIF_ID = "notifId";
    public static final String GA_TRACK_ID = "GaTrackingId";
    public static final String CALL_READ_TIME = "CallReadTime";
    public static final String REQ_LOCATION_PERMISSION = "Req_Location_Permission";
    public static final String BADGE_COUNT = Constants.BUNDLE_KEYS.BADGE_COUNT;
    public static final String CHECK_BADGE_COUNT = "checkBadgeCount";
    public static final String CHAT_URL = "chatUrl";
    public static final String PAYMENT_URL = "paymentUrl";
    public static final String PAGE_COUNT = "pageCount";
    public static final String REQUEST_JSON_SHARE = "requestJsonShare";
    public static final String START_IDX = "startIdx";
    public static final String COUNTRY_CODE = "CountryCode";
    private static final String CUSTOMER_CARE = "CustomerCare";
    public static final String IS_CODES_RECEIVED = "isCountriesReceived";
    public static final String FROM_DEEP_LINKING = "fromDeepLinking";
    public static final String JSON_SHARE_VERSION = "json_share_version";
    public static final String MY_BIDS_URL = "myBidsUrl";
    public static final String MY_BIDS_EXIT_URL = "myBidsExitUrl";
    public static final String MY_BIDS_CITY_JSON = "MyBidsCityJson";
    public static final String ENABLE_CALL_SYNC = "enableCallSync";
    private static final String POST_SEARCH_CARD_ENABLED = "postSearchCardEnabled";
    public static final String KEY_AES_PROCESS = "keyAesProcess";
    public static final String IS_WIFI_ENABLED = "isWifiEnabled";
    public static final String IS_CONTACTS_DELETED = "isContactDeleted4";
    public static final String CONTACTS_MAP = "contactsMap";
    public static final String NOTIF_BADGE_COUNT = "notifBadgeCount";
    public static final String CHAT_BADGE_COUNT = "chatBadgeCount";
    public static final String KEY_IS_CONTACT_SYNCED_FT = "isContactSyncedFirstTime";
    public static final String IS_BROKER_CONTACT_ENABLED = "isBrokerContactsEnabled";
    private static PBPreferences prefs;
    public static String CAMPAIGN_URI = "campaignUri";
    private static final String PREV_SEARCH_QUERRY = "prev_search_querry";
    private static final String IS_WHATSAPP_LAUNCHED = "is_whatsapp_launched";
    private static final String CLIENT_PRIVATE_KEY = "client_private_key";
    private static final String CLIENT_PUBLIC_KEY = "client_public_key";
    private static final String SERVER_PUBLIC_KEY = "server_public_key";
    private static final String AES_KEY = "aes_key";
    private static final String AES_KEY_DECRYPTED = "aes_key_decrypted";
    private static final String ENC_SIGNATURE = "enc_signature";
    private static final String SETTING_NOTIFICATION_VAL = "setting_notification_val";
    private static final String FEED_NOTIFICATION_VAL = "feed_notification_val";
    private static final String HOME_TOOL_TIP_TIME = "home_tool_tip_time";
    private static final String HOME_TOOL_TIP_SET = "home_tool_tip_set";
    private static final String PB_TOOL_TIP_SET = "pb_tool_tip_set";
    private static final String COMPOSED_SHOUT_TEXT = "composed_shout_text";
    private static final String IS_PROFILE_EDITED = "is_profile_edited";
    public static final String KEY_AUTO_SYNC = "autoSync";
    private static final String IS_CLIENT_KEY_SENT = "is_client_key_sent";
    private static final String IS_TRIAL_VERSION = "is_trial_version";
    public static final String CITY_ID = "city_id";
    public static final String IM_USER_COUNTER = "im_user_counter";
    public static final String RATING_TIMER = "rating_timer";
    public static final String IS_BROKER_CONTACT_SYNCED = "autoSyncBrokerContacts";
    public static final String FEED_DETAILS_FAB_COUNTER = "feed_details_fab_counter";


    private PBPreferences() {
    }

    public static PBPreferences getInstance() {
        if (prefs != null)
            prefs = new PBPreferences();
        return prefs;
    }


    public static void setIsPushNotificationEnable(boolean isPushNotificationEnable) {
        getSharedPreferences().edit().putBoolean(IS_PUSH_NOTIFICATION_ENABLE, isPushNotificationEnable).commit();
    }

    public static boolean getIsPushNotificationEnable() {
        return getSharedPreferences().getBoolean(IS_PUSH_NOTIFICATION_ENABLE, true);
    }

    public static void setIsChatNotificationEnable(boolean isPushNotificationEnable) {
        getSharedPreferences().edit().putBoolean(IS_CHAT_NOTIFICATION_ENABLE, isPushNotificationEnable).commit();
    }

    public static boolean getIsChatNotificationEnable() {
        return getSharedPreferences().getBoolean(IS_CHAT_NOTIFICATION_ENABLE, true);
    }

    public static void setIsDebugEnable(boolean isDebugEnable) {
        getSharedPreferences().edit().putBoolean(IS_DEBUG_ENABLE, isDebugEnable).commit();
    }

    public static boolean getIsDebugEnable() {
        return getSharedPreferences().getBoolean(IS_DEBUG_ENABLE, false);
    }

    public static SharedPreferences getSharedPreferences() {
        return AppController.getInstance().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }


    public static String getAuthKey() {
        return getSharedPreferences().getString(AUTH_KEY + "", "");
    }

    public static void setAuthKey(String auth_key) {
        getSharedPreferences().edit().putString(AUTH_KEY, auth_key).commit();
    }

    public static long getTat() {
        return getSharedPreferences().getLong(TAT, 200000);
    }

    public static void setTat(long tat) {
        getSharedPreferences().edit().putLong(TAT, tat).commit();
    }

    public static boolean getContactsState() {
        return getSharedPreferences().getBoolean(IS_CONTACTS_SEND, false);
    }

    public static void setContactsState(Boolean state) {
        getSharedPreferences().edit().putBoolean(IS_CONTACTS_SEND, state).commit();
    }

    public static String getPhone() {
        return getSharedPreferences().getString(PHONE, "");
    }

    public static void setPhone(String phone) {
        getSharedPreferences().edit().putString(PHONE, phone).commit();
    }

    public static String getImei() {
        String imei = getSharedPreferences().getString(IMEI, "");
        String imeiEncrypted = Hash.md5(imei);
        return imeiEncrypted;
    }

    public static void setImei(String imei) {
        getSharedPreferences().edit().putString(IMEI, imei).commit();
    }

    public static String getDeviceId() {
        String deviceId = getSharedPreferences().getString(DEVICE_ID, "");
        if (TextUtils.isEmpty(deviceId)) {
            String android_id = Settings.Secure.getString(AppController.getInstance().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            deviceId = android_id;
            PBPreferences.setDeviceId(deviceId);
        }
        return deviceId;
    }

    public static void setDeviceId(String deviceId) {
        getSharedPreferences().edit().putString(DEVICE_ID, deviceId).commit();
    }

    public static boolean getBookmarkState() {
        return getSharedPreferences().getBoolean(IS_REFRESH_BOOKMARK, false);
    }

    public static void setBookmarkState(Boolean state) {
        getSharedPreferences().edit().putBoolean(IS_REFRESH_BOOKMARK, state).commit();
    }

    public static boolean getShoutState() {
        return getSharedPreferences().getBoolean(IS_REFRESH_SHOUT, false);
    }

    public static void setShoutState(Boolean state) {
        getSharedPreferences().edit().putBoolean(IS_REFRESH_SHOUT, state).commit();
    }

    public static String getGCMRegistrarId() {
        return getSharedPreferences().getString(GCM_REGISTRAR_ID, "");
    }

    public static void setGCMRegistrationId(String regId) {
        getSharedPreferences().edit().putString(GCM_REGISTRAR_ID, regId).commit();
    }

    public static boolean isRegistrarIdSend() {
        return getSharedPreferences().getBoolean(IS_REGISTRAR_ID_SEND, false);
    }

    public static void setIsRegistrarIdSend(Boolean isRegIdSend) {
        getSharedPreferences().edit().putBoolean(IS_REGISTRAR_ID_SEND, isRegIdSend).commit();
    }

    public static boolean getLoggedInState() {
        return getSharedPreferences().getBoolean(IS_LOGGED_IN, false);
    }

    public static void setLoggedInState(Boolean state) {
        getSharedPreferences().edit().putBoolean(IS_LOGGED_IN, state).commit();
    }

    public static boolean getIsProfileEdited() {
        return getSharedPreferences().getBoolean(IS_PROFILE_EDITED, false);
    }

    public static void setIsProfileEdited(Boolean state) {
        getSharedPreferences().edit().putBoolean(IS_PROFILE_EDITED, state).commit();
    }


    public static boolean getNewPostState() {
        return getSharedPreferences().getBoolean(IS_NEW_POST, false);
    }

    public static void setNewPostState(Boolean state) {
        getSharedPreferences().edit().putBoolean(IS_NEW_POST, state).commit();
    }

    public static boolean getNewBroadCastState() {
        return getSharedPreferences().getBoolean(IS_NEW_BROADCAST, false);
    }

    public static void setNewBroadCastState(Boolean state) {
        getSharedPreferences().edit().putBoolean(IS_NEW_BROADCAST, state).commit();
    }

    public static String getBroadCastUrl() {
        return getSharedPreferences().getString(BROADCAST_LINK, "");
    }

    public static void setBroadCastUrl(String broadCastUrl) {
        getSharedPreferences().edit().putString(BROADCAST_LINK, broadCastUrl).commit();
    }


    public static boolean getSessEXpDialogState() {
        return getSharedPreferences().getBoolean(IS_SESSION_EXPIRE_DIALOG_ACTIVE, false);
    }

    public static void setSessEXpDialogState(Boolean state) {
        getSharedPreferences().edit().putBoolean(IS_SESSION_EXPIRE_DIALOG_ACTIVE, state).commit();
    }


    public static boolean getHomeDialogState() {
        return getSharedPreferences().getBoolean(IS_HOME_DIALOG_ACTIVE, false);
    }

    public static void setHomeDialogState(Boolean state) {
        getSharedPreferences().edit().putBoolean(IS_HOME_DIALOG_ACTIVE, state).commit();
    }

    public static String getUserImg() {
        return getSharedPreferences().getString(USER_IMG, "");
    }

    public static void setUserImg(String imgUrl) {
        getSharedPreferences().edit().putString(USER_IMG, imgUrl).commit();
    }

    public static boolean getDeviceState() {
        return getSharedPreferences().getBoolean(IS_CURREnt_DEVICE_SELECTED, false);
    }

    public static void setDeviceState(Boolean state) {
        getSharedPreferences().edit().putBoolean(IS_CURREnt_DEVICE_SELECTED, state).commit();
    }

    public static boolean getFirstTimeState() {
        return getSharedPreferences().getBoolean(IS_FIRST_TIME_USER, true);
    }


    public static void setFirstTimeState(Boolean state) {
        getSharedPreferences().edit().putBoolean(IS_FIRST_TIME_USER, state).commit();
    }

    public static boolean getNewFollowerState() {
        return getSharedPreferences().getBoolean(IS_NEW_FOLLOWER, false);
    }

    public static void setNewFollowerState(Boolean state) {
        getSharedPreferences().edit().putBoolean(IS_NEW_FOLLOWER, state).commit();
    }

    public static boolean getNewIMState() {
        return getSharedPreferences().getBoolean(IS_NEW_IM, false);
    }

    public static void setNewIMState(Boolean state) {
        getSharedPreferences().edit().putBoolean(IS_NEW_IM, state).commit();
    }

    public static String getNotAuthorId() {
        return getSharedPreferences().getString(NOTIFICATION_AUTHOR_ID, "");
    }

    public static void setNotAuthorId(String mAuthorID) {
        getSharedPreferences().edit().putString(NOTIFICATION_AUTHOR_ID, mAuthorID).commit();
    }


    public static String getIMAuthorId() {
        return getSharedPreferences().getString(IM_AUTHOR, "");
    }

    public static void setIMAuthorId(String mAuthorID) {
        getSharedPreferences().edit().putString(IM_AUTHOR, mAuthorID).commit();
    }

    public static String getIMPhone() {
        return getSharedPreferences().getString(IM_PHONE, "");
    }

    public static void setIMPhone(String mAuthorID) {
        getSharedPreferences().edit().putString(IM_PHONE, mAuthorID).commit();
    }


    public static boolean getCoachState() {
        return getSharedPreferences().getBoolean(IS_COACH_SEEN, false);
    }

    public static void setCoachState(Boolean state) {
        getSharedPreferences().edit().putBoolean(IS_COACH_SEEN, state).commit();
    }

    public static Set<String> getHomeToolTipSet() {
        Set<String> blankSet = new HashSet<String>();
        return getSharedPreferences().getStringSet(HOME_TOOL_TIP_SET, blankSet);
    }

    public static void setHomeToolTipSet(Set<String> val) {
        getSharedPreferences().edit().putStringSet(HOME_TOOL_TIP_SET, val).commit();
    }


    public static Set<String> getPBToolTipSet() {
        Set<String> blankSet = new HashSet<String>();
        return getSharedPreferences().getStringSet(PB_TOOL_TIP_SET, blankSet);
    }

    public static void setPBToolTipSet(Set<String> val) {
        getSharedPreferences().edit().putStringSet(PB_TOOL_TIP_SET, val).commit();
    }


    public static long getHomeToolTipTime() {
        return getSharedPreferences().getLong(HOME_TOOL_TIP_TIME, 0);
    }

    public static void setHomeToolTipTime(long val) {
        getSharedPreferences().edit().putLong(HOME_TOOL_TIP_TIME, val).commit();
    }

    public static boolean getNewIMOnApp() {
        return getSharedPreferences().getBoolean(NEW_IM_ON_APP, false);
    }

    public static void setNewIMOnApp(Boolean state) {
        getSharedPreferences().edit().putBoolean(NEW_IM_ON_APP, state).commit();
    }

    public static int getChatSize() {
        return getSharedPreferences().getInt(CHAT_SIZE, 0);
    }

    public static void setChatSize(int chatSize) {
        getSharedPreferences().edit().putInt(CHAT_SIZE, chatSize).commit();
    }

    public static int getFeedDetailsFabCounter() {
        return getSharedPreferences().getInt(FEED_DETAILS_FAB_COUNTER, 0);
    }

    public static void setFeedDetailsFabCounter(int ctr) {
        getSharedPreferences().edit().putInt(FEED_DETAILS_FAB_COUNTER, ctr).commit();
    }

    public static synchronized void saveData(String key, String value) {
        getSharedPreferences().edit().putString(key, value).commit();
    }

    public static synchronized void saveData(String key, float value) {
        getSharedPreferences().edit().putFloat(key, value).commit();
    }

    public static synchronized void saveData(String key, long value) {
        getSharedPreferences().edit().putLong(key, value).commit();
    }

    public static synchronized void saveData(String key, int value) {
        getSharedPreferences().edit().putInt(key, value).commit();
    }

    public static synchronized void saveData(String key, boolean value) {
        getSharedPreferences().edit().putBoolean(key, value).commit();
    }

    public static synchronized String getData(String key, String value) {
        return getSharedPreferences().getString(key, value);
    }

    public static synchronized int getData(String key, int value) {
        return getSharedPreferences().getInt(key, value);
    }

    public static synchronized boolean getData(String key, boolean value) {
        return getSharedPreferences().getBoolean(key, value);
    }

    public static synchronized float getData(String key, float value) {
        return getSharedPreferences().getFloat(key, value);
    }

    public static synchronized long getData(String key, long value) {
        return getSharedPreferences().getLong(key, value);
    }


    public static void setIsContactsCleared(boolean isContactsCleared) {
        getSharedPreferences().edit().putBoolean(IS_CONTACTS_CLEARED, isContactsCleared).commit();
    }

    public static boolean getIsContactsCleared() {
        return getSharedPreferences().getBoolean(IS_CONTACTS_CLEARED, false);
    }

    public static String getBaseUrl() {
        return getSharedPreferences().getString(BASE_URL, "");
    }

    public static void setBaseUrl(String baseURL) {
        getSharedPreferences().edit().putString(BASE_URL, baseURL).commit();
    }


    public static String getLastKnownLat() {
        return getSharedPreferences().getString(LAST_KNOWN_LAT, "");
    }

    public static void setLastKnownLat(String latitute) {
        getSharedPreferences().edit().putString(LAST_KNOWN_LAT, latitute).commit();
    }


    public static String getLastKnownLong() {
        return getSharedPreferences().getString(LAST_KNOWN_LONG, "");
    }

    public static void setLastKnownLong(String longitude) {
        getSharedPreferences().edit().putString(LAST_KNOWN_LONG, longitude).commit();
    }

    public static String getPrevSearchQuerry() {
        return getSharedPreferences().getString(PREV_SEARCH_QUERRY, "");
    }

    public static void setPrevSearchQuerry(String prevSearchQuerry) {
        getSharedPreferences().edit().putString(PREV_SEARCH_QUERRY, prevSearchQuerry).commit();
    }


    public static int getLastKnownDistance() {
        return getSharedPreferences().getInt(LAST_KNOWN_DISTANCE, 0);
    }

    public static void setLastKnownDistance(int distance) {
        getSharedPreferences().edit().putInt(LAST_KNOWN_DISTANCE, distance).commit();
    }

    public static Long getLocationUpdateTimestamp() {
        return getSharedPreferences().getLong(LOCATION_UPDATE_TIMESTAMP, 0L);
    }

    public static void setLocationUpdateTimestamp(Long locationTime) {
        getSharedPreferences().edit().putLong(LOCATION_UPDATE_TIMESTAMP, locationTime).commit();
    }

    public static Boolean getKeyboardUpdateTimestamp() {
        return getSharedPreferences().getBoolean(KEYBOARD_UPDATE_TIMESTAMP_FLAG, false);
    }

    public static void setKeyboardUpdateTimestamp(boolean locationTimeFlag) {
        getSharedPreferences().edit().putBoolean(KEYBOARD_UPDATE_TIMESTAMP_FLAG, locationTimeFlag).commit();
    }

    public static void setIsNewVersionAvailbale(boolean isNewVersionAvailbale) {
        getSharedPreferences().edit().putBoolean(IS_NEW_VERSION_AVAILABLE, isNewVersionAvailbale).commit();
    }

    public static boolean getIsNewVersionAvailbale() {
        return getSharedPreferences().getBoolean(IS_NEW_VERSION_AVAILABLE, false);
    }

    public static String getAppUpdateUrl() {
        return getSharedPreferences().getString(APP_UPD_URL, "");
    }

    public static void setAppUpdateUrl(String appURL) {
        getSharedPreferences().edit().putString(APP_UPD_URL, appURL).commit();
    }

    public static int getAppUpdateVer() {
        return getSharedPreferences().getInt(APP_UPD_VER_NO, 0);
    }

    public static void setAppUpdateVer(int appVer) {
        getSharedPreferences().edit().putInt(APP_UPD_VER_NO, appVer).commit();
    }


    public static String getClientPrivateKey() {
        return getSharedPreferences().getString(CLIENT_PRIVATE_KEY, "");
    }

    public static void setClientPrivateKey(String val) {
        getSharedPreferences().edit().putString(CLIENT_PRIVATE_KEY, val).commit();
    }

    public static String getClientPublicKey() {
        return getSharedPreferences().getString(CLIENT_PUBLIC_KEY, "");
    }

    public static void setClientPublicKey(String val) {
        getSharedPreferences().edit().putString(CLIENT_PUBLIC_KEY, val).commit();
    }

    public static String getServerPublicKey() {
        return getSharedPreferences().getString(SERVER_PUBLIC_KEY, "");
    }

    public static void setServerPublicKey(String val) {
        getSharedPreferences().edit().putString(SERVER_PUBLIC_KEY, val).commit();
    }

    public static String getAESKey() {
        return getSharedPreferences().getString(AES_KEY, "");
    }

    public static void setAESKey(String val) {
        getSharedPreferences().edit().putString(AES_KEY, val).commit();
    }

    public static String getAESKeyDecrypted() {
        return getSharedPreferences().getString(AES_KEY_DECRYPTED, "");
    }

    public static void setAESKeyDecrypted(String val) {
        getSharedPreferences().edit().putString(AES_KEY_DECRYPTED, val).commit();
    }

    public static String getComposedShoutText() {
        return getSharedPreferences().getString(COMPOSED_SHOUT_TEXT, "");
    }

    public static void setComposedShoutText(String val) {
        getSharedPreferences().edit().putString(COMPOSED_SHOUT_TEXT, val).commit();
    }


    public static void setClientKeySentToServer(boolean isNewVersionAvailbale) {
        getSharedPreferences().edit().putBoolean(IS_CLIENT_KEY_SENT, isNewVersionAvailbale).commit();
    }

    public static boolean getClientKeySentToServer() {
        return getSharedPreferences().getBoolean(IS_CLIENT_KEY_SENT, false);
    }

    public static String getEncSignature() {
        return getSharedPreferences().getString(ENC_SIGNATURE, "");
    }

    public static void setEncSignature(String val) {
        getSharedPreferences().edit().putString(ENC_SIGNATURE, val).commit();
    }

    public static String getSettingNotificationVal() {
        return getSharedPreferences().getString(SETTING_NOTIFICATION_VAL, "");
    }

    public static void setSettingNotificationVal(String val) {
        getSharedPreferences().edit().putString(SETTING_NOTIFICATION_VAL, val).commit();
    }

    public static int getFeedNotificationVal() {
        return getSharedPreferences().getInt(FEED_NOTIFICATION_VAL, 0);
    }

    public static void setFeedNotificationVal(int val) {
        getSharedPreferences().edit().putInt(FEED_NOTIFICATION_VAL, val).commit();
    }

    private static HashMap<String, String> contactsMap;

    public static void initContactsMap() {
        contactsMap = loadMap(CONTACTS_MAP);
    }

    public static HashMap<String, String> getContactsMap() {
        if (contactsMap == null) {
            contactsMap = loadMap(CONTACTS_MAP);
        }
        return contactsMap;
    }

    public static void saveMap(Map<String, String> inputMap, String mapName) {
        SharedPreferences pSharedPref = getSharedPreferences();
        contactsMap = (HashMap<String, String>) inputMap;
        if (pSharedPref != null) {
            JSONObject jsonObject = new JSONObject(inputMap);
            String jsonString = jsonObject.toString();
            SharedPreferences.Editor editor = pSharedPref.edit();
            editor.remove(mapName).commit();
            editor.putString(mapName, jsonString);
            editor.commit();
        }
    }

    public static HashMap<String, String> loadMap(String mapName) {
        HashMap<String, String> outputMap = new HashMap<String, String>();
        SharedPreferences pSharedPref = getSharedPreferences();
        try {
            if (pSharedPref != null) {
                String jsonString = pSharedPref.getString(mapName, (new JSONObject()).toString());
                JSONObject jsonObject = new JSONObject(jsonString);
                Iterator<String> keysItr = jsonObject.keys();
                while (keysItr.hasNext()) {
                    String key = keysItr.next();
                    String value = (String) jsonObject.get(key);
                    outputMap.put(key, value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputMap;
    }

    public static void setIsWhatsappLaunched(boolean isWhatsAppLaunched) {
        getSharedPreferences().edit().putBoolean(IS_WHATSAPP_LAUNCHED, isWhatsAppLaunched).commit();
    }

    public static boolean getIsWhatsappLaunched() {
        return getSharedPreferences().getBoolean(IS_WHATSAPP_LAUNCHED, false);
    }

    public static int getTrialVersionState() {

//          return 0;//todo remove
        return getSharedPreferences().getInt(IS_TRIAL_VERSION, 2);
    }

    public static void setTrialVersionState(int state) {
        getSharedPreferences().edit().putInt(IS_TRIAL_VERSION, state).commit();
    }


    public static long getRatingTimer() {
        return getSharedPreferences().getLong(RATING_TIMER, System.currentTimeMillis());
    }

    public static void setRatingTimer(long time) {
        getSharedPreferences().edit().putLong(RATING_TIMER, time).commit();
    }

    public static void clearAll() {
        getSharedPreferences().edit().clear().commit();
        Utility.deleteLoginObject();
    }

    public static String getCountryCode() {
        return getData(COUNTRY_CODE, Constants.DEF_COUNTRY_CODE);
    }

    public static String getPlabroCustomerCare() {
        return getData(CUSTOMER_CARE, "+919599480504");
    }

    public static boolean getPostSearchStatus() {
        return getSharedPreferences().getBoolean(POST_SEARCH_CARD_ENABLED, false);
    }

    public static void setPostSearchStatus(Boolean state) {
        getSharedPreferences().edit().putBoolean(POST_SEARCH_CARD_ENABLED, state).commit();
    }
}
