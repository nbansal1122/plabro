package com.plabro.realestate.models.payment;


import com.plabro.realestate.CustomLogger.Log;
import com.plabro.realestate.models.ServerResponse;
import com.plabro.realestate.utilities.JSONUtils;
import com.plabro.realestate.volley.AppVolleyCacheManager;

/**
 * Created by hemantkumar on 08/01/15.
 */
public class PaymentResponse extends ServerResponse {
    PaymentOutputParams output_params = new PaymentOutputParams();

    public PaymentOutputParams getOutput_params() {
        return output_params;
    }

    public void setOutput_params(PaymentOutputParams output_params) {
        this.output_params = output_params;
    }

    public static PaymentResponse parseJson(String json) {
        PaymentResponse response = (PaymentResponse) JSONUtils.parseJson(json, PaymentResponse.class);
        String dataE = response.getOutput_params().getDataE();

        Log.d("Payment Response", dataE + "");
        String data = AppVolleyCacheManager.getDecryptedData(dataE);
        Log.d("Payment Response", "" + data);
        PaymentFinalData data1 = (PaymentFinalData) JSONUtils.parseJson(data, PaymentFinalData.class);
        Log.d("Payment Response", "Final Data null or not" + data1);
        response.getOutput_params().setData(data1);

        return response;
    }
}
