package com.plabro.realestate.models.Tracker;

import java.io.Serializable;

/**
 * Created by jmd on 5/14/2015.
 */
public class WifiInfo implements Serializable {
    private String SSID;
    private String BSSID;

    public String getSSID() {
        return SSID;
    }

    public void setSSID(String SSID) {
        this.SSID = SSID;
    }

    public String getBSSID() {
        return BSSID;
    }

    public void setBSSID(String BSSID) {
        this.BSSID = BSSID;
    }
}
