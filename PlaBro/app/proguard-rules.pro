# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/hemant/tools/adt-bundle-linux-x86_64-20140702/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}


# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/hemant/tools/adt-bundle-linux-x86_64-20140702/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-include ../proguard-com.digits.sdk.android.digits.txt

-keepattributes SourceFile, LineNumberTable

##---------------Begin: proguard configuration for XMPP  ----------
-dontwarn org.jivesoftware.**

-keepclasseswithmembers class de.measite.smack.** {
    *;
}

-keepclasseswithmembers class * extends org.jivesoftware.smack.sasl.SASLMechanism {
    public <init>(org.jivesoftware.smack.SASLAuthentication);
}

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.

-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }
-keep public class com.google.gson
-keep class com.plabro.realestate.models.** { *; }

##---------------Begin: proguard configuration for Google Analytics  ----------

-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

-keepclassmembers class * {
    @fully.qualified.package.AnnotationType *;
}

-keep class com.activeandroid.** {
     *;
}
#-keep class me.dm7.barcodescanner.** {
#     *;
#}

-keep class android.support.** { *; }


-keep interface android.support.** { *; }

-dontwarn com.wizrocket.android.sdk.**
-dontwarn com.clevertap.android.sdk.**

-dontwarn com.squareup.picasso.**

-dontwarn com.github.castorflex.**

-dontwarn com.github.castorflex.**

-dontwarn fr.castorflex.android.**

-dontwarn org.slf4j.**

-dontwarn com.nhaarman.supertooltips.**

# For Facebook SDK
-keepattributes Signature
-keep class com.facebook.** {
*;
}
-keep class me.dm7.barcodescanner.** {
  *;
}

-keep class net.sourceforge.zbar.** {
  *;
}
-keep class com.desmond.parallaxviewpager.** {
  *;
}

# For CleverTap SDK
-dontwarn com.clevertap.android.sdk.**






#other file added

-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*
-optimizationpasses 5
-allowaccessmodification
-dontpreverify

# The remainder of this file is identical to the non-optimized version
# of the Proguard configuration file (except that the other file has
# flags to turn off optimization).

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose

-dontwarn javax.xml.**
-keepattributes *Annotation*
-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
    native <methods>;
}

# keep setters in Views so that animations can still work.
# see http://proguard.sourceforge.net/manual/examples.html#beans
-keepclassmembers public class * extends android.view.View {
   void set*(***);
   *** get*();
}

# We want to keep methods in Activity that could be used in the XML attribute onClick
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

-keepattributes SourceFile,LineNumberTable

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose



-keep class !android.support.v7.internal.view.menu.**,android.support.** {*;}

-keepattributes Signature
-dontwarn com.crashlytics.**
-keep class com.crashlytics.** { *; }
-keep class com.crashlytics.android.Answers.**{*;}
-keepattributes SourceFile,LineNumberTable
-keep class org.apache.http.** { *; }
-keep class org.apache.commons.codec.** { *; }
-keep class org.apache.commons.logging.** { *; }
-keep class android.net.compatibility.** { *; }
-keep class android.net.http.** { *; }
-keep class com.android.internal.http.multipart.** { *; }
-dontwarn org.apache.http.**
-dontwarn android.webkit.**

-dontwarn org.apache.http.**
-dontwarn android.net.http.AndroidHttpClient
-dontwarn com.google.android.gms.**
-dontwarn com.android.volley.toolbox.**

-dontwarn soundcloud.android.crop.*
-dontwarn org.apache.**
-dontwarn android.webkit.**
-dontwarn com.mixpanel.android.**
-dontwarn com.google.android.**

-dontwarn java.lang.invoke**
-dontwarn org.apache.lang.**
-dontwarn org.apache.commons.**

-keep class com.MyApp.subscreens.Search

-keep class android.support.v7.widget.** { *; }

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**

#digits rules
-dontwarn  com.digits.sdk.android.*ActionBarActivity

# retrofit specific
-dontwarn com.squareup.okhttp.**
-dontwarn com.google.appengine.api.urlfetch.**
-dontwarn rx.**
-dontwarn retrofit.**
-keepattributes Signature
-keepattributes *Annotation*
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}


